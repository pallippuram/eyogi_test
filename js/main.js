$(function () {

    var currentDate; // Holds the day clicked when adding a new event
    var currentEvent; // Holds the event object when editing an event
    var base_url = 'http://dev.eyogi.in/'; // Here i define the base_url
    $('#color').colorpicker(); // Colopicker
    var url = get_url();
    var arr = url.split('/');
//  $('div.fc-slats tr.fc-minor').each(function() {
//      console.log("bac")
////        $(this).find("td input:text,select").each(function() {
////            textVal = this.value;
////            inputName = $(this).attr("name");
////            formData+='&'+inputName+'='+textVal;
////        });
////        InsideCount++   
//    });

    $.ajax({
        url: base_url + 'admin/getCous',
        data: {id: arr[arr.length - 1]},
        type: 'post',
        success: function (data) {

            var res = data.split('-')
            $('#min').val(res[0])
            $('#max').val(res[1])
            $('#dur').val(res[2])
            $('#calendar').fullCalendar({

                header: {
                    left: 'prev, next, today',
                    center: 'title',
                    right: 'month, basicWeek, basicDay'
                },

                // Get all events stored in database
                minTime: res[0],
                maxTime: res[1],
                defaultView: 'agendaWeek',
                eventLimit: true, // allow "more" link when too many events
                events: base_url + 'calendar/getEvents?id=' + $('#user_id').val(),
                selectable: true,
                selectHelper: true,
                editable: true, // Make the event resizable true  
                slotDuration: '00:15:00',
                nowIndicator: true,
                displayEventEnd: true,
                select: function (start, end) {

                    $('#start').val(moment(start).format('YYYY-MM-DD HH:mm:ss'));
                    $('#end').val(moment(end).format('YYYY-MM-DD HH:mm:ss'));



                    if (moment().diff(start, 'minutes') > 1) {
                        $('#calendar').fullCalendar('unselect');
                        return false;
                    }



                    if ($('#role_hidden').val() == 3) {
                        $('#calendar').fullCalendar('unselect');
                        return false;
                    }
                    // Open modal to add event
                    modal({
                        // Available buttons when adding
                        buttons: {
                            add: {
                                id: 'add-event', // Buttons id
                                css: 'btn-success', // Buttons class
                                label: 'Add' // Buttons label
                            }
                        },
                        title: 'Add Event', // Modal title
                        refresh: true
                    });
                },
                eventAfterRender: function (event, $el, view) {
                    $el.removeClass('fc-short');
                },
                eventDrop: function (event, delta, revertFunc, start, end) {

                    start = event.start.format('YYYY-MM-DD HH:mm:ss');
                    if (event.end) {
                        end = event.end.format('YYYY-MM-DD HH:mm:ss');
                    } else {
                        end = start;
                    }

                    $.post(base_url + 'calendar/dragUpdateEvent', {
                        id: event.id,
                        start: start,
                        end: end
                    }, function (result) {
                        $('.alert').addClass('alert-success').text('Event updated successfuly');
                        hide_notify();


                    });



                },
                eventResize: function (event, dayDelta, minuteDelta, revertFunc) {

                    start = event.start.format('YYYY-MM-DD HH:mm:ss');
                    if (event.end) {
                        end = event.end.format('YYYY-MM-DD HH:mm:ss');
                    } else {
                        end = start;
                    }

                    $.post(base_url + 'calendar/dragUpdateEvent', {
                        id: event.id,
                        start: start,
                        end: end
                    }, function (result) {
                        $('.alert').addClass('alert-success').text('Event updated successfuly');
                        hide_notify();

                    });
                },
                eventRender: function (event, element) {
                    element.attr('title', event.description);

                    if ($('#role_hidden').val() == 3) {
                        var text = event.title;
                        if ($.isNumeric(text) || text.indexOf("@") != -1) {
                            element.text("Anonymous User")
                        } else {
                            element.text(event.title)
                        }
                    }

                },
                viewRender: function (view) {
                    minTime: getMintime();
                    maxTime:   getMaxtime();
                },
                // Event Mouseover
                eventMouseover: function (calEvent, jsEvent, view) {

                    var tooltip = '<div class="event-tooltip">' + calEvent.description + '</div>';
                    $("body").append(tooltip);

                    $(this).mouseover(function (e) {
                        $(this).css('z-index', 10000);
                        $('.event-tooltip').fadeIn('500');
                        $('.event-tooltip').fadeTo('10', 1.9);
                    }).mousemove(function (e) {
                        $('.event-tooltip').css('top', e.pageY + 10);
                        $('.event-tooltip').css('left', e.pageX + 20);
                    });
                },
                eventMouseout: function (calEvent, jsEvent) {
                    $(this).css('z-index', 8);
                    $('.event-tooltip').remove();
                },
                // Handle Existing Event Click
                eventClick: function (calEvent, jsEvent, view) {
                    // Set currentEvent variable according to the event clicked in the calendar
                    currentEvent = calEvent;
                    // Open modal to edit or delete event
                    var url = get_url();
                    var arr = url.split('/');

                    if (arr[arr.length - 1] == "index#_=_" || arr[arr.length - 1] == "index") {
                        modal({
                            // Available buttons when editing
                            buttons: {

                            },
                            title: 'Edit Event "' + calEvent.title + '"',
                            event: calEvent
                        });
                    } else {
                        modal({
                            // Available buttons when editing
                            buttons: {
                                delete: {
                                    id: 'delete-event',
                                    css: 'btn-danger',
                                    label: 'Delete'
                                },
                                update: {
                                    id: 'update-event',
                                    css: 'btn-success',
                                    label: 'Update'
                                },
                                reschedule: {
                                    id: 'reschedule-event',
                                    css: 'btn-primary',
                                    label: 'Reschedule'
                                }

                            },
                            title: 'Edit Event "' + calEvent.title + '"',
                            event: calEvent
                        });
                    }
                }

            });

        }
    });



    // Fullcalendar

    function GetDateFormat(date) {
        var month = (date.getMonth() + 1).toString();
        month = month.length > 1 ? month : '0' + month;
        var day = date.getDate().toString();
        day = day.length > 1 ? day : '0' + day;
        return date.getFullYear() + '-' + month + '-' + day;
    }
    // Prepares the modal window according to data passed
    function modal(data) {
        // update_time_ends($('#dur').val())

        var url = get_url();
        // console.log(formatAMPM($('#start').val()))
        var arr = url.split('/');
        if (arr[arr.length - 1] == "counselorSchedule") {
            var schedule_id = data.event.id;
            var visitor_info = data.event.uid;
            var dateObj = new Date(data.event ? data.event.start : '');
            var date = GetDateFormat(dateObj);
            var start = data.event.start._i;
            var sche_id = data.event.schedule_id;

            sheduleDetials(schedule_id, visitor_info, start, sche_id);

        } else {
            // Set modal title
            $('.modal-title').html(data.title);
            // Clear buttons except Cancel
            $('.modal-footer button:not(".btn-default")').remove();
            // Set input values
            var time = formatAMPM($('#start').val());
            var range = formatAMPM(new Date(new Date($('#start').val()).getTime() + 15 * 60000))

            var maxTime = $('#max').val()
            $('#startTime').timepicker({
                'minTime': time,
                'maxTime': maxTime,
                'step': 15,
                'timeFormat': 'h:i A',

            });
            $('#endTime').timepicker({
                'minTime': time,
                'showDuration': true,
                'maxTime': maxTime,
                'step': 15,
                'timeFormat': 'h:i A',
                disableTimeRanges: [[time, range]]
            });
            
            $('#endTime').prop('disabled', true)
            $('#startTime').val(formatAMPM($('#start').val()));
             var time_end = moment.utc(moment($('#start').val()).format('HH:mm:ss'), 'hh:mm A').add("45", 'minutes').format('hh:mm A')

                  
//            $('#time_slot_start').val(formatAMPM($('#start').val()));
//            $('#time_slot_dur').val($('#dur').val());
//            $('#time_slot_ends').val(formatAMPM(new Date(new Date($('#start').val()).getTime() + $('#time_slot_dur').val() * 60000)));
            var dateObj = new Date(data.event ? data.event.start : '');
            var date = GetDateFormat(dateObj);
            var uid = data.event ? data.event.uid : '';
            
            if ($.isNumeric(data.event ? data.event.uid : '') || uid == "") {
                $('#div_username').html("");
                $('#div_username').html('<select data-options="{&quot;allowClear&quot;:false,&quot;width&quot;:&quot;none&quot;,&quot;placeholder&quot;:&quot;&quot;,&quot;minimumResultsForSearch&quot;:-1}" required id="user" class="form-control iwjmb-select_advanced" name="gender"></select>');
                $('#user').append($('#user_listing').html());
                $('#user').val(data.event ? data.event.uid : '').trigger('change');
            } else {
                var title = data.event ? data.event.title : '';
                console.log(uid)
                
                $('#div_username').html("");
                //  $('#div_username').append('<input type="text" class="form-control" name="" value="'+ title +'" disabled>');
                $('#div_username').append('<input type="text" class="form-control" name="" value="Anonymous User" disabled><input type="hidden" id="user">');
            $('#user').val(data.event ? data.event.uid : '').trigger('change');
            }


            $('#schedule_table_id').val(data.event ? data.event.id : '');

            if (typeof data.event != "undefined") {
                $('#startTime').val(formatAMPM(data.event ? data.event.start._i : $('#start').val()));
                $('#startTime').attr('disabled', true)
                $('#endTime').attr('disabled', true)
                $('#endTime').val(formatAMPM(data.event ? data.event.end._i : '')).trigger('change');
            } else {

                $('#endTime').val(time_end)
            }
         
            $('#date').val(data.event ? date : '');

            $('#description').val(data.event ? data.event.description : '');

            $('#schedule_id').val(data.event ? data.event.schedule_id : '').trigger('change');


            // Create Butttons
            $.each(data.buttons, function (index, button) {
                $('.modal-footer').prepend('<button type="button" id="' + button.id + '" class="btn ' + button.css + '">' + button.label + '</button>')
            })
            //Show Modal
            $('.add_event_popup').modal('show');
        }
    }

    // Handle Click on Add Button
    $('.modal').on('click', '#add-event', function (e) {
        
        var start = moment($('#start').val()).format('YYYY-MM-DD')
        var time = getTime(start, $('#startTime').val());
        var time1 = getTime(start, $('#endTime').val());
      
        
        if (validator(['user', 'description'])) {
            $.post(
                    base_url + 'calendar/addEvent',
                    {
                        user_id: $('#user').val(),
                        description: $('#description').val(),
                        start: time,
                        end: time1,
                        con_id: $('#user_id').val(),
                        sche_phone: "",
                        schedule_id: $('#schedule_id').val(),
                        videocall:0
                    }, function (result) {
                if (result == 2) {
                    alert('User account balance is low to schedule this appointment')
                } else {
                    $('.modal').modal('hide');
                    $('#calendar').fullCalendar("refetchEvents");
                    hide_notify();
                }
            });
        }
    });


    // Handle click on Update Button
    $('.modal').on('click', '#update-event', function (e) {
        if (validator(['user', 'description'])) {
            $.post(base_url + 'calendar/updateEvent', {
                id: currentEvent.id,
                user_id: $('#user').val(),
                description: $('#description').val(),
                start: $('#start').val(),
                end: $('#end').val(),
                schedule_id: $('#schedule_id').val(),
            }, function (result) {
                $('.alert').addClass('alert-success').text('Event updated successfuly');
                $('.modal').modal('hide');
                $('#calendar').fullCalendar("refetchEvents");
                hide_notify();

            });
        }
    });

    // Handle click on Reschedule Button
    $('.modal').on('click', '#reschedule-event', function (e) {
        
        $.post(base_url + 'calendar/checkScheduleValid', {
            id: currentEvent.id,
            user_id: $('#user').val(),
            start: $('#date').val(),
            end: $('#end').val(),
            schedule_id: $('#schedule_id').val(),
        }, function (result) {


            var range = formatAMPM(new Date(new Date($('#start').val()).getTime() + 15 * 60000))
            var minTime = $('#min').val()
            var maxTime = $('#max').val()
            $('#startTime_re').timepicker({
                'minTime': minTime,
                'maxTime': maxTime,
                'step': 15,
                'timeFormat': 'h:i A',

            });
            $('#endTime_re').timepicker({
                'minTime': minTime,
                'showDuration': true,
                'maxTime': maxTime,
                'step': 15,
                'timeFormat': 'h:i A',

            });
            $('#reschedule_div').show();
            $("#reschedule-event").attr("id", "save-event").text('Save Details');

        });
    });

    // Handle click on Reschedule save Button
    $('.modal').on('click', '#save-event', function (e) {


        var start = $('#schedule_date').val()
        var time = getTime(start, $('#startTime_re').val());
        var time1 = getTime(start, $('#endTime_re').val());

        $('#start').val(arr[0] + " " + time)


        $.post(base_url + 'calendar/rescheduleEvent', {
            id: currentEvent.id,
            con_id: $('#user_id').val(),
            user_id: $('#user').val(),
            reason: $('#reason').val(),
            date: $('#schedule_date').val(),
            cur_date: $('#date').val(),
            start: time,
            end: time1,
            schedule_id: '',
            schedule_table_id: $('#schedule_table_id').val(),
            cur_schedule_id: $('#schedule_id').val(),
        }, function (result) {

            $('.modal').modal('hide');
            $('#calendar').fullCalendar("refetchEvents");
            hide_notify();

        });

    });

    // Handle Click on Delete Button
    $('.modal').on('click', '#delete-event', function (e) {
        $.get(base_url + 'calendar/deleteEvent?id=' + currentEvent.id + '&date=' + $('#date').val(), function (result) {
            $('.alert').addClass('alert-success').text('Event deleted successfully !');
            $('.modal').modal('hide');
            $('#calendar').fullCalendar("refetchEvents");
            hide_notify();
        });
    });

    function hide_notify()
    {
        setTimeout(function () {
            $('.alert').removeClass('alert-success').text('');
        }, 2000);
    }


    // Dead Basic Validation For Inputs
    function validator(elements) {
        var errors = 0;
        $.each(elements, function (index, element) {
            if ($.trim($('#' + element).val()) == '')
                errors++;
        });
        if (errors) {
            $('.error').html('Please insert title and description');
            return false;
        }
        return true;
    }
});



/*$(function () {

    var currentDate; // Holds the day clicked when adding a new event
    var currentEvent; // Holds the event object when editing an event
    var base_url = 'http://dev.eyogi.in/'; // Here i define the base_url
    $('#color').colorpicker(); // Colopicker
    var url = get_url();
    var arr = url.split('/');


    $.ajax({
        url: base_url + 'admin/getCous',
        data: {id: arr[arr.length - 1]},
        type: 'post',
        success: function (data) {

            var res = data.split('-')
            $('#min').val(res[0])
            $('#max').val(res[1])
            $('#dur').val(res[2])
            $('#calendar').fullCalendar({

                header: {
                    left: 'prev, next, today',
                    center: 'title',
                    right: 'month, basicWeek, basicDay'
                },

                // Get all events stored in database
                minTime: res[0],
                maxTime: res[1],
                defaultView: 'agendaWeek',
                eventLimit: true, // allow "more" link when too many events
                events: base_url + 'calendar/getEvents?id=' + $('#user_id').val(),
                selectable: true,
                selectHelper: true,
                editable: true, // Make the event resizable true  
                slotDuration: '00:15:00',
                nowIndicator: true,
                displayEventEnd: true,
                select: function (start, end) {

                    $('#start').val(moment(start).format('YYYY-MM-DD HH:mm:ss'));
                    $('#end').val(moment(end).format('YYYY-MM-DD HH:mm:ss'));



                    if (moment().diff(start, 'minutes') > 1) {
                        $('#calendar').fullCalendar('unselect');
                        return false;
                    }



                    if ($('#role_hidden').val() == 3) {
                        $('#calendar').fullCalendar('unselect');
                        return false;
                    }
                    // Open modal to add event
                    modal({
                        // Available buttons when adding
                        buttons: {
                            add: {
                                id: 'add-event', // Buttons id
                                css: 'btn-success', // Buttons class
                                label: 'Add' // Buttons label
                            }
                        },
                        title: 'Add Event', // Modal title
                        refresh: true
                    });
                },
                eventAfterRender: function (event, $el, view) {
                    $el.removeClass('fc-short');
                },
                eventDrop: function (event, delta, revertFunc, start, end) {

                    start = event.start.format('YYYY-MM-DD HH:mm:ss');
                    if (event.end) {
                        end = event.end.format('YYYY-MM-DD HH:mm:ss');
                    } else {
                        end = start;
                    }

                    $.post(base_url + 'calendar/dragUpdateEvent', {
                        id: event.id,
                        start: start,
                        end: end
                    }, function (result) {
                        $('.alert').addClass('alert-success').text('Event updated successfuly');
                        hide_notify();


                    });



                },
                eventResize: function (event, dayDelta, minuteDelta, revertFunc) {

                    start = event.start.format('YYYY-MM-DD HH:mm:ss');
                    if (event.end) {
                        end = event.end.format('YYYY-MM-DD HH:mm:ss');
                    } else {
                        end = start;
                    }

                    $.post(base_url + 'calendar/dragUpdateEvent', {
                        id: event.id,
                        start: start,
                        end: end
                    }, function (result) {
                        $('.alert').addClass('alert-success').text('Event updated successfuly');
                        hide_notify();

                    });
                },
                eventRender: function (event, element) {
                    element.attr('title', event.description);

                    if ($('#role_hidden').val() == 3) {
                        var text = event.title;
                        if ($.isNumeric(text) || text.indexOf("@") != -1) {
                            element.text("Anonymous User")
                        } else {
                            element.text(event.title)
                        }
                    }

                },
                viewRender: function (view) {
                    minTime: getMintime();
                    maxTime:   getMaxtime();
                },
                // Event Mouseover
                eventMouseover: function (calEvent, jsEvent, view) {

                    var tooltip = '<div class="event-tooltip">' + calEvent.description + '</div>';
                    $("body").append(tooltip);

                    $(this).mouseover(function (e) {
                        $(this).css('z-index', 10000);
                        $('.event-tooltip').fadeIn('500');
                        $('.event-tooltip').fadeTo('10', 1.9);
                    }).mousemove(function (e) {
                        $('.event-tooltip').css('top', e.pageY + 10);
                        $('.event-tooltip').css('left', e.pageX + 20);
                    });
                },
                eventMouseout: function (calEvent, jsEvent) {
                    $(this).css('z-index', 8);
                    $('.event-tooltip').remove();
                },
                // Handle Existing Event Click
                eventClick: function (calEvent, jsEvent, view) {
                    // Set currentEvent variable according to the event clicked in the calendar
                    currentEvent = calEvent;
                    // Open modal to edit or delete event
                    var url = get_url();
                    var arr = url.split('/');

                    if (arr[arr.length - 1] == "index#_=_" || arr[arr.length - 1] == "index") {
                        modal({
                            // Available buttons when editing
                            buttons: {

                            },
                            title: 'Edit Event "' + calEvent.title + '"',
                            event: calEvent
                        });
                    } else {
                        modal({
                            // Available buttons when editing
                            buttons: {
                                delete: {
                                    id: 'delete-event',
                                    css: 'btn-danger',
                                    label: 'Delete'
                                },
                                update: {
                                    id: 'update-event',
                                    css: 'btn-success',
                                    label: 'Update'
                                },
                                reschedule: {
                                    id: 'reschedule-event',
                                    css: 'btn-primary',
                                    label: 'Reschedule'
                                }

                            },
                            title: 'Edit Event "' + calEvent.title + '"',
                            event: calEvent
                        });
                    }
                }

            });

        }
    });



    // Fullcalendar

    function GetDateFormat(date) {
        var month = (date.getMonth() + 1).toString();
        month = month.length > 1 ? month : '0' + month;
        var day = date.getDate().toString();
        day = day.length > 1 ? day : '0' + day;
        return date.getFullYear() + '-' + month + '-' + day;
    }
    // Prepares the modal window according to data passed
    function modal(data) {
        // update_time_ends($('#dur').val())

        var url = get_url();
        // console.log(formatAMPM($('#start').val()))
        var arr = url.split('/');
        if (arr[arr.length - 1] == "counselorSchedule") {
            var schedule_id = data.event.id;
            var visitor_info = data.event.uid;
            var dateObj = new Date(data.event ? data.event.start : '');
            var date = GetDateFormat(dateObj);
            var start = date;
            var sche_id = data.event.schedule_id;

            sheduleDetials(schedule_id, visitor_info, start, sche_id);

        } else {
            // Set modal title
            $('.modal-title').html(data.title);
            // Clear buttons except Cancel
            $('.modal-footer button:not(".btn-default")').remove();
            // Set input values
            var time = formatAMPM($('#start').val());
            var range = formatAMPM(new Date(new Date($('#start').val()).getTime() + 15 * 60000))

            var maxTime = $('#max').val()
            $('#startTime').timepicker({
                'minTime': time,
                'maxTime': maxTime,
                'step': 15,
                'timeFormat': 'h:i A',

            });
            $('#endTime').timepicker({
                'minTime': time,
                'showDuration': true,
                'maxTime': maxTime,
                'step': 15,
                'timeFormat': 'h:i A',
                disableTimeRanges: [[time, range]]
            });
            
            $('#endTime').prop('disabled', true)
            $('#startTime').val(formatAMPM($('#start').val()));
//            $('#time_slot_start').val(formatAMPM($('#start').val()));
//            $('#time_slot_dur').val($('#dur').val());
//            $('#time_slot_ends').val(formatAMPM(new Date(new Date($('#start').val()).getTime() + $('#time_slot_dur').val() * 60000)));
            var dateObj = new Date(data.event ? data.event.start : '');
            var date = GetDateFormat(dateObj);
            var uid = data.event ? data.event.uid : '';
            if ($.isNumeric(data.event ? data.event.uid : '') || uid == "") {
                $('#div_username').html("");
                $('#div_username').html('<select data-options="{&quot;allowClear&quot;:false,&quot;width&quot;:&quot;none&quot;,&quot;placeholder&quot;:&quot;&quot;,&quot;minimumResultsForSearch&quot;:-1}" required id="user" class="form-control iwjmb-select_advanced" name="gender"></select>');
                $('#user').append($('#user_listing').html());
                $('#user').val(data.event ? data.event.uid : '').trigger('change');
            } else {
                var title = data.event ? data.event.title : '';
                $('#div_username').html("");
                //  $('#div_username').append('<input type="text" class="form-control" name="" value="'+ title +'" disabled>');
                $('#div_username').append('<input type="text" class="form-control" name="" value="Anonymous User" disabled>');
            }


            $('#schedule_table_id').val(data.event ? data.event.id : '');

            if (typeof data.event != "undefined") {
                $('#startTime').val(formatAMPM(data.event ? data.event.start._i : $('#start').val()));
                $('#startTime').attr('disabled', true)
                $('#endTime').attr('disabled', true)
                $('#endTime').val(formatAMPM(data.event ? data.event.end._i : ''));
            } else {

                $('#endTime').val('')
            }
            $('#date').val(data.event ? date : '');

            $('#description').val(data.event ? data.event.description : '');

            $('#schedule_id').val(data.event ? data.event.schedule_id : '').trigger('change');


            // Create Butttons
            $.each(data.buttons, function (index, button) {
                $('.modal-footer').prepend('<button type="button" id="' + button.id + '" class="btn ' + button.css + '">' + button.label + '</button>')
            })
            //Show Modal
            $('.add_event_popup').modal('show');
        }
    }

    // Handle Click on Add Button
    $('.modal').on('click', '#add-event', function (e) {
        
        var start = moment($('#start').val()).format('YYYY-MM-DD')
        var time = getTime(start, $('#startTime').val());
        var time1 = getTime(start, $('#endTime').val());
          
        if (validator(['user', 'description'])) {
            $.post(
                    base_url + 'calendar/addEvent',
                    {
                        user_id: $('#user').val(),
                        description: $('#description').val(),
                        start: time,
                        end: time1,
                        con_id: $('#user_id').val(),
                        sche_phone: "",
                        schedule_id: $('#schedule_id').val(),
                    }, function (result) {
                if (result == 2) {
                    alert('User account balance is low to schedule this appointment')
                } else {
                    $('.modal').modal('hide');
                    $('#calendar').fullCalendar("refetchEvents");
                    hide_notify();
                }
            });
        }
    });


    // Handle click on Update Button
    $('.modal').on('click', '#update-event', function (e) {
        if (validator(['user', 'description'])) {
            $.post(base_url + 'calendar/updateEvent', {
                id: currentEvent.id,
                user_id: $('#user').val(),
                description: $('#description').val(),
                start: $('#start').val(),
                end: $('#end').val(),
                schedule_id: $('#schedule_id').val(),
            }, function (result) {
                $('.alert').addClass('alert-success').text('Event updated successfuly');
                $('.modal').modal('hide');
                $('#calendar').fullCalendar("refetchEvents");
                hide_notify();

            });
        }
    });

    // Handle click on Reschedule Button
    $('.modal').on('click', '#reschedule-event', function (e) {
        
        $.post(base_url + 'calendar/checkScheduleValid', {
            id: currentEvent.id,
            user_id: $('#user').val(),
            start: $('#date').val(),
            end: $('#end').val(),
            schedule_id: $('#schedule_id').val(),
        }, function (result) {


            var range = formatAMPM(new Date(new Date($('#start').val()).getTime() + 15 * 60000))
            var minTime = $('#min').val()
            var maxTime = $('#max').val()
            $('#startTime_re').timepicker({
                'minTime': minTime,
                'maxTime': maxTime,
                'step': 15,
                'timeFormat': 'H:i A',

            });
            $('#endTime_re').timepicker({
                'minTime': minTime,
                'showDuration': true,
                'maxTime': maxTime,
                'step': 15,
                'timeFormat': 'H:i A',

            });
            $('#reschedule_div').show();
            $("#reschedule-event").attr("id", "save-event").text('Save Details');

        });
    });

    // Handle click on Reschedule save Button
    $('.modal').on('click', '#save-event', function (e) {


        var start = $('#schedule_date').val()
        var time = getTime(start, $('#startTime_re').val());
        var time1 = getTime(start, $('#endTime_re').val());

        $('#start').val(arr[0] + " " + time)


        $.post(base_url + 'calendar/rescheduleEvent', {
            id: currentEvent.id,
            con_id: $('#user_id').val(),
            user_id: $('#user').val(),
            reason: $('#reason').val(),
            date: $('#schedule_date').val(),
            cur_date: $('#date').val(),
            start: time,
            end: time1,
            schedule_id: '',
            schedule_table_id: $('#schedule_table_id').val(),
            cur_schedule_id: $('#schedule_id').val(),
        }, function (result) {

            $('.modal').modal('hide');
            $('#calendar').fullCalendar("refetchEvents");
            hide_notify();

        });

    });

    // Handle Click on Delete Button
    $('.modal').on('click', '#delete-event', function (e) {
        $.get(base_url + 'calendar/deleteEvent?id=' + currentEvent.id + '&date=' + $('#date').val(), function (result) {
            $('.alert').addClass('alert-success').text('Event deleted successfully !');
            $('.modal').modal('hide');
            $('#calendar').fullCalendar("refetchEvents");
            hide_notify();
        });
    });

    function hide_notify()
    {
        setTimeout(function () {
            $('.alert').removeClass('alert-success').text('');
        }, 2000);
    }


    // Dead Basic Validation For Inputs
    function validator(elements) {
        var errors = 0;
        $.each(elements, function (index, element) {
            if ($.trim($('#' + element).val()) == '')
                errors++;
        });
        if (errors) {
            $('.error').html('Please insert title and description');
            return false;
        }
        return true;
    }
});*/