(function($) {

    var iwj_filter_common;

    var tax_ul_order = ['ul.iwjob-list-categories', 'ul.iwjob-list-skills', 'ul.iwjob-list-levels', 'ul.iwjob-list-types', 'ul.iwjob-list-locations', 'ul.iwjob-list-salaries'];

    iwj_filter_common = {
        add_class_has_item : function() {
            for (var i = 0; i < tax_ul_order.length; i++) {

                var ulSelector = tax_ul_order[i];
                var liSelector = ulSelector + " li.iwj-input-checkbox";

                $(liSelector).each(function(index, value){
                    if ($(this).data('order') > 0) {
                        $(this).removeClass('empty');
                        $(this).addClass('has-item');
                    } else{
                        $(this).removeClass('has-item');
                        $(this).addClass('empty');
                    }
                });
            }
        },
        check_filter_job_checked : function() {
            $('input.iwjob-filter-jobs-cbx').each(function() {
                if ( $(this).prop("checked") ) {
                    $(this).closest('li').addClass('checked');
                } else {
                    $(this).closest('li').removeClass('checked');
                }
            });
            $('input.iwjob-filter-candidates-cbx').each(function() {
                if ( $(this).prop("checked") ) {
                    $(this).closest('li').addClass('checked');
                } else {
                    $(this).closest('li').removeClass('checked');
                }
            });
            $('input.iwjob-filter-employers-cbx').each(function() {
                if ( $(this).prop("checked") ) {
                    $(this).closest('li').addClass('checked');
                } else {
                    $(this).closest('li').removeClass('checked');
                }
            });
        },
        sort_tax_after_ajax : function() {
            iwj_filter_common.check_filter_job_checked();

            for (var i = 0; i < tax_ul_order.length; i++) {

                if ($(tax_ul_order[i]).length) {

                    var ulSelector = tax_ul_order[i];
                    var liSelector = ulSelector + " > li.iwj-input-checkbox";

                    var list_li_selector = $(liSelector);

                    list_li_selector.sort(function(a, b){
                            if ($(a).hasClass('checked') && $(b).hasClass('checked')) {
                                var res = $(b).data("order")-$(a).data("order");
                                return res;
                            } else if ($(a).hasClass('checked')) {
                                return -1;
                            } else if ($(b).hasClass('checked')) {
                                return 1;
                            } else {
                                var res = $(b).data("order")-$(a).data("order");
                                return res;
                            }
                        }
                    );

                    var show_more = $(ulSelector + ' li.show-more');
                    var show_less = $(ulSelector + ' li.show-less');

                    $( ulSelector ).html(list_li_selector);

                    // back append show less and show more to ul tag
                    $( ulSelector ).append( show_more );
                    $( ulSelector ).append( show_less );

                }

            } // end for
        },
        display_filter_box : function() {
            if($('li.iwj-filter-selected-item').length) {
                $('#iwj-filter-selected').show();
                $('#iwj-clear-filter-btn').show();
            } else {
                $('#iwj-filter-selected').hide();
                $('#iwj-clear-filter-btn').hide();
            }
        }
    };

    window.iwj_filter_common = iwj_filter_common;

})(jQuery);

jQuery(document).ready(function($) {

    function iwj_init_filter() {
        iwj_filter_common.add_class_has_item();
        iwj_filter_common.check_filter_job_checked();
    }

    iwj_init_filter();

    iwj_filter_common.display_filter_box();

});