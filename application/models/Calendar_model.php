<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class Calendar_model extends CI_Model {
    /* Read the data from DB */

   Public function getEvents() {

        if ($_GET['id'] != "undefined") {

            $sql = "SELECT eys.id as id,eys.time_starts as start, eys.time_ends as end,ey.username as title,eys.visitor_info as uid,eys.note as description,eys.date as date,eys.id as schedule_id,eys.is_modified "
                    . "FROM ey_schedule eys join ey_user ey on eys.visitor_info=ey.id"
                    . " WHERE eys.counselor_id= ? AND eys.is_modified=0 AND eys.date BETWEEN ? AND ? ORDER BY eys.date ASC";
            $result = $this->db->query($sql, array($_GET['id'], $_GET['start'], $_GET['end']))->result();


            $sql = "SELECT eys.id as id,eys.time_starts as start, eys.time_ends as end,ey.username as title,eys.visitor_info as uid,eys.reason_to_reschedule as description,eys.date as date,eys.schedule_id as schedule_id "
                    . "FROM ey_reschedule eys join ey_user ey on eys.visitor_info=ey.id"
                    . " WHERE eys.counselor_id= ? AND eys.is_modified=0 AND eys.date BETWEEN ? AND ? ORDER BY eys.date ASC";
            $result1 = $this->db->query($sql, array($_GET['id'], $_GET['start'], $_GET['end']))->result();



            $result1_sche[] = "";
            $result_sche[] = "";
            $sql = "SELECT eys.id as id,'Anonymous User' as title,eys.time_starts as start, eys.time_ends as end,eys.visitor_info as uid,eys.note as description,eys.date as date,eys.id as schedule_id,eys.is_modified "
                    . "FROM ey_schedule eys join ey_coupon_purchased ey on eys.visitor_info=ey.coupon_code"
                    . " WHERE eys.counselor_id= ? AND eys.is_modified=0 AND eys.date BETWEEN ? AND ? AND CHAR_LENGTH(ey.visiter_info)>5  ORDER BY eys.date ASC";
            $result_sche = $this->db->query($sql, array($_GET['id'], $_GET['start'], $_GET['end']))->result();


            $sql = "SELECT eys.id as id,'Anonymous User' as title,eys.time_starts as start, eys.time_ends as end,eys.visitor_info as uid,eys.reason_to_reschedule as description,eys.date as date,eys.schedule_id as schedule_id "
                    . "FROM ey_reschedule eys join ey_coupon_purchased ey on eys.visitor_info=ey.coupon_code"
                    . " WHERE eys.counselor_id= ? AND eys.is_modified=0 AND eys.date BETWEEN ? AND ? AND CHAR_LENGTH(ey.visiter_info)>5 ORDER BY eys.date ASC";
            $result1_sche = $this->db->query($sql, array($_GET['id'], $_GET['start'], $_GET['end']))->result();




            return array_merge($result1, $result, $result_sche, $result1_sche);
        } else {
            $sql = "SELECT eys.id as id,eys.time_starts as start, eys.time_ends as end,ey.username as title,eys.visitor_info as uid,eys.note as description,eys.date as date,eys.schedule_id as schedule_id,eys.is_modified "
                    . "FROM ey_schedule eys join ey_user ey on eys.visitor_info=ey.id"
                    . " WHERE eys.is_modified=0 AND eys.date BETWEEN ? AND ?   ORDER BY eys.date ASC";
            $result = $this->db->query($sql, array($_GET['start'], $_GET['end']))->result();


            $sql = "SELECT eys.id as id,eys.time_starts as start, eys.time_ends as end,ey.username as title,eys.visitor_info as uid,eys.reason_to_reschedule as description,eys.date as date,eys.schedule_id as schedule_id "
                    . "FROM ey_reschedule eys join ey_user ey on eys.visitor_info=ey.id"
                    . " WHERE eys.is_modified=0 AND eys.date BETWEEN ? AND ? ORDER BY eys.date ASC";
            $result1 = $this->db->query($sql, array($_GET['start'], $_GET['end']))->result();



            $result1_sche[] = "";
            $result_sche[] = "";
            $sql = "SELECT eys.id as id,'Anonymous User' as title,eys.time_starts as start, eys.time_ends as end,eys.visitor_info as uid,eys.note as description,eys.date as date,eys.schedule_id as schedule_id,eys.is_modified "
                    . "FROM ey_schedule eys join ey_coupon_purchased ey on eys.visitor_info=ey.coupon_code"
                    . " WHERE eys.is_modified=0 AND eys.date BETWEEN ? AND ? AND CHAR_LENGTH(ey.visiter_info)>5 ORDER BY eys.date ASC";
            $result_sche = $this->db->query($sql, array($_GET['start'], $_GET['end']))->result();


            $sql = "SELECT eys.id as id,'Anonymous User' as title,eys.time_starts as start, eys.time_ends as end,eys.visitor_info as uid,eys.reason_to_reschedule as description,eys.date as date,eys.schedule_id as schedule_id "
                    . "FROM ey_reschedule eys join ey_coupon_purchased ey on eys.visitor_info=ey.coupon_code"
                    . " WHERE eys.is_modified=0 AND eys.date BETWEEN ? AND ? AND CHAR_LENGTH(ey.visiter_info)>5 ORDER BY eys.date ASC";
            $result1_sche = $this->db->query($sql, array($_GET['start'], $_GET['end']))->result();




            return array_merge($result1, $result, $result_sche, $result1_sche);
        }
    }

    /* Get Counsiler details */

    Public function getcounsildetail($cdetail) {

        $sql = "SELECT * from ey_user WHERE id=?";



        $result = $this->db->query($sql, array($cdetail))->result();

        return $result;
    }

    /* Get Scheduler details */

    Public function getscheduledetail($sdetail) {

        //echo   $detail;exit;

        $sql = "SELECT * from ey_counsellor_schedule WHERE id=?";


        $result = $this->db->query($sql, array($sdetail))->result();

        return $result;
    }

    /* Get Visitor details */

    Public function getvisitordetail($vdetail) {

//echo   $detail;exit;

        if (filter_var($vdetail, FILTER_VALIDATE_EMAIL)) {
            $sql = "SELECT * from ey_coupon_purchased WHERE visiter_info=?";
        } else {
            $sql = "SELECT * from ey_coupon_purchased WHERE coupon_code=?";
        }



        $result = $this->db->query($sql, array($vdetail))->result();

        return $result;
    }
    
    Public function getvisitordetailUser($vdetail) {

//echo   $detail;exit;

        
            $sql = "SELECT * from ey_user WHERE id=?";
        



        $result = $this->db->query($sql, array($vdetail))->result();

        return $result;
    }

    public function getamt($id) {
        $this->db->select('amount');
        $this->db->from('ey_counselor_renumeration');
        $this->db->where('user_id', $id);
        $query = $this->db->get();
        $data = $query->result_array();

        return($data[0]['amount']);
    }

    public function getcoupon($visitor) {
        $this->db->select('coupon_balance');
        $this->db->from('ey_coupon_balance');
        $this->db->where('visitor_info', $visitor);
        $this->db->where('payment_status', "Captured");
        $query = $this->db->get();
        $data = $query->result_array();
        if (!empty($data)) {
            return($data[0]['coupon_balance']);
        } else {
            echo 0;
        }
    }

    /* Create new events */

   Public function addEvent() {
        
        $starttime = date("H:i", strtotime($this->input->post('start')));
        $endtime = date("H:i", strtotime($this->input->post('end')));

        $start_time = strtotime($starttime); //change to strtotime
        $end_time = strtotime($endtime);
        $old_date_timestamp = strtotime($_POST['end']);
        $start_date = date('Y-m-d H:i:s', strtotime($_POST['start']));
        $new_date = date('Y-m-d H:i:s', $old_date_timestamp);
        $sql = "INSERT INTO ey_schedule (counselor_id,visitor_info,date,time_starts,time_ends,schedule_id,note, status, video_schedule,is_modified,created_at) VALUES (?,?,?,?,?,?,?,?,?,?,?)";
        $this->db->query($sql, array($_POST['con_id'], $_POST['user_id'], $start_date, $start_date, $new_date, $_POST['schedule_id'], $_POST['description'], 'active', $_POST['videocall'], 0, date("Y-m-d H:i:s")));
        $insert_id = $this->db->insert_id();


        $renum = $this->getamt($_POST['con_id']);
        $code = $this->getcoupon($_POST['user_id']);
        $balance = $code - $renum;
        //if($insert_id){
        $sql = "UPDATE ey_coupon_balance SET coupon_balance = ? WHERE visitor_info = ?";
        $this->db->query($sql, array($balance, $_POST['user_id']));
        //}
        if (!is_numeric($_POST['user_id'])) {

            $sql = "INSERT INTO ey_coupon_details (sche_id,visitor_info,phone) VALUES (?,?,?)";
            $this->db->query($sql, array($insert_id, $_POST['user_id'], $_POST['sche_phone']));
        }

        return ($this->db->affected_rows() != 1) ? false : true;
    }
    
    public function payEvent() {
        
        $starttime = date("H:i", strtotime($this->session->userdata('start')));
        $endtime = date("H:i", strtotime($this->session->userdata('end')));
        
        $start_time = strtotime($starttime); //change to strtotime
        $end_time = strtotime($endtime);
        $old_date_timestamp = strtotime($this->session->userdata('end'));
        $start_date = date('Y-m-d H:i:s', strtotime($this->session->userdata('start')));
        $new_date = date('Y-m-d H:i:s', $old_date_timestamp);
        $sql = "INSERT INTO ey_schedule (counselor_id,visitor_info,date,time_starts,time_ends,schedule_id,note, status,video_schedule,is_modified,created_at) VALUES (?,?,?,?,?,?,?,?,?,?,?)";
        $this->db->query($sql, array($this->session->userdata('counselor_id'), $this->session->userdata('visitor_info'), $start_date,$start_date,$new_date, $this->session->userdata('schedule_id'), $this->session->userdata('description'), 'active', $this->session->userdata('videocall_enable'), 0, date("Y-m-d H:i:s")));
        $insert_id = $this->db->insert_id();

        
        $renum = $this->getamt($this->session->userdata('counselor_id'));
        $code = $this->getcoupon($this->session->userdata('visitor_info'));
        $balance = $code - $renum;

        $sql = "UPDATE ey_coupon_balance SET coupon_balance = ? WHERE visitor_info = ?";
        $this->db->query($sql, array($balance, $this->session->userdata('visitor_info')));

        if (!is_numeric($this->session->userdata('visitor_info'))) {
            
            $sql = "INSERT INTO ey_coupon_details (sche_id,visitor_info,phone) VALUES (?,?,?)";
            $this->db->query($sql, array($insert_id, $this->session->userdata('visitor_info'), $this->session->userdata('phone')));
        }

        return ($this->db->affected_rows() != 1) ? false : true;
        
    }
    
    Public function addEvent1() {
        $starttime = date("H:i", strtotime($this->input->post('start')));
        $endtime = date("H:i", strtotime($this->input->post('end')));
        
        $start_time = strtotime($starttime); //change to strtotime
        $end_time = strtotime($endtime);
        $old_date_timestamp = strtotime($_POST['end']);
        $start_date = date('Y-m-d H:i:s', strtotime($_POST['start']));
        $new_date = date('Y-m-d H:i:s', $old_date_timestamp);
        $sql = "INSERT INTO ey_schedule (counselor_id,visitor_info,date,time_starts,time_ends,schedule_id,note,status,video_schedule,is_modified,created_at) VALUES (?,?,?,?,?,?,?,?,?,?,?)";
        $this->db->query($sql, array($_POST['con_id'], $_POST['user_id'], $start_date,$start_date,$new_date, $_POST['schedule_id'], $_POST['description'], 'active',1, 0, date("Y-m-d H:i:s")));
        $insert_id = $this->db->insert_id();

        
        $renum = $this->getamt($_POST['con_id']);
        $code = $this->getcoupon($_POST['user_id']);
        $balance = $code - $renum;

        $sql = "UPDATE ey_coupon_balance SET coupon_balance = ? WHERE visitor_info = ?";
        $this->db->query($sql, array($balance, $_POST['user_id']));

        if (!is_numeric($_POST['user_id'])) {
            
            $sql = "INSERT INTO ey_coupon_details (sche_id,visitor_info,phone) VALUES (?,?,?)";
            $this->db->query($sql, array($insert_id, $_POST['user_id'], $_POST['sche_phone']));
        }

        return ($this->db->affected_rows() != 1) ? false : true;
    }

    /* Update  event */

     Public function updateEvent() {

        //echo $sql = "UPDATE ey_schedule SET visitor_info = '". $_POST['user_id'] ."', note = '". $_POST['description'] ."', schedule_id = '". $_POST['schedule_id'] ."' WHERE id = '". $_POST['id'] ."'";exit;
        $sql = "UPDATE ey_schedule SET visitor_info = ?, note = ?, schedule_id = ? WHERE id = ?";
        $this->db->query($sql, array($_POST['user_id'], $_POST['description'], $_POST['schedule_id'], $_POST['id']));
        if($this->db->affected_rows() == 0) {
            //return ($this->db->affected_rows() != 1) ? false : true;
            //echo $sql = "UPDATE ey_reschedule SET visitor_info = '". $_POST['user_id'] ."', reason_to_reschedule = '". $_POST['description'] ."', schedule_id = '". $_POST['schedule_id'] ."' WHERE id = '". $_POST['id'] ."'";exit;
            $sql = "UPDATE ey_reschedule SET visitor_info = ?, reason_to_reschedule = ?, schedule_id = ? WHERE id = ?";
            $this->db->query($sql, array($_POST['user_id'], $_POST['description'], $_POST['schedule_id'], $_POST['id']));
            return ($this->db->affected_rows() != 1) ? false : true;
        }
        else{
            return ($this->db->affected_rows() != 1) ? false : true;
        }
    }

    /* Update  event */

    Public function rescheduleEvent() {
        $row = $this->checkIsModified($_POST['schedule_table_id'], $_POST['cur_date']);

        $sql = "INSERT INTO ey_reschedule (counselor_id,visitor_info,current_schedule_id,date,time_starts,time_ends,schedule_id,reason_to_reschedule, status,created_at) VALUES (?,?,?,?,?,?,?,?,?,?)";
        $this->db->query($sql, array($_POST['con_id'], $_POST['user_id'], $_POST['cur_schedule_id'], $_POST['date'],$_POST['start'],$_POST['end'], $_POST['schedule_id'], $_POST['reason'], 'active', date("Y-m-d H:i:s")));
        return ($this->db->affected_rows() != 1) ? false : true;
    }

    /* Delete event */

    Public function deleteEvent() {

        $row = $this->checkIsModified1($_GET['id'],$_GET['date']);
        
        if (!empty($row)) {
            if ($row->is_modified == 1) {
                $sql = "DELETE FROM ey_reschedule WHERE id = " . $row->id . " and date='".$_GET['date']."'";
                $this->db->query($sql, array($_GET['id']));
            } else {
                $sql = "DELETE FROM ey_schedule WHERE id = " . $_GET['id']. " and date='".$_GET['date']."'";
                $this->db->query($sql, array($_GET['id']));
            }

            $renum = $this->getamt($row->counselor_id);
            $code = $this->getcoupon($row->visitor_info);
            $balance = $code + $renum;
            
            $row1= $this->getCallDetails($_GET['id']);
            if(empty($row1)){
            $sql = "UPDATE ey_coupon_balance SET coupon_balance = ? WHERE visiter_info = ? ";
            $this->db->query($sql, array($balance,$row->visitor_info));
            }
            
            
        }



        return ($this->db->affected_rows() != 1) ? false : true;
    }
    public function getCallDetails($id) {
        $this->db->select('*');
        $this->db->from('ey_call_details');
        $this->db->where('schedule_id', $id);
        $query = $this->db->get();
        $data = $query->result();

        return $data;
    }
    /* Update  event */

    Public function dragUpdateEvent() {
        //$date=date('Y-m-d h:i:s',strtotime($_POST['date']));

        $sql = "UPDATE ey_schedule SET  ey_schedule.date = ?,ey_schedule.time_starts=?,ey_schedule.time_ends=? WHERE id = ?";
        $this->db->query($sql, array($_POST['start'],$_POST['start'],$_POST['end'], $_POST['id']));
        return ($this->db->affected_rows() != 1) ? false : true;
    }

    /* Check schedule time  */

    Public function checkSchedule() {

        $sql = "select * from ey_schedule WHERE visitor_info= ? AND date= ?  AND schedule_id= ? ";
        $res = $this->db->query($sql, array($_POST['user_id'], $_POST['start'], $_POST['schedule_id']))->row();

        if (empty($res)) {
            $sql = "select * from ey_reschedule WHERE visitor_info= ? AND date= ?  AND schedule_id= ? ";
            $res = $this->db->query($sql, array($_POST['user_id'], $_POST['start'], $_POST['schedule_id']))->row();
        }

        return $res;
    }

    Public function checkIsModified($cdetail, $date) {

        $sql = "SELECT * from ey_schedule WHERE id=? and date=?";
        $result = $this->db->query($sql, array($cdetail, $date))->row();
        if (empty($result)) {
            $sql = "SELECT * from ey_reschedule WHERE id=? and date=?";
            $result = $this->db->query($sql, array($cdetail, $date))->row();
            if (!empty($result)) {
                $sql = "UPDATE ey_reschedule SET is_modified = 1 WHERE id = ? and date=?";
                $this->db->query($sql, array($cdetail, $date));
            }
        } else {
            $sql = "UPDATE ey_schedule SET is_modified = 1 WHERE id = ? and date=?";
            $this->db->query($sql, array($cdetail, $date));
        }

        return $result;
    }

    Public function checkIsModified1($cdetail,$date) {

        $sql = "SELECT * from ey_schedule WHERE id=? and date=?";
        $res = $this->db->query($sql, array($cdetail,$date))->row();

        if (empty($res)) {
            $sql = "SELECT * from ey_reschedule WHERE id=? and date=?";
            $res = $this->db->query($sql, array($cdetail,$date))->row();
            
            $sql = "DELETE FROM ey_reschedule WHERE id = " . $cdetail . " and date='".$date."'";
                $this->db->query($sql, array($_GET['id']));
        }
        return $res;
    }
    public function checkTimeEnds($end,$start) {

        $sql = "SELECT * from ey_schedule WHERE time_starts between '".date('Y-m-d H:i:s', strtotime($start."+1 minutes"))."'  and '".date('Y-m-d H:i:s', strtotime($end."-1 minutes"))."'";
        $res = $this->db->query($sql)->row();
        return $res;
    }
}
