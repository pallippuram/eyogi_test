<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class User_model extends CI_Model {

    public function __construct() {
        parent::__construct();
        $this->tableName = 'users';
        $this->primaryKey = 'id';
        date_default_timezone_set('Asia/Kolkata');
    }

    public function insertUser($data) {
        $this->db->insert('ey_user', $data);
        return true;
    }

    public function check_login($email, $password) {
        $this->db->select('*');
        $this->db->from('ey_user');
        $this->db->where('email', $email);
        $this->db->where('password', MD5($password));

        $query = $this->db->get();
        if ($query->num_rows() == 1) {
            return $query->row();
        } else {
            return false;
        }
    }

    public function check_login1($email, $password) {
        $this->db->select('*');
        $this->db->from('ey_user');
        $this->db->where('email', $email);
        $query = $this->db->get();

        return $query->num_rows();
    }

    public function insert_user($data) {
        $this->db->insert('users', $data);
        return TRUE;
    }

    public function insertCounselor($data, $data1) {
        $this->db->insert('ey_user', $data);

        $insert_id = $this->db->insert_id();
        $data1['user_id'] = $insert_id;

        $this->db->insert('ey_counsellor_expertise', $data1);
        return TRUE;
    }

    public function insert_enquiry($data) {
        $this->db->insert('ey_enquiry', $data);
        return TRUE;
    }

    public function insertAdmin($data, $data1) {
        $this->db->insert('ey_user', $data);

        $insert_id = $this->db->insert_id();
        $data1['user_id'] = $insert_id;

        $this->db->insert('ey_counsellor_expertise', $data1);
        return TRUE;
    }

    public function fetch_user($id) {
        $query = $this->db->query("SELECT *
                                 FROM ey_user
                                 WHERE id = '" . $id . "' ");
        //return $query->result(); 
        return $query->result();
    }

    public function fetch_user_data($id) {
        $query = $this->db->query("SELECT *
                                 FROM ey_user
                                 WHERE oauth_uid = '" . $id . "' ");
        //return $query->result(); 
        return $query->result();
    }

    public function update_entry() {
        $this->title = $_POST['title'];
        $this->content = $_POST['content'];
        $this->date = time();

        $this->db->update('entries', $this, array('id' => $_POST['id']));
    }

    public function checkUser($data = array()) {
        $this->db->select('user.id');
        $this->db->from('users user');
        $this->db->where(array('user.oauth_provider' => $data['oauth_provider'], 'user.oauth_uid' => $data['oauth_uid']));
        $query = $this->db->get();
        $check = $query->num_rows();

        if ($check > 0) {
            $result = $query->row_array();
            $data['modified'] = date("Y-m-d H:i:00");
            $update = $this->db->update($this->tableName, $data, array('id' => $result['id']));

            $page = $this->session->userdata('login');

            if ($page == 1) {
                $userID = 2;
            } else {
                $userID = 0;
            }
        } else {
            $data['created'] = date("Y-m-d H:i:00");
            $data['modified'] = date("Y-m-d H:i:00");
            $insert = $this->db->insert($this->tableName, $data);
            $userID = 1;
            $page = $this->session->userdata('page');

            if ($page == "joinus") {
                $insert_data = array(
                    'oauth_uid' => $data['oauth_uid'],
                    'username' => $data['first_name'],
                    'email' => $data['email'],
                    'photo' => $data['picture_url'],
                    'role_id' => 3,
                    'created_at' => date("Y-m-d H:i:00")
                );

                $insert = $this->db->insert('ey_user', $insert_data);
            } else {
                $insert_data = array(
                    'oauth_uid' => $data['oauth_uid'],
                    'username' => $data['first_name'],
                    'email' => $data['email'],
                    'photo' => $data['picture_url'],
                    'role_id' => 4,
                    'created_at' => date("Y-m-d H:i:00")
                );
                $insert = $this->db->insert('ey_user', $insert_data);
            }
        }

        return $userID ? $userID : false;
    }

    public function saveUser() {
        $data['created'] = date("Y-m-d H:i:00");
        $data['modified'] = date("Y-m-d H:i:00");
        $insert = $this->db->insert($this->tableName, $data);
        $userID = 1;
        $page = $this->session->userdata('page');

        if ($page == "reg") {
            $insert_data = array(
                'oauth_uid' => $data['oauth_uid'],
                'username' => $data['first_name'],
                'email' => $data['email'],
                'role_id' => 4,
                'created_at' => date("Y-m-d H:i:00")
            );
            $insert = $this->db->insert('ey_user', $insert_data);
        } elseif ($page == "joinus") {
            $insert_data = array(
                'oauth_uid' => $data['oauth_uid'],
                'username' => $data['first_name'],
                'email' => $data['email'],
                'role_id' => 3,
                'created_at' => date("Y-m-d H:i:00")
            );

            $insert = $this->db->insert('ey_user', $insert_data);
        }

        return true;
    }

    public function fetchUser() {
        $this->db->select('ey.id, ey.username,ey.photo,cn.domain,cn.experience');
        $this->db->from('ey_user ey');
        $this->db->join('ey_counsellor_expertise cn', 'ey.id=cn.user_id');
        $this->db->where('ey.role_id', 3);
        $this->db->where('ey.status', "approve");

        //   $this->db->limit($limit, $offset);
        $query = $this->db->get();

        if ($query->num_rows() > 0) {
            return $query->result();
        } else {
            return false;
        }
    }

    public function fetchUser1($limit, $offset) {
        $this->db->select('ey.id, ey.username,ey.photo,cn.domain,cn.experience,eyr.amount,eyr.minutes');
        $this->db->from('ey_user ey');
        $this->db->join('ey_counsellor_expertise cn', 'ey.id=cn.user_id');
        $this->db->join('ey_counselor_renumeration eyr', 'ey.id=eyr.user_id');
        $this->db->where('ey.role_id', 3);
        $this->db->where('ey.status', "approve");

        $this->db->limit($limit, $offset);
        $query = $this->db->get();

        if ($query->num_rows() > 0) {
            return $query->result();
        } else {
            return false;
        }
    }

    public function get_counselor1($filterOpts, $filterOpts1, $limit, $offset, $today) {
        
        
        $select = 'SELECT ey.id,eyc.user_id,ey.username,ey.photo,ey.video_call,cn.domain,cn.experience,eyr.amount,eyr.minutes';
        $from = ' FROM ey_user ey JOIN ey_counsellor_expertise cn ON ey.id=cn.user_id join ey_counselor_renumeration eyr on ey.id=eyr.user_id join ey_counselor_schedule eyc on ey.id=eyc.user_id';
        $where = ' WHERE ey.role_id=3 and ey.status="approve" and eyc.day="' . $today . '" and ';
        
        
        if (empty($filterOpts)) {
            // 0 checkboxes checked
            $where .= 'true';
        } else {
            //$this->session->set_userdata('filterOpts',$_POST['filterOpts']);
            $opts1 = $filterOpts;
            $opts = explode(',', $opts1);

            $where .= '(';
            if (count($opts) == 1) {
                // 1 checkbox checked
                $where .= '  cn.languages like "%' . $opts[0] . '%"';
            } else {
                // 2+ checkboxes checked
                //$options=explode(',',$opts);
                //  $options= json_decode($opts);

                for ($i = 0; $i < count($opts); $i++) {
                    $where .= 'cn.languages like "%' . $opts[$i] . '%"';
                    if ($i < count($opts) - 1) {
                        $where .= ' OR ';
                    }
                }
            }
            $where .= ' )';
        }

        $where .= ' and ';
        if (empty($filterOpts1)) {
            // 0 checkboxes checked
            $where .= ' true ';
        } else {
            // $this->session->set_userdata('filterOpts1',$_POST['filterOpts1']);
            $opts1 = $filterOpts1;
            $opts = explode(',', $opts1);

            $where .= '(';
            if (count($opts) == 1) {
                // 1 checkbox checked

                if ($opts[0] == 1) {
                    $where .= '  cn.domain like "%' . $opts[0] . '%"';
                } else {
                    for ($i = 0; $i < count($opts); $i++) {
                        $where .= 'cn.domain like "%' . $opts[$i] . '%"';
                        if ($i < count($opts) - 1) {
                            $where .= ' OR ';
                        }
                    }
                }
            } else {
                // 2+ checkboxes checked
                //$options=explode(',',$opts);
                //  $options= json_decode($opts);

                for ($i = 0; $i < count($opts); $i++) {
                    if ($opts[$i] == 1) {
                        $where .= '  cn.domain like "%' . $opts[0] . '%"';
                    } else {
                        for ($i = 0; $i < count($opts); $i++) {
                            $where .= 'cn.domain like "%' . $opts[$i] . '%"';
                            if ($i < count($opts) - 1) {
                                $where .= ' OR ';
                            }
                        }
                    }


                    if ($i < count($opts) - 1) {
                        $where .= ' OR ';
                    }
                }
            }
            $where .= ' )';
        }
        
        $where .= 'ORDER BY STR_TO_DATE(eyc.time_starts,"%l:%i %p") ';
        
        
      /*  if ($offset != "") {
            if($offset>4 && $offset<=8){
                 $d=$offset-4;
                 $where .= ' limit ' . $offset . ',' . $d . '';
            }
            else if ($offset>8) {
                 $d=$offset-8;
                 $where .= ' limit ' . $offset . ',' . $d . '';
            }
            else{
                $where .= ' limit ' . $offset . ',' . $offset . '';

            }
        } else {
           // $offset1=4;
           // $limit=0;
           // $where .= ' limit ' . $limit . ',' . $offset1 . '';
            
            $where .= ' limit ' . $limit . '';
        } */
        
        
       // $select1= 'select * from ey_counselor_schedule eyc';
        //$where1 =' where eyc.day in ("eyc.day","monday","tuesday") order by field("eyc.day","monday" )  ';
       // $where1 =' where day="monday" and';
        // $where1 .=' day="tuesday"';
        
        $sql = $select . $from . $where;
       // echo $sql;exit;
        $query = $this->db->query($sql);
        $results = $query->result();
        return $results;
    }
    public function AllCounselor($day){
        
        $sql = "SELECT DISTINCT eyc.*,ey.id, ey.username,ey.photo,ey.video_call,cn.domain,cn.experience,eyr.amount,eyr.minutes FROM ey_counselor_schedule eyc join ey_user ey on eyc.user_id=ey.id"
                . " JOIN ey_counsellor_expertise cn ON ey.id=cn.user_id join ey_counselor_renumeration eyr on ey.id=eyr.user_id"
                . " WHERE ey.role_id=3 and ey.status='approve' and eyc.day='" . $day . "'";
        //echo $sql1; exit;
        $query = $this->db->query($sql);
        $results = $query->result();
        return $results;
    }
    
    public function todayCounsel($user){
        
        $sql = "SELECT DISTINCT ey.id,eyc.user_id, ey.username,ey.photo,ey.video_call,cn.domain,cn.experience,eyr.amount,eyr.minutes FROM ey_counselor_schedule eyc join ey_user ey on eyc.user_id=ey.id"
                . " JOIN ey_counsellor_expertise cn ON ey.id=cn.user_id join ey_counselor_renumeration eyr on ey.id=eyr.user_id"
                . " WHERE ey.role_id=3 and ey.status='approve' and eyc.user_id='" . $user . "'";
        //echo $sql1; exit;
        $query = $this->db->query($sql);
        $results = $query->result();
        return $results;
    }

    public function fetchcount() {

        $this->db->select('ey.id, ey.username,ey.photo,cn.domain,cn.experience');
        $this->db->from('ey_user ey');
        $this->db->join('ey_counsellor_expertise cn', 'ey.id=cn.user_id');
        $this->db->where('ey.role_id', 3);
        $this->db->where('ey.status', "approve");



        $query = $this->db->get();

        if ($query->num_rows() > 0) {
            // print_r($query->result());exit;
            return $query->result();
        } else {
            return false;
        }
    }

    public function checkToken($token) {
        $q = $this->db->get_where('ey_lost_password', array('token' => $token), 1);
        if ($this->db->affected_rows() > 0) {
            $row = $q->row();
            return $row;
        } else {
            error_log('no user found getUserInfo(' . $email . ')');
            return false;
        }
    }

    public function updatePassword($id, $password) {

        $this->db->where('id', $id);
        $this->db->update('ey_user', array('password' => $password));

        return;
    }

    public function getUserInfoByEmail($email) {
        $q = $this->db->get_where('ey_user', array('email' => $email), 1);
        if ($this->db->affected_rows() > 0) {
            $row = $q->row();
            return $row;
        } else {
            error_log('no user found getUserInfo(' . $email . ')');
            return false;
        }
    }

    public function insertToken($data) {


        $this->db->insert('ey_lost_password', $data);
        return true;
    }

    public function getUser($id) {
        $query = $this->db->query("SELECT *
                                 FROM ey_user
                                 WHERE id = '" . $id . "' ");
        //return $query->result();
        return $query->result();
    }

    public function getrat_url($id) {

        //echo "SELECT * FROM ey_ratingurl WHERE id = '" . $id . "'  ";exit;
        $this->db->select('*');
        $this->db->from('ey_ratingurl');
        $this->db->where('id', $id);
        $query = $this->db->get();
        return $query->result_array();
    }

    public function insert_ratingurl($data) {


        if ($this->db->insert('ey_ratingurl', $data)) {

            $insert_id = $this->db->insert_id();

            $data1 = array('urls' => urlencode(base64_encode($insert_id)));

            $this->db->where('id', $insert_id);
            if ($this->db->update('ey_ratingurl', $data1)) {

                return urlencode(base64_encode($insert_id));
            }
        }
    }
    public function getUser1($id) {
        $query = $this->db->query("SELECT *
                                 FROM ey_user
                                 WHERE id = '" . $id . "' ");
        //return $query->result();
        return $query->row();
    }

    public function getUserCounselor($id) {
        $query = $this->db->query("SELECT * "
                . "FROM ey_user a join ey_counsellor_expertise b on a.id=b.user_id "
                . "WHERE a.id = '" . $id . "'");
        return $query->row();
    }

    public function updateCounsel($id, $data, $data1) {

        $this->db->where('id', $id);
        $this->db->update('ey_user', $data);

        // $update_id = $this->db->update_id();
        // echo  $update_id  =$this->db->insert_id();exit;
        // $data1['user_id'] = $update_id;
        $this->db->select('*');
        $this->db->from('ey_counsellor_expertise');
        $this->db->where('user_id', $id);

        $query = $this->db->get();

        if ($query->num_rows() <= 0) {
            $data1['user_id'] = $id;
            $this->db->insert('ey_counsellor_expertise', $data1);
        } else {
            $this->db->where('user_id', $id);
            $this->db->update('ey_counsellor_expertise', $data1);
        }

        return true;
    }

    public function updateCounselor($id, $data) {

        $this->db->where('id', $id);
        $this->db->update('ey_user', $data);


        return true;
    }

    public function checkpassword($id, $currentpassword) {
        $this->db->select('*');
        $this->db->from('ey_user');
        $this->db->where('id', $id);
        $this->db->where('password', MD5($currentpassword));
        $query = $this->db->get();
        if ($query->num_rows() == 1) {
            return $query->row();
        } else {
            return false;
        }
    }

    public function getUserList() {
        $query = $this->db->query("SELECT *
                                 FROM ey_user where role_id=4 and phone!=''");
        //return $query->result();
        return $query->result();
    }

    public function getCouponDetails() {

        $query = $this->db->query(" SELECT visitor_info, coupon_balance FROM ey_coupon_balance WHERE payment_status = 'captured'");
        return $query->result();
    }

    public function getUserdata($id, $sche_id) {

        $this->db->select('phone as phone');
        $this->db->from('ey_user');
        $this->db->where('id', $id);
        $query = $this->db->get();
        if ($query->num_rows() == 1) {
            return $query->row();
        } else {

            $this->db->select('visiter_info as phone');
            $this->db->from('ey_coupon_purchased');
            $this->db->where('coupon_code', $id);
            $query = $this->db->get();
            $row = $query->row();


            if (!empty($row)) {
                if (is_numeric($row->phone)) {
                    return $query->row();
                } else {
                    $this->db->select('phone as phone');
                    $this->db->from('ey_coupon_details');
                    $this->db->where('visitor_info', $id);
                    $this->db->where('sche_id', $sche_id);
                    $query = $this->db->get();
                    return $query->row();
                }
            }
        }
    }

    public function getUserdataSMS($id, $sche_id) {

        $this->db->select('phone as phone');
        $this->db->from('ey_user');
        $this->db->where('id', $id);
        $query = $this->db->get();
        if ($query->num_rows() == 1) {
            return $query->row();
        } else {

            $this->db->select('phone as phone');
            $this->db->from('ey_coupon_details');
            $this->db->where('visitor_info', $id);
            $this->db->where('sche_id', $sche_id);
            $query = $this->db->get();
            return $query->row();
        }
    }

    public function getUserdata2($id) {
        $query = $this->db->query("SELECT *
                                 FROM ey_user
                                 WHERE id = '" . $id . "' ");
        //return $query->result();
        return $query->row();
    }

    public function updateuser($id, $data) {

        $this->db->where('id', $id);
        $this->db->update('ey_user', $data);
        return true;
    }

    public function approveUser($id, $data, $data1) {

        $this->db->where('id', $id);
        $this->db->update('ey_user', $data1);

        $count = $this->checkUserId($id);
        $count1 = $this->checkWorkingId($id);

//        for ($i = 0; $i < count($timing); $i++) {
//            if ($timing[$i]['starts'] != "" && $timing[$i]['ends'] != "") {
//
//                $data_user = array(
//                    'user_id' => $id,
//                    'time_starts' => $timing[$i]['starts'],
//                    'time_ends' => $timing[$i]['ends'],
//                );
//
//                $this->db->insert('ey_counsellor_schedule', $data_user);
//            }
//        }



        if (empty($count)) {
            $data['created_at'] = date("Y-m-d H:i:00");
            $this->db->insert('ey_counselor_renumeration', $data);
        } else {
            $data['updated_at'] = date("Y-m-d H:i:00");
            $this->db->where('user_id', $id);
            $this->db->update('ey_counselor_renumeration', $data);
        }
        return true;
    }

    public function submitTimeSlots($id, $timing, $hours) {
        $this->db->where('user_id', $id);
        $this->db->delete('ey_counsellor_schedule');
        for ($i = 0; $i < count($timing); $i++) {

            $arr = explode(' - ', $timing[$i]);
            $data = array(
                'user_id' => $id,
                'working_hours' => $hours,
                'time_starts' => $arr[0],
                'time_ends' => $arr[1],
            );
            $this->db->insert('ey_counsellor_schedule', $data);
        }
    }

    public function getCounselorList() {
        $query = $this->db->query("SELECT *
                                 FROM ey_user where role_id=3");
        //return $query->result();
        return $query->result();
    }

    public function getAdminList() {
        $query = $this->db->query("SELECT *
                                 FROM ey_user where role_id=2");
        //return $query->result();
        return $query->result();
    }

    public function getUserDetails($id) {
        $this->db->select('eyu.id,eyu.username,eyu.status,eyr.minutes,eyr.amount,eyw.days');
        $this->db->from('ey_user eyu');
        $this->db->join('ey_counselor_renumeration eyr', 'eyu.id=eyr.user_id');
        $this->db->join('ey_counselor_workingdays eyw', 'eyu.id=eyw.counselor_id');
        $this->db->where('eyu.id', $id);
        $query = $this->db->get();

        if ($query->num_rows() >= 1) {
            return $query->row();
        } else {
            return false;
        }
    }

    public function checkUserId($id) {
        $this->db->select('id');
        $this->db->from('ey_counselor_renumeration');
        $this->db->where('user_id', $id);
        $query = $this->db->get();
        if ($query->num_rows() == 1) {
            return $query->row();
        } else {
            return false;
        }
    }

    public function checkWorkingId($id) {

        $this->db->select('*');
        $this->db->from('ey_counselor_workingdays');
        $this->db->where('counselor_id', $id);
        $query = $this->db->get();
        if ($query->num_rows() == 1) {
            return $query->row();
        } else {
            return false;
        }
    }

    public function getScheduleDetails($id) {
        
        //echo 'SELECT * FROM ey_counselor_schedule where user_id=" . $id';
        $query = $this->db->query("SELECT *
                                 FROM ey_counselor_schedule where user_id=" . $id);
        //return $query->result();
        return $query->result();
    }

    public function getScheduleIds() {
        $query = $this->db->query("SELECT schedule_id
                                 FROM ey_schedule ");
        //return $query->result();
        return $query->result();
    }

    public function checkSchedule($id, $date, $counsid) {
        $start_date = date('Y-m-d H:i:s', strtotime($date . " " . $id));
        $end_date = date('Y-m-d H:i:s', strtotime("+45 minutes", strtotime($start_date)));
        $sql = "SELECT * FROM ey_schedule where time_starts between '" . $start_date . "' and '" . $end_date . "' and date='" . $date . "' and counselor_id='" . $counsid . "' and is_modified = 0";

        $query = $this->db->query($sql);
        $res = $query->result();
        if (empty($res)) {
            $sql = "SELECT * FROM ey_reschedule where time_starts between '" . $start_date . "' and '" . $end_date . "' and date='" . $date . "' and counselor_id=" . $counsid;
            $query = $this->db->query($sql);
            $res = $query->result();
        }
        return $res;
    }

    public function getUserTiming($id) {
        $query = $this->db->query("SELECT *
                                 FROM ey_counselor_schedule where user_id=" . $id);
        //return $query->result();
        return $query->result();
    }

    public function get_counstime($emp_id, $day) {

        //echo "SELECT * FROM ey_counselor_schedule where user_id='". $emp_id ."' and day='" .$day. "'";
        //$query = $this->db->query("SELECT * FROM ey_counselor_schedule where user_id='". $emp_id ."' and day='" .$day. "'");
        $this->db->select('*');
        $this->db->from('ey_counselor_schedule');
        $this->db->where('user_id', $emp_id);
        $this->db->where('day', $day);
        $query = $this->db->get();
        return $query->result();
    }

    public function get_counstime_today($emp_id) {

        //$query = $this->db->query("SELECT * FROM `ey_counselor_schedule` WHERE day='" . $day . "' and user_id=" . $emp_id);
        $query = $this->db->query("SELECT * FROM `ey_counselor_schedule` WHERE user_id=" . $emp_id);
        return $query->result();
        //echo ($this->db->query("SELECT * FROM ey_counsellor_schedule where user_id=".$emp_id." and time_starts > ".date('h:i A').""));
        //$qry = $this->db->query("SELECT * FROM ey_counsellor_schedule where user_id=".$emp_id." and time_starts > ".date('h:i A')."");
//        $this->db->select('*');
//        $this->db->from('ey_counsellor_schedule');
//        $this->db->where('user_id', $emp_id);
//        $this->db->where('time_starts >', date('h:i A'));
//        $query = $this->db->get();
//        return $query->result();
    }

    public function getSchedule($id) {
        $query = $this->db->query("SELECT *
                                 FROM ey_counselor_schedule where user_id='" . $id . "'");
        //echo $query;exit;
        return $query->row();
    }

    public function checkCodeValid($code) {
        $query = $this->db->query("SELECT *
                                 FROM ey_coupon_purchased where coupon_code='" . $code . "'");
        //return $query->result();
        return $query->row();
    }

    //get days
    public function fetch_days() {
        $this->db->select('counselor_id,days');
        $this->db->from('ey_counselor_workingdays');
        $query = $this->db->get();
        return $query->result();
    }

    public function select_counselor($idd) {

        $this->db->select('ey.id, ey.username,ey.photo,ey.video_call,cn.domain,cn.experience,eyr.amount,eyr.minutes');
        $this->db->from('ey_user ey');
        $this->db->join('ey_counsellor_expertise cn', 'ey.id=cn.user_id');
        $this->db->join('ey_counselor_renumeration eyr', 'ey.id=eyr.user_id');
        $this->db->where('ey.role_id', 3);
        $this->db->where('ey.status', 'approve');
        $this->db->where('ey.id', $idd);

        $query = $this->db->get();
        return $query->result();
    }

    public function get_time($text, $day) {
        if ($text == "Now") {
            $sql = "SELECT DISTINCT id,user_id,day,time_starts,time_ends FROM ey_counselor_schedule WHERE day='" . $day . "'";
            return $this->db->query($sql)->result();
        } else {
            return;
        }
    }

    public function get_counsel($value, $dy) {
        if ($dy == 'All') {
            $this->db->select('ey.id, ey.username,ey.photo,ey.video_call,cn.domain,cn.experience,eyr.amount,eyr.minutes');
            $this->db->from('ey_user ey');
            $this->db->join('ey_counsellor_expertise cn', 'ey.id=cn.user_id');
            $this->db->join('ey_counselor_renumeration eyr', 'ey.id=eyr.user_id');
            $this->db->where('ey.role_id', 3);
            $this->db->where('ey.status', "approve");
            $query = $this->db->get();
            return $query->result();
        } else {

            $this->db->select('ey.id, ey.username,ey.photo,ey.video_call,cn.domain,cn.experience,eyr.amount,eyr.minutes');
            $this->db->from('ey_user ey');
            $this->db->join('ey_counsellor_expertise cn', 'ey.id=cn.user_id');
            $this->db->join('ey_counselor_renumeration eyr', 'ey.id=eyr.user_id');
            $this->db->where('ey.role_id', 3);
            $this->db->where('ey.id', $value);
            $this->db->where('ey.status', "approve");
            $query = $this->db->get();
            return $query->result();
        }
    }

    public function insertcoupon($data) {
        $this->db->insert('ey_coupon_code', $data);
    }

    public function getcouponlist() {
        $this->db->select('*');
        $this->db->from('ey_coupon_code');

        $query = $this->db->get();
        return $query->result();
    }

    public function get_employee($emp_id) {

        $this->db->select('ey.id,ey.username,ey.photo,ey.video_call,cn.domain,cn.experience,cn.languages,wc.days,pr.amount,cn.certifications,cn.accrediations');
        $this->db->from('ey_user ey');
        $this->db->join('ey_counsellor_expertise cn', 'ey.id=cn.user_id');
        $this->db->join('ey_counselor_renumeration pr', 'pr.user_id=cn.user_id');
        $this->db->join('ey_counselor_workingdays wc', 'wc.counselor_id=ey.id');
        $this->db->where('ey.role_id', 3);
        $this->db->where('ey.id', $emp_id);

        $query = $this->db->get();
        return $query->result();
    }

    public function getUserInfo($cid) {

        $this->db->distinct();
        $this->db->select('u.id,u.username,u.email,u.phone,u.status,u.role_id');
        $this->db->from('ey_schedule s');
        $this->db->join('ey_user u', 's.visitor_info=u.id');
        $this->db->where('s.counselor_id', $cid);

        $query = $this->db->get();
        return $query->result();
    }

    public function getbalanceamt($use_id) {

        $this->db->select('c.coupon_balance,p.purchase_date');
        $this->db->from('ey_coupon_purchased p');
        $this->db->join('ey_coupon_balance c', 'p.coupon_code=c.visitor_info');
        $this->db->where('p.coupon_code', $use_id);
        $this->db->where('c.payment_status', "Captured");
        $query = $this->db->get();
        
        return $query->result();
    }

    public function getUserBalance($balnce_id) {

         $this->db->select('c.coupon_balance,p.purchase_date');
        $this->db->from('ey_coupon_purchased p');
        $this->db->join('ey_coupon_balance c', 'p.coupon_code=c.visitor_info');
        $this->db->where('p.coupon_code', $balnce_id);
        $this->db->where('c.payment_status', "Captured");
        $query = $this->db->get();
        return $query->result();
    }

    public function getScheduledCounselor($idd) {

        $query = $this->db->query("SELECT ey.id , ey.username,ey.photo,cn.id as current_schedule_id,cn.date,cn.is_modified,cn.time_starts,cn.time_ends "
                . "FROM ey_user ey "
                . "join ey_schedule cn on ey.id=cn.counselor_id "
                . "where cn.visitor_info='" . $idd . "' and cn.date >= CURDATE() and cn.time_starts >='" . date("Y-m-d H:i:00") . "'");
        //return $query->result();
        return $query->result();
    }

    public function getReScheduledCounselor($idd) {

        $query = $this->db->query("SELECT ey.id, ey.username,cn.id as cid,ey.photo,cn.date,cn.id as current_schedule_id,cn.is_modified,cn.time_starts,cn.time_ends "
                . "FROM ey_user ey "
                . "join ey_reschedule cn on ey.id=cn.counselor_id "
                . "where cn.visitor_info='" . $idd . "' and cn.date >= CURDATE() and cn.time_starts >='" . date("Y-m-d H:i:00") . "' and cn.is_modified=0");
        //return $query->result();
        return $query->result();
    }

    public function getCounselorSchedule($id) {
        $query = $this->db->query("SELECT *
                                 FROM ey_schedule where id=" . $id);
        //return $query->result();
        return $query->row();
    }

    public function updateCounselorSchedule($id, $data) {

        $this->db->where('current_schedule_id', $id);
        $this->db->update('ey_reschedule', array('is_modified' => 1));

        $this->db->where('id', $id);
        $this->db->update('ey_schedule', array('is_modified' => 1));

        $this->db->insert('ey_reschedule', $data);
        return true;
    }

    public function getCouponCodes() {
        $this->db->select('*');
        $this->db->from('ey_coupon_code');
        $this->db->where('status', "active");
        $query = $this->db->get();
        return $query->result();
    }

    public function insertCouponDetails($data) {

        $user_id = $data['coupon_code'];
        
            $this->db->insert('ey_coupon_purchased', $data);
        



        return true;
    }

    public function updatePaymentDetails($id, $data, $paymentData) {

        $this->db->where('order_id', $id);
        $this->db->update('ey_coupon_purchased', array('payment_status' => $data));
        $this->db->insert('ey_payment', $paymentData);
        return true;
    }

    public function getScheduleInfo() {


        //echo "select ey.id, ey.visitor_info,ey.counselor_id,ey.schedule_id,ey.is_modified,ey.time_starts from ey_schedule ey  where ey.date='" . date("Y-m-d") . "' and ey.time_starts='" . date("Y-m-d H:i:s") . "' and ey.is_modified=0";
        $query = $this->db->query("select ey.id, ey.visitor_info,ey.counselor_id,ey.schedule_id,ey.is_modified,ey.video_schedule,ey.time_starts from ey_schedule ey  where ey.date='" . date("Y-m-d") . "' and ey.time_starts='" . date("Y-m-d H:i:00") . "' and ey.is_modified=0 and ey.video_schedule=0 ");
        //return $query->result();
        $result = $query->result();
        $sql = "select ey.id, ey.visitor_info,ey.counselor_id,ey.schedule_id,ey.is_modified,ey.video_schedule,ey.time_starts from ey_reschedule  ey  where ey.date='" . date("Y-m-d") . "' and"
                . " ey.time_starts='" . date("Y-m-d H:i:00") . "' and ey.is_modified=0 and ey.video_schedule=0 ";

        $result1 = $this->db->query($sql)->result();
        return array_merge($result1, $result);
    }

    public function getReminderDetails() {



        $query = $this->db->query("select ey.id, ey.visitor_info,ey.counselor_id,ey.schedule_id,ey.is_modified,ey.time_starts from ey_schedule ey where ey.date='" . date("Y-m-d") . "' and ey.time_starts='" . date("Y-m-d H:i:00", strtotime("+ 15 minutes")) . "' and ey.is_modified=0");
        //return $query->result();
        $result = $query->result();
        $sql = "select ey.id, ey.visitor_info,ey.counselor_id,ey.schedule_id,ey.is_modified,ey.time_starts from ey_reschedule  ey where ey.date='" . date("Y-m-d") . "' and"
                . " ey.time_starts='" . date("Y-m-d H:i:00", strtotime("+ 15 minutes")) . "' and ey.is_modified=0 ";

        $result1 = $this->db->query($sql)->result();
        return array_merge($result1, $result);
    }

    public function insertCallDetails($data) {


        $this->db->insert('ey_call_details', $data);
        return true;
    }

    public function getCallDetails() {
        $query = $this->db->query("SELECT *
                                 FROM ey_call_details where duration is null and DATE(dateCreated) = CURDATE()");
        //return $query->result(); 
        return $query->result();
    }

    public function getCallDetailsByDate() {
        $query = $this->db->query("SELECT *
                                 FROM ey_call_details where DATE(dateCreated) = CURDATE()");
        //return $query->result(); 
        return $query->result();
    }

    public function updateCallDetails($id, $data) {

        $this->db->where('sid', $id);
        $this->db->update('ey_call_details', $data);
        return true;
    }

    public function fetchPayment($id) {
        $query = $this->db->query("SELECT *
                                 FROM ey_coupon_purchased
                                 WHERE visiter_info = '" . $id . "' ");
        //return $query->result(); 
        return $query->result();
    }

    public function fetchCallDetails($id) {

        $query = $this->db->query("SELECT ey.id, ey.username,eyu.username as cname,cn.id as cid,ey.photo,con.amount,cn.counselor_id,cn.date,cn.is_modified,cn.time_starts,cn.time_ends,cc.status as cstatus,cc.duration,cc.price "
                . "FROM ey_user ey "
                . "join ey_schedule cn on ey.id=cn.visitor_info "
                . "join ey_counselor_renumeration con on cn.counselor_id=con.user_id "
                . "join ey_user eyu on cn.counselor_id=eyu.id "
                . "join ey_call_details cc on cc.schedule_id=cn.id "
                . "where cn.visitor_info='" . $id . "'");
        //return $query->result();
        $result = $query->result();

        if (!empty($result)) {
            foreach ($result as $key => $res) {
                if ($res->is_modified == 1) {

                    $query = $this->db->query("SELECT ey.id, ey.username,eyu.username as cname,cn.id as cid,con.amount,ey.photo,cn.counselor_id,cn.date,cn.is_modified,cn.time_starts,cn.time_ends,cc.status as cstatus,cc.duration,cc.price "
                            . "FROM ey_user ey "
                            . "join ey_reschedule cn on ey.id=cn.visitor_info "
                            . "join ey_counselor_renumeration con on cn.counselor_id=con.user_id "
                            . "join ey_call_details cc on cc.schedule_id=cn.current_schedule_id "
                            . "join ey_user eyu on cn.counselor_id=eyu.id "
                            . "where cn.visitor_info='" . $id . "'and cn.current_schedule_id=" . $res->cid);


                    $result1 = $query->result();

                    unset($result[$key]);
                } else {
                    $result1[] = "";
                }
            }
        } else {
            $result1[] = "";
        }

        return array_merge($result, $result1);
    }

    /* get counselor amount */

    public function getamt() {
        $this->db->select('amount');
        $this->db->from('ey_counselor_renumeration');
        $this->db->where('user_id', $_POST['con_id']);
        $query = $this->db->get();
        $data = $query->result_array();

        return($data[0]['amount']);
    }

    public function getCounsRenumeration($id) {
        $this->db->select('minutes,amount');
        $this->db->from('ey_counselor_renumeration');
        $this->db->where('user_id', $id);
        $query = $this->db->get();
        $data = $query->result_array();

        return $data;
    }

    public function getratingsbyid($cid, $ccid) {
//echo $cid;exit;


        $this->db->select('*');
        $this->db->from('ey_ratings');
        $this->db->where('c_id', $cid);
        $this->db->where('schedule_id', $ccid);
        $query = $this->db->get();
        return $query->result_array();
    }

    public function getcoupon() {
        
         $this->db->select('coupon_balance');
        $this->db->from('ey_coupon_balance');
        $this->db->where('visitor_info', $_POST['user_id']);
        $this->db->where('payment_status', "Captured");
        $query = $this->db->get();
        $data = $query->result_array();
        if (!empty($data)) {
            return($data[0]['coupon_balance']);
        } else {
            echo 0;
        }
    }

    public function get_appointment($uid) {
        $this->db->select('a.id as sid,a.counselor_id,a.schedule_id,a.date,a.is_modified,a.note,b.username,a.time_starts,a.time_ends');
        $this->db->from('ey_schedule a');
        $this->db->join('ey_user b', 'a.counselor_id=b.id');
        $this->db->where('a.visitor_info', $uid);
        $this->db->order_by('a.date desc,a.time_starts asc');


        $result = $this->db->get()->result();

        if (!empty($result)) {
            foreach ($result as $key => $res) {
                if ($res->is_modified == 1) {

                   $this->db->select('a.id as sid,a.schedule_id,a.date,a.visitor_info,a.is_modified,a.reason_to_reschedule as note,b.username,a.time_starts,a.time_ends');
                    $this->db->from('ey_reschedule a');
                    $this->db->join('ey_user b', 'a.counselor_id = b.id');
                    $this->db->where('a.visitor_info', $uid);
                    $this->db->where('a.current_schedule_id', $res->sid);
                    $this->db->order_by('a.date desc, a.time_starts asc');



                    $result1 = $this->db->get()->result();

                    unset($result[$key]);
                } else {
                    $result1[] = "";
                }
            }
        } else {
            $result1[] = "";
        }
        return array_merge($result1, $result);
    }

    public function insertOTPDetails($data) {
        $this->db->insert('ey_phone_verify', $data);
        return true;
    }

    public function getOTP($phone, $otp) {
        $this->db->select('*');
        $this->db->from('ey_phone_verify');
        $this->db->where('phone', $phone);
        $this->db->where('code', $otp);


        $query = $this->db->get();

        if ($query->num_rows() == 1) {
            return $query->row();
        } else {
            return false;
        }
    }

    public function getSchedData($id) {
        $this->db->select('schedule_id');
        $this->db->from('ey_call_details');
        $this->db->where('sid', $id);

        $query = $this->db->get();
        $data = $query->row();

        return $data;
    }

    public function getVisitorData($id) {
        $this->db->select('counselor_id,visitor_info');
        $this->db->from('ey_schedule');
        $this->db->where('id', $id);

        $query = $this->db->get();
        $data = $query->row();

        return $data;
    }

    public function updateCouponAmnt($visitor, $renum) {
        if (is_numeric($visitor)) {
            $this->db->where('visiter_info', $visitor);
            $this->db->where('payment_status', 'Captured');
            $this->db->update('ey_coupon_purchased', array('coupon_balance' => $renum));
        } else {
            $this->db->where('coupon_code', $visitor);
            $this->db->where('payment_status', 'Captured');
            $this->db->update('ey_coupon_purchased', array('coupon_balance' => $renum));
        }

        return true;
    }

    public function getConAmt($id) {
        $this->db->select('amount');
        $this->db->from('ey_counselor_renumeration');
        $this->db->where('user_id', $id);

        $query = $this->db->get();
        $data = $query->result_array();

        return($data[0]['amount']);
    }

    public function getStatus($id) {
        $this->db->select('*');
        $this->db->from('ey_coupon_purchased');
        $this->db->where('visiter_info', $id);
        $this->db->where('payment_status', 'Captured');
        $this->db->order_by('id', "desc")->limit(1);
        $query = $this->db->get();
        $data = $query->row();

        return $data;
    }

    public function getStatus1($id) {
        $this->db->select('*');
        $this->db->from('ey_coupon_purchased');
        $this->db->where('coupon_code', $id);
        $this->db->where('payment_status', 'Captured');
        $this->db->order_by('id', "desc")->limit(1);
        $query = $this->db->get();
        $data = $query->row();

        return $data;
    }

    public function counselor_appointment($uid) {

        $this->db->select('a.id as sid,a.visitor_info,a.schedule_id,a.date,a.is_modified,a.note,b.username,a.time_starts,a.time_ends');
        $this->db->from('ey_schedule a');
        $this->db->join('ey_user b', 'a.visitor_info=b.id');
        $this->db->where('a.counselor_id', $uid);
        $this->db->order_by("a.date", "DESC");
        $this->db->order_by("a.time_starts", "ASC");
        $result = $this->db->get()->result();


        if (!empty($result)) {

            foreach ($result as $key => $res) {
                if ($res->is_modified == 1) {

                    $this->db->select('a.id,a.schedule_id,a.date,a.visitor_info,a.is_modified,a.current_schedule_id as sid,d.note,b.username,a.time_starts,a.time_ends');
                    $this->db->from('ey_reschedule a');
                    $this->db->join('ey_schedule d', 'a.current_schedule_id = d.id');
                    $this->db->join('ey_user b', 'a.visitor_info = b.id');
                    $this->db->where('a.counselor_id', $uid);
                    $this->db->where('a.current_schedule_id', $res->sid);
                    $this->db->order_by("a.date", "DESC");
                    $this->db->order_by("a.time_starts", "ASC");
                    $result1 = $this->db->get()->result();

                    unset($result[$key]);
                } else {
                    $result1[] = "";
                }
            }
        } else {
            $result1[] = "";
        }

        $this->db->select('a.id as sid,a.visitor_info,a.schedule_id,a.date,a.is_modified,a.note,b.visiter_info as username,a.time_starts,a.time_ends');
        $this->db->from('ey_schedule a');
        $this->db->join('ey_coupon_purchased b', 'a.visitor_info=b.coupon_code');
        $this->db->where('a.counselor_id', $uid);
        $this->db->order_by("a.date", "DESC");
        $this->db->order_by("a.time_starts", "ASC");
        $this->db->where('LENGTH(a.visitor_info) >', 5, FALSE);
        $result_sche = $this->db->get()->result();

        if (!empty($result_sche)) {

            foreach ($result_sche as $key => $res) {
                if ($res->is_modified == 1) {

                    $this->db->select('a.id,a.schedule_id,a.date,a.visitor_info,a.is_modified,a.current_schedule_id as sid,d.note,b.visiter_info as username,a.time_starts,a.time_ends');
                    $this->db->from('ey_reschedule a');
                    $this->db->join('ey_schedule d', 'a.current_schedule_id = d.id');
                    $this->db->join('ey_coupon_purchased b', 'a.visitor_info = b.coupon_code');
                    $this->db->where('a.counselor_id', $uid);
                    $this->db->order_by("a.date", "DESC");
                    $this->db->order_by("a.time_starts", "ASC");
                    $this->db->where('LENGTH(a.visitor_info) >', 5, FALSE);
                    $this->db->where('a.current_schedule_id', $res->sid);
                    $result1_sche = $this->db->get()->result();

                    unset($result_sche[$key]);
                } else {
                    $result1_sche[] = "";
                }
            }
        } else {
            $result1_sche[] = "";
        }

        return array_merge($result, $result1, $result_sche, $result1_sche);
    }

    public function getAppointment($schedule_id, $date) {

        $this->db->select('a.id as sid,a.visitor_info,a.schedule_id,a.date,a.is_modified,a.note,b.username,a.time_starts,a.time_ends');
        $this->db->from('ey_schedule a');
        $this->db->join('ey_user b', 'a.counselor_id=b.id');
        $this->db->where('a.id', $schedule_id);
        $this->db->where('a.date', $date);
        $result = $this->db->get()->row();
        if (!empty($result)) {
            return $result;
        } else {
            $this->db->select('a.id as sid,a.visitor_info,a.schedule_id,a.date,a.is_modified,b.username,a.time_starts,a.time_ends');
            $this->db->from('ey_reschedule a');
            $this->db->join('ey_user b', 'a.visitor_info=b.id');
            $this->db->where('a.id', $schedule_id);
            $this->db->where('a.date', $date);
            $result = $this->db->get()->row();
            return $result;
        }
    }

    public function getCallData($id) {
        $this->db->select('status,duration,price,renumeration');
        $this->db->from('ey_call_details');
        $this->db->where('schedule_id', $id);
        $query = $this->db->get();
        $data = $query->row();

        return $data;
    }

    public function checkCounsData($cid) {

        $this->db->distinct();
        $this->db->select('*');
        $this->db->from('ey_counselor_schedule s');
        $this->db->join('ey_counselor_renumeration u', 's.user_id=u.user_id');
        $this->db->join('ey_user ey', 'ey.id=s.user_id');
        $this->db->where('s.user_id', $cid);
        $this->db->where('ey.status', 'approve');
        $query = $this->db->get();
        return $query->row();
    }

    public function getTokenDetails($order_id) {
        $this->db->select('*');
        $this->db->from('ey_coupon_purchased');
        $this->db->where('order_id', $order_id);
        $query = $this->db->get();
        $data = $query->result();

        return $data;
    }

    public function checkPassword1($cleanToken, $current_password) {

        $query = $this->db->query("SELECT ey.id  "
                . "FROM ey_user ey "
                . "join ey_lost_password cn on ey.id=cn.user_id "
                . "where cn.token='" . $cleanToken . "'");


        return $query->row();
    }

    public function getLoggedUser($email, $oauth_uid) {
        $this->db->select('*');
        $this->db->from('ey_user');
        $this->db->where('email', $email);
        $this->db->where('oauth_uid', $oauth_uid);


        $query = $this->db->get();


        if ($query->num_rows() > 0) {

            return $query->row();
        } else {
            return false;
        }
    }

    public function fetchPaymentDetails() {
        $query = $this->db->query("SELECT *
                                 FROM ey_coupon_purchased");
        return $query->result();
    }

    public function fetchCalls() {
//        $query = $this->db->query("SELECT ey.id, ey.username,eyu.username as cname,cn.id as cid,ey.photo,con.amount,cn.counselor_id,cn.date,cn.is_modified,cn.time_starts,cn.time_ends,cc.schedule_id as ccid,cc.status as cstatus,cc.duration,cc.price "
//                . "FROM ey_user ey "
//                . "join ey_schedule cn on ey.id=cn.visitor_info "
//                . "join ey_counselor_renumeration con on cn.counselor_id=con.user_id "
//                . "join ey_user eyu on cn.counselor_id=eyu.id "
//                . "join ey_call_details cc on cc.schedule_id=cn.id and cn.date >= CURDATE()");
//        //return $query->result();
//        $result = $query->result();
//
//        if (!empty($result)) {
//            foreach ($result as $key => $res) {
//                if ($res->is_modified == 1) {
//
//                    $query = $this->db->query("SELECT ey.id, ey.username,eyu.username as cname,cn.id as cid,con.amount,ey.photo,cn.counselor_id,cn.date,cn.is_modified,cn.time_starts,cn.time_ends,cc.schedule_id as ccid,cc.status as cstatus,cc.duration,cc.price "
//                            . "FROM ey_user ey "
//                            . "join ey_reschedule cn on ey.id=cn.visitor_info "
//                            . "join ey_counselor_renumeration con on cn.counselor_id=con.user_id "
//                            . "join ey_call_details cc on cc.schedule_id=cn.current_schedule_id "
//                            . "join ey_user eyu on cn.counselor_id=eyu.id "
//                            . "where cn.date >= CURDATE() and cn.current_schedule_id=" . $res->cid);
//
//
//                    $result1 = $query->result();
//
//                    unset($result[$key]);
//                } else {
//                    $result1[] = "";
//                }
//            }
//        } else {
//            $result1[] = "";
//        }
        $query = $this->db->query("SELECT ey.id, ey.username as cname,cn.id as cid,ey.photo,con.amount,cn.counselor_id,cn.date,cn.is_modified,cn.time_starts,cn.time_ends,cc.schedule_id as ccid,cc.status as cstatus,cc.duration,cc.price "
                . "FROM ey_coupon_purchased b "
                . "join ey_schedule cn on cn.visitor_info = b.coupon_code "
                . "join ey_counselor_renumeration con on cn.counselor_id=con.user_id "
                . "join ey_user ey on cn.counselor_id=ey.id "
                . "join ey_call_details cc on cc.schedule_id=cn.id ");
        //return $query->result();
        $result_sch = $query->result();
        $result1_sch[] = "";
        if (!empty($result_sch)) {
            foreach ($result_sch as $key => $res) {
                if ($res->is_modified == 1) {

                    $query = $this->db->query("SELECT ey.id, ey.username as cname,cn.id as cid,con.amount,ey.photo,cn.counselor_id,cn.date,cn.is_modified,cn.time_starts,cn.time_ends,cc.schedule_id as ccid,cc.status as cstatus,cc.duration,cc.price "
                            . "FROM ey_coupon_purchased b "
                            . "join ey_reschedule cn on cn.visitor_info = b.coupon_code "
                            . "join ey_counselor_renumeration con on cn.counselor_id=con.user_id "
                            . "join ey_call_details cc on cc.schedule_id=cn.current_schedule_id "
                            . "join ey_user ey on cn.counselor_id=ey.id "
                            . "where cn.current_schedule_id=" . $res->cid);


                    $result1_sch = $query->result();

                    unset($result_sch[$key]);
                }
            }
        } else {
            $result1_sch[] = "";
        }

        return array_merge($result_sch, $result1_sch);
    }

    public function checkScheduleDetails($id, $date) {
        $this->db->select('*');
        $this->db->from('ey_schedule');
        $this->db->where('schedule_id', $id);
        $this->db->where('date', $date);
        $query = $this->db->get();
        return $query->num_rows();
    }

    public function checkReScheduleDetails($id, $date) {
        $this->db->select('*');
        $this->db->from('ey_reschedule');
        $this->db->where('schedule_id', $id);
        $this->db->where('date', $date);
        $query = $this->db->get();
        return $query->num_rows();
    }

    public function getcallBackUserList() {
        $query = $this->db->query("SELECT *
                                 FROM ey_enquiry");
        //return $query->result();
        return $query->result();
    }

    public function getScheduleData($schedule_id, $visitor_info, $sche_id, $start) {

        if (is_numeric($visitor_info) || preg_match('/^[0-9]{3}-[0-9]{3}-[0-9]{4}$/', $visitor_info)) {

            $this->db->select('eys.date,ey.username as couns_name,ey1.username as visitor_name, eys.time_starts,eys.time_ends');
            $this->db->from('ey_schedule eys');
            $this->db->join('ey_user ey', 'ey.id=eys.counselor_id');
            $this->db->join('ey_user ey1', 'ey1.id=eys.visitor_info');
            $this->db->where('eys.id', $schedule_id);
            $this->db->where('eys.visitor_info', $visitor_info);
            $this->db->where('eys.date',$start_date = date('Y-m-d', strtotime($start)) );
            $query = $this->db->get();
            if ($query->num_rows() > 0) {

                return $query->row();
            } else {
                $this->db->select('eys.date,ey.username as couns_name,ey1.username as visitor_name, eys.time_starts,eys.time_ends');
                $this->db->from('ey_reschedule eys');
                $this->db->join('ey_user ey', 'ey.id=eys.counselor_id');
                $this->db->join('ey_user ey1', 'ey1.id=eys.visitor_info');
                $this->db->where('eys.id', $schedule_id);
                $this->db->where('eys.visitor_info', $visitor_info);
                $this->db->where('eys.date',$start_date = date('Y-m-d', strtotime($start)) );
                $query = $this->db->get();

                return $query->row();
            }
        } else {

            $this->db->select('eys.date,ey1.username as couns_name,ey.visiter_info as visitor_name, eys.time_starts,eys.time_ends');
            $this->db->from('ey_schedule eys');
            $this->db->join('ey_user ey1', 'ey1.id=eys.counselor_id');
            $this->db->join('ey_coupon_purchased ey', 'ey.coupon_code=eys.visitor_info');
            $this->db->where('eys.id', $schedule_id);
            $this->db->where('eys.visitor_info', $visitor_info);
             $this->db->where('eys.date',$start_date = date('Y-m-d', strtotime($start)) );
            $query = $this->db->get();
            if ($query->num_rows() > 0) {

                return $query->row();
            } else {
                $this->db->select('eys.date,ey1.username as couns_name,ey.visiter_info as visitor_name, eys.time_starts,eys.time_ends');
                $this->db->from('ey_reschedule eys');
                $this->db->join('ey_coupon_purchased ey', 'ey.coupon_code=eys.visitor_info');
                $this->db->join('ey_user ey1', 'ey1.id=eys.counselor_id');
                $this->db->where('eys.id', $schedule_id);
                $this->db->where('eys.visitor_info', $visitor_info);
                 $this->db->where('eys.date',$start_date = date('Y-m-d', strtotime($start)) );
                $query = $this->db->get();
                return $query->row();
            }
        }
    }

    /*public function getCouponBalance($id) {
        $this->db->select('sum(coupon_balance) as coupon_balance');
        $this->db->from('ey_coupon_purchased');
        $this->db->where('coupon_code', $id);
        $this->db->where('payment_status', "Captured");
        
        $query = $this->db->get();
        $data = $query->result_array();
        if (!empty($data)) {
            return($data[0]['coupon_balance']);
        } else {
            echo 0;
        }
    }*/

    public function getCouponBalance($id) {
        $this->db->select('coupon_balance as coupon_balance');
        $this->db->from('ey_coupon_balance');
        $this->db->where('visitor_info', $id);
        $this->db->where('payment_status', "Captured");
        
        $query = $this->db->get();
        $data = $query->result_array();
        if (!empty($data)) {
            return($data[0]['coupon_balance']);
        } else {
            echo 0;
        }
    }
    
    public function getCouponBalance1($id,$order_id) {
        $this->db->select('coupon_balance');
        $this->db->from('ey_coupon_purchased');
        $this->db->where('coupon_code', $id);
        $this->db->where('payment_status', "Captured");
        $this->db->order_by('id', "desc")->limit(1);
        $query = $this->db->get();
        $data = $query->result_array();
        if (!empty($data)) {
            return($data[0]['coupon_balance']);
        } else {
            echo 0;
        }
    }

    public function check_status($id, $stats) {
        $this->db->set('STATUS', $stats);
        $this->db->where('id', $id);
        $this->db->update('ey_coupon_code');
        return 1;
    }

    public function get_purchase_details($from, $to) {
        $query = $this->db->query("SELECT *
                                 FROM ey_coupon_purchased where purchase_date BETWEEN '" . $from . "' and '" . $to . "'");
        return $query->result();
    }

    public function getUserPurchase($user_id) {
        $query = $this->db->query("SELECT *
                                 FROM ey_coupon_purchased where id = '" . $user_id . "' ");
        return $query->result();
    }

    public function get_today_logs($from, $to) {

        $query = $this->db->query("SELECT ey.id,ey.username,eyu.username as cname,cn.id as cid,con.amount,cn.counselor_id,cn.date,cn.is_modified,cn.time_starts,cn.time_ends,cc.schedule_id as ccid,cc.status as cstatus,cc.duration,cc.price "
                . "FROM ey_user ey "
                . "join ey_schedule cn on ey.id=cn.visitor_info "
                . "join ey_counselor_renumeration con on cn.counselor_id=con.user_id "
                . "join ey_user eyu on cn.counselor_id=eyu.id "
                . "join ey_call_details cc on cc.schedule_id=cn.id "
                . "where cn.date BETWEEN '" . $from . "' and '" . $to . "'");
        //return $query->result();
        $result = $query->result();

        if (!empty($result)) {
            foreach ($result as $key => $res) {
                if ($res->is_modified == 1) {

                    $query = $this->db->query("SELECT ey.id,ey.username,eyu.username as cname,cn.id as cid,con.amount,cn.counselor_id,cn.date,cn.is_modified,cn.time_starts,cn.time_ends,cc.id as ccid,cc.status as cstatus,cc.duration,cc.price "
                            . "FROM ey_user ey "
                            . "join ey_reschedule cn on ey.id=cn.visitor_info "
                            . "join ey_counselor_renumeration con on cn.counselor_id=con.user_id "
                            . "join ey_call_details cc on cc.schedule_id=cn.current_schedule_id "
                            . "join ey_user eyu on cn.counselor_id=eyu.id "
                            . "where cn.date BETWEEN '" . $from . "' and '" . $to . "' and cn.current_schedule_id=" . $res->cid);


                    $result1 = $query->result();

                    unset($result[$key]);
                } else {
                    $result1[] = "";
                }
            }
        } else {
            $result1[] = "";
        }
        
        $query = $this->db->query("SELECT ey.id, ey.username as cname,cn.id as cid,ey.photo,con.amount,cn.counselor_id,cn.date,cn.is_modified,cn.time_starts,cn.time_ends,cc.schedule_id as ccid,cc.status as cstatus,cc.duration,cc.price "
                . "FROM ey_coupon_purchased b "
                . "join ey_schedule cn on cn.visitor_info = b.coupon_code "
                . "join ey_counselor_renumeration con on cn.counselor_id=con.user_id "
                . "join ey_user ey on cn.counselor_id=ey.id "
                . "join ey_call_details cc on cc.schedule_id=cn.id "
                . "where cn.date BETWEEN '" . $from . "' and '" . $to . "'");
        //return $query->result();
        $result_sch = $query->result();
        $result1_sch[] = "";
        if (!empty($result_sch)) {
            foreach ($result_sch as $key => $res) {
                if ($res->is_modified == 1) {

                    $query = $this->db->query("SELECT ey.id, ey.username as cname,cn.id as cid,con.amount,ey.photo,cn.counselor_id,cn.date,cn.is_modified,cn.time_starts,cn.time_ends,cc.schedule_id as ccid,cc.status as cstatus,cc.duration,cc.price "
                            . "FROM ey_coupon_purchased b "
                            . "join ey_reschedule cn on cn.visitor_info = b.coupon_code "
                            . "join ey_counselor_renumeration con on cn.counselor_id=con.user_id "
                            . "join ey_call_details cc on cc.schedule_id=cn.current_schedule_id "
                            . "join ey_user ey on cn.counselor_id=ey.id "
                            . "where cn.date BETWEEN '" . $from . "' and '" . $to . "' and cn.current_schedule_id=" . $res->cid);


                    $result1_sch = $query->result();

                    unset($result_sch[$key]);
                }
            }
        } else {
            $result1_sch[] = "";
        }
        return array_merge(array_merge($result, $result1, $result_sch, $result1_sch));
    }

    public function getUserlogs($user_id) {

//        $query = $this->db->query("SELECT ey.id,ey.username,eyu.username as cname,cn.id as cid,con.amount,cn.counselor_id,cn.date,cn.is_modified,cs.time_starts,cs.time_ends,cc.id as ccid,cc.status as cstatus,cc.duration,cc.price "
//                . "FROM ey_user ey "
//                . "join ey_schedule cn on ey.id=cn.visitor_info "
//                . "join ey_counselor_renumeration con on cn.counselor_id=con.user_id "
//                . "join ey_counsellor_schedule cs on cs.id=cn.schedule_id "
//                . "join ey_user eyu on cn.counselor_id=eyu.id "
//                . "join ey_call_details cc on cc.schedule_id=cn.id "
//                . "where cc.id = '" . $user_id . "'");
//        
//        $result = $query->result();
//
//        if (!empty($result)) {
//            foreach ($result as $key => $res) {
//                if ($res->is_modified == 1) {
//
//                    $query = $this->db->query("SELECT ey.id,ey.username,eyu.username as cname,cn.id as cid,con.amount,cn.counselor_id,cn.date,cn.is_modified,cs.time_starts,cs.time_ends,cc.id as ccid,cc.status as cstatus,cc.duration,cc.price "
//                            . "FROM ey_user ey "
//                            . "join ey_reschedule cn on ey.id=cn.visitor_info "
//                            . "join ey_counsellor_schedule cs on cs.id=cn.schedule_id "
//                            . "join ey_counselor_renumeration con on cn.counselor_id=con.user_id "
//                            . "join ey_call_details cc on cc.schedule_id=cn.current_schedule_id "
//                            . "join ey_user eyu on cn.counselor_id=eyu.id "
//                            . "where cc.id = '" . $user_id . "' and cn.current_schedule_id=" . $res->cid);
//
//                    $result1 = $query->result();
//
//                    unset($result[$key]);
//                } else {
//                    $result1[] = "";
//                }
//            }
//        } else {
//            $result1[] = "";
//        }
//        return array_merge($result, $result1);


        $query = $this->db->query("SELECT ey.id, ey.username,eyu.username as cname,cn.id as cid,ey.photo,con.amount,cn.counselor_id,cn.date,cn.is_modified,cn.time_starts,cn.time_ends,cc.schedule_id as ccid,cc.status as cstatus,cc.duration,cc.price "
                . "FROM ey_user ey "
                . "join ey_schedule cn on ey.id=cn.visitor_info "
                . "join ey_counselor_renumeration con on cn.counselor_id=con.user_id "
                . "join ey_user eyu on cn.counselor_id=eyu.id "
                . "join ey_call_details cc on cc.schedule_id=cn.id and cn.date >= CURDATE()"
                . "where cc.schedule_id = '" . $user_id . "'");
        //return $query->result();
        $result = $query->result();

        if (!empty($result)) {
            foreach ($result as $key => $res) {
                if ($res->is_modified == 1) {

                    $query = $this->db->query("SELECT ey.id, ey.username,eyu.username as cname,cn.id as cid,con.amount,ey.photo,cn.counselor_id,cn.date,cn.is_modified,cn.time_starts,cn.time_ends,cc.schedule_id as ccid,cc.status as cstatus,cc.duration,cc.price "
                            . "FROM ey_user ey "
                            . "join ey_reschedule cn on ey.id=cn.visitor_info "
                            . "join ey_counselor_renumeration con on cn.counselor_id=con.user_id "
                            . "join ey_call_details cc on cc.schedule_id=cn.current_schedule_id "
                            . "join ey_user eyu on cn.counselor_id=eyu.id "
                            . "where cn.date >= CURDATE() and cc.schedule_id = '" . $user_id . "' and cn.current_schedule_id=" . $res->cid);


                    $result1 = $query->result();

                    unset($result[$key]);
                } else {
                    $result1[] = "";
                }
            }
        } else {
            $result1[] = "";
        }
        $query = $this->db->query("SELECT ey.id, ey.username as cname,cn.id as cid,ey.photo,con.amount,cn.counselor_id,cn.date,cn.is_modified,cn.time_starts,cn.time_ends,cc.schedule_id as ccid,cc.status as cstatus,cc.duration,cc.price "
                . "FROM ey_coupon_purchased b "
                . "join ey_schedule cn on cn.visitor_info = b.coupon_code "
                . "join ey_counselor_renumeration con on cn.counselor_id=con.user_id "
                . "join ey_user ey on cn.counselor_id=ey.id "
                . "join ey_call_details cc on cc.schedule_id=cn.id "
                . "where cc.schedule_id = '" . $user_id . "'");
        //return $query->result();
        $result_sch = $query->result();
        $result1_sch[] = "";
        if (!empty($result_sch)) {
            foreach ($result_sch as $key => $res) {
                if ($res->is_modified == 1) {

                    $query = $this->db->query("SELECT ey.id, ey.username as cname,cn.id as cid,con.amount,ey.photo,cn.counselor_id,cn.date,cn.is_modified,cn.time_starts,cn.time_ends,cc.schedule_id as ccid,cc.status as cstatus,cc.duration,cc.price "
                            . "FROM ey_coupon_purchased b "
                            . "join ey_reschedule cn on cn.visitor_info = b.coupon_code "
                            . "join ey_counselor_renumeration con on cn.counselor_id=con.user_id "
                            . "join ey_call_details cc on cc.schedule_id=cn.current_schedule_id "
                            . "join ey_user ey on cn.counselor_id=ey.id "
                            . "where cc.schedule_id = '" . $user_id . "' and cn.current_schedule_id=" . $res->cid);


                    $result1_sch = $query->result();

                    unset($result_sch[$key]);
                }
            }
        } else {
            $result1_sch[] = "";
        }

        return array_merge(array_merge($result, $result1, $result_sch, $result1_sch));
    }

    public function checkPhoneNumber($id) {
        $this->db->select('phone');
        $this->db->from('ey_user');

        $this->db->where('id', $id);


        $query = $this->db->get();


        if ($query->num_rows() > 0) {

            return $query->row();
        } else {
            return false;
        }
    }

    public function updatePhoneNumber($id, $phone) {

        $this->db->where('id', $id);
        $this->db->update('ey_user', array('phone' => $phone));

        return;
    }

    public function insert_contact($data) {
        $this->db->insert('ey_contact', $data);
        return TRUE;
    }

    public function getCounsellingTypes() {
        $query = $this->db->query("SELECT *
                                 FROM ey_counselling_types");
        //return $query->result();
        return $query->result();
    }

    public function counselor_payment() {

//         $query = $this->db->query("SELECT cd.schedule_id,cd.duration,cd.renumeration,cd.status,es.id,es.counselor_id,eu.username "
//                . "FROM ey_call_details cd "
//                . "join ey_schedule es on cd.schedule_id = es.id "
//                . "join ey_user eu on es.counselor_id = eu.id "
//                . "where cd.status = 'completed' ");

        $this->db->select('cd.schedule_id, SUM(cd.duration) AS dur, SUM(cd.renumeration) AS renum, COUNT(cd.renumeration) as count, cd.status, es.id, es.counselor_id, eu.username', FALSE);
        $this->db->from('ey_call_details cd');
        $this->db->join('ey_schedule es', 'cd.schedule_id = es.id');
        $this->db->join('ey_user eu', 'es.counselor_id = eu.id');
        $this->db->where('cd.status =', 'completed');
        $this->db->group_by('es.counselor_id');
        $query = $this->db->get();

        $result = $query->result();
        return $result;
    }

    public function getNotes($user_id) {
        $this->db->select('*');
        $this->db->from('ey_notes');
        $this->db->where('sche_id', $user_id);
        $query = $this->db->get();
        return $query->result();
    }

    public function insert_notes($data) {
        $this->db->insert(' ey_notes', $data);
        return TRUE;
    }

    public function getRefundStatus($id) {
        $query = $this->db->query("SELECT *
                                 FROM ey_call_details where sid = '" . $id . "'");
        //return $query->result(); 
        return $query->row();
    }

    public function getCounsRenumerationCall($id) {
        $this->db->select('r.minutes,r.amount');
        $this->db->from('ey_counselor_renumeration r');
        $this->db->join('ey_schedule u', 'r.user_id=u.counselor_id');
        $this->db->where('u.id', $id);
        $query = $this->db->get();
        $data = $query->result_array();

        return $data;
    }

    public function getCallDetailsSID($sid) {
        $query = $this->db->query("SELECT *
                                 FROM ey_call_details where sid='" . $sid . "'");
        //return $query->result(); 
        return $query->row();
    }

    public function get_counspaymnt_date($from, $to) {

        $this->db->select('cd.schedule_id, SUM(cd.duration) AS dur, SUM(cd.renumeration) AS renum, COUNT(cd.renumeration) as count, cd.status, es.id, es.counselor_id, es.date, eu.username', FALSE);
        $this->db->from('ey_call_details cd');
        $this->db->join('ey_schedule es', 'cd.schedule_id = es.id');
        $this->db->join('ey_user eu', 'es.counselor_id = eu.id');
        $this->db->where('cd.status =', 'completed');
        $this->db->where("es.date BETWEEN '" . $from . "' and '" . $to . "'");
        $this->db->group_by('es.counselor_id');
        $query = $this->db->get();

        $result = $query->result();
        return $result;
    }

    public function schedulepayment_details($counsid) {

        $this->db->select('cd.schedule_id, cd.duration AS dur, cd.renumeration AS renum, cd.status, es.id, es.date, es.counselor_id, es.date, eu.username', FALSE);
        $this->db->from('ey_call_details cd');
        $this->db->join('ey_schedule es', 'cd.schedule_id = es.id');
        $this->db->join('ey_user eu', 'es.counselor_id = eu.id');
        $this->db->where('cd.status =', 'completed');
        $this->db->where('es.counselor_id', $counsid);
        $query = $this->db->get();

        $result = $query->result();
        return $result;
    }

    public function getVisitorInfo($sid) {
        $query = $this->db->query("SELECT *
                                 FROM ey_schedule where id='" . $sid . "'");
        $res = $query->row();
        $date = new DateTime($res->date);
        $now = new DateTime();
        $date_diff = $date->diff($now)->format("%d");
        if ($date_diff == 0) {
            return $query->row();
        } else {
            $query = $this->db->query("SELECT *
                                 FROM ey_reschedule where id='" . $sid . "'");
            return $query->row();
        }
    }

    public function check_couponlogin($data) {

        $this->db->select('*');
        $this->db->from('ey_couponlogin');

        $this->db->where('user', $data['email']);
        $this->db->where('pass', $data['pass']);
        $query = $this->db->get();


        if ($query->num_rows() > 0) {

            $login_data = array('logged_in' => TRUE, 'username' => $data['email']);
            $this->session->set_userdata($login_data);

            return TRUE;
        } else {
            return false;
        }
    }

    public function insert_coupon($data) {
        $this->db->insert(' ey_coupon_purchased', $data);
        return TRUE;
    }

    public function fetchcouponlist() {

        $this->db->select('a.*,b.coupon_balance as bal'); 
        $this->db->from('ey_coupon_purchased a');
        $this->db->join('ey_coupon_balance b','a.coupon_code = b.visitor_info');
        $this->db->where('type', 3);
        $query = $this->db->get();
        return $query->result();
    }
    

    public function insert_ratings($data) {
      
      
        if($this->db->insert('ey_ratings', $data)){
             return TRUE;
        }
       
    }

    public function getratings() {

        $this->db->select('*');
        $this->db->from('ey_ratings');
        $query = $this->db->get();
        return $query->result_array();
    }

    public function getratecounsilordetails($id) {

        $this->db->select('*');
        $this->db->from('ey_user');
        $this->db->where('id', $id);
        $query = $this->db->get();
        return $query->result_array();
    }

    public function get_rating_data($blogid) {
        $this->db->select('*');
        $this->db->from('users u');
        $this->db->join('rating r', 'r.user_id = u.user_id');
        $this->db->where('blog_id', $blogid);
        return $query = $this->db->get()->result();
    }

    public function insertTimeSlots($values, $id) {
        $this->db->where('user_id', $id);
        $this->db->delete('ey_counselor_schedule');

        foreach ($values as $val) {
            $data = array(
                "user_id" => $id,
                "day" => $val['day'],
                "working_hours" => $val['start'] . "-" . $val['end'],
                "time_starts" => $val['start'],
                "time_ends" => $val['end'],
            );
            $arr[] = $val['day'];



            $this->db->insert('ey_counselor_schedule', $data);
        }

        $this->db->where('counselor_id', $id);
        $this->db->delete('ey_counselor_workingdays');

        $data1 = array(
            "counselor_id" => $id,
            "days" => json_encode($arr),
        );

        $this->db->insert('ey_counselor_workingdays', $data1);
        return true;
    }

    public function fetchSchedule($id) {

        $this->db->select('*');
        $this->db->from('ey_counselor_schedule');
        $this->db->where('user_id', $id);
        $query = $this->db->get();
        return $query->result();
    }
    
    public function fetch_counselor($cid) {
        $this->db->select('username');
        $this->db->from('ey_user');
        $this->db->where('id', $cid);
        $query = $this->db->get();
        return $query->result();
    }

    public function getCounselingTypes() {

        $this->db->select('*');
        $this->db->from('ey_counselling_types');
        $query = $this->db->get();
        return $query->result();
    }

    public function get_counselor2($filterOpts, $filterOpts1, $limit, $offset, $uid) {
        $select = 'SELECT ey.id, ey.username,ey.photo,cn.domain,cn.experience,eyr.amount,eyr.minutes';
        $from = ' FROM ey_user ey JOIN ey_counsellor_expertise cn ON ey.id=cn.user_id join ey_counselor_renumeration eyr on ey.id=eyr.user_id ';
        $where = ' WHERE ey.role_id=3 and ey.status="approve" and cn.user_id=' . $uid . '  and ';

//echo $where;exit;

        if (empty($filterOpts)) {
            // 0 checkboxes checked
            $where .= 'true';
        } else {
            //$this->session->set_userdata('filterOpts',$_POST['filterOpts']);
            $opts1 = $filterOpts;
            $opts = explode(',', $opts1);

            $where .= '(';
            if (count($opts) == 1) {
                // 1 checkbox checked
                $where .= '  cn.languages like "%' . $opts[0] . '%"';
            } else {
                // 2+ checkboxes checked
                //$options=explode(',',$opts);
                //  $options= json_decode($opts);

                for ($i = 0; $i < count($opts); $i++) {
                    $where .= 'cn.languages like "%' . $opts[$i] . '%"';
                    if ($i < count($opts) - 1) {
                        $where .= ' OR ';
                    }
                }
            }
            $where .= ' )';
        }

        $where .= ' and ';
        if ($filterOpts1) {
            // 0 checkboxes checked
            $where .= ' true ';
        } else {
            // $this->session->set_userdata('filterOpts1',$_POST['filterOpts1']);
            $opts1 = $filterOpts1;
            $opts = explode(',', $opts1);

            $where .= '(';
            if (count($opts) == 1) {
                // 1 checkbox checked

                if ($opts[0] == 1) {
                    $where .= ' cn.experience<= 3';
                } else if ($opts[0] == 2) {

                    $where .= ' cn.experience>3';
                } else {
                    $where .= ' true ';
                }
            } else {
                // 2+ checkboxes checked
                //$options=explode(',',$opts);
                //  $options= json_decode($opts);

                for ($i = 0; $i < count($opts); $i++) {

                    if ($opts[$i] == 1) {
                        $where .= ' cn.experience<= 3';
                    } else if ($opts[$i] == 0) {
                        $where .= ' cn.experience>3';
                    } else {
                        $where .= ' true ';
                    }


                    if ($i < count($opts) - 1) {
                        $where .= ' OR ';
                    }
                }
            }
            $where .= ' )';
        }
        if ($offset != "") {
            $where .= ' limit ' . $limit . ',' . $offset . '';
        } else {
            $where .= ' limit ' . $limit . '';
        }


        $sql = $select . $from . $where;
        $query = $this->db->query($sql);
        $results = $query->result();
        return $results;
    }

    public function getdoamindetails() {


        $this->db->select('*');
        $this->db->from('ey_counsellor_expertise');
        //  $this->db->where('id', 55);
        $query = $this->db->get();
        return $query->result_array();
    }
   public function fetch_visitor_info($id){
       $this->db->select('*');
        $this->db->from('ey_coupon_purchased');
        $this->db->where('order_id', $id);
        $query = $this->db->get();
        return $query->row();
   }
    public function get_coupon_details($id) {
        $this->db->select('sum(coupon_balance) as coupon_balance');
        $this->db->from('ey_coupon_purchased');
        $this->db->where('coupon_code', $id);
        $this->db->where('payment_status', "Captured");
        $this->db->where('balance_status', 0);
        $query = $this->db->get();
        $data = $query->result_array();
        if (!empty($data)) {
            return($data[0]['coupon_balance']);
        } else {
            echo 0;
        }
    }
     public function get_coupon_balance($id) {
        
        $this->db->select('*');
        $this->db->where('visitor_info', $id);
        $this->db->from('ey_coupon_balance');
        $query = $this->db->get();
        $data = $query->result_array();
       
        if (!empty($data)) {
            return($data[0]['coupon_balance']);
        } else {
            echo 0;
        }
    }
    
    public function get_coupon_balance1($id) {
        
        $this->db->select('*');
        $this->db->where('visitor_info', $id);
        $this->db->from('ey_coupon_balance');
        $query = $this->db->get();
              
        $data = $query->row();
        return $data;
    }
    
    public function insertCouponBalance($data) {

        $user_id = $data['visitor_info'];
        $this->db->select('*');
        $this->db->where('visitor_info', $user_id);
        $this->db->from('ey_coupon_balance');
        $query = $this->db->get();
        $data5 = $query->result_array();
        
        if (!empty($data5)) {
            $bal=$data5[0]['coupon_balance'];
            $this->db->where('visitor_info', $user_id);
            $this->db->update('ey_coupon_balance', array('coupon_balance'=>$bal+$data['coupon_balance']));
        } else {
            $this->db->insert('ey_coupon_balance', $data);
        }

        $this->db->where('coupon_code', $user_id);
        $this->db->update('ey_coupon_purchased', array('balance_status'=>1));

        return true;
    }
    public function insertWebinar($paymentData){
        $this->db->insert('ey_seminar_payment', $paymentData);
        return true;
    }
    
    public function getCounselors()
    {

        $this->db->select('*');
        $this->db->from('ey_user');
        $this->db->where('role_id', 3);
        $this->db->where('status', 'approve');
        $query = $this->db->get();
        return $query->result();
    }

    public function VideoCallCount($user_id, $text)
    {
        $this->db->where('id', $user_id);
        $this->db->update('ey_user', array('video_call' => $text));

        return;
    }
    
    public function get_phoneno($info) {
        $this->db->select('phone');
        $this->db->from('ey_user');
        $this->db->where('id', $info);
        $query = $this->db->get();
        return $query->result();
    }
    
    public function getCouponPhone($info,$id) {
        $this->db->select('phone');
        $this->db->from('ey_coupon_details');
        $this->db->where('visitor_info', $info);
        $this->db->where('sche_id', $id);
        $query = $this->db->get();
        return $query->result();
        
    }
}
