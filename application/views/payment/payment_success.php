<?php $this->load->view('layout/header.php') ?>

<style>
    .register-account{
display:none;}
</style>
<div class="page-heading default">
    <div class="container-inner">
        <div class="container">
            <div class="page-title">
                <div class="iw-heading-title">
                    <h1>
                        Payment Complete                     </h1>

                </div>
            </div>
        </div>
    </div>
  
</div>



<div class="contents-main" style="background-color: #f2f2f1;padding-top: 50px;padding-bottom: 50px;" id="contents-main">
    <div class="container">
        <div class="row">
            <div class="col-sm-12 col-xs-12">

                <article id="post-143" class="post-143 page type-page status-publish hentry">
                    <div class="entry-content">

                        <form method="post" action="" class="iwj-form-2 iwj-candidate-form">


                            <div class="iwj-block">
                                <div class="basic-area iwj-block-inner">

                                    <div class="iwjmb-field iwjmb-avatar-wrapper"><div class="iwjmb-input ui-sortable">            
                                            <div id="_iwj_avatar" class="iwj-avatar-container">
                                                <!-- Current avatar -->
                                                <div>
                                                    <h2 class="theme-color">Thank you for your Purchase</h2>
                                                </div>



                                            </div>
                                        </div>
                                    </div>
                                    <div class="row">
                                        <div class="col-md-12">
                                            <a style="margin-top: 40px;padding: 10px 50px;" onclick="location.href = '<?php echo base_url(); ?>';" name="register" class="iwj-btn iwj-btn-primary iwj-btn-medium  iwj-register-btn">BACK TO HOME</a>
                                        </div>
                                    </div>
                                    <!--<div class="row text-center">
                                        <div class="col-md-offset-6">
                                           
                                        </div>

                                    </div>-->

                                </div>


                                <div class="iwj-respon-msg hide"></div>
                               
                            </div>
                        </form>

                    </div><!-- .entry-content -->
                    <div class="clearfix"></div>
                    <footer class="entry-footer">
                    </footer><!-- .entry-footer -->
                </article><!-- #post-## -->
            </div>
        </div>
    </div>
</div>

<?php $this->load->view('layout/footer.php') ?>