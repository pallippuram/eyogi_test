<?php $this->load->view('layout/header.php')?>
<head>
    <script>
    (function(h,o,t,j,a,r){
        h.hj=h.hj||function(){(h.hj.q=h.hj.q||[]).push(arguments)};
        h._hjSettings={hjid:971133,hjsv:6};
        a=o.getElementsByTagName('head')[0];
        r=o.createElement('script');r.async=1;
        r.src=t+h._hjSettings.hjid+j+h._hjSettings.hjsv;
        a.appendChild(r);
    })(window,document,'https://static.hotjar.com/c/hotjar-','.js?sv=');
    </script>
</head>
<div class="contents-main" id="contents-main">

                <article id="post-1614" class="post-1614 page type-page status-publish hentry">
                    <div class="entry-content">

                        <!-- Slider Section -->
                        <!-- Scroll Class : background-scroll background-scroll-home-v3 -->
                        <div  class="vc_row wpb_row vc_row-fluid  vc_custom_1502700206350 vc_row-has-fill vc_row-o-content-middle vc_row-flex " style="height: 100vh;" >
                            <div class="container">
                                
                                <div class="row">
                                    
                                    <div class="wpb_column vc_column_container vc_col-sm-12">
                                        <div class="vc_column-inner"><div class="wpb_wrapper">
                                            <div class="eyo-title  vc_custom_1501829096771 text-center border-color-theme " >
                                                    <!--<img src="assets/images/1920.png" class="img-responsive" style="width: 100%">-->
                                                    <h3 class="iwh-title eyo-title " style="color:#333333;font-size: 64px; line-height: 87px; text-shadow: 0 3px 7px rgba(0,0,0,0.25)"><strong class="mind">Mind to Mind</strong></h3>
                                                    <div class="iwh-description eyo-description" style="color: #00000;font-size: 20px">Talk to your counsellor now! </div>
                                                    <?php if ($this->session->userdata('loggedIn') == true) { ?>
                                                        <p align="center" style="display: inline-flex;padding-top: 20px">
                                                            <!-- <a style="margin-right:20px;display:block;" href="<?php echo base_url(); ?>user/index" name="register" class="iwj-btn iwj-btn-primary iwj-btn-large  iwj-register-btn eyo-btn">Register</a> -->
                                                            
                                                            <a style="margin-right:20px;display:block;" onclick="location.href = '<?php echo base_url(); ?>home/schedule';" class="iwj-btn iwj-btn-primary iwj-btn-large  iwj-register-btn ">Schedule</a>
                                                        </p>
                                                    <?php } else { ?>
                                                        <p align="center" style="display: inline-flex;padding-top: 20px">
                                                            <a style="margin-right:20px;display:block;" href="<?php echo base_url(); ?>user/index" name="register" class="iwj-btn iwj-btn-primary iwj-btn-large  iwj-register-btn eyo-btn">Register</a>
                                                            
                                                            <a style="margin-right:20px;display:block;" href="#anonymous_scheduling" class="iwj-btn iwj-btn-primary iwj-btn-large  iwj-register-btn ">Schedule</a>
                                                        </p>
                                                    <?php } ?>   
                                                </div>
                                                <div class="vc_row wpb_row vc_inner vc_row-fluid">
                                                    <div class="wpb_column vc_column_container vc_col-sm-2 vc_hidden-sm vc_hidden-xs">
                                                        <div class="vc_column-inner">
                                                            <div class="wpb_wrapper">

                                                            </div>

                                                        </div>

                                                    </div>

                                                    <div class="wpb_column vc_column_container vc_col-sm-8 ">
                                                        
                                                        <div class="vc_column-inner vc_custom_1502700751795">
                                                            <div class="wpb_wrapper"><div class="iw-work-steps">
                                                                    <div class="owl-carousel" data-plugin-options="{&quot;navigation&quot;:false,&quot;autoHeight&quot;:false,&quot;pagination&quot;:false,&quot;autoPlay&quot;:true,&quot;paginationNumbers&quot;:false,&quot;items&quot;:3,&quot;itemsDesktop&quot;:[1199,3],&quot;itemsDesktopSmall&quot;:[991,3],&quot;itemsTablet&quot;:[767,1],&quot;itemsTabletSmall&quot;:false,&quot;itemsMobile&quot;:[479,1]}">
                                                                        <div class="iw-work-step  style2">

                                                                        </div>
                                                                        <div class="iw-work-step  style2">

                                                                        </div>
                                                                        <div class="iw-work-step  style2">
                                                                        </div>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <div class="wpb_column vc_column_container vc_col-sm-2 vc_hidden-sm vc_hidden-xs">
                                                        <div class="vc_column-inner">
                                                            <div class="wpb_wrapper">

                                                            </div>


                                                        </div>

                                                    </div>

                                                </div>

                                            </div>

                                        </div>

                                    </div>

                                </div>

                            </div>

                        </div>
                        <!-- Slider Ends -->
                        <div class="vc_row wpb_row vc_inner vc_row-fluid" style="margin: -65px;"><div class="wpb_column vc_column_container vc_col-sm-12"><div class="vc_column-inner"><div class="wpb_wrapper">    <div class="iwj-categories style1">
                            <div class="item-category">
                        <div class="item-category-inner">
                            <span class="category-icon"><i class="fa fa-briefcase" aria-hidden="true"></i></span>
                            <h3 class="category-title"><a onclick="searchissues(this.innerHTML)">Guidance and career</a></h3>
                            
                        </div>
                    </div>
                                    <div class="item-category">
                        <div class="item-category-inner">
                            <span class="category-icon"><i class="fa fa-user" aria-hidden="true"></i></span>
                            <h3 class="category-title"><a onclick="searchissues(this.innerHTML)">Mental health</a></h3>
                            
                        </div>
                    </div>
                                    <div class="item-category">
                        <div class="item-category-inner">
                            <span class="category-icon"><i class="ion-android-color-palette"></i></span>
                            <h3 class="category-title"><a onclick="searchissues(this.innerHTML)">Rehabilitation</a></h3>
                           
                        </div>
                    </div>
                                    <div class="item-category">
                        <div class="item-category-inner">
                            <span class="category-icon"><i class="fa fa-users" aria-hidden="true"></i></span>
                            <h3 class="category-title"><a onclick="searchissues(this.innerHTML)">Marriage and family</a></h3>
                            
                        </div>
                    </div>
                                    <div class="item-category all-categories">
                        <div class="item-category-inner">
                            <h3 class="category-title"><a onclick="searchissues(this.innerHTML)">All Categories</a></h3>
                        </div>
                    </div>
                        </div>
                    </div></div></div></div>
                        <!-- How it works Section -->

                        <div class="vc_row wpb_row vc_row-fluid vc_custom_1501836183839">
                            <div class="container">
                                <div class="row">
                                    <div class="wpb_column vc_column_container vc_col-sm-12">
                                        <div class="vc_column-inner">
                                            <div class="wpb_wrapper">
                                                <div class="iw-heading  style1 vc_custom_1501836220622 text-center border-color-theme" style="width: 100%">
                                                    <h3 class="iwh-title" style="">How it works </h3>
                                                    <div class="iwh-description" style="">Help yourself, help your friends</div>
                                                </div>
                                                
                                                
                                                <div class="iw-work-steps">
                                                    <div class="owl-carousel" data-plugin-options="{&quot;navigation&quot;:false,&quot;autoHeight&quot;:false,&quot;pagination&quot;:false,&quot;autoPlay&quot;:true,&quot;paginationNumbers&quot;:false,&quot;items&quot;:3,&quot;itemsDesktop&quot;:[1199,3],&quot;itemsDesktopSmall&quot;:[991,3],&quot;itemsTablet&quot;:[767,1],&quot;itemsTabletSmall&quot;:false,&quot;itemsMobile&quot;:[479,1]}">
                                                        <div class="iw-work-step  style1">
                                                            <div class="icon" style="background:#2980b9; -webkit-box-shadow: 0 5px 25px 0 #2980b9; box-shadow: 0 5px 25px 0 #2980b9; border-radius: 50%;border: 8px solid #2980b9;">
                                                                <span class="count-step" style="color:#2980b9">1</span>
                                                                <img src="assets/images/user.svg" height="75px" width="75px" style="padding: 10px 10px">
                                                                
                                                            </div>
                                                            <div class="info-wrap"><h3 class="title">Pre-pay</h3>
                                                                <p class="description"> Buy code or credits online using card/net banking etc</p>
                                                            </div>
                                                        </div>
                                                        <div class="iw-work-step  style1">
                                                            <div class="icon" style="background:#16a085; -webkit-box-shadow: 0 5px 25px 0 #16a085; box-shadow: 0 5px 25px 0 #16a085; border-radius: 50%;border: 8px solid #16a085;">
                                                                <span class="count-step" style="color:#16a085">2</span>
                                                                <img src="assets/images/schedule.svg" height="75px" width="75px" style="padding: 10px 10px">
                                                                
                                                            </div>
                                                           
                                                            <div class="info-wrap"><h3 class="title">Schedule appointment</h3>
                                                                <p class="description"> Pick your convenient time and schedule an online appointment</p>
                                                            </div>
                                                        </div>
                                                        <div class="iw-work-step  style1">
                                                            <div class="icon" style="background:#e67e22; -webkit-box-shadow: 0 5px 25px 0 #e67e22; box-shadow: 0 5px 25px 0 #e67e22; border-radius: 50%;border: 8px solid #e67e22;">
                                                                <span class="count-step" style="color:#e67e22">3</span>
                                                                <img src="assets/images/recharge.svg" height="75px" width="75px" style="padding: 10px 10px">
                                                                
                                                            </div>
                                                           
                                                            <div class="info-wrap">
                                                                <h3 class="title">Recharge your life</h3>
                                                                <p class="description"> Our system will call and connect you to the Counsellor at the scheduled time automatically </p>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <!-- How it works Ends -->

                        <!-- Anonymous Scheduling Section -->
                        <div class="vc_row wpb_row vc_row-fluid vc_custom_1502072802630 vc_row-has-fill text-center" id="anonymous_scheduling" style="background-image: url('assets/images/anon.png');background-repeat: no-repeat;background-position:top; ">
                            <div class="container" style="width:93%">
                                <div class="row">
                                    <div class="wpb_column vc_column_container vc_col-sm-6" >
                                        <div class="vc_column-inner">
                                            <div class="wpb_wrapper">
                                                <div  class="wpb_single_image wpb_content_element vc_align_center">

                                                    <!--<figure class="wpb_wrapper vc_figure">
                                                        <div class="vc_single_image-wrapper   vc_box_border_grey">
                                                          <!--  <p><iframe src="https://player.vimeo.com/video/64760211" width="350" height="288" frameborder="0" title="Montreal By Winter" webki width="513" height="288" ftallowfullscreen mozallowfullscreen allowfullscreen></iframe></p>
                                                          <p><iframe class="embed-responsive-item" src="https://player.vimeo.com/video/257780866" width="450" height="288" frameborder="0" webkitallowfullscreen mozallowfullscreen allowfullscreen></iframe></p></p>

                                                        </div>
                                                    </figure>-->

                                                    <div class="embed-responsive embed-responsive-16by9">
                                                        <iframe class="embed-responsive-item" src="https://player.vimeo.com/video/257780866"></iframe>
                                                      </div>    


                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="wpb_column vc_column_container text-center">
                                        <div class="vc_column-inner vc_custom_1507100776832">
                                            <div class="wpb_wrapper">
                                                <div class="wpb_text_column wpb_content_element" >
                                                    <div class="wpb_wrapper">
                                                        <div class="how-it-work left">
                                                            <div class="iw-title">Anonymous Scheduling</div>
                                                            <div class="iw-desc" style="text-align:justify">Counselling sessions are done by trained experts. We will connect your counsellor to you via phone.
                                                            You don't even have to share your phone number with the counsellor. No one else will contact you at your phone number.
                                                            Our system will initiate the call between you and the counsellor at your convenient time. And the session will be private;
                                                            not even accessible to us. But, don't worry if you are having technical trouble during the call. You can always reach us at our number. We respect your privacy. 

                                                            </div>

                                                        </div>
                                                        <?php if ($this->session->userdata('loggedIn') == true) { ?> 
                                                        <a style="margin-top: 40px;padding: 10px 50px;" onclick="location.href = '<?php echo base_url(); ?>admin/credits';" name="register" class="iwj-btn iwj-btn-primary iwj-btn-medium  iwj-register-btn">BUY A CODE</a>
                                                        <?php }else { ?>
                                                        <a style="margin-top: 40px;padding: 10px 50px;" onclick="location.href = '<?php echo base_url(); ?>home/purchasecode';" name="register" class="iwj-btn iwj-btn-primary iwj-btn-medium  iwj-register-btn">BUY A CODE</a>
                                                        <?php } ?> 
                                                        <a style="margin-top: 40px;margin-left:10px;padding: 10px 50px; color: #ff8e16;background-color:#fff;" onclick="location.href = '<?php echo base_url(); ?>home/schedule';" class="iwj-btn  iwj-btn-medium  iwj-register-btn">SCHEDULE</a>

                                                        

                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <!-- Anonymous Scheduling Ends -->


                        <!-- Corporate Users Section -->
                        <div class="vc_row wpb_row vc_row-fluid vc_custom_1502076897543 vc_row-has-fill">
                            <div class="container" style="width:93%">
                                <div class="row">

                                    <div class="wpb_column vc_column_container vc_col-sm-6 text-center">
                                        <div class="vc_column-inner vc_custom_1507100776832">
                                            <div class="wpb_wrapper">
                                                <div class="wpb_text_column wpb_content_element " >
                                                    <div class="wpb_wrapper">
                                                        <div class="how-it-work left">
                                                            <div class="iw-title" style="color: #ff8e16 !important; margin-top:30px;">Corporates or Institutions</div>
                                                            <div class="iw-desc" style="text-align:justify">As a Corporation or an Institution, you can purchase codes from us and distribute it to your employees or students.
                                                            Each individual will have a unique code with which they can schedule a personal session with their counsellor at their convenience and privacy.
                                                            Even outside of normal business hours. 
                                                            </div>
                                                            <?php if ($this->session->userdata('loggedIn') == true) { ?> 
                                                            <a style="margin-top: 40px;padding: 7px 22px;" onclick="location.href = '<?php echo base_url(); ?>admin/credits';" name="register" class="iwj-btn iwj-btn-primary iwj-btn-medium  iwj-register-btn cop-btn">Purchase A Code</a>
                                                            <?php }else { ?>
                                                            <a style="margin-top: 40px;padding: 7px 22px;" onclick="location.href = '<?php echo base_url(); ?>home/purchasecode';" name="register" class="iwj-btn iwj-btn-primary iwj-btn-medium  iwj-register-btn cop-btn">Purchase A Code</a>
                                                            <?php } ?>
                                                            
                                                        </div>
                                                        
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="wpb_column vc_column_container vc_col-sm-6">
                                        <div class="vc_column-inner">
                                            <div class="wpb_wrapper">
                                                <div  class="wpb_single_image wpb_content_element vc_align_center">

                                                    <figure class="wpb_wrapper vc_figure">
                                                        <div class="vc_single_image-wrapper   vc_box_border_grey"><img width="560" height="394" src="uploads/2017/10/Group-19.png" class="vc_single_image-img attachment-full" alt="" srcset="uploads/2017/10/Group-19.png 560w, uploads/2017/10/Group-19-300x211.png 300w" sizes="(max-width: 560px) 100vw, 560px" /></div>
                                                    </figure>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <!-- Corporate Users Ends -->

                        <!-- Blog Section -->

                        <div class="vc_row wpb_row vc_row-fluid vc_custom_1501833554275">
                            <div class="container">
                                <div class="row">
                                    <div class="wpb_column vc_column_container vc_col-sm-12">
                                        <div class="vc_column-inner">
                                            <div class="wpb_wrapper">
                                                <div class="iw-heading  style1 vc_custom_1501832143838 text-center border-color-theme" style="width: 100%">
                                                    <h3 class="iwh-title" style="">Blog</h3>

                                                </div>					
                                                <div class="iw-posts-2  style2">
                                                    <div class="iw-posts-list row">
                                                        <div class="col-md-4 col-sm-6 col-xs-12 element-item">
                                                            <div class="post-item">
                                                                <div class="post-image">
                                                                    <span class="post-category theme-bg"><a onclick="window.open('http://dev.eyogi.in/blog/index.php/category/eyogi-news-portal/')">eYogi News Portal</a></span>
                                                                </div>
                                                                <div class="post-content">
                                                                    <!--                                                <span class="post-date">--><!--</span>-->
                                                                    <h3 class="post-title">
                                                                        <a onclick="window.open('http://dev.eyogi.in/blog/index.php/2018/03/23/introducing-eyogi-in-a-new-mental-wellness-startup-from-kerala/')">Introducing eYogi.in – A new age Mental Wellness Startup from Kerala</a>
                                                                    </h3>
                                                                    <div class="post-description">
                                                                        A high priority challenge for parents and teachers in Kerala these days is the lack of interest shown by children  in their studies and the high interest and time spent by them on social media networks. The addiction to social media results in attention deficit disorders and the lack of enthusiasm and focus leads to learning disabilities.                                              </div>
                                                                    <div class="post-bottom">
                                                                        <span class="post-author">
                                                                            By: <span><a href="" title="Posts by admin" rel="author">admin</a></span>
                                                                        </span>
                                                                        <a class="post-read-more theme-color" onclick="window.open('http://dev.eyogi.in/blog/index.php/2018/03/23/introducing-eyogi-in-a-new-mental-wellness-startup-from-kerala/')">Read more<i class="ion-arrow-right-c"></i></a>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                        </div>
                                                        <div class="col-md-4 col-sm-6 col-xs-12 element-item">
                                                            <div class="post-item">
                                                                <div class="post-image">
                                                                    <span class="post-category theme-bg"><a onclick="window.open('http://dev.eyogi.in/blog/index.php/category/eyogi-news-portal/')">eYogi News Portal</a></span>
                                                                </div>
                                                                <div class="post-content">
                                                                    <!--                                                <span class="post-date">--><!--</span>-->
                                                                    <h3 class="post-title">
                                                                        <a onclick="window.open('http://dev.eyogi.in/blog/index.php/2018/03/28/media-coverage-eyogi-in-incubated-at-iimk-live/')">Media Coverage: eYogi.in incubated at IIMK LIVE</a>
                                                                    </h3>
                                                                    <div class="post-description" style="padding-bottom: 12px">
                                                                        Our association with Indian Institute of Management Kozhikode (IIMK) Live Business Incubation facility has got us some media attention recently. The article by "The SME Times"? www.TheSMETimes.com talks about how it all started as an Academic project at IIMK where our founders were doing Executive PGP to how it took shape to something that addresses a real need in the society.                                                </div>
                                                                    <div class="post-bottom">
                                                                        <span class="post-author">
                                                                            By: <span><a onclick="window.open('http://dev.eyogi.in/blog/index.php/2018/03/28/media-coverage-eyogi-in-incubated-at-iimk-live/')" title="Posts by admin" rel="author">admin</a></span>
                                                                        </span>
                                                                        <a class="post-read-more theme-color" onclick="window.open('http://dev.eyogi.in/blog/index.php/2018/03/28/media-coverage-eyogi-in-incubated-at-iimk-live/')">Read more<i class="ion-arrow-right-c"></i></a>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                        </div>
                                                        <div class="col-md-4 col-sm-6 col-xs-12 element-item">
                                                            <div class="post-item">
                                                                <div class="post-image">
                                                                    <span class="post-category theme-bg"><a onclick="window.open('http://dev.eyogi.in/blog/index.php/category/research-studies/')">Research & Studies</a></span>
                                                                </div>
                                                                <div class="post-content">
                                                                    <!--                                                <span class="post-date">--><!--</span>-->
                                                                    <h3 class="post-title">
                                                                        <a onclick="window.open('http://dev.eyogi.in/blog/index.php/2018/03/28/cell-phones-multitasking-and-stress/')">Cell Phones, Multitasking and Stress</a>
                                                                    </h3>
                                                                    <div class="post-description" style="padding-bottom: 12px">
                                                                        We all think that when we are "multitasking"?, we are getting more things done at once. But, the truth is, it adds stress to our lives and without us knowing about it, we end up making mistakes in many of these tasks we do.   Missing attachments in sent emails, shouting at a colleague without a reason or that one important activity taking too long than what you expected. Sounds familiar?                                      </div>
                                                                    <div class="post-bottom">
                                                                        <span class="post-author">
                                                                            By: <span><a onclick="window.open('http://dev.eyogi.in/blog/index.php/2018/03/28/cell-phones-multitasking-and-stress/')" title="Posts by admin" rel="author">admin</a></span>
                                                                        </span>
                                                                        <a class="post-read-more theme-color" onclick="window.open('http://dev.eyogi.in/blog/index.php/2018/03/28/cell-phones-multitasking-and-stress/')">Read more<i class="ion-arrow-right-c"></i></a>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>

                        </div>

                        <!-- Blog Ends -->


                        <div class="vc_row wpb_row vc_row-fluid vc_custom_1502174616606" style="background-color: #FBF0E8;background-image: url('assets/images/contactus_paperplane.png');">
                            <div class="container">
                                <div class="row">
                                    <div role="form" style="width:75%; margin:0 auto; background:none!important;" class="wpcf7" id="wpcf7-f157-p155-o1" lang="en-US" dir="ltr">
                                        <div class="screen-reader-response">

                                        </div>
                                        <form action="<?php echo site_url('home/contactform'); ?>" method="post" name="contact5" class="" id="contactForm5" novalidate="novalidate">
                                      
                                            
                                            <div style="box-shadow:none;background:none!important;" class="form-wrapper iw-contact-form-7 contact-map">
                                                <div class="title">
                                                    Contact
                                                </div>
                                                <div id="succes" class="alert alert-success" style="display: none;"></div>
                                                <input type="hidden" value="index" name="page_type">
                                                <div class="field" style="background-color: white;">
                                                    <label>Email?</label>
                                                    <div class="iw-input"><i class="fa fa-user"></i><span class="wpcf7-form-control-wrap your-email">
                                                            <input type="email" name="email14" id="email14" value="" size="30" class="wpcf7-form-control wpcf7-text wpcf7-email wpcf7-validates-as-required wpcf7-validates-as-email" aria-required="true" aria-invalid="false" placeholder="Enter your Email address." /></span></div>
                                                </div>
                                                <div class="field" style="background-color: white;">
                                                    <label>Subject?</label>
                                                    <div class="iw-input"><i class="fa fa-edit"></i> <span class="wpcf7-form-control-wrap your-subject">
                                                    <input type="text" name="subject14" id="subject14" value="" size="30" class="wpcf7-form-control wpcf7-text wpcf7-validates-as-required" aria-required="true" aria-invalid="false" placeholder="Enter subject" /></span></div>
                                                </div>
                                                <input type="hidden" id="base5" value="<?php echo base_url();?>">
                                                <div class="field" style="background-color: white;">
                                                    <label>Write to us!</label>
                                                    <div class="iw-input"><i class="fa fa-keyboard-o"></i> <span class="wpcf7-form-control-wrap your-message">
                                                    <textarea name="message14" id="message14" cols="30" rows="4" class="wpcf7-form-control wpcf7-textarea wpcf7-validates-as-required" aria-required="true" aria-invalid="false" placeholder="We will get back to you as soon as possible."></textarea></span></div>
                                                </div>
                                                <div class="submit-button"><input type="submit" value="Send" class="wpcf7-form-control" /></div>
                                            </div>
                                            
                                            
                                        
                                        </form>


                                    </div>
                                </div>
                            </div>

                        </div>
                    </div><!-- .entry-content -->
                    <div class="clearfix"></div>



                    <footer class="entry-footer">
                    </footer><!-- .entry-footer -->
                </article><!-- #post-## -->
            </div>

<div id="searchk" class="modal-popup modal fade iwj-register-form-popup">
    <div class="modal-dialog">
        <div class="modal-header">
            <h4 class="modal-title">Search</h4>
            <button type="button" id="btn_modal" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
        </div>
        <div class="modal-content">

        <form action="<?php echo site_url('Home/searchissues'); ?>" method="POST" class="iwj-login-form1 iwj-form" name="searchform1" id="searchform1" > 

                             <div class="iwjmb-field iwjmb-text-wrapper  required"><div class="iwjmb-label"><label class="theme-color" for="_iwj_headline">Issues *</label></div>
                         <div class="iwjmb-input ui-sortable">
<select multiple data-options="{&quot;allowClear&quot;:false,&quot;width&quot;:&quot;none&quot;,&quot;placeholder&quot;:&quot;&quot;,&quot;minimumResultsForSearch&quot;:-1}" required  class="iwjmb-select_advanced" name="domain1[]" id="domain1">
                                                                
                               <?php  foreach ($counselling_types as $type) { ?>
                            <option value="<?=$type->type?>"><?=$type->type?></option>
                        <?php } ?>
                     </select>
                                                           
                 </div>
            </div> 




                <div class="iwj-respon-msg iwj-hide"></div>
                <div class="iwj-button-loader">

                    <!--   <button type="button" id="btn_issuessearch12" name="btn_issuessearch" class="iwj-btn iwj-btn-primary iwj-btn-full iwj-btn-medium iwj-register-btn">Submit</button> -->
<input type="submit" value="submit" name="">



                </div>


            </form>

        </div>
    </div>
</div>

<script type="text/javascript">
    
//    $(function() {
//
//$('#searchk').modal('show'); 
//
//
//     });


</script>

<script type="text/javascript">

  function searchissues(val){

      $.ajax({

                url: '<?php echo base_url(); ?>home/searchissues',
                data: {domain1: val},
                type: 'post',
                //dataType: 'json',
                success: function (data) {
                    //console.log(data);
                //    alert(data);

                if (data == 1) {
                    window.location.href = '<?php echo base_url(); ?>home/schedule';
                }

                  
                }
            }); 
  }

</script>

<?php $this->load->view('layout/footer.php') ?>