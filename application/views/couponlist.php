<?php if ($this->session->userdata('logged_in')){ $this->load->view('layout/header.php') ?>

<div class="page-heading default">
    <div class="container-inner">
        <div class="container">
            <div class="page-title">
                <div class="iw-heading-title">
                    <h1 style="color:#894724">
                       List of coupons created by Admin</h1>

                </div>
            </div>
        </div>
    </div>
   
        </div>

        <style type="text/css">
            
            #d1_wrapper{
                background-color: white;
                padding: 30px 30px;
            }
        </style>

        <div class="iwj-table-overflow-x container" style="padding-top:20px;padding-bottom: 20px">
            <a href="<?php echo base_url(); ?>home/coupon_create" class="iwj-btn iwj-btn-primary" style="margin-bottom:20px;">Create coupon</a>
            
            <table class="table userlist" cellspacing="0" width="100%" id="d1" >

                                <thead>
                                        <tr>
                                     
                                            
                                           
                                            <th width="">Visitor Info</th>
                                            <th width="">Amount</th>
                                            <th width="">Coupon Code</th>
                                            <th width="">Purchase_date </th>
                                            <th width="">Coupon_balance</th>
                                            
                                            
                                        </tr>
                                    </thead>

                                      <tbody>
                                        <?php  foreach ($couponlist as $note) { ?>
                                        <tr>
                                            
                                            <td>
                                                <?php echo $note->visiter_info; ?>
                                            </td>
                                            <td>
                                                <?php echo $note->coupon_amount; ?>
                                            </td>
                                            <td>
                                                <?php echo $note->coupon_code; ?>
                                            </td>
                                            <td>
                                                <?php echo $note->purchase_date; ?>
                                            </td>
                                            <td>
                                                <?php echo $note->bal; ?>
                                            </td>
                                            
                                        </tr>
                                        <?php } ?>
                                      </tbody>

                   </table>
             

             
                </div>  
       
<?php $this->load->view('layout/footer.php');}else{ ?> 
<script type="text/javascript">window.location.href = '<?php echo base_url() ?>'+'home/coupon_login';</script>  <?php } ?>              