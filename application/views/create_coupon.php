<?php if ($this->session->userdata('logged_in')){  $this->load->view('layout/header.php') ?>

<div class="page-heading default">
    <div class="container-inner">
        <div class="container">
            <div class="page-title">
                <div class="iw-heading-title">
                    <h1 style="color:#894724">
                        Create Coupon                            </h1>

                </div>
            </div>
        </div>
    </div>
   
        </div>
            <div class="contents-main" style="background-color: #f2f2f1" id="contents-main">
                <div class="container">
                    <div class="row">
                        <div class="col-sm-12 col-xs-12">
                            <form action="<?php echo site_url('home/coupon_create'); ?>" method="post" class="iwj-login-form1 iwj-form" name="coupon11" id="couponform" style="padding-top:20px" novalidate="novalidate">
                                                  <a href="<?php echo site_url('home/getCouponlist'); ?>"  style="float:right">View Coupon list</a>                      
                                <div class="form-wrapper iw-contact-form-7 contact-map" style="box-shadow: none;">
                                    <div class="title">
                                        <h3>Create Coupon</h3>
                                        
                                    </div>
                                    <?php if(isset($_GET['msg'])) { ?> 
                                    <div class="alert alert-success">
                                        <strong>Success!</strong> We shall contact you soon.
                                    </div>
                                                                            
                                    <?php } ?> 
                                     <input type="hidden" name="baseurl12" id="baseurl12" value="<?php echo base_url(); ?>">
                                    <div class="field">
                                        <label>Coupon Amount</label>
                                        <div class="iw-input"><i class="fa fa-user"></i> <span class="wpcf7-form-control-wrap your-email"><input type="text" name="amount12" id="amount12" value="" size="40" class="wpcf7-form-control wpcf7-text wpcf7-email wpcf7-validates-as-required wpcf7-validates-as-email" aria-required="true" aria-invalid="false" placeholder="Enter Amount." /></span></div>
                                   </div>
                                
                          
                                    <div class="submit-button"><input type="submit" value="Create" class="wpcf7-form-control wpcf7-submit" /></div>
                                </div>
                                <div id="succes"  class="alert alert-success" style="width: 50%; margin-left: 49%;display: none;">  </div>  
                                                                        
                            </form>
<!--                <script id="bx24_form_inline" data-skip-moving="true">
                    (function (w, d, u, b) {
                        w['Bitrix24FormObject'] = b;
                        w[b] = w[b] || function () {
                            arguments[0].ref = u;
                            (w[b].forms = w[b].forms || []).push(arguments[0])
                        };
                        if (w[b]['forms'])
                            return;
                        var s = d.createElement('script');
                        s.async = 1;
                        s.src = u + '?' + (1 * new Date());
                        var h = d.getElementsByTagName('script')[0];
                        h.parentNode.insertBefore(s, h);
                    })(window, document, 'https://eyogi.bitrix24.in/bitrix/js/crm/form_loader.js', 'b24form');

                    b24form({"id": "10", "lang": "en", "sec": "9nozcu", "type": "inline"});
                </script>-->
            </div>
        </div>
    </div>
</div>
<?php $this->load->view('layout/footer.php');}else{ ?> 
<script type="text/javascript">window.location.href = '<?php echo base_url() ?>'+'home/coupon_login';</script>  <?php } ?>