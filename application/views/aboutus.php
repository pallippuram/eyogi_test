<?php $this->load->view('layout/header.php') ?>
<div class="page-heading default">
                <div class="container-inner">
                    <div class="container">
                        <div class="page-title">
                            <div class="iw-heading-title">
                                <h1 style="color:#894724">
                                   About Us                         </h1>

                            </div>
                        </div>
                    </div>
                </div>
           
            </div>
            <div class="contents-main" id="contents-main">

                <article id="post-3117" class="post-3117 page type-page status-publish hentry">
                    <div class="entry-content">
                        
                        <div class="vc_row wpb_row vc_row-fluid vc_custom_1507100241663" style="padding-bottom:20px">
                            <div class="container" style="width:80%">
                                <div class="row">
                                    <div class="wpb_column vc_column_container vc_col-sm-12" style="background-color: #fff">
                                        <div class="vc_column-inner"><div class="wpb_wrapper">
                                                <div class="wpb_text_column wpb_content_element" >
                                                    <div class="wpb_wrapper wpb_wrapper_div">
                                                        <div class="how-it-work">
                                                            <div class="iw-title1" style="text-transform:uppercase;font-size:15px;color:#eea461;letter-spacing:3px;margin-top: 50px"><b>eYogi.in - About Us
                                                            </b></div>
                                                            <div class="iw-desc">
                                                                <p>   
                                                                    In India, mental wellness is one area which requires huge investments due to the lack of infrastructure and trained professionals.
                                                                    Bringing technology assisted services in this area is the need of the hour. We are looking at bringing innovative methods to handle this very need.
                                                                    We have taken our first step towards this by introducing a platform that connects our clients to counsellors using telephone.
                                                                </p>
                                                                <p>
                                                                    eYogi.in is founded by three IIM Kozhikode, Kochi campus Executive MBA graduates and is incubated at IIMK LIVE Incubator.
                                                                    The firm started as an academic project has evolved into a full-fledged company with the assistance from IIMK LIVE and is operating from IIMK LIVE and Nasscom 10,000 Startup Warehouse in Kochi, Kerala.
                                                                    We want to address the mental wellness area using new technology solutions to address the lack of trained experts, lack of availability of these trained counselors to the needy due to the limited infrastructure and the social stigma associated with mental health issues.
                                                                    eYogi.in intent to use latest technology solutions such as Artificial intelligence, Machine Learning, Mobility, Wearable devices etc to augment the availability of trained experts and to make their services available to the nook and corner of the country.
                                                                    
                                                                </p>
                                                                
                                                                <p>
                                                                    eYogi.in has also entered into a strategic alliance with SEDDAC, a multifaceted NGO which is operating from Kochi for more than a decade.
                                                                </p>

                                                            </div>
                                                        </div>

                                                    </div>
                                                </div>
                                            </div>

                                        </div>

                                    </div>
                                    
                                </div>
                            </div>
                        </div>

                       
                       

                    </div><!-- .entry-content -->
                    <div class="clearfix"></div>
                    <footer class="entry-footer">
                    </footer><!-- .entry-footer -->
                </article><!-- #post-## -->
            </div>
<?php $this->load->view('layout/footer.php') ?>
