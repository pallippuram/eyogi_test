<?php $this->load->view('layout/header.php') ?>
<div class="page-heading default">
                <div class="container-inner">
                    <div class="container">
                        <div class="page-title">
                            <div class="iw-heading-title">
                                <h1 style="color:#894724">
                                   Refund Policy                         </h1>

                            </div>
                        </div>
                    </div>
                </div>
           
            </div>
            <div class="contents-main" id="contents-main">

                <article id="post-3117" class="post-3117 page type-page status-publish hentry">
                    <div class="entry-content">
                        
                        <div class="vc_row wpb_row vc_row-fluid vc_custom_1507100241663" style="padding-bottom:20px">
                            <div class="container" style="width:80%">
                                <div class="row">
                                    <div class="wpb_column vc_column_container vc_col-sm-12" style="background-color: #fff">
                                        <div class="vc_column-inner"><div class="wpb_wrapper">
                                                <div class="wpb_text_column wpb_content_element" >
                                                    <div class="wpb_wrapper wpb_wrapper_div">
                                                        <div class="how-it-work">
                                                            <div class="iw-title1" style="text-transform:uppercase;font-size:15px;color:#eea461;letter-spacing:3px;margin-top: 50px"><b>eYogi.in - Refund and Cancellation Policy
</b></div>
                                                            <div class="iw-desc">
                                                                <p>   eYogi.in is an online wellness platform catering to improving your mental wellness which results in improvement in life and happiness. A happy and focussed mind achieves more and results in overall improvement in day to day life, career, family life and helps to improve the financial health along with the physical health and wellbeing of the person.</p><br

<p>eYogi.in is a customer first company. Our motto is to bring mental health and wellness to the needy by removing the social stigma and overcoming the infrastructure limitations. All our counsellors dedicate their time studying and preparing for your consultation process. We are trying to improve your experience continuously and your compliance to the scheduled appointment and suggestions by the counsellor help to achieve better results.</p>

<p>We will provide refund under the following conditions.
<br>
<ol>
<li>If you are not satisfied with your counselling sessions even after changing the counsellor once, you can ask for a refund of the balance available in your Purchased Code/Credits purchased.</li>
<li>If your counselling session was disconnected within 5 minutes after starting the call. If the session has progressed for more than 5 minutes, no refund will be applicable.</li>
<li>If there were technical issues during the session and you report about that to us within 24 hours. We will validate your claim and if there were technical issues due to the eYogi.in platform, we will recharge your code/credits with the amount that was consumed during the session which had technical problems. </li></p>
</ol>
<p>There will be no refund for the following situations.
    <ol>
       <li> Purchased coupons which were not used/consumed within 6 months from the purchase date.</li>
<li>The customer didn't turn up for the session after scheduling an appointment.</li>
<li>Any other situations arising out of low network coverage or any other challenges beyond our control.</li>
    </ol>
</p>

<p>For any issues related with refund or cancellation, send your queries to <b>support@eyogi.in</b> and we will review and update you based on the merit of the case.</p>

<p>Refunds (if applicable)
Once your return request is received and inspected, we will send you an email to notify you that we have received your request. We will also notify you of the approval or rejection of your refund.
If you are approved, then your refund will be processed, and a credit will automatically be applied to your credit card or original method of payment, within a certain amount of days.
</p>
<p>Late or missing refunds (if applicable)
If you haven’t received a refund yet, first check your bank account again.
Then contact your credit card company, it may take some time before your refund is officially posted.</p>


<p>Next contact your bank. There is often some processing time before a refund is posted.
If you’ve done all of this and you still have not received your refund yet, please contact us at <b>support@eyogi.in</b></p>



</p>

                                                            </div>
                                                        </div>

                                                    </div>
                                                </div>
                                            </div>

                                        </div>

                                    </div>
                                    
                                </div>
                            </div>
                        </div>

                       
                       

                    </div><!-- .entry-content -->
                    <div class="clearfix"></div>
                    <footer class="entry-footer">
                    </footer><!-- .entry-footer -->
                </article><!-- #post-## -->
            </div>
<?php $this->load->view('layout/footer.php') ?>