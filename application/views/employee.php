<?php $this->load->view('layout/header.php') ?>
<style>
    .btn-primary{
        width:20px
    }
    .ui-timepicker-wrapper{
        width:143px
    }
    .ui-timepicker-disabled{
        display: none
    }
</style>
<div class="page-heading default">
    <div class="container-inner">
        <div class="container">
            <div class="page-title">
                <div class="iw-heading-title">
                    <h1 style="color:#894724">

                        <?php echo $data2[0]->username; ?> </h1>

                </div>
            </div>
        </div>
    </div>
  
</div>
<div class="contents-main" id="contents-main" style="transform: none;">
    <div class="container" style="transform: none;">
        <div class="row" style="transform: none;">
            <div class="col-sm-12 col-xs-12" style="transform: none;">

                <article id="post-143" class="post-143 page type-page status-publish hentry" style="transform: none;">
                    <div class="entry-content" style="transform: none;">

                        <div class="iwj-employer-detail" style="transform: none;">
                            <div class="container" style="transform: none;">
                                <div class="row" style="transform: none;">
                                    <div class="col-sm-12 col-xs-12 col-lg-8 col-md-8">
                                        <div class="iwj-employerdl-content">

                                            <div class="employer-info-top">
                                                <div class="employer-logo">

                                                    <img style="border-radius:50%;" alt="AOEVN" src='<?php
                                                    if ($data2[0]->photo != "") {
                                                        echo $data2[0]->photo;
                                                    } else {
                                                        echo base_url() . "uploads/images/user.jpg";
                                                    }
                                                    ?>' class="avatar avatar-96 photo" width="150" height="150">								<div class="social-link">
                                                    </div>
                                                </div>
                                                <div class="conttent-right">
                                                    <div class="title-location">
                                                        <h3 class="title"><?php echo $data2[0]->username ?></h3>

                                                    </div>
                                                    <div class="employer-info">
                                                        <ul>
                                                            <li class="employer-phone">
                                                                <i class="fa fa-suitcase"></i>
                                                                <?php
                                                                $arr = json_decode($data2[0]->domain);
                                                                if (!empty($arr)) {
                                                                    echo implode(',  ', $arr);
                                                                } else {
                                                                    echo $data2[0]->domain;
                                                                }
                                                                ?> 


                                                            </li>
                                                            <li class="employer-address">
                                                                <i class="fa fa-cogs"></i><?php echo $data2[0]->experience ?> years Exp
                                                            </li>
                                                            <li class="employer-email">
                                                                <i class="fa fa-inr"></i><?php echo $data2[0]->amount ?>
                                                            </li>
                                                            <li class="employer-website">
                                                                <i class="fa fa-american-sign-language-interpreting"></i><?php
                                                                $arr = json_decode($data2[0]->languages);
                                                                $ord = array_map('ucfirst', $arr);
                                                                echo implode(',  ', $ord);
                                                                //echo implode(',  ', $arr);
                                                                ?>
                                                            </li>
                                                            <li class="employer-website">
                                                                <i class="fa fa-calendar"></i>Available on <?php
                                                                $arr1 = json_decode($data2[0]->days);
                                                                echo implode(',  ', $arr1);
                                                                ?>
                                                            </li>
                                                        </ul>
                                                    </div>
                                                </div>
                                            </div>


                                            <!--<div class="employer-detail-info">
                                                <div class="iwj-employerdl-des">
                                                    <div class="title"><h3>Company Detail</h3></div>
                                                    <div class="content"><p>If you feel you have the skills and experience needed for this great opportunity, please forward a copy of your CV to or call Suj on for more information</p>
                                                    </div>
                                                </div>
                                            </div>-->
                                            <div class="employer-detail-info">
                                                <div class="iwj-employerdl-des">
                                                    <div class="title"><h3>Qualifications</h3></div>
                                                    <div class="content"><p><?php echo $data2[0]->certifications ?></p>
                                                    </div>

                                                    <div class="title"><h3>Accreditations</h3></div>
                                                    <div class="content"><p><?php echo $data2[0]->accrediations ?></p>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="iwj-sidebar-sticky col-sm-12 col-xs-12 col-lg-4 col-md-4" style="position: relative; overflow: visible; box-sizing: border-box; min-height: 1px;">

                                        <div class="theiaStickySidebar" style="padding-top: 0px; padding-bottom: 1px; position: static; transform: none; top: 0px; left: 899.5px;"><div class="widget-area" role="complementary">

                                                <aside id="iwj_employer_contact_form-2" class="widget widget_iwj_employer_contact_form"><h3 class="widget-title"><span>Schedule Appointment</span></h3>
                                                    <div class="iwj-employer-widget-wrap">
                                                        <div class="iwj-single-contact-form iwj-single-widget">
                                                            <form class="iwj-contact-form" action="#" method="post" enctype="multipart/form-data">

                                                                <input type="hidden" name="couns_id" class="user_id" id="couns_id" value="<?php echo $data2[0]->id ?>">
                                                                <?php
                                                                if ($this->session->userdata('userData')) {
                                                                    $userid = $this->session->userdata('userData');
                                                                    ?>
                                                                    <input type="hidden" name="sche_user" value="<?php echo $userid->id; ?>" id="sche_user">
                                                                    <input type="hidden" name="user_role" value="<?php echo $userid->role_id; ?>" id="user_role">
                                                                <?php } else { ?>
                                                                    <input type="hidden" name="sche_user" value="0" id="sche_user">     
                                                                    <input type="hidden" name="user_role" value="0" id="user_role"> 
                                                                <?php } ?>
                                                                <input type="hidden" name="sche_user" value="0" id="sche_valid">




                                                                <div class="iwjmb-field iwjmb-select_advanced-wrapper  required">
                                                                    <div class="iwjmb-label">
                                                                        <label for="title">Choose Date</label>
                                                                    </div>    
                                                                    <div class="iwjmb-input ui-sortable">
<!--                                                                        <div class="input-group date datetimepicker_re" id="datepicker_emp">-->
                                                                            <input class="form-control datetimepicker_re" type="text" id="datepicker_emp" onkeydown="return false;">
<!--                                                                            <span class="input-group-addon" style="">
                                                                                <span class="glyphicon glyphicon-calendar"></span>
                                                                            </span>-->
<!--                                                                        </div>-->
                                                                    </div>
                                                                    <span style="color:red;display: none;margin-left: 10px;font-weight: bold;">Counsellor not available on selected day!!</span> 
                                                                </div> 

                                                                <div class="iwjmb-field iwjmb-select_advanced-wrapper  required">
                                                                    <div class="iwjmb-label">
                                                                        <label for="frequency">Time Starts</label>
                                                                    </div>
                                                                    <div class="iwjmb-input ui-sortable">
                                                                       <input id="startTime" class="form-control iwjmb-text counselor_availabilty" type="text" onkeydown="return false;"/>
                                                                    </div>
                                                                    <span style="color:red;display: none;margin-left: 20px;font-weight: bold;">This slot is already booked!!</span>
                                                                </div> 
                                                                <input type="hidden" name="employee" value="employee" id="sche_type">
                                                                <div class="iwjmb-field iwjmb-select_advanced-wrapper  required">
                                                                    <div class="iwjmb-label">
                                                                        <label for="frequency">Time Ends</label>
                                                                    </div>
                                                                    <div class="iwjmb-input ui-sortable">
                                                                        <input id="endTime" class="form-control iwjmb-text" type="text" onkeydown="return false;" />
                                                                    </div>
                                                                    <span style="color:red;display: none;margin-left: 20px;font-weight: bold;">This slot is already booked!!</span>
                                                                </div> 
                                                                <div class="iwjmb-field iwjmb-select_advanced-wrapper  required">
                                                                    <div class="iwjmb-label">
                                                                        <label for="description">Note</label>
                                                                    </div>    

                                                                    <div class="iwjmb-input ui-sortable">
                                                                        <textarea style="resize: none;" class="form-control" cols="15" id="user_description" name="description"></textarea>
                                                                    </div>
                                                                    <span style="color:red;display: none;margin-left: 10px;font-weight: bold;">Please Enter Proper Reason!!</span> 
                                                                </div>
                                                                <input type="hidden" name="video_enable" value="<?php echo $data2[0]->video_call; ?>" id="video_enable">
                                                                <?php if($data2[0]->video_call == 1) { ?>
                                                                
                                                                <div class="iwjmb-field iwjmb-select_advanced-wrapper  required">
                                                                    <div class="iwjmb-label">
                                                                        <input style="position:initial !important;" type="radio" name="video-call" id="r1" value="call" checked><strong>       Call Schedule</strong><br>
                                                                        <input style="position:initial !important;" type="radio" name="video-call" id="r2" value="video"><strong>       Video Schedule</strong><br>
                                                                    </div>    
                                                                   
                                                                </div>
                                                                 
                                                                <?php } ?>
                                                                <div>
                                                                    <button  type="button" id="schedule-appointment" class="btn btn-success">Schedule</button>
                                                                </div>   
                                                            </form>
                                                        </div>
                                                    </div>

                                                </aside>
                                            </div></div></div>
                                </div>
                            </div>
                        </div>
                        <input type="hidden" value="<?php 
                        if(!empty($data3))
                        {
                            echo $data3[0]->working_hours;
                        }
                        ?>" id="working_hours">
                    </div><!-- .entry-content -->
                    <div class="clearfix"></div>
                    <footer class="entry-footer">
                    </footer><!-- .entry-footer -->
                </article><!-- #post-## -->
            </div>
        </div>
    </div>
</div>
<script src='<?php echo base_url(); ?>js/jquery.timepicker.js'></script>
<script type="text/javascript">
    var workingHours = $('#working_hours').val();
    var hours = workingHours.split('-');

    var minTime = getTime1(hours[0])
    var maxTime = getTime1(hours[1])
    var now = new Date();
    var mins = now.getMinutes();
    var quarterHours = Math.round(mins / 15);
    if (quarterHours == 4)
    {
        now.setHours(now.getHours() + 1);
    }
    var rounded = (quarterHours * 15) % 60;
    now.setMinutes(rounded);



    $('#startTime').timepicker({

        'maxTime': maxTime,
        'forceRoundTime': true,
        'step': 15,
        dynamic: true,
        'timeFormat': 'h:i A',
        'minTime': formatTime(now),
    });
    $('#endTime').timepicker({
        'minTime': formatTime(now),
        'maxTime': maxTime,
        'showDuration': true,
        'step': 15,
        'timeFormat': 'h:i A',
    });

    function formatTime(dt) {
        return dt.getHours() + ':' + ('0' + dt.getMinutes()).slice(-2) + (dt.getHours() >= 12 ? ' PM' : ' AM')
    }
    function getTime1(time) {
        var time = time;
        var hours = Number(time.match(/^(\d+)/)[1]);
        var minutes = Number(time.match(/:(\d+)/)[1]);
        var AMPM = time.match(/\s(.*)$/)[1];
        if (AMPM == "PM" && hours < 12)
            hours = hours + 12;
        if (AMPM == "AM" && hours == 12)
            hours = hours - 12;
        var sHours = hours.toString();
        var sMinutes = minutes.toString();
        if (hours < 10)
            sHours = "0" + sHours;
        if (minutes < 10)
            sMinutes = "0" + sMinutes;
        var time = sHours + ":" + sMinutes + ":00";

        return  time;
    }
    
    $('#startTime').focus(function() {
        this.blur();
    });

    $('#endTime').focus(function() {
        this.blur();
    });

    $('.datetimepicker_re').focus(function() {
        this.blur();
    });  
</script>
<?php $this->load->view('layout/footer.php') ?>