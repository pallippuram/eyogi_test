<?php $this->load->view('layout/header.php') ?>
<div class="page-heading default">
                <div class="container-inner">
                    <div class="container">
                        <div class="page-title">
                            <div class="iw-heading-title">
                                <h1 style="color:#894724">
                                    How It Works                           </h1>

                            </div>
                        </div>
                    </div>
                </div>
             <div class="" ><div class="breadcrumbs-wrap">
                          <ul class="breadcrumbs"><li><a> </a>&nbsp;&nbsp;&nbsp;</li><li class="current"></li></ul></div>
                  </div>
            </div>
            <div class="contents-main" id="contents-main">

                <article id="post-3117" class="post-3117 page type-page status-publish hentry">
                    <div class="entry-content">
                        <div class="vc_row wpb_row vc_row-fluid">
                            <div class="container">
                                <div class="row">
                                    <div class="wpb_column vc_column_container vc_col-sm-12">
                                        <div class="vc_column-inner vc_custom_1507021372873">
                                            <div class="wpb_wrapper">
                                                <div class="iw-heading iw-job style1  text-center border-color-theme" style="width: 40%">
                                                    <h2 class="iwh-title" style="margin-bottom: 50px;color:rgb(87, 99, 119);margin-top: 50px;    font-size: 35px;">How eYogi Works</h2>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="vc_row wpb_row vc_row-fluid vc_custom_1507100241663" style="padding-bottom:20px">
                            <div class="container" style="width:95%">
                                <div class="row">
                                    <div class="wpb_column vc_column_container vc_col-sm-6">
                                        <div class="vc_column-inner"><div class="wpb_wrapper">
                                                <div class="wpb_text_column wpb_content_element" >
                                                    <div class="wpb_wrapper wpb_wrapper_div">
                                                        <div class="how-it-work right">
                                                            <div class="iw-title1" style="text-transform:uppercase;font-size:15px;color:#eea461;letter-spacing:3px;"><b>1. Anonymous User ( User without disclosing their Identity )</b></div>
                                                            <div class="iw-desc">If you do not want to disclose your identify, you can purchase a code as an anonymous user.No need for registration. This code can be used by you or your friends for scheduling an appointment.</div>
                                                            <div class="iw-title1" style="text-transform:uppercase;font-size:15px;color:#eea461;letter-spacing:3px;"><b>Registered Users</b></div>
                                                            <div class="iw-desc">If you are looking for convenience, you can register using your email Id or Phone number. Purchase credits to schedule appointments.</div>
                                                            <div class="iw-desc">Already have code or credits? Go ahead to Step 2.</div>
                                                        </div>

                                                    </div>
                                                </div>
                                            </div>

                                        </div>

                                    </div>
                                    <div class="wpb_column vc_column_container vc_col-sm-6">
                                        <div class="vc_column-inner">
                                            <div class="wpb_wrapper">
                                                <div  class="wpb_single_image wpb_content_element vc_align_center">

                                                    <figure class="wpb_wrapper vc_figure">
                                                        <div class="vc_single_image-wrapper   vc_box_border_grey"><img width="560" height="279" src="<?php echo base_url(); ?>uploads/2017/10/Group-4.png" class="vc_single_image-img attachment-full" alt="" srcset="<?php echo base_url(); ?>uploads/2017/10/Group-4.png 560w, <?php echo base_url(); ?>uploads/2017/10/Group-4-300x149.png 300w" sizes="(max-width: 560px) 100vw, 560px" /></div>
                                                    </figure>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>

                        <div class="vc_row wpb_row vc_row-fluid vc_custom_1507179302869" style="padding-bottom:20px">
                            <div class="container" style="width:95%">
                                <div class="row">
                                    <div class="wpb_column vc_column_container vc_col-sm-6">
                                        <div class="vc_column-inner"><div class="wpb_wrapper">
                                                <div  class="wpb_single_image wpb_content_element vc_align_center">

                                                    <figure class="wpb_wrapper vc_figure">
                                                        <div class="vc_single_image-wrapper   vc_box_border_grey"><img width="560" height="394" src="<?php echo base_url(); ?>uploads/2017/10/Group-18.png" class="vc_single_image-img attachment-full" alt="" srcset="<?php echo base_url(); ?>uploads/2017/10/Group-18.png 560w, <?php echo base_url(); ?>uploads/2017/10/Group-18-300x211.png 300w" sizes="(max-width: 560px) 100vw, 560px" /></div>
                                                    </figure>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="wpb_column vc_column_container vc_col-sm-6">
                                        <div class="vc_column-inner vc_custom_1507100776832">
                                            <div class="wpb_wrapper">
                                                <div class="wpb_text_column wpb_content_element" >
                                                    <div class="wpb_wrapper wpb_wrapper_div">
                                                        <div class="how-it-work left">
                                                            <div class="iw-title1" style="text-transform:uppercase;font-size:15px;color:#eea461;letter-spacing:3px;"><br><br><br><b>2. Schedule an appointment</b></div>
                                                            <div class="iw-desc"> Select  your convenient time and choose the counsellor you like. When you pick a counsellor, you can see his/her availability and fees. Schedule an appointment online and our system will connect you to the counsellor at the time you have selected. You or your counsellor will not know each other's phone numbers.

                                                            </div>
                                                        </div>

                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="vc_row wpb_row vc_row-fluid vc_custom_1507100192645" style="padding-bottom:20px">
                            <div class="container" style="width:95%">
                                <div class="row">
                                    <div class="wpb_column vc_column_container vc_col-sm-6"><div class="vc_column-inner"><div class="wpb_wrapper">
                                                <div class="wpb_text_column wpb_content_element" >
                                                    <div class="wpb_wrapper wpb_wrapper_div">
                                                        <div class="how-it-work right ">
                                                            <div class="iw-title1" style="text-transform:uppercase;font-size:15px;color:#eea461;letter-spacing:3px;"><br><br><br><b>3. Recharge your life</b></div>
                                                            <div class="iw-desc"> You will get an incoming call on your phone at the scheduled time. You can speak to the counsellor and discuss your problems. 
                                                                All you need to do is keep your phone ON and ensure there is enough battery charge for the session. 
                                                                Ensure you are in a comfortable place to reduce the noise in the call. 
                                                                Discuss your problems confidently and openly. A peaceful mind leads to a healthy body.
                                                            </div>
                                                        </div>

                                                    </div>
                                                </div>
                                            </div></div></div><div class="wpb_column vc_column_container vc_col-sm-6"><div class="vc_column-inner"><div class="wpb_wrapper">
                                                <div  class="wpb_single_image wpb_content_element vc_align_center">

                                                    <figure class="wpb_wrapper vc_figure">
                                                        <div class="vc_single_image-wrapper   vc_box_border_grey"><img width="560" height="273" src="<?php echo base_url(); ?>uploads/2017/10/Group-20.png" class="vc_single_image-img attachment-full" alt="" srcset="<?php echo base_url(); ?>uploads/2017/10/Group-20.png 560w, <?php echo base_url(); ?>uploads/2017/10/Group-20-300x146.png 300w" sizes="(max-width: 560px) 100vw, 560px" /></div>
                                                    </figure>
                                                </div>
                                            </div></div></div></div></div></div>

                    </div><!-- .entry-content -->
                    <div class="clearfix"></div>
                    <footer class="entry-footer">
                    </footer><!-- .entry-footer -->
                </article><!-- #post-## -->
            </div>
<?php $this->load->view('layout/footer.php') ?>