
<!DOCTYPE html>
<html lang="en-US">
    <head>
        <!-- Global site tag (gtag.js) - Google Analytics --> 
        
        <meta charset="UTF-8">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <link rel="profile" href="http://gmpg.org/xfn/11">
      
       
        <link rel='dns-prefetch' href='//www.google.com' />
        <link rel='dns-prefetch' href='//fonts.googleapis.com' />
        <link rel='dns-prefetch' href='//s.w.org' />
        <meta name="google-signin-client_id" content="493420901199-ml5l32mke8gs024lvq3opof2d1hu0rkm.apps.googleusercontent.com">
       

        <script src="https://apis.google.com/js/client:platform.js?onload=renderButton" async defer></script>


        <link rel='stylesheet' href='<?php echo base_url(); ?>css/dataTables.bootstrap4.min.css' type='text/css' media='all' />

        

        <style type="text/css">


            a,
            a label {
                cursor: pointer !important;
            }
            img.wp-smiley,
            img.emoji {
                display: inline !important;
                border: none !important;
                box-shadow: none !important;
                height: 1em !important;
                width: 1em !important;
                margin: 0 .07em !important;
                vertical-align: -0.1em !important;
                background: none !important;
                padding: 0 !important;
            }
            .iwj-field{
                border: 1px solid #eeeeee !important;
            }

            .modal-dialog1{
                overflow-y: initial !important
            }
            .modal-body1{
                height: 150px;
                overflow-y: auto;
            }
            .sbn {


                padding-top: 80px;
                border-top: 1px solid #fff;
                box-sizing: border-box;
            }
            .sbn:nth-of-type(odd) {

            }
            .sbn:nth-of-type(even) {

            }
            .sbn:nth-of-type(1),
            .sbn:nth-of-type(2) {

                padding-top: 0;
                border-top: none;
            }
            .sbn p {
                font-size: 20px;
                margin-bottom: 40px;
            }

            @media screen and (max-width: 800px) {
                .sbn-list {

                }
                .sbn {
                    width: auto;
                    float: none !important;
                }
                .sbn:nth-of-type(2) {

                    padding-top: 80px;
                    border-top: 1px solid #fff;
                }
                /*ul li {
                    margin: 0 .7em !important;
                }*/
                .sbn13 ul li {
                    width: 26px;
                    margin: 0 .6em !important;
                }
            }


            .sbn-new ul {
                list-style: none;
                text-align: center;
            }
            .sbn-new ul li {
                display: inline-block;
                margin: 0 1em;
            }
            .sbn-new ul li a {
                display: inline-block;
                width: 10px;
                height: 10px;
                font-size: 0;
                text-indent: 100%;
                white-space: nowrap;
                overflow: hidden;
                outline: none;
                box-sizing: border-box;
                transition: .2s ease-in-out;
                background-color: rgb(210, 212, 213);
            }
            /*            .vc_row{
                            width:100%
                        }*/

            .sbn01 ul li a {
                border: 2px solid rgb(210, 212, 213);
                border-radius: 50%;
            }
            .sbn01 ul li a:hover,
            .sbn01 ul li a.is-active {
                background-color: #ff8a00!important;
                border: 2px solid #ff8a00!important;
            }
            .iwj-field1{
                display: block;
                font-size: 11px;
                font-weight: 600;
                text-transform: uppercase;
                color: #ff8e16;
            }
            [type="radio"]:checked,
            [type="radio"]:not(:checked) {
                position: absolute;
                left: -9999px;
            }
            [type="radio"]:checked + label,
            [type="radio"]:not(:checked) + label
            {
                position: relative;
                padding-left: 28px;
                padding-right: 100px;
                cursor: pointer;
                line-height: 20px;
                display: inline-block;
                color: #666;
                margin-top: 60px;
            }
            [type="radio"]:checked + label:before,
            [type="radio"]:not(:checked) + label:before {
                content: '';
                position: absolute;
                left: 0;
                top: 0;
                width: 16px;
                height: 16px;
                border: 2px solid #ff8e16;
                border-radius: 100%;
                background: #fff;
            }
            [type="radio"]:checked + label:after,
            [type="radio"]:not(:checked) + label:after {
                content: '';
                width: 8px;
                height: 8px;
                background: #ff8e16;
                position: absolute;
                top: 4px;
                left: 4px;
                border-radius: 100%;
                -webkit-transition: all 0.2s ease;
                transition: all 0.2s ease;
            }
            [type="radio"]:not(:checked) + label:after {
                opacity: 0;
                -webkit-transform: scale(0);
                transform: scale(0);
            }
            [type="radio"]:checked + label:after {
                opacity: 1;
                -webkit-transform: scale(1);
                transform: scale(1);
            }
            .select2{
                width:100%;
            }
            .select2-container--default{
                background-color: #d3d5d6;        
            }

            .style1{
                color: #f6f7f9;
            }
            .error{
                border: 1.5px solid red !important;
            }
            .choose_file{
                position: relative;
                display: inline-block;
                border-radius: 8px;
                border: #ebebeb solid 1px;
                width: 198px;
                padding: 19px 2px 12px 69px;
                font: normal 14px Myriad Pro, Verdana, Geneva, sans-serif;
                color: #fff;
                margin-top: 2px;
                background: rgb(50, 158, 210);
            }
            .choose_file input[type="file"]{
                -webkit-appearance:none; 
                position:absolute;
                top:0; left:0;
                opacity:0; 
            }




            /* Carousel Control */
            .control-box {
                text-align: right;
                width: 100%;
            }
            .carousel-control{
                background: #666;
                border: 0px;
                border-radius: 0px;
                display: inline-block;
                font-size: 34px;
                font-weight: 200;
                line-height: 18px;
                opacity: 0.5;
                padding: 4px 10px 0px;
                position: static;
                height: 30px;
                width: 4% !important;
                top:58px !important;
            }
            .profile{
                border: 3px solid #B7B7B7;
                padding: 10px;
                margin-top: 10px;
                width: 350px;
                background-color: #F7F7F7;
                height: 160px;

                .abcRioButtonBlue{
                    background-color: #DB4437 !important;
                }
                .abcRioButtonBlue .abcRioButtonIcon{
                    background-color: #DB4437 !important;
                }
                .profile p{margin: 0px 0px 10px 0px;}
                .head{margin-bottom: 10px;}
                .head a{float: right;}
                .profile img{width: 100px;float: left;margin: 0px 10px 10px 0px;}
                .proDetails{float: left;}

                .select2-container{
                    width: 100% !important;
                }


                /*                
                                 Bitrix contact form style changes */


                .crm-webform-label {
                    display: block !important;
                    font-size: 11px !important;
                    font-weight: 600 !important;
                    text-transform: uppercase !important;
                    color: #ff8e16 !important;    
                }

                .crm-webform-input{
                    border: 1px solid #ff8e16 !important;
                    height: 60px !important;
                }
                .crm-webform-icon{
                    height: 40px !important;
                    padding-top: 9px !important;
                    color: #ff9128 !important;
                    border-color: #ff9128 !important;        
                } 

                .crm-webform-textarea{
                    border: 1px solid #ff8e16 !important;
                    height: 80px !important;
                }

                input[type="text"].crm-webform-input::placeholder { /* Chrome, Firefox, Opera, Safari 10.1+ */
                    color: #bcbcbc !important;
                    opacity: 1 !important; /* Firefox */
                }
                textarea.crm-webform-input::placeholder { /* Chrome, Firefox, Opera, Safari 10.1+ */
                    color: #bcbcbc !important;
                    opacity: 1 !important; /* Firefox */

                }
                .iwj-magic-line p.active{
                    color: #FF9128!important;
                }

                .btn[data-action="togglePeriod"], .btn-primary[data-action="togglePeriod"] { background: #fe6601; border:none; }

                .iw-social-all li a i{
                    margin-top: 5px !important;
                }
            </style>


<!--            <link rel="stylesheet" href="<?php //echo base_url();  ?>css/datafilter_style.css">-->


            <link rel='stylesheet' id='contact-form-7-css'  href='<?php echo base_url(); ?>plugins/contact-form-7/includes/css/styles.css?ver=4.9' type='text/css' media='all' />
            <link rel='stylesheet' id='iw-shortcodes-css'  href='<?php echo base_url(); ?>plugins/inwave-common/assets/css/iw-shortcodes.css?ver=2.0.0' type='text/css' media='all' />
            <link rel='stylesheet' id='iw-shortcodes-rtl-css'  href='<?php echo base_url(); ?>plugins/inwave-common/assets/css/iw-shortcodes-rtl.css?ver=2.0.0' type='text/css' media='all' />
            <link rel='stylesheet' id='iw_button-css'  href='<?php echo base_url(); ?>plugins/inwave-common/assets/css/iw-button.css?ver=2.0.0' type='text/css' media='all' />
            <link rel='stylesheet' id='iwjob-css'  href='<?php echo base_url(); ?>plugins/iwjob/includes/class/fields/assets/css/style.css?ver=4.8.2' type='text/css' media='all' />
            <link rel='stylesheet' id='iw-filter-job-load-css'  href='<?php echo base_url(); ?>plugins/iwjob/assets/css/load.css?ver=169' type='text/css' media='all' />
            <link rel='stylesheet' id='bxslider-css'  href='<?php echo base_url(); ?>plugins/iwjob/assets/css/jquery.bxslider.css?ver=169' type='text/css' media='all' />
            <link rel='stylesheet' id='iwj-rating-style-css'  href='<?php echo base_url(); ?>plugins/iwjob/assets/css/star-rating.css?ver=169' type='text/css' media='all' />
            <link rel='stylesheet' id='iwj-css'  href='<?php echo base_url(); ?>plugins/iwjob/assets/css/style.css?ver=4.8.2' type='text/css' media='all' />
            <link rel='stylesheet' id='rs-plugin-settings-css'  href='<?php echo base_url(); ?>plugins/revslider/public/assets/css/settings.css?ver=5.4.5.2' type='text/css' media='all' />
            <style id='rs-plugin-settings-inline-css' type='text/css'>
                #rs-demo-id {}
            </style>
            <link rel='stylesheet' id='bootstrap-css'  href='<?php echo base_url(); ?>assets/css/bootstrap.min.css?ver=2.0.0' type='text/css' media='all' />
            <link rel='stylesheet' id='font-awesome-css'  href='<?php echo base_url(); ?>plugins/js_composer/assets/lib/bower/font-awesome/css/font-awesome.min.css?ver=5.2.1' type='text/css' media='all' />
            <link rel='stylesheet' id='font-ionicons-css'  href='<?php echo base_url(); ?>assets/fonts/ionicons/ionicons.min.css?ver=2.0.0' type='text/css' media='all' />
            <link rel='stylesheet' id='font-iwj-css'  href='<?php echo base_url(); ?>assets/fonts/iwj/css/fontello.css?ver=2.0.0' type='text/css' media='all' />
            <link rel='stylesheet' id='injob-fonts-css'  href='https://fonts.googleapis.com/css?family=Open+Sans%3A300%2C400%2C600%2C700%2C800%7CPlayfair+Display%3A400%2C400i%2C700%2C700i%2C900%2C900i&#038;subset=latin%2Clatin-ext&#038;ver=2.0.0' type='text/css' media='all' />
            <link rel='stylesheet' id='select2-css'  href='<?php echo base_url(); ?>assets/css/select2.css?ver=2.0.0' type='text/css' media='all' />
            <link rel='stylesheet' id='owl-carousel-css'  href='<?php echo base_url(); ?>assets/css/owl.carousel.css?ver=2.0.0' type='text/css' media='all' />
            <link rel='stylesheet' id='owl-theme-css'  href='<?php echo base_url(); ?>assets/css/owl.theme.css?ver=2.0.0' type='text/css' media='all' />
            <link rel='stylesheet' id='owl-transitions-css'  href='<?php echo base_url(); ?>assets/css/owl.transitions.css?ver=2.0.0' type='text/css' media='all' />
            <link rel='stylesheet' id='animation-css'  href='<?php echo base_url(); ?>assets/css/animation.css?ver=2.0.0' type='text/css' media='all' />
            <link rel='stylesheet' id='injob-style-css'  href='<?php echo base_url(); ?>css/style.css?ver=4.8.2' type='text/css' media='all' />
            <link rel='stylesheet' id='iwjmb-avatar-css'  href='<?php echo base_url(); ?>plugins/iwjob/includes/class/fields/assets/css/avatar.css' type='text/css' media='all' />
            <style id='injob-style-inline-css' type='text/css'>
                html body{font-family:Open Sans}h1,h2,h3,h4,h5,h6{font-family:Open Sans}html body{font-size:13px}html body{line-height:28px}body .wrapper{background-color:#f1f1f1!important}html body{color:#777}.iw-footer-default .iw-footer-middle, .iw-footer-middle .widget_nav_menu .menu li a, .iw-footer-default .widget_inwave-subscribe .malchimp-desc, .iw-footer-middle .widget_inwave-subscribe .iw-email-notifications h5, body .iw-copy-right p{color:#2b495e}body .iw-footer-middle a, body .iw-footer-middle .widget li a, body .iw-footer-middle .widget_nav_menu .menu li a{color:#2b495e}.page-heading .container-inner{padding-top:200px}.page-heading .container-inner{padding-bottom:50px}.page-heading{background-image:url(<?php echo base_url(); ?>assets/images/bg-page-heading.png)!important;background-size: cover;background-repeat:no-repeat;}.header.header-default.header-sticky.clone{background-image:url(<?php echo base_url(); ?>assets/images/bg-header-sticky.jpg)!important;background-size: cover;background-repeat: no-repeat;position: relative;}
            </style>
            <link rel='stylesheet' id='injob-custom-css'  href='<?php echo base_url(); ?>uploads/injob/color-2980b9.css?ver=2.0.0' type='text/css' media='all' />
            <link rel='stylesheet' id='js_composer_front-css'  href='<?php echo base_url(); ?>plugins/js_composer/assets/css/js_composer.min.css?ver=5.2.1' type='text/css' media='all' />
            <script type='text/javascript' src='<?php echo base_url(); ?>js/jquery/jquery.js?ver=1.12.4'></script>
            <script type='text/javascript' src='<?php echo base_url(); ?>js/jquery/jquery-migrate.min.js?ver=1.4.1'></script>
            <script type='text/javascript' src='<?php echo base_url(); ?>plugins/inwave-common/assets/js/jquery.countdown.js?ver=2.0.0'></script>
            <script type='text/javascript'>
            /* <![CDATA[ */
            var userSettings = {"url": "\/wordpress\/", "uid": "0", "time": "1507287331", "secure": ""};
            /* ]]> */
            </script>
            <link rel='stylesheet' href='<?php echo base_url(); ?>css/bootstrap-datetimepicker.css' type='text/css' media='all' />

            <link rel='stylesheet' href='<?php echo base_url(); ?>css/sweet-alert.css' type='text/css' media='all' />

            <script type='text/javascript' src='<?php echo base_url(); ?>js/utils.min.js?ver=4.8.2'></script>
            <script type='text/javascript' src='<?php echo base_url(); ?>js/plupload/plupload.full.min.js?ver=2.1.8'></script>
            <!--[if lt IE 8]>
            <script type='text/javascript' src='./wp-includes/js/json2.min.js?ver=2015-05-03'></script>
            <![endif]-->
            <link rel='stylesheet' href='<?php echo base_url(); ?>css/jquery.timepicker.css' type='text/css' media='all' />
            <script type='text/javascript' src='<?php echo base_url(); ?>js/jquery.min.js'></script>
            <script type='text/javascript' src='<?php echo base_url(); ?>assets/js/bootstrap.min.js?ver=2.0.0'></script>
            <script src='<?php echo base_url(); ?>assets/js/custom.js' type="text/javascript"></script>
            <script type='text/javascript' src='<?php echo base_url(); ?>plugins/revslider/public/assets/js/jquery.themepunch.tools.min.js?ver=5.4.5.2'></script>
            <script type='text/javascript' src='<?php echo base_url(); ?>plugins/revslider/public/assets/js/jquery.themepunch.revolution.min.js?ver=5.4.5.2'></script>


            <style type="text/css">.recentcomments a{display:inline !important;padding:0 !important;margin:0 !important;}</style>
            <style type="text/css">.recentcomments a{display:inline !important;padding:0 !important;margin:0 !important;}</style>
            <link rel='stylesheet' href='<?php echo base_url(); ?>css/dataTables.bootstrap4.min.css' type='text/css' media='all' />

            <meta name="generator" content="Powered by Visual Composer - drag and drop page builder for WordPress."/>
            <!--[if lte IE 9]><link rel="stylesheet" type="text/css" href="./wp-content/<?php echo base_url(); ?>plugins/js_composer/assets/css/vc_lte_ie9.min.css" media="screen"><![endif]--><meta name="generator" content="Powered by Slider Revolution 5.4.5.2 - responsive, Mobile-Friendly Slider Plugin for WordPress with comfortable drag and drop interface." />
            <script type="text/javascript">function setREVStartSize(e) {
                try {
                    var i = jQuery(window).width(), t = 9999, r = 0, n = 0, l = 0, f = 0, s = 0, h = 0;
                    if (e.responsiveLevels && (jQuery.each(e.responsiveLevels, function (e, f) {
                        f > i && (t = r = f, l = e), i > f && f > r && (r = f, n = e)
                    }), t > r && (l = n)), f = e.gridheight[l] || e.gridheight[0] || e.gridheight, s = e.gridwidth[l] || e.gridwidth[0] || e.gridwidth, h = i / s, h = h > 1 ? 1 : h, f = Math.round(h * f), "fullscreen" == e.sliderLayout) {
                        var u = (e.c.width(), jQuery(window).height());
                        if (void 0 != e.fullScreenOffsetContainer) {
                            var c = e.fullScreenOffsetContainer.split(",");
                            if (c)
                                jQuery.each(c, function (e, i) {
                                    u = jQuery(i).length > 0 ? u - jQuery(i).outerHeight(!0) : u
                                }), e.fullScreenOffset.split("%").length > 1 && void 0 != e.fullScreenOffset && e.fullScreenOffset.length > 0 ? u -= jQuery(window).height() * parseInt(e.fullScreenOffset, 0) / 100 : void 0 != e.fullScreenOffset && e.fullScreenOffset.length > 0 && (u -= parseInt(e.fullScreenOffset, 0))
                        }
                        f = u
                    } else
                        void 0 != e.minHeight && f < e.minHeight && (f = e.minHeight);
                    e.c.closest(".rev_slider_wrapper").css({height: f})
                } catch (d) {
                    console.log("Failure at Presize of Slider:" + d)
                }
            }
            ;</script>
            <style type="text/css" data-type="vc_shortcodes-custom-css">.vc_custom_1502700206350{padding-top: 250px !important;background-image: url(<?php echo base_url(); ?>uploads/2017/07/slider-image-1.png?id=1456) !important;background-size: cover;background-repeat: no-repeat;background-position: center center; }.vc_custom_1502072802630{padding-top: 90px !important;padding-bottom: 105px !important;background-color: #ffbc85 !important;}.vc_custom_1501842017830{padding-top: 90px !important;padding-bottom: 90px !important;background-image: url(<?php echo base_url(); ?>uploads/2017/08/bg-info-home-4.png?id=1536) !important;background-position: center !important;background-repeat: no-repeat !important;background-size: cover !important;}.vc_custom_1502076897543{padding-top: 90px !important;padding-bottom: 90px !important;background-color: #ffffff !important;}.vc_custom_1501836210313{padding-bottom: 65px !important;background-image: url(<?php echo base_url(); ?>uploads/2017/03/Image-Background-2.png?id=1106) !important;background-position: center !important;background-repeat: no-repeat !important;background-size: cover !important;}.vc_custom_1501836183839{margin-bottom: 120px !important;}.vc_custom_1501833469137{padding-bottom: 80px !important;background-color: #fafafa !important;}.vc_custom_1501833554275{    background: linear-gradient(rgb(250, 246, 244), rgb(251, 239, 229))}.vc_custom_1502698608067{padding-top: 100px !important;padding-bottom: 80px !important;background-image: url(<?php echo base_url(); ?>uploads/2017/08/bg-home-2.png?id=1541) !important;background-position: center !important;background-repeat: no-repeat !important;background-size: cover !important;}.vc_custom_1502174616606{padding-bottom: 110px !important;    padding-top: 80px !important;                                                                                                                                                                                       background: linear-gradient( rgb(251, 239, 229),rgb(254, 229, 209));}.vc_custom_1501829096771{margin-bottom: 70px !important;}.vc_custom_1502700751795{margin-top: 115px !important;margin-bottom: 68px !important;}.vc_custom_1502072813366{margin-bottom: 75px !important;}.vc_custom_1501842006396{margin-bottom: 65px !important;}.vc_custom_1502080122461{margin-top: 90px !important;margin-bottom: 15px !important;}.vc_custom_1502178519794{margin-bottom: 65px !important;}.vc_custom_1501836220622{margin-top: 90px !important;margin-bottom: 105px !important;}.vc_custom_1505959921787{padding-right: 0px !important;padding-left: 0px !important;background-image: url(<?php echo base_url(); ?>uploads/2017/08/bg-info-home-3.png?id=1535) !important;background-position: center !important;background-repeat: no-repeat !important;background-size: cover !important;}.vc_custom_1505960719168{padding-right: 0px !important;padding-left: 0px !important;background-image: url(<?php echo base_url(); ?>uploads/2017/08/bg-info-home-4.png?id=1536) !important;background-position: center !important;background-repeat: no-repeat !important;background-size: cover !important;}.vc_custom_1505959894043{padding-top: 111px !important;padding-bottom: 116px !important;}.vc_custom_1505960895807{padding-top: 111px !important;padding-bottom: 116px !important;}.vc_custom_1501831660590{margin-top: 90px !important;margin-bottom: 70px !important;}.vc_custom_1501832143838{margin-top: 90px !important;margin-bottom: 70px !important;}.vc_custom_1501833634878{margin-top: 90px !important;margin-bottom: 55px !important;}</style><noscript><style type="text/css"> .wpb_animate_when_almost_visible { opacity: 1; }


            </style></noscript>
            <style type="text/css">
                @media (max-device-width : 1920px) {
                    .vc_custom_1502700206350{
                        padding-top: 250px !important;
                        background-image:url(<?php echo base_url(); ?>assets/images/1920.png)!important;
                        background-repeat: no-repeat!important;
                        background-size: cover!important
                    }
                    .iwj-magic-line p.active{
                        color: #FF9128!important;
                    }
                    .purchase{
                        width:68.5%
                    }
                    @media (max-device-width : 1024px) {
                        .vc_custom_1502700206350{
                            padding-top: 250px !important;
                            background-image:url(<?php echo base_url(); ?>assets/images/1024.png)!important;
                            background-repeat: no-repeat!important;
                            background-size: contain!important
                        }
                        .iwj-magic-line p.active{
                            color: #FF9128!important;
                        }
                        .purchase{
                            width:68.5%;
                        }
                    }

                    @media (max-width:768px){
                        .vc_custom_1502700206350{
                            padding-top: 250px !important;
                            background-image:url(<?php echo base_url(); ?>assets/images/1024.png)!important;  
                            background-repeat: no-repeat!important;  
                            background-size: contain!important;    
                            height: 432px }
                        .vc_custom_1502174616606{
                            background-color: #FBF0E8;
                            background-image: url('<?php echo base_url(); ?>assets/images/plane_768.png')!important;
                            background-repeat: no-repeat;
                        }
                        .purchase{
                            width: 100%;
                        }

                    }  
                    @media (max-width:640px){
                        .vc_custom_1502700206350{
                            padding-top: 250px !important;
                            background-image:url(<?php echo base_url(); ?>assets/images/640x1090.png)!important;  
                            background-repeat: no-repeat!important;  
                            background-size: cover!important;    
                            height: 432px }
                        .bgimg{
                            position: relative;
                            text-align: center;
                        }

                        .centered {
                            position: absolute;
                            top: 50%;
                            left: 50%;
                            transform: translate(-50%, -50%);
                        }
                        .eyo-title{
                            top: 50%;
                            left: 50%;
                            text-align: left!important;
                            margin-top: -149px;
                        }
                        .eyo-description{
                            text-align: left!important;
                            color: #000!important;
                        }
                        .eyo-btn{
                            padding: 14px 35px;
                        }
                        .vc_custom_1502174616606{
                            background-color: #FBF0E8;
                            background-image: url('<?php echo base_url(); ?>assets/images/plane_768.png')!important;
                            background-repeat: no-repeat;
                        }
                        .purchase{
                            width: 100%;
                        }
                        .mind{
                                font-size: 42px !important;
                        }
                    }  

                    @media (max-device-width : 480px) {
                        .vc_custom_1502700206350{
                            padding-top: 250px !important;
                            background-image:url(<?php echo base_url(); ?>assets/images/640x1090.png)!important;
                            background-repeat: no-repeat!important;
                            background-size: cover!important
                        }
                        .bgimg{
                            position: relative;
                            text-align: center;
                        }

                        .centered {
                            position: absolute;
                            top: 50%;
                            left: 50%;
                            transform: translate(-50%, -50%);
                        }
                        .eyo-title{
                            margin-bottom: 50px!important;
                            text-align: left!important;
                            margin-top: -149px;
                        }
                        .eyo-description{
                            text-align: left!important;
                            color: #000!important;
                        }
                        .eyo-btn{

                            padding: 10px 50px;
                        }
                        .vc_custom_1502174616606{
                            background-color: #FBF0E8;
                            background-image: url('<?php echo base_url(); ?>assets/images/plane_480.png')!important;
                            background-repeat: no-repeat;
                        }
                        .purchase{
                            width: 100%;
                        }

                    }
                    .iwj-magic-line p.active{
                        color: #FF9128!important;
                    }
                    i#icon{
                        padding:10px;
                    }
                </style>


                <!-- Facebook Pixel Code -->
                <script>
                    !function (f, b, e, v, n, t, s)
                    {
                        if (f.fbq)
                            return;
                        n = f.fbq = function () {
                            n.callMethod ?
                                    n.callMethod.apply(n, arguments) : n.queue.push(arguments)
                        };
                        if (!f._fbq)
                            f._fbq = n;
                        n.push = n;
                        n.loaded = !0;
                        n.version = '2.0';
                        n.queue = [];
                        t = b.createElement(e);
                        t.async = !0;
                        t.src = v;
                        s = b.getElementsByTagName(e)[0];
                        s.parentNode.insertBefore(t, s)
                    }(window, document, 'script',
                            'https://connect.facebook.net/en_US/fbevents.js');
                    fbq('init', '161435267886356');
                    fbq('track', 'PageView');
                </script>
                <noscript>
            <img height="1" width="1" 
                 src="https://www.facebook.com/tr?id=161435267886356&ev=PageView
                 &noscript=1"/>
            </noscript>
            <!-- End Facebook Pixel Code -->
            <meta name="google-site-verification" content="_rnVM6zGAht1ob298P--Bx24lGEyBJ0W52HGfBlKaBo" />

        </head><body id="page-top" class="page-template page-template-page-templates page-template-home-page page-template-page-templateshome-page-php page page-id-1614 wpb-js-composer js-comp-ver-5.2.1 vc_responsive">


           


                <div class="wrapper">
                    <div class="iw-overlay"></div>
                    <div class="header header-default header-style-default">
                        <div class="iw-top-bar-wrapper">
                            <div class="row">
                                <div class="col-md-6 col-sm-9 col-xs-9 hidden-xs">
                                    <div class="top-bar-left">
                                        <div class="contact"><span>Phone: 08039534067</span><span>Email: info@eyogi.in</span></div>
                                    </div>
                                </div>
                                <div class="col-md-6 col-sm-3 col-xs-3">
                                    <div class="top-bar-right">
                                        <div class="social-header">
                                            <div><ul class="iw-social-all"><li><a target="_blank" class="facebook" onclick="window.open('https://www.facebook.com/eyogi.in/')" title="Facebook"><i class="fa fa fa-facebook" style="margin-top:5px !important"></i></a></li><li><a class="twitter" target="_blank"  onclick="window.open('https://twitter.com/eyogi_in')" title="Twitter"><i class="fa fa fa-twitter" style="margin-top:5px !important"></i></a></li><li><a class="google-plus" target="_blank"  onclick="window.open('https://plus.google.com/105548168969313762025')" title="Google Plush"><i class="fa fa fa-google" style="margin-top:5px !important"></i></a></li></ul></div>

                                        </div>
                                                       



                                           
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="navbar navbar-default iw-header">
                            <div class="navbar-default-inner">
                                <h1 class="iw-logo float-left">
                                    <a href="<?php echo base_url(); ?>" title="eYogi">
                                        <img class="main-logo" src="<?php echo base_url(); ?>assets/images/logo.png" alt="eYogi">
                                        <img class="sticky-logo" src="<?php echo base_url(); ?>assets/images/logo-sticky.png" alt="eYogi">
                                        <img class="logo-mobile" src="<?php echo base_url(); ?>assets/images/logo-mobile.png" alt="eYogi">
                                    </a>
                                </h1>
                                <div class="header-btn-action">
                                    <span class="off-canvas-btn">
                                        <i class="fa fa-bars" id="icon"></i>
                                    </span>
                                    <div class="iwj-action-button float-right">

                                    </div> 
                                     </div>
                            <div class="iw-menu-header-default float-right">
                                
                                <!--<div class="search-form float-right">
                                    <form class="search-form-header" method="get" action="./../index.html">
                                        <span class="search-wrap">
                                            <input type="search" title="Search for:" value="" name="s" placeholder="Enter  key words..." class="top-search">
                                            <span class="icon"><i class="fa fa-search"></i></span>
                                        </span>
                                    </form>
                                </div> -->
                            </div>
                        </div>
                    </div>
                </div>

                <!--End Header--> 
