<?php $this->load->view('layout/header.php') ?>
<div class="page-heading default">
                <div class="container-inner">
                    <div class="container">
                        <div class="page-title">
                            <div class="iw-heading-title">
                                <h1 style="color:#894724">
                                    FAQ                         </h1>

                            </div>
                        </div>
                    </div>
                </div>
             
            </div>
            <div class="contents-main" id="contents-main">

                <article id="post-3117" class="post-3117 page type-page status-publish hentry">
                    <div class="entry-content">
                        
                        <div class="vc_row wpb_row vc_row-fluid vc_custom_1507100241663" style="padding-bottom:20px;">
                            <div class="container" style="width:80%">
                                <div class="row">
                                    <div class="wpb_column vc_column_container vc_col-sm-12" style="background-color: #fff">
                                        <div class="vc_column-inner"><div class="wpb_wrapper">
                                                <div class="wpb_text_column wpb_content_element" >
                                                    <div class="wpb_wrapper wpb_wrapper_div">
                                                        <div class="how-it-work">
                                                            <div class="iw-title1" style="text-transform:uppercase;font-size:15px;color:#eea461;letter-spacing:3px;margin-top: 50px"><b> FAQ    - eYogi.in</b></div>
                                                            <div class="iw-desc">
                                                                <p>   

                                                                <p style="text-transform:uppercase;font-size:15px;letter-spacing:3px;margin-top: 50px"> Anonymity - Coupon usage <br>
<p><b>1. I was given this coupon by my friend/colleague/institution or organization. Will they know if I
    have taken the session or what is being discussed with the counsellor?</b><br>
    We respect your privacy. The original coupon buyer will never know if the coupon was used or what was being discussed with 
    counsellor using the coupon code. No feedback will be given to the person/organization who purchased the coupon<br></p>
<p><b>2. How will the counsellor contact me if I am anonymous?</b><br>
Our system will connect you and the counsellor at the scheduled time. Counsellor will not know
your phone number.<br></p>
<p><b>3. What is shared with the counsellor for my anonymous consultation?</b><br>
Only the notes you provide during scheduling. Counsellor will not know any information about
you. You can even keep your name private during the session.</p><br>
<p><b>4. I have a coupon for Rs. 3000. Can I schedule multiple sessions with this?</b><br>
Yes, multiple sessions can be scheduled with the same coupon as long as there is sufficient
balance available.</p><br>
<p><b>5. How can I check the balance amount in my coupon?</b><br>
Please go to schedule page and click "Balance Check" button to check balance amount and
expiry date of coupon.</p><br>
<p><b>6. I have received a coupon from someone. Is there an expiry date for using this coupon?</b><br>
Yes. Coupons should be used within 180 days from the date of purchase.<br></p>

<p style="text-transform:uppercase;font-size:15px;letter-spacing:3px;margin-top: 50px"> Scheduling a session <br>
<p><b>1. What happens if I am unable to take the call during a scheduled time?</b><br>
We will allow you to reschedule it once.<br></p>
<p><b>2. I have scheduled a session. Will I have to do anything else during the scheduled time?</b><br>
No. You will get a call automatically from our number (08039534067) and you will be connected
to the counsellor. If you have enabled DND, please give a miss call to our number (08039534067).
You have to do this only once. I want to schedule a session, but none of the counsellors are available at the specified time.
Please use the chat option in home page to contact us. We will find a counsellor who can attend
to you at the specified time. If an appropriate counsellor is not available, then we will let you
know. You don't have to reveal your identity here.<br></p>

<p style="text-transform:uppercase;font-size:15px;letter-spacing:3px;margin-top: 50px"> During the session <br>
<p><b>1. What happens if I don't like the counsellor I am talking to and want to select another person?</b><br>
Please hang up the call within 6 minutes. Please use the chat option in home page to contact
us. We will find a counsellor who can attend to you at the specified time. You don't have to
reveal your identity here.<br></p>
<p><b>2. What happens if I am on a bad cellphone reception area an am unable to talk during the
scheduled time?</b><br>
Please hang up the call within 6 minutes. You can reschedule the call once.<br></p>
<p><b>3. What happens if my call gets disconnected in between a session?</b><br>
If you want to speak to the same counsellor again, please give a missed call to the number (08039534067). 
Disconnected session will be established automatically
<p><b>4. I am already on a session. What if I want to extend my consultation period?</b><br>
Consultation periods are for 45 minutes. You can rebook a session with the same counsellor on
the website.</p><br>
</p>

                                                            </div>
                                                        </div>

                                                    </div>
                                                </div>
                                            </div>

                                        </div>

                                    </div>
                                    
                                </div>
                            </div>
                        </div>

                       
                        

                    </div><!-- .entry-content -->
                    <div class="clearfix"></div>
                    <footer class="entry-footer">
                    </footer><!-- .entry-footer -->
                </article><!-- #post-## -->
            </div>
<?php $this->load->view('layout/footer.php') ?>