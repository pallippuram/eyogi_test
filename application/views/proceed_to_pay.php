<?php $this->load->view('logo_header.php') ?>
<div class="page-heading default">
    <div class="container-inner">
        <div class="container">
            <div class="page-title">
                <div class="iw-heading-title">
                    <h1>
                        Proceed to Pay                     </h1>

                </div>
            </div>
        </div>
    </div>
   
</div>



<div class="contents-main" style="background-color: #f2f2f1;padding-top: 50px;padding-bottom: 50px;" id="contents-main">
    <div class="container">
        <div class="row">
            <div class="col-sm-12 col-xs-12">

                <article id="post-143" class="post-143 page type-page status-publish hentry">
                    <div class="entry-content">

                        <form method="post" action="" class="iwj-form-2 iwj-candidate-form">


                            <div class="iwj-block">
                                <div class="basic-area iwj-block-inner">

                                    <div class="iwjmb-field iwjmb-avatar-wrapper"><div class="iwjmb-input ui-sortable">            
                                            <div id="_iwj_avatar" class="iwj-avatar-container">
                                                <!-- Current avatar -->
                                                <div>
                                                    <h2 class="theme-color">Order Summary</h2>
                                                </div>



                                            </div>
                                        </div>
                                    </div>
                                    <div class="row">

                                        <div class="col-md-12">
                                            <div class="table-responsive">          
                                                
                                            </div>
                                        </div>
                                    </div>
                                    <div class="row">
                                        <div class="">
                                            <h3><span class="theme-color" style="text-align: left !important;margin-left: 30px;"> PAY Rs <?php echo $amount; ?></span></h3>
                                        </div>

                                    </div>





                                </div>


                                <div class="iwj-respon-msg hide"></div>
                                <div class="iwj-button-loader">
                                    <button type="submit" id="place_order" class="iwj-btn edit_btn iwj-btn-primary iwj-candidate-btn">Place Order</button>
                                </div>
                            </div>
                        </form>




                    </div><!-- .entry-content -->
                    <div class="clearfix"></div>
                    <footer class="entry-footer">
                    </footer><!-- .entry-footer -->
                </article><!-- #post-## -->
            </div>
        </div>
    </div>
</div>


<script src="https://checkout.razorpay.com/v1/checkout.js"></script>
<form name='razorpayform' action="<?php echo site_url('payment/webinar_verify'); ?>" method="POST">
    <input type="hidden" name="razorpay_payment_id" id="razorpay_payment_id">
    <input type="hidden" name="razorpay_signature"  id="razorpay_signature" >
</form>
<script type='text/javascript' src='<?php echo base_url(); ?>js/jquery.min.js'></script>
<script>
// Checkout details as a json
    var options = <?php echo json_encode($data); ?>;

    /**
     * The entire list of Checkout fields is available at
     * https://docs.razorpay.com/docs/checkout-form#checkout-fields
     */
    options.handler = function (response) {
        document.getElementById('razorpay_payment_id').value = response.razorpay_payment_id;
        document.getElementById('razorpay_signature').value = response.razorpay_signature;
        document.razorpayform.submit();
    };

// Boolean whether to show image inside a white frame. (default: true)
    options.theme.image_padding = false;

    options.modal = {
        ondismiss: function () {
            console.log("This code runs when the popup is closed");
        },
        // Boolean indicating whether pressing escape key 
        // should close the checkout form. (default: true)
        escape: true,
        // Boolean indicating whether clicking translucent blank
        // space outside checkout form should close the form. (default: false)
        backdropclose: false
    };

    var rzp = new Razorpay(options);

    document.getElementById('place_order').onclick = function (e) {
        rzp.open();
        e.preventDefault();
    }

</script>