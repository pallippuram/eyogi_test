<?php $this->load->view('layout/user_header.php') ?>
<div class="contents-main" id="contents-main" style="margin-top:3%">

    <article id="post-141" class="post-141 page type-page status-publish hentry">
        <div class="entry-content">
            <div class="iwj-dashboard clearfix">

                <div class="iwj-dashboard-menu-mobile">
                    <div class="dropdown">
                        <button class="btn btn-primary dropdown-toggle"  type="button" data-toggle="dropdown">Menu Dashboard <span class="caret"></span></button>

                        <?php $this->load->view('layout/menu.php') ?>       
                    </div>
                </div>
                <div class="iwj-dashboard-main overview">
                    <div class="iwj-dashboard-main-inner">
                        <div class="iwj-overview iwj-profile clearfix">
                            <div class="info-top-wrap" style="width:100%">
                                <div class="sidebar-info">
                                    <div class="avatar">
                                        <img alt="Peter Pham" src="<?php if($data1[0]->photo!="") {  echo $data1[0]->photo; }else { echo base_url()."uploads/images/user.jpg";  } ?>"  class="avatar avatar-150 photo" height="150" width="150">
                                    </div>
                                    <a style="color:#777 !important" class="iwj-edit-profile" href="<?php echo base_url(); ?>counselor/edit_profile"><i class="fa fa-edit"></i>Edit My Profile</a>
                                </div>
                                <div class="main-info candidate-info iw-job-detail-sidebar">
                                    <div class="info-top" style="padding-bottom:0">
                                        <h3 class=""><a style="color:#777" href="myprofile.html"><?php echo $data[0]->username; ?></a></h3>

                                    </div>
                                    <div class="iwj-sidebar-bottom info-bottom" style="border-bottom: 1px #f6f7f9 solid;">
                                        <ul>
                                            <li class="location">
                                                <div class="left">
                                                    <i class="fa fa-user"></i>
                                                    <span class="title-meta ">Gender:</span>
                                                </div>
                                                <div class="content"><?php
                                                    if ($data[0]->gender == 0) {
                                                        echo 'Male';
                                                    } else {
                                                        echo 'Female';
                                                    }
                                                    ?>
                                                </div>
                                            </li>
                                            <li class="phone">
                                                <div class="left">
                                                    <i class="ion-android-phone-portrait"></i>
                                                    <span class="title-meta ">Phone:</span>
                                                </div>
                                                <div class="content"><?php echo $data[0]->phone; ?></div>
                                            </li>

                                            <li class="email">
                                                <div class="left ">
                                                    <i class="iwj-icon-email"></i>
                                                    <span class="title-meta">Email:</span>
                                                </div>
                                                <div class="content"><a href="<?php echo $data[0]->email; ?>"><?php echo $data[0]->email; ?></a></div>
                                            </li>
                                        </ul>
                                    </div>
                                    <div class="iwj-sidebar-bottom info-bottom">
                                       
                                    </div>
                                </div>
                            </div>

                           


                        </div>
                    </div>
                </div>


                <!-- iwj-sidebar-sticky-->
                <div class="iwj-dashboard-sidebar">
                    <div class="user-profile candidate clearfix">
                        <img alt='Peter Pham' src='<?php if($data1[0]->photo!="") { echo $data1[0]->photo; }else { echo base_url()."uploads/images/user.jpg";  } ?>' srcset='' class='avatar avatar-96 photo' height='96' width='96' />            
                        <h4>
                            <span>Howdy!</span>
                            <?php echo $data1[0]->username; ?>          
                        </h4>
                    </div>
                    <div class="iwj-dashboard-menu">
                        <?php $this->load->view('layout/sidebar.php') ?>   

                    </div>
                </div>
            </div>
        </div><!-- .entry-content -->
        <div class="clearfix"></div>
        <footer class="entry-footer ">
        </footer><!-- .entry-footer -->
    </article><!-- #post-## -->
</div>
<div id="buyCredit" class="modal-popup modal fade iwj-register-form-popup">
    <div class="modal-dialog">
        <div class="modal-header">
            <h4 class="modal-title">Purchase Credit</h4>
            <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
        </div>
        <div class="modal-content">
            <form action="<?php echo base_url(); ?>index.php/payment/index" method="post"  class="iwj-form iwj-login-form1">
                <input type="hidden" name="ind_type" value="user">
                <input type="hidden" name="qty_input" value="1">
                
                <div class="iwj-field">

                    <div class="iwj-input">

                        <input type="text" name="total_input" placeholder="Enter Credit Amount">
                    </div>
                </div>


                <div class="iwj-respon-msg iwj-hide"></div>
                <div class="iwj-button-loader">

                    <button type="submit" id="buy_credit" name="register" class="iwj-btn iwj-btn-primary iwj-btn-full iwj-btn-medium iwj-register-btn">Proceed to pay</button>
                </div>


            </form>
            
        </div>
    </div>
</div>

<script>
    function buyCredit() {

        $('#buyCredit').modal('show')
    }


</script>
<?php $this->load->view('layout/footer.php') ?>


