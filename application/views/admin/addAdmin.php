<?php $this->load->view('layout/user_header.php') ?>
<div class="contents-main" id="contents-main" style="margin-top:3%">

    <article id="post-141" class="post-141 page type-page status-publish hentry">
        <div class="entry-content">
            <div class="iwj-dashboard clearfix">

                <div class="iwj-dashboard-menu-mobile">
                    <div class="dropdown">
                        <button class="btn btn-primary dropdown-toggle"  type="button" data-toggle="dropdown">Menu Dashboard <span class="caret"></span></button>

                        <?php $this->load->view('layout/menu.php') ?>    
                         
                    </div>
                </div>
                <div class="iwj-dashboard-main profile">
                    <div class="iwj-dashboard-main-inner">
                        <div class="iwj-profile clearfix">
                            <div class="iwj-edit-candidate-profile iwj-edit-profile-page">


                                <form method="post" enctype="multipart/form-data" action="<?php echo site_url('admin/save_admin'); ?>" class="iwj-form-2 iwj-login-form1">

                                    <div class="form_error">
                                        <?php echo validation_errors(); ?>
                                    </div>
                                    <div class="iwj-block">
                                        <div class="basic-area iwj-block-inner">

                                            <div class="iwjmb-field iwjmb-avatar-wrapper"><div class="iwjmb-input ui-sortable">            
                                                    <div id="_iwj_avatar" class="iwj-avatar-container">
                                                        <!-- Current avatar -->
                                                        <div class="avatar-view">
                                                            <img src="<?php echo base_url() ?>uploads/images/user.jpg" alt="Avatar">
                                                        </div>
                                                        <div class="desc-change-image">
                                                            <p class="avatar-description"></p>
                                                                <div class="change-image-btn" style="padding-top:50px">

                                                                    <div class="choose_file btn btn-primary">
                                                                        <span>Choose File</span>
                                                                        <input name="image" type="file" />
                                                                    </div>
                                                                </div>
                                                        </div>

                                                    </div>
                                                </div>
                                            </div>
                                            <div class="row">
                                                <div class="col-md-6">
                                                    <div class="iwjmb-field iwjmb-text-wrapper  required">
                                                        <div class="iwjmb-label">
                                                            <label class="theme-color" for="your_name">Your Name *</label>
                                                        </div>
                                                        <div class="iwjmb-input ui-sortable">
                                                            <input size="30" placeholder="Enter your name" value="" type="text" id="your_name" class="iwjmb-text " name="username">
                                                        </div>
                                                    </div>
                                                    <div class="iwjmb-field iwjmb-email-wrapper  required">
                                                        <div class="iwjmb-label">
                                                            <label class="theme-color" for="email">Email *</label>
                                                        </div>
                                                        <div class="iwjmb-input ui-sortable">
                                                            <input value="" placeholder="Enter your email" type="email" required="required" id="email" class="iwjmb-email " name="email">
                                                        </div>
                                                    </div>                    
                                                </div>
                                                <div class="col-md-6">
                                                    <div class="iwjmb-field iwjmb-text-wrapper  required">
                                                        <div class="iwjmb-label">
                                                            <label class="theme-color" for="_iwj_phone">Phone *</label>
                                                        </div>
                                                        <div class="iwjmb-input ui-sortable">
                                                            <input size="30" placeholder="Enter your phone" value="" type="text" required="required" id="_iwj_phone" class="iwjmb-text " name="phone">
                                                        </div>
                                                    </div>
                                                    <div class="iwjmb-field iwjmb-email-wrapper  required">
                                                        <div class="iwjmb-label">
                                                            <label class="theme-color" for="email">Password *</label>
                                                        </div>
                                                        <div class="iwjmb-input ui-sortable">
                                                            <input value="" placeholder="Enter your password" type="password" required="required" id="passwrd" class="iwjmb-email " name="password">
                                                        </div>
                                                    </div>   
                                                </div>
                                            </div>
                                            <div class="row">
                                                <div class="col-md-6">
                                                    <div class="iwjmb-field iwjmb-text-wrapper  required"><div class="iwjmb-label"><label class="theme-color" for="_iwj_headline">Gender *</label></div>
                                                        <div class="iwjmb-input ui-sortable">

                                                            <select data-options="{&quot;allowClear&quot;:false,&quot;width&quot;:&quot;none&quot;,&quot;placeholder&quot;:&quot;&quot;,&quot;minimumResultsForSearch&quot;:-1}" required id="frequency" class="iwjmb-select_advanced" name="gender">
                                                                <option selected="">Please Select</option>
                                                                <option value="0">Male</option>
                                                                <option value="1">Female</option>
                                                                <option value="3">Others</option>

                                                            </select>
                                                        </div>
                                                    </div>                   
                                                </div>
                                                <div class="col-md-6">
                                                    <div class="iwjmb-field iwjmb-text-wrapper  required">
                                                        <div class="iwjmb-label"><label class="theme-color" for="_iwj_address">Role *</label></div>
                                                        <div class="iwjmb-input ui-sortable">
                                                            <select data-options="{&quot;allowClear&quot;:false,&quot;width&quot;:&quot;none&quot;,&quot;placeholder&quot;:&quot;&quot;,&quot;minimumResultsForSearch&quot;:-1}" required id="frequency" class="iwjmb-select_advanced" name="role">
                                                                
                                                                <option value="1" >Admin</option>
                                                            </select>
                                                            
                                                        </div>
                                                    </div> 
                                                </div>
                                            </div>
                                            
                                            <div class="row">
                                                <div class="col-md-6">
                                                    <div class="iwjmb-field iwjmb-text-wrapper  required"><div class="iwjmb-label"><label class="theme-color" for="_iwj_headline">Domain *</label></div>
                                                        <div class="iwjmb-input ui-sortable">

                                                            <select data-options="{&quot;allowClear&quot;:false,&quot;width&quot;:&quot;none&quot;,&quot;placeholder&quot;:&quot;&quot;,&quot;minimumResultsForSearch&quot;:-1}" required id="frequency" class="iwjmb-select_advanced" name="domain">
                                                                <option selected="">Please Select</option>
                                                                <option value="MD">MD</option>
                                                                <option value="MD">MS</option>

                                                            </select>
                                                        </div>
                                                    </div>                   
                                                </div>
                                                 
                                                <div class="col-md-6">
                                                    <div class="iwjmb-field iwjmb-text-wrapper  required">
                                                        <div class="iwjmb-label"><label class="theme-color" for="_iwj_address">Status *</label></div>
                                                        <div class="iwjmb-input ui-sortable">
                                                            <select data-options="{&quot;allowClear&quot;:false,&quot;width&quot;:&quot;none&quot;,&quot;placeholder&quot;:&quot;&quot;,&quot;minimumResultsForSearch&quot;:-1}" required id="frequency" class="iwjmb-select_advanced" name="status">
                                                                <option >Please Select</option>
                                                                <option value="pending"  >Pending</option>
                                                                <option value="approve">Approve</option>
                                                                <option value="disapprove">Disapprove</option>

                                                            </select>
                                                        </div>
                                                    </div> 
                                                </div>
                                            </div>
                                        <div class="iwj-respon-msg hide"></div>
                                        <input type="hidden" name="role" value="2">
                                        <div class="iwj-button-loader">
                                            <button type="submit" id="admin_submit" class="iwj-btn pull-right edit_btn iwj-btn-primary iwj-candidate-btn">Save</button>
                                        </div>
                                    </div>
                                </form>

                            </div>
                        </div>        
                    </div>
                </div>
            </div>    


             <!-- iwj-sidebar-sticky-->
            <div class="iwj-dashboard-sidebar">
                <div class="user-profile candidate clearfix">
                    <img alt='Peter Pham' src='<?php if($data1[0]->photo!="") { echo $data1[0]->photo; }else { echo base_url()."uploads/images/user.jpg";  } ?>' srcset='' class='avatar avatar-96 photo' height='96' width='96' />            <h4>
                        <span>Howdy!</span>
                            <?php echo $data1[0]->username; ?>          </h4>
                </div>
                <div class="iwj-dashboard-menu">
                       <?php $this->load->view('layout/sidebar.php') ?>    

                    </div>
                </div>
            </div>
        </div><!-- .entry-content -->
        <div class="clearfix"></div>
        <footer class="entry-footer ">
        </footer><!-- .entry-footer -->
    </article><!-- #post-## -->
</div>   
<?php $this->load->view('layout/footer.php') ?>

