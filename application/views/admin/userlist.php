<?php $this->load->view('layout/user_header.php') ?>
<div class="contents-main" id="contents-main" style="margin-top:3%">

    <article id="post-141" class="post-141 page type-page status-publish hentry">
        <div class="entry-content">
            <div class="iwj-dashboard clearfix">

                <div class="iwj-dashboard-menu-mobile">
                    <div class="dropdown">
                        <button class="btn btn-primary dropdown-toggle"  type="button" data-toggle="dropdown">Menu Dashboard <span class="caret"></span></button>


                        <?php $this->load->view('layout/menu.php') ?>       
                    </div>
                </div>

                <div class="iwj-dashboard-main save-jobs">
                    <div class="iwj-dashboard-main-inner">
                        <div class="iwj-save-jobs iwj-main-block">
                            <form method="post"  class="iwj-form-2 iwj-login-form1">

                                <div class="info-top" style="padding-bottom:0">
                                    <h3 class=""><?php echo "User List" ?></h3>

                                </div>

                            </form>

                            <div class="iwj-table-overflow-x">
                                <table class="table userlist" id="" class="display" cellspacing="0" width="100%">
                                    <thead>
                                        <tr>
                                            <th width="">ID</th>
                                            <th width="">User</th>
                                            <th width="">Email</th>
                                            <th width="">Phone</th>
                                            <?php  
                                            foreach ($data1 as $usr) {
                                            if ($usr->role_id == 1 || $usr->role_id == 2) { ?>
                                            <th width="">Credit Balance</th>
                                            <th width="">Date/Time</th>
                                            <?php } }?>
                                            <th width="">Status</th>
                                            <th width="" class="text-center">Action</th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                        <?php
                                        $i = 1;
                                        foreach ($userDetails as $user) {
                                            ?>
                                            <tr id="save-job-1212" class="save-job-item">
                                                <td>
                                                    <?= $i ?>
                                                </td>
                                                <td>
                                                    <?= $user->username ?>
                                                </td>
                                                <td>
                                                    <?= $user->email ?>
                                                </td>
                                                <td> 
                                                    <?php if($user->phone == ""){
                                                        echo "Yet to be Saved";
                                                    }else{
                                                        echo $user->phone; }?>
                                                </td>
                                                <?php foreach ($data1 as $usr) {
                                                if ($usr->role_id == 1 || $usr->role_id == 2) { ?>
                                                <td> 
                                                    <?php
                                                    foreach ($couponDetails as $user1) {
                                                        if($user->id == $user1->visitor_info){
                                                        
                                                            $val =  $user1->coupon_balance;
                                                            break;
                                                        }
                                                        else{
                                                            $val = 0;
                                                        }
                                                    } 
                                                    echo $val;
                                                    ?>
                                                </td>
                                                <td> 
                                                    <?= $user->created_at ?>
                                                </td>
                                        <?php } }?>
                                                <td> 
                                                    <?= $user->status ?>
                                                </td>
                                                <td class="text-center">
                                                    
                                                    <?php  if ($user->role_id == 1) { ?>
                                                    <a style="padding-right: 20px" href="<?php echo base_url(); ?>admin/updateUser/<?=$user->id?>"><i class="fa fa-pencil-square-o" title="Edit" aria-hidden="true"></i></a>
                                                    <a onclick="delUser(<?=$user->id?>)" href=""><i class="fa fa-trash-o" title="Delete" aria-hidden="true"></i></a>
                                                   
                                                     <?php }else{ ?>
                                                         <a style="padding-right: 20px" onclick="viewUser(<?=$user->id?>)"><i class="fa fa-eye" title="View Details" aria-hidden="true"></i></a>
                                                   
                                                   <?php  } ?>
                                                </td>
                                            </tr>
                                            <?php $i++;
                                        } ?>


                                    </tbody>
                                </table>
                            </div>
                            <div class="modal fade" id="iwj-confirm-undo-save-job" role="dialog">

                            </div>

                        </div>
                        <div class="clearfix"></div>

                    </div>

                </div>


                <!-- iwj-sidebar-sticky-->
                <div class="iwj-dashboard-sidebar">
                    <div class="user-profile candidate clearfix">
                        <img alt='Peter Pham' src='<?php if($data1[0]->photo!="") {  echo $data1[0]->photo; }else { echo base_url()."uploads/images/user.jpg";  } ?>' srcset='' class='avatar avatar-96 photo' height='96' width='96' />           
                        <h4>
                            <span>Howdy!</span>
                            <?php echo $data1[0]->username; ?>          
                        </h4>
                    </div>
                    <div class="iwj-dashboard-menu">
                        <?php $this->load->view('layout/sidebar.php') ?>   
                    </div>
                </div>
            </div>
        </div><!-- .entry-content -->
        
        <div class="modal fade" id="UserDetails">
    <div class="modal-dialog modal-lg">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">&times;</span><span class="sr-only">Close</span></button>
                <h4>Appointment Details</h4>
            </div>
            <div class="modal-body">

                <div class="iw-job-content iw-job-detail" style="background: #ffffff;" id="contents-main">
                    <div class="container">

                        <div class="col-sm-12 col-xs-12 col-lg-8 col-md-8">
                            <div class="job-detail">
                                <div class="job-detail-content">
                                    <div class="job-detail-info">
                                        <ul>
                                            <li class="job-type">
                                                <div class="content-inner">
                                                    <div class="left">
                                                        
                                                        <span class="title">User Name:</span>
                                                    </div>
                                                    <div id="user_user_name" class="content"></div>
                                                </div>
                                            </li>
                                            <li class="job-type">
                                                <div class="content-inner">
                                                    <div class="left">
                                                        
                                                        <span class="title">Phone :</span>
                                                    </div>
                                                    <div id="user_phone" class="content">$1,000 - $2,000</div>
                                                </div>
                                            </li>
                                            <li class="job-type">
                                                <div class="content-inner">
                                                    <div class="left">
                                                       
                                                        <span class="title">Email :</span>
                                                    </div>
                                                    <div id="user_email" class="content">
                                                        Full-Time                                        
                                                    </div>
                                                </div>
                                            </li>
                                            <li class="job-type">
                                                <div class="content-inner">
                                                    <div class="left">
                                                        
                                                        <span class="title">Status :</span>
                                                    </div>
                                                    <div id="user_status" class="content">
                                                        4 months ago                                        
                                                    </div>
                                                </div>
                                            </li>
                                           
                                            
                                        </ul>
                                    </div>
                                </div>

                            </div>
                        </div>

                    </div>

                </div>
            </div>
            <div class="modal-footer">
                
                <button type="button" class="btn btn-default" data-dismiss="modal">Cancel</button>
            </div>
        </div>
    </div>
</div>
        
        
        
        <div class="clearfix"></div>
        <footer class="entry-footer ">
        </footer><!-- .entry-footer -->
    </article><!-- #post-## -->
</div>
<script>
   function viewUser(id) {

        $.ajax({
            url: '<?php echo base_url(); ?>admin/getUser',
            data: {id: id},
            type: 'post',
            dataType: "json",
            success: function (data) {
                console.log(data);
                if (data != 0) {
                    $('#user_user_name').text(data.username);
                    $('#user_phone').text(data.phone);
                    $('#user_email').text(data.email);
                    $('#user_status').text(data.status);
                }
                $('#UserDetails').modal('show');
            }
        });

    } 
</script>
<?php $this->load->view('layout/footer.php') ?>