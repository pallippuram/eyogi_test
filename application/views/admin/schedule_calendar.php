<?php $this->load->view('layout/user_header.php')?>
<div class="contents-main" style="background-color: #f2f2f1;margin-top:3%"  id="contents-main" >
    <style>
        .btn-primary{
            width:20px
        }
        .ui-timepicker-wrapper{
            width:13%
        }
        .ui-timepicker-disabled{
            display: none
        }
    </style>
    <article id="post-141" class="post-141 page type-page status-publish hentry">
        <div class="entry-content">
            <div class="iwj-dashboard clearfix">

                <div class="iwj-dashboard-menu-mobile">
                    <div class="dropdown">
                        <button class="btn btn-primary dropdown-toggle"  type="button" data-toggle="dropdown">Menu Dashboard <span class="caret"></span></button>

                        <?php $this->load->view('layout/menu.php') ?>


                    </div>
                </div>

                <div class="iwj-dashboard-main1 overview">
                    <div class="iwj-dashboard-main-inner1">
                        <div class="iwj-overview iwj-profile clearfix">
                            <input type="hidden" id='min'>
                            <input type="hidden" id='max'>
                            <input type="hidden" id='dur'>
                            <div style="background-color: #fff;padding: 30px 0px" class="iwj-edit-candidate-profile iwj-edit-profile-page">
                                <div class="alert"></div>
                                <div class="row clearfix">
                                    <div class="col-md-12 column">

                                        <div id='calendar'></div>
                                    </div>
                                </div>
                            </div>
                        </div>

                    </div>
                </div>
                <div id="" class="modal fade add_event_popup">
                    <div class="modal-dialog">
                        <div class="modal-content">
                            <div class="modal-header">
                                <button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">&times;</span><span class="sr-only">Close</span></button>
                                <h4>Schedule Appointment</h4>
                            </div>
                            <div class="modal-body">
                                <div style="color:red;display: none" class="error text-center"></div>
                                <form class="form-horizontal" >

                                    <input type="hidden" id="start">
                                    <input type="hidden" id="date">
                                    <input type="hidden" id="end">
                                    <input type="hidden" id="schedule_table_id">
                                    <input type="hidden" id="schedule_date">
                                    <input type="hidden" id="schedule_id" value="">
                                    <input type="hidden" value="<?php
                                    if ($this->uri->segment(3) == "") {
                                        echo $data1[0]->id;
                                    } else {
                                        echo $this->uri->segment(3);
                                    }
                                    ?>" class="user_id" id="user_id">
                                    <div class="form-group">
                                        <label class="col-md-4 control-label" for="title">User</label>
                                        <div class="col-md-6" id="div_username">
                                            <select disabled="" data-options="{&quot;allowClear&quot;:false,&quot;width&quot;:&quot;none&quot;,&quot;placeholder&quot;:&quot;&quot;,&quot;minimumResultsForSearch&quot;:-1}" required id="user" class="iwjmb-select_advanced" name="gender">
                                                <option value="" >Select</option>
                                                <?php foreach ($userDetails as $user) { ?>
                                                    <option value="<?= $user->id ?>" ><?= $user->username ?></option>
                                                <?php }
                                                ?> 


                                            </select>

                                        </div>

                                        <div class="col-md-6" style="display:none">
                                            <select  data-options="{&quot;allowClear&quot;:false,&quot;width&quot;:&quot;none&quot;,&quot;placeholder&quot;:&quot;&quot;,&quot;minimumResultsForSearch&quot;:-1}" required id="user_listing" class="iwjmb-select_advanced" name="gender">
                                                <option value="" >Select</option>
                                                <?php foreach ($userDetails as $user) { ?>
                                                    <option value="<?= $user->id ?>" ><?= $user->username ?></option>
                                                <?php }
                                                ?> 


                                            </select>

                                        </div>

                                        <span style="color:red;display: none;margin-left: 20px;font-weight: bold;">This slot is already booked!!</span>
                                    </div>                            
                                    <div class="form-group">
                                        <label class="col-md-4 control-label" for="description">Note</label>
                                        <div class="col-md-5">
                                            <textarea style="resize: none;width: 270px;height: 73px;" class="form-control" cols="15" id="description" name="description"></textarea>
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <label class="col-md-4 control-label" for="title">Time Starts</label>
                                        <div class="col-md-4">
<!--                                            <input type="text" id="time_slot_start" disabled="" class="form-control iwjmb-text" value="">-->
                                    <input style="width: 270px;" id="startTime" class="form-control iwjmb-text" type="text" />

                                        </div>
                                        <span style="color:red;display: none;margin-left: 20px;font-weight: bold;">This slot is already booked!!</span>
                                    </div>  
                                    <div class="form-group">
                                        <label class="col-md-4 control-label" for="title">Time Ends</label>
                                        <div class="col-md-4">
                                            
                                            <input style="width: 270px;" id="endTime"  class="form-control iwjmb-text" type="text" />
                                        </div>
                                        <span style="color:red;display: none;margin-left: 20px;font-weight: bold;">This slot is unavailable!!</span>
                                    </div> 
<!--                                    <div class="form-group">
                                        <label class="col-md-4 control-label" for="title">Time Ends</label>
                                        <div class="col-md-4">
                                            <input type="text" disabled="" id="time_slot_ends" class="form-control iwjmb-text" value="">


                                        </div>
                                        <span style="color:red;display: none;margin-left: 20px;font-weight: bold;">This slot is already booked!!</span>
                                    </div> -->
                                    <div id="reschedule_div" style="display: none">
                                        <div class="form-group">
                                            <label class="col-md-4 control-label" for="title">Choose Date</label>
                                            <div class="col-md-4">
                                                <div class='input-group date datetimepicker_re3' id='datetimepicker_re'>
                                                    <input style="width: 232px;" type='text' class="form-control" />
                                                    <span class="input-group-addon">
                                                        <span class="glyphicon glyphicon-calendar"></span>
                                                    </span>
                                                </div>
                                            </div>
                                            
                                            <span style="color:red;display: none;margin-left: 87px;font-weight: bold;padding: 121px;">Counsellor not available on selected day!!</span>
                                        </div>
<!--                                        <div class="form-group">
                                            <label class="col-md-4 control-label" for="title">Counselor Availability</label>
                                            <div class="col-md-4">
                                                <select data-options="{&quot;allowClear&quot;:false,&quot;width&quot;:&quot;none&quot;,&quot;placeholder&quot;:&quot;&quot;,&quot;minimumResultsForSearch&quot;:-1}" required id="reschedule_id" class="iwjmb-select_advanced" name="gender">
                                                    <option value="" >Select</option>
                                                    <?php //foreach ($schedule as $user) {
                                                        ?>
                                                        <option value="<?php//$user->id ?>" ><?php //echo $user->time_starts . ' - ' . $user->time_ends ?></option>
                                                    <?php // }
                                                    ?> 


                                                </select>

                                            </div>
                                            <span style="color:red;display: none;margin-left: 20px;font-weight: bold;">This slot is already booked!!</span>
                                        </div>-->

                                        <div class="form-group">
                                        <label class="col-md-4 control-label" for="title">Time Starts</label>
                                        <div class="col-md-4">
<!--                                            <input type="text" id="time_slot_start" disabled="" class="form-control iwjmb-text" value="">-->
                                    <input style="width: 270px;" id="startTime_re" class="form-control iwjmb-text counselor_availabilty" type="text" />

                                        </div>
                                        <span style="color:red;display: none;margin-left: 20px;font-weight: bold;">This slot is already booked!!</span>
                                    </div>  
                                    <div class="form-group">
                                        <label class="col-md-4 control-label" for="title">Time Ends</label>
                                        <div class="col-md-4">
                                            
                                            <input style="width: 270px;" id="endTime_re"  class="form-control iwjmb-text" type="text" />
                                        </div>
                                        <span style="color:red;display: none;margin-left: 20px;font-weight: bold;">This slot is unavailable!!</span>
                                    </div>
                                        <div class="form-group">
                                            <label class="col-md-4 control-label" for="description">Reason</label>
                                            <div class="col-md-5">
                                                <textarea style="resize: none;width: 270px;height: 73px;" class="form-control" cols="15" id="reason" name="reason"></textarea>
                                            </div>
                                        </div>
                                    </div>
                                    <!--                                    <div class="form-group">
                                                                            <label class="col-md-4 control-label" for="color">Color</label>
                                                                            <div class="col-md-4">
                                                                                <input id="color" name="color" type="text" class="form-control input-md" readonly="readonly" />
                                                                                <span class="help-block">Click to pick a color</span>
                                                                            </div>
                                                                        </div>-->
                                </form>
                            </div>
                            <div class="modal-footer">
                                <button type="button" class="btn btn-default" data-dismiss="modal">Cancel</button>
                            </div>
                        </div>
                    </div>
                </div>
                <!-- iwj-sidebar-sticky-->
                <div class="iwj-dashboard-sidebar">
                    <div class="user-profile candidate clearfix">
                        <img alt='Peter Pham' src='<?php
                        if ($data1[0]->photo != "") {
                            echo $data1[0]->photo;
                        } else {
                            echo base_url() . "uploads/images/user.jpg";
                        }
                        ?>' srcset='' class='avatar avatar-96 photo' height='96' width='96' />           
                        <h4>
                            <span>Howdy!</span>
<?php echo $data1[0]->username; ?>          
                        </h4>
                    </div>
                    <div class="iwj-dashboard-menu">
<?php $this->load->view('layout/sidebar.php') ?>

                    </div>
                </div>
            </div>
        </div><!-- .entry-content -->
        <div class="clearfix"></div>
        <footer class="entry-footer ">
        </footer><!-- .entry-footer -->
    </article><!-- #post-## -->
</div>
<script src='<?php echo base_url(); ?>js/jquery.timepicker.js'></script>
<script>
   
</script>

<?php $this->load->view('layout/footer.php') ?>