<?php $this->load->view('layout/user_header.php') ?>
<link rel="stylesheet" href="//code.jquery.com/ui/1.12.1/themes/base/jquery-ui.css">

<div class="contents-main" id="contents-main" style="margin-top:3%">

    <article id="post-141" class="post-141 page type-page status-publish hentry">
        <div class="entry-content">
            <div class="iwj-dashboard clearfix">

                <div class="iwj-dashboard-menu-mobile">
                    <div class="dropdown">
                        <button class="btn btn-primary dropdown-toggle"  type="button" data-toggle="dropdown">Menu Dashboard <span class="caret"></span></button>


                        <?php $this->load->view('layout/menu.php') ?>       
                    </div>
                </div>

                <div class="iwj-dashboard-main save-jobs">
                    <div class="iwj-dashboard-main-inner">
                        <div class="iwj-save-jobs iwj-main-block">
                            <form method="post"  class="iwj-form-2 iwj-login-form1">

                                <div class="info-top" style="padding-bottom:0">
                                    <h3 class=""><?php echo "Counselor Payment Details" ?></h3>

                                </div>

                            </form>

                            <div class="iwj-table-overflow-x">
                                <p id="date_filter">
                                    <span id="date-label-from" class="date-label">From: </span><input class="date_range_filter date" type="text" id="datepicker_from" />
                                    <span id="date-label-to" class="date-label">To: </span><input class="date_range_filter date" type="text" id="datepicker_to" />
                                </p>
                                <table class="table userlist" id="counsdata" class="display" cellspacing="0" width="100%">
                                    <thead>
                                        <tr>
                                            <th width="">ID</th>
                                            <th width="">Counselor</th>
                                            <th width="">Duration</th>
                                            <th width="">Schedules</th>
                                            <th width="">Remuneration</th>
                                            <th width="">Status</th>
                                            <th width="" class="text-center">Action</th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                        <?php
                                        $i = 1;
                                        foreach ($couns_payments as $user) {
                                            ?>
                                            <tr id="save-job-1212" class="save-job-item">
                                                <td>
                                                    <?= $i ?>
                                                </td>
                                                <td>
                                                    <?= $user->username ?>
                                                </td>
                                                <td>
                                                    <?php
                                                        $init = $user->dur;
                                                        $hours = floor($init / 3600);
                                                        $minutes = floor(($init / 60) % 60);
                                                        $seconds = $init % 60;
                                                        
                                                        echo "$hours:$minutes:$seconds";
                                                        ?>
                                                </td>
                                                <td> 
                                                    <?= $user->count ?>
                                                </td>
                                                <td> 
                                                    <?= $user->renum ?>
                                                </td>
                                                <td> 
                                                    <?= $user->status ?>
                                                </td>
                                                <td class="text-center">
                                                    
                                                    
                                                <a style="padding-right: 20px" onclick="viewUser(<?=$user->counselor_id?>)"><i class="fa fa-eye" title="View Details" aria-hidden="true"></i></a>
                                                   
                                                   
                                                </td>
                                            </tr>
                                            <?php $i++;
                                        } ?>


                                    </tbody>
                                </table>
                            </div>
                            <div class="modal fade" id="iwj-confirm-undo-save-job" role="dialog">

                            </div>

                        </div>
                        <div class="clearfix"></div>

                    </div>

                </div>


                <!-- iwj-sidebar-sticky-->
                <div class="iwj-dashboard-sidebar">
                    <div class="user-profile candidate clearfix">
                        <img alt='Peter Pham' src='<?php if($data1[0]->photo!="") {  echo $data1[0]->photo; }else { echo base_url()."uploads/images/user.jpg";  } ?>' srcset='' class='avatar avatar-96 photo' height='96' width='96' />           
                        <h4>
                            <span>Howdy!</span>
                            <?php echo $data1[0]->username; ?>          
                        </h4>
                    </div>
                    <div class="iwj-dashboard-menu">
                        <?php $this->load->view('layout/sidebar.php') ?>   
                    </div>
                </div>
            </div>
        </div><!-- .entry-content -->
        
        <div id="classModal" class="modal fade bs-example-modal-lg" tabindex="-1" role="dialog" aria-labelledby="classInfo" aria-hidden="true">
            <div class="modal-dialog modal-lg">
                <div class="modal-content">
                    <div class="modal-header">
                        <div class="info-top" style="padding-bottom:0">
                            <h3 class=""><center><?php echo "List of Appointments and Payments" ?></center></h3>
                        </div>
                    </div>
                    <div class="modal-body">
                        <div class="iwj-table-overflow-x">
                           

                        <div class="iwj-table-overflow-x" id="counspays" >
                           
                        </div>
                    </div>
                    <div class="modal fade" id="iwj-confirm-undo-save-job" role="dialog">
                    </div>
                </div>
                <div class="clearfix"></div>
                <div class="modal-footer">
                </div>
            </div>
        </div>
    </div>
    <div class="clearfix"></div>
    <footer class="entry-footer ">
    </footer><!-- .entry-footer -->
    </article><!-- #post-## -->
</div>
<script>
   function viewUser(id) {
        $.ajax({
            url: '<?php echo base_url(); ?>admin/CounselorPaymentdetails',
            data: {id: id},
            type: 'post',
            success: function (data) {
             
                $("#counspays").html(data);

                $('#classModal').modal('show');
            }
        });

    } 
    
    $( function() {

        $("#datepicker_from").datepicker({
            dateFormat: "yy-mm-dd",
            //minDate: 0,
            onSelect: function (date) {
                var date2 = $('#datepicker_from').datepicker('getDate');
                date2.setDate(date2.getDate() + 1);
                //   $('#datepicker_to').datepicker('setDate', date2);
                //sets minDate to dt1 date + 1
                $('#datepicker_to').datepicker('option', 'minDate', date2);
                var fDate = $(this).val();
                var todate=$("#datepicker_to").val();
                if(todate!=""){

                    $.ajax({
                    type: "POST",
                    url: "<?php echo base_url() ?>admin/counspaymntsbydate",
                    data: {date1: fDate,date2: todate},
                    success: function (data) {
                        console.log(data);
                        $("#counsdata").html("");
                        $("#counsdata").html(data);
                        $('#counsdata').DataTable({ 
                            "destroy": true, //use for reinitialize datatable
                        });
           
                    }
                    });
                }
            }
        });
        $('#datepicker_to').datepicker({
            dateFormat: "yy-mm-dd",
            onClose: function () {
            var dt1 = $('#datepicker_from').datepicker('getDate');
            var dt2 = $('#datepicker_to').datepicker('getDate');
            //check to prevent a user from entering a date below date of dt1
            if (dt2 <= dt1) {
               var minDate = $('#datepicker_to').datepicker('option', 'minDate');
               $('#datepicker_to').datepicker('setDate', minDate);
           }

            var tDate = $(this).val();
             var fDate=$("#datepicker_from").val();
            //alert(tDate);
                     $.ajax({
                   type: "POST",
                   url: "<?php echo base_url() ?>admin/counspaymntsbydate",
                   data: {date1: fDate,date2: tDate},
                   success: function (data) {
                       console.log(data);
                       $("#counsdata").html("");
                       $("#counsdata").html(data);
                       $('#counsdata').DataTable({ 
                            "destroy": true, //use for reinitialize datatable
                  });
           
                }
            });
        }
   });
   });
</script>
<?php $this->load->view('layout/footer.php') ?>