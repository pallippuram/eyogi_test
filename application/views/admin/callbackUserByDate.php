 <table class="table userlist" id="userData" class="display" cellspacing="0" width="100%">
                                    <thead>
                                        <tr>
                                            <th width="">ID</th>
                                            <th width="">User</th>
                                            <th width="">Email</th>
                                            <th width="">Phone</th>
                                            <th width="">Message</th>
                                            <th width="" class="text-center">Action</th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                        <?php
                                        $i = 1;
                                        foreach ($userDetails as $user) {
                                            ?>
                                            <tr id="save-job-1212" class="save-job-item">
                                                <td>
                                                    <?= $i ?>
                                                </td>
                                                <td>
                                                    <?= $user->name ?>
                                                </td>
                                                <td>
                                                    <?= $user->email ?>
                                                </td>
                                                <td> 
                                                    <?= $user->phone ?>
                                                </td>
                                                <td> 
                                                    <?= $user->message ?>
                                                </td>
                                                <td class="text-center">
                                                     <a style="padding-right: 20px" onclick="callBackDetails(<?= $user->id ?>)"><i class="fa fa-eye" title="View Details" aria-hidden="true"></i></a>
                                                    <a onclick="delCallbackUsers(<?=$user->id?>)" href=""><i class="fa fa-trash-o" title="Delete" aria-hidden="true"></i></a>
                                                </td>
                                            </tr>
                                            <?php $i++;
                                        } ?>


                                    </tbody>
                                </table>
