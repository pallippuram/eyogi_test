<?php $this->load->view('layout/user_header.php') ?>

<style>
    .switch {
        position: relative;
        display: inline-block;
        width: 60px;
        height: 34px;
    }

    .switch input {display:none;}

    .slider {
        position: absolute;
        cursor: pointer;
        top: 0;
        left: 0;
        right: 0;
        bottom: 0;
        background-color: #ccc;
        -webkit-transition: .4s;
        transition: .4s;
    }

    .slider:before {
        position: absolute;
        content: "";
        height: 26px;
        width: 26px;
        left: 4px;
        bottom: 4px;
        background-color: white;
        -webkit-transition: .4s;
        transition: .4s;
    }

    input:checked + .slider {
        background-color: #2196F3;
    }

    input:focus + .slider {
        box-shadow: 0 0 1px #2196F3;
    }

    input:checked + .slider:before {
        -webkit-transform: translateX(26px);
        -ms-transform: translateX(26px);
        transform: translateX(26px);
    }

    /* Rounded sliders */
    .slider.round {
        border-radius: 34px;
    }

    .slider.round:before {
        border-radius: 50%;
    }
    .beta td {
        width:25% important;
    }
    .ui-timepicker-wrapper{
        width: 13%;
    }
</style>
<div class="contents-main" id="contents-main" style="margin-top:3%">

    <article id="post-141" class="post-141 page type-page status-publish hentry">
        <div class="entry-content">
            <div class="iwj-dashboard clearfix">

                <div class="iwj-dashboard-menu-mobile">
                    <div class="dropdown">
                        <button class="btn btn-primary dropdown-toggle"  type="button" data-toggle="dropdown">Menu Dashboard <span class="caret"></span></button>


                        <?php $this->load->view('layout/menu.php') ?>
                    </div>
                </div>


                <!-- date by report starts-->

                <div class="iwj-dashboard-main save-jobs" id="datebyreport">
                    <div class="iwj-dashboard-main-inner">
                        <div class="iwj-save-jobs iwj-main-block">
                            <form method="post"  class="iwj-form-2 iwj-login-form1">
                                <div class="info-top" style="padding-bottom:0">
                                    <h3 class=""><?php echo $data2[0]->username; ?></h3>
                                </div>
                            </form>
                            <form method="post"  class="iwj-form-2 iwj-login-form1">
                                <div class="info-top" style="padding-bottom:0">
                                    <h3 class=""><?php echo "Set Availability" ?></h3>
                                </div>
                            </form>

                            <div class="iwj-table-overflow-x">

                                <table class="table beta" id="logdata" class="display" cellspacing="0" width="100%">
                                    <thead>

                                    </thead>
                                    <tbody>
                                        <tr id="sunday">
                                            <td><label style="margin-top: 5px;">Sunday</label></td>
                                            <td>

                                                <label class="switch">
                                                    <input type="checkbox" <?php
                                                    if (!empty($timing)) {
                                                        if (in_array("sunday", $timing_arr)) {
                                                            echo "checked";
                                                        }
                                                    }
                                                    ?> class="chk" name="sunday_chk" >
                                                    <span class="slider round"></span>
                                                </label>
                                            </td>
                                            <td >


                                                <div style="display:inline-flex">
                                                    <input type="text"  class="iwjmb-text form-control amt_text sunday_start" value="<?php
                                                    if (!empty($timing)) {
                                                        foreach ($timing as $time) {
                                                            if ($time->day == "sunday") {
                                                                echo $time->time_starts;
                                                            }
                                                        }
                                                    }
                                                    ?>" id="sunday_start"   name="email" placeholder="Time Starts">
                                                </div>                     
                                            </td> <td><label style="margin-top: 5px;">to</label></td>
                                            <td >


                                                <div >
                                                    <input type="text" style="width:45%" class="iwjmb-text form-control amt_text sunday_end" value="<?php
                                                    if (!empty($timing)) {
                                                        foreach ($timing as $time) {
                                                            if ($time->day == "sunday") {
                                                                echo $time->time_ends;
                                                            }
                                                        }
                                                    }
                                                    ?>" id="sunday_end" name="email" placeholder="Time Ends">
                                                     <span style="color:red;display: none;font-weight: bold;">Time end should be greater than time starts</span>
                                                </div>                     
                                            </td> 
                                        </tr>
                                        <tr id="monday">
                                            <td><label style="margin-top: 5px;">Monday</label></td>
                                            <td>

                                                <label class="switch">
                                                    <input type="checkbox"  class="chk" <?php
                                                    if (!empty($timing)) {
                                                        if (in_array("monday", $timing_arr)) {
                                                            echo "checked";
                                                        }
                                                    }
                                                    ?> name="monday_chk" >
                                                    <span class="slider round"></span>
                                                </label>
                                            </td>
                                            <td >


                                                <div style="display:inline-flex">
                                                    <input type="text" class="iwjmb-text form-control amt_text monday_start" value="<?php
                                                    if (!empty($timing)) {
                                                        foreach ($timing as $time) {
                                                            if ($time->day == "monday") {
                                                                echo $time->time_starts;
                                                            }
                                                        }
                                                    }
                                                    ?>"  id="monday_start"  name="email" placeholder="Time Starts">
                                                </div>                     
                                            </td> <td><label style="margin-top: 5px;">to</label></td>
                                            <td >


                                                <div >
                                                    <input type="text" style="width:45%" class="iwjmb-text form-control amt_text monday_end" id="monday_end" value="<?php
                                                    if (!empty($timing)) {
                                                        foreach ($timing as $time) {
                                                            if ($time->day == "monday") {
                                                                echo $time->time_ends;
                                                            }
                                                        }
                                                    }
                                                    ?>"   name="email" placeholder="Time Ends">
                                                    <span style="color:red;display: none;font-weight: bold;">Time end should be greater than time starts</span>
                                                </div>                     
                                            </td> 
                                        </tr>

                                        <tr id="tuesday">
                                            <td><label style="margin-top: 5px;">Tuesday</label></td>
                                            <td>

                                                <label class="switch">
                                                    <input type="checkbox"  class="chk" <?php
                                                    if (!empty($timing)) {
                                                        if (in_array("tuesday", $timing_arr)) {
                                                            echo "checked";
                                                        }
                                                    }
                                                    ?>  name="tuesday_chk" >
                                                    <span class="slider round"></span>
                                                </label>
                                            </td>
                                            <td >


                                                <div style="display:inline-flex">
                                                    <input type="text"  class="iwjmb-text form-control amt_text tuesday_start" id="tuesday_start" value="<?php
                                                    if (!empty($timing)) {
                                                        foreach ($timing as $time) {
                                                            if ($time->day == "tuesday") {
                                                                echo $time->time_starts;
                                                            }
                                                        }
                                                    }
                                                    ?>" name="email" placeholder="Time Starts">
                                                </div>                     
                                            </td> <td><label style="margin-top: 5px;">to</label></td>
                                            <td >


                                                <div >
                                                    <input type="text" style="width:45%" class="iwjmb-text form-control amt_text tuesday_end" id="tuesday_end" value="<?php
                                                    if (!empty($timing)) {
                                                        foreach ($timing as $time) {
                                                            if ($time->day == "tuesday") {
                                                                echo $time->time_ends;
                                                            }
                                                        }
                                                    }
                                                    ?>" name="email" placeholder="Time Ends">
                                                    <span style="color:red;display: none;font-weight: bold;">Time end should be greater than time starts</span>
                                                </div>                     
                                            </td> 
                                        </tr>
                                        <tr id="wednesday">
                                            <td><label style="margin-top: 5px;">Wednesday</label></td>
                                            <td>
                                                <label class="switch">
                                                    <input  class="chk" type="checkbox"
                                                    <?php
                                                    if (!empty($timing)) {
                                                        if (in_array("wednesday", $timing_arr)) {
                                                            echo "checked";
                                                        }
                                                    }
                                                    ?>

                                                            name="wednesday_chk" >
                                                    <span class="slider round"></span>
                                                </label>
                                            </td>
                                            <td >


                                                <div style="display:inline-flex">
                                                    <input type="text"  class="iwjmb-text form-control amt_text wednesday_start" id="wednesday_start" value="<?php
                                                    if (!empty($timing)) {
                                                        foreach ($timing as $time) {
                                                            if ($time->day == "wednesday") {
                                                                echo $time->time_starts;
                                                            }
                                                        }
                                                    }
                                                    ?>"  name="email" placeholder="Time starts">
                                                </div>                     
                                            </td> <td><label style="margin-top: 5px;">to</label></td>
                                            <td >


                                                <div >
                                                    <input type="text" style="width:45%" class="iwjmb-text form-control amt_text wednesday_end" id="wednesday_end" value="<?php
                                                    if (!empty($timing)) {

                                                        foreach ($timing as $time) {
                                                            if ($time->day == "wednesday") {
                                                                echo $time->time_ends;
                                                            }
                                                        }
                                                    }
                                                    ?>"  name="email" placeholder="Time Ends">
                                                    <span style="color:red;display: none;font-weight: bold;">Time end should be greater than time starts</span>
                                                </div>                     
                                            </td> 
                                        </tr>

                                        <tr id="thursday">
                                            <td><label style="margin-top: 5px;">Thursday</label></td>
                                            <td>
                                                <label class="switch">
                                                    <input  class="chk" type="checkbox" <?php
                                                    if (!empty($timing)) {
                                                        if (in_array("thursday", $timing_arr)) {
                                                            echo "checked";
                                                        }
                                                    }
                                                    ?> name="thursday_chk" >
                                                    <span class="slider round"></span>
                                                </label>
                                            </td>
                                            <td >


                                                <div style="display:inline-flex">
                                                    <input type="text" class="iwjmb-text form-control amt_text thursday_start" value="<?php
                                                    if (!empty($timing)) {
                                                        foreach ($timing as $time) {
                                                            if ($time->day == "thursday") {
                                                                echo $time->time_starts;
                                                            }
                                                        }
                                                    }
                                                    ?>"  id="thursday_start" name="email" placeholder="Time Starts">
                                                </div>                     
                                            </td> <td><label style="margin-top: 5px;">to</label></td>
                                            <td >


                                                <div >
                                                    <input type="text" style="width:45%" class="iwjmb-text form-control amt_text thursday_end" value="<?php
                                                    if (!empty($timing)) {
                                                        foreach ($timing as $time) {
                                                            if ($time->day == "thursday") {
                                                                echo $time->time_ends;
                                                            }
                                                        }
                                                    }
                                                    ?>" id="thursday_end"   name="email" placeholder="Time Ends">
                                                    <span style="color:red;display: none;font-weight: bold;">Time end should be greater than time starts</span>
                                                </div>                     
                                            </td> 
                                        </tr>
                                        <tr id="friday">
                                            <td><label style="margin-top: 5px;">Friday</label></td>
                                            <td>
                                                <label class="switch">
                                                    <input  class="chk" type="checkbox" <?php
                                                    if (!empty($timing)) {
                                                        if (in_array("friday", $timing_arr)) {
                                                            echo "checked";
                                                        }
                                                    }
                                                    ?> name="friday_chk" >
                                                    <span class="slider round"></span>
                                                </label>
                                            </td>
                                            <td >


                                                <div style="display:inline-flex">
                                                    <input type="text" class="iwjmb-text form-control amt_text friday_start" id="friday_start" value="<?php
                                                    if (!empty($timing)) {
                                                        foreach ($timing as $time) {
                                                            if ($time->day == "friday") {
                                                                echo $time->time_starts;
                                                            }
                                                        }
                                                    }
                                                    ?>"   name="email" placeholder="Time Starts">
                                                </div>                     
                                            </td> <td><label style="margin-top: 5px;">to</label></td>
                                            <td >


                                                <div >
                                                    <input type="text" style="width:45%" class="iwjmb-text form-control amt_text friday_end" id="friday_end" value="<?php
                                                    if (!empty($timing)) {
                                                        foreach ($timing as $time) {
                                                            if ($time->day == "friday") {
                                                                echo $time->time_ends;
                                                            }
                                                        }
                                                    }
                                                    ?>"  name="email" placeholder="Time Ends">
                                                    <span style="color:red;display: none;font-weight: bold;">Time end should be greater than time starts</span>

                                                </div>                     
                                            </td> 
                                        </tr>
                                        <tr id="saturday">
                                            <td><label style="margin-top: 5px;">Saturday</label></td>
                                            <td>
                                                <label class="switch">
                                                    <input  class="chk" type="checkbox" <?php
                                                    if (!empty($timing)) {
                                                        if (in_array("saturday", $timing_arr)) {
                                                            echo "checked";
                                                        }
                                                    }
                                                    ?> name="saturday_chk" >
                                                    <span class="slider round"></span>
                                                </label>
                                            </td>
                                            <td >


                                                <div style="display:inline-flex">
                                                    <input type="text"  class="iwjmb-text form-control amt_text saturday_start" id="saturday_start" value="<?php
                                                    if (!empty($timing)) {
                                                        foreach ($timing as $time) {
                                                            if ($time->day == "saturday") {
                                                                echo $time->time_starts;
                                                            }
                                                        }
                                                    }
                                                    ?>"  name="email" placeholder="Time Starts">
                                                </div>                     
                                            </td> <td><label style="margin-top: 5px;">to</label></td>
                                            <td >


                                                <div >
                                                    <input type="text" style="width:45%" class="iwjmb-text form-control amt_text saturday_end" id="saturday_end" value="<?php
                                                    if (!empty($timing)) {
                                                        foreach ($timing as $time) {
                                                            if ($time->day == "saturday") {
                                                                echo $time->time_ends;
                                                            }
                                                        }
                                                    }
                                                    ?>"   name="email" placeholder="Time Ends">
                                                    <span style="color:red;display: none;font-weight: bold;">Time end should be greater than time starts</span>
                                                </div>                     
                                            </td> 
                                        </tr>

                                    </tbody>
                                </table>
                            </div>
                            <div class="iwj-button-loader">
                                <button type="button" id="save_schedule" class="iwj-btn pull-right edit_btn iwj-btn-primary iwj-candidate-btn">Save</button>
                            </div>

                        </div>
                        <div class="clearfix"></div>

                    </div>

                </div>

                <!-- date by report close-->



                <input type="hidden" value="<?php
                if ($this->uri->segment(3) == "") {
                    echo $data1[0]->id;
                } else {
                    echo $this->uri->segment(3);
                }
                ?>" id="user_data">

                <!-- iwj-sidebar-sticky-->
                <div class="iwj-dashboard-sidebar">
                    <div class="user-profile candidate clearfix">
                        <img alt='Peter Pham' src='<?php echo $data1[0]->photo; ?>' srcset='' class='avatar avatar-96 photo' height='96' width='96' />            
                        <h4>
                            <span>Howdy!</span>
                            <?php echo $data1[0]->username; ?>          
                        </h4>
                    </div>
                    <div class="iwj-dashboard-menu">
                        <?php $this->load->view('layout/sidebar.php') ?>
                    </div>
                </div>
            </div>
        </div><!-- .entry-content -->
        <div class="clearfix"></div>
        <footer class="entry-footer ">
        </footer><!-- .entry-footer -->
    </article><!-- #post-## -->
</div>
<script src='<?php echo base_url(); ?>js/jquery.timepicker.js'></script>
<script>

    $('#save_schedule').on('click', function () {
        var dataStore = [];
        var ret = true;
        var user_id = $('#user_data').val()
        $("#logdata tr").each(function (index, element) {
            var obj = {};
            var id = $(this).attr('id');
            console.log(id)
            if ($(this).find('td').find("input[name$='" + id + "_chk']").is(':checked')) {
                obj.day = id;
                obj.start = $(this).find('td').find("input[id$='" + id + "_start']").val();
                obj.end = $(this).find('td').find("input[id$='" + id + "_end']").val();
                var stt = new Date("November 13, 2013 " +  obj.start);
                stt = stt.getTime();

                var endt = new Date("November 13, 2013 " +  obj.end);
                endt = endt.getTime();


                if (stt < endt) {
                    dataStore.push(obj);
                } else {
                    $(this).find('td').find("input[id$='" + id + "_end']").addClass('error');
                    $(this).find('td').find("input[id$='" + id + "_end']").next('span').show()
                    ret = false;
                }
            }

        });

        if (ret != false) {
            $.ajax({
                url: '<?php echo base_url(); ?>admin/save_schedule',
                data: {dataStore: dataStore, user_id: user_id},
                type: 'post',

                success: function (data) {
                    if (data == 1) {
                        window.location.href = '<?php echo base_url(); ?>' + "admin/counselorlist";
                    }
                    if (data == 1) {
                        window.location.href = '<?php echo base_url(); ?>' + "admin/availability/" + user_id;
                    }

                }
            });
        }
    });

    $('.amt_text').timepicker({
        'step': 15,
        'timeFormat': 'h:i A',

    });

    $('.amt_text').on('changeTime', function () {
        var str = $(this).attr('id')
        var minTime = $(this).val();

        var arr = str.split('_');
        $('#' + arr[0] + '_end').empty()
        $('#' + arr[0] + '_end').timepicker({
            'minTime': $(this).val(),
            'step': 15,
            'timeFormat': 'h:i A',

        });
    });

//    $('.chk').click(function () {
//        
//        if($(this).find(':checkbox').attr('checked', false)){
//            var id= $(this).find(':checkbox').attr('id');
//            alert($(this).find(':checkbox').attr('id'))
//            $('#'+id+"_start").val();
//            $('#'+id+"_end").val();
//        }
//    });
</script>





<?php $this->load->view('layout/footer.php') ?>
  

