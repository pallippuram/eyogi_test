<?php $this->load->view('layout/user_header.php') ?>
<div class="contents-main" id="contents-main" style="margin-top:3%">

    <article id="post-141" class="post-141 page type-page status-publish hentry">
        <div class="entry-content">
            <div class="iwj-dashboard clearfix">

                <div class="iwj-dashboard-menu-mobile">
                    <div class="dropdown">
                        <button class="btn btn-primary dropdown-toggle"  type="button" data-toggle="dropdown">Menu Dashboard <span class="caret"></span></button>

                        <?php $this->load->view('layout/menu.php') ?>    
                         
                    </div>
                </div>
                <div class="iwj-dashboard-main profile">
                    <div class="iwj-dashboard-main-inner">
                        <div class="iwj-profile clearfix">
                            <div class="iwj-edit-candidate-profile iwj-edit-profile-page">
                                <form method="post" enctype="multipart/form-data" action="<?php echo site_url('admin/save_coupon'); ?>" class="iwj-form-2 iwj-login-form1">
                                    <div class="form_error">
                                        <?php echo validation_errors(); ?>
                                    </div>
                                    <div class="iwj-block">
                                        <div class="basic-area iwj-block-inner">
                                            <div class="row">
                                                   <div class="col-md-6">
                                                        <div class="iwjmb-field iwjmb-text-wrapper  required">
                                                            <div class="iwjmb-label">
                                                                <label class="theme-color" for="coupon_name">Coupon Name *</label>
                                                            </div>
                                                            <div class="iwjmb-input ui-sortable">
                                                                <input size="30" placeholder="Enter your coupon name" value="" type="text" id="coupon_name" class="iwjmb-text " name="coupon_name">
                                                            </div>
                                                        </div>
                                                        <div class="iwjmb-field iwjmb-text-wrapper  required">
                                                            <div class="iwjmb-label">
                                                                <label class="theme-color" for="your_amount">Coupon Amount *</label>
                                                            </div>
                                                            <div class="iwjmb-input ui-sortable">
                                                                <input size="30" placeholder="Enter your coupon amount" value="" type="text" id="your_amount" class="iwjmb-text " name="amount">
                                                            </div>
                                                        </div>
                                                       
                                                        <div class="iwjmb-field iwjmb-email-wrapper  required">
                                                            <div class="iwjmb-label">
                                                                <label class="theme-color" for="coupon_code">Coupon Code *</label>
                                                                <button type="generate" id="gen_btn" class="iwj-btn pull-right edit_btn iwj-btn-primary iwj-candidate-btn">Generate</button>
                                                            </div>
                                                           
                                                            <div class="iwjmb-input ui-sortable">
                                                                
                                                               
                                                                <input  style="color: transparent;text-shadow: 0 0 0 black;" value="" autocomplete="off" placeholder="Generate your code" type="text" required="required" id="coupon_code" class="iwjmb-text" name="code">
                                                   
                                                            </div>
                                                           
                                                        </div> 
                                                        <div class="iwjmb-field iwjmb-text-wrapper  required"><div class="iwjmb-label"><label class="theme-color" for="_iwj_headline">Status *</label></div>
                                                            <div class="iwjmb-input ui-sortable">
                                                                <select data-options="{&quot;allowClear&quot;:false,&quot;width&quot;:&quot;none&quot;,&quot;placeholder&quot;:&quot;&quot;,&quot;minimumResultsForSearch&quot;:-1}" required id="frequency" class="iwjmb-select_advanced" name="status">
                                                                    <option selected="">Please Select</option>
                                                                     <option value="1">ACTIVE</option>
                                                                    <option value="2">INACTIVE</option>
                                                                </select>
                                                            </div>
                                                        </div>   
                                                    </div>
                                                    
                                            </div>
                                            <div class="iwj-respon-msg hide"></div>
                                            <div class="iwj-button-loader">
                                                <button type="submit" id="btn_sub" class="iwj-btn pull-right edit_btn iwj-btn-primary iwj-candidate-btn">Save</button>
                                            </div>
                                        </div>
                                    </div>
                                </form>
                            </div>
                        </div>        
                    </div>
                </div>


                <!-- iwj-sidebar-sticky-->
                <div class="iwj-dashboard-sidebar">
                    <div class="user-profile candidate clearfix">
                        <img alt='Peter Pham' src='<?php if($data1[0]->photo!="") { echo $data1[0]->photo; }else { echo base_url()."uploads/images/user.jpg";  } ?>' srcset='' class='avatar avatar-96 photo' height='96' width='96' />            <h4>
                            <span>Howdy!</span>
                            <?php echo $data1[0]->username; ?>          </h4>
                    </div>
                    <div class="iwj-dashboard-menu">
                         <?php $this->load->view('layout/sidebar.php') ?>   

                    </div>
                </div>
            </div>
        </div><!-- .entry-content -->
        <div class="clearfix"></div>
        <footer class="entry-footer ">
        </footer><!-- .entry-footer -->
    </article><!-- #post-## -->
</div>   
<?php $this->load->view('layout/footer.php') ?>
