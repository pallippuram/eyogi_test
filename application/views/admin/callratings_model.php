<div class="modal fade" id="callRatings">
    <div class="modal-dialog modal-lg">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">&times;</span><span class="sr-only">Close</span></button>
                <h4>Call Ratings</h4>
            </div>
            <div class="modal-body">

                <div class="iw-job-content iw-job-detail" style="background: #ffffff;" id="contents-main">
                    <div class="container">



 <div class="col-sm-12 profile_follow_editor">
    <div class="container p0">
        <div class="col-sm-12 profile_follow">
            <div class="col-sm-8 profile_padd">
                <div class="col-sm-9 p0">
                   
                    <span class="pull-left profile-pull-left">

                        <div id="ratinglist">

                        <?php 

                            if(count($ratings)>0){   

                        for($k=0;$k<count($ratings);$k++){ ?>

                          <h3>Counselor : <?php 

                          echo  $ratings[$k]['c_name'];
                        ?></h3>
                        <h5>Comments :</h5>
                           <p><?php echo $ratings[$k]['comment']; ?></p>

                <div class="star pointer" >
                <?php  $rating_value =  $ratings[$k]['comment_count']; ?>
                    <span class="<?php if ($rating_value >= 1) {
                        echo 'fa fa-star';
                    }else{ echo 'fa fa-star-o';} ?>"></span>
                    <span class="<?php if ($rating_value >=2) {
                        echo 'fa fa-star';
                    }else{ echo 'fa fa-star-o'; } ?>" ></span>
                    <span class="<?php if ($rating_value >=3) {
                        echo 'fa fa-star';
                    }else{ echo 'fa fa-star-o';} ?>"></span>
                    <span class="<?php if ($rating_value >=4) {
                        echo 'fa fa-star';
                    }else{ echo 'fa fa-star-o'; } ?>" ></span>
                    <span class="<?php if ($rating_value >=5) {
                        echo 'fa fa-star';
                    }else{ echo 'fa fa-star-o'; } ?>"></span> <?php  ?>

       
                </div>

                    <h5>Comments based on calls :</h5>
                  <p><?php echo $ratings[$k]['cstatus']; ?></p>

                   <div class="star pointer" >
                <?php  $rating_value = $ratings[$k]['call_count']; ?>
                    <span class="<?php if ($rating_value >= 6) {
                        echo 'fa fa-star';
                    }else{ echo 'fa fa-star-o';} ?>"></span>
                    <span class="<?php if ($rating_value >=7) {
                        echo 'fa fa-star';
                    }else{ echo 'fa fa-star-o'; } ?>" ></span>
                    <span class="<?php if ($rating_value >=8) {
                        echo 'fa fa-star';
                    }else{ echo 'fa fa-star-o';} ?>"></span>
                    <span class="<?php if ($rating_value >=9) {
                        echo 'fa fa-star';
                    }else{ echo 'fa fa-star-o'; } ?>" ></span>
                    <span class="<?php if ($rating_value >=10) {
                        echo 'fa fa-star';
                    }else{ echo 'fa fa-star-o'; } ?>"></span> <?php  ?>


                </div>

              <?php } } else { ?>

              <p>No ratings have made</p>
                 <?php } ?>

            </div>


                     
     
                      
                  
                    <div class="clear10"></div>
                    <div class="clear5"></div>
                  
                    <div class="clearfix"></div>
                </div>
                <div class="col-sm-3 p0" id="following-data">
                    <ul class="follow_ul">
                    <!--    <li><a href="#" onclick="follower()"><h3>FOLLOWERS</h3> <span><?= $followers_count ?></span></li> -->
                      <!--  <li style="margin:0px;"><a href="#" onclick="following()"><h3>FOLLOWING</h3> <span><?= $follwing_count ?></span></a></li>-->
                    </ul>
                </div>
            </div>
        </div>
    </div>
</div>
                    </div>

                </div>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-default" data-dismiss="modal">Cancel</button>
            </div>
        </div>
    </div>
</div>
