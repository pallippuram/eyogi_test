
    <table class="table userlist" id="purchase_data" class="display" cellspacing="0" width="100%">
                                    <thead>
                                        <tr>
                                            <th width="">ID</th>
                                            <th width="">Amount</th>
                                            <th width="">Purchase Date</th>
                                            <th width="">Status</th>
                                            <th width="" class="text-center">Action</th>
                                            
                                        </tr>
                                    </thead>
                                    <tbody>
                                        <?php
                                        $i = 1;
                                        foreach ($payment as $user) {
                                            ?>
                                            <tr id="save-job-1212" class="save-job-item">
                                                <td>
                                                    <?= $i ?>
                                                </td>
                                                <td>
                                                    <?= $user->coupon_amount ?>
                                                </td>
                                                <td>
                                                    <?php $timestamp =  $user->purchase_date;
                                                          echo date('Y-m-d',strtotime($timestamp));
                                                          echo " / ";
                                                          echo date('h:i A',strtotime($timestamp));
                                                    ?>
                                                </td>
                                                <td> 
                                                    <?= $user->payment_status ?>
                                                </td>
                                                <td class="text-center">
                                                    <a style="padding-right: 20px" onclick="purchase_details(<?= $user->id ?>)"><i class="fa fa-eye" title="View Details" aria-hidden="true"></i></a>
                                                    <a onclick="delpurchase(<?=$user->id?>)" href=""><i class="fa fa-trash-o" title="Delete" aria-hidden="true"></i></a>
                                                </td>   
                                               
                                            </tr>
                                            <?php $i++;
                                        }
                                        ?>


                                    </tbody>
                                </table>

