<?php $this->load->view('layout/user_header.php') ?>
<div class="contents-main" style="background-color: #f2f2f1;margin-top:3%"  id="contents-main" >

    <article id="post-141" class="post-141 page type-page status-publish hentry">
        <div class="entry-content">
            <div class="iwj-dashboard clearfix">

                <div class="iwj-dashboard-menu-mobile">
                    <div class="dropdown">
                        <button class="btn btn-primary dropdown-toggle"  type="button" data-toggle="dropdown">Menu Dashboard <span class="caret"></span></button>


                        <?php $this->load->view('layout/menu.php') ?>
                        
                    </div>
                </div>
                

                <div class="iwj-dashboard-main1 overview">
                    <div class="iwj-dashboard-main-inner1">
                        <div class="iwj-overview iwj-profile clearfix">
                            <div style="background-color: #fff;padding: 30px 0px" class="iwj-edit-candidate-profile iwj-edit-profile-page">
                                <div  id='calendar'></div>
                            </div>
                        </div>

                    </div>
                </div>
                
                <div id="" class="modal fade show_event_popup">
                    <div class="modal-dialog modal-lg">
                        <div class="modal-content">
                            <div class="modal-header">
                                <button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">&times;</span><span class="sr-only">Close</span></button>
                                <h4>Schedule Details</h4>
                            </div>
                            <div class="modal-body">
                                  <div class="iw-job-content iw-job-detail" style="background: #ffffff;" id="contents-main">
                            <div class="container">

                        <div class="col-sm-12 col-xs-12 col-lg-8 col-md-8">
                            <div class="job-detail">
                                <div class="job-detail-content">
                                    <div class="job-detail-info">
                                        <ul>
                                            <li class="job-type" style="width: 50%">
                                                <div class="content-inner">
                                                    <div class="left" style="min-width: 40px">
                                                        
                                                        <span class="title">User :</span>
                                                    </div>
                                                    <div id="_user_name" class="content"></div>
                                                </div>
                                            </li>
                                            <li class="job-type" style="width: 50%">
                                                <div class="content-inner">
                                                    <div class="left">
                                                        
                                                        <span class="title">Counselor :</span>
                                                    </div>
                                                    <div id="_date" class="content">$1,000 - $2,000</div>
                                                </div>
                                            </li>
                                            
                                             <li class="job-type" style="width: 50%">
                                                <div class="content-inner">
                                                    <div class="left">
                                                        
                                                        <span class="title">Date :</span>
                                                    </div>
                                                    <div id="_status" class="content">
                                                        4 months ago                                        
                                                    </div>
                                                </div>
                                            </li>
                                            <li class="job-type" style="width: 50%">
                                                <div class="content-inner">
                                                    <div class="left">
                                                        <span class="title">Time Slot :</span>
                                                    </div>
                                                    <div id="_time_slot" class="content">
                                                        Full-Time                                        
                                                    </div>
                                                </div>
                                            </li>
                                           
                                            
                                        </ul>
                                    </div>
                                </div>

                            </div>
                        </div>

                    </div>

                </div>
                            </div>
                            <div class="modal-footer">
                                <button type="button" class="btn btn-default" data-dismiss="modal">Cancel</button>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="iwj-dashboard-sidebar">
                    <div class="user-profile candidate clearfix">
                        <img alt='Peter Pham' src='<?php if($data1[0]->photo!="") {  echo $data1[0]->photo; }else { echo base_url()."uploads/images/user.jpg";  } ?>' srcset='' class='avatar avatar-96 photo' height='96' width='96' />           
                        <h4>
                            <span>Howdy!</span>
                            <?php echo $data1[0]->username; ?>          
                        </h4>
                    </div>
                    <div class="iwj-dashboard-menu">
                <?php $this->load->view('layout/sidebar.php') ?>
				 </div>
                </div>
            </div>
        </div><!-- .entry-content -->
        <div class="clearfix"></div>
        <footer class="entry-footer ">
        </footer><!-- .entry-footer -->
    </article><!-- #post-## -->
</div>
<?php $this->load->view('layout/footer.php') ?>