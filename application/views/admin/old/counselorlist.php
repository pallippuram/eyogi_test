<?php $this->load->view('layout/user_header.php') ?>
<div class="contents-main" id="contents-main" style="margin-top:3%">

    <article id="post-141" class="post-141 page type-page status-publish hentry">
        <div class="entry-content">
            <div class="iwj-dashboard clearfix">

                <div class="iwj-dashboard-menu-mobile">
                    <div class="dropdown">
                        <button class="btn btn-primary dropdown-toggle"  type="button" data-toggle="dropdown">Menu Dashboard <span class="caret"></span></button>


                        <ul class="dropdown-menu" role="menu" aria-labelledby="dashboard-menu">
                       
                            <li>
                                <a onclick="location.href = '<?php echo base_url(); ?>admin/add_counselor';" ><i class="fa fa-user"></i>Add Counselor</a>
                            </li>
                            <li>
                                <a onclick="location.href = '<?php echo base_url(); ?>admin/admin_list';" ><i class="fa fa-user"></i>Admin List</a>
                            </li>
                            <li>
                                <a onclick="location.href = '<?php echo base_url(); ?>admin/userlist';" ><i class="fa fa-user"></i>User List</a>
                            </li>

                            <li>
                                <a onclick="location.href = '<?php echo base_url(); ?>admin/counselorlist';" ><i class="fa fa-user"></i>Counselor List</a>
                            </li>
                            <li>
                                <a onclick="location.href = '<?php echo base_url(); ?>admin/couponlist';" ><i class="fa fa-user"></i>Coupon List</a>
                            </li>
                          
                            <li>
                                <a onclick="location.href = '<?php echo base_url(); ?>admin/counselorSchedule';" ><i class="fa fa-user"></i>Counselor Schedule</a>
                            </li>
                            <li>
                                <a href="userhome.html"><i class="fa fa-user"></i>Schedule</a>
                            </li>


                            <li>
                                <a href="myschedules.html"><i class="fa fa-newspaper-o"></i>My Schedules</a>
                            </li>
                           
                            
                            <li>
                                <a onclick="location.href = '<?php echo base_url(); ?>admin/myprofile';"><i class="fa fa-envelope-o"></i>My Profile</a>
                            </li>


                            <li>
                                <a onclick="location.href = '<?php echo base_url(); ?>home/logout';"><i class="fa fa-sign-out"></i>Logout</a>
                            </li>
                        </ul>        
                        
                    </div>
                </div>

                <div class="iwj-dashboard-main save-jobs">
                    <div class="iwj-dashboard-main-inner">
                        <div class="iwj-save-jobs iwj-main-block">

                            <div class="iwj-table-overflow-x">
                                <table class="table userlist" cellspacing="0" width="100%">
                                    <thead>
                                        <tr>
                                            <th width="">ID</th>
                                            <th width="">Counselor</th>
                                            <th width="">Email</th>
                                            <th width="">Phone</th>
                                            <th width="">Status</th>
                                            <th width="" class="text-center">Action</th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                        <?php
                                        $i = 1;
                                        foreach ($userDetails as $user) {
                                            ?>
                                            <tr id="save-job-1212" class="save-job-item">
                                                <td>
                                                    <?= $i ?>
                                                </td>
                                                <td>
                                                    <?= $user->username ?>
                                                </td>
                                                <td>
                                                    <?= $user->email ?>
                                                </td>
                                                <td> 
                                                    <?= $user->phone ?>
                                                </td>
                                                <td> 
                                                    <?= $user->status ?>
                                                </td>
                                                <td class="text-center">
                                                    <a style="padding-right: 20px" href="<?php echo base_url(); ?>admin/updateCounselor/<?=$user->id?>"><i class="fa fa-pencil-square-o" title="Edit" aria-hidden="true"></i></a>
                                                    <a style="padding-right: 20px" onclick="approveUser(<?=$user->id?>)"><i class="fa fa-thumbs-up" title="Approve" aria-hidden="true"></i></a>
                                                    <a style="padding-right: 20px" href="<?php echo base_url(); ?>admin/schedule/<?=$user->id?>"><i class="fa fa-clock-o" title="Schedule Appointment" aria-hidden="true"></i></a>
                                               
                                                    <a onclick="delUser(<?=$user->id?>)" href=""><i class="fa fa-trash-o" title="Delete" aria-hidden="true"></i></a>
                                                     </td>
                                            </tr>
                                            <?php $i++;
                                        } ?>


                                    </tbody>
                                </table>
                            </div>
                            <div class="modal fade" id="iwj-confirm-undo-save-job" role="dialog">

                            </div>

                        </div>
                        <div class="clearfix"></div>

                    </div>
                </div>


                <!-- iwj-sidebar-sticky-->
                <div class="iwj-dashboard-sidebar">
                    <div class="user-profile candidate clearfix">
                        <img alt='Peter Pham' src='<?php echo base_url() ?>uploads/images/<?php echo $data1[0]->photo; ?>' srcset='' class='avatar avatar-96 photo' height='96' width='96' />           
                        <h4>
                            <span>Howdy!</span>
                            <?php echo $data1[0]->username; ?>          
                        </h4>
                    </div>
                    <div class="iwj-dashboard-menu">
                      <ul >
                            <li>
                                <a onclick="location.href = '<?php echo base_url(); ?>admin/add_counselor';" ><i class="fa fa-user"></i>Add Counselor</a>
                            </li>
                            <li>
                                <a onclick="location.href = '<?php echo base_url(); ?>admin/admin_list';" ><i class="fa fa-user"></i>Admin List</a>
                            </li>
                            <li>
                                <a onclick="location.href = '<?php echo base_url(); ?>admin/userlist';" ><i class="fa fa-user"></i>User List</a>
                            </li>

                            <li>
                                <a onclick="location.href = '<?php echo base_url(); ?>admin/counselorlist';" ><i class="fa fa-user"></i>Counselor List</a>
                            </li>
                            <li>
                                <a onclick="location.href = '<?php echo base_url(); ?>admin/couponlist';" ><i class="fa fa-user"></i>Coupon List</a>
                            </li>
                          
                            <li>
                                <a onclick="location.href = '<?php echo base_url(); ?>admin/counselorSchedule';" ><i class="fa fa-user"></i>Counselor Schedule</a>
                            </li>
                            <li>
                                <a href="userhome.html"><i class="fa fa-user"></i>Schedule</a>
                            </li>


                            <li>
                                <a href="myschedules.html"><i class="fa fa-newspaper-o"></i>My Schedules</a>
                            </li>
                           
                            
                            <li>
                                <a onclick="location.href = '<?php echo base_url(); ?>admin/myprofile';"><i class="fa fa-envelope-o"></i>My Profile</a>
                            </li>


                            <li>
                                <a onclick="location.href = '<?php echo base_url(); ?>home/logout';"><i class="fa fa-sign-out"></i>Logout</a>
                            </li>
                        </ul>    

                    </div>
                </div>
            </div>
        </div><!-- .entry-content -->
        <div class="clearfix"></div>
        <footer class="entry-footer ">
        </footer><!-- .entry-footer -->
    </article><!-- #post-## -->
</div>

<?php $this->load->view('layout/footer.php') ?>