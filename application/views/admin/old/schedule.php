<?php $this->load->view('layout/user_header.php') ?>
<div class="contents-main" style="background-color: #f2f2f1;margin-top:3%"  id="contents-main" >

    <article id="post-141" class="post-141 page type-page status-publish hentry">
        <div class="entry-content">
            <div class="iwj-dashboard clearfix">

                <div class="iwj-dashboard-menu-mobile">
                    <div class="dropdown">
                        <button class="btn btn-primary dropdown-toggle"  type="button" data-toggle="dropdown">Menu Dashboard <span class="caret"></span></button>


                        <?php $this->load->view('layout/menu.php') ?>
                    </div>
                </div>

                <div class="iwj-dashboard-main1 overview">
                    <div class="iwj-dashboard-main-inner1">
                        <div class="iwj-overview iwj-profile clearfix">
                            <div style="background-color: #fff;padding: 30px 0px" class="iwj-edit-candidate-profile iwj-edit-profile-page">
                                <div  id='calendar'></div>
                            </div>
                        </div>

                    </div>
                </div>
                <div class="iwj-dashboard-sidebar">
                    <div class="user-profile candidate clearfix">
                        <img alt='Peter Pham' src='<?php echo base_url() ?>uploads/images/<?php echo $data1[0]->photo; ?>' srcset='' class='avatar avatar-96 photo' height='96' width='96' />           
                        <h4>
                            <span>Howdy!</span>
                            <?php echo $data1[0]->username; ?>          
                        </h4>
                    </div>
                    <div class="iwj-dashboard-menu">
                <?php $this->load->view('layout/sidebar.php') ?>
				 </div>
                </div>
            </div>
        </div><!-- .entry-content -->
        <div class="clearfix"></div>
        <footer class="entry-footer ">
        </footer><!-- .entry-footer -->
    </article><!-- #post-## -->
</div>
<?php $this->load->view('layout/footer.php') ?>