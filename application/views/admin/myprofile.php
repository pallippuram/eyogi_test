<?php $this->load->view('layout/user_header.php') ?>
<div class="contents-main" id="contents-main" style="margin-top:3%">

    <article id="post-141" class="post-141 page type-page status-publish hentry">
        <div class="entry-content">
            <div class="iwj-dashboard clearfix">

                <div class="iwj-dashboard-menu-mobile">
                    <div class="dropdown">
                        <button class="btn btn-primary dropdown-toggle"  type="button" data-toggle="dropdown">Menu Dashboard <span class="caret"></span></button>

                        <?php $this->load->view('layout/menu.php') ?>       
                    </div>
                </div>
                <div class="iwj-dashboard-main overview">
                    <div class="iwj-dashboard-main-inner">
                        <div class="iwj-overview iwj-profile clearfix">
                            <div class="info-top-wrap" style="width:100%">
                                <div class="sidebar-info">
                                    <div class="avatar">
                                        <img alt="Peter Pham" src="<?php if($data1[0]->photo!="") {  echo $data1[0]->photo; }else { echo base_url()."uploads/images/user.jpg";  } ?>" srcset="http://jobboard.inwavethemes.com/wp-content/uploads/2017/06/31m-2.jpg 2x" class="avatar avatar-150 photo" height="150" width="150">
                                    </div>
                                    <?php if($data1[0]->role_id==3){?>
                                    <a style="color:#777 !important; font-size:11px;" class="iwj-edit-profile" href="<?php echo base_url(); ?>Counselor/edit_profile"><i class="fa fa-edit"></i>Edit My Profile</a>
                                    <?php }else{ ?>
                                    <a style="color:#777 !important; font-size:11px;" class="iwj-edit-profile" href="<?php echo base_url(); ?>admin/editprofile"><i class="fa fa-edit"></i>Edit My Profile</a>
                                    
                                    <?php } ?>
                                </div>
                                <div class="main-info candidate-info iw-job-detail-sidebar">
                                    <div class="info-top" style="padding-bottom:0">
                                        <h3 class=""><a style="color:#777" ><?php echo $data[0]->username; ?></a></h3>

                                    </div>
                                    <div class="iwj-sidebar-bottom info-bottom" style="border-bottom: 1px #f6f7f9 solid;">
                                        <ul>
                                            <li class="location">
                                                <div class="left">
                                                    <i class="fa fa-user"></i>
                                                    <span class="title-meta ">Gender:</span>
                                                </div>
                                                <div class="content"><?php
                                                    if ($data[0]->gender == 0) {
                                                        echo 'Male';
                                                    } else if ($data[0]->gender == 1) {
                                                        echo 'Female';
                                                    }
                                                    ?></div>
                                            </li>
                                            <li class="phone">
                                                <div class="left">
                                                    <i class="ion-android-phone-portrait"></i>
                                                    <span class="title-meta ">Phone:</span>
                                                </div>
                                                <div class="content"><?php echo $data[0]->phone; ?></div>
                                            </li>

                                            <li class="email">
                                                <div class="left ">
                                                    <i class="iwj-icon-email"></i>
                                                    <span class="title-meta">Email:</span>
                                                </div>
                                                <div class="content"><a href="mailto:<?php echo $data[0]->email;?>"><?php echo $data[0]->email; ?></a></div>
                                            </li>
                                        </ul>
                                    </div>
                                    <div class="iwj-sidebar-bottom info-bottom">
                                        <?php
                                           // $data_user = $this->session->userdata('userData');
                                           // if($data_user->role_id==4){ ?>
                                                 
                                         
                                      <!--  <ul>
                                            <li class="location">
                                                <div class="left">

                                                    <span class="title-meta">Age</span>
                                                </div>
                                                <div class="content"><?php //echo $data[0]->age; ?></div>
                                            </li>
                                            <li class="location">
                                                <div class="left">

                                                    <span class="title-meta ">Educational Qualification</span>
                                                </div>
                                                <div class="content"><?php // $data[0]->qualification; ?></div>
                                            </li>
                                            <li class="location">
                                                <div class="left">

                                                    <span class="title-meta ">Marital Status</span>
                                                </div>
                                                <div class="content"><?php
                                                //    if ($data[0]->marital_status == 1) {
                                                //        echo 'Married';
                                                //    } else {
                                                //        echo 'Single';
                                                //    }
                                                    ?></div>
                                            </li>
                                            <li class="location">
                                                <div class="left">

                                                    <span class="title-meta ">Kids</span>
                                                </div>
                                                <div class="content"><?php
                                                //    if ($data[0]->kids == 1) {
                                                //        echo 'Yes';
                                                //    } else if ($data[0]->kids == 0) {
                                                //        echo 'No';
                                                //    }
                                                    ?></div>
                                            </li>


                                        </ul> -->
                                        <?php //  }
                                            ?>
                                    </div>
                                </div>
                            </div>


                        </div>
                    </div>
                </div>


                <!-- iwj-sidebar-sticky-->
                <div class="iwj-dashboard-sidebar">
                    <div class="user-profile candidate clearfix">
                        <img alt='Peter Pham' src='<?php if($data1[0]->photo!="") {  echo $data1[0]->photo; }else { echo base_url()."uploads/images/user.jpg";  } ?>' srcset='' class='avatar avatar-96 photo' height='96' width='96' />            
                        <h4>
                            <span>Howdy!</span>
                            <?php echo $data1[0]->username; ?>          
                        </h4>
                    </div>
                    <div class="iwj-dashboard-menu">
                        <?php $this->load->view('layout/sidebar.php') ?>   

                    </div>
                </div>
            </div>
        </div><!-- .entry-content -->
        <div class="clearfix"></div>
        <footer class="entry-footer ">
        </footer><!-- .entry-footer -->
    </article><!-- #post-## -->
</div>

<script>

</script>

<?php $this->load->view('layout/footer.php') ?>
