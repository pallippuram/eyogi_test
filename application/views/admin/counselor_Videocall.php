<?php $this->load->view('layout/user_header.php') ?>

<div class="contents-main" id="contents-main" style="margin-top:3%">

    <article id="post-141" class="post-141 page type-page status-publish hentry">
        <div class="entry-content">
            <div class="iwj-dashboard clearfix">

                <div class="iwj-dashboard-menu-mobile">
                    <div class="dropdown">
                        <button class="btn btn-primary dropdown-toggle"  type="button" data-toggle="dropdown">Menu Dashboard <span class="caret"></span></button>


                        <?php $this->load->view('layout/menu.php') ?>
                    </div>
                </div>

                <div class="iwj-dashboard-main save-jobs">
                    <div class="iwj-dashboard-main-inner">
                        <div class="iwj-save-jobs iwj-main-block">
                            <form method="post"  class="iwj-form-2 iwj-login-form1">

                                <div class="info-top" style="padding-bottom:0">
                                    <h3 class=""><?php echo "Video-Call Enabling" ?></h3>

                                </div>

                            </form>
                            	<table id="example" class="stripe row-border order-column" style="width:100%">
        							<thead>
            							<tr>
                							<th><b>Counselors</b></th>
                							<th><b>Video-call</b></th>
            							</tr>
        							</thead>
        							<tbody>
        								<?php
                                        $i = 1;
                                        foreach ($counselors as $user) {
                                            ?>
            							<tr>
                							<td>
                								<?= $user->username ?>
                							</td>
                							<td>
                								<!-- <?= $user->status ?> -->
                								<input style="padding-right: 20px; margin-right: 40px; width:20px; height:20px;" type="checkbox" title="Checkbox" id="video_check" name="b" value="b" onclick="videochck(this,<?=$user->id ?>)" <?php if ($user->video_call == '1') { ?>checked <?php } ?> >
                							</td>
                							
            							</tr>
            							 <?php $i++;
                                        } ?>
            						</tbody>
            					</table>		
                                
                            </form> 
                           

                            
                            <div class="modal fade" id="iwj-confirm-undo-save-job" role="dialog">

                            </div>

                        </div>
                        <div class="clearfix"></div>

                    </div>

                </div>


                <!-- iwj-sidebar-sticky-->
                <div class="iwj-dashboard-sidebar">
                    <div class="user-profile candidate clearfix">
                        <img alt='Peter Pham' src='<?php if($data1[0]->photo!="") { echo $data1[0]->photo; }else { echo base_url()."uploads/images/user.jpg";  } ?>' srcset='' class='avatar avatar-96 photo' height='96' width='96' />           
                        <h4>
                            <span>Howdy!</span>
                            <?php echo $data1[0]->username; ?>          
                        </h4>
                    </div>
                    <div class="iwj-dashboard-menu">
                         <?php $this->load->view('layout/sidebar.php') ?>
                    </div>
                </div>
            </div>
        </div><!-- .entry-content -->
        <div class="clearfix"></div>
        <footer class="entry-footer ">
        </footer><!-- .entry-footer -->
    </article><!-- #post-## -->
</div>

<script type="text/javascript" src="https://code.jquery.com/jquery-3.3.1.js"></script>
<script type="text/javascript" src="https://cdn.datatables.net/1.10.19/js/jquery.dataTables.min.js"></script>
<script type="text/javascript" src="https://cdn.datatables.net/fixedcolumns/3.2.6/js/dataTables.fixedColumns.min.js"></script>

<script type="text/javascript">


	
$(document).ready(function() {
    var table = $('#example').DataTable( {
        scrollY:        "300px",
        //scrollX:        true,
        scrollCollapse: true,
        paging:         false,
        /*fixedColumns:   {
            leftColumns: 2
        }*/
    } );
} );


function videochck(value, user_id)
{
	$(value).val(value.checked ? '1' : '0');
    var txt = $(value).val();

    $.ajax({
        url: '<?php echo base_url(); ?>admin/videoCall_check',
        data: {id: user_id, text: txt},
        type: 'post',
        success: function (data) {
            if (data == 1) {
                //alert('success');
            }
        }   
    });

	//alert(txt);
	//alert(user_id);

}

</script>

<?php $this->load->view('layout/footer.php') ?>