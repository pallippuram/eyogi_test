<?php $this->load->view('layout/user_header.php') ?>
<div class="contents-main" id="contents-main" style="margin-top:3%">

    <article id="post-141" class="post-141 page type-page status-publish hentry">
        <div class="entry-content">
            <div class="iwj-dashboard clearfix">

                <div class="iwj-dashboard-menu-mobile">
                    <div class="dropdown">
                        <button class="btn btn-primary dropdown-toggle"  type="button" data-toggle="dropdown">Menu Dashboard <span class="caret"></span></button>


                        <?php $this->load->view('layout/menu.php') ?>
                    </div>
                </div>

                <div class="iwj-dashboard-main save-jobs">
                    <div class="iwj-dashboard-main-inner">
                        <div class="iwj-save-jobs iwj-main-block">
                            <form method="post"  class="iwj-form-2 iwj-login-form1">

                                <div class="info-top" style="padding-bottom:0">
                                    <h3 class=""><?php echo "Coupon List" ?></h3>

                                </div>

                            </form>
                            
                            <form>
                                <input type="button" value="ADD COUPON" onclick="window.location.href='<?php echo base_url(); ?>admin/addcoupon'" style="background-color: rgb(48, 194, 189);margin-left: 15px"/>
                            </form> 
                           

                            <div class="iwj-table-overflow-x">
                                <table class="table userlist" id="" class="display" cellspacing="0" width="100%">
                                    <thead>
                                        <tr>
                                            <th width="">ID</th>
                                            <th width="">Coupon Name</th>
                                            <th width="">Coupon Amount</th>
                                            <th width="">Coupon Code</th>
                                            <th width="">Status</th>
                                            <th width="" class="text-center">Action</th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                        <?php
                                        $i = 1;
                                        foreach ($userDetails as $user) {
                                            ?>
                                            <tr id="save-job-1212" class="save-job-item">
                                                <td>
                                                    <?= $i ?>
                                                </td>
                                                <td>
                                                    <?= $user->coupon_name ?>
                                                </td>
                                                <td>
                                                    <?= $user->amount ?>
                                                </td>
                                                <td>
                                                    <?= $user->code ?>
                                                </td>
                                                <td class="v3">
                                                   <a href="#"><?= $user->status ?></a>
                                               </td> 
                                                <td class="text-center">
                                                    <input style="padding-right: 20px; margin-right: 40px" type="checkbox" title="Checkbox" id="checktext" name="a" value="a" onclick="chkbox(this,<?=$user->id ?>)" <?php if ($user->status == 'ACTIVE') { ?>checked <?php } ?> >
                                                    <a onclick="delcoupon(<?=$user->id?>)" href=""><i class="fa fa-trash-o" title="Delete" aria-hidden="true"></i></a>
                                                </td>
                                            </tr>
                                            <?php $i++;
                                        } ?>


                                    </tbody>
                                </table>
                            </div>
                            <div class="modal fade" id="iwj-confirm-undo-save-job" role="dialog">

                            </div>

                        </div>
                        <div class="clearfix"></div>

                    </div>

                </div>


                <!-- iwj-sidebar-sticky-->
                <div class="iwj-dashboard-sidebar">
                    <div class="user-profile candidate clearfix">
                        <img alt='Peter Pham' src='<?php if($data1[0]->photo!="") { echo $data1[0]->photo; }else { echo base_url()."uploads/images/user.jpg";  } ?>' srcset='' class='avatar avatar-96 photo' height='96' width='96' />           
                        <h4>
                            <span>Howdy!</span>
                            <?php echo $data1[0]->username; ?>          
                        </h4>
                    </div>
                    <div class="iwj-dashboard-menu">
                         <?php $this->load->view('layout/sidebar.php') ?>
                    </div>
                </div>
            </div>
        </div><!-- .entry-content -->
        <div class="clearfix"></div>
        <footer class="entry-footer ">
        </footer><!-- .entry-footer -->
    </article><!-- #post-## -->
</div>

<?php $this->load->view('layout/footer.php') ?>