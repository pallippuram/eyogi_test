<table class="table userlist" id="counsdata" class="display" cellspacing="0" width="100%">
                                    <thead>
                                        <tr>
                                            <th width="">ID</th>
                                            <th width="">Counselor</th>
                                            <th width="">Duration</th>
                                            <th width="">Schedules</th>
                                            <th width="">Remuneration</th>
                                            <th width="">Status</th>
                                            <th width="" class="text-center">Action</th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                        <?php
                                        $i = 1;
                                        foreach ($couns_pay as $user) {
                                            ?>
                                            <tr id="save-job-1212" class="save-job-item">
                                                <td>
                                                    <?= $i ?>
                                                </td>
                                                <td>
                                                    <?= $user->username ?>
                                                </td>
                                                <td>
                                                    <?php
                                                        $init = $user->dur;
                                                        $hours = floor($init / 3600);
                                                        $minutes = floor(($init / 60) % 60);
                                                        $seconds = $init % 60;
                                                        
                                                        echo "$hours:$minutes:$seconds";
                                                        ?>
                                                </td>
                                                <td> 
                                                    <?= $user->count ?>
                                                </td>
                                                <td> 
                                                    <?= $user->renum ?>
                                                </td>
                                                <td> 
                                                    <?= $user->status ?>
                                                </td>
                                                <td class="text-center">
                                                    
                                                    
                                                <a style="padding-right: 20px" onclick="viewUser(<?=$user->counselor_id?>)"><i class="fa fa-eye" title="View Details" aria-hidden="true"></i></a>
                                                   
                                                   
                                                </td>
                                            </tr>
                                            <?php $i++;
                                        } ?>


                                    </tbody>
                                </table>