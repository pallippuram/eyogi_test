<?php $this->load->view('layout/user_header.php') ?>
<div class="contents-main" id="contents-main" style="margin-top:3%">

    <article id="post-141" class="post-141 page type-page status-publish hentry">
        <div class="entry-content">
            <div class="iwj-dashboard clearfix">

                <div class="iwj-dashboard-menu-mobile">
                    <div class="dropdown">
                        <button class="btn btn-primary dropdown-toggle"  type="button" data-toggle="dropdown">Menu Dashboard <span class="caret"></span></button>

                        <?php $this->load->view('layout/menu.php') ?>             
                    </div>
                </div>
                <div class="iwj-dashboard-main profile">
                    <div class="iwj-dashboard-main-inner">
                        <div class="iwj-profile clearfix">
                            <div class="iwj-edit-candidate-profile iwj-edit-profile-page">


                                <form method="post" enctype="multipart/form-data" action="<?php echo site_url('admin/update_counselor'); ?>"  class="iwj-form-2 iwj-login-form1">


                                    <div class="iwj-block">
                                        <div class="basic-area iwj-block-inner">
 <input type="hidden" name="id_hidden" value="<?php echo @$data[0]->id; ?>">
                                            <div class="iwjmb-field iwjmb-avatar-wrapper"><div class="iwjmb-input ui-sortable">            
                                                    <div id="_iwj_avatar" class="iwj-avatar-container">
                                                        <!-- Current avatar -->
                                                        <div class="avatar-view">
                                                            <!--<img src="<?php echo $data[0]->photo; ?>" alt="Avatar">-->
                                                            <img alt="Avatar" src="<?php if($data[0]->photo!="") {  echo $data[0]->photo; }else { echo base_url()."uploads/images/user.jpg";  } ?>" srcset="http://jobboard.inwavethemes.com/wp-content/uploads/2017/06/31m-2.jpg 2x" class="avatar avatar-150 photo" height="150" width="150">
                                                        </div>
                                                        <div class="desc-change-image">
                                                            <p class="avatar-description"></p>
                                                            <div class="change-image-btn" style="padding-top:50px">

                                                                <div class="choose_file btn btn-primary">
                                                                    <span>Choose File</span>
                                                                    <input name="image" type="file" />
                                                                </div>
                                                            </div>
                                                        </div>

                                                    </div>
                                                </div>
                                            </div>
                                            <div class="row">
                                                <div class="col-md-6">
                                                    <div class="iwjmb-field iwjmb-text-wrapper  required">
                                                        <div class="iwjmb-label">
                                                            <label class="theme-color" for="your_name">Your Name *</label>
                                                        </div>
                                                        <div class="iwjmb-input ui-sortable">
                                                            <input size="30" placeholder="Enter your name" value="<?php echo $data[0]->username; ?>" type="text" required="required" id="your_name" class="iwjmb-text " name="username">
                                                        </div>
                                                    </div>
                                                    <div class="iwjmb-field iwjmb-email-wrapper  required">
                                                        <div class="iwjmb-label">
                                                            <label class="theme-color" for="email">Email *</label>
                                                        </div>
                                                        <div class="iwjmb-input ui-sortable">
                                                            <input value="<?php echo $data[0]->email; ?>"  placeholder="Enter your email" type="email" required="required" id="email" class="iwjmb-email " name="email">
                                                        </div>
                                                    </div>    
                                                </div>
                                                <div class="col-md-6">
                                                    <div class="iwjmb-field iwjmb-text-wrapper  required">
                                                        <div class="iwjmb-label"><label class="theme-color" for="_iwj_phone">Phone *</label>
                                                        </div>
                                                        <div class="iwjmb-input ui-sortable">
                                                            <input size="30" placeholder="Enter your phone" value="<?php echo $data[0]->phone; ?>" type="text" required="required" id="_iwj_phone" class="iwjmb-text " name="phone">
                                                        </div>
                                                    </div>
                                                    <div class="iwjmb-field iwjmb-text-wrapper  required">
                                                        <div class="iwjmb-label"><label class="theme-color" for="_iwj_address">Gender *</label></div>
                                                        <div class="iwjmb-input ui-sortable">
                                                            <select data-options="{&quot;allowClear&quot;:false,&quot;width&quot;:&quot;none&quot;,&quot;placeholder&quot;:&quot;&quot;,&quot;minimumResultsForSearch&quot;:-1}" required id="frequency" class="iwjmb-select_advanced" name="gender">
                                                                <option value="0" <?php if($data[0]->gender==0){ echo 'selected'; } ?> >Male</option>
                                                                <option value="1" <?php if($data[0]->gender==1){ echo 'selected'; } ?>>Female</option>
                                                               <!-- <option value="2" <?php //if($data[0]->gender==1){ echo 'selected'; } ?>>Female</option>-->
                                                            </select>
                                                        </div>
                                                    </div> 
                                                </div>
                                            </div>

                                            <div class="row">
                                                <div class="col-md-6">
                                                    <div class="iwjmb-field iwjmb-text-wrapper  required"><div class="iwjmb-label"><label class="theme-color" for="_iwj_headline">Educational Qualification *</label></div>
                                                        <div class="iwjmb-input ui-sortable">

                                                            <select data-options="{&quot;allowClear&quot;:false,&quot;width&quot;:&quot;none&quot;,&quot;placeholder&quot;:&quot;&quot;,&quot;minimumResultsForSearch&quot;:-1}" required id="frequency" class="iwjmb-select_advanced" name="qualification">
                                                                <option value="Post Graduate"  <?php if($data[0]->qualification=="Post Graduate"){ echo 'selected'; } ?>>Post Graduate</option>
                                                                <option value="Under Graduate" <?php if($data[0]->qualification=="Under Graduate"){ echo 'selected'; } ?>>Under Graduate</option>

                                                            </select>
                                                        </div>
                                                    </div>                   
                                                </div>
                                                <div class="col-md-6">
                                                    <div class="iwjmb-field iwjmb-text-wrapper  required">
                                                        <div class="iwjmb-label"><label class="theme-color" for="_iwj_address">Employed *</label></div>
                                                        <div class="iwjmb-input ui-sortable">
                                                            <select data-options="{&quot;allowClear&quot;:false,&quot;width&quot;:&quot;none&quot;,&quot;placeholder&quot;:&quot;&quot;,&quot;minimumResultsForSearch&quot;:-1}" required id="frequency" class="iwjmb-select_advanced" name="employed">
                                                                <option <?php if($data[0]->employed==0){ echo 'selected'; } ?> value="0"  selected='selected'>Yes</option>
                                                                <option <?php if($data[0]->employed==1){ echo 'selected'; } ?> value="1">No</option>

                                                            </select>
                                                        </div>
                                                    </div> 
                                                </div>
                                            </div>
                                            <div class="row">
                                                <div class="col-md-6">
                                                    <div class="iwjmb-field iwjmb-text-wrapper  required"><div class="iwjmb-label"><label class="theme-color" for="_iwj_headline">Marital Status *</label></div>
                                                        <div class="iwjmb-input ui-sortable">
                                                            <select data-options="{&quot;allowClear&quot;:false,&quot;width&quot;:&quot;none&quot;,&quot;placeholder&quot;:&quot;&quot;,&quot;minimumResultsForSearch&quot;:-1}" required id="frequency" class="iwjmb-select_advanced" name="marital_status">
                                                                <option <?php if($data[0]->marital_status==0){ echo 'selected'; } ?> value="0"  selected='selected'>Single</option>
                                                                <option <?php if($data[0]->marital_status==1){ echo 'selected'; } ?> value="1">Married</option>

                                                            </select>   
                                                        </div>
                                                    </div>                   
                                                </div>
                                                <div class="col-md-6">
                                                    <div class="iwjmb-field iwjmb-text-wrapper  required">
                                                        <div class="iwjmb-label"><label class="theme-color" for="_iwj_address">Kids *</label></div>
                                                        <div class="iwjmb-input ui-sortable">
                                                            <select data-options="{&quot;allowClear&quot;:false,&quot;width&quot;:&quot;none&quot;,&quot;placeholder&quot;:&quot;&quot;,&quot;minimumResultsForSearch&quot;:-1}" required id="frequency" class="iwjmb-select_advanced" name="kids">
                                                                <option <?php if($data[0]->kids==0){ echo 'selected'; } ?> value="0"  selected='selected'>No</option>
                                                                <option <?php if($data[0]->kids==1){ echo 'selected'; } ?> value="1">Yes</option>

                                                            </select>
                                                        </div>
                                                    </div> 
                                                </div>
                                            </div>


                                        </div>

                                        <input type="hidden" value="<?php echo $data[0]->role_id ?>" name="role_id">
                                        <div class="iwj-respon-msg hide"></div>
                                        <div class="iwj-button-loader">
                                            <button type="submit" class="iwj-btn edit_btn iwj-btn-primary iwj-candidate-btn">Update Profile</button>
                                        </div>
                                    </div>
                                </form>

                                <div class="iwj-change-password iwj-block">
                                    <form  action="<?php echo site_url('admin/update_password'); ?>" method="post" class="iwj-form-2 iwj-login-form1">
                                        <h3 class="theme-color">Change Password</h3>   
                                        <input type="hidden" value="<?php echo $data[0]->id ?>" name="user_id">
                                        <div class="row">
                                            <div class="col-md-6">
                                                <div class="iwjmb-field iwjmb-password-wrapper  required">
                                                    <div class="iwjmb-label">
                                                        <label class="theme-color" for="current_password">Current Password</label>
                                                    </div>
                                                    <div class="iwjmb-input ui-sortable">
                                                        <input type="password" required="required" id="current_password" class="iwjmb-password " name="current_password">
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="col-md-6">
                                                <div class="iwjmb-field iwjmb-password-wrapper  required">
                                                    <div class="iwjmb-label">
                                                        <label class="theme-color" for="new_password">New Password</label>
                                                    </div>
                                                    <div class="iwjmb-input ui-sortable">
                                                        <input type="password" required="required" id="new_password" class="iwjmb-password " name="new_password">
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="clearfix"></div>
                                        <div class="iwj-respon-msg iwj-hide"></div>
                                        <div class="iwj-button-loader">
                                            <button type="submit" class="iwj-btn edit_btn iwj-btn-primary iwj-change-password-btn">Change Password</button>
                                        </div>
                                    </form>
                                </div>
                            </div>
                        </div>        
                    </div>
                </div>


                <!-- iwj-sidebar-sticky-->
                <div class="iwj-dashboard-sidebar">
                    <div class="user-profile candidate clearfix">
                        <img alt='Peter Pham' src='<?php if($data1[0]->photo!="") {  echo $data1[0]->photo; }else { echo base_url()."uploads/images/user.jpg";  } ?>' srcset='' class='avatar avatar-96 photo' height='96' width='96' />            
                        <h4>
                            <span>Howdy!</span>
                            <?php echo $data1[0]->username; ?>          
                        </h4>
                    </div>
                    <div class="iwj-dashboard-menu">
                       <?php $this->load->view('layout/sidebar.php') ?>    

                    </div>
                </div>
            </div>
        </div><!-- .entry-content -->
        <div class="clearfix"></div>
        <footer class="entry-footer ">
        </footer><!-- .entry-footer -->
    </article><!-- #post-## -->
</div>
<?php $this->load->view('layout/footer.php') ?>