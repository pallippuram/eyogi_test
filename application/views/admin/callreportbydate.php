
                                <table class="table userlist" id="logdata" class="display" cellspacing="0" width="100%">
                                    <thead>
                                        <tr>
                                            <th width="">ID</th>
                                            <th width="">Counselor Name</th>
                                            <th width="">Appointment Date</th>
                                            <th width="">Time Slot</th>
                                            <th width="">Status</th>
                                            <th width="">Duration</th>
                                           <th width="">Ratings</th>
                                            
                                            <th width="" class="text-center">Action</th>
                                            
                                        </tr>
                                    </thead>
                                    <tbody>
                                        <?php
                                        $i = 1;
                                        foreach ($call as $user) {
                                            if (!empty($user)) {
                                                ?>
                                                <tr id="save-job-1212" class="save-job-item">
                                                    <td>
                                                        <?= $i ?>
                                                    </td>
                                                    <td>
                                                        <?= $user->cname ?>
                                                    </td>
                                                    <td>
                                                        <?= $user->date ?>
                                                    </td>
                                                    <td> 
                                                        <?=   date('h:i A', strtotime($user->time_starts) ). " - " .date('h:i A', strtotime( $user->time_ends) ) ?>
                                                    </td>

                                                    <td>
                                                        <?= $user->cstatus ?>
                                                    </td>
                                                    <td>
                                                        <?php
                                                        $init = $user->duration;
                                                        $hours = floor($init / 3600);
                                                        $minutes = floor(($init / 60) % 60);
                                                        $seconds = $init % 60;
                                                        
                                                        echo "$hours:$minutes:$seconds";
                                                        ?>
                                                    </td>
                                                    <td> 
                                                        <?php
                                                        if ($user->cstatus=='completed') {

                                                            echo $user->amount;
                                                        } else {
                                                            echo "-";
                                                        }
                                                        ?>
                                                    </td>
                                                     <td>

                                                        <?php if($user->cstatus=='completed'){

                                                            ?>

                                                    <span class="fa fa-star-o" data-rating="1" onclick="show_ratings(<?php echo $user->counselor_id;?>,<?php echo $user->ccid ?>)" style="cursor: pointer;"></span>
                                                            <?php

                                                             }else{ ?>

                                                             <p>Call not completed</p>
                                                             <?php }
                                                       ?>

                                                     

                                                 </td>    
                                                    <td class="text-center">
                                                        <a style="padding-right: 20px" onclick="callReport(<?= $user->cid ?>)"><i class="fa fa-eye" title="View Details" aria-hidden="true"></i></a>
                                                        <a onclick="delCallReport(<?=$user->cid?>)" href=""><i class="fa fa-trash-o" title="Delete" aria-hidden="true"></i></a>
                                                    </td>

                                                    
                                                </tr>
                                                    <?php $i++;
                                                    }
                                                    }
                                                    ?>


                                    </tbody>
                                </table>
