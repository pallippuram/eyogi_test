<?php $this->load->view('layout/user_header.php') ?>
<link rel="stylesheet" href="//code.jquery.com/ui/1.12.1/themes/base/jquery-ui.css">

<div class="contents-main" id="contents-main" style="margin-top:3%">

    <article id="post-141" class="post-141 page type-page status-publish hentry">
        <div class="entry-content">
            <div class="iwj-dashboard clearfix">

                <div class="iwj-dashboard-menu-mobile">
                    <div class="dropdown">
                        <button class="btn btn-primary dropdown-toggle"  type="button" data-toggle="dropdown">Menu Dashboard <span class="caret"></span></button>


                        <?php $this->load->view('layout/menu.php') ?>
                    </div>
                </div>


                <!-- date by report starts-->

                <div class="iwj-dashboard-main save-jobs" id="datebyreport">
                    <div class="iwj-dashboard-main-inner">
                        <div class="iwj-save-jobs iwj-main-block">

                            <form method="post"  class="iwj-form-2 iwj-login-form1">

                                <div class="info-top" style="padding-bottom:0">
                                    <h3 class=""><?php echo "Call History" ?></h3>

                                </div>

                            </form>

                            <div class="iwj-table-overflow-x">
                                <p id="date_filter">
                                    <span id="date-label-from" class="date-label">From: </span><input class="date_range_filter date" type="text" id="datepicker_from" />
                                    <span id="date-label-to" class="date-label">To: </span><input class="date_range_filter date" type="text" id="datepicker_to" />
                                </p>
                                <table class="table userlist" id="logdata" class="display" cellspacing="0" width="100%">
                                    <thead>
                                        <tr>
                                            <th width="">ID</th>
                                            <th width="">Counselor Name</th>
                                            <th width="">Appointment Date</th>
                                            <th width="">Time Slot</th>
                                            <th width="">Status</th>
                                            <th width="">Duration</th>
                                            <th width="">Price</th>
                                            <th width="">Rating</th>
                                            <th width="" class="text-center">Action</th>
                                            
                                        </tr>
                                    </thead>
                                    <tbody>
                                        <?php
                                        $i = 1;
                                        foreach ($call as $user) {
                                            if (!empty($user)) {
                                                if( $user->date == date('Y-m-d')){
                                                ?>
                                                <tr id="save-job-1212" class="save-job-item">
                                                    <td>
                                                        <?= $i ?>
                                                    </td>
                                                    <td>
                                                        <?= $user->cname ?>
                                                    </td>
                                                    <td>
                                                        <?= $user->date ?>
                                                    </td>
                                                    <td> 
                                                         <?php echo date('h:i A', strtotime($user->time_starts) ). " - " .date('h:i A', strtotime( $user->time_ends) ) ?>
                                                    </td>

                                                    <td>
                                                        <?= $user->cstatus ?>
                                                    </td>
                                                    <td>
                                                        <?php
                                                        $init = $user->duration;
                                                        $hours = floor($init / 3600);
                                                        $minutes = floor(($init / 60) % 60);
                                                        $seconds = $init % 60;
                                                        
                                                        echo "$hours:$minutes:$seconds";
                                                        ?>
                                                    </td>
                                                    <td> 
                                                        <?php
                                                        if ($user->cstatus=='completed') {

                                                            echo $user->amount;
                                                        } else {
                                                            echo "-";
                                                        }
                                                        ?>
                                                    </td>
                                                    <td>
                                                                                         <?php if($user->cstatus=='completed'){

                                                            ?>

                                                     <span class="fa fa-star-o" data-rating="1" onclick="show_ratings(<?php echo $user->counselor_id;?>,<?php echo $user->ccid ?>)" style="cursor: pointer;"></span>
                                                            <?php

                                                             }else{ ?>

                                                             <p>Call not completed</p>
                                                             <?php }
                                                       ?>
                                                    
                                                    </td>
                                                    <td class="text-center">
                                                        <a style="padding-right: 20px" onclick="callReport(<?= $user->cid ?>)"><i class="fa fa-eye" title="View Details" aria-hidden="true"></i></a>
                                                        <a onclick="delCallReport(<?=$user->cid?>)" href=""><i class="fa fa-trash-o" title="Delete" aria-hidden="true"></i></a>
                                                    </td>

                                                    
                                                </tr>
                                                    <?php $i++;
                                                    }
                                                    }
                                                    }
                                                    ?>


                                    </tbody>
                                </table>
                            </div>
                            <div class="modal fade" id="iwj-confirm-undo-save-job" role="dialog">

                            </div>

                        </div>
                        <div class="clearfix"></div>

                    </div>

                </div>

                <!-- date by report close-->





                <!-- iwj-sidebar-sticky-->
                <div class="iwj-dashboard-sidebar">
                    <div class="user-profile candidate clearfix">
                        <img alt='Peter Pham' src='<?php if($data1[0]->photo!="") { echo $data1[0]->photo; }else { echo base_url()."uploads/images/user.jpg";  } ?>' srcset='' class='avatar avatar-96 photo' height='96' width='96' />           
                        <h4>
                            <span>Howdy!</span>
                            <?php echo $data1[0]->username; ?>          
                        </h4>
                    </div>
                    <div class="iwj-dashboard-menu">
                        <?php $this->load->view('layout/sidebar.php') ?>
                    </div>
                </div>
            </div>
        </div><!-- .entry-content -->
        <div class="clearfix"></div>
        <footer class="entry-footer ">
        </footer><!-- .entry-footer -->
    </article><!-- #post-## -->
</div>

<div class="modal fade" id="callReport">
    <div class="modal-dialog modal-lg">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">&times;</span><span class="sr-only">Close</span></button>
                <h4>Call Report</h4>
            </div>
            <div class="modal-body">

                <div class="iw-job-content iw-job-detail" style="background: #ffffff;" id="contents-main">
                    <div class="container">

                        <div class="col-sm-12 col-xs-12 col-lg-8 col-md-8">
                            <div class="job-detail">
                                <div class="job-detail-content">
                                    <div class="job-detail-info">
                                        <ul>
                                            <li class="job-type">
                                                <div class="content-inner">
                                                    <div class="left">
                                                        
                                                        <span class="title">Counselor :</span>
                                                    </div>
                                                    <div id="call_logs_user_name" class="content"></div>
                                                </div>
                                            </li>
                                              
                                            <li class="job-type">
                                                <div class="content-inner">
                                                    <div class="left">
                                                        
                                                        <span class="title">Appointment Date :</span>
                                                    </div>
                                                    <div id="call_logs_date" class="content"></div>
                                                </div>
                                            </li>
                                                                                      
                                            <li class="job-type">
                                                <div class="content-inner">
                                                    <div class="left">
                                                        
                                                        <span class="title">Time Slot :</span>
                                                    </div>
                                                    <div id="call_logs_time" class="content">
                                                                                               
                                                    </div>
                                                </div>
                                            </li>
                                            
                                            <li class="job-type">
                                                <div class="content-inner">
                                                    <div class="left">
                                                        
                                                        <span class="title">Status :</span>
                                                    </div>
                                                    <div id="call_logs_status" class="content"></div>
                                                </div>
                                            </li>
                                            
                                            <li class="job-type">
                                                <div class="content-inner">
                                                    <div class="left">
                                                        
                                                        <span class="title">Duration :</span>
                                                    </div>
                                                    <div id="call_logs_duratn" class="content"></div>
                                                </div>
                                            </li>
                                            
                                            <li class="job-type">
                                                <div class="content-inner">
                                                    <div class="left">
                                                        
                                                        <span class="title">Price :</span>
                                                    </div>
                                                    <div id="call_logs_price" class="content"></div>
                                                </div>
                                            </li>
                                                                
                                         
                                        </ul>
                                    </div>
                                </div>

                            </div>
                        </div>

                    </div>

                </div>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-default" data-dismiss="modal">Cancel</button>
            </div>
        </div>
    </div>
</div>
<!--<script src="https://code.jquery.com/jquery-1.12.4.js"></script>
<script src="https://code.jquery.com/ui/1.12.1/jquery-ui.js"></script>-->

<div id="ratin">
    

</div>


<script>
    
    
   
    
    $( function() {

        $("#datepicker_from").datepicker({
            dateFormat: "yy-mm-dd",
            //minDate: 0,
            onSelect: function (date) {
                var date2 = $('#datepicker_from').datepicker('getDate');
                date2.setDate(date2.getDate() + 1);
                //   $('#datepicker_to').datepicker('setDate', date2);
                //sets minDate to dt1 date + 1
                $('#datepicker_to').datepicker('option', 'minDate', date2);
                var fDate = $(this).val();
                var todate=$("#datepicker_to").val();
                if(todate!=""){

                    $.ajax({
                    type: "POST",
                    url: "<?php echo base_url() ?>home/todaylist",
                    data: {date1: fDate,date2: todate},
                    success: function (data) {
                        console.log(data);
                        $("#logdata").html("");
                        $("#logdata").html(data);
                        $('#logdata').DataTable({ 
                            "destroy": true, //use for reinitialize datatable
                        });
           
                    }
                    });
                }
            }
        });
        $('#datepicker_to').datepicker({
        dateFormat: "yy-mm-dd",
        onClose: function () {
            var dt1 = $('#datepicker_from').datepicker('getDate');
            var dt2 = $('#datepicker_to').datepicker('getDate');
            //check to prevent a user from entering a date below date of dt1
            if (dt2 <= dt1) {
               var minDate = $('#datepicker_to').datepicker('option', 'minDate');
               $('#datepicker_to').datepicker('setDate', minDate);
           }

            var tDate = $(this).val();
             var fDate=$("#datepicker_from").val();
            //alert(tDate);
                     $.ajax({
                   type: "POST",
                   url: "<?php echo base_url() ?>home/todaylist",
                   data: {date1: fDate,date2: tDate},
                   success: function (data) {
                       console.log(data);
                       $("#logdata").html("");
                       $("#logdata").html(data);
                       $('#logdata').DataTable({ 
                            "destroy": true, //use for reinitialize datatable
                  });
           
                }
            });
        }
   });
   });
       


        
</script>  

<script type="text/javascript">
  
  function show_ratings(d,e){
//alert(d);
         $.ajax({
           type: "POST",
           url: "<?php echo base_url() ?>home/ratelist",
           data: {cid: d,ccid: e},
           success: function (data) {
              // console.log(data);
             // alert(JSON.stringify(data));
          //    $("#logdata").html("");
               $("#ratin").html(data);
               $('#callRatings').modal('show');
           /*    $('#logdata').DataTable({ 
                    "destroy": true, //use for reinitialize datatable
          }); */
   
        }
    });


  }  

</script>

<?php $this->load->view('layout/footer.php') ?>
  
 
    