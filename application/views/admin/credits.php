<?php $this->load->view('layout/user_header.php') ?>
<div class="contents-main" id="contents-main" style="margin-top:3%">

    <article id="post-141" class="post-141 page type-page status-publish hentry">
        <div class="entry-content">
            <div class="iwj-dashboard clearfix">

                <div class="iwj-dashboard-menu-mobile">
                    <div class="dropdown">
                        <button class="btn btn-primary dropdown-toggle"  type="button" data-toggle="dropdown">Menu Dashboard <span class="caret"></span></button>

                        <?php $this->load->view('layout/menu.php') ?>       
                    </div>
                </div>
                <div class="iwj-dashboard-main overview">
                    <div class="iwj-dashboard-main-inner">
                        <div class="iwj-overview iwj-profile clearfix">
                          
 <?php if ($data1[0]->role_id == 4) { ?>
                            <div class="iwj-change-password iwj-block">
                                <form   method="post" class="iwj-form-2 iwj-login-form1">
                                    <h3 class="theme-color">Credit Details</h3>   
                                    <div class="row">
                                        <div style="padding-left: 0px;" class="col-md-6">
                                            <div class="panel panel-default" style=" height: 150px; background-color: rgb(251, 135, 142);">

                                                <div class="panel-body" style="display:inline-flex">

                                                    <div style="">
                                                        <i style="font-size:110px;margin-right: 19px;color: white;" class="fa fa-calendar-check-o"></i></div>

                                                    <div>  
                                                        <label style="margin:0;color: white;">Latest Credit Purchase</label>
                                                        <h2 style="margin:0;color: white;"> <?php if(!empty($coupon_details)){ echo $coupon_details->coupon_amount; }else{ echo "--"; }  ?> </h2>
                                                        <label style="color: white;">Purchased on : <?php if(!empty($coupon_details)){ echo date("d/m/Y", strtotime($coupon_details->purchase_date)); }  ?></label> 
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                        <div style="padding-left: 0px;" class="col-md-6">
                                            <div class="panel panel-default" style="height: 150px;    background-color: rgb(54, 159, 210);">

                                                <div class="panel-body" style="display:inline-flex">

                                                    <div style="">
                                                        <i style="font-size:110px;margin-right: 45px;color: white;" class="fa fa-inr"></i></div>

                                                    <div>  
                                                        <label style="margin:0;color: white;">Credit Balance</label>
                                                        <h2 style="margin:0;color: white;"> <?php if(!empty($coupon_bal)){ echo $coupon_bal->coupon_balance; } else { echo "--"; } ?> </h2>
                                                        <label style="color: white;">Expires on : 27/10/2019</label> 
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="clearfix"></div>
                                    <div class="iwj-respon-msg iwj-hide"></div>
                                    <div class="iwj-button-loader">
                                        <button type="button" onclick="buyCredit()" class="iwj-btn edit_btn iwj-btn-primary iwj-change-password-btn">Purchase Credit</button>
                            
                                    
                                    </div>
                                </form>
                            </div>
 <?php } ?>

                        </div>
                    </div>
                </div>


                <!-- iwj-sidebar-sticky-->
                <div class="iwj-dashboard-sidebar">
                    <div class="user-profile candidate clearfix">
                        <img alt='Peter Pham' src='<?php if($data1[0]->photo!="") {  echo $data1[0]->photo; }else { echo base_url()."uploads/images/user.jpg";  } ?>' srcset='' class='avatar avatar-96 photo' height='96' width='96' />            
                        <h4>
                            <span>Howdy!</span>
                            <?php echo $data1[0]->username; ?>          
                        </h4>
                    </div>
                    <div class="iwj-dashboard-menu">
                        <?php $this->load->view('layout/sidebar.php') ?>   

                    </div>
                </div>
            </div>
        </div><!-- .entry-content -->
        <div class="clearfix"></div>
        <footer class="entry-footer ">
        </footer><!-- .entry-footer -->
    </article><!-- #post-## -->
</div>

<div id="buyCredit" class="modal-popup modal fade iwj-register-form-popup">
    <div class="modal-dialog">
        <div class="modal-header">
            <h4 class="modal-title">Purchase Credit</h4>
            <button  type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
        </div>
        <div class="modal-content">
            <label style="margin:3px;left: 200px;">NOTE : Please check rates of counsellors. Starts at Rs. 400 for a session</label>
            <form action="<?php echo base_url(); ?>payment/index" method="post" id="purchase_more_credit" class="iwj-form iwj-login-form1">
                <input type="hidden" name="ind_type" value="user">
                <input type="hidden" name="qty_input" value="1">
                
                <div class="iwj-field">

                    <div class="iwj-input">

                        <input type="text" id="credit_bal" name="total_input" placeholder="Enter Credit Amount">
                    </div>
                </div>


                <div class="iwj-respon-msg iwj-hide"></div>
                <div class="iwj-button-loader">

                    <button type="submit" id="buy_credit1" name="register" class="iwj-btn iwj-btn-primary iwj-btn-full iwj-btn-medium iwj-register-btn">Proceed to pay</button>
                </div>


            </form>
            
        </div>
    </div>
</div>
<script>

    $('#buy_credit1').on('click',function(e){

        e.preventDefault();
        var credit = $('#credit_bal').val();
        var form = $(this).parents('form');
        var ret = true;

        if (credit == 0) {
            swal("Oops!", "Please enter an amount and proceed to pay!!!", "error")
            ret = false;
        }

        if (ret !== false) {
            form.submit();
        }

    });        
</script>
<?php $this->load->view('layout/footer.php') ?>
