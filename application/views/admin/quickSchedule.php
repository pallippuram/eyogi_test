<?php $this->load->view('layout/user_header.php') ?>
<div class="contents-main" id="contents-main" style="margin-top:3%">

    <article id="post-141" class="post-141 page type-page status-publish hentry">
        <div class="entry-content">
            <div class="iwj-dashboard clearfix">

                <div class="iwj-dashboard-menu-mobile">
                    <div class="dropdown">
                        <button class="btn btn-primary dropdown-toggle"  type="button" data-toggle="dropdown">Menu Dashboard <span class="caret"></span></button>

                        <?php $this->load->view('layout/menu.php') ?>    

                    </div>
                </div>
                <div class="iwj-dashboard-main profile">
                    <div class="iwj-dashboard-main-inner">
                        <div class="iwj-profile clearfix">
                            <div class="iwj-edit-candidate-profile iwj-edit-profile-page">



                                <div class="form_error">
                                    <?php echo validation_errors(); ?>
                                </div>
                                <div class="iwj-block">
                                    <div class="basic-area iwj-block-inner">
                                        <div class="iwjmb-field iwjmb-select_advanced-wrapper  required">

                                            <div class="iwjmb-input">
                                                <input type="radio" class="radio_email" id="registered_user" value="email" name="radio_email" checked>
                                                <label style="display:inline; color: rgb(148, 150, 153)" for="registered_user">Registered User</label>

                                                <input type="radio" class="radio_phone" id="anonymous_user" value="phone" name="radio_email">
                                                <label  style="display:inline; color: rgb(148, 150, 153)" for="anonymous_user">Anonymous User</label>

                                            </div>
                                        </div>  
                                        <div id="registered_user_form"> 
                                            <form method="post" enctype="multipart/form-data" class="iwj-form-2 iwj-login-form1">

                                                <div class="row">
                                                    <div class="col-md-6">
                                                        <div class="iwjmb-field iwjmb-text-wrapper  required">
                                                            <div class="iwjmb-label">
                                                                <label class="theme-color" for="your_name">Counselor</label>
                                                            </div>
                                                            <div class="iwjmb-input ui-sortable">

                                                                <select id="couns_name" data-options="{&quot;allowClear&quot;:false,&quot;width&quot;:&quot;none&quot;,&quot;placeholder&quot;:&quot;&quot;,&quot;minimumResultsForSearch&quot;:-1}" required  class="iwjmb-select_advanced" name="gender">
                                                                    <option selected="">Please Select</option>
                                                                  <?php  foreach ($counsDetails as $user) { 
                                                                      if($user->status=="approve"){
                                                                      ?>
                                                                    <option value="<?=$user->id?>"><?= $user->username ?></option>
                                                                    
                                                                  <?php } } ?>
                                                                </select>
                                                            </div>
                                                        </div>
                                                        <div class="iwjmb-field iwjmb-email-wrapper  required" id="user_registered">
                                                            <div class="iwjmb-label">
                                                                <label class="theme-color" for="email">User</label>
                                                            </div>
                                                            <div class="iwjmb-input ui-sortable">

                                                                <select id="user_name" data-options="{&quot;allowClear&quot;:false,&quot;width&quot;:&quot;none&quot;,&quot;placeholder&quot;:&quot;&quot;,&quot;minimumResultsForSearch&quot;:-1}" required  class="iwjmb-select_advanced" name="gender">
                                                                    <option selected="">Please Select</option>
                                                                    <?php  foreach ($userDetails as $user) { 
                                                                     
                                                                      ?>
                                                                    <option value="<?=$user->id?>"><?= $user->username ?></option>
                                                                    
                                                                  <?php } ?>

                                                                </select>
                                                            </div>
                                                        </div>  
                                                        <div class="iwjmb-field iwjmb-email-wrapper  required" id="user_anonymous" style="display: none">
                                                            <div class="iwjmb-label">
                                                                <label class="theme-color" for="email">Code</label>
                                                            </div>
                                                            <div class="iwjmb-input ui-sortable">
                                                                <input size="30" placeholder="Enter code" value="" type="text" id="user_code"   class="iwjmb-text " name="phone">
                                                            </div>
                                                        </div> 
                                                    </div>
                                                    <div class="col-md-6">
                                                        <div class="iwjmb-field iwjmb-text-wrapper  required">
                                                            <div class="iwjmb-label"><label class="theme-color" for="_iwj_phone">From Phone *</label>
                                                            </div>
                                                            <div class="iwjmb-input ui-sortable">
                                                                <input size="30" placeholder="Enter from phone"  id="counse_ph" value="" type="text" required="required" class="iwjmb-text " name="phone">
                                                            </div>
                                                        </div>
                                                        <div class="iwjmb-field iwjmb-email-wrapper  required">
                                                            <div class="iwjmb-label">
                                                                <label class="theme-color" for="email">To Phone *</label>
                                                            </div>
                                                            <div class="iwjmb-input ui-sortable">
                                                                <input value="" placeholder="Enter to phone" id="user_ph" type="text" required="required" class="iwjmb-email " name="password">
                                                            </div>
                                                        </div>   
                                                    </div>
                                                </div>
                                                <div class="row">
                                                    <div class="col-md-6">
                                                        <div class="iwjmb-field iwjmb-text-wrapper  required"><div class="iwjmb-label"><label class="theme-color" for="_iwj_headline">Time Starts</label></div>
                                                            <div class="iwjmb-input ui-sortable">
                                                                        <input id="startTime_quick" class="iwjmb-text" type="text" />
                                                                    </div>
                                                                    <span style="color:red;display: none;margin-left: 20px;font-weight: bold;">This slot is already booked!!</span>
                                                        </div>                   
                                                    </div>
                                                    <input type="hidden" name="employee" value="quick" id="sche_type">
                                                    <div class="col-md-6">
                                                        <div class="iwjmb-field iwjmb-text-wrapper  required">
                                                            <div class="iwjmb-label"><label class="theme-color" for="_iwj_address">Time Ends</label></div>
                                                            <div class="iwjmb-input ui-sortable">
                                                                        <input id="endTime_quick" class="iwjmb-text" type="text" />
                                                                    </div>
                                                                    <span style="color:red;display: none;margin-left: 20px;font-weight: bold;">This slot is already booked!!</span>
                                                        </div> 
                                                    </div>
                                                    
                                                </div>
                                                <div class="row">
                                                    <div class="col-md-6">
                                                        <div class="iwjmb-field iwjmb-text-wrapper  required"><div class="iwjmb-label"><label class="theme-color" for="_iwj_headline">Description</label></div>
                                                           <div class="iwjmb-input">
                                                               <textarea style="width:100%" id="quick_desc" cols="80" rows="3" placeholder="Description"  class="iwjmb-textarea  large-text" name="accrediations"></textarea>
                                                        </div>
                                                        </div>                   
                                                    </div>
                                                </div>
                                                
                                                <input id="dur_time" class="iwjmb-text" value="" type="hidden" />
                                                <div class="iwj-respon-msg hide"></div>
                                                <div class="iwj-button-loader">
                                                    <button type="button" id="quickschedule" class="iwj-btn pull-right edit_btn iwj-btn-primary iwj-candidate-btn">Save</button>
                                                    <button type="button" id="call_now" class="iwj-btn pull-right edit_btn iwj-btn-primary iwj-candidate-btn pull-left">Call Now</button>
                                                </div>

                                            </form>
                                        </div>


                                    </div>

                                </div>


                            </div>
                        </div>        
                    </div>
                </div>


                <!-- iwj-sidebar-sticky-->
                <div class="iwj-dashboard-sidebar">
                    <div class="user-profile candidate clearfix">
                        <img alt='Peter Pham' src='<?php if ($data1[0]->photo != "") {
                                        echo $data1[0]->photo;
                                    } else {
                                        echo base_url() . "uploads/images/user.jpg";
                                    } ?>' srcset='' class='avatar avatar-96 photo' height='96' width='96' />            <h4>
                            <span>Howdy!</span>
<?php echo $data1[0]->username; ?>          </h4>
                    </div>
                    <div class="iwj-dashboard-menu">
<?php $this->load->view('layout/sidebar.php') ?>   

                    </div>
                </div>
            </div>
        </div><!-- .entry-content -->
        <div class="clearfix"></div>
        <footer class="entry-footer ">
        </footer><!-- .entry-footer -->
    </article><!-- #post-## -->
</div>   
<script src='<?php echo base_url(); ?>js/jquery.timepicker.js'></script>
<script>
    $('#language1').val(["english", "hindi", "malayalam", "tamil"]).trigger("change");
    
    $("#registered_user").click(function () {
        $('#user_registered').show();
        $('#user_anonymous').hide();
       
    })
    
    $("#anonymous_user").click(function () {
        $('#user_anonymous').show();
        $('#user_registered').hide();
       
    })
</script>
<?php $this->load->view('layout/footer.php') ?>
