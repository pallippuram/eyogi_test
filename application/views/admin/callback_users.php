<?php $this->load->view('layout/user_header.php') ?>
<div class="contents-main" id="contents-main" style="margin-top:3%">

    <article id="post-141" class="post-141 page type-page status-publish hentry">
        <div class="entry-content">
            <div class="iwj-dashboard clearfix">

                <div class="iwj-dashboard-menu-mobile">
                    <div class="dropdown">
                        <button class="btn btn-primary dropdown-toggle"  type="button" data-toggle="dropdown">Menu Dashboard <span class="caret"></span></button>


                        <?php $this->load->view('layout/menu.php') ?>       
                    </div>
                </div>

                <div class="iwj-dashboard-main save-jobs">
                    <div class="iwj-dashboard-main-inner">
                        <div class="iwj-save-jobs iwj-main-block">
                            <form method="post"  class="iwj-form-2 iwj-login-form1">

                                <div class="info-top" style="padding-bottom:0">
                                    <h3 class=""><?php echo "Call-Back Users" ?></h3>

                                </div>

                            </form>

                            <div class="iwj-table-overflow-x">
                                <table class="table userlist" id="" class="display" cellspacing="0" width="100%">
                                    <thead>
                                        <tr>
                                            <th width="">ID</th>
                                            <th width="">User</th>
                                            <th width="">Email</th>
                                            <th width="">Phone</th>
                                            <th width="">Message</th>
                                            <th width="" class="text-center">Action</th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                        <?php
                                        $i = 1;
                                        foreach ($userDetails as $user) {
                                            ?>
                                            <tr id="save-job-1212" class="save-job-item">
                                                <td>
                                                    <?= $i ?>
                                                </td>
                                                <td>
                                                    <?= $user->name ?>
                                                </td>
                                                <td>
                                                    <?= $user->email ?>
                                                </td>
                                                <td> 
                                                    <?= $user->phone ?>
                                                </td>
                                                <td> 
                                                    <?= $user->message ?>
                                                </td>
                                                <td class="text-center">
                                                     <a style="padding-right: 20px" onclick="callBackDetails(<?= $user->id ?>)"><i class="fa fa-eye" title="View Details" aria-hidden="true"></i></a>
                                                    <a onclick="delCallbackUsers(<?=$user->id?>)" href=""><i class="fa fa-trash-o" title="Delete" aria-hidden="true"></i></a>
                                                </td>
                                            </tr>
                                            <?php $i++;
                                        } ?>


                                    </tbody>
                                </table>
                            </div>
                            <div class="modal fade" id="iwj-confirm-undo-save-job" role="dialog">

                            </div>

                        </div>
                        <div class="clearfix"></div>

                    </div>

                </div>


                <!-- iwj-sidebar-sticky-->
                <div class="iwj-dashboard-sidebar">
                    <div class="user-profile candidate clearfix">
                        <img alt='Peter Pham' src='<?php if($data1[0]->photo!="") { echo $data1[0]->photo; }else { echo base_url()."uploads/images/user.jpg";  } ?>' srcset='' class='avatar avatar-96 photo' height='96' width='96' />           
                        <h4>
                            <span>Howdy!</span>
                            <?php echo $data1[0]->username; ?>          
                        </h4>
                    </div>
                    <div class="iwj-dashboard-menu">
                        <?php $this->load->view('layout/sidebar.php') ?>   
                    </div>
                </div>
            </div>
        </div><!-- .entry-content -->
        <div class="clearfix"></div>
        <footer class="entry-footer ">
        </footer><!-- .entry-footer -->
    </article><!-- #post-## -->
</div>

<div class="modal fade" id="callBackDetails">
    <div class="modal-dialog modal-lg">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">&times;</span><span class="sr-only">Close</span></button>
                <h4>Call Back User Details</h4>
            </div>
            <div class="modal-body">

                <div class="iw-job-content iw-job-detail" style="background: #ffffff;" id="contents-main">
                    <div class="container">

                        <div class="col-sm-12 col-xs-12 col-lg-8 col-md-8">
                            <div class="job-detail">
                                <div class="job-detail-content">
                                    <div class="job-detail-info">
                                        <ul>
                                            <li class="job-type">
                                                <div class="content-inner">
                                                    <div class="left">
                                                        
                                                        <span class="title">User :</span>
                                                    </div>
                                                    <div id="callback_user_name" class="content"></div>
                                                </div>
                                            </li>
                                              
                                            <li class="job-type">
                                                <div class="content-inner">
                                                    <div class="left">
                                                        
                                                        <span class="title">Date :</span>
                                                    </div>
                                                    <div id="callback_date" class="content"></div>
                                                </div>
                                            </li>
                                                                                      
                                            <li class="job-type">
                                                <div class="content-inner">
                                                    <div class="left">
                                                        
                                                        <span class="title">Message :</span>
                                                    </div>
                                                    <div id="callback_msg" class="content">
                                                                                               
                                                    </div>
                                                </div>
                                            </li>
                                            
                                            <li class="job-type">
                                                <div class="content-inner">
                                                    <div class="left">
                                                        
                                                        <span class="title">Phone :</span>
                                                    </div>
                                                    <div id="callback_phone" class="content"></div>
                                                </div>
                                            </li>
                                                                
                                         
                                        </ul>
                                    </div>
                                </div>

                            </div>
                        </div>

                    </div>

                </div>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-default" data-dismiss="modal">Cancel</button>
            </div>
        </div>
    </div>
</div>


<script>
    function callBackDetails(id) {
        
        $.ajax({
            url : '<?php echo base_url(); ?>index.php/home/call_back_details',
            data : {id: id},
            type : 'post',
            dataType : "json",
            success : function (data){
              
                $('#callback_user_name').text(data[0].name);
                $('#callback_date').text(data[0].date);
                $('#callback_msg').text(data[0].message);
                $('#callback_phone').text(data[0].phone);
                
                $('#callBackDetails').modal('show');
                
            }
        });

        
    }


</script>

<?php $this->load->view('layout/footer.php') ?>

