<?php $this->load->view('layout/user_header.php') ?>
<div class="contents-main" id="contents-main" style="margin-top:3%">

    <article id="post-141" class="post-141 page type-page status-publish hentry">
        <div class="entry-content">
            <div class="iwj-dashboard clearfix">

                <div class="iwj-dashboard-menu-mobile">
                    <div class="dropdown">
                        <button class="btn btn-primary dropdown-toggle"  type="button" data-toggle="dropdown">Menu Dashboard <span class="caret"></span></button>

                        <?php $this->load->view('layout/menu.php') ?>             
                    </div>
                </div>
                <div class="iwj-dashboard-main profile">
                    <div class="iwj-dashboard-main-inner">
                        <div class="iwj-profile clearfix">
                            <div class="iwj-edit-candidate-profile iwj-edit-profile-page">


                                <form method="post" enctype="multipart/form-data" action="<?php echo site_url('counselor/update_counselor'); ?>"  class="iwj-form-2 iwj-login-form1">


                                    <div class="iwj-block">
                                        <div class="basic-area iwj-block-inner">
                                            <input type="hidden" name="id_hidden" value="<?php echo $data->user_id; ?>">
                                            <div class="iwjmb-field iwjmb-avatar-wrapper"><div class="iwjmb-input ui-sortable">            
                                                    <div id="_iwj_avatar" class="iwj-avatar-container">
                                                        <!-- Current avatar -->
                                                        <div class="avatar-view">
                                                            <img src="<?php  echo @$data->photo; ?>" alt="Avatar">
                                                        </div>
                                                        <div class="desc-change-image">
                                                            <p class="avatar-description"></p>
                                                            <div class="change-image-btn" style="padding-top:50px">

                                                                <div class="choose_file btn btn-primary">
                                                                    <span>Choose File</span>
                                                                    <input name="image" type="file" />
                                                                </div>
                                                            </div>
                                                        </div>

                                                    </div>
                                                </div>
                                            </div>
                                            <div class="row">
                                                <div class="col-md-6">
                                                    <div class="iwjmb-field iwjmb-text-wrapper  required">
                                                        <div class="iwjmb-label">
                                                            <label class="theme-color" for="your_name">Your Name *</label>
                                                        </div>
                                                        <div class="iwjmb-input ui-sortable">
                                                            <input size="30" placeholder="Enter your name" value="<?php echo $data->username; ?>" type="text" id="your_name" class="iwjmb-text " name="username">
                                                        </div>
                                                    </div>
                                                    <div class="iwjmb-field iwjmb-email-wrapper  required">
                                                        <div class="iwjmb-label">
                                                            <label class="theme-color" for="email">Email *</label>
                                                        </div>
                                                        <div class="iwjmb-input ui-sortable">
                                                            <input value="<?php echo $data->email; ?>" placeholder="Enter your email" type="email" required="required" id="email" class="iwjmb-email " name="email">
                                                        </div>
                                                    </div>                    
                                                </div>
                                                <div class="col-md-6">
                                                    <div class="iwjmb-field iwjmb-text-wrapper  required">
                                                        <div class="iwjmb-label"><label class="theme-color" for="_iwj_phone">Phone *</label>
                                                        </div>
                                                        <div class="iwjmb-input ui-sortable">
                                                            <input size="30" placeholder="Enter your phone" value="<?php echo $data->phone; ?>" type="text" required="required" id="_iwj_phone" class="iwjmb-text " name="phone">
                                                        </div>
                                                    </div>
                                                    <div class="iwjmb-field iwjmb-text-wrapper  required">
                                                        <div class="iwjmb-label"><label class="theme-color" for="_iwj_address">Gender *</label></div>
                                                        <div class="iwjmb-input ui-sortable">
                                                            <select data-options="{&quot;allowClear&quot;:false,&quot;width&quot;:&quot;none&quot;,&quot;placeholder&quot;:&quot;&quot;,&quot;minimumResultsForSearch&quot;:-1}" required  class="iwjmb-select_advanced" name="gender">
                                                                <option value="0" <?php if($data->gender==0){ echo 'selected'; } ?> >Male</option>
                                                                <option value="1" <?php if($data->gender==1){ echo 'selected'; } ?>>Female</option>

                                                            </select>
                                                        </div>
                                                    </div>    
                                                </div>
                                            </div>

                                            <div class="row">
                                                <div class="col-md-6">
                                                  <div class="iwjmb-field iwjmb-text-wrapper  required"><div class="iwjmb-label"><label class="theme-color" for="_iwj_headline">Domain *</label></div>
                                                        <div class="iwjmb-input ui-sortable">

                                                            <select multiple id="couns_type" data-options="{&quot;allowClear&quot;:false,&quot;width&quot;:&quot;none&quot;,&quot;placeholder&quot;:&quot;&quot;,&quot;minimumResultsForSearch&quot;:-1}" required  class="iwjmb-select_advanced" name="domain[]">
                                                                
                                                               <?php  foreach ($counselling_types as $type) { ?>
                                                                <option value="<?=$type->type?>"><?=$type->type?></option>
                                                               <?php } ?>
                                                            </select>
                                                        </div>
                                                    </div>
                                                    
                                                </div>
                                                <div class="col-md-6">
                                                    <div class="iwjmb-field iwjmb-text-wrapper  required">
                                                        <div class="iwjmb-label"><label class="theme-color" for="_iwj_address">Experience *</label></div>
                                                        <div class="iwjmb-input ui-sortable">
                                                            <select data-options="{&quot;allowClear&quot;:false,&quot;width&quot;:&quot;none&quot;,&quot;placeholder&quot;:&quot;&quot;,&quot;minimumResultsForSearch&quot;:-1}" required class="iwjmb-select_advanced" name="experience">
                                                                
                                                                <?php if($data->experience!=""){ ?>
                                                                
                                                                <option value="1" <?php if($data->experience==1){ echo 'selected'; } ?>>1</option>
                                                                <option value="2" <?php if($data->experience==2){ echo 'selected'; } ?>>2</option>
                                                                <option value="3" <?php if($data->experience==3){ echo 'selected'; } ?>>3</option>
                                                                <option value="4" <?php if($data->experience==4){ echo 'selected'; } ?>>4</option>
                                                                <option value="5" <?php if($data->experience==5){ echo 'selected'; } ?>>5</option>
                                                                <option value="6" <?php if($data->experience==6){ echo 'selected'; } ?>>6</option>
                                                                <option value="7" <?php if($data->experience==7){ echo 'selected'; } ?>>7</option>
                                                                <option value="8" <?php if($data->experience==8){ echo 'selected'; } ?>>8</option>
                                                                <option value="9" <?php if($data->experience==9){ echo 'selected'; } ?>>9</option>
                                                                <option value="10" <?php if($data->experience==10){ echo 'selected'; } ?>>10</option>
                                                                <option value="11" <?php if($data->experience==11){ echo 'selected'; } ?>>11</option>
                                                                <option value="12" <?php if($data->experience==12){ echo 'selected'; } ?>>12</option>
                                                                <option value="13" <?php if($data->experience==13){ echo 'selected'; } ?>>13</option>
                                                                <option value="14" <?php if($data->experience==14){ echo 'selected'; } ?>>14</option>
                                                                <option value="15" <?php if($data->experience==15){ echo 'selected'; } ?>>15</option>
                                                                <?php }else{ ?>
                                                                <option selected=''>Please Select</option>
                                                                <option value="1">1</option>
                                                                <option value="2">2</option>
                                                                <option value="3">3</option>
                                                                <option value="4">4</option>
                                                                <option value="5">5</option>
                                                                <option value="6">6</option>
                                                                <option value="7">7</option>
                                                                <option value="8">8</option>
                                                                <option value="9">9</option>
                                                                <option value="10">10</option>
                                                                <option value="11">11</option>
                                                                <option value="12">12</option>
                                                                <option value="13">13</option>
                                                                <option value="14">14</option>
                                                                <option value="15">15</option>
                                                             <?php   } ?>                                                   
                                                            </select>
                                                        </div>
                                                    </div> 
                                                </div>
                                            </div>
                                            <div class="row">
                                                <div class="col-md-6">
                                                   <div class="iwjmb-field iwjmb-select_advanced-wrapper  required">
                                                        <div class="iwjmb-label">
                                                            <label style="color: #ff8e16;" for="frequency">Languages Known</label>
                                                        </div>
                                                        <div class="iwjmb-input">
                                                            <select id="language12" multiple data-options="{&quot;allowClear&quot;:false,&quot;width&quot;:&quot;none&quot;,&quot;placeholder&quot;:&quot;&quot;,&quot;minimumResultsForSearch&quot;:-1}" required class="iwjmb-select_advanced" name="languages[]">

                                                                <option value="english"  selected="selected" >English</option>
                                                                <option value="hindi">Hindi</option>
                                                                <option value="malayalam" >Malayalam</option>
                                                                <option value="tamil" >Tamil</option>
                                                                <option value="urdu" >Urdu</option>
                                                                <option value="gujarati" >Gujarati</option>
                                                                <option value="telugu" >Telugu</option>
                                                            </select>
                                                        </div>
                                                    </div>                    
                                                </div>
                                                <div class="col-md-6">
                                                   <div class="iwjmb-field">

                                                        <label style="color: #ff8e16;" for="frequency">Certifications</label>

                                                        <div class="iwjmb-input">
                                                            <textarea style="width:100%"  cols="80" rows="2" placeholder="Certifications" value="<?php if($data->certifications!=""){ echo $data->certifications; } ?>" required id="message" class="iwjmb-textarea  large-text" name="certifications"><?php if($data->certifications!=""){ echo $data->certifications; } ?></textarea>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                            
                                            <div class="row">
                                                <div class="col-md-6">
                                                    <div class="iwjmb-field">
                                                        <div class="iwjmb-label">
                                                            <label style="color: #ff8e16;"  for="frequency">Accreditations</label>
                                                        </div>
                                                        <div class="iwjmb-input">
                                                            <textarea style="width:100%" cols="80" rows="2" placeholder="Accreditations" value="<?php if($data->accrediations!=""){ echo $data->accrediations;  } ?>" required  class="iwjmb-textarea  large-text" name="accrediations"><?php if($data->accrediations!=""){ echo $data->accrediations;  } ?></textarea>
                                                        </div>
                                                    </div>                      
                                                </div>
                                                
                                            </div>
                                            
                                        </div>


                                        <div class="iwj-respon-msg hide"></div>
                                        <div class="iwj-button-loader">
                                            <button type="submit" class="iwj-btn edit_btn iwj-btn-primary iwj-candidate-btn">Update Profile</button>
                                        </div>
                                    </div>
                                </form>

                                <div class="iwj-change-password iwj-block">
                                    <form  action="<?php echo site_url('admin/update_password'); ?>" method="post" class="iwj-form-2 iwj-login-form1">
                                        <h3 class="theme-color">Change Password</h3>   
                                        <input type="hidden" value="<?php echo $data->id ?>" name="user_id">
                                        <div class="row">
                                            <div class="col-md-6">
                                                <div class="iwjmb-field iwjmb-password-wrapper  required">
                                                    <div class="iwjmb-label">
                                                        <label class="theme-color" for="current_password">Current Password</label>
                                                    </div>
                                                    <div class="iwjmb-input ui-sortable">
                                                        <input type="password" required="required" id="current_password" class="iwjmb-password " name="current_password">
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="col-md-6">
                                                <div class="iwjmb-field iwjmb-password-wrapper  required">
                                                    <div class="iwjmb-label">
                                                        <label class="theme-color" for="new_password">New Password</label>
                                                    </div>
                                                    <div class="iwjmb-input ui-sortable">
                                                        <input type="password" required="required" id="new_password" class="iwjmb-password " name="new_password">
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="clearfix"></div>
                                        <div class="iwj-respon-msg iwj-hide"></div>
                                        <div class="iwj-button-loader">
                                            <button type="submit" class="iwj-btn edit_btn iwj-btn-primary iwj-change-password-btn">Change Password</button>
                                        </div>
                                    </form>
                                </div>
                            </div>
                        </div>        
                    </div>
                </div>


                <!-- iwj-sidebar-sticky-->
                <div class="iwj-dashboard-sidebar">
                    <div class="user-profile candidate clearfix">
                        <img alt='Peter Pham' src='<?php if($data1[0]->photo!="") { echo $data1[0]->photo; }else { echo base_url()."uploads/images/user.jpg";  } ?>' srcset='' class='avatar avatar-96 photo' height='96' width='96' />            
                        <h4>
                            <span>Howdy!</span>
                            <?php echo $data1[0]->username; ?>          
                        </h4>
                    </div>
                    <div class="iwj-dashboard-menu">
                       <?php $this->load->view('layout/sidebar.php') ?>    

                    </div>
                </div>
            </div>
        </div><!-- .entry-content -->
        <div class="clearfix"></div>
        <footer class="entry-footer ">
        </footer><!-- .entry-footer -->
    </article><!-- #post-## -->
</div>
<script>
 $(function () {
        var lang=<?php echo $data->languages ?>;
       
        $('#language12').val(lang).trigger("change");
        
        var type=<?php echo $data->domain ?>;
         $('#couns_type').val(type).trigger("change");
        
    });
</script>
<?php $this->load->view('layout/footer.php') ?>
