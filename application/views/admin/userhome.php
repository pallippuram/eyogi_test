<?php $this->load->view('layout/user_header.php') ?>
<div class="contents-main" id="contents-main" style="margin-top:3%">

    <article id="post-141" class="post-141 page type-page status-publish hentry">
        <div class="entry-content">
            <div class="iwj-dashboard clearfix">

                <div class="iwj-dashboard-menu-mobile">
                    <div class="dropdown">
                        <button class="btn btn-primary dropdown-toggle"  type="button" data-toggle="dropdown">Menu Dashboard <span class="caret"></span></button>
					<?php $this->load->view('layout/menu.php') ?>
					</div>
                </div>

                <div class="iwj-dashboard-main overview">
                    <div class="iwj-dashboard-main-inner">
                        <div class="iwj-overview iwj-profile clearfix">
                            <div style="padding-left: 0px;" class="col-md-12">
                                <div class="row">
                                <div style="padding-left: 0px;" class="col-md-6">
                                    <div class="panel panel-default" style="height: 150px;    background-color: rgb(54, 159, 210);">

                                        <div class="panel-body" style="display:inline-flex">

                                            <div style="">
                                                <i style="font-size:110px;margin-right: 45px;color: white;" class="fa fa-inr"></i></div>

                                            <div>  
                                                <label style="margin:0;color: white;">Credit Balance</label>
                                                <h2 style="margin:0;color: white;"> 100.00 </h2>
                                                <label style="color: white;">Expires on : 27/10/2019</label> 
                                            </div>
                                        </div>
                                    </div>
                                </div>


                                <div style="padding-left: 0px;" class="col-md-6">
                                    <div class="panel panel-default" style=" height: 150px; background-color: rgb(251, 135, 142);">

                                        <div class="panel-body" style="display:inline-flex">

                                            <div style="">
                                                <i style="font-size:110px;margin-right: 19px;color: white;" class="fa fa-calendar-check-o"></i></div>

                                            <div>  
                                                <label style="margin:0;color: white;">Credit Balance</label>
                                                <h2 style="margin:0;color: white;"> 100.00 </h2>
                                                <label style="color: white;">Expires on : 27/10/2019</label> 
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-md-3 iwj-sidebar-sticky iwj-sidebar-1" style="top:0px">
                                    <div class="widget-area sidebar-jobs-1">
                                        <aside id="iwj_job_filter-2" class="widget sidebar-jobs widget_iwj_job_filter">


                                            <div id="iwj-filter-selected" class="iwj-filter-selected" style="display: none;">
                                                <h3 class="widget-title"><span>Your selected</span></h3>
                                            </div>

                                            <div id="iwj-clear-filter-btn" class="facet open-filters" style="display: none;">
                                                <button class="btn btn-primary" id="clear-filter-job">
                                                    Clear Filter</button>
                                            </div>

                                            <aside class="widget sidebar-jobs-item">
                                                <h3 class="widget-title"><span>Domain</span></h3>
                                                <div class="job-categories sidebar-job-1">
                                                    <form name="iwjob-categories" id="iwjob-categories">
                                                        <input type="hidden" name="limit" value="5" />
                                                        <input type="hidden" name="limit_show_more" value="20" />
                                                        <ul class="iwjob-list-categories tags">
                                                            <li  class="theme-color-hover iwj-input-checkbox iwj_cat" >
                                                                <input type="checkbox"  name="iwj_cat" id="counselor_chk" value="MS">
                                                                <label for="counselor_chk">
                                                                    Counselor</label>
                                                                <span id="iwj-count-178" class="iwj-count">
                                                                    13</span>
                                                            </li>
                                                            <li  class="theme-color-hover iwj-input-checkbox iwj_cat" data-order="4">
                                                                <input type="checkbox" id="mentor_chk"  name="iwj_cat"

                                                                       value="MD" >
                                                                <label for="mentor_chk">
                                                                    Mentor</label>
                                                                <span id="iwj-count-73" class="iwj-count">
                                                                    4</span>
                                                            </li>


                                                            <li class="show-more"><a class="theme-color" href="#">Show more</a></li>
                                                            <li class="show-less" style="display: none"><a class="theme-color" href="#">Show less</a></li>
                                                        </ul>
                                                    </form>
                                                </div>
                                            </aside>


                                            <aside class="widget sidebar-jobs-item">
                                                <h3 class="widget-title"><span>Experience</span></h3>
                                                <div class="job-skills sidebar-job-1">
                                                    <form name="iwjob-skills" id="iwjob-skills">
                                                        <input type="hidden" name="limit" value="5" />
                                                        <input type="hidden" name="limit_show_more" value="20" />
                                                        <ul class="iwjob-list-skills">

                                                            <li  class="theme-color-hover iwj-input-checkbox iwj_skill">
                                                                <input  type="checkbox"  name="iwj_skill"
                                                                        id="yr1"
                                                                        value="1" >
                                                                <label for="yr1">
                                                                    1 Year        </label>

                                                                <span id="iwj-count-97" class="iwj-count"> 9</span>
                                                            </li>
                                                            <li  class="theme-color-hover iwj-input-checkbox iwj_skill">
                                                                <input type="checkbox"  name="iwj_skill"
                                                                       id="yr2"
                                                                       value="2" >
                                                                <label for="yr2">
                                                                    2 Years        </label>

                                                                <span id="iwj-count-85" class="iwj-count"> 8</span>
                                                            </li>
                                                            <li  class="theme-color-hover iwj-input-checkbox iwj_skill" >
                                                                <input type="checkbox"  name="iwj_skill"
                                                                       id="yr3" 
                                                                       value="3" >
                                                                <label for="yr3">
                                                                    3 Years       </label>

                                                                <span id="iwj-count-57" class="iwj-count"> 7</span>
                                                            </li>
                                                            <li   class="theme-color-hover iwj-input-checkbox iwj_skill" data-order="5">
                                                                <input data-filter="green" type="checkbox"  name="iwj_skill"
                                                                       id="yr4" 
                                                                       value="4" >
                                                                <label for="yr4">
                                                                    4 Years      </label>

                                                                <span id="iwj-count-149" class="iwj-count"> 5</span>
                                                            </li>
                                                            <li  class="theme-color-hover iwj-input-checkbox iwj_skill" data-order="5">
                                                                <input type="checkbox"  name="iwj_skill"
                                                                       id="yr5" 
                                                                       value="5" >
                                                                <label for="yr5">
                                                                    5 Years        
                                                                </label>

                                                                <span id="iwj-count-137" class="iwj-count"> 5</span>
                                                            </li>

                                                            <li style="display:none" class="theme-color-hover iwj-input-checkbox iwj_skill" data-order="4">
                                                                <input type="checkbox"  name="iwj_skill"
                                                                       id="yr6" 
                                                                       value="6" >
                                                                <label for="yr6">
                                                                    6 Years         </label>

                                                                <span id="iwj-count-111" class="iwj-count"> 4</span>
                                                            </li>
                                                            <li style="display:none" class="theme-color-hover iwj-input-checkbox iwj_skill" data-order="4">
                                                                <input data-filter="green" type="checkbox"  name="iwj_skill"
                                                                       id="iwjob-filter-jobs-cbx-121" class="iwjob-filter-jobs-cbx"
                                                                       value="7" >
                                                                <label for="iwjob-filter-jobs-cbx-121">
                                                                    7 Years         </label>

                                                                <span id="iwj-count-121" class="iwj-count"> 4</span>
                                                            </li>
                                                            <li style="display:none" class="theme-color-hover iwj-input-checkbox iwj_skill" data-order="3">
                                                                <input type="checkbox"  name="iwj_skill"
                                                                       id="iwjob-filter-jobs-cbx-99" class="iwjob-filter-jobs-cbx"
                                                                       value="8" data-title="jQuery">
                                                                <label for="iwjob-filter-jobs-cbx-99">
                                                                    8 Years         </label>

                                                                <span id="iwj-count-99" class="iwj-count"> 3</span>
                                                            <li style="display:none" class="theme-color-hover iwj-input-checkbox iwj_skill" data-order="3">
                                                                <input type="checkbox"  name="iwj_skill"
                                                                       id="iwjob-filter-jobs-cbx-90" class="iwjob-filter-jobs-cbx"
                                                                       value="9" data-title="Integrated Marketing">
                                                                <label for="iwjob-filter-jobs-cbx-90">
                                                                    9 Years      </label>

                                                                <span id="iwj-count-90" class="iwj-count"> 3</span>
                                                            <li style="display:none" class="theme-color-hover iwj-input-checkbox iwj_skill" data-order="3">
                                                                <input type="checkbox"  name="iwj_skill[]"
                                                                       id="iwjob-filter-jobs-cbx-128" class="iwjob-filter-jobs-cbx"
                                                                       value="128" data-title="NodeJS">
                                                                <label for="iwjob-filter-jobs-cbx-128">
                                                                    10 Years         </label>

                                                                <span id="iwj-count-128" class="iwj-count"> 3</span>
                                                            <li class="show-more"><a class="theme-color" href="#">Show more</a></li>
                                                            <li class="show-less" style="display: none"><a class="theme-color" href="#">Show less</a></li>
                                                        </ul>
                                                    </form>
                                                </div>
                                            </aside>

                                            <input type="hidden" value="job" name="wgit_job_filter" id="wgit-job-filter" />
                                        </aside>


                                    </div>
                                </div>
                                <div class="col-md-6">
                                    <div class="iwj-content">

                                        <article id="post-656" class="post-656 page type-page status-publish hentry">
                                            <div class="entry-content">
                                                <div class="vc_row wpb_row vc_row-fluid"><div class="wpb_column vc_column_container vc_col-sm-12"><div class="vc_column-inner"><div class="wpb_wrapper"><div class="iwj-content-inner">
                                                                    <div class="iwj-filter-form">
                                                                        <div class="jobs-layout-form">
                                                                            <form>
                                                                                <div class='col-sm-6'>
                                                                                    <div class="form-group">
                                                                                        <div class='input-group date' id='datetimepicker1'>
                                                                                            <input type='text' class="form-control" />
                                                                                            <span class="input-group-addon">
                                                                                                <span class="glyphicon glyphicon-calendar"></span>
                                                                                            </span>
                                                                                        </div>
                                                                                    </div>
                                                                                </div>

                                                                                <div class="clear"></div>
                                                                            </form>
                                                                            <a href="#" class="job-alert-btn" style="background-color: rgb(48, 194, 189);margin-left: 15px" >Now</a>
                                                                        </div>
                                                                    </div>

                                                                    <div id="iwajax-load">
                                                                        <div class="iwj-jobs iwj-listing">
                                                                            <div class="iwj-job-items" id="cn_content">


                                                                                <?php
                                                                                if (count($data) > 0) {
                                                                                    foreach ($data as $value) {
                                                                                        ?>
                                                                                        <div class="grid-content"  data-id="1346">
                                                                                            <div class="job-item featured-item">
                                                                                                <div class="job-image"><img alt='AOEVN' src='<?php echo base_url() ?>uploads/images/<?php echo $value->photo; ?>' class='avatar avatar-full photo' width='408' height='401' /></div>
                                                                                                <div class="job-info">
                                                                                                    <h3 class="job-title"><a href="employee.html"><?php echo $value->username ?></a></h3>
                                                                                                    <div class="info-company">
                                                                                                        <div class="company"><i class="fa fa-suitcase"></i>
                                                                                                            <a href="employee.html"><?php echo $value->domain ?></a>
                                                                                                        </div>
                                                                                                        <div class="address"><i class="fa fa-cogs"></i><?php echo $value->experience ?> years Exp</div>
                                                                                                        <div class="sallary"><i class="fa fa-inr"></i>Rs 900 - Rs 1,500</div>

                                                                                                    </div>
                                                                                                    <span class="job-featured"><a class="type-name" href="employee.html" >Details</a></span>
                                                                                                    <div class="job-type full-time">
                                                                                                        <a class="type-name" href="employee.html">Schedule</a>

                                                                                                    </div>
                                                                                                </div>
                                                                                                <div class="job-featured"></div>
                                                                                            </div>
                                                                                        </div>
                                                                                        <?php
                                                                                    }
                                                                                }
                                                                                ?>
                                                                                <div class="clearfix"></div>
                                                                            </div>

                                                                            <div class="w-pagination">
                                                                                <div class="iwjob-ajax-pagination pagination-main"><ul class="pagination pagination-job page-nav"><li class="disabled page-numbers"><span aria-label="Previous"><span aria-hidden="true"><i class="fa fa-angle-left"></i></span></span></li><li class="active page-numbers" data-paged="1"><a >1 <span class="sr-only"></span></a></li><li class="page-numbers" data-paged="2"><a >2</a></li><li class="page-numbers" data-paged="3"><a >3</a></li><li class="page-numbers" data-paged="2"><a aria-label="Next" ><span aria-hidden="true"><i class="fa fa-angle-right"></i></span></a></li></ul><input type="hidden" name="page_number" value="1" /></div>               
                                                                            </div>
                                                                        </div>
                                                                    </div>

                                                                    <input type="hidden" name="url" id="url" value="./jobs">
                                                                    <form name="iwjob-filter-url">

                                                                    </form>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div><!-- .entry-content -->
                                            <div class="clearfix"></div>
                                            <footer class="entry-footer">
                                            </footer><!-- .entry-footer -->
                                        </article><!-- #post-## -->
                                    </div>
                                </div>

                            </div>
                        </div>

                    </div>
                </div>
				 <!-- iwj-sidebar-sticky-->
                <div class="iwj-dashboard-sidebar">
                    <div class="user-profile candidate clearfix">
                        <img alt='Peter Pham' src='<?php if($data1[0]->photo!="") { echo $data1[0]->photo; }else { echo base_url()."uploads/images/user.jpg";  } ?>' srcset='' class='avatar avatar-96 photo' height='96' width='96' />           
                        <h4>
                            <span>Howdy!</span>
                            <?php echo $data1[0]->username; ?>          
                        </h4>
                    </div>
                    <div class="iwj-dashboard-menu">
                <?php $this->load->view('layout/sidebar.php') ?>
				 </div>
                </div>
            </div>
        </div><!-- .entry-content -->
        <div class="clearfix"></div>
        <footer class="entry-footer ">
        </footer><!-- .entry-footer -->
    </article><!-- #post-## -->
</div>




<script type="text/javascript">
    $(function () {
        $('#datetimepicker1').datetimepicker();
    });
    $(function () {
        $('#datetimepicker12').datetimepicker();
    });
    $('#balance_check_submit').on('click', function () {

        $('#purchase_code').hide();
        $('#credit_balance').show();
        return false;
    });
    $('#reschedule_submit').on('click', function () {

        $('#reschedule_purchase_code').hide();
        $('#reschedule').show();
        return false;
    });
    $('#reschedule_btn').on('click', function () {

        $('#reschedule').hide();
        $('#reschedule_confirm').show();
        return false;
    });
</script>
<?php $this->load->view('layout/footer.php') ?>