<?php $this->load->view('layout/user_header.php') ?>
<link rel="stylesheet" href="//code.jquery.com/ui/1.12.1/themes/base/jquery-ui.css">
<div class="contents-main" id="contents-main" style="margin-top:3%">

    <article id="post-141" class="post-141 page type-page status-publish hentry">
        <div class="entry-content">
            <div class="iwj-dashboard clearfix">

                <div class="iwj-dashboard-menu-mobile">
                    <div class="dropdown">
                        <button class="btn btn-primary dropdown-toggle"  type="button" data-toggle="dropdown">Menu Dashboard <span class="caret"></span></button>


                        <?php $this->load->view('layout/menu.php') ?>
                    </div>
                </div>

                <div class="iwj-dashboard-main save-jobs">
                    <div class="iwj-dashboard-main-inner">
                        <div class="iwj-save-jobs iwj-main-block">

                            <form method="post"  class="iwj-form-2 iwj-login-form1">

                                <div class="info-top" style="padding-bottom:0">
                                    <h3 class=""><?php echo "Purchase History" ?></h3>

                                </div>

                            </form>

                            <div class="iwj-table-overflow-x">
                                <p id="date_filter">
                                    <span id="date-label-from" class="date-label">From: </span><input class="date_range_filter date" type="text" id="datepicker2_from" />
                                    <span id="date-label-to" class="date-label">To: </span><input class="date_range_filter date" type="text" id="datepicker2_to" />
                                </p>
                                <table class="table userlist" id="purchase_data" class="display" cellspacing="0" width="100%">
                                    <thead>
                                        <tr>
                                            <th width="">ID</th>
                                            <th width="">Amount</th>
                                            <th width="">Purchase Date</th>
                                            <th width="">Status</th>
                                            <th width="" class="text-center">Action</th>
                                            
                                        </tr>
                                    </thead>
                                    <tbody>
                                        <?php
                                        $i = 1;
                                        foreach ($payment as $user) {
                                            
                                            $timestamp =  $user->purchase_date;
                                            if( date('Y-m-d',strtotime($timestamp)) == date('Y-m-d')){
                                            ?>
                                            <tr id="save-job-1212" class="save-job-item">
                                                <td>
                                                    <?= $i ?>
                                                </td>
                                                <td>
                                                    <?= $user->coupon_amount ?>
                                                </td>
                                                <td>
                                                    <?php $timestamp =  $user->purchase_date;
                                                          echo date('Y-m-d',strtotime($timestamp));
                                                          echo " / ";
                                                          echo date('h:i A',strtotime($timestamp));
                                                    ?>
                                                </td>
                                                <td> 
                                                    <?= $user->payment_status ?>
                                                </td>
                                                <td class="text-center">
                                                    <a style="padding-right: 20px" onclick="purchase_details(<?= $user->id ?>)"><i class="fa fa-eye" title="View Details" aria-hidden="true"></i></a>
                                                    <a onclick="delpurchase(<?=$user->id?>)" href=""><i class="fa fa-trash-o" title="Delete" aria-hidden="true"></i></a>
                                                </td>   
                                               
                                            </tr>
                                            
                                            <?php $i++;
                                            }
                                        }
                                        ?>


                                    </tbody>
                                </table>
                            </div>
                            <div class="modal fade" id="iwj-confirm-undo-save-job" role="dialog">

                            </div>

                        </div>
                        <div class="clearfix"></div>

                    </div>

                </div>


                <!-- iwj-sidebar-sticky-->
                <div class="iwj-dashboard-sidebar">
                    <div class="user-profile candidate clearfix">
                        <img alt='Peter Pham' src='<?php if($data1[0]->photo!="") {  echo $data1[0]->photo; }else { echo base_url()."uploads/images/user.jpg";  } ?>' srcset='' class='avatar avatar-96 photo' height='96' width='96' />           
                        <h4>
                            <span>Howdy!</span>
                            <?php echo $data1[0]->username; ?>          
                        </h4>
                    </div>
                    <div class="iwj-dashboard-menu">
                        <?php $this->load->view('layout/sidebar.php') ?>
                    </div>
                </div>
            </div>
        </div><!-- .entry-content -->
        <div class="clearfix"></div>
        <footer class="entry-footer ">
        </footer><!-- .entry-footer -->
    </article><!-- #post-## -->
</div>

<div class="modal fade" id="purchaseReport">
    <div class="modal-dialog modal-lg">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">&times;</span><span class="sr-only">Close</span></button>
                <h4>Purchase Report</h4>
            </div>
            <div class="modal-body">

                <div class="iw-job-content iw-job-detail" style="background: #ffffff;" id="contents-main">
                    <div class="container">

                        <div class="col-sm-12 col-xs-12 col-lg-8 col-md-8">
                            <div class="job-detail">
                                <div class="job-detail-content">
                                    <div class="job-detail-info">
                                        <ul>
                                            <li class="job-type">
                                                <div class="content-inner">
                                                    <div class="left">
                                                        
                                                        <span class="title">Amount :</span>
                                                    </div>
                                                    <div id="pr_amount" class="content"></div>
                                                </div>
                                            </li>
                                              
                                            <li class="job-type">
                                                <div class="content-inner">
                                                    <div class="left">
                                                        
                                                        <span class="title">Purchase Date :</span>
                                                    </div>
                                                    <div id="pr_date" class="content"></div>
                                                </div>
                                            </li>
                                                                                      
                                            <li class="job-type">
                                                <div class="content-inner">
                                                    <div class="left">
                                                        
                                                        <span class="title">Status :</span>
                                                    </div>
                                                    <div id="pr_status" class="content">
                                                                                               
                                                    </div>
                                                </div>
                                            </li>  
                                        </ul>
                                    </div>
                                </div>

                            </div>
                        </div>

                    </div>

                </div>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-default" data-dismiss="modal">Cancel</button>
            </div>
        </div>
    </div>
</div>

<script>



    $( function() {

        $("#datepicker2_from").datepicker({
            dateFormat: "yy-mm-dd",
            //minDate: 0,
            onSelect: function (date) {
                var date2 = $('#datepicker2_from').datepicker('getDate');
                date2.setDate(date2.getDate() + 1);
                //   $('#datepicker_to').datepicker('setDate', date2);
                //sets minDate to dt1 date + 1
                $('#datepicker2_to').datepicker('option', 'minDate', date2);
                var fDate = $(this).val();
                var todate=$("#datepicker2_to").val();
                if(todate!=""){

                    $.ajax({
                    type: "POST",
                    url: "<?php echo base_url() ?>home/purchaseList",
                    data: {date1: fDate,date2: todate},
                    success: function (data) {

                        $("#purchase_data").html("");
                        $("#purchase_data").html(data);
                        $('#purchase_data').DataTable({ 
                            "destroy": true, //use for reinitialize datatable
                        });
           
                    }
                    });
                }
            }
        });
        $('#datepicker2_to').datepicker({
        dateFormat: "yy-mm-dd",
        onClose: function () {
            var dt1 = $('#datepicker2_from').datepicker('getDate');
            var dt2 = $('#datepicker2_to').datepicker('getDate');
            //check to prevent a user from entering a date below date of dt1
            if (dt2 <= dt1) {
               var minDate = $('#datepicker2_to').datepicker('option', 'minDate');
               $('#datepicker2_to').datepicker('setDate', minDate);
           }

            var tDate = $(this).val();
             var fDate=$("#datepicker2_from").val();
            //alert(tDate);
                     $.ajax({
                   type: "POST",
                   url: "<?php echo base_url() ?>home/purchaseList",
                   data: {date1: fDate,date2: tDate},
                   success: function (data) {

                       $("#purchase_data").html("");
                       $("#purchase_data").html(data);
                       $('#purchase_data').DataTable({ 
                            "destroy": true, //use for reinitialize datatable
                  });
           
                }
            });
        }
   });
   });

    
    



</script>

<?php $this->load->view('layout/footer.php') ?>
