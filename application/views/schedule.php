<?php $this->load->view('layout/header.php') ?>
<div class="page-heading default">
    <div class="container-inner">
        <div class="container">
            <div class="page-title">
                <div class="iw-heading-title">
                    <h1 style="color:#894724">
                        Schedule                            </h1>

                </div>
            </div>
        </div>
    </div>
   
</div>
<div class="contents-main" style="background-color: #f2f2f1;padding-top: 50px" id="contents-main">
    <div class="container">
        <div class="row">
            <a id="filter" class="btn btn-primary" style="background-color: rgb(48, 194, 189);display: none;height:35px; width:120px; margin-left:60px;" onclick="myFunction()" >Show Filter</a>
            <div class="col-md-3 iwj-sidebar-sticky iwj-sidebar-1" style="top:0px" id="filter_sidebar">
                <div class="widget-area sidebar-jobs-1">
                    <aside id="iwj_job_filter-2" class="widget sidebar-jobs widget_iwj_job_filter">


                        <div id="iwj-filter-selected" class="iwj-filter-selected" style="display: none;">
                            <h3 class="widget-title"><span>Your selected</span></h3>
                        </div>

                        <div id="iwj-clear-filter-btn" class="facet open-filters" style="display: none;">
                            <button class="btn btn-primary" id="clear-filter-job">
                                Clear Filter</button>
                        </div>


                        <aside class="widget sidebar-jobs-item">
                            <h3 class="widget-title"><span>Languages</span></h3>
                            <div class="job-categories sidebar-job-1">
                                <form name="iwjob-categories" id="iwjob-categories">
                                    <input type="hidden" name="limit" value="5" />
                                    <input type="hidden" name="limit_show_more" value="20" />
                                    <?php if($this->session->userdata('filterOpts')){ ?>
                                        <ul class="iwjob-list-categories tags">
                                        <li  class="theme-color-hover iwj-input-checkbox iwj_cat" >
                                            <input type="checkbox" <?php if (strpos($this->session->userdata('filterOpts'), 'malayalam') !== false) { echo "checked"; } ?>    name="iwj_cat" id="malayalam_chk" value="malayalam">
                                            <label for="malayalam_chk">
                                                Malayalam</label>
                                            
                                        </li>
                                        <li  class="theme-color-hover iwj-input-checkbox iwj_cat" data-order="4">
                                            <input type="checkbox" <?php if (strpos($this->session->userdata('filterOpts'), 'english') !== false) { echo "checked"; } ?> id="english_chk"  name="iwj_cat"

                                                   value="english" >
                                            <label for="english_chk">
                                                English</label>
                                            
                                        </li>
                                        <li  class="theme-color-hover iwj-input-checkbox iwj_cat" >
                                            <input type="checkbox"  <?php if (strpos($this->session->userdata('filterOpts'), 'hindi') !== false) { echo "checked"; } ?>  name="iwj_cat" id="hindi_chk" value="hindi">
                                            <label for="hindi_chk">
                                                Hindi</label>
                                            
                                        </li>
                                        <li  class="theme-color-hover iwj-input-checkbox iwj_cat" data-order="4">
                                            <input type="checkbox" <?php if (strpos($this->session->userdata('filterOpts'), 'tamil') !== false) { echo "checked"; } ?> id="tamil_chk"  name="iwj_cat"

                                                   value="tamil" >
                                            <label for="tamil_chk">
                                                Tamil</label>
                                            
                                        </li>
                                        <li  class="theme-color-hover iwj-input-checkbox iwj_cat" data-order="5">
                                            <input type="checkbox" <?php if (strpos($this->session->userdata('filterOpts'), 'urdu') !== false) { echo "checked"; } ?> id="urdu_chk"  name="iwj_cat" value="urdu" >
                                            <label for="urdu_chk">
                                                Urdu</label>
                                            
                                        </li>
                                        <li  class="theme-color-hover iwj-input-checkbox iwj_cat" data-order="6">
                                            <input type="checkbox" <?php if (strpos($this->session->userdata('filterOpts'), 'gujarati') !== false) { echo "checked"; } ?> id="gujarati_chk"  name="iwj_cat" value="gujarati" >
                                            <label for="gujarati_chk">
                                                Gujarati</label>
                                            
                                        </li>
                                        <li  class="theme-color-hover iwj-input-checkbox iwj_cat" data-order="7">
                                            <input type="checkbox" <?php if (strpos($this->session->userdata('filterOpts'), 'telugu') !== false) { echo "checked"; } ?> id="telugu_chk"  name="iwj_cat" value="telugu" >
                                            <label for="telugu_chk">
                                                Telugu</label>
                                            
                                        </li>


                                       </ul>
                               <?php     }else{ ?>
                                   
                                    <ul class="iwjob-list-categories tags">
                                        <li  class="theme-color-hover iwj-input-checkbox iwj_cat" >
                                            <input type="checkbox"   name="iwj_cat" id="malayalam_chk" value="malayalam">
                                            <label for="malayalam_chk">
                                                Malayalam</label>
                                            
                                        </li>
                                        <li  class="theme-color-hover iwj-input-checkbox iwj_cat" data-order="4">
                                            <input type="checkbox" id="english_chk"  name="iwj_cat"

                                                   value="english" >
                                            <label for="english_chk">
                                                English</label>
                                            
                                        </li>
                                        <li  class="theme-color-hover iwj-input-checkbox iwj_cat" >
                                            <input type="checkbox"  name="iwj_cat" id="hindi_chk" value="hindi">
                                            <label for="hindi_chk">
                                                Hindi</label>
                                            
                                        </li>
                                        <li  class="theme-color-hover iwj-input-checkbox iwj_cat" data-order="4">
                                            <input type="checkbox" id="tamil_chk"  name="iwj_cat"

                                                   value="tamil" >
                                            <label for="tamil_chk">
                                                Tamil</label>
                                            
                                        </li>
                                        <li  class="theme-color-hover iwj-input-checkbox iwj_cat" data-order="5">
                                            <input type="checkbox" id="urdu_chk"  name="iwj_cat"

                                                   value="urdu" >
                                            <label for="urdu_chk">
                                                Urdu</label>
                                            
                                        </li>
                                        <li  class="theme-color-hover iwj-input-checkbox iwj_cat" data-order="6">
                                            <input type="checkbox" id="gujarati_chk"  name="iwj_cat"

                                                   value="gujarati" >
                                            <label for="gujarati_chk">
                                                Gujarati</label>
                                            
                                        </li>
                                        <li  class="theme-color-hover iwj-input-checkbox iwj_cat" data-order="7">
                                            <input type="checkbox" id="telugu_chk"  name="iwj_cat"

                                                   value="telugu" >
                                            <label for="telugu_chk">
                                                Telugu</label>
                                            
                                        </li>

                                       </ul>
                          <?php     } ?>
                                   
                                </form>
                            </div>
                        </aside>


                      <aside class="widget sidebar-jobs-item">
                            <h3 class="widget-title"><span>Specialization</span></h3>
                            <div class="job-skills sidebar-job-1">
                                <form name="iwjob-skills" id="iwjob-skills">
                                    <input type="hidden" name="limit" value="5" />
                                    <input type="hidden" name="limit_show_more" value="30" />
                                    <?php if($this->session->userdata('filterOpts1')){ ?>
                                    <ul class="iwjob-list-skills">

                                        
                                        <?php 
                                        $i=1;
                                        foreach($types as $type){ ?> 
                                              <li  class="theme-color-hover iwj-input-checkbox iwj_skill" <?php if($i>=5){ echo "style='display:none'"; } ?> data-order="4">
                                            <input type="checkbox"  <?php if (strpos($this->session->userdata('filterOpts1'), $type->type) !== false) { echo "checked"; } ?> id="<?php echo $type->type ?>_chk"  name="iwj_skill"

                                                   value="<?php echo $type->type ?>" >
                                            <label for="<?php echo $type->type ?>_chk">
                                                <?php echo $type->type ?></label>
                                            
                                        </li>
                                           <?php $i++; } ?>
                                        
                                         <li class="show-more"><a class="theme-color" >Show more</a></li>
                                                   <li class="show-less"  style="display: none"><a class="theme-color">Show less</a></li>
                                      
                                    </ul>
                                    
                                    <?php }else{ ?>
                                        <ul class="iwjob-list-skills">
                                       <?php
                                       $i=1;
                                       foreach ($types as $type) { ?> 
                                               <li  class="theme-color-hover iwj-input-checkbox iwj_skill" <?php if($i>=5){ echo "style='display:none'"; } ?> data-order="<?=$i?>">
                                                   <input type="checkbox"  id="<?php echo $type->type ?>_chk"  name="iwj_skill"

                                                          value="<?php echo $type->type ?>" >
                                                   <label for="<?php echo $type->type ?>_chk">
                                                <?php echo $type->type ?></label>

                                               </li>
                                                <?php $i++; } ?>
                                               
                                               <li class="show-more"><a class="theme-color" >Show more</a></li>
                                                   <li class="show-less"  style="display: none"><a class="theme-color">Show less</a></li>
                                       </ul>
                                  <?php  } ?>
                                </form>
                            </div>
                        </aside>


                        <input type="hidden" value="job" name="wgit_job_filter" id="wgit-job-filter" />
                    </aside>


                </div>
            </div>
            <div class="col-md-6">
                <div class="iwj-content">

                    <article id="post-656" class="post-656 page type-page status-publish hentry">
                        <div class="entry-content">


                            <div class="vc_row wpb_row vc_row-fluid"><div class="wpb_column vc_column_container vc_col-sm-12"><div class="vc_column-inner"><div class="wpb_wrapper"><div class="iwj-content-inner">
                                                <div class="iwj-filter-form">
                                                    <div class="jobs-layout-form">
                                                        <form>
                                                                        <div class='col-sm-6'>
                                                                            <div class="form-group">
<!--                                                                                <div class='input-group date' id='datetimepicker1'>-->
                                                                                    <input type='text' class="form-control date" id='datetimepicker1' onkeydown="return false" placeholder="dd/mm/yyyy" />
<!--                                                                                    <span class="input-group-addon">
                                                                                        <span class="glyphicon glyphicon-calendar" style="color: #000"></span>
                                                                                    </span>
                                                                                </div>-->
                                                                            </div>
                                                                        </div>

                                                                        <div class="clear"></div>
                                                                    </form>
                                                        <a id="now_btn" class="job-alert-btn" style="background-color: rgb(48, 194, 189);margin-left: 15px" >Now</a>
                                                    </div>
                                                </div>

                                                <div id="iwajax-load">
                                                    <div class="iwj-jobs iwj-listing">
                                                        <div class="iwj-job-items" id="cn_content">


                                                            <?php
                                                            if (!empty($sort_data)) {
                                                             //   foreach ($sort_data as $value) {
                                                                
                                                                for($k=0;$k<count($sort_data);$k++){
                                                                    ?>
                                                                    <div class="grid-content"  data-id="1346">
                                                                        <div class="job-item featured-item">
                                                                            <div class="job-image"><img alt='AOEVN' src='<?php if($sort_data[$k]['photo']!="") {  echo $sort_data[$k]['photo']; }else { echo base_url()."uploads/images/user.jpg";  } ?>' class='avatar avatar-full photo' width='408' height='401' /></div>
                                                                            <div class="job-info">
                                                                                <h3 class="job-title"><a ><?php echo $sort_data[$k]['username'] ?></a></h3>
                                                                                <div class="info-company">
                                                                                    <div class="company"><i class="fa fa-suitcase"></i>
                                                                                        <a >
                                                                                           <?php
                                                                 $arr = json_decode($sort_data[$k]['domain']);
                                                                    if(!empty($arr)){
                                                                        echo implode(',', $arr);
                                                                    }else{
                                                                        echo $sort_data[$k]['domain'];
                                                                    }
                                                                
                                                                
                                                               
                                                                ?> 
                                                                                           
                                                                                    </div>
                                                                                    <div class="address"><i class="fa fa-cogs"></i><?php echo $sort_data[$k]['experience'] ?> years Exp</div>
                                                                                     <?php if($sort_data[$k]['video_call'] == 1){ ?><div class="video"><i class="fa fa-video-camera"></i>Video Call  Available</div><?php } ?>
                                                                                    <div class="sallary" style="display: inline-flex"><i class="fa fa-inr" style="margin-top: 8px;"></i>Rs <?= $sort_data[$k]['amount'] ?> for <?= $sort_data[$k]['minutes'] ?> min
                                                                                        <div class="help-tip"><p>Please note: 1st session will start with an assessment.</p></div>
                                                                                    </div>

                                                                                </div>
<!--                                                                                <span class="job-featured"><a class="type-name" onclick="set_user_session(<?=$value->id?>)" style="color:#fff">Details</a></span>-->
                                                                                <div class="job-type full-time">
                                                                                    <?php
                                                                                    if ($this->session->userdata('userData')) {

                                                                                        //onclick="user_schedule(<?php echo $value->id //\)"onclick="code_schedule(<?php echo $value->id )"
                                                                                        ?>
                                                                                        <a onclick="set_user_session(<?=$sort_data[$k]['id']?>)" class="type-name"  >Schedule</a>
                                                                                    <?php } else { ?>
                                                                                        <a onclick="set_user_session(<?=$sort_data[$k]['id']?>)" class="type-name" >Schedule</a>
        <?php } ?>
                                                                                </div>
                                                                            </div>
                                                                            <div class="job-featured"></div>
                                                                        </div>
                                                                    </div>
                                                                    <?php
                                                                }
                                                            } else {
                                                                ?>
                                                                <div class="alert alert-info">
                                                                    <strong>Info!</strong> No Counselors available.
                                                                </div>
                                                            <?php }
                                                            ?>
                                                            <div class="clearfix"></div>
                                                        </div>
                                                        <div class="w-pagination">
                                                                        
                                                            <div class="iwjob-ajax-pagination pagination-main">
                                                                <?php
                                                        echo "<br /><div class='pagination'>" . $this->pagination->create_links() . "</div>";
                                                        ?>
                                                            </div>
                                                        </div>

                                                       
                                                        
                                                    </div>
                                                </div>

                                                <input type="hidden" name="url" id="url" value="./jobs">
                                                <form name="iwjob-filter-url">

                                                </form>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            
                            
                        </div><!-- .entry-content -->
                        <div class="clearfix"></div>
                        <footer class="entry-footer">
                        </footer><!-- .entry-footer -->
                    </article><!-- #post-## -->
                </div>
            </div>
            <?php
            if ($this->session->userdata('loggedIn') == true) { 
            if ($data1[0]->role_id == 4) { ?>
            <div class="col-md-3 iwj-sidebar-sticky iwj-sidebar-2">
                <div class="widget-area sidebar-jobs-2">
                    <aside id="iwj_employer_contact_form-2" class="widget widget_iwj_employer_contact_form">
                        <div class="iwj-employer-widget-wrap">
                            <div class="iwj-single-contact-form iwj-single-widget">
                                <form class="iwj-contact-form" action="#" method="post" enctype="multipart/form-data">
                                    
                                    <div class="iwj-btn-action text-center">
                                        <?php
                                        if ($this->session->userdata('loggedIn') == true) {
                                        ?>
                                            <div class="iwj-button-loader">
                                                <button type="button" id="userbalcheck" data-toggle="modal"  style="background-color: rgb(50, 158, 210);"  class="iwj-btn iwj-btn-primary iwj-contact-btn">Balance Check</button>
                                            </div>
                                            
                                        <?php } else{ ?>
                                            
                                            <div class="iwj-button-loader">
                                                <button type="button" data-toggle="modal" data-target="#iwj-balance_check-popup" style="background-color: rgb(50, 158, 210);"  class="iwj-btn iwj-btn-primary iwj-contact-btn">Balance Check</button>
                                            </div>
                                        
                                        <?php }?>    
                                        <br>
                                        <div class="iwj-button-loader">
                                            <button data-toggle="modal" data-target="#iwj-reschedule-popup" style="padding-right:35px; padding-left: 40px;background-color: rgb(251, 135, 142);" type="button" id="reschedule_popup" class="iwj-btn iwj-btn-primary iwj-contact-btn">Reschedule</button>
                                        </div>
                                    </div>
                                
                                </form>
                            </div>
                        </div>

                    </aside>    

                </div>
            </div>
            <?php } } else {?>

            <div class="col-md-3 iwj-sidebar-sticky iwj-sidebar-2">
                <div class="widget-area sidebar-jobs-2">
                    <aside id="iwj_employer_contact_form-2" class="widget widget_iwj_employer_contact_form">
                        <div class="iwj-employer-widget-wrap">
                            <div class="iwj-single-contact-form iwj-single-widget">
                                <form class="iwj-contact-form" action="#" method="post" enctype="multipart/form-data">
                                    
                                    <div class="iwj-btn-action text-center">
                                        <?php
                                        if ($this->session->userdata('loggedIn') == true) {
                                        ?>
                                            <div class="iwj-button-loader">
                                                <button type="button" id="userbalcheck" data-toggle="modal"  style="background-color: rgb(50, 158, 210);"  class="iwj-btn iwj-btn-primary iwj-contact-btn">Balance Check</button>
                                            </div>
                                            
                                        <?php } else{ ?>
                                            
                                            <div class="iwj-button-loader">
                                                <button type="button" data-toggle="modal" data-target="#iwj-balance_check-popup" style="background-color: rgb(50, 158, 210);"  class="iwj-btn iwj-btn-primary iwj-contact-btn">Balance Check</button>
                                            </div>
                                        
                                        <?php }?>    
                                        <br>
                                        <div class="iwj-button-loader">
                                            <button data-toggle="modal" data-target="#iwj-reschedule-popup" style="padding-right:35px; padding-left: 40px;background-color: rgb(251, 135, 142);" type="button" class="iwj-btn iwj-btn-primary iwj-contact-btn">Reschedule</button>
                                        </div>
                                    </div>
                                
                                </form>
                            </div>
                        </div>

                    </aside>    

                </div>
            </div>

            <?php } ?>
        </div>
    </div>
</div>





<!-- #modal 2 -->

<div class="modal fade" id="modal-2">
    <div class="modal-dialog ">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">&times;</span><span class="sr-only">Close</span></button>
                <h4>Schedule Appointment</h4>
            </div>
            <div class="modal-body">

                <form class="form-horizontal" id="crud-form">
                    <input type="hidden" name="couns_id" class="user_id" id="couns_id">
                    <input type="hidden" name="sche_user"  id="sche_user">
                    <input type="hidden" name="sche_user" value="0"  id="sche_valid">
s<?php if ($this->session->userdata('loggedIn') == true) { ?>
                        <div class="form-group">
                            <label class="col-md-4 control-label" for="description">User</label>
                            <div class="col-md-5">
                                <input disabled="" class="form-control"  placeholder="Enter your name" value="" type="text" id="sche_username" name="username">
                            </div>
                        </div>
<?php } else { ?>
                        <div class="form-group">
                            <label class="col-md-4 control-label" for="description">User</label>
                            <div class="col-md-5">
                                <input class="form-control"  placeholder="Enter your code" value="" onchange="checkCodeValid(this.value)" type="text" id="sche_username" name="username">
                            </div>
                        </div>
<?php } ?>
                    <div class="form-group">
                        <label class="col-md-4 control-label" for="description">Note</label>
                        <div class="col-md-5">
                            <textarea class="form-control" cols="15" id="user_description" name="description"></textarea>
                        </div>
                    </div>

                    <div class="form-group">
                        <label class="col-md-4 control-label" for="title">Choose Date</label>
                        <div class="col-md-4">
                            <div class='input-group date datetimepicker_re'  id='datetimepicker_re'>
                                <input type='text' class="form-control" />
                                <span class="input-group-addon">
                                    <span class="glyphicon glyphicon-calendar"></span>
                                </span>
                            </div>
                        </div>
                        <span style="color:red;display: none;margin-left: 10px;font-weight: bold;">Counsellor not available on selected day!!</span>
                    </div>
                    <div class="form-group">
                        <label class="col-md-4 control-label" for="title">Counselor Availability</label>
                        <div class="col-md-4">
                            <select data-options="{&quot;allowClear&quot;:false,&quot;width&quot;:&quot;none&quot;,&quot;placeholder&quot;:&quot;&quot;,&quot;minimumResultsForSearch&quot;:-1}" required id="counselor_availabilty1" class="iwjmb-select_advanced counselor_availabilty" name="gencounselor_availabilityder">
                            </select>

                        </div>
                        <span style="color:red;display: none;margin-left: 20px;font-weight: bold;">This slot is already booked!!</span>
                    </div>   
                    <!--                                    <div class="form-group">
                                                            <label class="col-md-4 control-label" for="color">Color</label>
                                                            <div class="col-md-4">
                                                                <input id="color" name="color" type="text" class="form-control input-md" readonly="readonly" />
                                                                <span class="help-block">Click to pick a color</span>
                                                            </div>
                                                        </div>-->
                </form>
            </div>
            <div class="modal-footer">
                <button type="button" id="schedule-appointment" class="btn btn-success">Schedule</button>
                <button type="button" class="btn btn-default" data-dismiss="modal">Cancel</button>
            </div>
        </div>
    </div>
</div>
<script>
function myFunction() {
    var x = document.getElementById("filter_sidebar");

    if (x.style.display === "none") {
        x.style.display = "block";
    } else {
        x.style.display = "none";
    }
}
</script>
<script src='<?php echo base_url(); ?>js/jquery.timepicker.js'></script>

<script>
    function getTime1(time) {
        var time = time;
        var hours = Number(time.match(/^(\d+)/)[1]);
        var minutes = Number(time.match(/:(\d+)/)[1]);
        var AMPM = time.match(/\s(.*)$/)[1];
        if (AMPM == "PM" && hours < 12)
            hours = hours + 12;
        if (AMPM == "AM" && hours == 12)
            hours = hours - 12;
        var sHours = hours.toString();
        var sMinutes = minutes.toString();
        if (hours < 10)
            sHours = "0" + sHours;
        if (minutes < 10)
            sMinutes = "0" + sMinutes;
        var time = sHours + ":" + sMinutes + ":00";

        return  time;
    }
    function set_user_session(id){
       $.ajax({
            url: '<?php echo base_url(); ?>home/setUserId',
            data: {id: id},
            type: 'post',
            success: function (data) {
               window.location.href = '<?php echo base_url(); ?>home/employee_counselor';
            }
        });

    }
     var $checkboxes = $("input:checkbox");
    $checkboxes.on("change", function (e) {
        e.preventDefault();
        var opts = getPhoneFilterOptions();
        var arr = opts.split('-');

        updatePhones(arr[0], arr[1]);
    });
</script>
<?php $this->load->view('layout/footer.php') ?>