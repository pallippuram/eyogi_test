<?php $this->load->view('layout/header.php') ?>
<div class="page-heading default">
    <div class="container-inner">
        <div class="container">
            <div class="page-title">
                <div class="iw-heading-title">
                    <h1>
                        Blog                            </h1>

                </div>
            </div>
        </div>
    </div>
    
</div>
<div class="contents-main" style="background-color: #f2f2f1" id="contents-main">
    <div class="container" style="margin-top: 40px;margin-bottom: 40px">
                        <div class="row">
                            <div class="col-sm-12 col-xs-12 blog-content single-content">
                                <article id="post-66" class="post-66 post type-post status-publish format-image has-post-thumbnail hentry category-our-blog tag-blog tag-dean tag-employer tag-sales post_format-post-format-image">
                                    <div class="post-item fit-video">
                                        <div class="post-content">
                                            <div class="featured-image">
                                                <img width="1170" height="595" src="<?php echo base_url(); ?>uploads/2017/03/blog-image-1.jpg" class="attachment-post-thumbnail size-post-thumbnail wp-post-image" alt="" srcset="uploads/2017/03/blog-image-1.jpg 1170w,uploads/2017/03/blog-image-1-300x153.jpg 300w, uploads/2017/03/blog-image-1-768x391.jpg 768w, ./../../../../wp-content/uploads/2017/03/blog-image-1-1024x521.jpg 1024w" sizes="(max-width: 1170px) 100vw, 1170px" />                </div>
                                            <div class="post-main-content">
                                                <div class="post-meta">
                                                    <ul>
                                                        <li><i class="ion-android-folder-open"></i> <a  rel="category tag">Our Blog</a></li>
                                                        <li><i class="ion-android-calendar"></i> March 29, 2017</li>
                                                    </ul>
                                                    <span class="feature-post">Sticky post</span>                </div>
                                                <div class="post-content-desc">
                                                    <div class="post-text">
                                                        <p>Think back over your life. Think about the people that had a positive influence on you. If your past was like mine, many of them didn’t realize the impact they made. The influence was usually due to them caring about you and doing some little thing.</p>
                                                        <p><strong><em>&#8220;You might remember the Dell computer commercials in which a youth reports this exciting news to his friends that youth reports this exciting news to his friends that&#8221;</em></strong></p>
                                                        <p><img class="alignleft wp-image-1802 size-medium" src="./../../../../wp-content/uploads/2017/03/post-image-10-300x249.jpg" alt="" width="300" height="249" srcset="./../../../../wp-content/uploads/2017/03/post-image-10-300x249.jpg 300w, ./../../../../wp-content/uploads/2017/03/post-image-10.jpg 423w" sizes="(max-width: 300px) 100vw, 300px" /></p>
                                                        <p>I was born and raised in the heart of the Deep South.<em><strong> I have moved on from my roots</strong></em> and have made my way into other regions of the world.</p>
                                                        <p>One thing, however, that has remained constant in my life is my deep and abiding love for southern cooking and cuisine.</p>
                                                    </div>
                                                    <div class="clearfix"></div>
                                                </div>

                                                <div class="post-tags"><span class="tag-title"><i class="ion-ios-pricetags"></i> Tags:</span><a  rel="tag">Blog</a><a  rel="tag">Dean</a><a  rel="tag">Employer</a><a  rel="tag">Sales</a></div>
                                                <div class="post-bar clearfix">
                                                    <div class="post-bar-left">
                                                        <div class="next-post">
                                                            <span class="next-post-title">Next Article : </span>
                                                            <a  rel="next">Join the world’s largest developer community</a>                            </div>
                                                    </div>
                                                    <div class="post-bar-right">
                                                        <div class="post-social-share">
                                                            <span class="share-title">Share : </span>
                                                            <a class="social-share-item share-buttons-fb" target="_blank" href="https://www.facebook.com/sharer.php?s=100&#038;t=Top+fun+activities+tips+for+you&#038;u=.%2F2017%2F03%2F29%2Ftop-fun-activities-tips-for-you%2F&#038;picture=./wp-content/uploads/2017/03/blog-image-1-1024x521.jpg" title="Share on Facebook" onclick="return InwaveOpenWindow(this.href);"><i class="fa fa-facebook"></i></a><a class="social-share-item share-buttons-tt" target="_blank" href="https://twitter.com/share?url=.%2F2017%2F03%2F29%2Ftop-fun-activities-tips-for-you%2F&#038;text=" title="Share on Twitter" onclick="return InwaveOpenWindow(this.href);"><i class="fa fa-twitter"></i></a><a class="social-share-item share-buttons-linkedin" target="_blank" href="https://www.linkedin.com/shareArticle?mini=true&#038;url=.%2F2017%2F03%2F29%2Ftop-fun-activities-tips-for-you%2F&#038;title=Top+fun+activities+tips+for+you&#038;summary=" title="Share on Linkedin" onclick="return InwaveOpenWindow(this.href);"><i class="fa fa-linkedin"></i></a>                            </div>
                                                    </div>
                                                </div>
                                            </div>

                                            <div id="comments" class="comments">
                                                <div class="comments-content">

                                                    <div class="commentList">
                                                        <h3 class="comments-title">
                                                            <span>5</span> Comments                </h3>
                                                        <ul class="comment_list">
                                                            <li class="comment even thread-even depth-1" id="li-comment-9">
                                                                <div id="comment-9" class="comment answer">
                                                                    <div class="commentAvt commentLeft">
                                                                        <img alt='' src='http://2.gravatar.com/avatar/e465cb0c76ea50de6837055fb7d2a153?s=91&#038;d=mm&#038;r=g' srcset='http://2.gravatar.com/avatar/e465cb0c76ea50de6837055fb7d2a153?s=182&amp;d=mm&amp;r=g 2x' class='avatar avatar-91 photo' height='91' width='91' />                    </div>
                                                                    <!-- .comment-meta -->
                                                                    <div class="commentRight">
                                                                        <div class="content-cmt">
                                                                            <div class="comment-head-info">
                                                                                <span class="name-cmt">admin</span>
                                                                                <span class="date-cmt"> 6 months ago</span>
                                                                                <div class="content-reply">
                                                                                    <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat.</p>

                                                                                </div>
                                                                                <div class="comment_reply"><a rel='nofollow' class='comment-reply-link' onclick='return addComment.moveForm("comment-9", "9", "respond", "66")' aria-label='Reply to admin'><i class="ion-reply"></i> Reply</a></div>

                                                                            </div>
                                                                        </div>
                                                                    </div>
                                                                    <!-- .comment-content -->

                                                                    <div class="clearfix"></div>
                                                                </div>
                                                                <!-- #comment-## -->
                                                                <ul class="children">
                                                                    <li class="comment odd alt depth-2" id="li-comment-11">
                                                                        <div id="comment-11" class="comment answer">
                                                                            <div class="commentAvt commentLeft">
                                                                                <img alt='' src='http://2.gravatar.com/avatar/e465cb0c76ea50de6837055fb7d2a153?s=91&#038;d=mm&#038;r=g' srcset='http://2.gravatar.com/avatar/e465cb0c76ea50de6837055fb7d2a153?s=182&amp;d=mm&amp;r=g 2x' class='avatar avatar-91 photo' height='91' width='91' />                    </div>
                                                                            <!-- .comment-meta -->
                                                                            <div class="commentRight">
                                                                                <div class="content-cmt">
                                                                                    <div class="comment-head-info">
                                                                                        <span class="name-cmt">admin</span>
                                                                                        <span class="date-cmt"> 6 months ago</span>
                                                                                        <div class="content-reply">
                                                                                            <p>Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur.</p>

                                                                                        </div>
                                                                                        <div class="comment_reply"><a rel='nofollow' class='comment-reply-link'  onclick='return addComment.moveForm("comment-11", "11", "respond", "66")' aria-label='Reply to admin'><i class="ion-reply"></i> Reply</a></div>

                                                                                    </div>
                                                                                </div>
                                                                            </div>
                                                                            <!-- .comment-content -->

                                                                            <div class="clearfix"></div>
                                                                        </div>
                                                                        <!-- #comment-## -->
                                                                        <ul class="children">
                                                                            <li class="comment even depth-3" id="li-comment-12">
                                                                                <div id="comment-12" class="comment answer">
                                                                                    <div class="commentAvt commentLeft">
                                                                                        <img alt='' src='http://2.gravatar.com/avatar/e6d67fed862c439aa6e911ce49c7857d?s=91&#038;d=mm&#038;r=g' srcset='http://2.gravatar.com/avatar/e6d67fed862c439aa6e911ce49c7857d?s=182&amp;d=mm&amp;r=g 2x' class='avatar avatar-91 photo' height='91' width='91' />                    </div>
                                                                                    <!-- .comment-meta -->
                                                                                    <div class="commentRight">
                                                                                        <div class="content-cmt">
                                                                                            <div class="comment-head-info">
                                                                                                <span class="name-cmt"><a href='https://codecanyon.net/' rel='external nofollow' class='url'>3docean</a></span>
                                                                                                <span class="date-cmt"> 2 months ago</span>
                                                                                                <div class="content-reply">
                                                                                                    <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua.</p>

                                                                                                </div>
                                                                                                <div class="comment_reply"><a rel='nofollow' class='comment-reply-link'  onclick='return addComment.moveForm("comment-12", "12", "respond", "66")' aria-label='Reply to 3docean'><i class="ion-reply"></i> Reply</a></div>

                                                                                            </div>
                                                                                        </div>
                                                                                    </div>
                                                                                    <!-- .comment-content -->

                                                                                    <div class="clearfix"></div>
                                                                                </div>
                                                                                <!-- #comment-## -->
                                                                            </li><!-- #comment-## -->
                                                                        </ul><!-- .children -->
                                                                    </li><!-- #comment-## -->
                                                                    <li class="comment odd alt depth-2" id="li-comment-13">
                                                                        <div id="comment-13" class="comment answer">
                                                                            <div class="commentAvt commentLeft">
                                                                                <img alt='' src='http://2.gravatar.com/avatar/e6d67fed862c439aa6e911ce49c7857d?s=91&#038;d=mm&#038;r=g' srcset='http://2.gravatar.com/avatar/e6d67fed862c439aa6e911ce49c7857d?s=182&amp;d=mm&amp;r=g 2x' class='avatar avatar-91 photo' height='91' width='91' />                    </div>
                                                                            <!-- .comment-meta -->
                                                                            <div class="commentRight">
                                                                                <div class="content-cmt">
                                                                                    <div class="comment-head-info">
                                                                                        <span class="name-cmt"><a href='https://codecanyon.net/' rel='external nofollow' class='url'>3docean</a></span>
                                                                                        <span class="date-cmt"> 2 months ago</span>
                                                                                        <div class="content-reply">
                                                                                            <p>Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat.</p>

                                                                                        </div>
                                                                                        <div class="comment_reply"><a rel='nofollow' class='comment-reply-link'  onclick='return addComment.moveForm("comment-13", "13", "respond", "66")' aria-label='Reply to 3docean'><i class="ion-reply"></i> Reply</a></div>

                                                                                    </div>
                                                                                </div>
                                                                            </div>
                                                                            <!-- .comment-content -->

                                                                            <div class="clearfix"></div>
                                                                        </div>
                                                                        <!-- #comment-## -->
                                                                    </li><!-- #comment-## -->
                                                                </ul><!-- .children -->
                                                            </li><!-- #comment-## -->
                                                            <li class="comment even thread-odd thread-alt depth-1" id="li-comment-10">
                                                                <div id="comment-10" class="comment answer">
                                                                    <div class="commentAvt commentLeft">
                                                                        <img alt='' src='http://2.gravatar.com/avatar/e465cb0c76ea50de6837055fb7d2a153?s=91&#038;d=mm&#038;r=g' srcset='http://2.gravatar.com/avatar/e465cb0c76ea50de6837055fb7d2a153?s=182&amp;d=mm&amp;r=g 2x' class='avatar avatar-91 photo' height='91' width='91' />                    </div>
                                                                    <!-- .comment-meta -->
                                                                    <div class="commentRight">
                                                                        <div class="content-cmt">
                                                                            <div class="comment-head-info">
                                                                                <span class="name-cmt">admin</span>
                                                                                <span class="date-cmt"> 6 months ago</span>
                                                                                <div class="content-reply">
                                                                                    <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua</p>

                                                                                </div>
                                                                                <div class="comment_reply"><a rel='nofollow' class='comment-reply-link'  onclick='return addComment.moveForm("comment-10", "10", "respond", "66")' aria-label='Reply to admin'><i class="ion-reply"></i> Reply</a></div>

                                                                            </div>
                                                                        </div>
                                                                    </div>
                                                                    <!-- .comment-content -->

                                                                    <div class="clearfix"></div>
                                                                </div>
                                                                <!-- #comment-## -->
                                                            </li><!-- #comment-## -->
                                                        </ul>

                                                    </div>
                                                    <div class="form-comment">

                                                        <div id="respond" class="comment-respond">
                                                            <h3 id="reply-title" class="comment-reply-title">Leave a Reply <small><a rel="nofollow" id="cancel-comment-reply-link"  style="display:none;">Cancel reply</a></small></h3>			<form action="./../../../../wp-comments-post.php" method="post" id="commentform" class="comment-form" novalidate>
                                                                <div class="row"><div class="col-md-4 col-sm-12 col-xs-12 commentFormField"><input id="author" class="input-text" name="author" placeholder="Name *" type="text" value="" size="30" /></div>
                                                                    <div class="col-md-4 col-sm-12 col-xs-12 commentFormField"><input id="email" class="input-text" name="email" placeholder="Email address *" type="email" value="" size="30" /></div>
                                                                    <div class="col-md-4 col-sm-12 col-xs-12 commentFormField"><input id="url" class="input-text" name="url" placeholder="Website" type="url" value="" size="30" /></div></div>
                                                                <div class="row"><div class="col-xs-12 commentFormField"><textarea id="comment" class="control" placeholder="Your comment *" name="comment" cols="45" rows="4" aria-required="true"></textarea></div></div>
                                                                <p class="form-submit"><input name="submit" type="submit" id="submit" class="btn-submit button" value="Post Comment" /> <input type='hidden' name='comment_post_ID' value='66' id='comment_post_ID' />
                                                                    <input type='hidden' name='comment_parent' id='comment_parent' value='0' />
                                                                </p>			</form>
                                                        </div><!-- #respond -->
                                                    </div>
                                                </div>
                                                <!-- #comments -->
                                            </div><!-- #comments -->
                                        </div>
                                    </div>
                                </article><!-- #post-## -->                                            </div>
                        </div>
                    </div>
</div>

<?php $this->load->view('layout/footer.php') ?>