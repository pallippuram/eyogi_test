<?php $this->load->view('layout/header.php') ?>
<div class="page-heading default">
                <div class="container-inner">
                    <div class="container">
                        <div class="page-title">
                            <div class="iw-heading-title">
                                <h1>
                                    Register                            </h1>

                            </div>
                        </div>
                    </div>
                </div>
               
            </div>
            <div class="contents-main" style="background-color: #f2f2f1" id="contents-main">
                <div class="container">
                    <div class="row">
                        <div class="col-sm-12 col-xs-12">

                            <article id="post-143" class="post-143 page type-page status-publish hentry">
                                <div class="entry-content">

                                    <div class="sbn-list" >
                                        <div class="sbn sbn01 sbn-new" style="margin:0 auto;padding-top: 30px;">

                                            <ul class="tabs">
                                                <li id="1"><a class="is-active" href="#">1</a></li>
                                                <li id="2"><a  href="#">2</a></li>
                                                <li id="3"><a href="#">3</a></li>
                                                <li id="4"><a href="#">4</a></li>
                                                <li id="5"> <a href="#">5</a></li>
                                            </ul>
                                        </div>
                                    </div>



                                    <div class="iwj-login" style="padding-top:20px;padding-bottom: 100px;height:auto;">
                                        <form  action="<?php echo site_url('user/register'); ?>" method="post" style="height:auto" class="iwj-form iwj-login-form1">
                                            
                                                <?php if($msg!=""){ ?>
                                                   <div id="error_msg_login" class="alert alert-danger">User already registerd with this email id!!! Please <a onclick="show_popup()"> Login</a> </div>
                                              <?php  } ?>
                                                
                                            
                                            <div class="reg_1" >
                                                <input type="hidden" value="<?php if(isset($oauth_uid)){ echo $oauth_uid;} ?>" name="oauth_uid">
                                                <div class="iwj-respon-msg hide"></div>
                                                <div class="iwj-field">
                                                    <label>User Name</label>
                                                    <div class="iwj-input">
                                                        <i class="fa fa-user"></i>
                                                        <input type="text" value="<?php if(isset($first_name)){ echo $first_name;} ?>" name="username" id="reg_username" placeholder="Enter Your Name.">
                                                    </div>
                                                </div>
                                                <div class="iwj-field">
                                                    <label>Email</label>
                                                    <div class="iwj-input">
                                                        <i class="fa fa-envelope"></i>
                                                        <input type="email" value="<?php if(isset($email)){ echo $email;} ?>" name="email" id="reg_email"  placeholder="Enter Your Email.">
                                                    </div>
                                                </div>
                                                <div class="iwj-field">
                                                    <label>Password</label>
                                                    <div class="iwj-input">
                                                        <i class="fa fa-keyboard-o"></i>
                                                        <input type="password" name="password" id="reg_password" onclick="showmsg()" placeholder="Enter Password.">
                                                    </div>
                                                </div>
                                                <span id="msg_submit" style="color:red;display: none;font-weight: bold;"></span>
                                                <div class="iwj-field">
                                                    <label>Confirm Password</label>
                                                    <div class="iwj-input">
                                                        <i class="fa fa-keyboard-o"></i>
                                                        <input type="password" name="conpassword" id="reg_conpassword" onclick="check_passwrd(document.getElementById('reg_password').value)" placeholder="Confirm Password.">
                                                    </div>
                                                </div>
                                                <div class="iwj-button-loader">
                                                    <button id="reg_btn_1" type="button" name="login" class="iwj-btn iwj-btn-primary iwj-btn-full iwj-btn-large iwj-login-btn">NEXT</button>
                                                </div>

                                                <div class="iwj-divider">
                                                    <span class="line"></span>
                                                    <span class="circle">Or</span>
                                                </div>
                                                <div class="text-center register-account">
                                                    Register Using
                                                </div>
                                                <div class="social-login row">
                                                    <div class="col-md-4">
                                                        <a href="<?php echo $authUrl ?>" class="iwj-btn iwj-btn-primary iwj-btn-medium iwj-btn-full iwj-btn-icon social-login-facebook"><i class="fa fa-facebook"></i>Facebook</a>
                                                    </div>
                                                    <div class="col-md-4">
                                                        <a href="<?php echo $loginURL; ?>" class="iwj-btn iwj-btn-primary iwj-btn-medium iwj-btn-full iwj-btn-icon social-login-google"><i class="fa fa-google-plus"></i>Google</a>
                                                    </div>
                                                    <div class="col-md-4">
                                                        <a href="<?php echo $oauthURL; ?>" class="iwj-btn iwj-btn-primary iwj-btn-medium iwj-btn-full iwj-btn-icon social-login-linkedin"><i class="fa fa-linkedin"></i>Linkedin</a>
                                                    </div>
                                                </div>     

                                                <div class="text-center register-account">
                                                    Already a Registered User? <a onclick="show_popup()">Login</a>
                                                </div>
                                                <input type="hidden" name="redirect_to" value="/user/profile">
                                            </div>
                                            <div class="reg_2" style="display:none;">
                                                <div style="display:inline-flex; width:100%" id="mobile_otp">
                                                    <div class="iwj-field col-md-8">

                                                        <label>Mobile Number</label>
                                                        <div class="iwj-input">
                                                            <i class="fa fa-user"></i>
                                                            <input type="text" id="reg_mobile_number" name="phone" placeholder="Enter Mobile Number">
                                                        </div>
                                                    </div>
                                                    <div class="col-md-4">
                                                        <button type="button" onclick="generate_code()" name="login" class="iwj-btn iwj-btn-primary iwj-btn-full iwj-btn-large iwj-login-btn">Send OTP</button>
                                                    </div>
                                                </div>
                                                <span  style="display: none;padding: 3px !important;" class="alert alert-info generate_otp_span">OTP has been sent to your mobile number.. Click <a onclick="generate_code()">here</a> to resend OTP</span>
                                                
                                                <div class="iwj-field" style="margin-top: 30px">
                                                    <label>One time password</label>
                                                    <div class="iwj-input">
                                                        <i class="fa fa-keyboard-o"></i>
                                                        <input type="text" id="reg_mobile_otp" name="otp" placeholder="Enter OTP received">
                                                    </div>
                                                </div>
                                                <div class="iwj-button-loader">
                                                    <button  id="reg_btn_2" type="button" name="login" class="iwj-btn iwj-btn-primary iwj-btn-full iwj-btn-large iwj-login-btn">SAVE</button>
                                                </div>
                                            </div>

                                            <div class="reg_3" style="display:none;">
                                                <div class="iwj-field">
                                                    <label>Age</label>
                                                    <div class="iwj-input">
                                                        <i class="fa fa-user"></i>
                                                        <input type="text"  email name="age" placeholder="Enter Age">
                                                    </div>
                                                </div>
                                                <div class="iwj-field">
                                                    <label>Gender</label>

                                                    <input type="radio" <?php if(isset($gender) && $gender=='male'){ echo 'checked';} ?> value="0" id="test1" name="gender">
                                                    <label style="display:inline; color: rgb(148, 150, 153)" for="test1">Male</label>

                                                    <input type="radio" <?php if(isset($gender) && $gender=='female'){ echo 'checked';} ?> value="1" id="test2" name="gender">
                                                    <label  style="display:inline; color: rgb(148, 150, 153)" for="test2">Female</label>

                                                    <input type="radio" <?php if(isset($gender) && $gender=='others'){ echo 'checked';} ?> value="2" id="test3" name="gender">
                                                    <label  style="display:inline; color: rgb(148, 150, 153)" for="test3">Other</label>

                                                </div>
                                                <div class="iwj-button-loader">
                                                    <button  id="reg_btn_3" type="button"  name="login" class="iwj-btn iwj-btn-primary iwj-btn-full iwj-btn-large iwj-login-btn">NEXT</button>
                                                </div>
                                            </div>

                                            <div class="reg_4" style="display:none;">
                                                <div class="iwjmb-field iwjmb-select_advanced-wrapper  required">
                                                    <div class="iwjmb-label">
                                                        <label style="color: #ff8e16;" for="frequency">Educational Qualification</label>
                                                    </div>
                                                    <div class="iwjmb-input">
                                                        <select data-options="{&quot;allowClear&quot;:false,&quot;width&quot;:&quot;none&quot;,&quot;placeholder&quot;:&quot;&quot;,&quot;minimumResultsForSearch&quot;:-1}" id="qualification" class="iwjmb-select_advanced" name="qualification">
                                                            <option value=""  selected='selected'>Select Qualification</option>
                                                            <option value="Under Graduate">Under Graduate</option>
                                                            <option value="Post Graduate" >Post Graduate</option>
                                                        </select>
                                                    </div>
                                                </div>   
                                                
                                                <div class="iwjmb-field iwjmb-select_advanced-wrapper  required">
                                                    <div class="iwjmb-label">
                                                        <label style="color: #ff8e16;" for="frequency">Employed</label>
                                                    </div>
                                                    <div class="iwjmb-input">
                                                        <select data-options="{&quot;allowClear&quot;:false,&quot;width&quot;:&quot;none&quot;,&quot;placeholder&quot;:&quot;&quot;,&quot;minimumResultsForSearch&quot;:-1}" id="employed" class="iwjmb-select_advanced" name="employed">
                                                            <option value=""  selected='selected'>None</option>
                                                            <option value="0">Yes</option>
                                                            <option value="1" >No</option>
                                                        </select>
                                                    </div>
                                                </div> 
                                                <div class="text-center"><a onclick="skip_this_step()">Skip this step</a></div>
                                                <br>
                                                <div class="iwj-button-loader">
                                                    <button id="reg_btn_4" type="button" name="login" class="iwj-btn iwj-btn-primary iwj-btn-full iwj-btn-large iwj-login-btn">NEXT</button>
                                                </div>
                                            </div>
                                            
                                            <div class="reg_5" style="display:none;">
                                                <div class="iwjmb-field iwjmb-select_advanced-wrapper  required">
                                                    <div class="iwjmb-label">
                                                        <label style="color: #ff8e16;" for="frequency">Marital Status</label>
                                                    </div>
                                                    <div class="iwjmb-input">
                                                        <select data-options="{&quot;allowClear&quot;:false,&quot;width&quot;:&quot;none&quot;,&quot;placeholder&quot;:&quot;&quot;,&quot;minimumResultsForSearch&quot;:-1}" required id="frequency" class="iwjmb-select_advanced" name="marital_status">
                                                            <option value="0"  selected='selected'>Single</option>
                                                            <option value="1">Married</option>
                                                            
                                                        </select>
                                                    </div>
                                                </div>   
                                                <input type="hidden" name="role_id" value="4">
                                                <div class="iwj-field">
                                                    <label>Kids</label>

                                                    <input type="radio" value="1" id="test_1" name="gender">
                                                    <label style="display:inline; color: rgb(148, 150, 153)" for="test_1">Yes</label>

                                                    <input type="radio"  value="0" id="test_2" name="gender">
                                                    <label  style="display:inline; color: rgb(148, 150, 153)" for="test_2">No</label>

                                                    

                                                </div>
                                                <br>
                                                <div class="iwj-button-loader">
                                                    <input type="submit" id="submit_btn" style="padding: 15px 25px"  name="login" value="Submit" class="iwj-btn iwj-btn-primary iwj-btn-full iwj-btn-large iwj-login-btn">
                                                </div>
                                            </div>
                                        </form>
                                    </div>

                                </div><!-- .entry-content -->
                                <div class="clearfix"></div>
                                <footer class="entry-footer">
                                </footer><!-- .entry-footer -->
                            </article><!-- #post-## -->
                        </div>
                    </div>
                </div>
            </div>
<script>
    function skip_this_step(){
        $('#reg_btn_4').click();
    }
    
</script>
<?php $this->load->view('layout/footer.php') ?>