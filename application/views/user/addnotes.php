<?php  $this->load->view('layout/header.php') ?>

<div class="page-heading default">
    <div class="container-inner">
        <div class="container">
            <div class="page-title">
                <div class="iw-heading-title">
                    <h1 style="color:#894724">
                       Enter your notes                           </h1>

                </div>
            </div>
        </div>
    </div>
   
        </div>
            <div class="contents-main" style="background-color: #f2f2f1" id="contents-main">
                <div class="container">
                    <div class="row">
                        <div class="col-sm-12 col-xs-12">
                       <form action="<?php echo site_url('Counselor/counselor_insertnotes'); ?>" method="post" class="iwj-login-form1 iwj-form" name="notes11" id="notes11" style="padding-top:20px" novalidate="novalidate"> 
                           
                                                                        
                                <div class="" style="box-shadow: none;">
                                    <div class="title">
                                        <h3></h3>
                                    </div>
                                    <input type="hidden" name="baseurl22" id="baseurl22" value="<?php echo base_url(); ?>">
                                 

                                   
                                    
                                <div class="form-group">
								    <label for="pwd">Name:</label>
								    <input type="text" class="form-control" id="name" name="name" value="<?php echo $user->username;?>" readonly="">
								 </div>

								    <input type="hidden" name="user_id1" id="user_id1" value="<?php echo $details->sid; ?>">

								
							

								       <div class="iwjmb-field iwjmb-text-wrapper  required"><div class="iwjmb-label"><label class="theme-color" for="_iwj_headline">Issues *</label></div>
                                                        <div class="iwjmb-input ui-sortable">
<select multiple data-options="{&quot;allowClear&quot;:false,&quot;width&quot;:&quot;none&quot;,&quot;placeholder&quot;:&quot;&quot;,&quot;minimumResultsForSearch&quot;:-1}" required  class="iwjmb-select_advanced" name="domain[]" id="domain">
                                                                
                                                               <?php  foreach ($counselling_types as $type) { ?>
                                                                <option value="<?=$type->type?>"><?=$type->type?></option>
                                                               <?php } ?>
                                                            </select>
                                                           
                                                        </div>
                                                    </div> 

								 <div class="form-group">
								    <label for="pwd">Notes:</label>
								    
								    <textarea class="form-control" name="notes" id="notes"></textarea>
								 </div>

                                  
                          
                                    <!--<div class="submit-button"><input type="submit" value="Send" class="wpcf7-form-control wpcf7-submit" style="color:#ff9128" /></div>-->
                                    <div class="submit-button"><input type="submit" value="Send" class="iwj-btn pull-left edit_btn iwj-btn-primary iwj-candidate-btn"/></div>
                                </div>
                                <div id="succes"  class="alert alert-success" style="width: 50%; margin-left: 49%;display: none;">  </div>  
                                <div id="note_submit" class="alert alert-success" style="width: 50%; margin-left: 49%;display: none;"></div>
                                                                        
                            </form>
<!--                <script id="bx24_form_inline" data-skip-moving="true">
                    (function (w, d, u, b) {
                        w['Bitrix24FormObject'] = b;
                        w[b] = w[b] || function () {
                            arguments[0].ref = u;
                            (w[b].forms = w[b].forms || []).push(arguments[0])
                        };
                        if (w[b]['forms'])
                            return;
                        var s = d.createElement('script');
                        s.async = 1;
                        s.src = u + '?' + (1 * new Date());
                        var h = d.getElementsByTagName('script')[0];
                        h.parentNode.insertBefore(s, h);
                    })(window, document, 'https://eyogi.bitrix24.in/bitrix/js/crm/form_loader.js', 'b24form');

                    b24form({"id": "10", "lang": "en", "sec": "9nozcu", "type": "inline"});
                </script>-->
            </div>
        </div>
    </div>
</div>

<br>
<style type="text/css">
table {
    font-family: arial, sans-serif;
    border-collapse: collapse;
    width: 100%;
}

td, th {
    border: 1px solid #dddddd;
    text-align: left;
    padding: 8px;
}

tr:nth-child(even) {
    background-color: #dddddd;
}
    </style>

    <div class="contents-main col-md-12 iw-job-content iw-job-detail" style="background: #f2f2f1;padding-top: 50px;padding-bottom: 50px;" id="contents-main">
        <div class="container purchase" style="">
            
            <div class="row">
                <div class="col-sm-12 col-xs-12 col-md-12">
                    <div class="job-detail">
                        
                        <div class="job-detail-content" style="padding: 30px 20px;">
                            
                   <table class="table userlist cell-border" cellspacing="0" id="d1">

                   	            <thead>
                                        <tr>
                                     
                                            
                                           
                                            <th width="">Name</th>
                                            <th width="">Issues</th>
                                            <th width="">Notes</th>
                                            
                                        </tr>
                                    </thead>

                                      <tbody>
                                      	<?php  foreach ($notes as $note) { ?>
                                      	<tr>
                                      		
                                      		<td>
                                      			<?php echo $note->name; ?>
                                      		</td>
                                      		<td>
                                      			<?php
                                                                $arr = json_decode($note->issues);
                                                                if(!empty($arr)){
                                                                     echo implode(',', $arr);
                                                                }else{
                                                                    echo $note->issues;
                                                                }   
                                                                ?> 
                                      		</td>
                                      		<td>
                                      			<?php echo $note->notes; ?>
                                      		</td>
                                      		
                                      	</tr>
                                      	<?php } ?>
                                      </tbody>

                   </table>
                </div>

                    </div>
                </div>

            </div>
        </div>
    </div>
<?php $this->load->view('layout/footer.php') ?>