<?php  $this->load->view('layout/user_header.php') ?>
<div class="contents-main" id="contents-main" style="margin-top:3%">

    <article id="post-141" class="post-141 page type-page status-publish hentry">
        <div class="entry-content">
            <div class="iwj-dashboard clearfix">

                <div class="iwj-dashboard-menu-mobile">
                    <div class="dropdown">
                        <button class="btn btn-primary dropdown-toggle"  type="button" data-toggle="dropdown">Menu Dashboard <span class="caret"></span></button>


                        <?php $this->load->view('layout/menu.php') ?>        

                    </div>
                </div>


                <div class="iwj-dashboard-main save-jobs">
                    <div class="iwj-dashboard-main-inner">

                        <div class="iwj-save-jobs iwj-main-block">
                            <form method="post"  class="iwj-form-2 iwj-login-form1">

                                <div class="info-top" style="padding-bottom:0">
                                    <h3 class=""><?php echo "Upcoming Appointments" ?></h3>

                                </div>

                            </form>
                            <div class="iwj-table-overflow-x">
                                <table class="table userlist" cellspacing="0" width="100%">
                                    <thead>
                                        <tr>
                                            <th width="">ID</th>
                                            <?php
                                            $data = $this->session->userdata('userData');
                                            if($data->role_id==3){ ?>
                                                 <th width="">User</th>
                                        <?php    }else { ?>
                                                 <th width="">Counselor</th>
                                         <?php   }
                                            ?>
                                            
                                           
                                            <th width="">Appointment Date</th>
                                            <th width="">Time slot</th>
                                            <th width="">Note</th>
                                            <th width="">Status</th>
                                            <th width="" class="text-center">Action</th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                        <?php
                                        $i = 1;
                                        foreach ($data2 as $user) {
                                            if (!empty($user)) {

                                                
                                                if (strtotime($user->date) > strtotime(date('Y-m-d'))) {
                                                    //echo $user->date."-".date('Y-m-d');
                                                    ?>
                                                    <tr id="save-job-1212" class="save-job-item">
                                                        <td>
                                                            <?= $i ?>
                                                        </td>
                                                        <td>
                                                            <?php
                                                           
                                                            if (is_numeric($user->username)&&strlen((string)$user->username)<6 || strpos($user->username, '@') !== false) {
                                                                 echo "Anonymous user";
                                                            } else {
                                                                
                                                               echo $user->username;
                                                            }
                                                            ?>
                                                        </td>
                                                        <td>
                                                            <?= $user->date ?>
                                                        </td>
                                                        <td> 
                                                         <?=   date('h:i A', strtotime($user->time_starts) ). " - " .date('h:i A', strtotime( $user->time_ends) ) ?>
                                                        </td>
                                                        <td>
                                                            <?= $user->note ?>
                                                        </td>
                                                        <td> 
                                                            <?php if ($user->is_modified == 0) { ?>Schedule
                                                            <?php } else { ?>Reschedule
                                                            <?php } ?>
                                                        </td>
                                                        <td class="text-center">
                                                            <a style="padding-right: 20px" onclick="appDetails(<?= $user->sid ?>, '<?= $user->date ?>')"><i class="fa fa-eye" title="View Details" aria-hidden="true"></i></a>
                                                        </td>
                                                    </tr>
                                                    <?php
                                                    $i++;
                                                } else if (strtotime($user->date) == strtotime(date('Y-m-d'))&&strtotime($user->time_starts) > strtotime(date("h:i A"))) {
                                                   
                                                        ?>
                                                        <tr id="save-job-1212" class="save-job-item">
                                                            <td>
                                                        <?= $i ?>
                                                            </td>
                                                            <td>
                                                                 <?php
                                                           
                                                            if (is_numeric($user->username)&&strlen((string)$user->username)<6 || strpos($user->username, '@') !== false) {
                                                                 echo "Anonymous user";
                                                            } else {
                                                                
                                                               echo $user->username;
                                                            }
                                                            ?>
                                                            </td>
                                                            <td>
                                                                <?= $user->date ?>
                                                            </td>
                                                            <td> 
                                                                 <?=   date('h:i A', strtotime($user->time_starts) ). " - " .date('h:i A', strtotime( $user->time_ends) ) ?>
                                                            </td>
                                                            <td>
                                                                <?= $user->note ?>
                                                            </td>
                                                            <td> 
                                                                <?php if ($user->is_modified == 0) { ?>Schedule
                                                                <?php } else { ?>Reschedule
                <?php } ?>
                                                            </td>
                                                            <td class="text-center">
                                                                <a style="padding-right: 20px" onclick="appDetails(<?= $user->sid ?>, '<?= $user->date ?>')"><i class="fa fa-eye" title="View Details" aria-hidden="true"></i></a>
                                                            </td>
                                                        </tr>
                <?php
                $i++;
            }
        
    }
}
?>


                                    </tbody>
                                </table>
                            </div>
                            <div class="modal fade" id="iwj-confirm-undo-save-job" role="dialog">

                            </div>

                        </div>


                        <div class="iwj-save-jobs iwj-main-block">
                            <form method="post"  class="iwj-form-2 iwj-login-form1">

                                <div class="info-top" style="padding-bottom:0">
                                    <h3 class=""><?php echo "Previous Appointments" ?></h3>

                                </div>

                            </form>
                            <div class="iwj-table-overflow-x">
                                <table class="table userlist" cellspacing="0" width="100%">
                                    <thead>
                                        <tr>
                                            <th width="">ID</th>
                                            <?php
                                            $data = $this->session->userdata('userData');
                                            if($data->role_id==3){ ?>
                                                 <th width="">User</th>
                                        <?php    }else { ?>
                                                 <th width="">Counselor</th>
                                         <?php   }
                                            ?>
                                            <th width="">Appointment Date</th>
                                            <th width="">Time slot</th>
                                            <th width="">Note</th>
                                            <th width="">Status</th>
                                            <th width="" class="text-center">Action</th>
                                        </tr>
                                    </thead>
                                    <tbody>
<?php
$i = 1;
foreach ($data2 as $user) {
    if (!empty($user)) {
        
        if (strtotime($user->date) < strtotime(date('Y-m-d'))) {
            ?>
                                                    <tr id="save-job-1212" class="save-job-item">
                                                        <td>
                                                    <?= $i ?>
                                                        </td>
                                                        <td>
                                                            <?php
                                                            
                                                            if (is_numeric($user->username)&&strlen((string)$user->username)<6 || strpos($user->username, '@') !== false) {
                                                                 echo "Anonymous user";
                                                            } else {
                                                                if (is_numeric(trim($user->username))) 
                                                                    echo "Anonymous user";
                                                                else
                                                                    echo $user->username;
                                                            }
                                                            ?>
                                                        </td>
                                                        <td>
                                                            <?= $user->date ?>
                                                        </td>
                                                        <td> 
             <?=   date('h:i A', strtotime($user->time_starts) ). " - " .date('h:i A', strtotime( $user->time_ends) ) ?>
                                                        </td>
                                                        <td>
            <?= $user->note ?>
                                                        </td>
                                                        <td> 
            <?php if ($user->is_modified == 0) { ?>Schedule
                                                            <?php } else { ?>Reschedule
                                                            <?php } ?>
                                                        </td>
                                                        <td class="text-center">
                                                            <a style="padding-right: 20px" onclick="appDetails(<?= $user->sid ?>, '<?= $user->date ?>')"><i class="fa fa-eye" title="View Details" aria-hidden="true"></i></a>
<?php
                                                        if ($this->session->userdata('loggedIn') == true) {
                                                            
                                                       if ($data1[0]->role_id == 3) {
                                                            ?>
                                                            <a style="padding-right: 20px" href="<?php echo base_url();?>Counselor/counselor_addnotes/<?php echo urlencode(base64_encode($user->sid)); ?>/<?= urlencode(base64_encode($user->date)) ?>"><span class="glyphicon glyphicon-pencil"></span></a>
                                                        <?php } } ?>
                                                        </td>
                                                    </tr>
            <?php
            $i++;
        }else if(strtotime($user->date) == strtotime(date('Y-m-d'))&& strtotime($user->time_starts) < strtotime(date("h:i A"))){
             ?>
                                                    <tr id="save-job-1212" class="save-job-item">
                                                        <td>
                                                    <?= $i ?>
                                                        </td>
                                                        <td>
                                                            <?php
                                                            
                                                            if (is_numeric($user->username)&&strlen((string)$user->username)<6 || strpos($user->username, '@') !== false) {
                                                                 echo "Anonymous user";
                                                            } else {
                                                                
                                                               echo $user->username;
                                                            }
                                                            ?>
                                                        </td>
                                                        <td>
                                                            <?= $user->date ?>
                                                        </td>
                                                        <td> 
             <?=   date('h:i A', strtotime($user->time_starts) ). " - " .date('h:i A', strtotime( $user->time_ends) ) ?>
                                                        </td>
                                                        <td>
            <?= $user->note ?>
                                                        </td>
                                                        <td> 
            <?php if ($user->is_modified == 0) { ?>Schedule
                                                            <?php } else { ?>Reschedule
                                                            <?php } ?>
                                                        </td>
                                                        <td class="text-center">
                                                            <a style="padding-right: 20px" onclick="appDetails(<?= $user->sid ?>, '<?= $user->date ?>')"><i class="fa fa-eye" title="View Details" aria-hidden="true"></i></a>
                                                        
                                                            
                                                           <?php
                                                        if ($this->session->userdata('loggedIn') == true) {
                                                            
                                                       if ($data1[0]->role_id == 3) {
                                                            ?>
                                                            <a style="padding-right: 20px" href="<?php echo base_url();?>Counselor/counselor_addnotes/<?php echo urlencode(base64_encode($user->sid)); ?>/<?= urlencode(base64_encode($user->date)) ?>"><span class="glyphicon glyphicon-pencil"></span></a>
                                                        <?php } } ?>
                                                        </td>
                                                    </tr>
            <?php
            $i++;
        }
    }
}
?>  


                                    </tbody>
                                </table>
                            </div>
                            <div class="modal fade" id="iwj-confirm-undo-save-job" role="dialog">

                            </div>

                        </div>
                        <div class="clearfix"></div>

                    </div>
                </div>


                <!-- iwj-sidebar-sticky-->
                <div class="iwj-dashboard-sidebar">
                    <div class="user-profile candidate clearfix">
                        <img alt='Peter Pham' src='<?php if($data1[0]->photo!="") { echo $data1[0]->photo; }else { echo base_url()."uploads/images/user.jpg";  } ?>' srcset='' class='avatar avatar-96 photo' height='96' width='96' />            <h4>
                            <span>Howdy!</span>
                            <?php echo $data1[0]->username; ?>          </h4>
                    </div>
                    <div class="iwj-dashboard-menu">
                            <?php $this->load->view('layout/sidebar.php') ?>  
                    </div>
                </div>
            </div>
        </div><!-- .entry-content -->
        <div class="clearfix"></div>
        <footer class="entry-footer ">
        </footer><!-- .entry-footer -->
    </article><!-- #post-## -->
</div>
<div class="modal fade" id="appDetails">
    <div class="modal-dialog modal-lg">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">&times;</span><span class="sr-only">Close</span></button>
                <h4>Appointment Details</h4>
            </div>
            <div class="modal-body">

                <div class="iw-job-content iw-job-detail" style="background: #ffffff;" id="contents-main">
                    <div class="container">

                        <div class="col-sm-12 col-xs-12 col-lg-8 col-md-8">
                            <div class="job-detail">
                                <div class="job-detail-content">
                                    <div class="job-detail-info">
                                        <ul>
                                            <li class="job-type">
                                                <div class="content-inner">
                                                    <div class="left">

                                                        <span class="title">Counselor :</span>
                                                    </div>
                                                    <div id="app_user_name" class="content"></div>
                                                </div>
                                            </li>
                                            <li class="job-type">
                                                <div class="content-inner">
                                                    <div class="left">

                                                        <span class="title">Date :</span>
                                                    </div>
                                                    <div id="app_date" class="content">$1,000 - $2,000</div>
                                                </div>
                                            </li>
                                            <li class="job-type">
                                                <div class="content-inner">
                                                    <div class="left">

                                                        <span class="title">Time Slot :</span>
                                                    </div>
                                                    <div id="app_time_slot" class="content">
                                                        Full-Time                                        
                                                    </div>
                                                </div>
                                            </li>
                                            <li class="job-type">
                                                <div class="content-inner">
                                                    <div class="left">

                                                        <span class="title">Status :</span>
                                                    </div>
                                                    <div id="app_status" class="content">
                                                        4 months ago                                        
                                                    </div>
                                                </div>
                                            </li>
                                            <li class="job-type">
                                                <div class="content-inner">
                                                    <div class="left">

                                                        <span class="title">Call Status :</span>
                                                    </div>
                                                    <div id="app_call_status" class="content">
                                                        Web Developer                                        
                                                    </div>
                                                </div>
                                            </li>
                                            <li class="job-type">
                                                <div class="content-inner">
                                                    <div class="left">

                                                        <span class="title">Call Duration :</span>
                                                    </div>
                                                    <div id="app_call_duration" class="content">
                                                        Web Developer                                        
                                                    </div>
                                                </div>
                                            </li>
                                            <li class="job-type">
                                                <div class="content-inner">
                                                    <div class="left">

                                                        <span class="title">Remuneration :</span>
                                                    </div>
                                                    <div id="app_renumeration" class="content">
                                                        June 30, 2017                                        
                                                    </div>
                                                </div>
                                            </li>

                                            <li class="job-type">
                                                <div class="content-inner">
                                                    <div class="left">

                                                        <span class="title">Note :</span>
                                                    </div>
                                                    <div id="app_note" class="content">
                                                        June 30, 2017                                        
                                                    </div>
                                                </div>
                                            </li>
                                        </ul>
                                    </div>
                                </div>

                            </div>
                        </div>

                    </div>

                </div>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
            </div>
        </div>
    </div>
</div>


<script>
    function appDetails(id, date) {


        $.ajax({
            url: '<?php echo base_url(); ?>home/appDetails',
            data: {id: id, date: date},
            type: 'post',
            dataType: "json",
            success: function (data) {
                var q = new Date();
                var m = q.getMonth() + 1;
                var d = q.getDay();
                var y = q.getYear();

                var date = new Date(y, m, d);
console.log(date)
                mydate = new Date(data.date);
                

                if (date > mydate)
                {
                    var stat="Completed"
                } else
                {
                    var stat="Pending"
                }
                if(data.status!="completed"){
                    var duration="-";
                    var renum="-";
                }else{
                     var duration=data.duration;
                    var renum=data.renumeration;
                }
                $('#app_user_name').text(data.username);
                $('#app_date').text(data.date);
                $('#app_time_slot').text(formatAMPM(data.time_starts) + ' - ' + formatAMPM(data.time_ends));
                $('#app_status').text(stat);
                $('#app_call_status').text(data.status);
                $('#app_call_duration').text(duration);
                $('#app_renumeration').text(renum);
                $('#app_note').text(data.note);

                $('#appDetails').modal('show')
            }
        });

    }


</script>
<?php $this->load->view('layout/footer.php') ?>

