<?php $this->load->view('layout/header.php') ?>      
<div class="page-heading default">
    <div class="container-inner">
        <div class="container">
            <div class="page-title">
                <div class="iw-heading-title">
                    <h1>
                        Lost Password                            </h1>

                </div>
            </div>
        </div>
    </div>
  
</div>
<div class="contents-main" id="contents-main" style="background-color: #f2f2f1;padding: 50px">
    <div class="container">
        <div class="row">
            <div class="col-sm-12 col-xs-12">

                <article id="post-147" class="post-147 page type-page status-publish hentry">
                    <div class="entry-content">
                        <div class="iwj-lostpass">
                            <?php if($token_expired=="false") { ?>
                            <form action="<?php echo site_url('home/updatepassword'); ?>" method="post" class="iwj-form iwj-login-form1">
                                <div class="iwj-respon-msg hide"></div>
                                <?php if ($this->session->flashdata('flash_message')) { ?>
                                <span id="generate_otp_span1" style="margin-bottom: 20px"  class="alert alert-info"><?= $this->session->flashdata('flash_message') ?></span>
                                    <?php } ?>
                                <input type="hidden" name="token" value="<?php echo $token ?>">
<!--                                <div class="iwj-field">
                                    <label>Current Password</label>
                                    <div class="iwj-input">
                                        <i class="fa fa-user"></i>
                                        <input type="password"  name="current_password" placeholder="Enter Current Password">
                                    </div>
                                </div>-->
                                <div class="iwj-field">
                                    <label>New Password</label>
                                    <div class="iwj-input">
                                        <i class="fa fa-user"></i>
                                        <input type="password"  name="password" placeholder="Enter New Password">
                                    </div>
                                </div>
                                 <div class="iwj-field">
                                     
                                     
                                    <label>Confirm Password</label>
                                    <div class="iwj-input">
                                        <i class="fa fa-user"></i>
                                        <input type="password"  name="conpassword" placeholder="Confirm New Password">
                                    </div>
                                </div>
                                <div class="iwj-button-loader">
                                    <input type="submit" style="padding: 15px 25px" class="iwj-btn iwj-btn-primary iwj-btn-full iwj-btn-large iwj-lostpass-btn" value="Get New Password">
                                </div>
                                <div class="login-register-account">
                                    <a onclick="show_popup()">Login</a>
                                    <a href="<?php echo site_url('user/index'); ?>">Register</a>
                                </div>
                            </form>
                            <?php } else{ ?>
                          
                            <div class="alert alert-warning">
                                <strong>Please note:</strong> Your reset link have expired. Click  <a href="<?php echo site_url('home/lostpass'); ?>">here</a> to send a new request.
                            </div>
                            
                            <?php } ?>
                        </div>

                    </div><!-- .entry-content -->
                    <div class="clearfix"></div>
                    <footer class="entry-footer">
                    </footer><!-- .entry-footer -->
                </article><!-- #post-## -->
            </div>
        </div>
    </div>
</div>
<?php $this->load->view('layout/footer.php') ?>