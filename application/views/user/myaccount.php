<?php $this->load->view('layout/user_header.php') ?>
<div class="contents-main" id="contents-main" style="margin-top:3%">

    <article id="post-141" class="post-141 page type-page status-publish hentry">
        <div class="entry-content">
            <div class="iwj-dashboard clearfix">

                <div class="iwj-dashboard-menu-mobile">
                    <div class="dropdown">
                        <button class="btn btn-primary dropdown-toggle"  type="button" data-toggle="dropdown">Menu Dashboard <span class="caret"></span></button>


                        <?php $this->load->view('layout/menu.php') ?>
                    </div>
                </div>

                <div class="iwj-dashboard-main save-jobs">
                    <div class="iwj-dashboard-main-inner">
                        <div class="iwj-save-jobs iwj-main-block">

                            <form method="post"  class="iwj-form-2 iwj-login-form1">

                                <div class="info-top" style="padding-bottom:0">
                                    <h3 class=""><?php echo "Purchase History" ?></h3>

                                </div>

                            </form>

                            <div class="iwj-table-overflow-x">
                                <table class="table userlist" id="" class="display" cellspacing="0" width="100%">
                                    <thead>
                                        <tr>
                                            <th width="">ID</th>
                                            <th width="">Amount</th>
                                            <th width="">Purchase Date</th>
                                            <th width="">Status</th>
<!--                                            <th width="" class="text-center">Action</th>-->
                                        </tr>
                                    </thead>
                                    <tbody>
                                        <?php
                                        $i = 1;
                                        foreach ($payment as $user) {
                                            ?>
                                            <tr id="save-job-1212" class="save-job-item">
                                                <td>
                                                    <?= $i ?>
                                                </td>
                                                <td>
                                                    <?= $user->coupon_amount ?>
                                                </td>
                                                <td>
                                                    <?= $user->purchase_date ?>
                                                </td>
                                                <td> 
                                                    <?= $user->payment_status ?>
                                                </td>

<!--                                                <td class="text-center">
                                                    <a style="padding-right: 20px" href="<?php echo base_url(); ?>admin/updatecoupon/<?= $user->id ?>"><i class="fa fa-pencil-square-o" title="Edit" aria-hidden="true"></i></a>
                                                    <a onclick="delcoupon(<?= $user->id ?>)" href=""><i class="fa fa-trash-o" title="Delete" aria-hidden="true"></i></a>
                                                </td>-->
                                            </tr>
                                            <?php $i++;
                                        }
                                        ?>


                                    </tbody>
                                </table>
                            </div>
                            <div class="modal fade" id="iwj-confirm-undo-save-job" role="dialog">

                            </div>

                        </div>


                        <div class="iwj-save-jobs iwj-main-block">

                            <form method="post"  class="iwj-form-2 iwj-login-form1">

                                <div class="info-top" style="padding-bottom:0">
                                    <h3 class=""><?php echo "Call History" ?></h3>

                                </div>

                            </form>

                            <div class="iwj-table-overflow-x">
                                <table class="table userlist" id="" class="display" cellspacing="0" width="100%">
                                    <thead>
                                        <tr>
                                            <th width="">ID</th>
                                            <th width="">Counselor Name</th>
                                            <th width="">Appointment Date</th>
                                            <th width="">Time Slot</th>
                                            <th width="">Status</th>
                                            <th width="">Duration</th>
                                            <th width="">Price</th>
<!--                                            <th width="" class="text-center">Action</th>-->
                                        </tr>
                                    </thead>
                                    <tbody>
                                        <?php
                                        $i = 1;
                                        foreach ($call as $user) {
                                            if (!empty($user)) {
                                                ?>
                                                <tr id="save-job-1212" class="save-job-item">
                                                    <td>
        <?= $i ?>
                                                    </td>
                                                    <td>
        <?= $user->cname ?>
                                                    </td>
                                                    <td>
        <?= $user->date ?>
                                                    </td>
                                                    <td> 
        <?php echo date('h:i A', strtotime($user->time_starts) ). " - " .date('h:i A', strtotime( $user->time_ends) ) ?>
                                                    </td>

                                                    <td>
        <?= $user->cstatus ?>
                                                    </td>
                                                    <td>
                                                        <?php
                                                        $init = $user->duration;
                                                        $hours = floor($init / 3600);
                                                        $minutes = floor(($init / 60) % 60);
                                                        $seconds = $init % 60;

                                                        echo "$hours:$minutes:$seconds";
                                                        ?>
                                                    </td>
                                                    <td> 
                                                        <?php
                                                        if ($user->duration > 60) {

                                                            echo $user->amount;
                                                        } else {
                                                            echo "-";
                                                        }
                                                        ?>
                                                    </td>

<!--                                                    <td class="text-center">
                                                        <a style="padding-right: 20px" href="<?php echo base_url(); ?>admin/updatecoupon/<?= $user->id ?>"><i class="fa fa-pencil-square-o" title="Edit" aria-hidden="true"></i></a>
                                                        <a onclick="delcoupon(<?= $user->id ?>)" href=""><i class="fa fa-trash-o" title="Delete" aria-hidden="true"></i></a>
                                                    </td>-->
                                                </tr>
        <?php $i++;
    }
}
?>


                                    </tbody>
                                </table>
                            </div>
                            <div class="modal fade" id="iwj-confirm-undo-save-job" role="dialog">

                            </div>

                        </div>
                        <div class="clearfix"></div>

                    </div>

                </div>


                <!-- iwj-sidebar-sticky-->
                <div class="iwj-dashboard-sidebar">
                    <div class="user-profile candidate clearfix">
                        <img alt='Peter Pham' src='<?php  echo $data1[0]->photo; ?>' srcset='' class='avatar avatar-96 photo' height='96' width='96' />           
                        <h4>
                            <span>Howdy!</span>
                            <?php echo $data1[0]->username; ?>          
                        </h4>
                    </div>
                    <div class="iwj-dashboard-menu">
                        <?php $this->load->view('layout/sidebar.php') ?>
                    </div>
                </div>
            </div>
        </div><!-- .entry-content -->
        <div class="clearfix"></div>
        <footer class="entry-footer ">
        </footer><!-- .entry-footer -->
    </article><!-- #post-## -->
</div>

<?php $this->load->view('layout/footer.php') ?>