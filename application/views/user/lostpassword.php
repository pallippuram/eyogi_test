<?php $this->load->view('layout/header.php') ?>      
<div class="page-heading default">
    <div class="container-inner">
        <div class="container">
            <div class="page-title">
                <div class="iw-heading-title">
                    <h1>
                        Lost Password </h1>

                </div>
            </div>
        </div>
    </div>
  
</div>
<div class="contents-main" id="contents-main" style="background-color: #f2f2f1;padding: 50px">
    <div class="container">
        <div class="row">
            <div class="col-sm-12 col-xs-12">

                <article id="post-147" class="post-147 page type-page status-publish hentry">
                    <div class="entry-content">
                        <div class="iwj-lostpass">
                            <form action="<?php echo site_url('user/updatepassword'); ?>" method="post" class="iwj-form iwj-login-form1">
                                <div class="iwj-respon-msg hide"></div>
                                <div class="iwj-field">
                                    <label>Email Id</label>
                                    <div class="iwj-input" style="padding-left: 0px !important">
                                       
                                        <input type="email"  name="email" placeholder="Enter Email Id">
                                    </div>
                                </div>
                                <?php if ($this->session->flashdata('flash_message')) { ?>
                                 <span id="generate_otp_span1"  class="alert alert-info"><?= $this->session->flashdata('flash_message') ?></span>
                                    <?php } ?>
                                <div class="iwj-button-loader">
                                    <input type="submit" style="padding: 15px 25px" class="iwj-btn iwj-btn-primary iwj-btn-full iwj-btn-large iwj-lostpass-btn" value="Get Password">
                                </div>
                                <div class="login-register-account">
                                    <a onclick="show_popup()">Login</a>
                                    <a href="<?php echo site_url('user/index'); ?>">Register</a>
                                </div>
                            </form>
                        </div>

                    </div><!-- .entry-content -->
                    <div class="clearfix"></div>
                    <footer class="entry-footer">
                    </footer><!-- .entry-footer -->
                </article><!-- #post-## -->
            </div>
        </div>
    </div>
</div>

<?php $this->load->view('layout/footer.php') ?>

<script type="text/javascript">
    
    $("#generate_otp_span1").click(function(){
    $("#generate_otp_span1").hide();
});
</script>
