<?php $this->load->view('layout/header.php') ?>

<div class="page-heading default">
    <div class="container-inner">
        <div class="container">
            <div class="page-title">
                <div class="iw-heading-title">
                    <h1 style="color:#894724">
                        
                        Rate Your Comments         
                    </h1>

                </div>
            </div>
        </div>
    </div>
    
        </div>

        <style>
#rating_modal .close{
    font-size: 40px;
    color: #fff;
    opacity: 1;
}

#rating_modal .modal-header{
    background: #d9534f;
    color: #fff;
    border-top-left-radius: 5px;
    border-top-right-radius: 5px;
}   

.star-rating {
    line-height:32px;
    font-size:1.25em;       
}

.star {
    line-height:32px;
    font-size:1.25em;
    cursor: pointer;
}

.star-rating .fa-star{color: #c20001;}
.star .fa-star{color: #c20001;}


.rating {
  unicode-bidi: bidi-override;
  direction: rtl;
  text-align: center;
}
.rating > .star {
  display: inline-block;
  position: relative;
  width: 1.1em;
  color:green;
}
.rating > .star:hover,
.rating > .star:hover ~ .star {
  color: transparent;
}
.rating > .star:hover:before,
.rating > .star:hover ~ .star:before {
   content: "\2605";
   position: absolute;
   left: 0; 
   color: gold;
}
</style>


<div class="contents-main col-md-12 iw-job-content iw-job-detail" style="background: #f2f2f1;padding-top: 50px;padding-bottom: 50px;" id="contents-main">
    <div class="container purchase" style="">
        <div class="row">
            <div class="col-sm-12 col-xs-12 col-md-12">
                <div class="job-detail">
                    <div class="job-detail-content">
                        <div class="col-sm-12 profile_follow_editor" style="background-color: #fff;">
                            <div class="container p0">
                                <div class="col-sm-12 profile_follow">
                                    <div class="col-sm-8 profile_padd">
                                        <div class="col-sm-9 p0">
                                            <h3>Counselor : <?php for($k=0;$k<count($cdetails);$k++){
                                                echo  $cdetails[$k]['username'];}?></h3>

                                                <form action="#" method="post" name="star1" id="star1" style="padding-top:20px" novalidate="novalidate"> 

                                                <?php for($k=0;$k<count($cdetails);$k++){ ?>

                                                      <input type="hidden" id="c_name" name="c_name" value="<?php echo  $cdetails[$k]['username']; ?>">
                                                      <input type="hidden" id="c_id" name="c_id" value="<?php echo  $cdetails[$k]['id']; ?>">
                            
                                                <?php }?>

                                                <?php for($k=0;$k<count($udetails);$k++){ ?>
                                                       
                                                      <input type="hidden" id="u_name" name="u_name" value="<?php echo  $udetails[$k]['username']; ?>">
                                                      <input type="hidden" id="u_id" name="u_id" value="<?php echo  $udetails[$k]['id']; ?>">

                                                <?php }?>

                                                    <div class="form-group">
                                                        <label for="pwd">Hi, <?php for($k=0;$k<count($udetails);$k++){ echo  $udetails[$k]['username']; } ?>
                                                        <br>Leave a comment here:</label>
                                    
                                                        <textarea class="form-control" name="comment" id="comment"></textarea>
                                                    </div>

                                                    <div id="a1">    
                                                        <div class="star pointer" >
                                                            <span class="fa fa-star-o" data-rating="1" onclick="rating(1)"></span>
                                                            <span class="fa fa-star-o" data-rating="2" onclick="rating(2)"></span>
                                                            <span class="fa fa-star-o" data-rating="3" onclick="rating(3)"></span>
                                                            <span class="fa fa-star-o" data-rating="4" onclick="rating(4)"></span>
                                                            <span class="fa fa-star-o" data-rating="5" onclick="rating(5)"></span>

                                                            <input type="hidden" name="cstart" id="cstart" value="">
                                                        </div>
                                                    </div>  

                                                    <div class="form-group">
                                                        <label for="pwd">Leave your call status:</label>
                                                            <textarea class="form-control" name="cstatus" id="cstatus" value=""></textarea>
                                                    </div>

                                                    <div id="a2">
                                                        <div class="star pointer" >
                                                            <span class="fa fa-star-o" data-rating="6" onclick="rating(6)"></span>
                                                            <span class="fa fa-star-o" data-rating="7" onclick="rating(7)"></span>
                                                            <span class="fa fa-star-o" data-rating=8 onclick="rating(8)"></span>
                                                            <span class="fa fa-star-o" data-rating="9" onclick="rating(9)"></span>
                                                            <span class="fa fa-star-o" data-rating="10" onclick="rating(10)"></span>

                                                            <input type="hidden" name="callstart" id="callstart">
                                                        </div>
                                                    </div>

                                                    <input type="hidden" id="s_id" name="s_id" value="<?php echo  $s_id ?>">
                            <!-- <div class="submit-button"><input type="submit" value="Submit" class="wpcf7-form-control wpcf7-submit" style="color:none" /></div>-->
                                                    <input type="submit" class="iwj-btn pull-left edit_btn iwj-btn-primary iwj-candidate-btn" value="submit" />
                                                    <div id="rate_submit" class="alert alert-success" style="width: 50%; margin-left: 49%;display: none;"></div>

                                                </form>
                                                <div class="clear10"></div>
                                                <div class="clear5"></div>
                                                <div class="clearfix"></div>
                                        </div>
                                        <div class="col-sm-3 p0" id="following-data">
                                            <ul class="follow_ul">
                    <!--    <li><a href="#" onclick="follower()"><h3>FOLLOWERS</h3> <span><?= $followers_count ?></span></li> -->
                      <!--  <li style="margin:0px;"><a href="#" onclick="following()"><h3>FOLLOWING</h3> <span><?= $follwing_count ?></span></a></li>-->
                                            </ul>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<div class="clear40"></div>
<div class="container p0"> 
    <div class="col-sm-9 comment_main_padd">
      <div class="col-sm-12 p0"></div>
      <div class="clear10"></div>
      <div class="clear5"></div>
      <div class="col-sm-12 profile_feedback">
          <span class="pull-right">
              
                <!-- End Rating Picks -->
          </span>
          <div class="clearfix"></div>
    

        <!-- aria-expanded attribute is mandatory -->
        <!-- bootstrap changes it to true/false on toggle -->
          <a href="#block-id" class="" data-toggle="collapse" aria-expanded="false" aria-controls="block-id"></a>
          <input type="hidden" name="blog_id" id="blog_id" value="1">

      </div>
    </div>
</div>  

<?php $this->load->view('layout/footer.php') ?>

<script type="text/javascript">
    

function rating(a) {
    //$('#rating_modal').modal('show');
    if(a<6){

 

        $.ajax({
            type: 'POST',
            // make sure you respect the same origin policy with this url:
            // http://en.wikipedia.org/wiki/Same_origin_policy
            url: '<?php echo base_url();?>Home/add_ratings',
            data: { 
                    'count': a
        
                },
            success: function(msg){
            // alert(msg);
                $("#a1").html("");
                $("#a1").html(msg);
                $("#cstart").val(a);
            }
        });

    }else if(a>5){

        $.ajax({
            type: 'POST',
            // make sure you respect the same origin policy with this url:
            // http://en.wikipedia.org/wiki/Same_origin_policy
            url: '<?php echo base_url();?>Home/add_ratings',
        data: { 
            'count': a
        
            },
        success: function(msg){

            // alert(msg);

            $("#a2").html("");
            $("#a2").html(msg);
            $("#callstart").val(a);
        }
        });

    }

} </script>


