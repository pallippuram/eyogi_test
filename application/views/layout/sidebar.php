
<ul >
    <?php if ($data1[0]->role_id == 1) { ?>
        <li>
            <a onclick="location.href = '<?php echo base_url(); ?>admin/add_counselor';" ><i class="fa fa-user"></i>Add Counselor</a>
        </li>                          
        <li>
            <a onclick="location.href = '<?php echo base_url(); ?>admin/userlist';" ><i class="fa fa-user"></i>User List</a>
        </li>
        <li>
            <a onclick="location.href = '<?php echo base_url(); ?>admin/admin_list';" ><i class="fa fa-user"></i>Admin List</a>
        </li>
        <li>
            <a onclick="location.href = '<?php echo base_url(); ?>admin/counselorlist';" ><i class="fa fa-user"></i>Counselor List</a>
        </li>
        <li>
            <a onclick="location.href = '<?php echo base_url(); ?>admin/counselor_paymnt';" ><i class="fa fa-newspaper-o"></i>Counselor Payments</a>
        </li>
        <li>
            <a onclick="location.href = '<?php echo base_url(); ?>admin/videoCall';" ><i class="fa fa-video-camera"></i>Counselor Video-Call</a>
        </li>
        <li>
            <a onclick="location.href = '<?php echo base_url(); ?>admin/couponlist';" ><i class="fa fa-user"></i>Coupon List</a>
        </li>

        <li>
            <a onclick="location.href = '<?php echo base_url(); ?>admin/purchaseDetails';" ><i class="fa fa-newspaper-o"></i>Purchase Report</a>
        </li>
        <li>
            <a onclick="location.href = '<?php echo base_url(); ?>admin/callDetails';" ><i class="fa fa-newspaper-o"></i>Call Report</a>
        </li>
        <!--         <li>
                    <a onclick="location.href = '<?php echo base_url(); ?>admin/call_back_user_list';" ><i class="fa fa-user"></i>Call Back Users</a>
                </li>-->
        <li>
            <a onclick="location.href = '<?php echo base_url(); ?>admin/counselorSchedule';"><i class="fa fa-envelope-o"></i>Schedule Calendar</a>
        </li>
        <li>
            <a onclick="location.href = '<?php echo base_url(); ?>admin/quickSchedule';"><i class="fa fa-envelope-o"></i>Quick  Schedule</a>
        </li>
        <li>
            <a onclick="location.href = '<?php echo base_url(); ?>admin/myprofile';"><i class="fa fa-envelope-o"></i>My Profile</a>
        </li>


        <li>
            <a onclick="location.href = '<?php echo base_url(); ?>home/logout';"><i class="fa fa-sign-out"></i>Logout</a>
        </li>
    <?php } else if ($data1[0]->role_id == 2) { ?>

        <li>
            <a onclick="location.href = '<?php echo base_url(); ?>admin/add_counselor';" ><i class="fa fa-user"></i>Add Counselor</a>
        </li>                          
        <li>
            <a onclick="location.href = '<?php echo base_url(); ?>admin/userlist';" ><i class="fa fa-user"></i>User List</a>
        </li>
        <li>
            <a onclick="location.href = '<?php echo base_url(); ?>admin/counselorlist';" ><i class="fa fa-user"></i>Counselor List</a>
        </li>
        <li>
            <a onclick="location.href = '<?php echo base_url(); ?>admin/counselor_paymnt';" ><i class="fa fa-newspaper-o"></i>Counselor Payments</a>
        </li>
        <li>
            <a onclick="location.href = '<?php echo base_url(); ?>admin/videoCall';" ><i class="fa fa-video-camera"></i>Counselor Video-Call</a>
        </li>
        <li>
            <a onclick="location.href = '<?php echo base_url(); ?>admin/couponlist';" ><i class="fa fa-user"></i>Coupon List</a>
        </li>

        <li>
            <a onclick="location.href = '<?php echo base_url(); ?>admin/purchaseDetails';" ><i class="fa fa-newspaper-o"></i>Purchase Report</a>
        </li>
        <li>
            <a onclick="location.href = '<?php echo base_url(); ?>admin/callDetails';" ><i class="fa fa-newspaper-o"></i>Call Report</a>
        </li>
        <!--        <li>
                    <a onclick="location.href = '<?php echo base_url(); ?>admin/call_back_user_list';" ><i class="fa fa-user"></i>Call Back Users</a>
                </li>-->
        <li>
            <a onclick="location.href = '<?php echo base_url(); ?>admin/myprofile';"><i class="fa fa-envelope-o"></i>My Profile</a>
        </li>


        <li>
            <a onclick="location.href = '<?php echo base_url(); ?>home/logout';"><i class="fa fa-sign-out"></i>Logout</a>
        </li>
    <?php } else if ($data1[0]->role_id == 3) { ?> 
        <li>
            <a onclick="location.href = '<?php echo base_url(); ?>counselor/index';" ><i class="fa fa-user"></i>Dashboard</a>
        </li>
        
        <li>
            <a onclick="location.href = '<?php echo base_url(); ?>admin/availability/<?=$data1[0]->id?>';" ><i class="fa fa-user"></i>Set Availability</a>
        </li>
<!--        <li>
            <a onclick="location.href = '<?php //echo base_url(); ?>admin/userlist';" ><i class="fa fa-user"></i>User List</a>
        </li>-->

        <li>
            <a onclick="location.href = '<?php echo base_url(); ?>counselor/counselor_appointments';"><i class="fa fa-newspaper-o"></i>My Appointments</a>
        </li>
        <li>
            <a onclick="location.href = '<?php echo base_url(); ?>admin/myprofile';"><i class="fa fa-envelope-o"></i>My Profile</a>
        </li>
        <li>
            <a onclick="location.href = '<?php echo base_url(); ?>home/logout';"><i class="fa fa-sign-out"></i>Logout</a>
        </li>
    <?php } else if ($data1[0]->role_id == 4) { ?>

        <li>
            <a  onclick="location.href = '<?php echo base_url(); ?>user/myaccount';"><i class="fa fa-user"></i>My Account</a>
        </li>
         

        <li>
            <a onclick="location.href = '<?php echo base_url(); ?>user/appointment_list';"><i class="fa fa-newspaper-o"></i>My Appointments</a>
        </li>


        <li>
            <a onclick="location.href = '<?php echo base_url(); ?>admin/myprofile';"><i class="fa fa-envelope-o"></i>My Profile</a>
        </li>
        <li>
            <a onclick="location.href = '<?php echo base_url(); ?>admin/credits';"><i class="fa  fa-inr"></i>My Credits</a>
        </li>

        <li>
            <a onclick="location.href = '<?php echo base_url(); ?>home/logout';"><i class="fa fa-sign-out"></i>Logout</a>
        </li>
    <?php } ?>
</ul>    

