<footer class="iw-footer iw-footer-default">
    <div class="iw-footer-middle">
        <div class="container">
            <div class="row">
                <div class="col-lg-6 col-md-6 col-sm-6 col-sx-12"><div class="footer-left"><div class="row"><div class="col-lg-6 col-md-6 col-sm-12 col-sx-12"><div class="inwave-contact-info-2 widget widget_inwave-contact-info"><div class="widget-info-footer no-title"><h3 class="widget-title"></h3><div class="subtitle">
                                            <div class="line1"></div>
                                            <div class="line2"></div>
                                            <div class="clearfix"></div>
                                        </div>            <a class="iw-widget-about-us" href="">
                                            <img src="<?php echo base_url(); ?>assets/images/logo.png" alt=""/>
                                        </a>            <p>At vero eos et accusamus et iusto odio dignissimos ducimus qui blanditiis praesentium voluptatum deleniti atque corrupti quos...        </p>
                                        <a class="link_page_about_us" href="#">Learn more </a>
                                        <ul class="iw-social-footer-all">
                                            <li><a class="facebook" href="http://facebook.com" title="Facebook"><i class="fa iwj-icon-facebook"></i></a></li><li><a class="twitter" href="http://twitter.com" title="Twitter"><i class="fa iwj-icon-twitter"></i></a></li><li><a class="google-plus" href="http://google-plus.com" title="Google Plush"><i class="fa iwj-icon-gplus"></i></a></li><li><a class="linkedin" href="http://linkedin.com" title="Linkedin"><i class="fa iwj-icon-linkedin"></i></a></li></ul>
                                    </div></div></div><div class="col-lg-6 col-md-6 col-sm-12 col-sx-12">
                                <div class="nav_menu-2 widget widget_nav_menu"><h3 class="widget-title">Quick Links</h3>
                                    <div class="subtitle">
                                        <div class="line1"></div>
                                        <div class="line2"></div>
                                        <div class="clearfix"></div>
                                    </div>
                                    <div class="menu-for-candidates-container">
                                        <ul id="menu-for-candidates" class="menu">

                                            <li id="menu-item-1387" class="menu-item menu-item-type-post_type menu-item-object-page menu-item-1387"><a href="">Home</a></li>
                                            <li id="menu-item-1389" class="menu-item menu-item-type-post_type menu-item-object-page menu-item-1389"><a href="">Schedule</a></li>
                                            <li id="menu-item-1386" class="menu-item menu-item-type-post_type menu-item-object-page current-menu-item page_item page-item-141 current_page_item menu-item-1386 selected active "><a href="">Code Purchase</a></li>
                                        </ul>
                                    </div>
                                </div></div></div></div></div>
                <div class="col-lg-6 col-md-6 col-sm-6 col-sx-12">
                    <div class="footer-right">
                        <div class="row">
                            <div class="col-lg-6 col-md-6 col-sm-12 col-sx-12">
                                <div class="nav_menu-3 widget widget_nav_menu"><h3 class="widget-title"></h3><div class="subtitle">
                                        <div class="line1"></div>
                                        <div class="line2"></div>
                                        <div class="clearfix"></div>
                                    </div>
                                    <div class="menu-for-employers-container">
                                        <ul id="menu-for-employers" class="menu">
                                            <li id="menu-item-1390" class="menu-item menu-item-type-post_type menu-item-object-page menu-item-1390"><a href="">Blog</a></li>
                                            <li id="menu-item-1392" class="menu-item menu-item-type-post_type menu-item-object-page menu-item-1392"><a href="">Contact Us</a></li>
                                            <li id="menu-item-594" class="menu-item menu-item-type-custom menu-item-object-custom menu-item-594"><a href="">About Us</a></li>

                                        </ul>
                                    </div>
                                </div>
                            </div>
                            <div class="col-lg-6 col-md-6 col-sm-12 col-sx-12">
                                <div class="nav_menu-3 widget widget_nav_menu"><h3 class="widget-title"></h3><div class="subtitle">
                                        <div class="line1"></div>
                                        <div class="line2"></div>
                                        <div class="clearfix"></div>
                                    </div>
                                    <div class="menu-for-employers-container">
                                        <ul id="menu-for-employers" class="menu">
                                            <li id="menu-item-1390" class="menu-item menu-item-type-post_type menu-item-object-page menu-item-1390"><a href="">Terms & Conditions</a></li>
                                            <li id="menu-item-1392" class="menu-item menu-item-type-post_type menu-item-object-page menu-item-1392"><a href="">Privacy Policy</a></li>


                                        </ul>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>            </div>
        </div>
    </div>

    <div class="iw-copy-right">
        <div class="container">
            <div class="row">
                <div class="col-md-12 col-sm-12">
                    <p>© <a href='#'>eYogi</a> All rights reserved.</p>
                </div>
            </div>
        </div>
    </div>
</footer>
</div> <!--end .content-wrapper -->
<div id="iwj-login-popup" class="modal-popup modal fade iwj-login-form-popup">
    <div class="modal-dialog">
        <div class="modal-header">
            <h4 class="modal-title">Login to your account</h4>
            <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
        </div>
        <div class="modal-content">
            <form  id="user_login" action="<?php echo base_url(); ?>user/login"  method="post" class="iwj-form iwj-login-form1">
                <div id="error_msg_login" style="display:none" class="alert alert-danger"></div>
                <div class="iwj-field">
                    <label>User name</label>
                    <div class="iwj-input">
                        <i class="fa fa-user"></i>
                        <input type="text" id="login_email" name="email" placeholder="Enter Your Username.">
                    </div>
                </div>
                <div class="iwj-field">
                    <label>Password</label>
                    <div class="iwj-input">
                        <i class="fa fa-keyboard-o"></i>
                        <input type="password" id="login_password" name="password" placeholder="Enter Password.">
                    </div>
                </div>
                <div class="form-field iwj-button-loader">
                    <input type="hidden" name="redirect_to" value="user/login">
                    <button type="submit" name="login" class="iwj-btn iwj-btn-primary iwj-btn-large iwj-btn-full iwj-login-btn">Login</button>
                </div>
                <div class="text-center lost-password">
                    <a target="_blank" href="<?php echo base_url(); ?>home/lostpass">Lost Password?</a>
                </div>
                <div class="iwj-divider">
                    <span class="line"></span>
                    <span class="circle">Or</span>
                </div>

                <!-- HTML for render Google Sign-In button -->

                <!-- HTML for displaying user details -->
                <!--                <div class="userContent"></div>-->


                <div class="social-login row">
                    <div class="col-md-4">
                        <a href="<?php echo $authUrl ?>" class="iwj-btn iwj-btn-primary iwj-btn-medium iwj-btn-full iwj-btn-icon social-login-facebook"><i class="fa fa-facebook"></i>Facebook</a>
                    </div>
                    <div class="col-md-4">
                        <a  href="<?php echo $loginURL; ?>" class="iwj-btn iwj-btn-primary iwj-btn-medium iwj-btn-full iwj-btn-icon social-login-google"><i class="fa fa-google-plus"></i>Google</a>

                    </div>
                    <div class="col-md-4">
                        <a href="<?php echo $oauthURL; ?>" class="iwj-btn iwj-btn-primary iwj-btn-medium iwj-btn-full iwj-btn-icon social-login-linkedin"><i class="fa fa-linkedin"></i>Linkedin</a>
                    </div>
                </div>                  
                <div class="text-center register-account">
                    You don't have an account? <a href="./../register/index.html">Register</a>
                </div>
            </form>

        </div>
    </div>
</div>
<div id="iwj-balance_check-popup" class="modal-popup modal fade iwj-register-form-popup">
    <div class="modal-dialog">
        <div class="modal-header">
            <h4 class="modal-title">Balance Check</h4>
            <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
        </div>
        <div class="modal-content">
            <form action="./index.html" method="post"  class="iwj-form iwj-register-form">

                <div class="iwj-field">

                    <div class="iwj-input">

                        <input type="text" name="purchase_code" placeholder="Enter Purchase Code">
                    </div>
                </div>


                <div class="iwj-respon-msg iwj-hide"></div>
                <div class="iwj-button-loader">

                    <button type="button" id="balance_check_submit" name="register" class="iwj-btn iwj-btn-primary iwj-btn-full iwj-btn-medium iwj-register-btn">Submit</button>
                </div>


            </form>
            <form action="./index.html" id="credit_balance" style="display:none"  method="post" class="iwj-form iwj-register-form">


                <div id="credit_balance" class="text-center">
                    <label style="font-size:16px">Your credit balance is</label>
                    <h1 style="font-weight: 800" class="text-center">100</h1>

                </div>

                <div class="iwj-respon-msg iwj-hide"></div>
                <div class="iwj-button-loader">

                    <button type="submit" style="background: rgb(50, 158, 210);" name="register" class="iwj-btn iwj-btn-primary iwj-btn-full iwj-btn-large iwj-register-btn">Buy Credits</button>
                </div>


            </form>
        </div>
    </div>
</div>

<div id="iwj-reschedule-popup" class="modal-popup modal fade iwj-register-form-popup">
    <div class="modal-dialog">
        <div class="modal-header">
            <h4 class="modal-title">Reschedule</h4>
            <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
        </div>
        <div class="modal-content">
            <form action="./index.html" method="post"  id="reschedule_purchase_code"  class="iwj-form iwj-register-form">

                <div class="iwj-field">

                    <div class="iwj-input">
                        <?php if ($this->session->userdata('userData')) {
                            ?>
                            <input type="text" name="purchase_code1" value="<?php echo $data1[0]->username; ?>" placeholder="Enter Purchase Code to reschedule">
                            <input type="hidden" value="<?php echo $data1[0]->id; ?>" id="purchase_code" name="purchase_code" placeholder="Enter Purchase Code to reschedule">
                        <?php } else { ?>
                            <input type="text" id="purchase_code"  name="purchase_code" onchange="checkCodeValid(this.value)" placeholder="Enter Purchase Code to reschedule">
                        <?php } ?>

                    </div>
                </div>


                <div class="iwj-respon-msg iwj-hide"></div>
                <div class="iwj-button-loader">

                    <button type="button" id="reschedule_submit" name="register" class="iwj-btn iwj-btn-primary iwj-btn-full iwj-btn-medium iwj-register-btn">Submit</button>
                </div>


            </form>
            <form action="./index.html" id="reschedule" style="display:none"  method="post" class="iwj-form iwj-register-form">

                <div class="carousel slide" id="myCarousel">
                    <div class="carousel-inner" id="carousel_inner">

                    </div>

                    <div class="control-box">                            
                        <a data-slide="prev" href="#myCarousel" class="carousel-control left">‹</a>
                        <a data-slide="next" href="#myCarousel" class="carousel-control right">›</a>
                    </div><!-- /.control-box -->   

                </div><!-- /#myCarousel -->
                <!--                <div class="grid-content" style="background-color:rgb(241, 243, 242);margin-bottom: 10px" data-id="1346">
                                    <div style="padding:0" class="job-item featured-item">
                                        <div class="job-image" style="margin-left:10px"><img alt='AOEVN' src='uploads/2017/07/logo5.jpg' class='avatar avatar-full photo' width='408' height='401' /></div>
                                        <div class="job-info">
                                            <h3 class="job-title"><a href="./../job/uxui-designer/index.html">Ajith</a></h3>
                                            <div class="info-company" style="width:145px;padding-bottom: 10px">
                                                <div class="company">
                                                    Scheduled date & time
                                                </div>
                                                <div class="address"><i class="fa fa-calendar"></i>12/11/2017</div>
                                                <div class="sallary"><i class="fa fa-clock-o"></i>10.00 am</div>
                
                                            </div>
                
                                        </div>
                                        <div class="job-featured"></div>
                                    </div>
                                </div>
                -->
                <div class="text-center" style="margin-bottom: 10px;">
                    Are you sure, you want to reschedule?
                </div>
                <div class="iwj-respon-msg iwj-hide"></div>
                <div class="iwj-button-loader">
                    <button type="submit" style="padding:7px 70px;background-color: rgb(54, 159, 210);" class="iwj-btn" value="submit">No</button>
                    <button type="button" style="padding:7px 70px; float: right;background-color: rgb(251, 135, 142);" id="reschedule_btn" class="iwj-btn iwj-btn-icon iwj-btn-primary iwj-submit-alert-btn" >Yes</button>
                </div>


            </form>

            <form action="./index.html" id="reschedule_confirm" style="display:none;"  method="post" class="iwj-form iwj-register-form">

                <input type="hidden" value="" id="current_schedule_id"> 
                <input type="hidden" value="" id="purchase_code_hdn"> 
                <input type="hidden" name="couns_id" class="user_id" id="re_couns_id">
                <div class="form-group">
                    <label class="col-md-4 control-label" for="description">User</label>
                    <div class="col-md-5">
                        <input disabled="" class="form-control"  placeholder="Enter your name" value="" type="text" id="re_sche_username" name="username">
                    </div>
                </div><br><br>

                <div class="form-group">
                    <label class="col-md-4 control-label" for="description">Reason to reschedule</label>
                    <div class="col-md-5">
                        <textarea class="form-control" cols="15" id="user_re_reason" name="description"></textarea>
                    </div>

                </div><br><br>
                <div class="form-group">
                    <label class="col-md-4 control-label" for="title">Choose Date</label>
                    <div class="col-md-4">
                        <div class='input-group date datetimepicker_re'  id='datetimepicker_re_popup'>
                            <input type='text' class="form-control" />
                            <span class="input-group-addon">
                                <span class="glyphicon glyphicon-calendar"></span>
                            </span>
                        </div>
                    </div>
                    <span style="color:red;display: none;margin-left: 10px;font-weight: bold;">Doctor not available on selected day!!</span>
                </div><br><br>
                <div class="form-group">
                    <label class="col-md-4 control-label" for="title">Counselor Availability</label>
                    <div class="col-md-4">
                        <select data-options="{&quot;allowClear&quot;:false,&quot;width&quot;:&quot;none&quot;,&quot;placeholder&quot;:&quot;&quot;,&quot;minimumResultsForSearch&quot;:-1}" required id="counselor_availabilty" class="iwjmb-select_advanced counselor_availabilty" name="gencounselor_availabilityder">
                        </select>

                    </div>
                    <span style="color:red;display: none;margin-left: 20px;font-weight: bold;">This slot is already booked!!</span>
                </div>   
                <br><br>
                <div class="iwj-respon-msg iwj-hide"></div>
                <div class="iwj-button-loader">

                    <button type="button" id="reschedule_confirm_btn" style="background: rgb(50, 158, 210);padding: 8px 20px; margin-top: 20px" name="register" class="iwj-btn iwj-btn-primary iwj-btn-full iwj-btn-large iwj-register-btn">Confirm</button>
                </div>
            </form>
        </div>
    </div>
</div>

<div id="iwj-approve-popup" class="modal-popup modal fade iwj-register-form-popup">
    <div class="modal-dialog">
        <div class="modal-header">
            <h4 class="modal-title">Approve Counselor</h4>
            <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
        </div>
        <div class="modal-content col-md-12" style="border-radius: 0px;padding: 25px">
            <form action="./index.html" method="post"  id="reschedule_purchase_code"  class="iwj-form-2 iwj-login-form1">

                <input type="hidden" value="" id="con_id">

                <div class="iwjmb-field iwjmb-text-wrapper  required">
                    <div class="iwjmb-label">
                        <label class="theme-color" for="your_name">Counselor Name *</label>
                    </div>
                    <div class="iwjmb-input ui-sortable">
                        <input size="30" disabled="" placeholder="Enter your name" value="" type="text" id="con_username" name="username">
                    </div>
                </div>

                <div class="iwjmb-field iwjmb-text-wrapper  required">
                    <div class="iwjmb-label">
                        <label class="theme-color" for="your_name">Renumeration *</label>
                    </div>
                    <div class="iwjmb-input ui-sortable">
                        <input style="width:25%" placeholder="Amount" value="" type="text" id="con_amount" class="iwjmb-text " name="username"> <label>per</label>
                        <input style="width:25%" size="30" placeholder="hours" value="" type="text" id="con_hours" class="iwjmb-text " name="username"><label >Minutes</label>
                    </div>
                </div>
                <div class="iwjmb-field iwjmb-select_advanced-wrapper  required">
                    <div class="iwjmb-label">
                        <label style="color: #ff8e16;" for="frequency">Counselor Working Days</label>
                    </div>
                    <div class="iwjmb-input">
                        <select id="language" multiple data-options="{&quot;allowClear&quot;:false,&quot;width&quot;:&quot;none&quot;,&quot;placeholder&quot;:&quot;&quot;,&quot;minimumResultsForSearch&quot;:-1}" required class="iwjmb-select_advanced" name="languages[]">

                            <option value="sunday"  selected="">Sunday</option>
                            <option value="monday">Monday</option>
                            <option value="tuesday" >Tuesday</option>
                            <option value="wednesday">Wednesday</option>
                            <option value="thursday">Thursday</option>
                            <option value="friday" >Friday</option>
                            <option value="saturday" >Saturday</option>
                        </select>
                    </div>
                </div>  
                <div class="iwjmb-field iwjmb-text-wrapper  required">
                    <div class="iwjmb-label">
                        <label class="theme-color" for="your_name">Counselor Slots *</label>
                    </div>
                    <div class="iwjmb-input ui-sortable" style="margin-bottom: 10px">
                        <table class="form-table" id="customFields">
                            <tr valign="top">
                                <th scope="row"><label for="customFieldName">Slot - 1</label></th>
                                <td>
                                    <input style="width:25%" type="text" class="iwjmb-text timepicker " id="customFieldName" name="customFieldName[]" value="" placeholder="Starts" /> &nbsp;
                                    <input style="width:25%" type="text" class="iwjmb-text timepicker" id="customFieldValue" name="customFieldValue[]" value="" placeholder="Ends" /> &nbsp;
                                    <a href="javascript:void(0);" class="addCF">Add</a>
                                </td>
                            </tr>
                        </table>
                    </div>

                </div>

                <div class="iwjmb-field iwjmb-text-wrapper  required">
                    <div class="iwjmb-label"><label class="theme-color" for="_iwj_address">Status *</label></div>
                    <div class="iwjmb-input ui-sortable">
                        <select data-options="{&quot;allowClear&quot;:false,&quot;width&quot;:&quot;none&quot;,&quot;placeholder&quot;:&quot;&quot;,&quot;minimumResultsForSearch&quot;:-1}" required id="con_status" class="iwjmb-select_advanced" name="status">
                            <option >Please Select</option>
                            <option value="pending"  >Pending</option>
                            <option value="approve">Approve</option>
                            <option value="disapprove">Disapprove</option>

                        </select>
                    </div>
                </div> 


                <div class="iwj-respon-msg iwj-hide"></div>
                <div class="iwj-button-loader">
                    <button type="button" id="approve_submit" name="register" class="iwj-btn iwj-btn-primary iwj-btn-full iwj-register-btn">Submit</button>
                </div>


            </form>

        </div>
    </div>
</div>
<script data-skip-moving="true"> 
        (function(w,d,u,b){
                s=d.createElement('script');r=(Date.now()/60000|0);s.async=1;s.src=u+'?'+r;
                h=d.getElementsByTagName('script')[0];h.parentNode.insertBefore(s,h);
        })(window,document,'https://cdn.bitrix24.in/b5553359/crm/site_button/loader_4_tb461y.js');
</script>

<link rel='stylesheet' id='iw-testimonials-css'  href='<?php echo base_url(); ?>plugins/inwave-common/assets/css/iw-testimonials.css?ver=2.0.0' type='text/css' media='all' />
<link rel='stylesheet' id='slick-css'  href='<?php echo base_url(); ?>plugins/inwave-common/assets/css/slick.css?ver=2.0.0' type='text/css' media='all' />
<link rel='stylesheet' id='bootstrap-multiselect-css'  href='<?php echo base_url(); ?>plugins/iwjob/includes/class/fields/assets/css/bootstrap-multiselect.css?ver=4.8.2' type='text/css' media='all' />
<link rel='stylesheet' id='iwjmb-taxonomy2-css'  href='<?php echo base_url(); ?>plugins/iwjob/includes/class/fields/assets/css/taxonomy2.css?ver=4.8.2' type='text/css' media='all' />
<link rel='stylesheet' id='iwjmb-select2-css'  href='<?php echo base_url(); ?>plugins/iwjob/includes/class/fields/assets/css/select2.css?ver=4.0.1' type='text/css' media='all' />
<link rel='stylesheet' id='iwjmb-select-advanced-css'  href='<?php echo base_url(); ?>plugins/iwjob/includes/class/fields/assets/css/select-advanced.css?ver=4.8.2' type='text/css' media='all' />
<link rel='stylesheet' id='iwjmb-select-css'  href='<?php echo base_url(); ?>plugins/iwjob/includes/class/fields/assets/css/select.css?ver=4.8.2' type='text/css' media='all' />
<script type='text/javascript'>
    /* <![CDATA[ */
    var wpcf7 = {"apiSettings": {"root": ".\/wp-json\/contact-form-7\/v1", "namespace": "contact-form-7\/v1"}, "recaptcha": {"messages": {"empty": "Please verify that you are not a robot."}}};
    /* ]]> */
</script>
<script type='text/javascript' src='<?php echo base_url(); ?>plugins/contact-form-7/includes/js/scripts.js?ver=4.9'></script>
<script type='text/javascript'>
    /* <![CDATA[ */
    var ajax_posts = {"ajaxurl": ".\/user\/register", "noposts": "No older posts found"};
    /* ]]> */
</script>
<script type='text/javascript' src='<?php echo base_url(); ?>plugins/inwave-common/assets/js/iw-shortcodes.js?ver=2.0.0'></script>
<script type='text/javascript' src='<?php echo base_url(); ?>js/underscore.min.js?ver=1.8.3'></script>
<script type='text/javascript' src='<?php echo base_url(); ?>js/shortcode.min.js?ver=4.8.2'></script>
<script type='text/javascript' src='<?php echo base_url(); ?>js/backbone.min.js?ver=1.2.3'></script>
<script type='text/javascript'>
    /* <![CDATA[ */
    var _wpUtilSettings = {"ajax": {"url": "\/wordpress\/wp-admin\/admin-ajax.php"}};
    /* ]]> */
</script>
<script type='text/javascript' src='<?php echo base_url(); ?>js/wp-util.min.js?ver=4.8.2'></script>
<script type='text/javascript' src='<?php echo base_url(); ?>js/wp-backbone.min.js?ver=4.8.2'></script>
<script type='text/javascript'>
    /* <![CDATA[ */
    var _wpMediaModelsL10n = {"settings": {"ajaxurl": "\/wordpress\/wp-admin\/admin-ajax.php", "post": {"id": 0}}};
    /* ]]> */
</script>
<script type='text/javascript' src='<?php echo base_url(); ?>js/media-models.min.js?ver=4.8.2'></script>
<script type='text/javascript'>
    /* <![CDATA[ */
    var pluploadL10n = {"queue_limit_exceeded": "You have attempted to queue too many files.", "file_exceeds_size_limit": "%s exceeds the maximum upload size for this site.", "zero_byte_file": "This file is empty. Please try another.", "invalid_filetype": "Sorry, this file type is not permitted for security reasons.", "not_an_image": "This file is not an image. Please try another.", "image_memory_exceeded": "Memory exceeded. Please try another smaller file.", "image_dimensions_exceeded": "This is larger than the maximum size. Please try another.", "default_error": "An error occurred in the upload. Please try again later.", "missing_upload_url": "There was a configuration error. Please contact the server administrator.", "upload_limit_exceeded": "You may only upload 1 file.", "http_error": "HTTP error.", "upload_failed": "Upload failed.", "big_upload_failed": "Please try uploading this file with the %1$sbrowser uploader%2$s.", "big_upload_queued": "%s exceeds the maximum upload size for the multi-file uploader when used in your browser.", "io_error": "IO error.", "security_error": "Security error.", "file_cancelled": "File canceled.", "upload_stopped": "Upload stopped.", "dismiss": "Dismiss", "crunching": "Crunching\u2026", "deleted": "moved to the trash.", "error_uploading": "\u201c%s\u201d has failed to upload."};
    /* ]]> */
</script>
<script type='text/javascript' src='<?php echo base_url(); ?>js/plupload/wp-plupload.min.js?ver=4.8.2'></script>




<script type='text/javascript' src='<?php echo base_url(); ?>js/jquery/ui/core.min.js?ver=1.11.4'></script>
<script type='text/javascript' src='<?php echo base_url(); ?>js/jquery/ui/widget.min.js?ver=1.11.4'></script>
<script type='text/javascript' src='<?php echo base_url(); ?>js/jquery/ui/mouse.min.js?ver=1.11.4'></script>
<script type='text/javascript' src='<?php echo base_url(); ?>js/jquery/ui/sortable.min.js?ver=1.11.4'></script>
<script type='text/javascript'>
    /* <![CDATA[ */
    var mejsL10n = {"language": "en-US", "strings": {"Close": "Close", "Fullscreen": "Fullscreen", "Turn off Fullscreen": "Turn off Fullscreen", "Go Fullscreen": "Go Fullscreen", "Download File": "Download File", "Download Video": "Download Video", "Play": "Play", "Pause": "Pause", "Captions\/Subtitles": "Captions\/Subtitles", "None": "None", "Time Slider": "Time Slider", "Skip back %1 seconds": "Skip back %1 seconds", "Video Player": "Video Player", "Audio Player": "Audio Player", "Volume Slider": "Volume Slider", "Mute Toggle": "Mute Toggle", "Unmute": "Unmute", "Mute": "Mute", "Use Up\/Down Arrow keys to increase or decrease volume.": "Use Up\/Down Arrow keys to increase or decrease volume.", "Use Left\/Right Arrow keys to advance one second, Up\/Down arrows to advance ten seconds.": "Use Left\/Right Arrow keys to advance one second, Up\/Down arrows to advance ten seconds."}};
    var _wpmejsSettings = {"pluginPath": "\/wordpress\/wp-includes\/js\/mediaelement\/"};
    /* ]]> */
</script>
<script type='text/javascript' src='<?php echo base_url(); ?>js/mediaelement/mediaelement-and-player.min.js?ver=2.22.0'></script>
<script type='text/javascript' src='<?php echo base_url(); ?>js/mediaelement/wp-mediaelement.min.js?ver=4.8.2'></script>
<script type='text/javascript' src='<?php echo base_url(); ?>js/media-views.min.js?ver=4.8.2'></script>
<script type='text/javascript' src='<?php echo base_url(); ?>js/media-editor.min.js?ver=4.8.2'></script>
<script type='text/javascript' src='<?php echo base_url(); ?>js/media-grid.js?ver=4.8.2'></script>
<script type='text/javascript' src='<?php echo base_url(); ?>plugins/iwjob/includes/class/fields/assets/js/clone.js?ver=4.8.2'></script>
<script type='text/javascript'>
    /* <![CDATA[ */
    var iwj = {"site_url": ".", "base_url": ".", "ajax_url": ".\/register", "security": "112c25d382", "query_vars": "{\"page\":\"\",\"pagename\":\"home-v3\"}", "map_styles": "", "total_email_queue": "0"};
    /* ]]> */
</script>
<script type='text/javascript' src='<?php echo base_url(); ?>plugins/iwjob/assets/js/script.js?ver=4.8.2'></script>
<script type='text/javascript' src='https://www.google.com/recaptcha/api.js?onload=iwj_recaptcha&#038;render=explicit&#038;ver=4.8.2'></script>
<script type='text/javascript' src='<?php echo base_url(); ?>plugins/iwjob/assets/js/iwj_serialize_form_json.js?ver=169'></script>
<script type='text/javascript' src='<?php echo base_url(); ?>plugins/iwjob/assets/js/iwj_cookie.js?ver=169'></script>
<script type='text/javascript' src='<?php echo base_url(); ?>plugins/iwjob/assets/js/iwj_filter_common.js?ver=169'></script>
<script type='text/javascript' src='<?php echo base_url(); ?>plugins/iwjob/assets/js/filter_jobs.js?ver=169'></script>
<script type='text/javascript' src='<?php echo base_url(); ?>plugins/iwjob/assets/js/filter_candidates.js?ver=169'></script>
<script type='text/javascript' src='<?php echo base_url(); ?>plugins/iwjob/assets/js/filter_employers.js?ver=169'></script>
<script type='text/javascript' src='<?php echo base_url(); ?>plugins/iwjob/assets/js/show_less_more.js?ver=169'></script>
<script type='text/javascript' src='<?php echo base_url(); ?>plugins/iwjob/assets/js/ajax_pagination.js?ver=169'></script>
<script type='text/javascript' src='<?php echo base_url(); ?>plugins/iwjob/assets/js/widget_filter_search.js?ver=169'></script>
<script type='text/javascript' src='<?php echo base_url(); ?>plugins/iwjob/assets/js/send_ajax_mail_queue.js?ver=169'></script>
<script type='text/javascript' src='<?php echo base_url(); ?>plugins/iwjob/assets/js/filter_alpha.js?ver=169'></script>
<script type='text/javascript' src='<?php echo base_url(); ?>plugins/iwjob/assets/js/switch_layout.js?ver=169'></script>
<script type='text/javascript' src='<?php echo base_url(); ?>plugins/iwjob/assets/js/iwj_filter_box.js?ver=169'></script>
<script type='text/javascript' src='<?php echo base_url(); ?>plugins/iwjob/assets/js/jquery.matchHeight.js?ver=169'></script>
<script type='text/javascript' src='<?php echo base_url(); ?>plugins/iwjob/assets/js/jquery.bxslider.js?ver=169'></script>
<script type='text/javascript' src='<?php echo base_url(); ?>plugins/js_composer/assets/lib/bower/isotope/dist/isotope.pkgd.min.js?ver=5.2.1'></script>
<script type='text/javascript' src='<?php echo base_url(); ?>plugins/iwjob/assets/fancybox/jquery.fancybox.js?ver=169'></script>
<script type='text/javascript' src='<?php echo base_url(); ?>plugins/iwjob/assets/js/star-rating.js?ver=169'></script>

<script type='text/javascript' src='<?php echo base_url(); ?>assets/js/select2.full.min.js?ver=2.0.0'></script>
<script type='text/javascript' src='<?php echo base_url(); ?>assets/js/jquery.fitvids.js?ver=2.0.0'></script>
<script type='text/javascript' src='<?php echo base_url(); ?>assets/js/owl.carousel.min.js?ver=2.0.0'></script>
<script type='text/javascript' src='<?php echo base_url(); ?>assets/js/theia-sticky-sidebar.js?ver=2.0.0'></script>
<script type='text/javascript' src='<?php echo base_url(); ?>assets/js/jquery.enscroll.min.js?ver=2.0.0'></script>
<script type='text/javascript'>
    /* <![CDATA[ */
    var inwaveCfg = {"siteUrl": ".\/wp-admin\/", "themeUrl": ".\/wp-content\/themes\/injob", "baseUrl": ".", "ajaxUrl": ".\/wp-admin\/admin-ajax.php"};
    /* ]]> */
</script>
<script type='text/javascript' src='<?php echo base_url(); ?>assets/js/template.js?ver=2.0.0'></script>
<script type='text/javascript' src='<?php echo base_url(); ?>assets/js/jquery.navgoco.js?ver=2.0.0'></script>
<script type='text/javascript' src='<?php echo base_url(); ?>assets/js/off-canvas.js?ver=2.0.0'></script>
<script type='text/javascript' src='<?php echo base_url(); ?>js/wp-embed.min.js?ver=4.8.2'></script>
<script type='text/javascript' src='<?php echo base_url(); ?>plugins/js_composer/assets/js/dist/js_composer_front.min.js?ver=5.2.1'></script>
<script type='text/javascript' src='<?php echo base_url(); ?>assets/js/jquery.countTo.js?ver=2.0.0'></script>
<script type='text/javascript' src='<?php echo base_url(); ?>plugins/js_composer/assets/lib/waypoints/waypoints.min.js?ver=5.2.1'></script>
<script type='text/javascript' src='<?php echo base_url(); ?>plugins/inwave-common/assets/js/iw-testimonials.js?ver=2.0.0'></script>
<script type='text/javascript' src='<?php echo base_url(); ?>plugins/inwave-common/assets/js/slick.min.js?ver=2.0.0'></script>
<script type='text/javascript' src='<?php echo base_url(); ?>assets/js/mailchimp.js?ver=2.0.0'></script>
<script type='text/javascript' src='<?php echo base_url(); ?>plugins/iwjob/includes/class/fields/assets/js/bootstrap-multiselect.js?ver=4.8.2'></script>
<script type='text/javascript' src='<?php echo base_url(); ?>plugins/iwjob/includes/class/fields/assets/js/taxonomy2.js?ver=4.8.2'></script>


<script type='text/javascript' src='<?php echo base_url(); ?>js/sweet-alert.js'></script>
<script type='text/javascript' src='<?php echo base_url(); ?>js/moment.min.js'></script>

<script type='text/javascript' src='<?php echo base_url(); ?>js/bootstrap-datetimepicker.js'></script>
<script type='text/javascript' src='<?php echo base_url(); ?>plugins/iwjob/includes/class/fields/assets/js/select2.min.js?ver=4.8.2'></script>
<script type='text/javascript' src='<?php echo base_url(); ?>plugins/iwjob/includes/class/fields/assets/js/select.js?ver=4.8.2'></script>
<script type='text/javascript' src='<?php echo base_url(); ?>plugins/iwjob/includes/class/fields/assets/js/select-advanced.js?ver=4.8.2'></script>
<script type='text/javascript' src='<?php echo base_url(); ?>js/jquery.dataTables.min.js'></script>
<script type='text/javascript' src='<?php echo base_url(); ?>js/dataTables.bootstrap4.min.js'></script>
<script src="<?php echo base_url(); ?>js/bootstrapValidator.min.js"></script>
<script src="<?php echo base_url(); ?>js/fullcalendar.min.js"></script>
<script src='<?php echo base_url(); ?>js/bootstrap-colorpicker.min.js'></script>
<script type='text/javascript'>

    $(function () {
        $('#datetimepicker12').datetimepicker();

    });

    $('.datetimepicker_re').datetimepicker({useCurrent: false, format: 'YYYY-MM-DD', minDate: new Date()}).on('dp.change', function (e) {

        var formatedValue = e.date.format(e.date._f);

        var weekday = ["sunday", "monday", "tuesday", "wednesday", "thursday", "friday", "saturday"];
        var a = new Date(e.date);
        var day = (weekday[a.getDay()]);
        var dateObject = $('.datetimepicker_re').data('date')

        $('#schedule_date').val(dateObject);


        $.ajax({
            type: "POST",
            url: "<?php echo base_url() ?>admin/checkCounselorWorkingDay",

            data: {day: day, user_id: $('.user_id').val(), },
            success: function (data) {
                if (data == 1) {
                    $(".datetimepicker_re").css('border', '2px solid red');
                    $(".datetimepicker_re").parent().next('span').show();
                } else {
                    $(".datetimepicker_re").css('border', '');
                    $(".datetimepicker_re").parent().next('span').hide();
                }
            }
        });

    });

    $('#reschedule_id').on('change', function () {
        var reschedule_id = $(this).val();
        var date = $('#schedule_date').val();

        $.ajax({
            url: '<?php echo base_url(); ?>admin/checkSchedule',
            data: {id: reschedule_id, date: date},
            type: 'post',
            success: function (data) {
                if (data == 1) {


                    $("#reschedule_id").siblings(".select2-container").css('border', '2px solid red');
                    $("#reschedule_id").parent().next('span').show();
                } else {
                    $("#reschedule_id").siblings(".select2-container").css('border', '');
                    $("#reschedule_id").parent().next('span').hide();
                }

            }
        });
    });


    $(document).ready(function () {
        $('ul li a').on('click', function () {
            $(this).closest('ul').find('a').removeClass('is-active');
            $(this).addClass('is-active');
            return false;
        });
        $('#reg_btn_1').on('click', function () {

            var ret = true;


            if ($('#reg_username').val() == "") {

                $('#reg_username').parent().parent().addClass("error");
                $('#reg_username').attr("placeholder", "Please enter your username");
                ret = false;
            }
            if (!isEmail($('#reg_email').val())) {

                $('#reg_email').parent().parent().addClass("error");
                $('#reg_email').attr("placeholder", "Please enter a valid email address");
                ret = false;
            }
            if ($('#reg_password').val() == "") {

                $('#reg_password').parent().parent().addClass("error");
                $('#reg_password').attr("placeholder", "Please enter your password");
                ret = false;
            }
            if ($('#reg_conpassword').val() == "" || $('#reg_conpassword').val() != $('#reg_password').val()) {

                $('#reg_conpassword').parent().parent().addClass("error");
                $('#reg_conpassword').attr("placeholder", "Please check your password");
                ret = false;
            }
            if (ret != false) {
                var ref_this = $("ul.tabs li");

                ref_this.find('a').removeClass('is-active');
                ref_this.next('li:first').find('a').addClass('is-active');
                $('.reg_1').hide();
                $('.reg_2').show();
                return false;
            }
        });
        $('#reg_btn_2').on('click', function () {
            var ret = true;


            if ($('#reg_phone').val() == "") {

                $('#reg_phone').parent().parent().addClass("error");
                $('#reg_phone').attr("placeholder", "Please enter your phone number");
                ret = false;
            }
            if (ret != false) {
                var ref_this = $("ul.tabs li a.is-active");

                ref_this.removeClass('is-active');
                ref_this.parent().next('li:first').find('a').addClass('is-active');
                $('.reg_2').hide();
                $('.reg_3').show();
                return false;
            }

        });
        $('#reg_btn_3').on('click', function () {
            var ref_this = $("ul.tabs li a.is-active");

            ref_this.removeClass('is-active');
            ref_this.parent().next('li:first').find('a').addClass('is-active');
            $('.reg_3').hide();
            $('.reg_4').show();
            return false;
        });

        $('#reg_btn_4').on('click', function () {
            var ref_this = $("ul.tabs li a.is-active");

            ref_this.removeClass('is-active');
            ref_this.parent().next('li:first').find('a').addClass('is-active');
            $('.reg_4').hide();
            $('.reg_5').show();
            return false;
        });
    });


    function toggle_visibility(id) {

        if (id == 'individual') {
            $('#individual_form').show();
            $('#organization_form').hide();
            $(".qty_select").html('')
            $(".qty_select").append('<option value="1">1</option>');
            $('#indivudual_a').removeClass('style1');
            $('#organization_a').addClass('style1');
        } else {
            $(".qty_select").html('')
            $('#individual_form').hide();
            $(".qty_select").append('<option value="1">1</option><option value="2">2</option><option value="3">3</option><option value="4">4</option><option value="5">5</option>');
            $('#organization_form').show();
            $('#organization_a').removeClass('style1');
            $('#indivudual_a').addClass('style1');
        }

    }
    $(document).ready(function () {
        $("input[name$='radio_email']").click(function () {
            var id = $(this).val();
            if (id == 'email') {
                $('#purchaseEmail').show();
                $('#purchaseMobile').hide();
            } else {
                $('#purchaseEmail').hide();
                $('#purchaseMobile').show();
            }
        });
    });

    $(document).ready(function () {



        $('#user_login').submit(function (e) {
            var ret = true;
            if (!isEmail($('#login_email').val())) {

                $('#login_email').parent().parent().addClass("error");
                $('#login_email').attr("placeholder", "Please enter a valid email address");
                ret = false;
            }

            if ($('#login_password').val() == "") {

                $('#login_password').parent().parent().addClass("error");
                $('#login_password').attr("placeholder", "Please enter your password");
                ret = false;
            }
            if (ret != false) {
                $.ajax({
                    url: $(this).attr('action'),
                    data: $(this).serialize(),
                    type: 'post',
                    success: function (data) {
                        if (data == 1) {
                            window.location.href = '<?php echo base_url(); ?>user/profile';
                        } else {
                            $('#error_msg_login').text('Username or Passwird you entered is incorrect!!!').show();
                        }
                    }
                });
            }

            e.preventDefault();
        });
    });
    function isEmail(email) {
        var regex = /^([a-zA-Z0-9_.+-])+\@(([a-zA-Z0-9-])+\.)+([a-zA-Z0-9]{2,4})+$/;
        return regex.test(email);
    }


    function delUser(id) {
        $.ajax({
            url: '<?php echo base_url(); ?>admin/delUser',
            data: {id: id},
            type: 'post',
            success: function (data) {
                if (data == 1) {
                    window.location.href = '<?php echo base_url(); ?>admin/userlist';
                } else {
                    $('#error_msg_login').text('Username or Passwird you entered is incorrect!!!').show();
                }
            }
        });
    }
    function approveUser(id) {
        $.ajax({
            url: '<?php echo base_url(); ?>admin/getUserDetails',
            data: {id: id},
            type: 'post',
            dataType: "json",
            success: function (res) {
                console.log(res);
                if (res.data != "") {
                    var val = res.data.days;
                    var sel = JSON.parse(val);
                    var records = res.timing;

                    $('#con_username').val(res.data.username);
                    $('#con_amount').val(res.data.amount);
                    $('#con_hours').val(res.data.minutes);
                    $('#con_status').val(res.data.status).trigger('change');
                    $('#con_id').val(res.data.id);
                    $('#language').val(sel).trigger('change');

                    if (records.length > 0) {

                        $.each(records, function (index, value) {
                            var tds = $("#customFields").children('tbody').children('tr').children('td').length;

                            $("#customFields").append('<tr valign="top"><th scope="row"><label for="customFieldName">Slot - ' + (parseInt(tds) + 1) + '</label></th><td><input style="width:25%" type="text" class="iwjmb-text timepicker" id="customFieldName" name="customFieldName[]" value="' + value.time_starts + '" placeholder="Starts" /> &nbsp; <input style="width:25%" type="text" class="iwjmb-text timepicker" id="customFieldValue" name="customFieldValue[]" value="' + value.time_ends + '" placeholder="Ends" /> &nbsp; <a href="javascript:void(0);" class="remCF">Remove</a></td></tr>');

                        });
                    }
                    $('#iwj-approve-popup').modal('show')
                } else if (res.user != "") {
                    $('#con_username').val(res.user.username);
                    $('#con_status').val(res.user.status).trigger('change');
                    $('#con_id').val(res.user.id);
                    $('#iwj-approve-popup').modal('show')
                }


            }
        });



    }

    $('#approve_submit').on('click', function () {
        var user_id = $('#con_id').val();
        var user_status = $('#con_status').val();
        var amount = $('#con_amount').val();
        var hours = $('#con_hours').val();
        var days = $('#language').val();


        var text = new Array();
        $("input[name='customFieldName[]']").each(function () {

            text.push({
                starts: $(this).val(),
                ends: $(this).parent().find("input[name='customFieldValue[]']").val(),
            });

        });


        $.ajax({
            url: '<?php echo base_url(); ?>admin/approveUser',
            data: {id: user_id, user_status: user_status, amount: amount, hours: hours, timing: text, days: days},
            type: 'post',
            success: function (data) {
                console.log(data)
                if (data == 1) {
                    window.location.href = '<?php echo base_url(); ?>admin/counselorlist';
                } else {
                    $('#error_msg_login').text('Username or Passwird you entered is incorrect!!!').show();
                }
            }
        });
    });
</script>
<script type='text/javascript'>
    $(document).ready(function () {
        $('ul li a').on('click', function () {
            $(this).closest('ul').find('a').removeClass('is-active');
            $(this).addClass('is-active');
            return false;
        });
        $('#join_btn1').on('click', function () {
            var elm = $('#language');
            $(elm).select2()
            var ref_this = $("ul.tabs li");
            ref_this.find('a').removeClass('is-active');
            ref_this.next('li:first').find('a').addClass('is-active');
            $('.join_1').hide();
            $('.join_2').show();
            return false;
        });
        $('#join_btn2').on('click', function () {
            var ref_this = $("ul.tabs li a.is-active");
            ref_this.removeClass('is-active');
            ref_this.parent().next('li:first').find('a').addClass('is-active');
            $('.join_2').hide();
            $('.join_3').show();
            return false;
        });


    });
//    
// $(document).ready(function () {
//    $('ul.tags').find('input:checkbox').on('click', function (e) {
//        e.preventDefault();
//        if ($(this).prop("checked")) {
//            var chk=$(this).val();
//            
//            
//            
//        } else{
//           $('.results > li').show();
//        }
//    });
//});

    function getPhoneFilterOptions() {
        var opts_1 = [];
        var opts1 = [];
        var opts = [];

        $checkboxes.each(function () {
            if (this.name == "iwj_cat") {
                if (this.checked) {
                    opts_1.push(this.value);
                }
            } else {
                if (this.checked) {
                    opts1.push(this.value);
                }
            }

        });

        return opts_1 + '-' + opts1;
    }

    function updatePhones(opts, opts1) {
        var tbl_body = "";
        var tbl_row;

        $.ajax({
            type: "POST",
            url: "<?php echo base_url() ?>user/get_counselor",
            dataType: 'json',
            cache: false,
            data: {filterOpts: opts, filterOpts1: opts1},
            success: function (records) {
                console.log(records.length);
                tbl_row = "";
                if (records.length > 0) {

                    $.each(records, function (index, value) {

                        tbl_row += '<div class="grid-content"  data-id="1346"><div class="job-item featured-item">\n\
                                              <div class="job-image"><img alt="AOEVN" src="<?php echo base_url() ?>uploads/images/' + value.photo + '" class="avatar avatar-full photo" width="408" height="401" /></div>\n\
                                                <div class="job-info"><h3 class="job-title"><a href="employee.html">' + value.username + '</a></h3>\n\
                                                     <div class="info-company"><div class="company"><i class="fa fa-suitcase"></i>\n\
                                                        <a href="employee.html">' + value.domain + '</a></div><div class="address"><i class="fa fa-cogs"></i>' + value.experience + ' years Exp</div>\n\
                                                             <div class="sallary"><i class="fa fa-inr"></i>Rs 900 - Rs 1,500</div></div>\n\
                                                             <span class="job-featured"><a class="type-name" href="employee.html" >Details</a></span>\n\
                                                                <div class="job-type full-time"><a class="type-name" href="employee.html">Schedule</a>\n\
                                                            </div></div><div class="job-featured"></div></div></div>';

                    });
                } else {
                    tbl_row += '<div class="alert"><span class="closebtn" onclick="this.parentElement.style.display="none";">&times;</span> No Data Found</div>';
                }
                $("#cn_content").html(tbl_row);
            }
        });
    }

    var $checkboxes = $("input:checkbox");
    $checkboxes.on("change", function (e) {
        e.preventDefault();
        var opts = getPhoneFilterOptions();
        var arr = opts.split('-');

        updatePhones(arr[0], arr[1]);
    });

</script>



<script>
    function onSuccess(googleUser) {
        var profile = googleUser.getBasicProfile();
        gapi.client.load('plus', 'v1', function () {
            var request = gapi.client.plus.people.get({
                'userId': 'me'
            });
            //Display the user details
            request.execute(function (resp) {
                var profileHTML = '<div class="profile"><div class="head">Welcome ' + resp.name.givenName + '! <a href="javascript:void(0);" onclick="signOut();">Sign out</a></div>';
                profileHTML += '<img src="' + resp.image.url + '"/><div class="proDetails"><p>' + resp.displayName + '</p><p>' + resp.emails[0].value + '</p><p>' + resp.gender + '</p><p>' + resp.id + '</p><p><a href="' + resp.url + '">View Google+ Profile</a></p></div></div>';
                $('.userContent').html(profileHTML);
                $('#gSignIn').slideUp('slow');
            });
        });
    }
    function onFailure(error) {
        alert(error);
    }
    function renderButton() {
        gapi.signin2.render('gSignIn', {
            'scope': 'profile email',
            'width': 150,
            'height': 50,
            'longtitle': false,
            'theme': 'dark',
            'onsuccess': onSuccess,
            'onfailure': onFailure
        });
    }
    function signOut() {
        var auth2 = gapi.auth2.getAuthInstance();
        auth2.signOut().then(function () {
            $('.userContent').html('');
            $('#gSignIn').slideDown('slow');
        });
    }
    function show_popup() {
        $('#modal-1').modal('hide')
        $('#iwj-login-popup').modal()


    }

    $('#schedule_id').on('change', function () {
        var schedule_id = $(this).val();
        var date = $('#start').val();
        $.ajax({
            url: '<?php echo base_url(); ?>admin/checkSchedule',
            data: {id: schedule_id, date: date},
            type: 'post',
            success: function (data) {
                if (data == 1) {


                    $("#schedule_id").siblings(".select2-container").css('border', '2px solid red');
                    $("#schedule_id").parent().next('span').show();
                } else {
                    $("#schedule_id").siblings(".select2-container").css('border', '');
                    $("#schedule_id").parent().next('span').hide();
                }

            }
        });
    });
    $(document.body).on('hidden.bs.modal', function () {
        $("#schedule_id").siblings(".select2-container").css('border', '');
        $("#schedule_id").parent().next('span').hide();
    });

    $(document).ready(function () {

//        var email = '<?php //echo $email                                       ?>';
//        var login = '<?php //echo $login                                       ?>';
//        if (login == 1) {
//            $('#login_email').val(email);
//            $('#iwj-login-popup').modal('show')
//        } else {
//            $('#iwj-login-popup').modal('hide')
//        }

        $('.userlist').DataTable();
    });

    $(document).ready(function () {
        $(".addCF").click(function () {
            var tds = $("#customFields").children('tbody').children('tr').children('td').length;

            $("#customFields").append('<tr valign="top"><th scope="row"><label for="customFieldName">Slot - ' + (parseInt(tds) + 1) + '</label></th><td><input style="width:25%" type="text" class="iwjmb-text timepicker" id="customFieldName" name="customFieldName[]" value="" placeholder="Starts" /> &nbsp; <input style="width:25%" type="text" class="iwjmb-text timepicker" id="customFieldValue" name="customFieldValue[]" value="" placeholder="Ends" /> &nbsp; <a href="javascript:void(0);" class="remCF">Remove</a></td></tr>');
        });
        $("#customFields").on('click', '.remCF', function () {
            $(this).parent().parent().remove();
        });
    });
    $(function () {
        $('#datetimepicker1').datetimepicker({format: 'DD/MM/YYYY'});

        $('body').on('focus', ".timepicker", function () {
            $(this).datetimepicker({
                format: 'LT'
            });
        });

    });

    function user_schedule(id) {

        $('#couns_id').val(id)
        getCounselorData(id)
        $('#modal-2').modal('show')
    }

    function code_schedule(id) {
        $('#couns_id').val(id)
        getCounselorData(id)
        $('#modal-1').modal('show')
    }

    function getCounselorData(id) {
        $.ajax({
            url: '<?php echo base_url(); ?>home/getCounselorDetails',
            data: {id: id},
            type: 'post',
            dataType: "json",
            success: function (data) {

                var tbl_row = "";
                var tbl_row_timing = "";
                $('#sche_user').val(data.user)
                $('#sche_username').val(data.username)
                if (data.timing.length > 0) {

                    $.each(data.timing, function (index, value) {
                        var timing = value.time_starts + ' - ' + value.time_ends
                        tbl_row_timing += '<option value=' + value.id + '>' + timing + '</option>';

                    });
                } else {
                    tbl_row_timing += '<div class="alert"><span class="closebtn" onclick="this.parentElement.style.display="none";">&times;</span> No Data Found</div>';
                }
                $("#counselor_select").html(tbl_row);
                $("#counselor_availabilty").html(tbl_row_timing);
            }
        });

    }
    $('#counselor_availabilty').on('change', function () {
        var schedule_id = $(this).val();
        var date = $('.datetimepicker_re').data('date')
        $.ajax({
            url: '<?php echo base_url(); ?>admin/checkSchedule',
            data: {id: schedule_id, date: date},
            type: 'post',
            success: function (data) {
                if (data == 1) {


                    $("#counselor_availabilty").siblings(".select2-container").css('border', '2px solid red');
                    $("#counselor_availabilty").parent().next('span').show();
                } else {
                    $("#counselor_availabilty").siblings(".select2-container").css('border', '');
                    $("#counselor_availabilty").parent().next('span').hide();
                }

            }
        });
    });

    $('#schedule-appointment').on('click', function () {
        var is_valid = $('#sche_valid').val();
        var couns_id = $('#couns_id').val();
        var user_id = $('#sche_user').val();
        var description = $('#user_description').val();
        var start = $('.datetimepicker_re').data('date')
        var schedule_id = $('#counselor_availabilty').val();
        var ret;
        if (is_valid != 0) {
            ret = false;
        }
        if (ret != false) {
            $.ajax({
                url: '<?php echo base_url(); ?>calendar/addEvent',
                data: {con_id: couns_id, user_id: user_id, description: description, start: start, schedule_id: schedule_id},
                type: 'post',
                success: function (data) {
                    if (data == 1) {

                        swal({title: "Success", text: "Your appointment have been scheduled", type: "success"},
                                function () {
                                    location.reload();
                                }
                        );



                    } else {
                        swal("Oops!", "Something went wrong! Please try again later", "error")
                    }
                }
            });
        }
    });

    function checkCodeValid(value) {
        $.ajax({
            url: '<?php echo base_url(); ?>home/checkCodeValid',
            data: {code: value},
            type: 'post',
            success: function (data) {

                if (data != 1) {

                    $('#sche_valid').val(1);
                    swal("Oops!", "Please check the code you have entered", "error")
                } else {
                    $('#sche_valid').val(0);
                    $('#sche_user').val(value)
                }
            }
        });
    }




    // select counselor from their available date
    $('#datetimepicker1').datetimepicker({useCurrent: false, format: 'DD/MM/YYYY'}).on('dp.change', function (e) {
        var formatedValue = e.date.format(e.date._f);

        var weekday = ["sunday", "monday", "tuesday", "wednesday", "thursday", "friday", "saturday"];
        var a = new Date(e.date);
        var day = (weekday[a.getDay()]);


        $.ajax({
            type: "POST",
            url: "<?php echo base_url() ?>home/daydate",
            data: {id: day},
            success: function (record) {

                tbl_row = "";
                if (record.length > 7) {

                    $.each($.parseJSON(record), function (index, value1) {
                        $.each(value1, function (index, value) {

                            tbl_row += '<div class="grid-content"  data-id="1346"><div class="job-item featured-item">\n\
                                <div class="job-image"><img alt="AOEVN" src="<?php echo base_url() ?>uploads/images/' + value.photo + '" class="avatar avatar-full photo" width="408" height="401" /></div>\n\
                                    <div class="job-info"><h3 class="job-title"><a href="employee.html">' + value.username + '</a></h3>\n\
                                        <div class="info-company"><div class="company"><i class="fa fa-suitcase"></i>\n\
                                            <a href="employee.html">' + value.domain + '</a></div><div class="address"><i class="fa fa-cogs"></i>' + value.experience + ' years Exp</div>\n\
                                                <div class="sallary"><i class="fa fa-inr"></i>Rs 900 - Rs 1,500</div></div>\n\
                                                    <span class="job-featured"><a class="type-name" href="employee.html" >Details</a></span>\n\
                                                        <div class="job-type full-time"><a class="type-name" href="employee.html">Schedule</a>\n\
                                                        </div></div><div class="job-featured"></div></div></div>';
                        });

                    });
                } else {

                    tbl_row += '<div class="alert" style= "background-color: #d9edf7;margin-left: 15px;"<span class="closebtn" onclick="this.parentElement.style.display="none";">&times;</span><strong> No Data Found </strong></div>';
                }

                $("#cn_content").html(tbl_row);
            }

        });

    });

    $(function () {

        $('.job-alert-btn').click(function () {

            var fn_text = $(this).text();
            $.ajax({
                url: "<?php echo base_url() ?>home/now",
                data: {text: fn_text},
                type: 'post',
                success: function (record) {
                    console.log(record.length)
                    tbl_row = "";
                    if (record.length > 7) {

                        $.each($.parseJSON(record), function (index, value1) {
                            $.each(value1, function (index, value) {

                                tbl_row += '<div class="grid-content"  data-id="1346"><div class="job-item featured-item">\n\
                                <div class="job-image"><img alt="AOEVN" src="<?php echo base_url() ?>uploads/images/' + value.photo + '" class="avatar avatar-full photo" width="408" height="401" /></div>\n\
                                    <div class="job-info"><h3 class="job-title"><a href="employee.html">' + value.username + '</a></h3>\n\
                                        <div class="info-company"><div class="company"><i class="fa fa-suitcase"></i>\n\
                                            <a href="employee.html">' + value.domain + '</a></div><div class="address"><i class="fa fa-cogs"></i>' + value.experience + ' years Exp</div>\n\
                                                <div class="sallary"><i class="fa fa-inr"></i>Rs 900 - Rs 1,500</div></div>\n\
                                                    <span class="job-featured"><a class="type-name" href="employee.html" >Details</a></span>\n\
                                                        <div class="job-type full-time"><a class="type-name" href="employee.html">Schedule</a>\n\
                                                        </div></div><div class="job-featured"></div></div></div>';
                            });

                        });
                    } else {

                        tbl_row += '<div class="alert" style= "background-color: #d9edf7;margin-left: 15px;"<span class="closebtn" onclick="this.parentElement.style.display="none";">&times;</span><strong> No Data Found </strong></div>';
                    }
                    if ($(this).text() == "Now") {
                        $(this).text("All")
                    } else {
                        $(this).text("Now")
                    }
                    $("#cn_content").html(tbl_row);
                }
            });

        });
    });


    $('#reschedule_submit').on('click', function () {

        var purchase_code = $('#purchase_code').val();
        $('#purchase_code_hdn').val(purchase_code)
        var ret = true;
        if (purchase_code == "") {

            $('#purchase_code').parent().parent().addClass("error");
            $('#purchase_code').attr("placeholder", "Please check your coupon code");
            ret = false;
        }
        if (ret != false) {

            $.ajax({
                type: "POST",
                url: "<?php echo base_url() ?>home/get_scheduled_counselor",
                dataType: 'json',
                cache: false,
                data: {purchase_code: purchase_code},
                success: function (records) {

                    tbl_row = "";
                    if (records.length > 0) {
                        var i = 1;
                        $.each(records, function (index, value) {
                            if (i == 1) {
                                var flag = "active";
                            } else {
                                flag = "";
                            }


                            tbl_row += '<div class="item ' + flag + '"><input type="hidden" value="' + value.current_schedule_id + '" class="sche_id_hdn"><input type="hidden" value="' + value.id + '" class="user_id_hdn"><input type="hidden" value="' + value.username + '" class="user_name_hdn">\n\
                <div class="grid-content" style="background-color:rgb(241, 243, 242);margin-bottom: 10px" data-id="1346">\n\    <div style="padding:0" class="job-item featured-item">\n\
                                              <div class="job-image"><img alt="AOEVN" src="<?php echo base_url() ?>uploads/images/' + value.photo + '" class="avatar avatar-full photo" width="408" height="401" /></div>\n\
                                                <div class="job-info">\n\
<h3 class="job-title">' + value.username + '</h3>\n\
                                                     <div class="info-company">\n\
<div class="company">Scheduled date & time</div>\n\
\n\<div class="sallary"><i class="fa fa-calendar"></i>' + value.date + '</div>\n\
<div class="address"><i class="fa fa-clock-o"></i>' + value.time_starts + ' - ' + value.time_ends + '</div>\n\
                                                         </div></div>\n\
<div class="job-featured">\n\
</div>\n\
</div>\n\
</div>\n\
</div>\n\
';


                            i++;
                        });
                        $("#carousel_inner").html(tbl_row);
                        $('#reschedule_purchase_code').hide();
                        $('#reschedule').show();

                    } else {
                        swal("Oops!", "You have no appointments scheduled", "error")
                    }



                }
            });


            return false;
        }
    });
    $('#reschedule_btn').on('click', function () {

        var schedule_id = $('div .active').find('.sche_id_hdn').val();
        var sche_user_id = $('div .active').find('.user_id_hdn').val();

        var sche_user_name = $('div .active').find('.user_name_hdn').val();
        $('#current_schedule_id').val(schedule_id)
        $('.user_id').val(sche_user_id)
        $.ajax({
            url: '<?php echo base_url(); ?>home/getCounselorDetails',
            data: {id: sche_user_id},
            type: 'post',
            dataType: "json",
            success: function (data) {


                var tbl_row_timing = "";
                $('#re_sche_username').val(sche_user_name)
                if (data.timing.length > 0) {

                    $.each(data.timing, function (index, value) {
                        var timing = value.time_starts + ' - ' + value.time_ends
                        tbl_row_timing += '<option value=' + value.id + '>' + timing + '</option>';

                    });
                } else {
                    tbl_row_timing += '<div class="alert"><span class="closebtn" onclick="this.parentElement.style.display="none";">&times;</span> No Data Found</div>';
                }

                $(".counselor_availabilty").html(tbl_row_timing);

                $('#reschedule').hide();
                $('#reschedule_confirm').show();
                return false;
            }
        });
    });

    $('#reschedule_confirm_btn').on('click', function () {

        var current_schedule_id = $('#current_schedule_id').val();
        var sche_user_id = $('.user_id').val();
        var description = $('#user_re_reason').val();
        var start = $('#datetimepicker_re_popup').data('date')
        var visitor_info = $('#purchase_code_hdn').val()
        var schedule_id = $('.counselor_availabilty').val();
        $.ajax({
            url: '<?php echo base_url(); ?>home/userReschedule',
            data: {id: current_schedule_id, user_id: sche_user_id, description: description, start: start, schedule_id: schedule_id, visitor_info: visitor_info},
            type: 'post',
            dataType: "json",
            success: function (data) {
                if (data == 1) {
                    swal({title: "Success", text: "Your appointment have been rescheduled", type: "success"});
                }
            }
        });
    });

    var ajax_call = function () {
        $.ajax({
            url: '<?php echo base_url(); ?>home/makeCall',

            type: 'post',
            dataType: "json",
            success: function (data) {
                var yourval = jQuery.parseJSON(JSON.stringify(data));

//                if (yourval.Call.Sid != "") {
//                    var get_call = function () {
//                        $.ajax({
//                            url: '<?php //echo base_url(); ?>home/getCall',
//
//                            type: 'post',
//                            dataType: "json",
//                            success: function (data) {
//                                var yourval = jQuery.parseJSON(JSON.stringify(data));
//
//                                console.log(data)
//                            }
//                        });
//                    }
//
//                    var interval = 1000 * 60 * 6; // where X is your every X minutes
//
//                    setInterval(get_call, interval);
//                }
            }
        });
    };

    var interval = 1000 * 60 * 1; // where X is your every X minutes

//setInterval(ajax_call, interval);


</script>
</body>
</html>

