<footer class="iw-footer iw-footer-default">

    <div class="iw-footer-middle">
        <div class="container">
            <div class="row">
                <div class="col-md-12 col-sm-12">
                    <h4 style="color:#FFFFFF;"><center>**Online counselling is not advisable if you are in acute distress or feeling suicidal. Please visit your nearest hospital immediately if you are feeling suicidal or at risk of self-harm.</center></h4>
                </div>
            </div>
            <div class="row">
                <div class="col-lg-6 col-md-6 col-sm-6 col-sx-12"><div class="footer-left"><div class="row"><div class="col-lg-6 col-md-6 col-sm-12 col-sx-12"><div class="inwave-contact-info-2 widget widget_inwave-contact-info"><div class="widget-info-footer no-title"><h3 class="widget-title"></h3><div class="subtitle">
                                            <div class="line1"></div>
                                            <div class="line2"></div>
                                            <div class="clearfix"></div>
                                        </div>            <a class="iw-widget-about-us" href="">
                                            <img src="<?php echo base_url(); ?>assets/images/eYogi_189x48.png" alt=""/>
                                        </a>            <p>Stigma or convenience - online counselling may be the right choice for you. It can open up a whole new way you think about yourself and about others...</p>
                                        <a class="link_page_about_us" onclick="location.href = '<?php echo base_url(); ?>home/howitworks';" style="background-color:#fff;color:#ff9128" >Learn more </a>
                                        <ul class="iw-social-footer-all">
                                            <li><a class="facebook" onclick="window.open('https://www.facebook.com/eyogi.in/')" title="Facebook"><i class="fa iwj-icon-facebook" style="margin-top:4px !important"></i></a></li><li><a class="twitter" onclick="window.open('https://twitter.com/eyogi_in')" title="Twitter"><i class="fa iwj-icon-twitter" style="margin-top:4px !important"></i></a></li><li><a class="google-plus" onclick="window.open('https://plus.google.com/105548168969313762025')" title="Google Plush"><i class="fa iwj-icon-gplus" style="margin-top:4px !important"></i></a></li><li><a class="linkedin" onclick="window.open('https://www.linkedin.com/company/eyogi/')" title="Linkedin"><i class="fa iwj-icon-linkedin" style="margin-top:4px !important"></i></a></li></ul>
                                    </div></div></div><div class="col-lg-6 col-md-6 col-sm-12 col-sx-12">
                                <div class="nav_menu-2 widget widget_nav_menu"><h3 class="widget-title">Quick Links</h3>
                                    <div class="subtitle">
                                        <div class="line1"></div>
                                        <div class="line2"></div>
                                        <div class="clearfix"></div>
                                    </div>
                                    <div class="menu-for-candidates-container">
                                        <ul id="menu-for-candidates" class="menu">

                                            <li id="menu-item-1387" class="menu-item menu-item-type-post_type menu-item-object-page menu-item-1387"><a  onclick="location.href = '<?php echo base_url(); ?>';" style="color:white">Home</a></li>
                                            <li id="menu-item-1389" class="menu-item menu-item-type-post_type menu-item-object-page menu-item-1389"><a onclick="location.href = '<?php echo base_url(); ?>home/schedule';" style="color:white">Schedule</a></li>
                                            <?php if ($this->session->userdata('loggedIn') == true) { 
                                                if ($data1[0]->role_id == 4) {?>    
                                            <li id="menu-item-1386" class="menu-item menu-item-type-post_type menu-item-object-page current-menu-item page_item page-item-141 current_page_item menu-item-1386 selected  "><a onclick="location.href = '<?php echo base_url(); ?>admin/credits';" style="color:white">Code Purchase</a></li>
                                                <?php }else { ?>
                                                    <li id="menu-item-1386" class="menu-item menu-item-type-post_type menu-item-object-page current-menu-item page_item page-item-141 current_page_item menu-item-1386 selected  "><a onclick="location.href = '<?php echo base_url(); ?>home/purchasecode';" style="color:white">Code Purchase</a></li>
                                                <?php } ?>
                                            <?php }else {?>
                                            <li id="menu-item-1386" class="menu-item menu-item-type-post_type menu-item-object-page current-menu-item page_item page-item-141 current_page_item menu-item-1386 selected  "><a onclick="location.href = '<?php echo base_url(); ?>home/purchasecode';" style="color:white">Code Purchase</a></li>
                                            <?php } ?>
                                            <li id="menu-item-1390" class="menu-item menu-item-type-post_type menu-item-object-page menu-item-1390"><a onclick="location.href = 'http://dev.eyogi.in/join';"  style="color:white">Join as a Counsellor</a></li>

                                        </ul>
                                    </div>
                                </div></div></div></div></div>
                <div class="col-lg-6 col-md-6 col-sm-6 col-sx-12">
                    <div class="footer-right">
                        <div class="row">
                            <div class="col-lg-6 col-md-6 col-sm-12 col-sx-12">
                                <div class="nav_menu-3 widget widget_nav_menu"><h3 class="widget-title"></h3><div class="subtitle">
                                        <div class="line1"></div>
                                        <div class="line2"></div>
                                        <div class="clearfix"></div>
                                    </div>
                                    <div class="menu-for-employers-container">
                                        <ul id="menu-for-employers" class="menu">
                                            <li id="menu-item-1390" class="menu-item menu-item-type-post_type menu-item-object-page menu-item-1390"><a onclick="window.open('http://dev.eyogi.in/blog')" style="color:white">Blog</a></li>
                                            <li id="menu-item-1392" class="menu-item menu-item-type-post_type menu-item-object-page menu-item-1392"><a onclick="location.href = '<?php echo base_url(); ?>home/keepintouch';" style="color:white">Contact Us</a></li>
                                            <li id="menu-item-594" class="menu-item menu-item-type-custom menu-item-object-custom menu-item-594"><a onclick="location.href = '<?php echo base_url(); ?>home/aboutus';" style="color:white">About Us</a></li>
                                            <li id="menu-item-1392" class="menu-item menu-item-type-post_type menu-item-object-page menu-item-1392"><a onclick="location.href = ('http://dev.eyogi.in/home/faq')" style="color:white">FAQ</a></li>

                                        </ul>
                                    </div>
                                </div>
                            </div>
                            <div class="col-lg-6 col-md-6 col-sm-12 col-sx-12">
                                <div class="nav_menu-3 widget widget_nav_menu"><h3 class="widget-title"></h3><div class="subtitle">
                                        <div class="line1"></div>
                                        <div class="line2"></div>
                                        <div class="clearfix"></div>
                                    </div>
                                    <div class="menu-for-employers-container">
                                        <ul id="menu-for-employers" class="menu">
                                            <li id="menu-item-1390" class="menu-item menu-item-type-post_type menu-item-object-page menu-item-1390"><a onclick="location.href = ('http://dev.eyogi.in/home/terms_conditions')" style="color:white">Terms & Conditions</a></li>
                                            <li id="menu-item-1392" class="menu-item menu-item-type-post_type menu-item-object-page menu-item-1392"><a onclick="location.href = ('http://dev.eyogi.in/home/privacy')" style="color:white">Privacy Policy</a></li>
                                            <li id="menu-item-1392" class="menu-item menu-item-type-post_type menu-item-object-page menu-item-1392"><a onclick="location.href = ('http://dev.eyogi.in/home/refund')" style="color:white">Refund Policy</a></li>


                                        </ul>
                                    </div>
                                </div>
                            </div>
                            
                        </div>
                    </div>
                </div>    
            
            </div>
        </div>
    </div>

    <!--    <button type="button" class="btn btn-info btn-lg" data-toggle="modal" data-target="#myModal">Call Back</button>-->

    <div class="modal fade" id="myModal" role="dialog">
        <div class="modal-dialog" style="width: 508px">

            <!-- Modal content-->
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal">&times;</button>
                    <h4 class="modal-title">Please Enter Your Details</h4>
                </div>
                <div class="modal-body"  style="padding: 0px">





                    <form action="#" method="post" class="" name="contact2" id="contactForm2" style="padding-top:20px" novalidate="novalidate">

                        <input type="hidden" id="base" name="base" value="<?php echo base_url(); ?>">
                        <div class="form-wrapper iw-contact-form-7 contact-map" style="box-shadow: none;">

                            <div class="field1">

                                <input type="text" id="name13" name="name13" name="" placeholder=" Enter your name">
                            </div>
                            <input type="hidden" id="base1" value="<?php echo base_url() ?>" >
                            <div class="field1">

                                <input type="email" id="email13" name="email13"  placeholder=" Enter your email">
                            </div>
                            <div class="field1">

                                <input type="text" id="phone13" name="phone13"  placeholder=" Enter your Number">
                            </div>
                            <div class="field1">

                                <input type="text" id="message13" name="message13"  placeholder=" Enter message">
                            </div>
                            <div class="submit-button"><input type="submit" value="Send" class="wpcf7-form-control wpcf7-submit" /></div>
                        </div>
                        <div id="succes5"  class="alert alert-success" style="width: 50%; margin-left: 49%;display: none;">  </div>  
                        <div class="wpcf7-response-output wpcf7-display-none"></div>
                    </form>

                    <style type="text/css">

                        .alert-success{
                            background-color: rgb(255, 145, 40)!important;
                        }

                        .iw-contact-form-7.contact-map .field1{

                            border: 1px solid #ff8e16;
                            padding: 10px 12px 12px;
                            margin-bottom: 30px;
                            border-radius: 5px;
                        }

                    </style>


                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                </div>
            </div>

        </div>
    </div>

    <div class="iw-copy-right">
        <div class="container">
            <div class="row">
                <div class="col-md-12 col-sm-12">
                    <p>� <a href='#'>eYogi</a> All rights reserved.</p>
                </div>
            </div>
        </div>
    </div>
</footer>
</div> <!--end .content-wrapper -->
<div id="iwj-login-popup" class="modal-popup modal fade iwj-login-form-popup" style="overflow-x: hidden;overflow-y: auto;">
    <div class="modal-dialog">
        <div class="modal-header">
            <h4 class="modal-title">Login to your account</h4>
            <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
        </div>
        <div class="modal-content">
            <form  id="user_login" action="<?php echo base_url(); ?>user/login"  method="post" class="iwj-form iwj-login-form1">
                <div id="error_msg_login" style="display:none" class="alert alert-danger"></div>
                <div class="iwj-field">
                    <label>User name</label>
                    <div class="iwj-input">
                        <i class="fa fa-user"></i>
                        <input type="text" id="login_email" name="email" placeholder="Enter Your Username.">
                    </div>
                </div>
                <div class="iwj-field">
                    <label>Password</label>
                    <div class="iwj-input">
                        <i class="fa fa-keyboard-o"></i>
                        <input type="password" id="login_password" name="password" placeholder="Enter Password.">
                    </div>
                </div>
                <div class="form-field iwj-button-loader">
                    <input type="hidden" name="redirect_to" value="user/login">
                    <button type="submit" name="login" class="iwj-btn iwj-btn-primary iwj-btn-large iwj-btn-full iwj-login-btn">Login</button>
                </div>
                <div class="text-center lost-password">
                    <a target="_blank" href="<?php echo base_url(); ?>home/lostpass">Lost Password?</a>
                </div>
                <div class="iwj-divider">
                    <span class="line"></span>
                    <span class="circle">Or</span>
                </div>

                <!-- HTML for render Google Sign-In button -->

                <!-- HTML for displaying user details -->
                <!--                <div class="userContent"></div>-->


                <div class="social-login row">
                    <div class="col-md-4">
                        <a href="<?php echo $authUrl ?>" class="iwj-btn iwj-btn-primary iwj-btn-medium iwj-btn-full iwj-btn-icon social-login-facebook"><i class="fa fa-facebook"></i>Facebook</a>
                    </div>
                    <div class="col-md-4">
                        <a  href="<?php echo $loginURL; ?>" class="iwj-btn iwj-btn-primary iwj-btn-medium iwj-btn-full iwj-btn-icon social-login-google"><i class="fa fa-google-plus"></i>Google</a>

                    </div>
                    <div class="col-md-4">
                        <a href="<?php echo $oauthURL; ?>" class="iwj-btn iwj-btn-primary iwj-btn-medium iwj-btn-full iwj-btn-icon social-login-linkedin"><i class="fa fa-linkedin"></i>Linkedin</a>
                    </div>
                </div>                  
                <div class="text-center register-account">
                    You don't have an account? <a href="<?php echo base_url(); ?>user/index">Register</a>
                </div>
            </form>

        </div>
    </div>
</div>
<div id="iwj-balance_check-popup" class="modal-popup modal fade iwj-register-form-popup">
    <div class="modal-dialog">
        <div class="modal-header">
            <h4 class="modal-title">Balance Check</h4>
            <button type="button" id="btn_modal" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
        </div>
        <div class="modal-content">

            <form method="post" id="purchase_code_form" class="iwj-form iwj-register-form">

                <div class="iwj-field">
                    <div class="iwj-input">
                        <input type="text" name="purchase_code" onchange="checkCodeValid(this.value)" id="test" placeholder="Enter Purchase Code">
                    </div>
                </div>

                <div id="cap" class="iwj-field captchadiv" style="display:none;">
                    <div style="margin-left:0px;" class="g-recaptcha" data-sitekey="6Lf6-VkUAAAAANk2ArTsSb7T7wj8GJRXeTTZUJf4"></div>
                    <span id="captcha" style="margin-left:0px;color:red"/>
                </div>


                <div class="iwj-respon-msg iwj-hide"></div>
                <div class="iwj-button-loader">
                    <button type="button" onclick="balcheck()" id="balance_check_submit" name="register" class="iwj-btn iwj-btn-primary iwj-btn-full iwj-btn-medium iwj-register-btn">Submit</button>
                </div>
            </form>

            <form  id="credit_balance" style="display:none"  method="post" class="iwj-form iwj-register-form">


                <div id="credit_balance1" class="text-center">
                    <label style="font-size:16px">Your credit balance is</label>
                    <h1 style="font-weight: 800" class="text-center">100</h1>
                    <h1 style="font-weight: 800" id="expires_on" class="text-center"></h1>
                </div>

                <div class="iwj-respon-msg iwj-hide"></div>


            </form>

        </div>
    </div>
</div>
<div id="user-balance_check-popup" class="modal-popup modal fade iwj-register-form-popup">
    <div class="modal-dialog">
        <div class="modal-header">
            <h4 class="modal-title">Balance Check</h4>
            <button type="button" id="btn_modal" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
        </div>
        <div class="modal-content">



            <form  id="" method="post" class="iwj-form iwj-register-form">


                <div id="credit_balance_user" class="text-center">


                </div>

                <div class="iwj-respon-msg iwj-hide"></div>
                <div class="iwj-button-loader">


                </div>


            </form>

        </div>
    </div>
</div>

<div id="iwj-reschedule-popup" data-backdrop="static" data-keyboard="false" class="modal-popup modal fade iwj-register-form-popup">
    <div class="modal-dialog">
        <div class="modal-header">
            <h4 class="modal-title">Reschedule</h4>
            <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
        </div>
        <div class="modal-content">
            <form method="post"  id="reschedule_purchase_code1"  class="iwj-form iwj-register-form">


                <?php if ($this->session->userdata('userData')) {
                    ?>
                    <div class="iwj-field" style="background: #ebebe4;">

                        <div class="iwj-input">
                            <input type="text" disabled="" name="purchase_code1" value="<?php echo $data1[0]->username; ?>" placeholder="Enter Purchase Code to reschedule">
                            <input type="hidden" value="<?php echo $data1[0]->id; ?>" id="purchase_code" name="purchase_code" placeholder="Enter Purchase Code to reschedule">
                        </div>
                    </div>

                <?php } else { ?>
                    <div class="iwj-field">

                        <div class="iwj-input">
                            <input type="text" id="purchase_code"  name="purchase_code" onchange="checkCodeValid(this.value)" placeholder="Enter Purchase Code to reschedule">
                        </div>
                    </div>

                <?php } ?>




                <div class="iwj-respon-msg iwj-hide"></div>
                <div class="iwj-button-loader">

                    <button type="button" id="reschedule_submit" name="register" class="iwj-btn iwj-btn-primary iwj-btn-full iwj-btn-medium iwj-register-btn">Submit</button>
                </div>


            </form> 
            <form  id="reschedule" style="display:none"  method="post" class="iwj-form iwj-register-form">

                <div class="carousel slide" id="myCarousel">
                    <div class="carousel-inner" id="carousel_inner">

                    </div>

                    <div class="control-box">                            
                        <a data-slide="prev" data-target="#myCarousel" class="carousel-control left">�</a>
                        <a data-slide="next" data-target="#myCarousel" class="carousel-control right">�</a>
                    </div><!-- /.control-box -->   

                </div><!-- /#myCarousel -->
                <!--                <div class="grid-content" style="background-color:rgb(241, 243, 242);margin-bottom: 10px" data-id="1346">
                                    <div style="padding:0" class="job-item featured-item">
                                        <div class="job-image" style="margin-left:10px"><img alt='AOEVN' src='uploads/2017/07/logo5.jpg' class='avatar avatar-full photo' width='408' height='401' /></div>
                                        <div class="job-info">
                                            <h3 class="job-title"><a >Ajith</a></h3>
                                            <div class="info-company" style="width:145px;padding-bottom: 10px">
                                                <div class="company">
                                                    Scheduled date & time
                                                </div>
                                                <div class="address"><i class="fa fa-calendar"></i>12/11/2017</div>
                                                <div class="sallary"><i class="fa fa-clock-o"></i>10.00 am</div>
                
                                            </div>
                
                                        </div>
                                        <div class="job-featured"></div>
                                    </div>
                                </div>
                -->
                <div class="text-center" style="margin-bottom: 10px;">
                    Are you sure, you want to reschedule?
                </div>
                <div class="iwj-respon-msg iwj-hide"></div>
                <div class="iwj-button-loader">

                    <button type="button" style="padding:7px 70px; float: right;background-color: rgb(251, 135, 142);" id="reschedule_btn" class="iwj-btn iwj-btn-icon iwj-btn-primary iwj-submit-alert-btn" >Yes</button>
                </div>


            </form>

            <form  id="reschedule_confirm" style="display:none;"  method="post" class="iwj-form iwj-register-form">

                <input type="hidden" value="" id="current_schedule_id"> 
                <input type="hidden" value="" id="purchase_code_hdn"> 
                <input type="hidden" name="couns_id" class="user_id" id="re_couns_id">
                <div class="form-group">
                    <label class="col-md-4 control-label" for="description">Counselor</label>
                    <div class="col-md-5">
                        <input disabled="" class="form-control"  placeholder="Enter your name" value="" type="text" id="re_sche_username" name="username">
                    </div>
                </div><br><br>

                <div class="form-group">
                    <label class="col-md-4 control-label" for="description">Reason to reschedule</label>
                    <div class="col-md-5">
                        <textarea class="form-control" cols="15" id="user_re_reason" name="description"></textarea>
                    </div>

                </div><br><br>
                <div class="form-group">
                    <label class="col-md-4 control-label" for="title">Choose Date</label>
                    <div class="col-md-4">
                        <div class='input-group date datetimepicker_re'  id='datetimepicker_re_popup'>
                            <input type='text' class="form-control" />
                            <span class="input-group-addon">
                                <span class="glyphicon glyphicon-calendar"></span>
                            </span>
                        </div>
                    </div>
                    <span style="color:red;display: none;margin-left: 10px;font-weight: bold;">Counsellor not available on selected day!!</span>
                </div><br><br>
                <div class="form-group">
                    <label class="col-md-4 control-label" for="title">Time Starts</label>
                    <div class="col-md-4">
<!--                                            <input type="text" id="time_slot_start" disabled="" class="form-control iwjmb-text" value="">-->
                        <input id="startTime_re1" class="form-control iwjmb-text" type="text" />

                    </div>
                    <span style="color:red;display: none;margin-left: 20px;font-weight: bold;">This slot is already booked!!</span>
                </div>  <br><br>
                <div class="form-group">
                    <label class="col-md-4 control-label" for="title">Time Ends</label>
                    <div class="col-md-4">

                        <input id="endTime_re1"  class="form-control iwjmb-text" type="text" />
                    </div>
                    <span style="color:red;display: none;margin-left: 20px;font-weight: bold;">This slot is unavailable!!</span>
                </div>
                <br><br>
                <div class="iwj-respon-msg iwj-hide"></div>
                <div class="iwj-button-loader">

                    <button type="button" id="reschedule_confirm_btn" style="padding: 8px 20px; margin-top: 20px" name="register" class="iwj-btn iwj-btn-primary iwj-btn-full iwj-btn-large iwj-register-btn">Confirm</button>
                </div>
            </form>
        </div>
    </div>
</div>

<div id="iwj-approve-popup" class="modal-popup modal fade iwj-register-form-popup">
    <div class="modal-dialog modal-dialog1">
        <div class="modal-header">
            <h4 class="modal-title">Approve Counselor</h4>
            <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
        </div>
        <div class="modal-content col-md-12" style="border-radius: 0px;padding: 25px">
            <div class="modal-body modal-body1">
                <form method="post"  id="reschedule_purchase_code"  class="iwj-form-2 iwj-login-form1">

                    <input type="hidden" value="" id="con_id">

                    <div class="iwjmb-field iwjmb-text-wrapper  required">
                        <div class="iwjmb-label">
                            <label class="theme-color" for="your_name">Counselor Name *</label>
                        </div>
                        <div class="iwjmb-input ui-sortable">
                            <input size="30" disabled="" placeholder="Enter your name" value="" type="text" id="con_username" name="username">
                        </div>
                    </div>

                    <div class="iwjmb-field iwjmb-text-wrapper  required">
                        <div class="iwjmb-label">
                            <label class="theme-color" for="your_name">Remuneration *</label>
                        </div>
                        <div class="iwjmb-input ui-sortable">
                            <input style="width:25%" placeholder="Amount" value="" type="text" id="con_amount" class="iwjmb-text " name="username"> <label>per</label>
                            <input style="width:25%" size="30" placeholder="minutes" value="" type="text" id="con_hours" class="iwjmb-text " name="username"><label >Minutes</label>
                        </div>
                    </div>
                    


                    <div class="iwjmb-field iwjmb-text-wrapper  required">
                        <div class="iwjmb-label"><label class="theme-color" for="_iwj_address">Status *</label></div>
                        <div class="iwjmb-input ui-sortable">
                            <select data-options="{&quot;allowClear&quot;:false,&quot;width&quot;:&quot;none&quot;,&quot;placeholder&quot;:&quot;&quot;,&quot;minimumResultsForSearch&quot;:-1}" required id="con_status" class="iwjmb-select_advanced" name="status">
                                <option >Please Select</option>
                                <option value="pending"  >Pending</option>
                                <option value="approve">Approve</option>
                                <option value="disapprove">Disapprove</option>

                            </select>
                        </div>
                    </div> 


                    <div class="iwj-respon-msg iwj-hide"></div>
                    <div class="iwj-button-loader">
                        <button type="button" style="margin-bottom: 30px" id="approve_submit" name="register" class="iwj-btn iwj-btn-primary iwj-btn-full iwj-register-btn">Submit</button>
                    </div>


                </form>
            </div>
        </div>
    </div>
</div>


<div class="modal fade" id="modal-1">
    <div class="modal-dialog">
        <div class="modal-content" style="width:100%">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">&times;</span><span class="sr-only">Close</span></button>
                <h4 class="modal-title">Schedule Appointment</h4>
            </div>
            <div class="modal-body text-center">
                <a href="#modal-1" class="iwj-btn iwj-btn-primary iwj-btn-medium iwj-register-btn" onclick="show_popup()" style="width:178px;">Login</a><br><br>

                <a href="#modal-3" class="iwj-btn iwj-btn-primary iwj-btn-medium iwj-register-btn"  data-toggle="modal" data-dismiss="modal" style="width:178px;" >Use Code</a><br><br>
                
                <a href="#modal-4" class="iwj-btn iwj-btn-primary iwj-btn-medium iwj-register-btn"  data-toggle="modal" data-dismiss="modal" style="width:178px;">Pay and Schedule</a><br><br>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>

            </div>
        </div><!-- /.modal-content -->
    </div><!-- /.modal-dialog -->
</div><!-- /.modal --> 



<div class="modal fade" id="modal-3" data-keyboard="false" data-backdrop="static"  style="overflow-x: hidden;overflow-y: auto;">
    <div class="modal-dialog ">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">&times;</span><span class="sr-only">Close</span></button>
                <h4>Schedule Appointment</h4>
            </div>
            <div class="modal-body">

                <form class="form-horizontal">


                    <div class="form-group">
                        <label class="col-md-4 control-label" for="description">Code</label>
                        <div class="col-md-5">
                            <input class="form-control"  placeholder="Enter your code" value="" onchange="checkCodeValidnew(this.value)" type="text" id="sche_code" name="username">
                        </div>
                    </div>

                    <div class="form-group">
                        <label class="col-md-4 control-label" for="description">Phone Number</label>
                        <div class="col-md-5">
                            <input class="form-control"   placeholder="Enter Phone" value="" onchange="phonenumber(this.value)"  type="text" id="sche_phone" name="username">
                        </div>
                    </div>



                </form>
            </div>
            <div class="modal-footer">
                <button type="button" id="schedule-appointment1" class="btn btn-success">Schedule</button>
                <button type="button"  id="cancel_sch_popup" class="btn btn-default" data-dismiss="modal">Cancel</button>
            </div>
        </div>
    </div>
</div>

<div class="modal fade" id="modal-4" data-keyboard="false" data-backdrop="static"  style="overflow-x: hidden;overflow-y: auto;">
    <div class="modal-dialog ">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">&times;</span><span class="sr-only">Close</span></button>
                <h4>Pay and Schedule Appointment</h4>
            </div>
            <div class="modal-body">

                <form class="form-horizontal" id="pay_sche" action="<?php echo base_url(); ?>payment/index" onsubmit="return validatePhone()" method="post" class="iwj-form iwj-login-form1">
                    <input type="hidden" name="ind_type" id="ind_type" value="anonymous">
                    <input type="hidden" name="couns" id="couns" value="">
                    <input type="hidden" name="qty_input" id="qty_input" value="1">
                    <input type="hidden" name="total_input" id="total_input" value="">
                    <input type="hidden" name="descrptn" id="descrptn" value="">
                    <input type="hidden" name="date" id="date" value="">
                    <input type="hidden" name="start" id="start" value="">
                    <input type="hidden" name="end" id="end" value="">
                    <input type="hidden" name="schedule_id" id="schedule_id" value="">
                    <input type="hidden" name="video_count" id="video_count" value="">
                    <div class="form-group">
                        <label class="col-md-4 control-label" for="description">Phone Number</label>
                        <div class="col-md-5">
                            <input class="form-control"   placeholder="Enter your Phone number" value="" onchange="phonenumber(this.value)"  type="text" id="user_ph" name="user_ph">
                        </div>
                    </div>
                    
                    <div class="modal-footer">
                        <button type="submit" id="payment-schedule"  class="btn btn-success">Go to Payment</button>
                        <button type="submit" id="cancel_sch_popup1" class="btn btn-default" data-dismiss="modal">Cancel</button>
                    </div>
                </form>
            </div>
            
            
        </div>
    </div>
</div>


<div class="modal fade" id="modal_reg_phone">
    <div class="modal-dialog ">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">&times;</span><span class="sr-only">Close</span></button>
                <h4>Update Phone number</h4>
            </div>
            <div class="modal-body">
                <p class="text-center"><b style="color: #49a51b;">Please update your phone number before proceeding!!</b></p>
                <form class="form-horizontal">




                    <div class="form-group">
                        <label class="col-md-4 control-label" for="description">Phone Number</label>
                        <div class="col-md-5">
                            <input class="form-control"   placeholder="Enter Phone" value="" onchange="phonenumber(this.value)"  type="text" id="user_reg_phone" name="username">
                        </div>
                        <span style="color:red;display: none;font-weight: bold;">Please Enter Valid Phone Number</span> 
                    </div>



                </form>
            </div>
            <div class="modal-footer">
                <button type="button" id="update_phone" class="btn btn-success">Update</button>

            </div>
        </div>
    </div>
</div>
<script src="//code.jquery.com/ui/1.11.4/jquery-ui.js"></script>

<script data-skip-moving="true">
                                (function (w, d, u, b) {
                                    s = d.createElement('script');
                                    r = (Date.now() / 60000 | 0);
                                    s.async = 1;
                                    s.src = u + '?' + r;
                                    h = d.getElementsByTagName('script')[0];
                                    h.parentNode.insertBefore(s, h);
                                })(window, document, 'https://cdn.bitrix24.in/b5553359/crm/site_button/loader_4_tb461y.js');
</script>




<link rel='stylesheet' id='iw-testimonials-css'  href='<?php echo base_url(); ?>plugins/inwave-common/assets/css/iw-testimonials.css?ver=2.0.0' type='text/css' media='all' />
<link rel='stylesheet' id='slick-css'  href='<?php echo base_url(); ?>plugins/inwave-common/assets/css/slick.css?ver=2.0.0' type='text/css' media='all' />
<link rel='stylesheet' id='bootstrap-multiselect-css'  href='<?php echo base_url(); ?>plugins/iwjob/includes/class/fields/assets/css/bootstrap-multiselect.css?ver=4.8.2' type='text/css' media='all' />
<link rel='stylesheet' id='iwjmb-taxonomy2-css'  href='<?php echo base_url(); ?>plugins/iwjob/includes/class/fields/assets/css/taxonomy2.css?ver=4.8.2' type='text/css' media='all' />
<link rel='stylesheet' id='iwjmb-select2-css'  href='<?php echo base_url(); ?>plugins/iwjob/includes/class/fields/assets/css/select2.css?ver=4.0.1' type='text/css' media='all' />
<link rel='stylesheet' id='iwjmb-select-advanced-css'  href='<?php echo base_url(); ?>plugins/iwjob/includes/class/fields/assets/css/select-advanced.css?ver=4.8.2' type='text/css' media='all' />
<link rel='stylesheet' id='iwjmb-select-css'  href='<?php echo base_url(); ?>plugins/iwjob/includes/class/fields/assets/css/select.css?ver=4.8.2' type='text/css' media='all' />
<script type='text/javascript'>
    /* <![CDATA[ */
    var wpcf7 = {"apiSettings": {"root": ".\/wp-json\/contact-form-7\/v1", "namespace": "contact-form-7\/v1"}, "recaptcha": {"messages": {"empty": "Please verify that you are not a robot."}}};
    /* ]]> */
</script>
<script type='text/javascript' src='<?php echo base_url(); ?>plugins/contact-form-7/includes/js/scripts.js?ver=4.9'></script>
<script type='text/javascript'>
    /* <![CDATA[ */
    var ajax_posts = {"ajaxurl": ".\/user\/register", "noposts": "No older posts found"};
    /* ]]> */
</script>
<script type='text/javascript' src='<?php echo base_url(); ?>plugins/inwave-common/assets/js/iw-shortcodes.js?ver=2.0.0'></script>
<script type='text/javascript' src='<?php echo base_url(); ?>js/underscore.min.js?ver=1.8.3'></script>
<script type='text/javascript' src='<?php echo base_url(); ?>js/shortcode.min.js?ver=4.8.2'></script>
<script type='text/javascript' src='<?php echo base_url(); ?>js/backbone.min.js?ver=1.2.3'></script>
<script type='text/javascript'>
    /* <![CDATA[ */
    var _wpUtilSettings = {"ajax": {"url": "\/wordpress\/wp-admin\/admin-ajax.php"}};
    /* ]]> */
</script>
<script type='text/javascript' src='<?php echo base_url(); ?>js/wp-util.min.js?ver=4.8.2'></script>
<script type='text/javascript' src='<?php echo base_url(); ?>js/wp-backbone.min.js?ver=4.8.2'></script>
<script type='text/javascript'>
    /* <![CDATA[ */
    var _wpMediaModelsL10n = {"settings": {"ajaxurl": "\/wordpress\/wp-admin\/admin-ajax.php", "post": {"id": 0}}};
    /* ]]> */
</script>
<script type='text/javascript' src='<?php echo base_url(); ?>js/media-models.min.js?ver=4.8.2'></script>
<script type='text/javascript'>
    /* <![CDATA[ */
    var pluploadL10n = {"queue_limit_exceeded": "You have attempted to queue too many files.", "file_exceeds_size_limit": "%s exceeds the maximum upload size for this site.", "zero_byte_file": "This file is empty. Please try another.", "invalid_filetype": "Sorry, this file type is not permitted for security reasons.", "not_an_image": "This file is not an image. Please try another.", "image_memory_exceeded": "Memory exceeded. Please try another smaller file.", "image_dimensions_exceeded": "This is larger than the maximum size. Please try another.", "default_error": "An error occurred in the upload. Please try again later.", "missing_upload_url": "There was a configuration error. Please contact the server administrator.", "upload_limit_exceeded": "You may only upload 1 file.", "http_error": "HTTP error.", "upload_failed": "Upload failed.", "big_upload_failed": "Please try uploading this file with the %1$sbrowser uploader%2$s.", "big_upload_queued": "%s exceeds the maximum upload size for the multi-file uploader when used in your browser.", "io_error": "IO error.", "security_error": "Security error.", "file_cancelled": "File canceled.", "upload_stopped": "Upload stopped.", "dismiss": "Dismiss", "crunching": "Crunching\u2026", "deleted": "moved to the trash.", "error_uploading": "\u201c%s\u201d has failed to upload."};
    /* ]]> */
</script>
<script type='text/javascript' src='<?php echo base_url(); ?>js/plupload/wp-plupload.min.js?ver=4.8.2'></script>



<script  src="<?php echo base_url(); ?>js/index.js"></script>
<script type='text/javascript' src='<?php echo base_url(); ?>js/jquery/ui/core.min.js?ver=1.11.4'></script>
<script type='text/javascript' src='<?php echo base_url(); ?>js/jquery/ui/widget.min.js?ver=1.11.4'></script>
<script type='text/javascript' src='<?php echo base_url(); ?>js/jquery/ui/mouse.min.js?ver=1.11.4'></script>
<script type='text/javascript' src='<?php echo base_url(); ?>js/jquery/ui/sortable.min.js?ver=1.11.4'></script>
<script type='text/javascript'>
    /* <![CDATA[ */
    var mejsL10n = {"language": "en-US", "strings": {"Close": "Close", "Fullscreen": "Fullscreen", "Turn off Fullscreen": "Turn off Fullscreen", "Go Fullscreen": "Go Fullscreen", "Download File": "Download File", "Download Video": "Download Video", "Play": "Play", "Pause": "Pause", "Captions\/Subtitles": "Captions\/Subtitles", "None": "None", "Time Slider": "Time Slider", "Skip back %1 seconds": "Skip back %1 seconds", "Video Player": "Video Player", "Audio Player": "Audio Player", "Volume Slider": "Volume Slider", "Mute Toggle": "Mute Toggle", "Unmute": "Unmute", "Mute": "Mute", "Use Up\/Down Arrow keys to increase or decrease volume.": "Use Up\/Down Arrow keys to increase or decrease volume.", "Use Left\/Right Arrow keys to advance one second, Up\/Down arrows to advance ten seconds.": "Use Left\/Right Arrow keys to advance one second, Up\/Down arrows to advance ten seconds."}};
    var _wpmejsSettings = {"pluginPath": "\/wordpress\/wp-includes\/js\/mediaelement\/"};
    /* ]]> */
</script>
<script src="<?php echo base_url() ?>assets/js/validate.js"></script>
<script src="<?php echo base_url() ?>assets/js/custom.js"></script>
<script type='text/javascript' src='<?php echo base_url(); ?>js/mediaelement/mediaelement-and-player.min.js?ver=2.22.0'></script>
<script type='text/javascript' src='<?php echo base_url(); ?>js/mediaelement/wp-mediaelement.min.js?ver=4.8.2'></script>
<script type='text/javascript' src='<?php echo base_url(); ?>js/media-views.min.js?ver=4.8.2'></script>
<script type='text/javascript' src='<?php echo base_url(); ?>js/media-editor.min.js?ver=4.8.2'></script>
<script type='text/javascript' src='<?php echo base_url(); ?>js/media-grid.js?ver=4.8.2'></script>
<script type='text/javascript' src='<?php echo base_url(); ?>plugins/iwjob/includes/class/fields/assets/js/clone.js?ver=4.8.2'></script>
<script type='text/javascript'>
    /* <![CDATA[ */
    var iwj = {"site_url": ".", "base_url": ".", "ajax_url": ".\/register", "security": "112c25d382", "query_vars": "{\"page\":\"\",\"pagename\":\"home-v3\"}", "map_styles": "", "total_email_queue": "0"};
    /* ]]> */
</script>
<script type='text/javascript' src='<?php echo base_url(); ?>plugins/iwjob/assets/js/script.js?ver=4.8.2'></script>
<script type='text/javascript' src='https://www.google.com/recaptcha/api.js?onload=iwj_recaptcha&#038;render=explicit&#038;ver=4.8.2'></script>


<script type='text/javascript' src='<?php echo base_url(); ?>plugins/iwjob/assets/js/iwj_serialize_form_json.js?ver=169'></script>
<script type='text/javascript' src='<?php echo base_url(); ?>plugins/iwjob/assets/js/iwj_cookie.js?ver=169'></script>
<script type='text/javascript' src='<?php echo base_url(); ?>plugins/iwjob/assets/js/iwj_filter_common.js?ver=169'></script>
<script type='text/javascript' src='<?php echo base_url(); ?>plugins/iwjob/assets/js/filter_jobs.js?ver=169'></script>
<script type='text/javascript' src='<?php echo base_url(); ?>plugins/iwjob/assets/js/filter_candidates.js?ver=169'></script>
<script type='text/javascript' src='<?php echo base_url(); ?>plugins/iwjob/assets/js/filter_employers.js?ver=169'></script>
<script type='text/javascript' src='<?php echo base_url(); ?>plugins/iwjob/assets/js/show_less_more.js?ver=169'></script>
<script type='text/javascript' src='<?php echo base_url(); ?>plugins/iwjob/assets/js/ajax_pagination.js?ver=169'></script>
<script type='text/javascript' src='<?php echo base_url(); ?>plugins/iwjob/assets/js/widget_filter_search.js?ver=169'></script>
<script type='text/javascript' src='<?php echo base_url(); ?>plugins/iwjob/assets/js/send_ajax_mail_queue.js?ver=169'></script>
<script type='text/javascript' src='<?php echo base_url(); ?>plugins/iwjob/assets/js/filter_alpha.js?ver=169'></script>
<script type='text/javascript' src='<?php echo base_url(); ?>plugins/iwjob/assets/js/switch_layout.js?ver=169'></script>
<script type='text/javascript' src='<?php echo base_url(); ?>plugins/iwjob/assets/js/iwj_filter_box.js?ver=169'></script>
<script type='text/javascript' src='<?php echo base_url(); ?>plugins/iwjob/assets/js/jquery.matchHeight.js?ver=169'></script>
<script type='text/javascript' src='<?php echo base_url(); ?>plugins/iwjob/assets/js/jquery.bxslider.js?ver=169'></script>
<script type='text/javascript' src='<?php echo base_url(); ?>plugins/js_composer/assets/lib/bower/isotope/dist/isotope.pkgd.min.js?ver=5.2.1'></script>
<script type='text/javascript' src='<?php echo base_url(); ?>plugins/iwjob/assets/fancybox/jquery.fancybox.js?ver=169'></script>
<script type='text/javascript' src='<?php echo base_url(); ?>plugins/iwjob/assets/js/star-rating.js?ver=169'></script>

<script type='text/javascript' src='<?php echo base_url(); ?>assets/js/select2.full.min.js?ver=2.0.0'></script>
<script type='text/javascript' src='<?php echo base_url(); ?>assets/js/jquery.fitvids.js?ver=2.0.0'></script>
<script type='text/javascript' src='<?php echo base_url(); ?>assets/js/owl.carousel.min.js?ver=2.0.0'></script>
<script type='text/javascript' src='<?php echo base_url(); ?>assets/js/theia-sticky-sidebar.js?ver=2.0.0'></script>
<script type='text/javascript' src='<?php echo base_url(); ?>assets/js/jquery.enscroll.min.js?ver=2.0.0'></script>
<script type='text/javascript'>
    /* <![CDATA[ */
    var inwaveCfg = {"siteUrl": ".\/wp-admin\/", "themeUrl": ".\/wp-content\/themes\/injob", "baseUrl": ".", "ajaxUrl": ".\/wp-admin\/admin-ajax.php"};
    /* ]]> */
</script>
<script type='text/javascript' src='<?php echo base_url(); ?>assets/js/template.js?ver=2.0.0'></script>
<script type='text/javascript' src='<?php echo base_url(); ?>assets/js/jquery.navgoco.js?ver=2.0.0'></script>
<script type='text/javascript' src='<?php echo base_url(); ?>assets/js/off-canvas.js?ver=2.0.0'></script>
<script type='text/javascript' src='<?php echo base_url(); ?>js/wp-embed.min.js?ver=4.8.2'></script>
<script type='text/javascript' src='<?php echo base_url(); ?>plugins/js_composer/assets/js/dist/js_composer_front.min.js?ver=5.2.1'></script>
<script type='text/javascript' src='<?php echo base_url(); ?>assets/js/jquery.countTo.js?ver=2.0.0'></script>
<script type='text/javascript' src='<?php echo base_url(); ?>plugins/js_composer/assets/lib/waypoints/waypoints.min.js?ver=5.2.1'></script>
<script type='text/javascript' src='<?php echo base_url(); ?>plugins/inwave-common/assets/js/iw-testimonials.js?ver=2.0.0'></script>
<script type='text/javascript' src='<?php echo base_url(); ?>plugins/inwave-common/assets/js/slick.min.js?ver=2.0.0'></script>
<script type='text/javascript' src='<?php echo base_url(); ?>assets/js/mailchimp.js?ver=2.0.0'></script>
<script type='text/javascript' src='<?php echo base_url(); ?>plugins/iwjob/includes/class/fields/assets/js/bootstrap-multiselect.js?ver=4.8.2'></script>
<script type='text/javascript' src='<?php echo base_url(); ?>plugins/iwjob/includes/class/fields/assets/js/taxonomy2.js?ver=4.8.2'></script>


<script type='text/javascript' src='<?php echo base_url(); ?>js/sweet-alert.js'></script>

<script type='text/javascript' src='<?php echo base_url(); ?>js/moment.min.js'></script>

<script type='text/javascript' src='<?php echo base_url(); ?>js/bootstrap-datetimepicker.js'></script>
<script type='text/javascript' src='<?php echo base_url(); ?>plugins/iwjob/includes/class/fields/assets/js/select2.min.js?ver=4.8.2'></script>
<script type='text/javascript' src='<?php echo base_url(); ?>plugins/iwjob/includes/class/fields/assets/js/select.js?ver=4.8.2'></script>
<script type='text/javascript' src='<?php echo base_url(); ?>plugins/iwjob/includes/class/fields/assets/js/select-advanced.js?ver=4.8.2'></script>
<script type='text/javascript' src='<?php echo base_url(); ?>js/jquery.dataTables.min.js'></script>
<script type='text/javascript' src='<?php echo base_url(); ?>js/dataTables.bootstrap4.min.js'></script>
<script src="<?php echo base_url(); ?>js/bootstrapValidator.min.js"></script>
<script src="<?php echo base_url(); ?>js/fullcalendar.min.js"></script>
<script src='<?php echo base_url(); ?>js/bootstrap-colorpicker.min.js'></script>
<script src='<?php echo base_url(); ?>js/jquery.timepicker.js'></script>

<script type='text/javascript'>

    $(function () {

        $('#datetimepicker12').datetimepicker();

        $('#language').val(["sunday", "monday", "tuesday", "wednesday", "thursday", "friday", "saturday"]).trigger("change");
    });
    var minStartDate = "<?php echo date('Y-m-d'); ?>"
    var date = new Date();
    var currentMonth = date.getMonth();
    var currentDate = date.getDate();
    var currentYear = date.getFullYear();
    $('.datetimepicker_re').datetimepicker({useCurrent: false, format: 'YYYY-MM-DD', focusOnShow: false, ignoreReadonly: true, minDate: minStartDate, maxDate: new Date(currentYear, currentMonth+3, currentDate)}).on('dp.change', function (e) {

        var formatedValue = e.date.format(e.date._f);

        var weekday = ["sunday", "monday", "tuesday", "wednesday", "thursday", "friday", "saturday"];
        var a = new Date(e.date);

        var day = (weekday[a.getDay()]);
        
        var b = new Date(minStartDate);
        var today = (weekday[b.getDay()]);
        
        var dateObject = $('.datetimepicker_re').data('date')
        
        var dateOne = new Date(dateObject); 
        var dateTwo = new Date();
        var oneDay = 24*60*60*1000;
        var diffDays = Math.round(Math.abs((a.getTime() - dateTwo.getTime())/(oneDay)));

        $('#schedule_date').val(dateObject);
        $.ajax({
            type: "POST",
            url: "<?php echo base_url() ?>admin/checkCounselorWorkingDay",

            data: {day: day, user_id: $('.user_id').val(), date: dateObject},

            success: function (data) {

                if (data == 0) {

                    document.getElementById("startTime").disabled = true;
                    document.getElementById("endTime").disabled = true;

                    $('#schedule-appointment').prop('disabled', true);
                    $('#reschedule_confirm_btn').prop('disabled', true);

                    $(".datetimepicker_re").css('border', '2px solid red');
                    $(".datetimepicker_re").parent().next('span').text("Counsellor not available on selected day!!").show();
                }  
                else if(data == 1){
                    document.getElementById("startTime").disabled = true;
                    document.getElementById("endTime").disabled = true;

                    $('#schedule-appointment').prop('disabled', true);
                    $('#reschedule_confirm_btn').prop('disabled', true);
                    //$('#save-event').prop('disabled', true);
                    $(".datetimepicker_re").css('border', '2px solid red');
                    $(".datetimepicker_re").parent().next('span').text("Today's booking time is over!!. Please choose another day").show();
                }
                else {

                    document.getElementById("startTime").disabled = false;
                    document.getElementById("endTime").disabled = false;

                    //document.getElementById("startTime_re").disabled = false;
                    //document.getElementById("endTime_re").disabled = false;
                    $(".datetimepicker_re").parent().next('span').hide();
                    $('#startTime').timepicker('remove')
                    var hours = data.split('-');

                    var minTime = getTime1(hours[0])
                    var maxTime = getTime1(hours[1])
                    var end_time = "<?php echo date('h:i A') ?>"
                    var stt = new Date("November 13, 2013 " + minTime);
                    stt = stt.getTime();

                    var endt = new Date("November 13, 2013 " + end_time);

                    endt = endt.getTime();
                    var maxt = new Date("November 13, 2013 " + maxTime);
                    maxt = maxt.getTime();
                    
                    
                    
                     if (today == day){
                        
                        if ((maxt < endt) && (diffDays==0)) {
                        //if (maxt < endt) {
                            $('#schedule-appointment').prop('disabled', true);
                            $('#reschedule_confirm_btn').prop('disabled', true);
                            $(".datetimepicker_re").css('border', '2px solid red');
                            $(".datetimepicker_re").parent().next('span').text("Today's booking time is over!!. Please choose another day").show();
                        } else {
                            $('#schedule-appointment').prop('disabled', false);
                            $('#reschedule_confirm_btn').prop('disabled', false);
                            $(".datetimepicker_re").css('border', '');
                            $('#startTime').removeClass('error')
                            $('#startTime').timepicker({
                                'minTime': minTime,
                                'maxTime': maxTime,
                                'step': 15,
                                'timeFormat': 'h:i A',

                            })
                        }
                        
                    }  
                    else{
                        $('#schedule-appointment').prop('disabled', false);
                        $('#reschedule_confirm_btn').prop('disabled', false);
                        $(".datetimepicker_re").css('border', '');
                        $('#startTime').timepicker({
                            'minTime': minTime,
                            'maxTime': maxTime,
                            'step': 15,
                            'timeFormat': 'h:i A',

                        })
                        
                    }    
                }
            }
        });

    });
    
    $('#datepicker_emp').on('dp.change', function(e){
        
        $("#startTime").css('border', '');
        $("#startTime").parent().next('span').hide();
        $("#endTime").css('border', '');
        $("#endTime").parent().next('span').hide();
        document.getElementById("startTime").value = "";
        document.getElementById("endTime").value = "";
    })    
    
    $('#datetimepicker_re').on('dp.change', function(e){

        $("#startTime_re").css('border', '');
        $("#startTime_re").parent().next('span').hide();
        document.getElementById("startTime_re").value = "";
        document.getElementById("endTime_re").value = "";

    })  

    $('#datetimepicker_re_popup').on('dp.change', function(e){

        $("#startTime_re1").css('border', '');
        $("#startTime_re1").parent().next('span').hide();
        document.getElementById("startTime_re1").value = "";
        document.getElementById("endTime_re1").value = "";

    })      
    

    $(".add_event_popup").on('shown.bs.modal', function () {
        
        $('#reschedule_div').hide();
        var weekday = ["sunday", "monday", "tuesday", "wednesday", "thursday", "friday", "saturday"];
        
        var date = $('#start').val();
        if (typeof date == "undefined" || date == "") {
            var a = new Date($('#date').val());
            var date=$('#date').val()
        } else {
            var a = new Date($('#start').val());
            var date=$('#start').val()
        }
        var ret = true;
        var day = (weekday[a.getDay()]);

        var flag = $('#schedule_table_id').val();
        
        if (flag == "") {
            ret = 1;
        }

        if (ret == true) {

            $.ajax({
                type: "POST",
                url: "<?php echo base_url() ?>admin/checkCounselorWorkingDay",

                data: {day: day, user_id: $('.user_id').val(),date:date},
                success: function (data) {
                    //alert(data);
                    if (data == 0) {

                        setTimeout(function () {
                            $('.add_event_popup').modal('hide');
                        }, 2000);
                        swal({
                            title: "Oops!",
                            text: "Counsellor not available on this day!!! Please choose another day",
                            type: "error",
                            timer: 2000
                        });

                    }
                    else{
                        
                        $('#startTime').timepicker('remove')
                        var hours = data.split('-');

                        var minTime = getTime1(hours[0]);
                        var maxTime = getTime1(hours[1]);

                        var end_time = "<?php echo date('h:i A') ?>";
                        var endtime=$('#endTime').val();
                        var starttime=$('#startTime').val();

                        var stt = new Date("November 13, 2013 " + minTime);
                        stt = stt.getTime();

                        var endt = new Date("November 13, 2013 " + end_time);//current time
                        endt = endt.getTime();

                        var maxt = new Date("November 13, 2013 " + maxTime);
                        maxt = maxt.getTime();

                        var edt = new Date("November 13, 2013 " + endtime);
                        edt = edt.getTime();

                        if((new Date() > new Date(a)))
                        {
                            if(edt < endt)
                            {
                                setTimeout(function () {
                                    $('.add_event_popup').modal('hide');
                                }, 2000);
                                swal({
                                    title: "Oops!",
                                    text: "Can't Change this appointment",
                                    type: "error",
                                    timer: 2000
                                });
                            }
                            else {
                                $('#startTime').val(starttime);
                            }
                        }    

                        if (maxt < endt) {
                            //$('#startTime').addClass('error')
                        } else {

                            $('#startTime').timepicker({
                                'minTime': minTime,
                                'maxTime': maxTime,
                                'step': 15,
                                'timeFormat': 'h:i A',

                            })
                        }

                        /*if (stt > endt) {

                            $('#startTime').val(minTime)
                        }*/
                    }
                }
            });
            // $('#start').val("");
        }
    });

    $('.datetimepicker_re3').datetimepicker({useCurrent: false,format: 'YYYY-MM-DD'}).on('dp.change', function(e){

        $('#delete-event').prop('disabled', true);
        $('#update-event').prop('disabled', true);
        var weekday = ["sunday", "monday", "tuesday", "wednesday", "thursday", "friday", "saturday"];
        var date = $('.datetimepicker_re3').data('date');
        var dateObject = new Date($('#datetimepicker_re').data('date'));
        var day = (weekday[dateObject.getDay()]);
        var couns = $('.user_id').val();
        $('#schedule_date').val(date);

        $.ajax({
            type: "POST",
            url: "<?php echo base_url() ?>admin/checkCounselorWorkingDay",

            data: {day: day, user_id: couns, date:date},
            success: function (data) {
                if (data == 0) {

                    document.getElementById("startTime_re").disabled = true;
                    document.getElementById("endTime_re").disabled = true;

                    $('#save-event').prop('disabled', true);

                    $(".datetimepicker_re3").css('border', '2px solid red');
                    $(".datetimepicker_re3").parent().next('span').text("Counsellor not available on selected day!!").show();

                }
                else if (data == 1) {

                    document.getElementById("startTime_re").disabled = true;
                    document.getElementById("endTime_re").disabled = true;

                    $('#save-event').prop('disabled', true);

                    $(".datetimepicker_re3").css('border', '2px solid red');
                    $(".datetimepicker_re3").parent().next('span').text("Today's booking time is over. Please choose another day").show();

                }
                else {

                    document.getElementById("startTime_re").disabled = false;
                    document.getElementById("endTime_re").disabled = false;

                    $('#save-event').prop('disabled', false);

                    $(".datetimepicker_re3").css('border', '');
                    $(".datetimepicker_re3").parent().next('span').hide();
                    $('#startTime_re').timepicker('remove')
                    var first = data.split('-');

                    var minTime1 = getTime1(first[0])
                    var maxTime2 = getTime1(first[1])

                    $('#startTime_re').timepicker({
                        'minTime': minTime1,
                        'maxTime': maxTime2,
                        'step': 15,
                        'timeFormat': 'h:i A',

                    })
                }

            }
        });
        

    });

    $('#datetimepicker_re_popup').datetimepicker({useCurrent: false,format: 'YYYY-MM-DD'}).on('dp.change', function(e){

        var weekday = ["sunday", "monday", "tuesday", "wednesday", "thursday", "friday", "saturday"];
        var date = $('#datetimepicker_re_popup').data('date');
        var dateObject = new Date($('#datetimepicker_re_popup').data('date'));
        var resday = (weekday[dateObject.getDay()]);
        var couns = $('.user_id').val();

        $.ajax({
            type: "POST",
            url: "<?php echo base_url() ?>admin/checkCounselorWorkingDay",

            data: {day: resday, user_id: couns, date:date},
            success: function (data) {
                
                if (data == 0) {

                    document.getElementById("startTime_re1").disabled = true;
                    document.getElementById("endTime_re1").disabled = true;

                    $("#datetimepicker_re_popup").css('border', '2px solid red');
                    $("#datetimepicker_re_popup").parent().next('span').text("Counsellor not available on selected day!!").show();

                }
                else if (data == 1) {

                    document.getElementById("startTime_re1").disabled = true;
                    document.getElementById("endTime_re1").disabled = true;

                    $("#datetimepicker_re_popup").css('border', '2px solid red');
                    $("#datetimepicker_re_popup").parent().next('span').text("Today's booking time is over. Please choose another day").show();

                }
                else {

                    document.getElementById("startTime_re1").disabled = false;
                    document.getElementById("endTime_re1").disabled = false;

                    $("#datetimepicker_re_popup").css('border', '');
                    $("#datetimepicker_re_popup").parent().next('span').hide();
                    $('#startTime_re1').timepicker('remove')
                    var first = data.split('-');

                    var minTime1 = getTime1(first[0])
                    var maxTime2 = getTime1(first[1])

                    $('#startTime_re1').timepicker({
                        'minTime': minTime1,
                        'maxTime': maxTime2,
                        'step': 15,
                        'timeFormat': 'h:i A',

                    })
                }
            }
        });

    });

    $('#reschedule_id').on('change', function () {
        var reschedule_id = $(this).val();
        var date = $('#schedule_date').val();

        $.ajax({
            url: '<?php echo base_url(); ?>admin/checkSchedule',
            data: {id: reschedule_id, date: date},
            type: 'post',
            success: function (data) {
                if (data == 1) {


                    $("#reschedule_id").siblings(".select2-container").css('border', '2px solid red');
                    $("#reschedule_id").parent().next('span').show();
                } else {
                    $("#reschedule_id").siblings(".select2-container").css('border', '');
                    $("#reschedule_id").parent().next('span').hide();
                }

            }
        });
    });

    $('.counselor_availabilty').on('change', function () {


        var schedule_id = $(this).val();
        var date = $('.datetimepicker_re').data('date')
        var url = "<?php echo $this->session->userdata('counselor_id') ?>";
        var array = url.split('/');

        var lastsegment = array[array.length - 1];

        if (typeof date == "undefined") {

            var date1 = $('#datetimepicker_re_popup').data('date')
            if (typeof date1 == "undefined") {

                var date = $('#start').val()
            } else {

                date = date1;
            }
        } else {
            date = date;
        }

        if (lastsegment == "quickSchedule") {
            lastsegment = $('#couns_name').val();
        }


        $.ajax({
            url: '<?php echo base_url(); ?>admin/checkSchedule',
            data: {id: schedule_id, date: date, couns_id: url},
            type: 'post',
            success: function (data) {
                if (data >= 1) {


                    $(".counselor_availabilty").css('border', '2px solid red');
                    $(".counselor_availabilty").parent().next('span').show();
                    $('#add-event').prop('disabled', true);
                    $('#schedule-appointment').prop('disabled', true);
                } else {
                    $(".counselor_availabilty").siblings(".select2-container").css('border', '');
                    $(".counselor_availabilty").parent().next('span').hide();
                    $('#add-event').prop('disabled', false);

                    $('#schedule-appointment').prop('disabled', false);
                }

            }
        });
    });


    $(document).ready(function () {
        $('ul li a').on('click', function () {
            $(this).closest('ul').find('a').removeClass('is-active');
            $(this).addClass('is-active');
            return false;
        });
        $('#reg_btn_1').on('click', function (e) {

            var ret = true;
            var pwd = document.getElementById('reg_password').value;
            
            var passwrd=  /^(?=.*[0-9])(?=.*[!@#$%^&*])[a-zA-Z0-9!@#$%^&*]{6,15}$/;
            if(!pwd.match(passwrd)) 
            { 
                $('#reg_password').parent().parent().addClass("error");
                $('#reg_password').attr("placeholder", "Please enter valid password");
                showmsg();
                ret = false;
            }
            else
            { 
                $('#reg_password').parent().parent().removeClass("error");
            }

            if ($('#reg_username').val() == "") {

                $('#reg_username').parent().parent().addClass("error");
                $('#reg_username').attr("placeholder", "Please enter your username");
                ret = false;
            }
            if (!isEmail($('#reg_email').val())) {

                $('#reg_email').parent().parent().addClass("error");
                $('#reg_email').attr("placeholder", "Please enter a valid email address");
                ret = false;
            }
            if ($('#reg_password').val() == "") {

                $('#reg_password').parent().parent().addClass("error");
                $('#reg_password').attr("placeholder", "Please enter your password");
                ret = false;
            }
            if ($('#reg_conpassword').val() == "" || $('#reg_conpassword').val() != $('#reg_password').val()) {

                $('#reg_conpassword').parent().parent().addClass("error");
                $('#reg_conpassword').attr("placeholder", "Please check your password");
                ret = false;
            }
            if (ret != false) {
                var ref_this = $("ul.tabs li");

                ref_this.find('a').removeClass('is-active');
                ref_this.next('li:first').find('a').addClass('is-active');
                $('.reg_1').hide();
                $('.reg_2').show();
                return false;

            }

        });
        $('#reg_btn_2').on('click', function (e) {
            var ret = true;
            var reg_mobile_number = $('#reg_mobile_number').val();
            var reg_mobile_otp = $('#reg_mobile_otp').val();
            if ($('#reg_mobile_number').val() == "") {

                $('#reg_mobile_number').parent().parent().addClass("error");
                $('#reg_mobile_number').attr("placeholder", "Please enter your phone number");
                ret = false;
            }

            if ($('#reg_mobile_otp').val() == "") {

                $('#reg_mobile_otp').parent().parent().addClass("error");
                $('#reg_mobile_otp').attr("placeholder", "Please enter your OTP");
                ret = false;
            }
            if (ret != false) {

                $.ajax({
                    url: '<?php echo base_url(); ?>user/validate_otp',
                    data: {phone: reg_mobile_number, otp: reg_mobile_otp},
                    type: 'post',
                    dataType: "json",
                    success: function (data) {
                        if (data == 1) {
                            var ref_this = $("ul.tabs li a.is-active");
//
                            ref_this.removeClass('is-active');
                            ref_this.parent().next('li:first').find('a').addClass('is-active');
                            $('.reg_2').hide();
                            skip_this_step_1();

                            // $('.reg_3').show();
                        } else {
                            $('#reg_mobile_otp').val('');
                            $('#reg_mobile_otp').parent().parent().addClass("error");
                            $('#reg_mobile_otp').attr("placeholder", "Incorrect OTP");
                            ret = false;


                        }
                    }
                });


                return false;
            }

        });
        $('#reg_btn_3').on('click', function (e) {
            var ref_this = $("ul.tabs li a.is-active");

            ref_this.removeClass('is-active');
            ref_this.parent().next('li:first').find('a').addClass('is-active');
            $('.reg_3').hide();
            $('.reg_4').show();
            return false;

        });

        $('#reg_btn_4').on('click', function (e) {
            var ref_this = $("ul.tabs li a.is-active");

            ref_this.removeClass('is-active');
            ref_this.parent().next('li:first').find('a').addClass('is-active');
            $('.reg_4').hide();
            $('.reg_5').show();
            return false;

        });
    });
    function skip_this_step_1() {

        $('#submit_btn').click();
    }

    function toggle_visibility(id) {
$('input[name=amnt_radio]').prop('checked', false);
$('#total_txt').text("0.00")
        if (id == 'individual') {
            $('#individual_form').show();
            $('#organization_form').hide();
            $(".qty_select").html('')
             $("li.salary").hide();
            $(".qty_select").append('<option value="1">1</option>');
            document.getElementById('qty_inpt_org_hdn').value=1;
            $('#indivudual_a').removeClass('style1');
            $('#organization_a').addClass('style1');
        } else {
            $(".qty_select").html('')
            $('#individual_form').hide();
            $(".qty_select").append('<option value="">Please select</option><option value="5">5</option><option value="10">10</option><option value="20">20</option><option value="50">50</option><option value="100">100</option><option value="250">250</option>');
            $("li.salary").show();
            $('#organization_form').show();
            $('#organization_a').removeClass('style1');
            $('#indivudual_a').addClass('style1');
        }

    }
    $(document).ready(function () {
        $("input[name$='radio_email']").click(function () {
            var id = $(this).val();


            if (id == 'email') {
                $('#purchaseEmail').show();
                $('#purchaseMobile').hide();
            } else {
                $('#purchaseEmail').hide();
                $('#purchaseMobile').show();
            }
        });
    });

    $(document).ready(function () {

        $('#user_login').submit(function (e) {
            var ret = true;
            if (!isEmail($('#login_email').val())) {

                $('#login_email').parent().parent().addClass("error");
                $('#login_email').attr("placeholder", "Please enter a valid email address");
                ret = false;
            }

            if ($('#login_password').val() == "") {

                $('#login_password').parent().parent().addClass("error");
                $('#login_password').attr("placeholder", "Please enter your password");
                ret = false;
            }
            if (ret != false) {
                $.ajax({
                    url: $(this).attr('action'),
                    data: $(this).serialize(),
                    type: 'post',
                    success: function (data) {
                        if (data == 1) {
                            window.location.href = '<?php echo base_url(); ?>user/profile';
                        } else if (data == 0) {
                            $('#error_msg_login').text('Username or Password you entered is incorrect!!!').show();
                        } else {
                            window.location.href = window.location.href = '<?php echo base_url(); ?>home/schedule';
                        }
                    }
                });
            }

            e.preventDefault();
        });
    });

    $(document).ready(function () {
        $('#coupon_submit').click(function (e) {
            if ($('#your_name').val() == "") {

                $('#your_name').parent().parent().addClass("error");
                $('#your_name').css('border-color', 'red');
                $('#your_name').attr("placeholder", "Please enter your name");
            }
            if (!isEmail($('#email').val())) {

                $('#email').parent().parent().addClass("error");
                $('#email').css('border-color', 'red');
                $('#email').attr("placeholder", "Please enter a valid email address");
            }
            if ($('#_iwj_phone').val() == "") {

                $('#_iwj_phone').parent().parent().addClass("error");
                $('#_iwj_phone').css('border-color', 'red');
                $('#_iwj_phone').attr("placeholder", "Please enter your valid phone number");
            }
            if ($('#passwrd').val() == "") {

                $('#passwrd').parent().parent().addClass("error");
                $('#passwrd').css('border-color', 'red');
                $('#passwrd').attr("placeholder", "Please enter your password");
            }
        });
    });

    $(document).ready(function () {
        $('#schedule-appointment').click(function (e) {

            if ($('#sche_username').val() == "") {

                //$('#sche_username').parent().parent().addClass("error");
                $('#sche_username').css('border-color', 'red');
                $('#sche_username').attr("placeholder", "Please enter valid code");
            }
        });
    });

    $(document).ready(function () {
        var ret = true;
        $('#gen_btn').click(function () {
            
            if ($('#your_amount').val() == "") {
                $('#your_amount').parent().parent().addClass("error");
                $('#your_amount').css('border-color', 'red');
                $('#your_amount').attr("placeholder", "Please enter valid coupon amount");
                ret = false;

            }
            else{
                var ret = true;
                $('#gen_btn').prop('disabled', true);
            }
            if (ret != false) {
                
                $.ajax({
                    type: "POST",
                    url: "<?php echo base_url() ?>admin/generateRandomString",
                    success: function (data) {
                        console.log(data);
                        $('#coupon_code').val(data);
                    }
                });
                
            }
            
            
        });
    })



    $(document).ready(function () {

        $('#btn_sub').click(function (e) {
            // var ret = true;
            if ($('#coupon_name').val() == "") {
                $('#coupon_name').parent().parent().addClass("error");
                $('#coupon_name').css('border-color', 'red');
                $('#coupon_name').attr("placeholder", "Please enter coupon name");
                // ret = false;

            }
            if ($('#your_amount').val() == "") {
                $('#your_amount').parent().parent().addClass("error");
                $('#your_amount').css('border-color', 'red');
                $('#your_amount').attr("placeholder", "Please enter valid coupon amount");
                // ret = false;

            }

            if ($('#coupon_code').val() == "") {
                $('#coupon_code').parent().parent().addClass("error");
                $('#coupon_code').css('border-color', 'red');
                $('#coupon_code').attr("placeholder", "Please generate your code");
                // ret = false;
            }


        });
    });


    function isEmail(email) {
        var regex = /^([a-zA-Z0-9_.+-])+\@(([a-zA-Z0-9-])+\.)+([a-zA-Z0-9]{2,4})+$/;
        return regex.test(email);
    }

//    function isDate(txtDate)
//    {
//        //Here comes one ugly, long and working regexp
//        var rxDatePattern = /^(((0?[1-9]|[12]\d|3[01])\.(0?[13578]|1[02])\.((19|[2-9]\d)\d{2}))|((0?[1-9]|[12]\d|30)\.(0?[13456789]|1[012])\.((19|[2-9]\d)\d{2}))|((0?[1-9]|1\d|2[0-8])\.0?2\.((19|[2-9]\d)\d{2}))|(29\.0?2\.((1[6-9]|[2-9]\d)(0?[48]|[2468][048]|[13579][26])|((16|[2468][048]|[3579][26])00))))$/g;
//        return txtDate.match(rxDatePattern); //Returns true iff. currval is a valid date
//    }
//    ;


    function delUser(id) {

        event.preventDefault();
        var form = event.target.form;
        swal({
            title: "Are you sure?",
            text: "You want to be delete this record!",
            type: "warning",
            showCancelButton: true,
            confirmButtonColor: '#DD6B55',
            confirmButtonText: 'Yes, I am sure!',
            cancelButtonText: "No, cancel it!",
            closeOnConfirm: false,
            closeOnCancel: false
        },
                function (isConfirm) {
                    if (isConfirm) {
                        swal({
                            title: 'Deleted!',
                            text: 'Record is successfully deleted!',
                            type: 'success'
                        }, function () {
                            $.ajax({
                                url: '<?php echo base_url(); ?>index.php/admin/delUser',
                                data: {id: id},
                                type: 'post',
                                success: function (data) {
                                    if (data == 1) {
                                        window.location.href = '<?php echo base_url(); ?>index.php/admin/userlist';
                                    } else {
                                        $('#error_msg_login').text('Username or Passwird you entered is incorrect!!!').show();
                                    }
                                }
                            });
                        });

                    } else {
                        swal("Cancelled", "Your Record is safe :)", "error");
                    }
                });
    }

    function delCounselor(id) {

        event.preventDefault();
        var form = event.target.form;
        swal({
            title: "Are you sure?",
            text: "You want to be delete this record!",
            type: "warning",
            showCancelButton: true,
            confirmButtonColor: '#DD6B55',
            confirmButtonText: 'Yes, I am sure!',
            cancelButtonText: "No, cancel it!",
            closeOnConfirm: false,
            closeOnCancel: false
        },
                function (isConfirm) {
                    if (isConfirm) {
                        swal({
                            title: 'Deleted!',
                            text: 'Record is successfully deleted!',
                            type: 'success'
                        }, function () {
                            $.ajax({
                                url: '<?php echo base_url(); ?>index.php/admin/delUser',
                                data: {id: id},
                                type: 'post',
                                success: function (data) {
                                    if (data == 1) {
                                        window.location.href = '<?php echo base_url(); ?>index.php/admin/counselorlist';
                                    } else {
                                        $('#error_msg_login').text('Username or Passwird you entered is incorrect!!!').show();
                                    }
                                }
                            });
                        });

                    } else {
                        swal("Cancelled", "Your Record is safe :)", "error");
                    }
                });
    }

    function delAdmin(id) {

        event.preventDefault();
        var form = event.target.form;
        swal({
            title: "Are you sure?",
            text: "You want to be delete this record!",
            type: "warning",
            showCancelButton: true,
            confirmButtonColor: '#DD6B55',
            confirmButtonText: 'Yes, I am sure!',
            cancelButtonText: "No, cancel it!",
            closeOnConfirm: false,
            closeOnCancel: false
        },
                function (isConfirm) {
                    if (isConfirm) {
                        swal({
                            title: 'Deleted!',
                            text: 'Record is successfully deleted!',
                            type: 'success'
                        }, function () {
                            $.ajax({
                                url: '<?php echo base_url(); ?>admin/delAdmin',
                                data: {id: id},
                                type: 'post',
                                success: function (data) {
                                    if (data == 1) {
                                        window.location.href = '<?php echo base_url(); ?>admin/admin_list';
                                    } else {
                                        $('#error_msg_login').text('Username or Passwird you entered is incorrect!!!').show();
                                    }
                                }
                            });
                        });

                    } else {
                        swal("Cancelled", "Your Record is safe :)", "error");
                    }
                });
    }

    function delcoupon(id) {

        event.preventDefault();
        var form = event.target.form;
        swal({
            title: "Are you sure?",
            text: "You want to be delete this record!",
            type: "warning",
            showCancelButton: true,
            confirmButtonColor: '#DD6B55',
            confirmButtonText: 'Yes, I am sure!',
            cancelButtonText: "No, cancel it!",
            closeOnConfirm: false,
            closeOnCancel: false
        },
                function (isConfirm) {
                    if (isConfirm) {
                        swal({
                            title: 'Deleted!',
                            text: 'Record is successfully deleted!',
                            type: 'success'
                        }, function () {
                            $.ajax({

                                url: '<?php echo base_url(); ?>admin/delcoupon',
                                data: {id: id},

                                type: 'post',
                                success: function (data) {

                                    if (data == 1) {
                                        window.location.href = '<?php echo base_url(); ?>admin/couponlist';
                                    } else {
                                        $('#error_msg_login').text('Username or Passwird you entered is incorrect!!!').show();
                                    }
                                }
                            });
                        });

                    } else {
                        swal("Cancelled", "Your Record is safe :)", "error");
                    }
                });
    }

    function chkbox(chck, id) {

        $(chck).val(chck.checked ? 'ACTIVE' : 'INACTIVE');
        var txt = $(chck).val();

        $.ajax({
            url: '<?php echo base_url(); ?>admin/statuscheck',
            data: {id: id, text: txt},
            type: 'post',
            success: function (data) {
                if (data == 1) {
                    if (txt == 'ACTIVE')
                    {
                        $(chck).closest("tr").find(".v3").text("ACTIVE");
                    } else
                    {
                        $(chck).closest("tr").find(".v3").text("INACTIVE");
                    }
                }
            }
        });


    }
    function approveUser(id) {
        $.ajax({
            url: '<?php echo base_url(); ?>admin/getUserDetails',
            data: {id: id},
            type: 'post',
            dataType: "json",
            success: function (res) {

                if (res.data != "") {
                    var val = res.data.days;
                    var sel = JSON.parse(val);
//                    var records = res.timing;

                    $('#con_username').val(res.data.username);
                    $('#con_amount').val(res.data.amount);
                    $('#con_hours').val(res.data.minutes);
                    $('#con_status').val(res.data.status).trigger('change');
                    $('#con_id').val(res.data.id);
                    $('#language').val(sel).trigger('change');

                    $('#iwj-approve-popup').modal('show')
                } else if (res.user != "") {
                    $('#con_username').val(res.user.username);
                    $('#con_status').val(res.user.status).trigger('change');
                    $('#con_id').val(res.user.id);
                    $('#iwj-approve-popup').modal('show')
                }


            }
        });
    }

    function phonenumber(inputtxt)
    {

        var filter = /^\d*(?:\.\d{1,2})?$/;
        var link = 'https://en.wikipedia.org/wiki/Do_Not_Disturb_Registry';
        if (filter.test(inputtxt)) {
            if (inputtxt.length == 10) {


                return true;



            } else {

                return false;
            }
        } else {

            return false;
        }

//        var phoneno = /^\d{10}$/;
//        if (/^[0-9]{1,10}$/.test(+inputtxt))
//        {
//            return true;
//        } else
//        {
//            $('#sche_phone').val('')
//            $('#sche_phone').addClass("error");
//            $('#sche_phone').attr("placeholder", "Please enter phone number");
//            return false;
//            
//        }
    }

    function purchase_details(id) {

        $.ajax({
            url: '<?php echo base_url(); ?>home/purchaseDetails',
            data: {id: id},
            type: 'post',
            dataType: "json",
            success: function (data) {
              
                if (data != 0) {
                    $('#pr_amount').text(data[0].coupon_amount);
                    $('#pr_date').text(data[0].purchase_date);
                    $('#pr_status').text(data[0].payment_status);
                }
                $('#purchaseReport').modal('show');
            }
        });

    }

    function delpurchase(id) {

        event.preventDefault();
        var form = event.target.form;
        swal({
            title: "Are you sure?",
            text: "You want to be delete this record!",
            type: "warning",
            showCancelButton: true,
            confirmButtonColor: '#DD6B55',
            confirmButtonText: 'Yes, I am sure!',
            cancelButtonText: "No, cancel it!",
            closeOnConfirm: false,
            closeOnCancel: false
        },
                function (isConfirm) {
                    if (isConfirm) {
                        swal({
                            title: 'Deleted!',
                            text: 'Record is successfully deleted!',
                            type: 'success'
                        }, function () {
                            $.ajax({
                                url: '<?php echo base_url(); ?>home/delPurchasedetails',
                                data: {id: id},

                                type: 'post',
                                success: function (data) {

                                    if (data == 1) {
                                        window.location.href = '<?php echo base_url(); ?>admin/purchaseDetails';
                                    } else {
                                        $('#error_msg_login').text('Username or Password you entered is incorrect!!!').show();
                                    }
                                }
                            });
                        });

                    } else {
                        swal("Cancelled", "Your Record is safe :)", "error");
                    }
                });
    }

    function callReport(id) {
        //alert(id);
        $.ajax({
            url: '<?php echo base_url(); ?>home/callReports',
            data: {id: id},
            type: 'post',
            dataType: "json",
            success: function (data) {
                $.each(data, function (index, value) {
                     
                    if (typeof value.cname != "undefined") {
                        
                        $('#call_logs_user_name').text(value.cname);
                        $('#call_logs_date').text(value.date);
                        $('#call_logs_time').text(formatAMPM(value.time_starts) + " - " + formatAMPM(value.time_ends));
                        $('#call_logs_status').text(value.cstatus);

                        var init = value.duration;
                        var hours = Math.floor(init / 3600);
                        var minutes = Math.floor((init / 60) % 60);
                        var seconds = init % 60;
                        $('#call_logs_duratn').text(hours + ":" + minutes + ":" + seconds);

                        if (value.duration > 60) {
                            $('#call_logs_price').text(value.amount);
                        } else {
                            $('#call_logs_price').text("");
                        }
                    }
                });

                $('#callReport').modal('show');


            }
        });
    }

    function delCallReport(id) {
        event.preventDefault();
        var form = event.target.form;
        swal({
            title: "Are you sure?",
            text: "You want to be delete this record!",
            type: "warning",
            showCancelButton: true,
            confirmButtonColor: '#DD6B55',
            confirmButtonText: 'Yes, I am sure!',
            cancelButtonText: "No, cancel it!",
            closeOnConfirm: false,
            closeOnCancel: false
        },
                function (isConfirm) {
                    if (isConfirm) {
                        swal({
                            title: 'Deleted!',
                            text: 'Record is successfully deleted!',
                            type: 'success'
                        }, function () {
                            $.ajax({
                                url: '<?php echo base_url(); ?>home/delCalldetails',
                                data: {id: id},

                                type: 'post',
                                success: function (data) {

                                    if (data == 1) {
                                        window.location.href = '<?php echo base_url(); ?>admin/callDetails';
                                    } else {
                                        $('#error_msg_login').text('Username or Password you entered is incorrect!!!').show();
                                    }
                                }
                            });
                        });

                    } else {
                        swal("Cancelled", "Your Record is safe :)", "error");
                    }
                });
    }


    function setWorkingTime(id) {
        $('#time_slot').val("").trigger('change');
        $('#time_slot').empty();
        $.ajax({
            url: '<?php echo base_url(); ?>admin/getUserDetails',
            data: {id: id},
            type: 'post',
            dataType: "json",
            success: function (res) {

                if (res.data != "") {
                    var val = res.timing;
                    //   var sel = JSON.parse(val);
                    var hours = res.workings_hours;
                    

                    
                    if (typeof hours != "undefined") {
                        var arr = hours.split('-');
                        $('.slider-time').text(arr[0]);
                        $('.slider-time2').text(arr[1]);
                        $("#slider-range").slider({
                            range: true,
                            min: 0,
                            max: 1440,
                            step: 15,
                            values: [res.time_starts, res.time_ends],
                            slide: function (e, ui) {
                                var hours1 = Math.floor(ui.values[0] / 60);
                                var minutes1 = ui.values[0] - (hours1 * 60);

                                if (hours1.length == 1)
                                    hours1 = '0' + hours1;
                                if (minutes1.length == 1)
                                    minutes1 = '0' + minutes1;
                                if (minutes1 == 0)
                                    minutes1 = '00';
                                if (hours1 >= 12) {
                                    if (hours1 == 12) {
                                        hours1 = hours1;
                                        var min1 = minutes1;
                                        minutes1 = minutes1 + " PM";

                                    } else {
                                        hours1 = hours1 - 12;
                                        min1 = minutes1;
                                        minutes1 = minutes1 + " PM";
                                        min1 = minutes1;
                                    }
                                } else {
                                    hours1 = hours1;
                                    min1 = minutes1;
                                    minutes1 = minutes1 + " AM";

                                }
                                if (hours1 == 0) {
                                    hours1 = 12;
                                    min1 = minutes1;
                                    minutes1 = minutes1;

                                }



                                $('.slider-time').html(hours1 + ':' + minutes1);

                                var hours2 = Math.floor(ui.values[1] / 60);
                                var minutes2 = ui.values[1] - (hours2 * 60);

                                if (hours2.length == 1)
                                    hours2 = '0' + hours2;
                                if (minutes2.length == 1)
                                    minutes2 = '0' + minutes2;
                                if (minutes2 == 0)
                                    minutes2 = '00';
                                if (hours2 >= 12) {
                                    if (hours2 == 12) {
                                        hours2 = hours2;
                                        minutes2 = minutes2 + " PM";
                                    } else if (hours2 == 24) {
                                        hours2 = 11;
                                        minutes2 = "59 PM";
                                    } else {
                                        hours2 = hours2 - 12;
                                        minutes2 = minutes2 + " PM";
                                    }
                                } else {
                                    hours2 = hours2;
                                    minutes2 = minutes2 + " AM";
                                }

                                $('.slider-time2').html(hours2 + ':' + minutes2);

                                $.ajax({
                                    url: '<?php echo base_url(); ?>admin/changeSlot',
                                    data: {start: hours1 + ':' + minutes1, end: hours2 + ':' + minutes2, id: id},
                                    type: 'post',
                                    dataType: "json",
                                    success: function (data) {
                                        // var sel = JSON.parse(data);
                                       
                                        $('#time_slot').val("").trigger('change');
                                        $('#time_slot').empty();
                                        $.each(data, function (index, value) {
                                            var opt = '<option value="' + value + '">' + value + '</option>'

                                            $('#time_slot').append(opt);

                                        });

                                        $('#time_slot').val(data).trigger('change');

                                    }
                                });

                            }
                        });

                    } else {
                        $("#slider-range").slider({
                            range: true,
                            min: 0,
                            max: 1440,
                            step: 15,
                            values: [540, 1020],
                            slide: function (e, ui) {
                                var hours1 = Math.floor(ui.values[0] / 60);
                                var minutes1 = ui.values[0] - (hours1 * 60);

                                if (hours1.length == 1)
                                    hours1 = '0' + hours1;
                                if (minutes1.length == 1)
                                    minutes1 = '0' + minutes1;
                                if (minutes1 == 0)
                                    minutes1 = '00';
                                if (hours1 >= 12) {
                                    if (hours1 == 12) {
                                        hours1 = hours1;
                                        var min1 = minutes1;
                                        minutes1 = minutes1 + " PM";

                                    } else {
                                        hours1 = hours1 - 12;
                                        min1 = minutes1;
                                        minutes1 = minutes1 + " PM";
                                        min1 = minutes1;
                                    }
                                } else {
                                    hours1 = hours1;
                                    min1 = minutes1;
                                    minutes1 = minutes1 + " AM";

                                }
                                if (hours1 == 0) {
                                    hours1 = 12;
                                    min1 = minutes1;
                                    minutes1 = minutes1;

                                }

                                $('.slider-time').html(hours1 + ':' + minutes1);

                                var hours2 = Math.floor(ui.values[1] / 60);
                                var minutes2 = ui.values[1] - (hours2 * 60);

                                if (hours2.length == 1)
                                    hours2 = '0' + hours2;
                                if (minutes2.length == 1)
                                    minutes2 = '0' + minutes2;
                                if (minutes2 == 0)
                                    minutes2 = '00';
                                if (hours2 >= 12) {
                                    if (hours2 == 12) {
                                        hours2 = hours2;
                                        minutes2 = minutes2 + " PM";
                                    } else if (hours2 == 24) {
                                        hours2 = 11;
                                        minutes2 = "59 PM";
                                    } else {
                                        hours2 = hours2 - 12;
                                        minutes2 = minutes2 + " PM";
                                    }
                                } else {
                                    hours2 = hours2;
                                    minutes2 = minutes2 + " AM";
                                }

                                $('.slider-time2').html(hours2 + ':' + minutes2);

                                $.ajax({
                                    url: '<?php echo base_url(); ?>admin/changeSlot',
                                    data: {start: hours1 + ':' + minutes1, end: hours2 + ':' + minutes2, id: id},
                                    type: 'post',
                                    dataType: "json",
                                    success: function (data) {
                                        // var sel = JSON.parse(data);
                               
                                        $('#time_slot').val("").trigger('change');
                                        $('#time_slot').empty();
                                        $.each(data, function (index, value) {
                                            var opt = '<option value="' + value + '">' + value + '</option>'

                                            $('#time_slot').append(opt);

                                        });

                                        $('#time_slot').val(data).trigger('change');

                                    }
                                });

                            }
                        });
                    }
                    $('#con_slot_username').val(res.data.username);
                    // $('#con_amount').val(res.data.amount);
                    //$('#con_hours').val(res.data.minutes);
                    //$('#con_status').val(res.data.status).trigger('change');
                    $('#con_slot_id').val(res.data.id);

                    $.each(val, function (index, value) {
                        var opt = '<option value="' + value + '">' + value + '</option>'

                        $('#time_slot').append(opt);

                    });

                    $('#time_slot').val(val).trigger('change');

                    $('#iwj-slot-popup').modal('show')
                } else if (res.user != "") {
                    $('#con_slot_username').val(res.user.username);
                    // $('#con_status').val(res.user.status).trigger('change');
                    $('#con_slot_id').val(res.user.id);
                    $('#iwj-slot-popup').modal('show')
                }


            }
        });
    }

    $('#approve_submit').on('click', function () {
        var user_id = $('#con_id').val();
        var user_status = $('#con_status').val();
        var amount = $('#con_amount').val();
        var hours = $('#con_hours').val();
       

        $.ajax({
            url: '<?php echo base_url(); ?>admin/approveUser',
            data: {id: user_id, user_status: user_status, amount: amount, hours: hours},
            type: 'post',
            success: function (data) {
               
                if (data == 1) {
                    window.location.href = '<?php echo base_url(); ?>admin/counselorlist';
                } else {
                    $('#error_msg_login').text('Username or Passwird you entered is incorrect!!!').show();
                }
            }
        });
    });

    $('#slot_submit').on('click', function () {
        var user_id = $('#con_slot_id').val();
        var hours = $('.slider-time').text() + "-" + $('.slider-time2').text();
        var timing = $('#time_slot').val();

        $.ajax({
            url: '<?php echo base_url(); ?>admin/submitTimeSlots',
            data: {id: user_id, timing: timing, hours: hours},
            type: 'post',
            success: function (data) {
               
                if (data == 1) {
                    window.location.href = '<?php echo base_url(); ?>admin/counselorlist';
                } else {
                    $('#error_msg_login').text('Username or Passwird you entered is in  correct!!!').show();
                }
            }
        });
    });
</script>
<script>
    function buyCredit() {

        $('#buyCredit').modal('show')
    }
    $('#buy_credit1').click(function () {

        if ($('#credit_bal').val() == "") {

            $('#credit_bal').parent().parent().addClass("error");
            $('#credit_bal').attr("placeholder", "Please Enter Credit Amount");
            return false;
        } else {
            $("#purchase_more_credit").submit();
        }


    });

</script>
<script type='text/javascript'>

//    
// $(document).ready(function () {
//    $('ul.tags').find('input:checkbox').on('click', function (e) {
//        e.preventDefault();
//        if ($(this).prop("checked")) {
//            var chk=$(this).val();
//            
//            
//            
//        } else{
//           $('.results > li').show();
//        }
//    });
//});

    function getPhoneFilterOptions() {
        var opts_1 = [];
        var opts1 = [];
        var opts = [];

        $checkboxes.each(function () {
            if (this.name == "iwj_cat") {
                if (this.checked) {
                    opts_1.push(this.value);
                }
            } else {
                if (this.checked) {
                    opts1.push(this.value);
                }
            }

        });

        return opts_1 + '-' + opts1;
    }

    function updatePhones(opts, opts1) {

        var tbl_body = "";
        var tbl_row;

        $.ajax({
            type: "POST",
            url: "<?php echo base_url() ?>user/get_counselor",
            dataType: 'json',
            cache: false,
            data: {filterOpts: opts, filterOpts1: opts1},
            success: function (records) {
               
                tbl_row = "";
                if (records.length > 0) {

                    $.each(records, function (index, value) {
                        if (value.photo != "") {
                            var photo = value.photo
                        } else {
                            var photo = "<?php echo base_url() . 'uploads/images/user.jpg' ?>";
                        }
                        
                        if (value.video_call == 1){
                            var video = '<div class="video"><i class="fa fa-video-camera"></i>Video Call Available</div>';
                        } else {
                            var video = "";
                        }

                        var dom = JSON.parse(value.domain);

                        tbl_row += '<div class="grid-content"  data-id="1346"><div class="job-item featured-item">\n\
                                              <div class="job-image"><img alt="AOEVN" src="' + photo + '" class="avatar avatar-full photo" width="408" height="401" /></div>\n\
                                                <div class="job-info"><h3 class="job-title"><a >' + value.username + '</a></h3>\n\
                                                     <div class="info-company"><div class="company"><i class="fa fa-suitcase"></i>\n\
                                                        <a >' + dom + '</a></div><div class="address"><i class="fa fa-cogs"></i>' + value.experience + ' years Exp</div>' + video + '\n\
                                                             <div class="sallary" style="display: inline-flex"><i class="fa fa-inr"></i>Rs ' + value.amount + ' for ' + value.minutes + ' minutes<div class="help-tip"><p>Please note: 1st session will start with an assessment.</p></div></div></div>\n\
                                                             \n\
                                                                <div class="job-type full-time">\n\
<?php if ($this->session->userdata("userData")) { ?><a class="type-name" onclick="set_user_session('+value.id+')" >Schedule</a><?php } else { ?>\n\
                                                                <a class="type-name" onclick="set_user_session('+value.id+')">Schedule</a><?php } ?>\n\
                                                            </div></div><div class="job-featured"></div></div></div>';

                    });
                } else {
                    tbl_row += '<div class="alert alert-info"><strong>Info!</strong> No Counselors available.</div>';
                }
                $('ul.pagination').find('li.active').removeClass('active');
                $('ul.pagination li:first').addClass('active');
                $("#cn_content").html(tbl_row);
            }
        });
    }

   

</script>
<script>
    $(document).ready(function () {
        $('ul li a').on('click', function () {
            $(this).closest('ul').find('a').removeClass('is-active');
            $(this).addClass('is-active');
            return false;
        });
        $('#join_btn1').on('click', function () {

            var ret = true;


            if ($('#join_username').val() == "") {

                $('#join_username').parent().parent().addClass("error");
                $('#join_username').attr("placeholder", "Please enter your username");
                ret = false;
            }
            if (!isEmail($('#join_email').val())) {

                $('#join_email').parent().parent().addClass("error");
                $('#join_email').attr("placeholder", "Please enter a valid email address");
                ret = false;
            }
            if ($('#join_password').val() == "") {

                $('#join_password').parent().parent().addClass("error");
                $('#join_password').attr("placeholder", "Please enter your password");
                ret = false;
            }
            if ($('#join_conpassword').val() == "" || $('#join_conpassword').val() != $('#join_password').val()) {

                $('#join_conpassword').parent().parent().addClass("error");
                $('#join_conpassword').attr("placeholder", "Please check your password");
                ret = false;
            }
            if (ret != false) {
                var ref_this = $("ul.tabs li");
                ref_this.find('a').removeClass('is-active');
                ref_this.next('li:first').find('a').addClass('is-active');
                $('.join_1').hide();
                $('.join_m').show();
                return false;
            }
        });
        $('#join_btn_2').on('click', function () {
            var ret = true;
            var reg_mobile_number = $('#reg_mobile_number').val();
            var reg_mobile_otp = $('#reg_mobile_otp').val();
            if ($('#reg_mobile_number').val() == "") {

                $('#reg_mobile_number').parent().parent().addClass("error");
                $('#reg_mobile_number').attr("placeholder", "Please enter your phone number");
                ret = false;
            }

            if ($('#reg_mobile_otp').val() == "") {

                $('#reg_mobile_otp').parent().parent().addClass("error");
                $('#reg_mobile_otp').attr("placeholder", "Please enter your OTP");
                ret = false;
            }
            if (ret != false) {

                $.ajax({
                    url: '<?php echo base_url(); ?>user/validate_otp',
                    data: {phone: reg_mobile_number, otp: reg_mobile_otp},
                    type: 'post',
                    dataType: "json",
                    success: function (data) {
                        if (data == 1) {
                            var elm = $('#languages');
                            $(elm).select2();
                            $('#languages').val(["english", "hindi", "malayalam", "tamil"]).trigger("change");
                            var ref_this = $("ul.tabs li");
                            ref_this.find('a').removeClass('is-active');
                            ref_this.next('li:first').find('a').addClass('is-active');
                            $('.join_m').hide();
                            $('.join_2').show();
                            return false;
                        } else {
                            $('#reg_mobile_otp').val('');
                            $('#reg_mobile_otp').parent().parent().addClass("error");
                            $('#reg_mobile_otp').attr("placeholder", "Incorrect OTP");
                            ret = false;
                        }
                    }
                });

//                
                return false;
            }

        });
        $('#join_btn2').on('click', function () {
            var ref_this = $("ul.tabs li a.is-active");
            ref_this.removeClass('is-active');
            ref_this.parent().next('li:first').find('a').addClass('is-active');
            $('.join_2').hide();
            $('.join_3').show();
            return false;
        });


    });
</script>


<script>
    function onSuccess(googleUser) {
        var profile = googleUser.getBasicProfile();
        gapi.client.load('plus', 'v1', function () {
            var request = gapi.client.plus.people.get({
                'userId': 'me'
            });
            //Display the user details
            request.execute(function (resp) {
                var profileHTML = '<div class="profile"><div class="head">Welcome ' + resp.name.givenName + '! <a href="javascript:void(0);" onclick="signOut();">Sign out</a></div>';
                profileHTML += '<img src="' + resp.image.url + '"/><div class="proDetails"><p>' + resp.displayName + '</p><p>' + resp.emails[0].value + '</p><p>' + resp.gender + '</p><p>' + resp.id + '</p><p><a href="' + resp.url + '">View Google+ Profile</a></p></div></div>';
                $('.userContent').html(profileHTML);
                $('#gSignIn').slideUp('slow');
            });
        });
    }
    function onFailure(error) {
        alert(error);
    }
    function renderButton() {
        gapi.signin2.render('gSignIn', {
            'scope': 'profile email',
            'width': 150,
            'height': 50,
            'longtitle': false,
            'theme': 'dark',
            'onsuccess': onSuccess,
            'onfailure': onFailure
        });
    }
    function signOut() {
        var auth2 = gapi.auth2.getAuthInstance();
        auth2.signOut().then(function () {
            $('.userContent').html('');
            $('#gSignIn').slideDown('slow');
        });
    }
    function show_popup() {
        var url = "<?php echo current_url(); ?>";
        $.ajax({
            url: '<?php echo base_url(); ?>home/keepPageUrl',
            data: {url: url},
            type: 'post',
            success: function (data) {


            }
        });

        $('#modal-1').modal('hide')
        $('#iwj-login-popup').modal()


    }


    $(document.body).on('hidden.bs.modal', function () {
        $("#schedule_id").siblings(".select2-container").css('border', '');
        $("#schedule_id").parent().next('span').hide();
    });

    $(document).ready(function () {

//        var email = '<?php //echo $email                                                                  ?>';
//        var login = '<?php //echo $login                                                                  ?>';
//        if (login == 1) {
//            $('#login_email').val(email);
//            $('#iwj-login-popup').modal('show')
//        } else {
//            $('#iwj-login-popup').modal('hide')
//        }

        $('.userlist').DataTable();
    });

    $(document).ready(function () {
        $(".addCF").click(function () {
            var tds = $("#customFields").children('tbody').children('tr').children('td').length;

            $("#customFields").append('<tr valign="top"><th scope="row"><label for="customFieldName">Slot - ' + (parseInt(tds) + 1) + '</label></th><td><input style="width:25%" type="text" class="iwjmb-text timepicker" name="customFieldName[]" value="" placeholder="Starts" /> &nbsp; <input style="width:25%" type="text" class="iwjmb-text timepicker" id="customFieldValue" name="customFieldValue[]" value="" placeholder="Ends" /> &nbsp; <a href="javascript:void(0);" class="remCF">Remove</a></td></tr>');
        });
        $("#customFields").on('click', '.remCF', function () {
            $(this).parent().parent().remove();
        });
    });
    $(function () {
        $('#datetimepicker1').datetimepicker({format: 'DD/MM/YYYY'});

        $('body').on('focus', ".timepicker", function () {
            $(this).datetimepicker({
                format: 'LT'
            });
        });

    });

    function user_schedule(id) {

        $('#couns_id').val(id)
        getCounselorData(id)
        $('#modal-2').modal('show')
    }

    function code_schedule(id) {
        $('#couns_id').val(id)
        getCounselorData(id)
        $('#modal-1').modal('show')
    }

    function getCounselorData(id) {
        $.ajax({
            url: '<?php echo base_url(); ?>home/getCounselorDetails',
            data: {id: id},
            type: 'post',
            dataType: "json",
            success: function (data) {

                var tbl_row = "";
                var tbl_row_timing = "";
                $('#sche_user').val(data.user)
                $('#sche_username').val(data.username)
                if (data.timing.length > 0) {

                    $.each(data.timing, function (index, value) {
                        var timing = value.time_starts + ' - ' + value.time_ends
                        tbl_row_timing += '<option value=' + value.id + '>' + timing + '</option>';

                    });
                } else {
                    tbl_row_timing += '<div class="alert"><span class="closebtn" onclick="this.parentElement.style.display="none";">&times;</span> No Data Found</div>';
                }
                $("#counselor_select").html(tbl_row);
                $(".counselor_availabilty").html(tbl_row_timing);
            }
        });

    }


    $('#schedule-appointment').on('click', function () {

        var link = 'https://en.wikipedia.org/wiki/Do_Not_Disturb_Registry';
        var link2 = '<?php echo base_url(); ?>admin/credits';
        var is_valid = $('#sche_valid').val();
        var couns_id = $('#couns_id').val();
        var user_id = $('#sche_user').val();
        var description = $('#user_description').val();
        var date = $('.datetimepicker_re').data('date');
        var start = ""
        var end = ""
        var video_call = "";
        var schedule_id = 0;
        var sche_phone = "";
        var set_video = $('#video_enable').val();
        if(set_video == 1)
        {   
            var value;
            if (document.getElementById('r1').checked) {
                value = document.getElementById('r1').value;
            }
            else if (document.getElementById('r2').checked) {
                value = document.getElementById('r2').value;
            }
            if(value == "video")
                var video_call = 1;
            else if(value == "call")
                var video_call = 0;
        }
        else {
            var video_call = 0;
        }
        
        var rid = $('#user_role').val();
        var ret = true;
        if(rid == 3)
        {
            swal("Oops!", "Logged as counselor !!! Not authorized to schedule", "error")
            ret = false;
        }
        if (date == null) 
        {
            $("#datepicker_emp").css('border', '2px solid red');
            $("#datepicker_emp").parent().next('span').text('Please pick Date from datepicker').show();
            //$("#datepicker_emp").parent().addClass('error');
            ret = false;
        }
        if ($('#startTime').val() == "") 
        {
            $("#startTime").parent().addClass('error');
            $("#startTime").parent().next('span').text('Please Select Available time').show();
            ret = false;
        }
        else
        {
            var start = date + " " + getTime1($('#startTime').val())
            var end = date + " " + getTime1($('#endTime').val())
        }
        if ($('#endTime').val() == "") {
            $("#endTime").parent().addClass('error');
        }
        if (description == "") {

            $("#user_description").css('border', '2px solid red');
            $("#user_description").parent().next('span').show();
            ret = false;
        }
        if (user_id == 0 && ret != false) 
        {

            $('#modal-1').modal('show')
            ret = false;
        }
        if (is_valid != 0) {
            ret = false;
        }
        
        if (ret != false) {
                $.ajax({
                    url: '<?php echo base_url(); ?>calendar/addEvent',
                    data: {con_id: couns_id, user_id: user_id, description: description, start: start, end: end, schedule_id: schedule_id, videocall: video_call, sche_phone: sche_phone},
                    type: 'post',
                    success: function (data) {
                        console.log(data);
                        if (data == 1) {

                            swal({title: "Success", text: "Your appointment has been scheduled . If you have enabled <a target='_blank' href=" + link + " >DND </a> in your Number, Please give a missed Call to 08039534067 ", html: true, type: "success", closeOnConfirm: false, closeOnCancel: false, allowOutsideClick: false},
                                function () {
                                    location.reload();
                                }
                            );


                        } else {
                        //swal("Oops!", "Something went wrong! Please check if you have enough balance to schedule this appointment", "error")
                            swal({title: "oops", text: "Something went wrong! Please check if you have enough balance to schedule this appointment . Press <a href=" + link2 + " >CREDIT </a> to purchase your coupon code !!!", html: true, type: "error", closeOnConfirm: false, showCancelButton: false, showConfirmButton: false},
                                function () {
                                    location.reload();
                                }
                            );
                        }
                    }
                });
        }
    });
    function update_time_ends(val) {
        $('#time_slot_ends').val(new Date(new Date($('#start').val()).getTime() + val * 60000));
        $('#end').val(moment(new Date(new Date($('#start').val()).getTime() + val * 60000)).format('YYYY-MM-DD HH:mm:ss'));
    }

    /*$('#schedule-appointment1').on('click', function () {

        var link = 'https://en.wikipedia.org/wiki/Do_Not_Disturb_Registry';
        var link2 = '<?php echo base_url(); ?>home/purchasecode';
        var is_valid = $('#sche_valid').val();
        var couns_id = $('#couns_id').val();
        var user_id = $('#sche_code').val();
        var description = $('#user_description').val();
        var date = $('.datetimepicker_re').data('date')
        var start = date + " " + getTime1($('#startTime').val())
        var end = date + " " + getTime1($('#endTime').val())
        var schedule_id = 0;
        var ret;
        var sche_phone = $('#sche_phone').val();

        if (!phonenumber(sche_phone)) {
            $('#sche_phone').addClass("error");
            $('#sche_phone').attr("placeholder", "Please enter phone number");

            ret = false;
        }




        if (is_valid != 0) {
            ret = false;
        }
        if (ret != false) {
            $.ajax({
                url: '<?php echo base_url(); ?>calendar/addEvent',
                data: {con_id: couns_id, user_id: user_id, description: description, start: start, end: end, schedule_id: schedule_id, sche_phone: sche_phone},
                type: 'post',
                success: function (data) {
                    if (data == 1) {

//$('*').unbind('click');+
// $('body > div.showSweetAlert').find('.confirm').bind('click', function(){
//     location.reload();
//    });
                        swal({title: "Success", text: "Your appointment has been scheduled . If you have enabled <a target='_blank' href=" + link + " >DND </a> in your Number, Please give a missed Call to 08039534067 ", html: true, type: "success", closeOnConfirm: false, closeOnCancel: false, allowOutsideClick: false},
                                function () {
                                    location.reload();
                                }
                        );



                    } else if (data == 123) {
                        swal({title: "Oops", text: "Something went Wrong . You have enabled <a target='_blank' href=" + link + " >DND </a> in your Number, Please give a missed Call to 08039534067 ", html: true, type: "info", closeOnConfirm: false, closeOnCancel: false, allowOutsideClick: false},
                                );
                    } else {
                        swal({title: "oops", text: "Something went wrong! Please check if you have enough balance to schedule this appointment . Press <a href=" + link2 + " >CREDIT </a> to purchase your coupon code !!!", html: true, type: "error", closeOnConfirm: false, showCancelButton: false, showConfirmButton: false},
                                function () {
                                    location.reload();
                                }
                        );
                    }
                }
            });
        }
    });*/
    
    $('#schedule-appointment1').on('click', function () {

        var set_video = $('#video_enable').val();
        var link = 'https://en.wikipedia.org/wiki/Do_Not_Disturb_Registry';
        var link2 = '<?php echo base_url(); ?>home/purchasecode';
        var is_valid = $('#sche_valid').val();
        var couns_id = $('#couns_id').val();
        var user_id = $('#sche_code').val();
        var description = $('#user_description').val();
        var date = $('.datetimepicker_re').data('date')
        var start = date + " " + getTime1($('#startTime').val())
        var end = date + " " + getTime1($('#endTime').val())
        var schedule_id = 0;
        var ret;
        var sche_phone = $('#sche_phone').val();
        if(set_video == 1)
        {   
            var value;
            if (document.getElementById('r1').checked) {
                value = document.getElementById('r1').value;
            }
            else if (document.getElementById('r2').checked) {
                value = document.getElementById('r2').value;
            }
            if(value == "video")
                var video_call = 1;
            else if(value == "call")
                var video_call = 0;
        }
        else {
            var video_call = 0;
        }
        
        if (!phonenumber(sche_phone)) {
            $('#sche_phone').addClass("error");
            $('#sche_phone').attr("placeholder", "Please enter phone number");

            ret = false;
        }

        if (is_valid != 0) {
            ret = false;
        }
        
        if (ret != false) {
            
            $.ajax({
                url: '<?php echo base_url(); ?>calendar/addEvent',
                data: {con_id: couns_id, user_id: user_id, description: description, start: start, end: end, schedule_id: schedule_id, videocall: video_call, sche_phone: sche_phone},
                type: 'post',
                success: function (data) {
                    console.log(data);
                    if (data == 1) {

                        swal({title: "Success", text: "Your appointment has been scheduled . If you have enabled <a target='_blank' href=" + link + " >DND </a> in your Number, Please give a missed Call to 08039534067 ", html: true, type: "success", closeOnConfirm: false, closeOnCancel: false, allowOutsideClick: false},
                            function () {
                                location.reload();
                            }
                        );

                    } else if (data == 123) {
                        
                        swal({title: "Oops", text: "Something went Wrong . You have enabled <a target='_blank' href=" + link + " >DND </a> in your Number, Please give a missed Call to 08039534067 ", html: true, type: "info", closeOnConfirm: false, closeOnCancel: false, allowOutsideClick: false},
                        );
                    
                    } else {
                          
                        swal({title: "oops", text: "Something went wrong! Please check if you have enough balance to schedule this appointment . Press <a href=" + link2 + " >CREDIT </a> to purchase your coupon code !!!", html: true, type: "error", closeOnConfirm: false, showCancelButton: false, showConfirmButton: false},
                            function () {
                                location.reload();
                            }
                        );
                    }
                }
            });
        }
    });    

    function checkCodeValidnew(value) {
        var val = $.trim(value)
        $.ajax({
            url: '<?php echo base_url(); ?>home/checkCodeValid',
            data: {code: val},
            type: 'post',
            success: function (data) {
                //console.log(data);
                if (data != 1) {
                    $('#sche_valid').val(1);
                    swal("Oops!", "Please check the code you have entered", "error")
                    document.getElementById('cap').style.display = "block";
                } else {
                    $('#sche_valid').val(0);
                    $('#sche_user').val(value)
                }
            }
        });
    }
    
    function checkCodeValid(value) {
        var val = $.trim(value);
        if(val == "IIMK-P1"){

            swal("Oops!", "Please check the code you have entered", "error");
        }
        else{

            $.ajax({
                url: '<?php echo base_url(); ?>home/checkCodeValid',
                data: {code: val},
                type: 'post',
                success: function (data) {
               
                    if (data != 1) {
                        $('#sche_valid').val(1);
                        swal("Oops!", "Please check the code you have entered", "error")
                        document.getElementById('cap').style.display = "block";
                    } else {
                        $('#sche_valid').val(0);
                        $('#sche_user').val(value)
                    }
                }
            });
        }
        
    }

    $('#userbalcheck').on('click', function () {

        var link = '<?php echo base_url(); ?>admin/credits';

<?php
if ($this->session->userdata('loggedIn') == true) {
    $userid = $this->session->userdata('userData');
    ?>
            var balnce_id = '<?php echo $userid->id ?>'

            $.ajax({

                url: '<?php echo base_url(); ?>home/Userbalcheck',
                data: {bal_id: balnce_id},
                type: 'post',
                dataType: 'json',
                success: function (data) {
                  

                    tbl_row = "";
                    if (data != 1) {
                        if (data.length > 0) {
                            $.each(data, function (index, value) {
                                tbl_row = '<h1 style="font-size:16px" class="text-center">Your credit balance is</h1>\n\
                                                                                        <h1 style="font-weight: 800" class="text-center"> Rs ' + value.coupon_balance + '</h1></div>';
                            });
                            $("#credit_balance_user").html(tbl_row);
                            $('#user-balance_check-popup').modal('show');
                        }

                        return false;
                    } else {
                        swal({title: "oops", text: "Your balance is 0 . Press <a href=" + link + " >CREDIT </a> to purchase your coupon code !!!", html: true, type: "error", closeOnConfirm: false, showCancelButton: false, showConfirmButton: false},
                                function () {
                                    location.reload();
                                }
                        );
                    }
                }
            });

<?php } ?>
    });

    function balcheck() {

        var user_id = $('#test').val();
//        var response = grecaptcha.getResponse();
//        alert(response);
        document.getElementById('cap').style.display = "block";
        var ret = true;
        if (user_id == "") {

            $('#test').parent().parent().addClass("error");
            $('#test').attr("placeholder", "Please enter valid purchase code");
            ret = false;
        }
        if (grecaptcha.getResponse() == "") {
            //document.getElementById('captcha').innerHTML = "You can't leave Captcha Code empty";
            ret = false;
        }
        if (user_id == "IIMK-P1") {

            swal("Oops!", "Your coupon code is not valid!!!", "error");
            ret = false;
        }

        if (ret != false) {

            $.ajax({
                url: '<?php echo base_url(); ?>home/balcheck',
                data: {uid: user_id},
                type: 'post',
                dataType: 'json',
                success: function (data) {

                    //alert(data[0].purchase_date);

//                    var date = data[0].purchase_date;
//                    alert('the original date is '+date);
//                    var newdate = new Date(date);
//                    newdate.setDate(newdate.getDate() + 180);
//                    var nd = new Date(newdate);
//                    alert('the new date is '+nd);
//                    

                    tbl_row = "";
                    if (data != 1) {
                        if (data.length > 0) {
                            $.each(data, function (index, value) {
                                var dateString = value.purchase_date,
                                        date = new Date(dateString);


                                var numDays = 180;
                                date.setDate(date.getDate() + parseInt(numDays));

                                var dateEnd = date.getDate() + '/' + (date.getMonth() + 1) + '/' + date.getFullYear();

                                tbl_row += '<h1 style="font-size:16px" class="text-center">Your credit balance is</h1>\n\
                        <h1 style="font-weight: 800" class="text-center"> Rs ' + value.coupon_balance + '</h1>Expires On: <h4>' + dateEnd + ' </h4></div>';
                            });
                            $('#purchase_code_form').hide();

                            $('#credit_balance').show();
//                    } else {
//                        $('#test').parent().parent().addClass("error");
//                        $('#test').attr("placeholder", "Please enter valid purchase code");
                        }
                        $("#credit_balance1").html(tbl_row);
                        return false;
                    } else {
                        swal("Oops!", "Your coupon code is not valid!!!", "error")
                    }
                }
            });
        }
    }

    $(document.body).on('hidden.bs.modal', function () {
        $('#myModal').removeData('bs.modal')
    });
    $(document).ready(function () {
        $('.modal').on('hidden.bs.modal', function () {
            $(this).find('form')[0].reset();
            $('#sche_username').attr("placeholder", "Enter Purchase Code");
            document.getElementById("credit_balance").style.display="none";
            document.getElementById("purchase_code_form").style.display="block";
            $(".captchadiv").hide();
            grecaptcha.reset(); 
        });
    });

    // select counselor from their available date
    $('#datetimepicker1').datetimepicker({useCurrent: false, format: 'DD/MM/YYYY', minDate: minStartDate}).on('dp.change', function (e) {
        var formatedValue = e.date.format(e.date._f);

        var weekday = ["sunday", "monday", "tuesday", "wednesday", "thursday", "friday", "saturday"];
        var a = new Date(e.date);
        var day = (weekday[a.getDay()]);


        $.ajax({
            type: "POST",
            url: "<?php echo base_url() ?>home/daydate",
            data: {id: day},
            success: function (record) {

                tbl_row = "";
                if (record.length > 7) {

                    $.each($.parseJSON(record), function (index, value1) {
                        $.each(value1, function (index, value) {
                            if (value.photo != "") {
                                var photo = value.photo
                            } else {
                                var photo = "<?php echo base_url() . 'uploads/images/user.jpg' ?>";
                            }
                            
                            if (value.video_call == 1){
                                var video = '<div class="video"><i class="fa fa-video-camera"></i>Video Call Available</div>';
                            } else {
                                var video = "";
                            }

                            var dom = JSON.parse(value.domain);
                            tbl_row += '<div class="grid-content"  data-id="1346"><div class="job-item featured-item">\n\
                                              <div class="job-image"><img alt="AOEVN" src="' + photo + '" class="avatar avatar-full photo" width="408" height="401" /></div>\n\
                                                <div class="job-info"><h3 class="job-title"><a>' + value.username + '</a></h3>\n\
                                                     <div class="info-company"><div class="company"><i class="fa fa-suitcase"></i>\n\
                                                        <a >' + dom + '</a></div><div class="address"><i class="fa fa-cogs"></i>' + value.experience + ' years Exp</div>' + video + '\n\
                                                           <div class="sallary" style="display: inline-flex"><i class="fa fa-inr"></i>Rs ' + value.amount + ' for ' + value.minutes + ' minutes<div class="help-tip"><p>Please note: 1st session will start with an assessment.</p></div></div></div>\n\
                                                               <span class="job-featured"><a class="type-name" onclick="set_user_session('+value.id+')"></a></span>\n\
                                                                <div class="job-type full-time">\n\
<?php if ($this->session->userdata("userData")) { ?><a class="type-name" onclick="set_user_session('+value.id+')">Schedule</a><?php } else { ?>\n\
                                                                                        <a class="type-name" onclick="set_user_session('+value.id+')">Schedule</a><?php } ?>\n\
                                                            </div></div><div class="job-featured"></div></div></div>';
                        });

                    });
                } else {

                    tbl_row += '<div class="alert alert-info"><strong>Info!</strong> No Counselors available.</div>';
                }

                $("#cn_content").html(tbl_row);
            }

        });

    });




    $('#now_btn').on('click', function () {

        var button_text = $('#now_btn').text();
        if (button_text == "Now") {
            $('#now_btn').text("All");
        } else {
            $('#now_btn').text("Now");
        }
        $.ajax({
            type: "POST",
            url: "<?php echo base_url() ?>home/now",
            data: {text: button_text},

            success: function (record) {

                tbl_row = "";
                if (record.length > 7) {
                   
                    $.each($.parseJSON(record), function (index, value1) {
                       
                        $.each(value1, function (index, value) {
                            if (value.photo != "") {
                                var photo = value.photo
                            } else {
                                var photo = "<?php echo base_url() . 'uploads/images/user.jpg' ?>";
                            }
                            
                            if (value.video_call == 1){
                                var video = '<div class="video"><i class="fa fa-video-camera"></i>Video Call Available</div>';
                            } else {
                                var video = "";
                            }
                            var dom = JSON.parse(value.domain);
                            tbl_row += '<div class="grid-content"  data-id="1346"><div class="job-item featured-item">\n\
                                              <div class="job-image"><img alt="AOEVN" src="' + photo + '" class="avatar avatar-full photo" width="408" height="401" /></div>\n\
                                                <div class="job-info"><h3 class="job-title"><a >' + value.username + '</a></h3>\n\
                                                     <div class="info-company"><div class="company"><i class="fa fa-suitcase"></i>\n\
                                                        <a >' + dom + '</a></div><div class="address"><i class="fa fa-cogs"></i>' + value.experience + ' years Exp</div>' + video + '\n\
                                                             <div class="sallary" style="display: inline-flex"><i class="fa fa-inr"></i>Rs ' + value.amount + ' for ' + value.minutes + ' minutes<div class="help-tip"><p>Please note: 1st session will start with an assessment.</p></div></div></div>\n\
                                                             <span class="job-featured"><a class="type-name" onclick="set_user_session('+value.id+')"  ></a></span>\n\
                                                                <div class="job-type full-time">\n\
<?php if ($this->session->userdata("userData")) { ?><a class="type-name" onclick="set_user_session('+value.id+')" >Schedule</a><?php } else { ?>\n\
                                                                <a class="type-name" onclick="set_user_session('+value.id+')">Schedule</a><?php } ?>\n\
                                                            </div></div><div class="job-featured"></div></div></div>';
                        });

                    });
                } else {

                    tbl_row += '<div class="alert alert-info"><strong>Info!</strong> No Counselors available.</div>';
                }


                $("#cn_content").html(tbl_row);
            }
        });
    });


    $('#reschedule_popup').on('click', function () {
        $('#reschedule_purchase_code1').show();
        $('#reschedule').hide();
        $('#reschedule_confirm').hide();

    });

    $('#reschedule_submit').on('click', function () {

        var purchase_code = $.trim($('#purchase_code').val());
        $('#purchase_code_hdn').val(purchase_code)
        var ret = true;
        if (purchase_code == "") {

            $('#purchase_code').parent().parent().addClass("error");
            $('#purchase_code').attr("placeholder", "Please check your coupon code");
            ret = false;
        }
        if(purchase_code == "IIMK-P1")
        {
            swal("Oops!", "You have no appointments scheduled", "error");
            ret = false;
        }
        if (ret != false) {

            $.ajax({
                type: "POST",
                url: "<?php echo base_url() ?>home/get_scheduled_counselor",
                dataType: 'json',
                cache: false,
                data: {purchase_code: purchase_code},
                success: function (records) {

                    tbl_row = "";
                    if (records!="") {
                        var i = 1;
                        $.each(records, function (index, value) {
                            if (i == 1) {
                                var flag = "active";
                            } else {
                                flag = "";
                            }
                            if (value.photo != "") {
                                var photo = value.photo
                            } else {
                                var photo = "<?php echo base_url() . 'uploads/images/user.jpg' ?>";
                            }

                            tbl_row += '<div class="item ' + flag + '"><input type="hidden" value="' + value.current_schedule_id + '" class="sche_id_hdn"><input type="hidden" value="' + value.id + '" class="user_id_hdn"><input type="hidden" value="' + value.username + '" class="user_name_hdn">\n\
                <div class="grid-content" style="background-color:rgb(241, 243, 242);margin-bottom: 10px" data-id="1346">\n\    <div style="padding:0" class="job-item featured-item">\n\
                                              <div class="job-image"><img alt="AOEVN" src="' + photo + '" class="avatar avatar-full photo" width="408" height="401" /></div>\n\
                                                <div class="job-info">\n\
<h3 class="job-title">' + value.username + '</h3>\n\
                                                     <div class="info-company">\n\
<div class="company">Scheduled date & time</div>\n\
\n\<div class="sallary"><i class="fa fa-calendar"></i>' + value.date + '</div>\n\
<div class="address"><i class="fa fa-clock-o"></i>' + formatAMPM(value.time_starts) + ' - ' + formatAMPM(value.time_ends) + '</div>\n\
                                                         </div></div>\n\
<div class="job-featured">\n\
</div>\n\
</div>\n\
</div>\n\
</div>\n\
';


                            i++;
                        });
                        $("#carousel_inner").html(tbl_row);
                        $('#reschedule_purchase_code1').hide();
                        $('#reschedule').show();

                    } else {
                        swal("Oops!", "You have no appointments scheduled", "error")
                    }



                }
            });


            return false;
        }
    });
    $('#reschedule_btn').on('click', function () {

        var schedule_id = $('div .active').find('.sche_id_hdn').val();
        var sche_user_id = $('div .active').find('.user_id_hdn').val();

        var sche_user_name = $('div .active').find('.user_name_hdn').val();
        $('#current_schedule_id').val(schedule_id)
        $('.user_id').val(sche_user_id)
        $.ajax({
            url: '<?php echo base_url(); ?>home/getCounselorDetails',
            data: {id: sche_user_id},
            type: 'post',
            dataType: "json",
            success: function (data) {


                var tbl_row_timing = "";
                $('#re_sche_username').val(sche_user_name)
                if (data.timing.length > 0) {

                    $.each(data.timing, function (index, value) {
                        var timing = value.time_starts + ' - ' + value.time_ends
                        tbl_row_timing += '<option value=' + value.id + '>' + timing + '</option>';

                    });
                } else {
                    tbl_row_timing += '<div class="alert"><span class="closebtn" onclick="this.parentElement.style.display="none";">&times;</span> No Data Found</div>';
                }

                $(".counselor_availabilty").html(tbl_row_timing);

                $('#reschedule').hide();
                $('#reschedule_confirm').show();
                return false;
            }
        });
    });

   $('#reschedule_confirm_btn').on('click', function () {

        var current_schedule_id = $('#current_schedule_id').val();
        var sche_user_id = $('.user_id').val();
        var description = $('#user_re_reason').val();
        
        var visitor_info = $('#purchase_code_hdn').val()
         var date = $('#datetimepicker_re_popup').data('date')
         
       
        var schedule_id = 0;
        var ret = true;
         if (date == "") {

            $("#datetimepicker_re_popup").css('border', '2px solid red');
            $("#datetimepicker_re_popup").parent().next('span').show();
            ret = false;
        }
        if ($('#startTime_re1').val() == "") {



            $("#startTime_re1").parent().addClass('error');
            ret = false;
        }else{
            var start = date + " " + getTime1($('#startTime_re1').val())
        var end = date + " " + getTime1($('#endTime_re1').val())
        }
        if (description == "") {

            $("#user_description").css('border', '2px solid red');
            $("#user_description").parent().next('span').show();
            ret = false;
        }
        if (ret != false) {
        $.ajax({
            url: '<?php echo base_url(); ?>home/userReschedule',
            data: {id: current_schedule_id, user_id: sche_user_id, description: description,  start: start, end: end, schedule_id: schedule_id, visitor_info: visitor_info},
                 
            type: 'post',
            dataType: "json",
            success: function (data) {
                if (data == 1) {
                    swal({title: "Success", text: "Your appointment has been rescheduled", type: "success"},
                            function () {
                                location.reload();
                            }
                    );
                }
            }
        });
        }
    });

    function get_url() {
        var url = "<?php echo current_url(); ?>";

        return url;
    }

    function sheduleDetials(schedule_id, visitor_info, start, sche_id) {


        $.ajax({
            url: '<?php echo base_url(); ?>admin/scheduleInfo',
            data: {schedule_id: schedule_id, visitor_info: visitor_info, start: start, sche_id: sche_id},
            type: 'post',
            dataType: "json",
            success: function (data) {
                $('#_user_name').text(data.visitor_name)
                $('#_date').text(data.couns_name)
                $('#_status').text(data.date)

                $('#_time_slot').text(formatAMPM(data.time_starts) + ' - ' + formatAMPM(data.time_ends))
                $('.show_event_popup').modal('show');
            }
        });
    }


    function generate_code() {
        var ret = true;
        var link = 'https://en.wikipedia.org/wiki/Do_Not_Disturb_Registry';
        var reg_mobile_number = $('#reg_mobile_number').val();

        if (reg_mobile_number == "") {
            $('#reg_mobile_number').parent().parent().addClass("error");
            $('#reg_mobile_number').attr("placeholder", "Enter Phone");
            ret = false;
        }

        if (!phonenumber(reg_mobile_number)) {
            $('#reg_mobile_number').parent().parent().addClass("error");
            $('#reg_mobile_number').attr("placeholder", "Enter Phone");
            ret = false;
        }
        if (ret != false) {

            swal({
                title: "Are you sure?",
                text: "If you have enabled <a target='_blank' href=" + link + " >DND </a> in your Number, Please give a missed Call to 08039534067",
                type: "info",
                html: true,
                showCancelButton: true,
                confirmButtonColor: '#ff770f',
                confirmButtonText: 'Send OTP',
                cancelButtonText: "Cancel",
                
            },
                    function (isConfirm) {
                        if (isConfirm) {
                            $.ajax({
                                url: '<?php echo base_url(); ?>user/generate_code',
                                data: {phone: reg_mobile_number},
                                type: 'post',
                                dataType: "json",
                                success: function (data) {
                                    $('.generate_otp_span').show();
                                    $('#reg_mobile_number').parent().parent().removeClass("error");
                                }
                            });

                        } else {
                            swal("Cancelled");
                        }
                    });


        }


    }
    function scheduleCouns(id) {
        $.ajax({
            url: '<?php echo base_url(); ?>admin/checkCounsData',
            data: {id: id},
            type: 'post',
            success: function (data) {

                if (data == 1) {

                    window.location.href = '<?php echo base_url(); ?>admin/schedule/' + id;
                } else {
                    swal("Oops!", "Counselor renumeration and time slot is not entered!! Check if counselor is approved", "error")
                }
            }
        });
    }



    $('ul.pagination-job li a').click(function (e)
    {

        window.location.href = $(this).attr('href');
    });

    $(function () {
        var url = "<?php echo current_url(); ?>";


        var value = url.substring(url.lastIndexOf('/') + 1);

        if (value == "") {
            $('.iw-main-menu').find('a.home').closest("li").addClass('current-menu-ancestor');
        } else {
            $('.iw-main-menu').find('a.' + value).closest("li").addClass('current-menu-ancestor');
        }


    });
    
    function showmsg()
    {
        $('#msg_submit').fadeIn();
        $('#msg_submit').html("");
  
        $('#msg_submit').append("Passwords must contain at least 6 characters,including one special character and numbers.!");
        $('#msg_submit').fadeOut(10000);
    }
    
    function check_passwrd(pwd){
        //alert(pwd);
        var passwrd=  /^(?=.*[0-9])(?=.*[!@#$%^&*])[a-zA-Z0-9!@#$%^&*]{6,15}$/;
        if(!pwd.match(passwrd)) 
        { 
            $('#reg_password').parent().parent().addClass("error");
            $('#reg_password').attr("placeholder", "Please enter valid password");
            showmsg();
            
        }
        else
        { 
            $('#reg_password').parent().parent().removeClass("error");
        }
        
    }

    function checkAmountValid(value) {
        var remainder = value % 200;
        if (remainder == 0) {
            $('#your_amount').removeClass("error");

            $('#gen_btn').prop('disabled', false);
        } else {
            $('#your_amount').addClass("error");
            $('#your_amount').val('');
            $('#your_amount').attr("placeholder", "Please enter amount as multiples of 200");

            $('#gen_btn').prop('disabled', true);
        }
    }
    $(document).ready(function () {
        var url = "<?php echo current_url(); ?>";

<?php
if ($this->session->userdata('loggedIn') == true) {
    $userid = $this->session->userdata('userData');
    ?>
            var id = '<?php echo $userid->id ?>'
<?php } else { ?>
            var id = "";
<?php } ?>

       
        var arr = url.split('/');
        if (id != "") {

            if (arr[arr.length - 2] != "home" || arr[arr.length - 1] == "schedule") {


                $.ajax({
                    url: '<?php echo base_url(); ?>admin/checkPhoneNumber',
                    data: {id: id},
                    type: 'post',
                    success: function (data) {
                        if (data == 0) {
                            $('#modal_reg_phone').modal({backdrop: 'static', keyboard: false})
                            $('#modal_reg_phone').modal('show');
                        }
                    }

                });
            }

        }

    });


    $('#update_phone').on('click', function () {
        var ret = true;
        var user_reg_phone = $('#user_reg_phone').val();
        var filter = /^\d*(?:\.\d{1,2})?$/;
        //var link = 'https://en.wikipedia.org/wiki/Do_Not_Disturb_Registry';
        if (filter.test(user_reg_phone)) {
            if (user_reg_phone.length == 10) {
                ret = true;
            } else {
                $('#user_reg_phone').addClass("error");
                $('#user_reg_phone').parent().next('span').show();
                ret = false;
            }
        } else {
            $('#user_reg_phone').addClass("error");
            $('#user_reg_phone').parent().next('span').show(); 
            ret = false;
        }

        var user_reg_phone = $('#user_reg_phone').val();
        if (user_reg_phone == "") {
            $('#user_reg_phone').addClass("error");
            $('#user_reg_phone').attr("placeholder", "Please enter your phone number");
            ret = false;
        }
        if (ret != false) {
            $.ajax({
                url: '<?php echo base_url(); ?>admin/updatePhoneNumber',
                data: {user_reg_phone: user_reg_phone},
                type: 'post',
                success: function (data) {
                    location.reload();
                }


            });
        }
    });
    $('#cancel_sch_popup').on('click', function () {
        location.reload();
    })
    
      $('#couns_name').on('change', function () {
        var couns_id = $(this).val();


        $.ajax({
            url: '<?php echo base_url(); ?>admin/getCounsTimeSlot',
            data: {id: couns_id},
            type: 'post',
            dataType: "json",
            success: function (data) {

                if (data != "") {

                    
                    var time = data.timeslot;
                    var hours = time.split('-');

                    var minTime = getTime1(hours[0])
                    var maxTime = getTime1(hours[1])


                    $('#startTime').timepicker({
                        'minTime': minTime,
                        'maxTime': maxTime,
                        'step': 15,
                        'timeFormat': 'h:i A',

                    })
                    $('#startTime').trigger('change')

//                      

                    $('#counse_ph').val(data.phone)
                    $('#dur_time').val(data.duration)
                }

            }
        });
    });
    $('#user_name').on('change', function () {
        var user_id = $(this).val();
        $.ajax({
            url: '<?php echo base_url(); ?>admin/getPhone',
            data: {id: user_id},
            type: 'post',
            dataType: "json",
            success: function (data) {
                $('#user_ph').val(data.phone)

            }
        });
    })

    $('#quickschedule').on('click', function () {

        var link = 'https://en.wikipedia.org/wiki/Do_Not_Disturb_Registry';
        var is_valid = $('#sche_valid').val();
        var couns_id = $('#couns_name').val();
        if ($('#registered_user').is(':checked')) {
            var user_id = $('#user_name').val();
        } else if ($('#anonymous_user').is(':checked')) {
            var user_id = $('#user_code').val();
            //var user_id = $('#user_ph').val();
        }

        var description = $('#quick_desc').val();
        var video_call = 0;
        var d = new Date();

        var month = d.getMonth() + 1;
        var day = d.getDate();

        var output = d.getFullYear() + '-' +
                (('' + month).length < 2 ? '0' : '') + month + '-' +
                (('' + day).length < 2 ? '0' : '') + day;


        var date = output;

        var start = date + " " + getTime1($('#startTime_quick').val())
        var end = date + " " + getTime1($('#endTime_quick').val())
        var schedule_id = 0;
        var ret;
        var sche_phone = $('#user_ph').val();

        if (!phonenumber(sche_phone)) {
            $('#user_ph').addClass("error");
            $('#user_ph').attr("placeholder", "Please enter phone number");

            ret = false;
        }

        if (ret != false) {
            $.ajax({
                url: '<?php echo base_url(); ?>calendar/addEvent',
                data: {con_id: couns_id, user_id: user_id, description: description, start: start, end: end, schedule_id: schedule_id, videocall: video_call, sche_phone: sche_phone},
                type: 'post',
                success: function (data) {
                    if (data == 1) {

//$('*').unbind('click');+
// $('body > div.showSweetAlert').find('.confirm').bind('click', function(){
//     location.reload();
//    });
                        swal({title: "Success", text: "Your appointment has been scheduled . If you have enabled <a target='_blank' href=" + link + " >DND </a> in your Number, Please give a missed Call to 08039534067 ", html: true, type: "success", closeOnConfirm: false, closeOnCancel: false, allowOutsideClick: false},
                                function () {
                                    location.reload();
                                }
                        );



                   /* } else if (data == 123) {
                        swal({title: "Oops", text: "Something went Wrong . This Number " + sche_phone + " have enabled <a target='_blank' href=" + link + " >DND </a> in your Number, Please give a missed Call to 08039534067 ", html: true, type: "info", closeOnConfirm: false, closeOnCancel: false, allowOutsideClick: false},
                                );*/
                    } else if (data == 2) {
                        swal("Oops!", "Something went wrong! Please check if you have enough balance to schedule this appointment", "error")
                    }
                }
            });
        }
    });

    $('#call_now').on('click', function () {

        var link = 'https://en.wikipedia.org/wiki/Do_Not_Disturb_Registry';
        var is_valid = $('#sche_valid').val();
        var couns_id = $('#couns_name').val();
        if ($('#registered_user').is(':checked')) {
            var user_id = $('#user_name').val();
        } else if ($('#anonymous_user').is(':checked')) {
            var user_id = $('#user_code').val();
        }

        var description = $('#quick_desc').val();
        var video_call = 0;
        var d = new Date();

        var month = d.getMonth() + 1;
        var day = d.getDate();

        var output = d.getFullYear() + '-' +
                (('' + month).length < 2 ? '0' : '') + month + '-' +
                (('' + day).length < 2 ? '0' : '') + day;


        var date = output;
        var d = new Date($.now() + 2 * 60 * 1000); // current time + 60s
        var startt = d.getHours() + ':' + d.getMinutes() + ':00';
        var d = new Date($.now() + parseInt($('#dur_time').val()) * 60 * 1000);
        var endd = d.getHours() + ':' + d.getMinutes() + ':00';

        var start = date + " " + startt
        var end = date + " " + endd
        var schedule_id = 0;
        var ret;
        var sche_phone = $('#user_ph').val();

        if (!phonenumber(sche_phone)) {
            $('#user_ph').addClass("error");
            $('#user_ph').attr("placeholder", "Please enter phone number");

            ret = false;
        }


if (ret != false) {
            $.ajax({
                url: '<?php echo base_url(); ?>calendar/addEvent',
                data: {con_id: couns_id, user_id: user_id, description: description, start: start, end: end, schedule_id: schedule_id, videocall: video_call, sche_phone: sche_phone},
                type: 'post',
                success: function (data) {
                    if (data == 1) {

//$('*').unbind('click');+
// $('body > div.showSweetAlert').find('.confirm').bind('click', function(){
//     location.reload();
//    });
                        swal({title: "Success", text: "Your appointment has been scheduled . If you have enabled <a target='_blank' href=" + link + " >DND </a> in your Number, Please give a missed Call to 08039534067 ", html: true, type: "success", closeOnConfirm: false, closeOnCancel: false, allowOutsideClick: false},
                                function () {
                                    location.reload();
                                }
                        );



                    } else if (data == 123) {
                        swal({title: "Oops", text: "Something went Wrong . You have enabled <a target='_blank' href=" + link + " >DND </a> in your Number, Please give a missed Call to 08039534067 ", html: true, type: "info", closeOnConfirm: false, closeOnCancel: false, allowOutsideClick: false},
                                );
                    } else {
                        swal("Oops!", "Something went wrong! Please check if you have enough balance to schedule this appointment", "error")
                    }
                }
            });
        }

    });
    
    function validatePhone() {
            
            var phone = $('#user_ph').val();
            if ($('#user_ph').val() == "") {

                $('#user_ph').addClass("error");
                //$('#user_ph').css('border-color', 'red');
                $('#user_ph').attr("placeholder", "Please enter valid phone number");
                return false;
            }
            
            if(!phonenumber(phone)) {
                
                $('#user_ph').addClass("error");
                //$('#user_ph').css('border-color', 'red');
                $('#user_ph').attr("placeholder", "Please enter valid phone number");
                return false;
            }
            
    }   

    $('#payment-schedule').on('click',function(e){
        e.preventDefault();
        var form = $(this).parents('form');
        var link = 'https://en.wikipedia.org/wiki/Do_Not_Disturb_Registry';
        var phone = $('#user_ph').val();
        var id = $('#couns_id').val();
        var description = $('#user_description').val();
        var date = $('.datetimepicker_re').data('date')
        var start = date + " " + getTime1($('#startTime').val())
        var end = date + " " + getTime1($('#endTime').val())
        var schedule_id = 0;
        var set_video = $('#video_enable').val();
        if(set_video == 1) {
            
            var value;
            if (document.getElementById('r1').checked) {
                value = document.getElementById('r1').value;
            }
            else if (document.getElementById('r2').checked) {
                value = document.getElementById('r2').value;
            }
            if(value == "video")
                var video = 1;
            else if(value == "call")
                var video = 0;
        }
        else {
            var video = 0;
        }
        $.ajax({
                url: '<?php echo base_url(); ?>calendar/checkPhone',
                data: {phone: phone},
                type: 'post',
                success: function (data) {
                    //console.log(data);
                    if (data == 1) {
                       swal({
                            title: "Are you sure?",
                            text: "That you have enabled <a target='_blank' href=" + link + " >DND </a> in your Number ?, If so, Please give a missed Call to 08039534067 ",
                            html: true,
                            type: "info",
                            showCancelButton: true,
                            confirmButtonColor: "#DD6B55",
                            confirmButtonText: "OK",
                            allowOutsideClick: false,
                            closeOnConfirm: false
                        }, function(isConfirm){
                                if (isConfirm){
                                    $('#couns').val(id);
                                    $('#user_ph').val(phone);
                                    $('#descrptn').val(description);
                                    $('#date').val(date);
                                    $('#start').val(start);
                                    $('#end').val(end);
                                    $('#schedule_id').val(schedule_id); 
                                    $('#video_count').val(video);
                                    form.submit();
                                }
                        });
                        
                    }
                   
                }
        });
        
    });
    
    
    $('#cancel_sch_popup1').on('click', function () {
        location.reload();
    })

    $('#add_counselor').on('click',function(e){

        e.preventDefault();
        var ret = true;
        var couns_phone = $('#_iwj_phone').val();
        var filter = /^\d*(?:\.\d{1,2})?$/;
        var form = $(this).parents('form');
        if ($('#your_name').val() == "") {
            $('#your_name').addClass("error");
            $('#your_name').attr("placeholder", "Please Enter Counselor Name");
            ret = false;
        }
        if ($('#email').val() == "") {
            $('#email').addClass("error");
            $('#email').attr("placeholder", "Please Enter Counselor Email");
            ret = false;
        }

        if (filter.test(couns_phone)) {
            if (couns_phone.length == 10) {
                ret = true;
            } else {
                $('#couns_phone').addClass("error");
                $('#couns_phone').attr("placeholder", "Please Enter Valid Phone Number");
                ret = false;
            }
        } else {
            $('#couns_phone').addClass("error");
            $('#couns_phone').attr("placeholder", "Please Enter Valid Phone Number"); 
            ret = false;
        }

        if ($('#_iwj_phone').val() == "") {
            $('#_iwj_phone').addClass("error");
            $('#_iwj_phone').attr("placeholder", "Please Enter Valid Phone Number");
            ret = false;
        }
        if ($('#Couns_pwrd').val() == "") {
            $('#Couns_pwrd').addClass("error");
            $('#Couns_pwrd').attr("placeholder", "Please Enter Counselor Password");
            ret = false;
        }
        if ($('#certify').val() == "") {
            $('#certify').addClass("error");
            $('#certify').attr("placeholder", "Please Enter Proper Qualifications & Certifications");
            ret = false;
        }
        if ($('#accrediate').val() == "") {
            $('#accrediate').addClass("error");
            $('#accrediate').attr("placeholder", "Please Enter Proper Accreditations");
            ret = false;
        }

        if (ret != false) {
            form.submit();
        }

    });
    
    
</script>
<script type="text/javascript">

    $("form[name='contact11']").validate({
        // Specify validation rules
        rules: {
            // The key name on the left side is the name attribute
            // of an input field. Validation rules are defined
            // on the right side
            name: "required",

            pass6: {
                required: true,

            },

            email6: {
                required: true,
                // Specify that email should be validated
                // by the built-in "email" rule
                email: true
            },

        },
        // Specify validation error messages
        messages: {

            email6: "Please Enter a valid email address",

            pass6: "Please Enter Password"




        },
        // Make sure the form is submitted to the destination defined
        // in the "action" attribute of the form when valid
        submitHandler: function (form) {
            event.preventDefault();

            var form = $('#contactForm11')[0];

            var formData = new FormData(form);

            var base_url = $("#baseurl").val();

            // alert(base_url);

            $.ajax({
                url: base_url + "home/coupon_loginsubmit",
                type: 'POST',
                data: formData,
                processData: false,
                contentType: false,
                async: false,
                success: function (data) {
                    //  alert(JSON.stringify(data));

                    if (data == 1) {

                        $("#email6").val("");
                        $("#pass6").val("");


                        window.location.href = base_url + 'home/coupon_create';



                    } else if (data == 0) {




                        $('#succes').fadeIn();
                        $('#succes').html("");

                        $('#succes').append("Login Failed");

                        $('#succes').fadeOut(3000);

                    }




                },

            });

            return false;
        }
    });


    // creating coupon

    $("form[name='coupon11']").validate({
        // Specify validation rules
        rules: {
            // The key name on the left side is the name attribute
            // of an input field. Validation rules are defined
            // on the right side
            name: "required",

            amount12: {
                required: true,

            },

        },
        // Specify validation error messages
        messages: {

            amount12: "Please Enter an Amount"






        },
        // Make sure the form is submitted to the destination defined
        // in the "action" attribute of the form when valid
        submitHandler: function (form) {
            event.preventDefault();

            var form = $('#couponform')[0];

            var formData = new FormData(form);

            var base_url = $("#baseurl12").val();

            // alert(base_url);

            $.ajax({
                url: base_url + "home/coupon_creates",
                type: 'POST',
                data: formData,
                processData: false,
                contentType: false,
                async: false,
                success: function (data) {
                    //  alert(JSON.stringify(data));

                    if (data == 1) {

                        $("#amount12").val("");
                        $('#succes').fadeIn();
                        $('#succes').html("");

                        $('#succes').append("Coupon Added");

                        $('#succes').fadeOut(3000);



                    } else if (data == 0) {





                    }




                },

            });

            return false;
        }
    });


    $("form[name='notes11']").validate({
        // Specify validation rules
        rules: {
            // The key name on the left side is the name attribute
            // of an input field. Validation rules are defined
            // on the right side
            name: "required",

            domain: {
                required: true,

            },
            notes: {
                required: true,

            },

        },
        // Specify validation error messages
        messages: {

            domain: "Please select an issue",
            notes: "Please Enter Notes"






        },
        // Make sure the form is submitted to the destination defined
        // in the "action" attribute of the form when valid
        submitHandler: function (form) {
            event.preventDefault();

            var form = $('#notes11')[0];

            var formData = new FormData(form);

            var base_url = $("#baseurl22").val();

            // alert(base_url);

            $.ajax({
                url: base_url + "Counselor/counselor_insertnotes",
                type: 'POST',
                data: formData,
                processData: false,
                contentType: false,
                async: false,
                success: function (data) {
                    //   alert(JSON.stringify(data));
                    $("#issuelist").val("");
                    $("#notes").val("");
                    $(".userlist").empty();

                    $(".userlist").last().append(data);
                    $('#note_submit').fadeIn();
                    $('#note_submit').html("");
  
                    $('#note_submit').append("Details Submitted Successfully");
                    $('#note_submit').fadeOut(3000);

                },

            });

            return false;
        }
    });
    function getMintime() {
        return parseInt($('#min').val())
    }
    function getMaxtime() {
        return parseInt($('#max').val())
    }

    function formatAMPM(date) {
        date = new Date(date)
        var hours = date.getHours();
        var minutes = date.getMinutes();

        var ampm = hours >= 12 ? 'PM' : 'AM';
        hours = hours % 12;
        hours = hours ? hours : 12; // the hour '0' should be '12'
        minutes = minutes < 10 ? '0' + minutes : minutes;
        var strTime = hours + ':' + minutes + ' ' + ampm;
        return strTime;
    }
    function getTime(start, time) {
        var start = start


        var time = time;
        var hours = Number(time.match(/^(\d+)/)[1]);
        var minutes = Number(time.match(/:(\d+)/)[1]);
        var AMPM = time.match(/\s(.*)$/)[1];
        if (AMPM == "PM" && hours < 12)
            hours = hours + 12;
        if (AMPM == "AM" && hours == 12)
            hours = hours - 12;
        var sHours = hours.toString();
        var sMinutes = minutes.toString();
        if (hours < 10)
            sHours = "0" + sHours;
        if (minutes < 10)
            sMinutes = "0" + sMinutes;
        var time = sHours + ":" + sMinutes + ":00";

        return start + " " + time;

    }
    $(function () {

        $('#startTime_re').on('changeTime', function () {
            
            var maxTime = $('#max').val()
            var arr = getTime1($(this).val());
            var time = moment.utc(arr, 'hh:mm A').add('45', 'minutes').format('hh:mm A')
            var ftime = $(this).val();
            var date = $('.datetimepicker_re3').data('date');
            var couns = $('.user_id').val();
            $.ajax({
                url: '<?php echo base_url(); ?>admin/checkSchedule',
                data: {id: ftime, date: date, couns_id: couns},
                type: 'post',
                success: function (data) {
                    
                    if (data >= 1) {

                        $("#startTime_re").css('border', '2px solid red');
                        $("#startTime_re").parent().next('span').show();
                        $('#add-event').prop('disabled', true);
                        $('#save-event').prop('disabled', true);
                    } else {
                        $("#startTime_re").siblings(".select2-container").css('border', '');
                        $("#startTime_re").parent().next('span').hide();
                        $('#add-event').prop('disabled', false);
                        $('#save-event').prop('disabled', false);

                        $('#endTime_re').val(time)
                    }
                    
                }
            });

            
        });
        
        $('#startTime_quick').timepicker({
                'minTime': $('#min').val(),
                'maxTime': $('#max').val(),
                'step': 15,
                'showDuration': true,
                'timeFormat': 'h:i A',

        });
        $('#endTime_quick').attr('disabled',true)
        
        $('#startTime_re1').timepicker({
                'minTime': $('#min').val(),
                'maxTime': $('#max').val(),
                'step': 15,
                'timeFormat': 'h:i A',

            });
        $('#endTime_re1').attr('disabled',true)
            
        $('#startTime_re1').on('changeTime', function () {
            var arr = getTime1($(this).val());
            $('#endTime_re1').attr('disabled', true)
            var time = moment.utc(arr, 'hh:mm A').add("45", 'minutes').format('hh:mm A')

            $('#endTime_re1').val(time)
            //   var arr1 = arr.split(' ')
//            var url = $('.user_id').val()
//            if (url == "") {
//                url = $('#couns_name').val();
//            }
//            if (typeof url == "undefined") {
//                var url = $('#user_id').val()
//            }
//            $.ajax({
//                url: '<?php echo base_url(); ?>' + "home/getCounsRenum",
//                type: 'POST',
//                data: {url: url},
//
//                success: function (data) {
//                    var data_arr = data.split("-");
//                    var time_arr = data_arr[1];
//                    var time = moment.utc(arr, 'hh:mm A').add(data_arr[0], 'minutes').format('hh:mm A')
//
//                    $('#endTime_re1').val(time)
//                    $('#endTime_re1').attr('disabled', true)
//                },
//
//            });
        });
        
        $('#startTime_quick').on('changeTime', function () {
            var arr = getTime1($(this).val());
            //   var arr1 = arr.split(' ')
            var url = $('.user_id').val()
            if (url == "") {
                url = $('#couns_name').val();
            }
            if (typeof url == "undefined") {
                var url = $('#user_id').val()
            }
            var weekday = ["sunday", "monday", "tuesday", "wednesday", "thursday", "friday", "saturday"];
            var day = (weekday[new Date().getDay()]);
            
            $.ajax({
                url: '<?php echo base_url(); ?>' + "home/getCounsRenumQuick",
                type: 'POST',
                data: {url: url,day:day},

                success: function (data) {
                    var data_arr = data.split("-");
                    var time_arr = data_arr[1];
                    var time = moment.utc(arr, 'hh:mm A').add(data_arr[0], 'minutes').format('hh:mm A')

                    $('#endTime_quick').val(time)
                    $('#endTime_quick').attr('disabled', true)
                },

            });
        });


        $('#startTime').on('changeTime', function () {
            var arr = getTime1($(this).val());
            var weekday = ["sunday", "monday", "tuesday", "wednesday", "thursday", "friday", "saturday"];
            var date = $('#datepicker_emp').data('date')
            var date_emp = new Date(date);
            var type = $('#sche_type').val();
            if(type == "employee"){
                var day = (weekday[date_emp.getDay()]);
            }else if(type == "quick"){
                var day = (weekday[new Date().getDay()]);
            }
            //var day = (weekday[date_emp.getDay()]);
            //alert(day);
            //   var arr1 = arr.split(' ')
            var url = '<?php echo $this->session->userdata('counselor_id') ?>'
            if (url == "") {
                url = $('#couns_name').val();
            }
            if(typeof url == "undefined"){
                url='<?php echo $this->uri->segment(3); ?>'
            }
            $.ajax({
                url: '<?php echo base_url(); ?>' + "home/getCounsRenum",
                type: 'POST',
                data: {url: url, day:day},

                success: function (data) {
                    //alert(data);
                    var data_arr = data.split("-");
                    var time_arr = data_arr[1];
                    var time = moment.utc(arr, 'hh:mm A').add(data_arr[0], 'minutes').format('hh:mm A')
                    //alert(time);
                    $('#endTime').val(time)
                    $('#endTime').attr('disabled', true)
                },

            });

            // $('#endTime').timepicker('option', 'minTime', $(this).val());


        });

        $('#endTime').on('changeTime', function () {
            var start = $('#startTime').val();
            
            if (typeof start != "undefined") {

                var arr = start.split(" ");
                var arr1 = arr[0];

            } else {
                var date = $('.datetimepicker_re').data('date')
                var start = date + " " + getTime1($('#startTime').val())
                var arr1 = $('.datetimepicker_re').data('date')

            }

            var time = $(this).val();
            var hours = Number(time.match(/^(\d+)/)[1]);
            var minutes = Number(time.match(/:(\d+)/)[1]);
            var AMPM = time.match(/\s(.*)$/)[1];
            if (AMPM == "PM" && hours < 12)
                hours = hours + 12;
            if (AMPM == "AM" && hours == 12)
                hours = hours - 12;
            var sHours = hours.toString();
            var sMinutes = minutes.toString();
            if (hours < 10)
                sHours = "0" + sHours;
            if (minutes < 10)
                sMinutes = "0" + sMinutes;
            var time = sHours + ":" + sMinutes + ":00";
            var end = arr1 + " " + time

            $.ajax({
                url: '<?php echo base_url(); ?>' + "calendar/checkTimeEnds",
                type: 'POST',
                data: {end: end, start: start},

                success: function (data) {
                    //console.log(data)
                    if (data == 1) {
                        $('#end').val(arr1 + " " + time)
                        $('#endTime').removeClass('error')
                        $('#endTime').parent().next('span').hide()
                    } else {
                        $('#endTime').addClass('error')
                        $('#endTime').parent().next('span').show();
                    }
                },

            });




        });
        $('#datetimepicker3').datetimepicker({
            format: 'LT',

        });
    });
</script>

<script type="text/javascript">


   
    $("form[name='star1']").validate({
        // Specify validation rules
        rules: {
            // The key name on the left side is the name attribute
            // of an input field. Validation rules are defined
            // on the right side
            name: "required",

            comment: {
                required: true,

            },

        },
        // Specify validation error messages
        messages: {

            comment: "Please leave a comment"


        },
        // Make sure the form is submitted to the destination defined
        // in the "action" attribute of the form when valid
        submitHandler: function (form) {
            event.preventDefault();

            var form = $('#star1')[0];

            var formData = new FormData(form);

            //    alert($("#cstart").val());

            var base_url = '<?php echo base_url(); ?>';


            $.ajax({
                url: base_url + "Home/insert_ratings",
                type: 'POST',
                data: formData,
                processData: false,
                contentType: false,
                async: false,
                success: function (data) {
                    // alert(JSON.stringify(data));

                    if(data == 2)
                    {
                    
                        swal("Oops!", "You have already rated this Appointment", "error")
                        
                    }
                    else
                    {
                        //   $("#domain").val($("#domain option:eq(0)").val());
                        $("#comment").val("");
                        $("#cstatus").val("");
                        $("#ratinglist").html("");
                        $("#ratinglist").html(data);
                    
                        $('#rate_submit').fadeIn();
                        $('#rate_submit').html("");
  
                        $('#rate_submit').append("Details Submitted Successfully");
                        $('#rate_submit').fadeOut(3000);

                    }
                    
                },

            });

            return false;
        }
    });
    function getTime1(time) {
        var time = time;
        var hours = Number(time.match(/^(\d+)/)[1]);
        var minutes = Number(time.match(/:(\d+)/)[1]);
       
        var AMPM = time.match(/\s(.*)$/)[1];
        if (AMPM == "PM" && hours < 12)
            hours = hours + 12;
        if (AMPM == "AM" && hours == 12)
            hours = hours - 12;
        var sHours = hours.toString();
        var sMinutes = minutes.toString();
       
        if (sHours < 10)
            sHours = "0" + sHours;
        if (sMinutes < 10)
            sMinutes = "0" + sMinutes;
        var time = sHours + ":" + sMinutes + ":00";

        return  time;
    }
    
</script>


</body>
</html>

