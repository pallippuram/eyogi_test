<!DOCTYPE html>
<html lang="en-US">
    <head>
        <!-- Global site tag (gtag.js) - AdWords: 813949002 -->
        <script async src="https://www.googletagmanager.com/gtag/js?id=AW-813949002"></script>
        <script>
            window.dataLayer = window.dataLayer || [];
            function gtag(){dataLayer.push(arguments);}
            gtag('js', new Date());

            gtag('config', 'AW-813949002');
        </script>

        <script>
            gtag('event', 'page_view', {
            'send_to': 'AW-813949002',
            'user_id': 'replace with value'
            });
        </script>
        <script>
            window.dataLayer = window.dataLayer || [];
            function gtag() {
                dataLayer.push(arguments);
            }
            gtag('js', new Date());

            gtag('config', 'UA-113236692-1');
        </script>
        <meta charset="UTF-8">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <link rel="profile" href="http://gmpg.org/xfn/11">

        <title>eYogi</title>
        <link rel='dns-prefetch' href='//www.google.com' />
        <link rel='dns-prefetch' href='//fonts.googleapis.com' />
        <link rel='dns-prefetch' href='//s.w.org' />

        <script type="text/javascript">
            window._wpemojiSettings = {"baseUrl": "https:\/\/s.w.org\/images\/core\/emoji\/2.3\/72x72\/", "ext": ".png", "svgUrl": "https:\/\/s.w.org\/images\/core\/emoji\/2.3\/svg\/", "svgExt": ".svg"};
            !function (a, b, c) {
                function d(a) {
                    var b, c, d, e, f = String.fromCharCode;
                    if (!k || !k.fillText)
                        return!1;
                    switch (k.clearRect(0, 0, j.width, j.height), k.textBaseline = "top", k.font = "600 32px Arial", a) {
                        case"flag":
                            return k.fillText(f(55356, 56826, 55356, 56819), 0, 0), b = j.toDataURL(), k.clearRect(0, 0, j.width, j.height), k.fillText(f(55356, 56826, 8203, 55356, 56819), 0, 0), c = j.toDataURL(), b !== c && (k.clearRect(0, 0, j.width, j.height), k.fillText(f(55356, 57332, 56128, 56423, 56128, 56418, 56128, 56421, 56128, 56430, 56128, 56423, 56128, 56447), 0, 0), b = j.toDataURL(), k.clearRect(0, 0, j.width, j.height), k.fillText(f(55356, 57332, 8203, 56128, 56423, 8203, 56128, 56418, 8203, 56128, 56421, 8203, 56128, 56430, 8203, 56128, 56423, 8203, 56128, 56447), 0, 0), c = j.toDataURL(), b !== c);
                        case"emoji4":
                            return k.fillText(f(55358, 56794, 8205, 9794, 65039), 0, 0), d = j.toDataURL(), k.clearRect(0, 0, j.width, j.height), k.fillText(f(55358, 56794, 8203, 9794, 65039), 0, 0), e = j.toDataURL(), d !== e
                    }
                    return!1
                }
                function e(a) {
                    var c = b.createElement("script");
                    c.src = a, c.defer = c.type = "text/javascript", b.getElementsByTagName("head")[0].appendChild(c)
                }
                var f, g, h, i, j = b.createElement("canvas"), k = j.getContext && j.getContext("2d");
                for (i = Array("flag", "emoji4"), c.supports = {everything:!0, everythingExceptFlag:!0}, h = 0; h < i.length; h++)
                    c.supports[i[h]] = d(i[h]), c.supports.everything = c.supports.everything && c.supports[i[h]], "flag" !== i[h] && (c.supports.everythingExceptFlag = c.supports.everythingExceptFlag && c.supports[i[h]]);
                c.supports.everythingExceptFlag = c.supports.everythingExceptFlag && !c.supports.flag, c.DOMReady = !1, c.readyCallback = function () {
                    c.DOMReady = !0
                }, c.supports.everything || (g = function () {
                    c.readyCallback()
                }, b.addEventListener ? (b.addEventListener("DOMContentLoaded", g, !1), a.addEventListener("load", g, !1)) : (a.attachEvent("onload", g), b.attachEvent("onreadystatechange", function () {
                    "complete" === b.readyState && c.readyCallback()
                })), f = c.source || {}, f.concatemoji ? e(f.concatemoji) : f.wpemoji && f.twemoji && (e(f.twemoji), e(f.wpemoji)))
            }(window, document, window._wpemojiSettings);
        </script>
        <style type="text/css">
            a,
            a label {
                cursor: pointer !important;
            }
            img.wp-smiley,
            img.emoji {
                display: inline !important;
                border: none !important;
                box-shadow: none !important;
                height: 1em !important;
                width: 1em !important;
                margin: 0 .07em !important;
                vertical-align: -0.1em !important;
                background: none !important;
                padding: 0 !important;
            }
            .iwj-field{
                border: 1px solid #eeeeee !important;
            }
            .contents-main{

                padding-top: 90px !important;       
            }
            li.active a{
                color:  rgb(145, 69, 30) !important
            }
            li.active i{
                color:  rgb(145, 69, 30) !important
            }
            .avatar-full{
                height:45px !important;
            }
            .select2{
                width:100%;
            }
            .select2-container--default{
                background-color: #d3d5d6;        
            }
            .select2-container{
                width: 100% !important;
            }
            .error{
                border: 1.5px solid red !important;
            }
            .choose_file{
                position: relative;
                display: inline-block;
                border-radius: 8px;
                border: #ebebeb solid 1px;
                width: 198px;
                padding: 19px 2px 12px 69px;
                font: normal 14px Myriad Pro, Verdana, Geneva, sans-serif;
                color: #fff;
                margin-top: 2px;
                background: rgb(50, 158, 210);
            }
            .choose_file input[type="file"]{
                -webkit-appearance:none; 
                position:absolute;
                top:0; left:0;
                opacity:0; 
            }
            .iwj-dashboard-main1 {
                float: right;
                width: calc(100% - 250px);
            }
            .iwj-dashboard-main-inner1 {
                padding: 50px;
                background: #eee;
                min-height: 630px;
            }
            #calendar{
                padding: 15px 52px;
            }

            .select2-results .select2-highlighted {
                background: green;
            }
            .btn-primary{
                position: relative!important;
                display: inline-block!important;
                border-radius: 8px!important;
                border: #FF9128 solid 1px!important;
                width: 198px!important;
                padding: 10px 2px 10px 2px!important;
                font: normal 14px Myriad Pro, Verdana, Geneva, sans-serif!important;
                color: #fff!important;
                margin-top: 2px!important;
                background: #FF9128!important;
            }
            [type="radio"]:checked,
            [type="radio"]:not(:checked) {
                position: absolute;
                left: -9999px;
            }
            [type="radio"]:checked + label,
            [type="radio"]:not(:checked) + label
            {
                position: relative;
                padding-left: 28px;
                padding-right: 100px;
                cursor: pointer;
                line-height: 20px;
                display: inline-block;
                color: #666;
                margin-top: 60px;
            }
            [type="radio"]:checked + label:before,
            [type="radio"]:not(:checked) + label:before {
                content: '';
                position: absolute;
                left: 0;
                top: 0;
                width: 16px;
                height: 16px;
                border: 2px solid #ff8e16;
                border-radius: 100%;
                background: #fff;
            }
            [type="radio"]:checked + label:after,
            [type="radio"]:not(:checked) + label:after {
                content: '';
                width: 8px;
                height: 8px;
                background: #ff8e16;
                position: absolute;
                top: 4px;
                left: 4px;
                border-radius: 100%;
                -webkit-transition: all 0.2s ease;
                transition: all 0.2s ease;
            }
            [type="radio"]:not(:checked) + label:after {
                opacity: 0;
                -webkit-transform: scale(0);
                transform: scale(0);
            }
            [type="radio"]:checked + label:after {
                opacity: 1;
                -webkit-transform: scale(1);
                transform: scale(1);
            }
            .fc-agendaWeek-view tr {
                height: 30px;
            }
            i#icon{
                padding:10px;
            }

        </style>

        <link rel='stylesheet' id='contact-form-7-css'  href='<?php echo base_url(); ?>plugins/contact-form-7/includes/css/styles.css?ver=4.9' type='text/css' media='all' />
        <link rel='stylesheet' id='iw-shortcodes-css'  href='<?php echo base_url(); ?>plugins/inwave-common/assets/css/iw-shortcodes.css?ver=2.0.0' type='text/css' media='all' />
        <link rel='stylesheet' id='iw-shortcodes-rtl-css'  href='<?php echo base_url(); ?>plugins/inwave-common/assets/css/iw-shortcodes-rtl.css?ver=2.0.0' type='text/css' media='all' />
        <link rel='stylesheet' id='iw_button-css'  href='<?php echo base_url(); ?>plugins/inwave-common/assets/css/iw-button.css?ver=2.0.0' type='text/css' media='all' />
        <link rel='stylesheet' id='iwjob-css'  href='<?php echo base_url(); ?>plugins/iwjob/includes/class/fields/assets/css/style.css?ver=4.8.2' type='text/css' media='all' />
        <link rel='stylesheet' id='iw-filter-job-load-css'  href='<?php echo base_url(); ?>plugins/iwjob/assets/css/load.css?ver=169' type='text/css' media='all' />
        <link rel='stylesheet' id='bxslider-css'  href='<?php echo base_url(); ?>plugins/iwjob/assets/css/jquery.bxslider.css?ver=169' type='text/css' media='all' />
        <link rel='stylesheet' id='iwj-rating-style-css'  href='<?php echo base_url(); ?>plugins/iwjob/assets/css/star-rating.css?ver=169' type='text/css' media='all' />


        <link rel='stylesheet' id='iwj-css'  href='<?php echo base_url(); ?>plugins/iwjob/assets/css/style.css?ver=4.8.2' type='text/css' media='all' />
        <link rel='stylesheet' id='rs-plugin-settings-css'  href='<?php echo base_url(); ?>plugins/revslider/public/assets/css/settings.css?ver=5.4.5.2' type='text/css' media='all' />
        <style id='rs-plugin-settings-inline-css' type='text/css'>
            #rs-demo-id {}
        </style>

        <link rel="stylesheet" href="<?php echo base_url(); ?>css/style_slider.css">
        <link rel='stylesheet' href='<?php echo base_url(); ?>css/bootstrap-datetimepicker.css' type='text/css' media='all' />
        <link rel='stylesheet' href='<?php echo base_url(); ?>css/bootstrap.min.css' type='text/css' media='all' />
        <link href='<?php echo base_url(); ?>css/fullcalendar.min.css' rel='stylesheet' />
        <link href='<?php echo base_url(); ?>css/fullcalendar.print.min.css' rel='stylesheet' media='print' />
        <link href="<?php echo base_url(); ?>css/bootstrapValidator.min.css" rel="stylesheet" />        
        <link href="<?php echo base_url(); ?>css/bootstrap-colorpicker.min.css" rel="stylesheet" />
        <link rel='stylesheet' id='bootstrap-css'  href='<?php echo base_url(); ?>assets/css/bootstrap.min.css?ver=2.0.0' type='text/css' media='all' />

        <link rel='stylesheet' id='font-awesome-css'  href='<?php echo base_url(); ?>plugins/js_composer/assets/lib/bower/font-awesome/css/font-awesome.min.css?ver=5.2.1' type='text/css' media='all' />
        <link rel='stylesheet' id='font-ionicons-css'  href='<?php echo base_url(); ?>assets/fonts/ionicons/ionicons.min.css?ver=2.0.0' type='text/css' media='all' />
        <link rel='stylesheet' id='font-iwj-css'  href='<?php echo base_url(); ?>assets/fonts/iwj/css/fontello.css?ver=2.0.0' type='text/css' media='all' />
        <link rel='stylesheet' id='injob-fonts-css'  href='https://fonts.googleapis.com/css?family=Open+Sans%3A300%2C400%2C600%2C700%2C800%7CPlayfair+Display%3A400%2C400i%2C700%2C700i%2C900%2C900i&#038;subset=latin%2Clatin-ext&#038;ver=2.0.0' type='text/css' media='all' />
        <link rel='stylesheet' id='select2-css'  href='<?php echo base_url(); ?>assets/css/select2.css?ver=2.0.0' type='text/css' media='all' />
        <link rel='stylesheet' id='owl-carousel-css'  href='<?php echo base_url(); ?>assets/css/owl.carousel.css?ver=2.0.0' type='text/css' media='all' />
        <link rel='stylesheet' id='owl-theme-css'  href='<?php echo base_url(); ?>assets/css/owl.theme.css?ver=2.0.0' type='text/css' media='all' />
        <link rel='stylesheet' id='owl-transitions-css'  href='<?php echo base_url(); ?>assets/css/owl.transitions.css?ver=2.0.0' type='text/css' media='all' />
        <link rel='stylesheet' id='animation-css'  href='<?php echo base_url(); ?>assets/css/animation.css?ver=2.0.0' type='text/css' media='all' />

        <link rel='stylesheet' id='injob-style-css'  href='<?php echo base_url(); ?>css/style.css?ver=4.8.2' type='text/css' media='all' />
        <link rel='stylesheet' id='iwjmb-avatar-css'  href='<?php echo base_url(); ?>plugins/iwjob/includes/class/fields/assets/css/avatar.css' type='text/css' media='all' />


        <style id='injob-style-inline-css' type='text/css'>
            html body{font-family:Open Sans}h1,h2,h3,h4,h5,h6{font-family:Open Sans}html body{font-size:13px}html body{line-height:28px}body .wrapper{background-color:#f1f1f1!important}html body{color:#777}.iw-footer-default .iw-footer-middle, .iw-footer-middle .widget_nav_menu .menu li a, .iw-footer-default .widget_inwave-subscribe .malchimp-desc, .iw-footer-middle .widget_inwave-subscribe .iw-email-notifications h5, body .iw-copy-right p{color:#2b495e}body .iw-footer-middle a, body .iw-footer-middle .widget li a, body .iw-footer-middle .widget_nav_menu .menu li a{color:#2b495e}.page-heading .container-inner{padding-top:200px}.page-heading .container-inner{padding-bottom:50px}.page-heading{background-image:url(<?php echo base_url(); ?>assets/images/bg-page-heading.png)!important;background-size: cover;background-repeat:repeat;}.header.header-default.header-sticky.clone{background-image:url(<?php echo base_url(); ?>assets/images/bg-header-sticky.jpg)!important;background-size: cover;background-repeat: no-repeat;position: relative;}
        </style>
        <link rel='stylesheet' id='injob-custom-css'  href='<?php echo base_url(); ?>uploads/injob/color-2980b9.css?ver=2.0.0' type='text/css' media='all' />
        <link rel='stylesheet' id='js_composer_front-css'  href='<?php echo base_url(); ?>plugins/js_composer/assets/css/js_composer.min.css?ver=5.2.1' type='text/css' media='all' />
        <script type='text/javascript' src='<?php echo base_url(); ?>js/jquery/jquery.js?ver=1.12.4'></script>

        <script type='text/javascript' src='<?php echo base_url(); ?>js/jquery/jquery-migrate.min.js?ver=1.4.1'></script>
        <script type='text/javascript' src='<?php echo base_url(); ?>plugins/inwave-common/assets/js/jquery.countdown.js?ver=2.0.0'></script>

        <script type='text/javascript' src='<?php echo base_url(); ?>js/jquery.min.js'></script>
        <script src='<?php echo base_url(); ?>js/main.js'></script>
        <script type='text/javascript' src='<?php echo base_url(); ?>assets/js/bootstrap.min.js?ver=2.0.0'></script>
        <script type='text/javascript'>
            /* <![CDATA[ */
            var userSettings = {"url": "\/wordpress\/", "uid": "0", "time": "1507287331", "secure": ""};
            /* ]]> */
        </script>

        <script type='text/javascript' src='<?php echo base_url(); ?>js/utils.min.js?ver=4.8.2'></script>
        <script type='text/javascript' src='<?php echo base_url(); ?>js/plupload/plupload.full.min.js?ver=2.1.8'></script>
        <!--[if lt IE 8]>
        <script type='text/javascript' src='./wp-includes/js/json2.min.js?ver=2015-05-03'></script>
        <![endif]-->
        <script type='text/javascript' src='<?php echo base_url(); ?>plugins/revslider/public/assets/js/jquery.themepunch.tools.min.js?ver=5.4.5.2'></script>
        <script type='text/javascript' src='<?php echo base_url(); ?>plugins/revslider/public/assets/js/jquery.themepunch.revolution.min.js?ver=5.4.5.2'></script>



        <link rel='stylesheet' href='<?php echo base_url(); ?>css/sweet-alert.css' type='text/css' media='all' />

        <link rel='stylesheet' href='<?php echo base_url(); ?>css/dataTables.bootstrap4.min.css' type='text/css' media='all' />

        <link rel='stylesheet' href='<?php echo base_url(); ?>css/jquery.timepicker.css' type='text/css' media='all' />
        <style type="text/css">.recentcomments a{display:inline !important;padding:0 !important;margin:0 !important;}</style>
        <style type="text/css">.recentcomments a{display:inline !important;padding:0 !important;margin:0 !important;}</style>
        <meta name="generator" content="Powered by Visual Composer - drag and drop page builder for WordPress."/>
        <!--[if lte IE 9]><link rel="stylesheet" type="text/css" href="./wp-content/plugins/js_composer/assets/css/vc_lte_ie9.min.css" media="screen"><![endif]--><meta name="generator" content="Powered by Slider Revolution 5.4.5.2 - responsive, Mobile-Friendly Slider Plugin for WordPress with comfortable drag and drop interface." />
        <script type="text/javascript">function setREVStartSize(e) {
                try {
                    var i = jQuery(window).width(), t = 9999, r = 0, n = 0, l = 0, f = 0, s = 0, h = 0;
                    if (e.responsiveLevels && (jQuery.each(e.responsiveLevels, function (e, f) {
                        f > i && (t = r = f, l = e), i > f && f > r && (r = f, n = e)
                    }), t > r && (l = n)), f = e.gridheight[l] || e.gridheight[0] || e.gridheight, s = e.gridwidth[l] || e.gridwidth[0] || e.gridwidth, h = i / s, h = h > 1 ? 1 : h, f = Math.round(h * f), "fullscreen" == e.sliderLayout) {
                        var u = (e.c.width(), jQuery(window).height());
                        if (void 0 != e.fullScreenOffsetContainer) {
                            var c = e.fullScreenOffsetContainer.split(",");
                            if (c)
                                jQuery.each(c, function (e, i) {
                                    u = jQuery(i).length > 0 ? u - jQuery(i).outerHeight(!0) : u
                                }), e.fullScreenOffset.split("%").length > 1 && void 0 != e.fullScreenOffset && e.fullScreenOffset.length > 0 ? u -= jQuery(window).height() * parseInt(e.fullScreenOffset, 0) / 100 : void 0 != e.fullScreenOffset && e.fullScreenOffset.length > 0 && (u -= parseInt(e.fullScreenOffset, 0))
                        }
                        f = u
                    } else
                        void 0 != e.minHeight && f < e.minHeight && (f = e.minHeight);
                    e.c.closest(".rev_slider_wrapper").css({height: f})
                } catch (d) {
                    console.log("Failure at Presize of Slider:" + d)
                }
            }
            ;</script>
        <style type="text/css" data-type="vc_shortcodes-custom-css">.vc_custom_1502700206350{padding-top: 250px !important;background-image: url(uploads/2017/07/slider-image-1.jpg?id=1456) !important;}.vc_custom_1502072802630{padding-top: 90px !important;padding-bottom: 105px !important;background-color: #ffbc85 !important;}.vc_custom_1501842017830{padding-top: 90px !important;padding-bottom: 90px !important;background-image: url(<?php echo base_url(); ?>uploads/2017/08/bg-info-home-4.png?id=1536) !important;background-position: center !important;background-repeat: no-repeat !important;background-size: cover !important;}.vc_custom_1502076897543{padding-top: 90px !important;padding-bottom: 90px !important;background-color: #ffffff !important;}.vc_custom_1501836210313{padding-bottom: 65px !important;background-image: url(<?php echo base_url(); ?>uploads/2017/03/Image-Background-2.png?id=1106) !important;background-position: center !important;background-repeat: no-repeat !important;background-size: cover !important;}.vc_custom_1501836183839{margin-bottom: 120px !important;}.vc_custom_1501833469137{padding-bottom: 80px !important;background-color: #fafafa !important;}.vc_custom_1501833554275{    background: linear-gradient(rgb(250, 246, 244), rgb(251, 239, 229))}.vc_custom_1502698608067{padding-top: 100px !important;padding-bottom: 80px !important;background-image: url(<?php echo base_url(); ?>uploads/2017/08/bg-home-2.png?id=1541) !important;background-position: center !important;background-repeat: no-repeat !important;background-size: cover !important;}.vc_custom_1502174616606{padding-bottom: 110px !important;    padding-top: 80px !important;
                                                                                                                                                                                                        background: linear-gradient( rgb(251, 239, 229),rgb(254, 229, 209));}.vc_custom_1501829096771{margin-bottom: 70px !important;}.vc_custom_1502700751795{margin-top: 115px !important;margin-bottom: 68px !important;}.vc_custom_1502072813366{margin-bottom: 75px !important;}.vc_custom_1501842006396{margin-bottom: 65px !important;}.vc_custom_1502080122461{margin-top: 90px !important;margin-bottom: 15px !important;}.vc_custom_1502178519794{margin-bottom: 65px !important;}.vc_custom_1501836220622{margin-top: 90px !important;margin-bottom: 105px !important;}.vc_custom_1505959921787{padding-right: 0px !important;padding-left: 0px !important;background-image: url(<?php echo base_url(); ?>uploads/2017/08/bg-info-home-3.png?id=1535) !important;background-position: center !important;background-repeat: no-repeat !important;background-size: cover !important;}.vc_custom_1505960719168{padding-right: 0px !important;padding-left: 0px !important;background-image: url(<?php echo base_url(); ?>uploads/2017/08/bg-info-home-4.png?id=1536) !important;background-position: center !important;background-repeat: no-repeat !important;background-size: cover !important;}.vc_custom_1505959894043{padding-top: 111px !important;padding-bottom: 116px !important;}.vc_custom_1505960895807{padding-top: 111px !important;padding-bottom: 116px !important;}.vc_custom_1501831660590{margin-top: 90px !important;margin-bottom: 70px !important;}.vc_custom_1501832143838{margin-top: 90px !important;margin-bottom: 70px !important;}.vc_custom_1501833634878{margin-top: 90px !important;margin-bottom: 55px !important;}</style><noscript><style type="text/css"> .wpb_animate_when_almost_visible { opacity: 1; }</style></noscript>
            <meta name="google-site-verification" content="_rnVM6zGAht1ob298P--Bx24lGEyBJ0W52HGfBlKaBo" />
            <!-- Facebook Pixel Code -->
            <script>
                !function (f, b, e, v, n, t, s)
                {
                    if (f.fbq)
                        return;
                    n = f.fbq = function () {
                        n.callMethod ?
                                n.callMethod.apply(n, arguments) : n.queue.push(arguments)
                    };
                    if (!f._fbq)
                        f._fbq = n;
                    n.push = n;
                    n.loaded = !0;
                    n.version = '2.0';
                    n.queue = [];
                    t = b.createElement(e);
                    t.async = !0;
                    t.src = v;
                    s = b.getElementsByTagName(e)[0];
                    s.parentNode.insertBefore(t, s)
                }(window, document, 'script',
                        'https://connect.facebook.net/en_US/fbevents.js');
                fbq('init', '161435267886356');
                fbq('track', 'PageView');
            </script>
            <noscript>
        <img height="1" width="1" src="https://www.facebook.com/tr?id=161435267886356&ev=PageView&noscript=1"/></noscript>
        
        <!-- End Facebook Pixel Code -->
    </head>
    <body id="page-top" class="page-template page-template-page-templates page-template-full-width-page page-template-page-templatesfull-width-page-php page page-id-141 logged-in iwj-dashboard-page wpb-js-composer js-comp-ver-5.2.1 vc_responsive">

   <nav class="off-canvas-menu off-canvas-menu-scroll">
                <h2 class="canvas-menu-title">Main Menu <span class="text-right"><a href="#" id="off-canvas-close"><i class="fa fa-times"></i></a></span></h2>
                <ul id="menu-main-menu-1" class="iw-nav-menu  nav-menu nav navbar-nav">
                    <li class="menu-item "><a onclick="location.href = '<?php echo base_url(); ?>';" >Home</a>
                    </li>
                    <li class="menu-item "><a onclick="location.href = '<?php echo base_url(); ?>home/howitworks';" >How it Works</a>

                    </li>
                    <?php if ($this->session->userdata('loggedIn') == true) {
                        if ($data1[0]->role_id == 4) { ?>
                            <li class="menu-item "><a onclick="location.href = '<?php echo base_url(); ?>admin/credits';" >Purchase Code</a></li>
                        <?php }else{ ?>  
                            <li class="menu-item "><a onclick="location.href = '<?php echo base_url(); ?>home/purchasecode';" >Purchase Code</a></li>
                        <?php } ?>
                                
                    <?php }else{ ?>
                    <li class="menu-item "><a onclick="location.href = '<?php echo base_url(); ?>home/purchasecode';" >Purchase Code</a></li>
                    <?php }?>
                    <li class="menu-item "><a onclick="location.href = '<?php echo base_url(); ?>home/schedule';" >Schedule</a>

                    </li>

                    <li class="menu-item "><a onclick="window.open('http://dev.eyogi.in/blog')">Blog</a></li>
                    <li class="menu-item "><a onclick="location.href = '<?php echo base_url(); ?>home/keepintouch';" >Contact Us</a></li>
                    <li class="menu-item " style="margin-left: 16px;color:#FFF ">Email: info@eyogi.in</li>
                        <li class="menu-item " style="margin-left: 16px;color:#FFF ">Phone: 08039534067</li>
                    </ul>

                </nav>


    <div class="wrapper">
        <div class="iw-overlay"></div>
        <div class="header header-default header-style-default page-heading default ">
            <div class="iw-top-bar-wrapper">
                <div class="row">
                    <div class="col-md-6 col-sm-9 col-xs-9">
                        <div class="top-bar-left">
                            <div class="contact" style="text-align: left"><span>Phone: 08039534067</span><span>Email: info@eyogi.in</span></div>
                        </div>

                    </div>
                    <div class="col-md-6 col-sm-3 col-xs-3">
                        <div class="top-bar-right">
                            <div class="social-header">
                                <div><ul class="iw-social-all"><li><a target="_blank" class="facebook" onclick="window.open('https://www.facebook.com/eyogi.in/')" title="Facebook"><i class="fa fa fa-facebook"></i></a></li><li><a class="twitter" target="_blank"  onclick="window.open('https://twitter.com/eyogi_in')" title="Twitter"><i class="fa fa fa-twitter"></i></a></li><li><a class="google-plus" target="_blank"  onclick="window.open('https://plus.google.com/105548168969313762025')" title="Google Plush"><i class="fa fa fa-google"></i></a></li></ul></div></div>
                            <input type="hidden" id="role_hidden" value="<?php echo $data1[0]->role_id ?>">
                            <div class="author-login">
                                <a href="">
                                    <span class="author-avatar"><img alt='Peter Pham' src='<?php
if ($data1[0]->photo != "") {
    echo $data1[0]->photo;
} else {
    echo base_url() . "uploads/images/user.jpg";
}
?>' srcset='http://jobboard.inwavethemes.com/wp-content/uploads/2017/06/31m-2.jpg 2x' class='avatar avatar-90 photo' height='90' width='90' /></span>
                                    <span class="author-name"><?php echo $data1[0]->username; ?></span>
                                </a>
                                <div class="iwj-dashboard-menu">
<?php $this->load->view('layout/sidebar.php') ?>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="navbar navbar-default iw-header ">
                <div class="navbar-default-inner ">
                    <h1 class="iw-logo float-left">
                        <a onclick="location.href = '<?php echo base_url(); ?>';" title="eYogi">
                            <img class="main-logo" src="<?php echo base_url(); ?>assets/images/logo.png" alt="eYogi">
                            <img class="sticky-logo" src="<?php echo base_url(); ?>assets/images/logo-sticky.png" alt="eYogi">
                            <img class="logo-mobile" src="<?php echo base_url(); ?>assets/images/logo-mobile.png" alt="eYogi">
                        </a>
                    </h1>
                    <div class="header-btn-action">
                        <span class="off-canvas-btn">
                            <i class="fa fa-bars" id="icon"></i>
                        </span>
                        <div class="iwj-action-button float-right">
                            <div class="iw-post-a-job">

                            </div>
                        </div>
                        <div class="iwj-author-mobile float-right">                        
                            <div class="author-login">
                                <a href="">
                                    <span class="author-name"><?php echo $data1[0]->username; ?> </span>
                                    <span class="action-button author-avatar"><img alt='Peter Pham' src='<?php
                                        if ($data1[0]->photo != "") {
                                            echo $data1[0]->photo;
                                        } else {
                                            echo base_url() . "uploads/images/user.jpg";
                                        }
?>' srcset='' class='avatar avatar-40 photo' height='40' width='40' /></span>
                                </a>
                                                <div class="iwj-dashboard-menu-mobile">
                                                        <div class="dropdown">
                                                            <button class="btn dropdown-toggle hidden-md" type="button" data-toggle="dropdown" style="background-color: transparent!important;"><span class="caret"></span></button>
                                                            <?php $this->load->view('layout/menu.php') ?>
                                                        </div>
                                                </div>
                            </div>
                        </div>                           
                    </div>
                    <div class="iw-menu-header-default float-right">
                        <nav class="main-menu iw-menu-main nav-collapse">
                            <!--Menu desktop-->
                            <div class="iw-main-menu ">

                                <ul id="menu-main-menu-1" class="iw-nav-menu  nav-menu nav navbar-nav">
                                    <li class="menu-item menu-item-type-post_type menu-item-object-page menu-item-home menu-item-has-children menu-item-3301   "><a class="home" onclick="location.href = '<?php echo base_url(); ?>';" >Home</a>
                                    </li>
                                    <li class="menu-item menu-item-type-post_type menu-item-object-page menu-item-has-children menu-item-3290  "><a class="howitworks" onclick="location.href = '<?php echo base_url(); ?>home/howitworks';" >How it Works</a>

                                    </li>
                                    <?php if ($this->session->userdata('loggedIn') == true) { 
                                        if ($data1[0]->role_id == 4) {?>
                                    <li class="menu-item menu-item-type-post_type menu-item-object-page menu-item-has-children menu-item-3288"><a class="purchasecode" onclick="location.href = '<?php echo base_url(); ?>admin/credits';" >Purchase Code</a>
                                        <?php }else{ ?>
                                            <li class="menu-item menu-item-type-post_type menu-item-object-page menu-item-has-children menu-item-3288"><a class="purchasecode" onclick="location.href = '<?php echo base_url(); ?>home/purchasecode';" >Purchase Code</a>

                                            </li>
                                        <?php } ?>
                                    </li>
                                    <?php }else{ ?>
                                        <li class="menu-item menu-item-type-post_type menu-item-object-page menu-item-has-children menu-item-3288"><a class="purchasecode" onclick="location.href = '<?php echo base_url(); ?>home/purchasecode';" >Purchase Code</a>

                                        </li>
                                    <?php } ?>
                                    <li class="menu-item menu-item-type-post_type menu-item-object-page menu-item-has-children menu-item-3289"><a class="schedule"  onclick="location.href = '<?php echo base_url(); ?>home/schedule';" >Schedule</a>

                                    </li>
                                    <li class="menu-item menu-item-type-post_type menu-item-object-page menu-item-3307"><a onclick="window.open('http://dev.eyogi.in/blog')">Blog</a></li>
                                    <li class="menu-item menu-item-type-post_type menu-item-object-page menu-item-3307  "><a class="keepintouch" onclick="location.href = '<?php echo base_url(); ?>home/keepintouch';" >Contact Us</a></li>
                                </ul>
                            </div>                
                        </nav>

                    </div>
                </div>
            </div>
        </div>

        <!--End Header-->        