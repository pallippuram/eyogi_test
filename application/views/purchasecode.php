<?php $this->load->view('layout/header.php') ?>
<div class="page-heading default">
    <div class="container-inner">
        <div class="container">
            <div class="page-title">
                <div class="iw-heading-title">
                    <h1 style="color:#894724">
                        Purchase Code                     </h1>
                    <div class="iwh-description" style="color: #303C42"><h4>Codes purchased here can be used by anyone for a private session</h4>
                        <?php
                        if ($this->session->userdata('loggedIn') == true) {
                            if ($data1[0]->role_id == 4) {
                                ?>
                                <h4>Buy <a onclick="location.href = '<?php echo base_url(); ?>admin/credits';">CREDITS</a> for me instead</h4>
    <?php }
} ?>
                    </div>


                </div>
            </div>
        </div>
    </div>

</div>



<div class="contents-main" style="background-color: #f2f2f1;padding-top: 50px;" id="contents-main">
    <div class="container">
        <div class="row">
            <div class="col-sm-12 col-xs-12">

                <article id="post-143" class="post-143 page type-page status-publish hentry">
                    <div class="entry-content">
                        <div class="iwj-magic-line-wrap" style="background: #f2f2f1">
                            <div class="iwj-magic-line">
                                <a id="indivudual_a"  style="text-decoration: none;" onclick="toggle_visibility('individual')"><p  class="active iwj-toggle iwj-candidate-toggle">Individual</p></a>
                                <a id="organization_a" style="text-decoration: none;" class="style1" onclick="toggle_visibility('organization')" ><p class="iwj-toggle iwj-employer-toggle">Organization</p></a>
                            </div>
                        </div>

                        <div class="contents-main iw-job-content iw-job-detail" style="background: #f2f2f1;padding-top: 50px;" id="contents-main">
                            <div class="container purchase" style="">
                                <div class="row">
                                    <div class="col-sm-12 col-xs-12 col-md-12">
                                        <div class="job-detail">
                                            <div class="job-detail-content">
                                                <div class="job-detail-info">

                                                    <ul id="123">
<?php foreach ($codes as $code) { ?>
                                                            <li style="width:38%; ">
                                                                <input type="radio" class="amnt_radio" id="f-option-<?= $code->id ?>" name="amnt_radio">
                                                                <label for="f-option-<?= $code->id ?>"><?= $code->coupon_name ?></label>
                                                                <input id="amnt-<?= $code->id ?>" type="hidden" value="<?= $code->amount ?>" style="width: 80px">
                                                                <div class="check"></div>
                                                            </li>
                                                            <li class="address">
                                                                <div class="content-inner">

                                                                    <span>Amount</span>
                                                                    <br>
                                                                    <div class="content">
                                                                        <h2 style="color: rgb(48, 194, 189)">Rs.<?= $code->amount ?></h2>
                                                                    </div>
                                                                </div>
                                                            </li>
                                                            <li class="salary" style="display:none;width:30%;">
                                                                <div class="content-inner">


                                                                    <span >Qty</span>
                                                                    <br>
                                                                    <div class="content" style="margin-top:20px">
                                                                        <div class="iwjmb-field iwjmb-select_advanced-wrapper  required">


                                                                            <div class="iwjmb-input">
                                                                                <select data-options="{&quot;allowClear&quot;:false,&quot;width&quot;:&quot;none&quot;,&quot;placeholder&quot;:&quot;&quot;,&quot;minimumResultsForSearch&quot;:-1}" required id="qty-<?= $code->id ?>" class="iwjmb-select_advanced qty_select" name="qty">

                                                                                    <option value="1">1</option>

                                                                                </select>
                                                                            </div>
                                                                        </div>   
                                                                    </div>
                                                                </div>
                                                            </li>
                                                            <!--                                                            <li class="salary">
                                                                                                                            <div class="content-inner">
                                                            
                                                            
                                                                                                                                <span >Total</span>
                                                                                                                                <br>
                                                                                                                                <div class="content text-center amnt_div" id="amnt_div-<?= $code->id ?>" style="margin-top: 20px;background-color: #f3f2f1;">
                                                                                                                                    0.00                                     
                                                                                                                                </div>
                                                                                                                            </div>
                                                                                                                        </li>-->
<?php } ?>

                                                        <li class="job-type" style="width:38%">
                                                            <input class="amnt_radio" type="radio" id="f-option-opt" name="amnt_radio">
                                                            <label for="f-option-opt">Buy</label>

                                                            <div class="check"></div>
                                                        </li>
                                                        <li class="address">
                                                            <div class="content-inner">

                                                                <span>Amount</span>
                                                                <br>
                                                                <div class="content" style="margin-top:20px">
                                                                    <div class="iwj-field">

                                                                        <div class="iwj-input ">

                                                                            <input type="text" class="iwjmb-text amt_text chatinput" onkeyup="if (/\D/g.test(this.value))
                                                                                        this.value = this.value.replace(/\D/g, '')" id="amnt_input" style="width: 100px;" onchange="changeTotal(this.value, this.id)" placeholder="Enter Amount">
                                                                        </div>

                                                                    </div>
                                                                </div>
                                                            </div>
                                                        </li>
                                                        <li class="salary"  style="display:none;width:30%;height: 172px;">
                                                            <div class="content-inner">


                                                                <span >Qty</span>
                                                                <br>
                                                                <div class="content" style="margin-top:20px">
                                                                    <div class="iwjmb-field iwjmb-select_advanced-wrapper  required">


                                                                        <div class="iwjmb-input">
                                                                            <select data-options="{&quot;allowClear&quot;:false,&quot;width&quot;:&quot;none&quot;,&quot;placeholder&quot;:&quot;&quot;,&quot;minimumResultsForSearch&quot;:-1}" required id="qty_select" class="iwjmb-select_advanced qty_select" name="qty">
                                                                                <option value="1">1</option>

                                                                            </select>
                                                                        </div>
                                                                    </div>   
                                                                </div>
                                                            </div>
                                                        </li>

                                                    </ul>
                                                </div>
                                                <div class="job-detail-about">
                                                    <div class="job-detail-desc item" style="background:rgb(253, 246, 242)">
                                                        <label style=" margin-left: 20px; text-transform: none !important"><h2 style="color:#F59433;padding-left:15px;"><b>Total</b></h2></label>
                                                        <label style="float:right;margin-right: 20px"><h2 class="printchatbox" id="total_txt" >0.00</h2></label>
                                                    </div>

                                                </div>


                                            </div>

                                        </div>
                                    </div>

                                </div>
                            </div>
                        </div>


                        <div class="iwj-login" style="padding-top:20px;padding-bottom: 50px;height:auto;">
                            <form id="individual_form" action="<?php echo base_url(); ?>payment/index" method="post" class="iwj-form iwj-login-form1">
                                <input type="hidden" value="" id="ind_amt_hdn" name="ind_amt_hdn">
                                <input type="hidden" value="" id="total_inpt" name="total_input">
                                <input type="hidden" value="0" name="ind_type">
                                <input type="hidden" value="" id="qty_inpt" name="qty_input">
                                <input type="hidden" value="0" name="change_tot">
                                <div class="reg_2" >


                                    <label class="iw-title1" style="margin-bottom:20px;color: #ff9129;">Where do you want us to send the code?</label>
                                    <div class="iwjmb-field iwjmb-select_advanced-wrapper  required">

                                        <div class="iwjmb-input">
                                             
                                            
                                            <input type="radio" class="radio_email" id="test1" value="email" name="radio_email" checked>
                                            <label style="display:inline; color: rgb(148, 150, 153)" for="test1">Email</label>

                                            <input type="radio" class="radio_phone" id="test2" value="phone" name="radio_email">
                                            <label  style="display:inline; color: rgb(148, 150, 153)" for="test2">Phone</label>

                                        </div>
                                    </div>  

                                    <div id="purchaseEmail" >
                                        <div class="iwj-field">

                                            <div class="iwj-input">

                                                <input type="email" name="purachase_email" id="purachase_email" placeholder="Enter Email">
                                            </div>
                                        </div>
                                        <div class="iwj-field">

                                            <div class="iwj-input">

                                                <input type="email" name="purachase_email_confirm" id="purachase_email_confirm" placeholder="Re-enter your Email for verification">
                                            </div>
                                        </div>

                                    </div>
                                    <div id="purchaseMobile" style="display:none;">
                                        <div class="iwj-field">

                                            <div class="iwj-input">

                                                <input type="text" id="purachase_phone" name="purachase_phone" placeholder="Enter Phone">
                                            </div>
                                        </div>
                                        <div class="iwj-field">

                                            <div class="iwj-input">

                                                <input type="text" name="purachase_phone_confirm" id="purachase_phone_confirm" placeholder="Re-enter your phone for verification">
                                            </div>
                                        </div>

                                    </div>

                                </div>
                                <div class="iwj-button-loader">
                                    <button type="button" id="proceed_to_pay_individual"  class="iwj-btn iwj-btn-primary iwj-btn-full iwj-btn-large proceed_to_pay">Proceed to pay</button>
                                </div>

                            </form>

                            <form id="organization_form" action="<?php echo base_url(); ?>payment/index" method="post" style="display:none" class="iwj-form iwj-login-form1">
                                <input type="hidden" value="" id="org_amt_hdn" name="org_amt_hdn">
                                <input type="hidden" value="" id="ind_amt_hdn_org" name="ind_amt_hdn_org">
                                <input type="hidden" value="" id="total_inpt_org" name="total_input">
                                <input type="hidden" value="1" name="ind_type">
                                <input type="hidden" value="1" id="qty_inpt_org" name="qty_input">
                                <input type="hidden" value="1" id="qty_inpt_org_hdn" name="qty_inpt_org_hdn">

                                <div class="reg_2" >

                                    <div class="iwjmb-label">
                                        <label style="color: #ff8e16;" for="frequency">Organization</label>
                                    </div>
                                    <div class="iwj-field">

                                        <div class="iwj-input">

                                            <input type="text" name="organization_input" id="organization_input" placeholder="Enter your organization name">
                                        </div>
                                    </div>
                                    <div class="iwjmb-label">
                                        <label style="color: #ff8e16;" for="frequency">Email</label>
                                    </div>
                                    <div class="iwj-field">

                                        <div class="iwj-input">

                                            <input type="text" id="organization_email" name="organization_email" placeholder="Enter email">
                                        </div>
                                    </div>
                                    <div class="iwj-field">

                                        <div class="iwj-input">

                                            <input type="text" id="organization_conemail" name="organization_conemail" placeholder="Re-enter your Email for verification">
                                        </div>
                                    </div>



                                    <br>
                                    <div class="iwj-button-loader">
                                        <button type="button" id="proceed_to_pay_organization"  name="login" class="iwj-btn iwj-btn-primary iwj-btn-full iwj-btn-large iwj-login-btn proceed_to_pay">Proceed to pay</button>
                                    </div>
                                </div>


                            </form>
                        </div>

                    </div><!-- .entry-content -->
                    <div class="clearfix"></div>
                    <footer class="entry-footer">
                    </footer><!-- .entry-footer -->
                </article><!-- #post-## -->
            </div>
        </div>
    </div>
</div>
<script>
//    $(".proceed_to_pay").click(function () {
//         swal({title: "Oops", text: "Sorry!!! We are still in beta. Please come back later to purchase a code!!", html: true, type: "error", closeOnConfirm: false, closeOnCancel: false, allowOutsideClick: false},
//                                function () {
//                                    location.reload();
//                                }
//                        );
//    });
    $(document).ready(function () {
        $('input[name=amnt_radio]').prop('checked', false);
    });
    $("input[name$='amnt_radio']").click(function () {

        if ($(this).is(':checked'))
        {

            $('.amnt_div').text("0.00")
            $('#total_txt').text("0.00");
            //$("#amnt_input").text("Enter Amount");
            var txt = $(this).parent().find('input[type="hidden"]');
            var amount = $('#qty_inpt_org_hdn').val();

            var amt = txt.val() * amount;

            if (typeof amt != "undefined") {
                var txt_id = txt.attr('id');
                var arr = txt_id.split('-');
                txt.parent().parent().find('div #amnt_div-' + arr[1]).text(amt);
                $('#total_txt').text(amt)
                $('#ind_amt_hdn').val(amt)
                $('#total_inpt').val(amt)
                $('#org_amt_hdn').val(amt);
                $('#qty_inpt').val($('#qty-' + arr[1]).val())
            }

        }
    });

    function changeTotal(value, id) {

        var amt = value;
        $('#amnt_input').closest('li').parent().find('div #amt_cusotm').text(amt);
        $('#total_txt').text(amt)
        $('#ind_amt_hdn').val(amt)
        $('#total_inpt').text(amt)
        $('#org_amt_hdn').val(amt);
        $('#qty_inpt').val($('#qty_select').val())
        $('#change_tot').val(1);
    }
    $('#proceed_to_pay_individual').on('click', function () {
        var ret = true;
        if ($('.amnt_radio').is(':checked')) {
            if ($('.radio_email').is(':checked')) {
                var email = $('#purachase_email').val();
                var conemail = $('#purachase_email_confirm').val();
                var amnt = +document.getElementById('total_txt').innerText;

                if (!isEmail(email)) {
                    $('#purachase_email').parent().parent().addClass("error");
                    $('#purachase_email').attr("placeholder", "Please enter a valid email address");
                    ret = false;
                } else {
                    $('#purachase_email').parent().parent().removeClass("error");

                }

                if (!isEmail(conemail)) {
                    $('#purachase_email_confirm').parent().parent().addClass("error");
                    $('#purachase_email_confirm').attr("placeholder", "Please enter a valid email address");
                    ret = false;
                } else {
                    $('#purachase_email_confirm').parent().parent().removeClass("error");

                }

                if (email != conemail) {
                    $('#purachase_email_confirm').parent().parent().addClass("error");
                    $('#purachase_email_confirm').attr("placeholder", "Email mismatch");
                    ret = false;
                }
                
                if (amnt == 0) {
                    swal("Oops!", "Please enter an amount and proceed to pay!!!", "error")
                    ret = false;
                }

                if (ret != false) {
                    var vistior_info = email
                }


            } else if ($('.radio_phone').is(':checked')) {
                var phone = $('#purachase_phone').val();
                var conphone = $('#purachase_phone_confirm').val();
                var amnt = +document.getElementById('total_txt').innerText;
                var filter = /^\d*(?:\.\d{1,2})?$/;
                if (filter.test(phone)) {
                    if (phone.length == 10) {
                        $('#purachase_phone').parent().parent().removeClass("error");
                    } 
                    else {
                        $('#purachase_phone').addClass("error");
                        $('#purachase_phone').attr("placeholder", "Please Enter Valid Phone Number");
                        ret = false;
                    }
                } else {
                    $('#purachase_phone').addClass("error");
                    $('#purachase_phone').attr("placeholder", "Please Enter Valid Phone Number"); 
                    ret = false;
                }
                
                if (phone == "") {
                    $('#purachase_phone').parent().parent().addClass("error");
                    $('#purachase_phone').attr("placeholder", "Please enter your phone");
                    ret = false;
                } else {
                    $('#purachase_phone').parent().parent().removeClass("error");

                }

                if (conphone == "") {
                    $('#purachase_phone_confirm').parent().parent().addClass("error");
                    $('#purachase_phone_confirm').attr("placeholder", "Please confirm your phone");
                    ret = false;
                } else {
                    $('#purachase_phone_confirm').parent().parent().removeClass("error");

                }

                if (phone != conphone) {
                    $('#purachase_phone_confirm').parent().parent().addClass("error");
                    $('#purachase_phone_confirm').attr("placeholder", "Phone number mismatch");
                    ret = false;
                }
                //var amnt = +document.getElementById('total_txt').innerText;
                if (amnt == 0) {
                    swal("Oops!", "Please enter an amount and proceed to pay!!!", "error")
                    ret = false;
                }

                if (ret != false) {
                    var vistior_info = phone
                }
            }
            var amnt = $('#total_txt').text();
            if (amnt == "0.00") {
                swal("Oops!", "Please enter an amount and proceed to pay!!!", "error")
                ret = false;
            }
            if (ret != false) {
                $('#individual_form').submit();
            }


        } else {
            swal("Oops!", "Please choose coupon amount", "error")
        }
    });
    $('.qty_select').on('change', function () {
        $('#qty_inpt_org_hdn').val($(this).val());
        $('#qty_inpt_org').val($(this).val());
        var amnt = $('#org_amt_hdn').val();
        var change_tot = $('#change_tot').val();
        var total = $(this).val() * amnt;
        var sel = $(this).attr('id')
        if (change_tot == 1) {
            $(this).closest('li').next('li').find('div #amnt_div').text(total)
        } else {
            $(this).closest('li').next('li').find('div .amnt_div').text(total)
        }

        $('#total_txt').text(total);
        $('#ind_amt_hdn_org').val(total);
        $('#total_inpt_org').val(total);
    });
    $('#proceed_to_pay_organization').on('click', function () {
        var ret = true;
        if ($('.amnt_radio').is(':checked')) {
            var name = $('#organization_input').val();
            var email = $('#organization_email').val();
            var conemail = $('#organization_conemail').val();
            var amnt = +document.getElementById('total_txt').innerText;

            if (name == "") {
                $('#organization_input').parent().parent().addClass("error");
                $('#organization_input').attr("placeholder", "Please enter your organization name");
                ret = false;
            } else {
                $('#organization_input').parent().parent().removeClass("error");

            }


            if (!isEmail(email)) {
                $('#organization_email').parent().parent().addClass("error");
                $('#organization_email').attr("placeholder", "Please enter a valid email address");
                ret = false;
            } else {
                $('#organization_email').parent().parent().removeClass("error");

            }

            if (!isEmail(conemail)) {
                $('#organization_conemail').parent().parent().addClass("error");
                $('#organization_conemail').attr("placeholder", "Please enter a valid email address");
                ret = false;
            } else {
                $('#organization_conemail').parent().parent().removeClass("error");

            }

            if (email != conemail) {
                $('#organization_conemail').parent().parent().addClass("error");
                $('#organization_conemail').attr("placeholder", "Email mismatch");
                ret = false;
            }

            if (ret != false) {
                var vistior_info = email
            }
            
            //var amnt = +document.getElementById('total_txt').innerText;
            if (amnt == 0) {
                swal("Oops!", "Please enter an amount and proceed to pay!!!", "error")
                ret = false;
            }
            if (ret != false) {
                $('#organization_form').submit();
            }
        } else {
            swal("Oops!", "Please choose coupon amount", "error")
        }
    });

    $('.chatinput').keyup(function(event) {
        newText = event.target.value;
        $('.printchatbox').text(newText);
    });  
</script>
<?php $this->load->view('layout/footer.php') ?>