<?php $this->load->view('layout/header.php') ?>
<div class="page-heading default">
    <div class="container-inner">
        <div class="container">
            <div class="page-title">
                <div class="iw-heading-title">
                    <h1>
                        Join Us                            </h1>

                </div>
            </div>
        </div>
    </div>
   
</div>
<div class="contents-main" style="background-color: #f2f2f1" id="contents-main">
    <div class="container">
        <div class="row">
            <div class="col-sm-12 col-xs-12">

                <article id="post-143" class="post-143 page type-page status-publish hentry">
                    <div class="entry-content">

                        <div class="sbn-list" >
                            <div class="sbn sbn01 sbn-new" style="margin:0 auto;">

                                <ul class="tabs">
                                    <li id="1"><a class="is-active" href="#">1</a></li>
                                    <li id="2"><a  href="#">2</a></li>
                                    <li id="3"><a href="#">3</a></li>
                                    <li id="4"><a href="#">3</a></li>
                                </ul>
                            </div>
                        </div>



                        <div class="iwj-login" style="padding-top:20px;padding-bottom: 50px;height:auto;height:926px">
                            <form action="<?php echo site_url('admin/save_counselor'); ?>" method="post" enctype="multipart/form-data" style="height:850px" class="iwj-form iwj-login-form1">
                                 <?php if($msg!=""){ ?>
                                                   <div id="error_msg_login" class="alert alert-danger"><?php echo $msg; ?> </div>
                                              <?php  } ?>
                                
                                <div class="join_1" >
                                    <div class="iwj-respon-msg hide"></div>
                                    <div class="iwj-field">
                                        <label>User Name</label>
                                        <div class="iwj-input">
                                            <i class="fa fa-user"></i>
                                            <input type="text" id="join_username" value="<?php
                                            if (isset($first_name)) {
                                                echo $first_name;
                                            } else {
                                                echo "";
                                            }
                                            ?>" name="username" placeholder="Enter Your Username.">
                                        </div>
                                    </div>
                                    <div class="iwj-field">
                                        <label>Email</label>
                                        <div class="iwj-input">
                                            <i class="fa fa-envelope"></i>
                                            <input id="join_email" type="email" value="<?php
                                            if (isset($email)) {
                                                echo $email;
                                            } else {
                                                echo "";
                                            }
                                            ?>" name="email" placeholder="Enter Your Email.">
                                        </div>
                                    </div>
                                    <div class="iwj-field">
                                        <label>Password</label>
                                        <div class="iwj-input">
                                            <i class="fa fa-keyboard-o"></i>
                                            <input type="password" id="join_password" name="password" placeholder="Enter Password.">
                                        </div>
                                    </div>
                                    <div class="iwj-field">
                                        <label>Confirm Password</label>
                                        <div class="iwj-input">
                                            <i class="fa fa-keyboard-o"></i>
                                            <input type="password" id="join_conpassword" name="conpassword" placeholder="Confirm Password.">
                                        </div>
                                    </div>
                                    <div class="iwj-button-loader">
                                        <button type="button" id="join_btn1" name="login" class="iwj-btn iwj-btn-primary iwj-btn-full iwj-btn-large iwj-login-btn">NEXT</button>
                                    </div>

                                    <div class="iwj-divider">
                                        <span class="line"></span>
                                        <span class="circle">Or</span>
                                    </div>
                                    <div class="text-center register-account">
                                        Register Using
                                    </div>
                                    <div class="social-login row">
                                        <div class="col-md-4">
                                            <a href="<?php echo $authUrl ?>" class="iwj-btn iwj-btn-primary iwj-btn-medium iwj-btn-full iwj-btn-icon social-login-facebook"><i class="fa fa-facebook"></i>Facebook</a>
                                        </div>
                                        <div class="col-md-4">
                                            <a href="<?php echo $loginURL; ?>" class="iwj-btn iwj-btn-primary iwj-btn-medium iwj-btn-full iwj-btn-icon social-login-google"><i class="fa fa-google-plus"></i>Google</a>
                                        </div>
                                        <div class="col-md-4">
                                            <a href="<?php echo $oauthURL; ?>" class="iwj-btn iwj-btn-primary iwj-btn-medium iwj-btn-full iwj-btn-icon social-login-linkedin"><i class="fa fa-linkedin"></i>Linkedin</a>
                                        </div>
                                    </div>   

                                   
                                   
                                </div>
                                <div class="join_m" style="display:none;">
                                                <div style="display:inline-flex; width:100%">
                                                    <div class="iwj-field col-md-8">

                                                        <label>Mobile Number</label>
                                                        <div class="iwj-input">
                                                            <i class="fa fa-user"></i>
                                                            <input type="text" id="reg_mobile_number" name="phone" placeholder="Enter Mobile Number">
                                                        </div>
                                                    </div>
                                                    <div class="col-md-4">
                                                        <button type="button" onclick="generate_code()" name="login" class="iwj-btn iwj-btn-primary iwj-btn-full iwj-btn-large iwj-login-btn">Send OTP</button>
                                                    </div>
                                                </div>
                                                <span style="display: none" class="alert alert-info generate_otp_span">OTP has been send to your mobile number.. Click <a onclick="generate_code()">here</a> to resend OTP</span>
                                                
                                                <div class="iwj-field" style="margin-top: 30px">
                                                    <label>One time password</label>
                                                    <div class="iwj-input">
                                                        <i class="fa fa-keyboard-o"></i>
                                                        <input type="text" id="reg_mobile_otp" name="otp" placeholder="Enter OTP received">
                                                    </div>
                                                </div>
                                                <div class="iwj-button-loader">
                                                    <button  type="button" id="join_btn_2"  name="login" class="iwj-btn iwj-btn-primary iwj-btn-full iwj-btn-large iwj-login-btn">NEXT</button>
                                                </div>
                                            </div>


                                <div class="join_2" style="display:none;">

                                    <div class="iwjmb-field iwjmb-avatar-wrapper">
                                        <div class="iwjmb-input ui-sortable">           
                                            <div id="_iwj_avatar" class="iwj-avatar-container">
                                                <!-- Current avatar -->
                                                <div class="avatar-view">
                                                    <img src="<?php
                                                    if (isset($picture_url)) {
                                                        echo $picture_url;
                                                    } else {
                                                        echo "uploads/2017/06/31m-2-96x96.jpg";
                                                    }
                                                    ?>" alt="Avatar">
                                                </div>
                                                <div class="desc-change-image">
                                                    <p class="avatar-description"></p>
                                                    <div class="change-image-btn" style="padding-top:50px">

                                                        <div class="choose_file">
                                                            <span>Choose File</span>
                                                            <input name="image" type="file" />
                                                        </div>
                                                    </div>
                                                </div>
                                                <!-- Cropping modal -->

                                            </div>
                                        </div></div>


                                    <div class="iwjmb-field iwjmb-select_advanced-wrapper  ">
                                        <div class="iwjmb-label">
                                            <label style="color: #ff8e16;" for="frequency">Domain*</label>
                                        </div>		

                                        <div class="iwjmb-input">
                                            <div class="iwjmb-input">
                                                       <select multiple data-options="{&quot;allowClear&quot;:false,&quot;width&quot;:&quot;none&quot;,&quot;placeholder&quot;:&quot;&quot;,&quot;minimumResultsForSearch&quot;:-1}" required  class="iwjmb-select_advanced" name="domain[]">
                                                                
                                                               <?php  foreach ($counselling_types as $type) { ?>
                                                                <option value="<?=$type->type?>"><?=$type->type?></option>
                                                               <?php } ?>
                                                            </select>
                                                    </div>
                                        </div>
                                    </div>   

                                    <div class="iwjmb-field iwjmb-select_advanced-wrapper  ">
                                        <div class="iwjmb-label">
                                            <label style="color: #ff8e16;" for="frequency">Experience</label>
                                        </div>
                                        <div class="iwjmb-input">
                                            <select data-options="{&quot;allowClear&quot;:false,&quot;width&quot;:&quot;none&quot;,&quot;placeholder&quot;:&quot;&quot;,&quot;minimumResultsForSearch&quot;:-1}"  id="experience" class="iwjmb-select_advanced" name="experience">
                                                <option selected=''>Please Select</option>
                                                                <option value="1">1</option>
                                                                <option value="2">2</option>
                                                                <option value="3">3</option>
                                                                <option value="4">4</option>
                                                                <option value="5">5</option>
                                                                <option value="6">6</option>
                                                                <option value="7">7</option>
                                                                <option value="8">8</option>
                                                                <option value="9">9</option>
                                                                <option value="10">10</option>
                                                                <option value="11">11</option>
                                                                <option value="12">12</option>
                                                                <option value="13">13</option>
                                                                <option value="14">14</option>
                                                                <option value="15">15</option>
                                            </select>
                                        </div>
                                    </div>   

                                    <div class="iwjmb-field iwjmb-select_advanced-wrapper  ">
                                        <div class="iwjmb-label">
                                            <label style="color: #ff8e16;" for="frequency">Languages Known</label>
                                        </div>
                                        <div class="iwjmb-input">
                                            <select id="languages" multiple data-options="{&quot;allowClear&quot;:false,&quot;width&quot;:&quot;none&quot;,&quot;placeholder&quot;:&quot;&quot;,&quot;minimumResultsForSearch&quot;:-1}"  class="iwjmb-select_advanced" name="languages[]">
                                                <option value="english"  selected='selected'>English</option>
                                                <option value="hindi">Hindi</option>
                                                <option value="malayalam" >Malayalam</option>
                                                <option value="tamil" >Tamil</option>
                                            </select>
                                        </div>
                                    </div>   
                                    <br>
                                    <div class="iwj-button-loader">
                                        <button type="button" id="join_btn2"  name="login" class="iwj-btn iwj-btn-primary iwj-btn-full iwj-btn-large iwj-login-btn">NEXT</button>
                                    </div>
                                </div>

                                <div class="join_3" style="display:none;">

                                    <div class="iwjmb-field">

                                        <label style="color: #ff8e16;" for="frequency">Certifications</label>

                                        <div class="iwjmb-input">
                                            <textarea style="width:100%"  cols="80" rows="3" placeholder="Certifications" id="certifications" class="iwjmb-textarea  large-text" name="certifications"></textarea>
                                        </div>
                                    </div>   

                                    <div class="iwjmb-field">
                                        <div class="iwjmb-label">
                                            <label style="color: #ff8e16;"  for="frequency">Accreditations</label>
                                        </div>
                                        <div class="iwjmb-input">
                                            <textarea style="width:100%" cols="80" rows="3" placeholder="Accreditations" id="accrediations" class="iwjmb-textarea  large-text" name="accrediations"></textarea>
                                        </div>
                                    </div>  
                                    <div class="text-center"><a onclick="skip_step()">Skip this step</a></div>
                                    <br>
                                    <input type="hidden" name="page_id" value="3">
                                    <input type="hidden" name="page" value="joinus">
                                    <input type="hidden" name="role" value="3">
                                    <div class="iwj-button-loader">
                                        <input type="submit" id="join_btn3"  name="login" class="iwj-btn iwj-btn-primary iwj-btn-full iwj-btn-large iwj-login-btn" value="Submit">
                                    </div>
                                </div>
                            </form>
                        </div>

                    </div><!-- .entry-content -->
                    <div class="clearfix"></div>
                    <footer class="entry-footer">
                    </footer><!-- .entry-footer -->
                </article><!-- #post-## -->
            </div>
        </div>
    </div>
</div>
<script>
    function skip_step(){
        $('#join_btn3').click();
    }
    
</script>
<?php $this->load->view('layout/footer.php') ?>