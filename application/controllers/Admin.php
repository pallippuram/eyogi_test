<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class Admin extends CI_Controller {

    public function __construct() {
        parent::__construct();
        $this->load->library('google');
        $this->load->model('user_model');
        $this->load->config('linkedin');
        $this->load->library('facebook');
        $this->load->library('email');
        $this->load->helper('string');
        $this->load->helper('session_helper');
    }

    public function index() {

        $userid = $this->session->userdata('userData');
        if ($this->session->userdata('loggedIn') == true) {

            if ($userid->role_id == 1 || $userid->role_id == 2) {
                //redirect to login page if user not logged in
                $this->session->unset_userdata('login');
                $this->session->unset_userdata('page');
                $this->session->unset_userdata('reg');
                $userData['logoutUrl'] = $this->facebook->logout_url();
                $userData['oauthURL'] = base_url() . $this->config->item('linkedin_redirect_url') . '?oauth_init=1';
                $userData['authUrl'] = $this->facebook->login_url();
                $userData['loginURL'] = $this->google->loginURL();
                $userData['data'] = $this->user_model->fetchUser();
                $userid = $this->session->userdata('userData');
                //print_r($userid);exit;
                if (!isset($userid->id)) {
                    $userData['data1'] = $this->user_model->fetch_user_data($userid['oauth_uid']);
                } else {
                    $userData['data1'] = $this->user_model->fetch_user($userid->id);
                }
                $this->session->set_userdata('page', 'joinus');
                $this->load->view('admin/userhome', $userData);
            } else {
                $this->output->set_status_header('404');
                $this->load->view('Error_page');
            }
        } else {
            $this->output->set_status_header('404');
            $this->load->view('Error_page');
        }
    }

    public function add_counselor() {

        $userid = $this->session->userdata('userData');
        if ($this->session->userdata('loggedIn') == true) {

            if ($userid->role_id == 1 || $userid->role_id == 2) {

                $this->session->unset_userdata('login');
                $userData['logoutUrl'] = $this->facebook->logout_url();
                $userData['oauthURL'] = base_url() . $this->config->item('linkedin_redirect_url') . '?oauth_init=1';
                $userData['authUrl'] = $this->facebook->login_url();
                $userData['loginURL'] = $this->google->loginURL();
                $userData['data'] = $this->user_model->fetchUser();
                $userData['email'] = "";
                $userData['login'] = 0;
                $userid = $this->session->userdata('userData');
                if (!isset($userid->id)) {
                    $userData['data1'] = $this->user_model->fetch_user_data($userid['oauth_uid']);
                } else {
                    $userData['data1'] = $this->user_model->fetch_user($userid->id);
                }
                $this->session->set_userdata('page', 'joinus');
                $userData['counselling_types'] = $this->user_model->getCounsellingTypes();
                $this->load->view('admin/addCounselor', $userData);
            } else {
                $this->output->set_status_header('404');
                $this->load->view('Error_page');
            }
        } else {
            $this->output->set_status_header('404');
            $this->load->view('Error_page');
        }
    }

    public function save_counselor() {

        $this->form_validation->set_rules('username', 'User Name', 'trim|required|max_length[30]');
        $this->form_validation->set_rules('email', 'Email ID', 'trim|required');
        $this->form_validation->set_rules('password', 'Password', 'trim|required');

        //validate form input
        if ($this->form_validation->run() == FALSE) {
            // fails

            $userData['logoutUrl'] = $this->facebook->logout_url();
            $userData['oauthURL'] = base_url() . $this->config->item('linkedin_redirect_url') . '?oauth_init=1';
            $userData['authUrl'] = $this->facebook->login_url();
            $userData['loginURL'] = $this->google->loginURL();
            $userData['data'] = $this->user_model->fetchUser();
            $userData['email'] = "";
            $userData['login'] = 0;
            $this->session->set_userdata('page', 'joinus');
            redirect(site_url() . 'user/joinus');
        } else {

            if (!empty($_FILES['image']['name'])) {

                $config['upload_path'] = 'uploads/images/';
                $config['allowed_types'] = 'jpg|jpeg|png|gif';
                $config['file_name'] = $_FILES['image']['name'];

                //Load upload library and initialize configuration
                $this->load->library('upload', $config);
                $this->upload->initialize($config);

                if ($this->upload->do_upload('image')) {
                    $uploadData = $this->upload->data();
                    $picture = base_url() . 'uploads/images/' . $uploadData['file_name'];
                } else {
                    $picture = '';
                }
            } else {
                $picture = '';
            }

            $data = array(
                'username' => $this->input->post('username'),
                'email' => $this->input->post('email'),
                'password' => md5($this->input->post('password')),
                'photo' => $picture,
                'phone' => $this->input->post('phone'),
                'gender' => $this->input->post('gender'),
                'role_id' => $this->input->post('role'),
                'status' => $this->input->post('status'),
                'created_at' => date("Y-m-d H:i:s")
            );
            $data1 = array(
                'domain' => json_encode($this->input->post('domain')),
                'experience' => $this->input->post('experience'),
                'languages' => json_encode($this->input->post('languages')),
                'certifications' => $this->input->post('certifications'),
                'accrediations' => $this->input->post('accrediations'),
            );

            $data_user = $this->user_model->check_login1($this->input->post('email'), $this->input->post('password'));
            //print_r($data_user);exit;
            if ($data_user > 0) {
                if ($this->input->post('page') == 'add_counselor') {
                    redirect(site_url() . 'admin/counselorlist');
                } else {
                    $this->session->set_userdata('page', 'joinus');

                    $userData['logoutUrl'] = $this->facebook->logout_url();
                    $userData['oauthURL'] = base_url() . $this->config->item('linkedin_redirect_url') . '?oauth_init=1';
                    $userData['authUrl'] = $this->facebook->login_url();
                    $userData['userData'] = $userData;
                    $userData['loginURL'] = $this->google->loginURL();
                    $userData['email'] = "";
                    $userData['login'] = 0;
                    $userData['msg'] = "User already registerd with this email id!!! Please login";
                    // Load login & profile view

                    $this->load->view('joinus', $userData);
                }
            } else {
                if ($this->user_model->insertCounselor($data, $data1)) {


                    $email = $this->input->post('email');
                    $htmlContent = '<h1>eYogi Login Credentials</h1>';
                    $htmlContent .= '<p>Please use the following credentials to login to eYogi.</p>';
                    $htmlContent .= '<p>Username : ' . $this->input->post('email') . '</p>';
                    $htmlContent .= '<p>Password : ' . $this->input->post('password') . '</p>';

                    $config['mailtype'] = 'html';
                    $this->email->initialize($config);
                    $this->email->to($email);
                    $this->email->from('admin@eyogi.in', 'eYogi');
                    $this->email->subject('Counselor Login Details');
                    $this->email->message($htmlContent);
                    $this->email->send();

                    if ($this->input->post('page') == 'add_counselor') {
                        redirect(site_url() . 'admin/counselorlist');
                    } else {
                        $data_user = $this->user_model->check_login($this->input->post('email'), $this->input->post('password'));
                        $this->session->set_userdata('userData', $data_user);
                        $this->session->set_userdata('loggedIn', true);
                        redirect(site_url() . 'counselor/index');
                    }
                }
            }

            // insert form data into database
        }
    }

    public function myprofile() {
        //redirect to login page if user not logged in
        $this->session->unset_userdata('login');
        if ($this->session->userdata('loggedIn') == true) {

            $userData['logoutUrl'] = $this->facebook->logout_url();
            $userData['oauthURL'] = base_url() . $this->config->item('linkedin_redirect_url') . '?oauth_init=1';
            $userData['authUrl'] = $this->facebook->login_url();
            $userData['loginURL'] = $this->google->loginURL();

            $userid = $this->session->userdata('userData');
            if (!isset($userid->id)) {
                $userData['data1'] = $this->user_model->fetch_user_data($userid['oauth_uid']);
            } else {
                $userData['data1'] = $this->user_model->fetch_user($userid->id);
            }

            $userData['data'] = $this->user_model->getUser($userid->id);
            $userData['coupon_details'] = $this->user_model->getStatus($userid->id);

            $this->load->view('admin/myprofile', $userData);
        } else {
            $this->output->set_status_header('404');
            $this->load->view('Error_page');
        }
    }

    public function editprofile() {

        $this->session->unset_userdata('login');
        if ($this->session->userdata('loggedIn') == true) {
            $userData['logoutUrl'] = $this->facebook->logout_url();
            $userData['oauthURL'] = base_url() . $this->config->item('linkedin_redirect_url') . '?oauth_init=1';
            $userData['authUrl'] = $this->facebook->login_url();
            $userData['loginURL'] = $this->google->loginURL();
            $userid = $this->session->userdata('userData');
            $userid = $this->session->userdata('userData');
            $userData['data1'] = $this->user_model->fetch_user($userid->id);
            $userData['data'] = $this->user_model->getUser($userid->id);
            $userData['counselling_types'] = $this->user_model->getCounsellingTypes();
            $this->load->view('admin/editprofile', $userData);
        } else {
            $this->output->set_status_header('404');
            $this->load->view('Error_page');
        }
    }

    public function update_counselor() {

        $data = $this->session->userdata('userData');
        if (!empty($_FILES['image']['name'])) {

            $config['upload_path'] = 'uploads/images/';
            $config['allowed_types'] = 'jpg|jpeg|png|gif';
            $config['file_name'] = $_FILES['image']['name'];

            //Load upload library and initialize configuration
            $this->load->library('upload', $config);
            $this->upload->initialize($config);

            if ($this->upload->do_upload('image')) {
                $uploadData = $this->upload->data();
                $picture = base_url() . 'uploads/images/' . $uploadData['file_name'];
            } else {
                $picture = '';
            }
            $data = array(
                'username' => $this->input->post('username'),
                'email' => $this->input->post('email'),
                'phone' => $this->input->post('phone'),
                'gender' => $this->input->post('gender'),
                'qualification' => $this->input->post('qualification'),
                'employed' => $this->input->post('employed'),
                'marital_status' => $this->input->post('marital_status'),
                'kids' => $this->input->post('kids'),
                'photo' => $picture,
                'updated_at' => date("Y-m-d H:i:s")
            );
        } else {

            $data = array(
                'username' => $this->input->post('username'),
                'email' => $this->input->post('email'),
                'phone' => $this->input->post('phone'),
                'gender' => $this->input->post('gender'),
                'qualification' => $this->input->post('qualification'),
                'employed' => $this->input->post('employed'),
                'marital_status' => $this->input->post('marital_status'),
                'kids' => $this->input->post('kids'),
                'updated_at' => date("Y-m-d H:i:s")
            );
        }
        $id = $this->input->post('id_hidden');
        $role_id = $this->input->post('role_id');

        // insert form data into database
        if ($this->user_model->updateCounselor($id, $data)) {

//                $email=$this->input->post('email');
//                $htmlContent = '<h1>eYogi Login Credentials</h1>';
//                $htmlContent .= '<p>Please use the following credentials to login to eYogi.</p>';
//                $htmlContent .= '<p>Username : '.$this->input->post('email').'</p>';
//                $htmlContent .= '<p>Password : '.$this->input->post('password').'</p>';
//
//                $config['mailtype'] = 'html';
//                $this->email->initialize($config);
//                $this->email->to($email);
//                $this->email->from('admin@eyogi.in', 'eYogi');
//                $this->email->subject('Counselor Login Details');
//                $this->email->message($htmlContent);
//                $this->email->send();

            if ($role_id == 1) {
                redirect(site_url() . 'admin/index');
                //$this->load->view('admin/userhome', $userData, $data);
            } else if ($role_id == 3) {
                redirect(site_url() . 'counselor/index');
            } else {

                redirect(site_url() . 'admin/myprofile');
            }
        }
    }

    public function update_password() {
        $id = $this->input->post('user_id');
        $currentpassword = $this->input->post('current_password');

        $newpassword = $this->input->post('new_password');

        $res = $this->user_model->checkpassword($id, md5($currentpassword));
        if (count($res) == 1) {
            $res = $this->user_model->updatePassword($id, md5($newpassword));
        }
        redirect('admin/myprofile');
    }

    public function userlist() {

        if ($this->session->userdata('loggedIn') == true) {

            $userData['logoutUrl'] = $this->facebook->logout_url();
            $userData['oauthURL'] = base_url() . $this->config->item('linkedin_redirect_url') . '?oauth_init=1';
            $userData['authUrl'] = $this->facebook->login_url();
            $userData['loginURL'] = $this->google->loginURL();
            $userid = $this->session->userdata('userData');
            $userData['email'] = "";
            $userData['login'] = 0;
            $userid = $this->session->userdata('userData');
            $role = $userid->role_id;
            $userData['data1'] = $this->user_model->fetch_user($userid->id);
            $userData['data'] = $this->user_model->getUser($userid->id);
            if ($userid->role_id == 1 || $userid->role_id == 2) {
                $userData['userDetails'] = $this->user_model->getUserList();
                $userData['couponDetails'] = $this->user_model->getCouponDetails();
                $this->load->view('admin/userlist', $userData);
            } else{
                $this->output->set_status_header('404');
                $this->load->view('Error_page');
            }
        } else {

            $this->output->set_status_header('404');
            $this->load->view('Error_page');
        }
    }

    public function updateUser() {

        $id = $this->uri->segment(3);
        $userData['logoutUrl'] = $this->facebook->logout_url();
        $userData['oauthURL'] = base_url() . $this->config->item('linkedin_redirect_url') . '?oauth_init=1';
        $userData['authUrl'] = $this->facebook->login_url();
        $userData['loginURL'] = $this->google->loginURL();
        $userid = $this->session->userdata('userData');
        $userData['email'] = "";
        $userData['login'] = 0;
        $userid = $this->session->userdata('userData');
        $userData['data1'] = $this->user_model->fetch_user($userid->id);
        $userData['data'] = $this->user_model->getUser($userid->id);
        $userData['userDetails'] = $this->user_model->getUserdata2($id);
        if (!empty($userData['userDetails'])) {
            $this->load->view('admin/updateuser', $userData);
        } else {
            $this->output->set_status_header('404');
            $this->load->view('Error_page');
        }
    }

    public function update_user() {
        if (!empty($_FILES['image']['name'])) {

            $config['upload_path'] = 'uploads/images/';
            $config['allowed_types'] = 'jpg|jpeg|png|gif';
            $config['file_name'] = $_FILES['image']['name'];

            //Load upload library and initialize configuration
            $this->load->library('upload', $config);
            $this->upload->initialize($config);

            if ($this->upload->do_upload('image')) {
                $uploadData = $this->upload->data();
                $picture = base_url() . 'uploads/images/' . $uploadData['file_name'];
            } else {
                $picture = '';
            }
            $data = array(
                'username' => $this->input->post('username'),
                'email' => $this->input->post('email'),
                'phone' => $this->input->post('phone'),
                'status' => $this->input->post('status'),
                'photo' => $picture,
                'updated_at' => date("Y-m-d H:i:s")
            );
        } else {

            $data = array(
                'username' => $this->input->post('username'),
                'email' => $this->input->post('email'),
                'phone' => $this->input->post('phone'),
                'status' => $this->input->post('status'),
                'updated_at' => date("Y-m-d H:i:s")
            );
        }
        $id = $this->input->post('id_hidden');


        // insert form data into database
        if ($this->user_model->updateuser($id, $data)) {

//                $email=$this->input->post('email');
//                $htmlContent = '<h1>eYogi Login Credentials</h1>';
//                $htmlContent .= '<p>Please use the following credentials to login to eYogi.</p>';
//                $htmlContent .= '<p>Username : '.$this->input->post('email').'</p>';
//                $htmlContent .= '<p>Password : '.$this->input->post('password').'</p>';
//
//                $config['mailtype'] = 'html';
//                $this->email->initialize($config);
//                $this->email->to($email);
//                $this->email->from('admin@eyogi.in', 'eYogi');
//                $this->email->subject('Counselor Login Details');
//                $this->email->message($htmlContent);
//                $this->email->send();

            redirect('admin/userlist');
        }
    }

    public function delUser() {
        $id = $this->input->post('id');
        $this->db->where('id', $id);
        $this->db->delete('ey_user');
        echo 1;
    }

    public function updateCounselor() {

        if ($this->session->userdata('loggedIn') == true) {

            $id = $this->uri->segment(3);
            $userData['logoutUrl'] = $this->facebook->logout_url();
            $userData['oauthURL'] = base_url() . $this->config->item('linkedin_redirect_url') . '?oauth_init=1';
            $userData['authUrl'] = $this->facebook->login_url();
            $userData['loginURL'] = $this->google->loginURL();
            $userid = $this->session->userdata('userData');
            $userData['email'] = "";
            $userData['login'] = 0;
            $userid = $this->session->userdata('userData');
            $userData['data1'] = $this->user_model->fetch_user($userid->id);
            $userData['data'] = $this->user_model->getUser($userid->id);
            $userData['userDetails'] = $this->user_model->getUserdata2($id);

            if (!empty($userData['userDetails'])) {
                $this->load->view('admin/updateCounselor', $userData);
            } else {
                $this->output->set_status_header('404');
                $this->load->view('Error_page');
            }
        } else {
            $this->output->set_status_header('404');
            $this->load->view('Error_page');
        }
    }

    public function updatecounsel() {
        if (!empty($_FILES['image']['name'])) {

            $config['upload_path'] = 'uploads/images/';
            $config['allowed_types'] = 'jpg|jpeg|png|gif';
            $config['file_name'] = $_FILES['image']['name'];

            //Load upload library and initialize configuration
            $this->load->library('upload', $config);
            $this->upload->initialize($config);

            if ($this->upload->do_upload('image')) {
                $uploadData = $this->upload->data();
                $picture = base_url() . 'uploads/images/' . $uploadData['file_name'];
            } else {
                $picture = '';
            }
            $data = array(
                'username' => $this->input->post('username'),
                'email' => $this->input->post('email'),
                'phone' => $this->input->post('phone'),
                'photo' => $picture,
                'updated_at' => date("Y-m-d H:i:s")
            );
        } else {

            $data = array(
                'username' => $this->input->post('username'),
                'email' => $this->input->post('email'),
                'phone' => $this->input->post('phone'),
                'updated_at' => date("Y-m-d H:i:s")
            );
        }
        $id = $this->input->post('id_hidden');


        // insert form data into database
        if ($this->user_model->updateuser($id, $data)) {

//                $email=$this->input->post('email');
//                $htmlContent = '<h1>eYogi Login Credentials</h1>';
//                $htmlContent .= '<p>Please use the following credentials to login to eYogi.</p>';
//                $htmlContent .= '<p>Username : '.$this->input->post('email').'</p>';
//                $htmlContent .= '<p>Password : '.$this->input->post('password').'</p>';
//
//                $config['mailtype'] = 'html';
//                $this->email->initialize($config);
//                $this->email->to($email);
//                $this->email->from('admin@eyogi.in', 'eYogi');
//                $this->email->subject('Counselor Login Details');
//                $this->email->message($htmlContent);
//                $this->email->send();

            redirect('admin/counselorlist');
        }
    }

    public function counselorlist() {

        $userid = $this->session->userdata('userData');
        if ($this->session->userdata('loggedIn') == true) {

            if ($userid->role_id == 1 || $userid->role_id == 2) {

                $userData['logoutUrl'] = $this->facebook->logout_url();
                $userData['oauthURL'] = base_url() . $this->config->item('linkedin_redirect_url') . '?oauth_init=1';
                $userData['authUrl'] = $this->facebook->login_url();
                $userData['loginURL'] = $this->google->loginURL();
                $userid = $this->session->userdata('userData');
                $userData['email'] = "";
                $userData['login'] = 0;
                $userid = $this->session->userdata('userData');
                $userData['data1'] = $this->user_model->fetch_user($userid->id);
                $userData['data'] = $this->user_model->getUser($userid->id);
                $userData['userDetails'] = $this->user_model->getCounselorList();

                $this->load->view('admin/counselorlist', $userData);
            } else {
                $this->output->set_status_header('404');
                $this->load->view('Error_page');
            }
        } else {
            $this->output->set_status_header('404');
            $this->load->view('Error_page');
        }
    }

    public function approveUser() {
        $id = $this->input->post('id');
        // $timing = $this->input->post('timing');
        $data = array(
            'user_id' => $this->input->post('id'),
            'minutes' => $this->input->post('hours'),
            'amount' => $this->input->post('amount'),
        );
        $data1 = array('status' => $this->input->post('user_status'));

        $this->user_model->approveUser($id, $data, $data1);
        echo 1;
    }

    public function submitTimeSlots() {
        $id = $this->input->post('id');
        $timing = $this->input->post('timing');
        $hours = $this->input->post('hours');

        //  print_r($timing);exit;
        $this->user_model->submitTimeSlots($id, $timing, $hours);
        echo 1;
    }

    public function getUserDetails() {
        $id = $this->input->post('id');
        $res['data'] = $this->user_model->getUserDetails($id);
        $res['user'] = $this->user_model->getUserdata2($id);
        $res['timing'] = $this->user_model->getUserTiming($id);
        if (!empty($res['timing'])) {
            foreach ($res['timing'] as $val) {
                $arr[] = $val->time_starts . " - " . $val->time_ends;
                $workings_hours = $val->working_hours;
            }
            $arr1 = explode("-", $workings_hours);
            $res['time_starts'] = $this->hoursToMinutes(date("G:i", strtotime($arr1[0])));
            $res['time_ends'] = $this->hoursToMinutes(date("G:i", strtotime($arr1[1])));
            $res['timing'] = $arr;
            $res['workings_hours'] = $workings_hours;
        }

        print json_encode($res);
    }

    function hoursToMinutes($hours) {
        if (strstr($hours, ':')) {
            # Split hours and minutes.
            $separatedData = explode(':', $hours);

            $minutesInHours = $separatedData[0] * 60;
            $minutesInDecimals = $separatedData[1];

            $totalMinutes = $minutesInHours + $minutesInDecimals;
        } else {
            $totalMinutes = $hours * 60;
        }

        return $totalMinutes;
    }

    public function counselorSchedule() {

        if ($this->session->userdata('loggedIn') == true) {

            $id = $this->uri->segment(3);
            $userData['logoutUrl'] = $this->facebook->logout_url();
            $userData['oauthURL'] = base_url() . $this->config->item('linkedin_redirect_url') . '?oauth_init=1';
            $userData['authUrl'] = $this->facebook->login_url();
            $userData['loginURL'] = $this->google->loginURL();
            $userid = $this->session->userdata('userData');
            $userData['email'] = "";
            $userData['login'] = 0;
            $userid = $this->session->userdata('userData');
            $userData['data1'] = $this->user_model->fetch_user($userid->id);
            $userData['data'] = $this->user_model->getUser($userid->id);
            $userData['userDetails'] = $this->user_model->getUserdata($id = "", $sche_id = "");

            $this->load->view('admin/schedule', $userData);
        } else {
            $this->output->set_status_header('404');
            $this->load->view('Error_page');
        }
    }

    public function schedule() {

        if ($this->session->userdata('loggedIn') == true) {

            $id = $this->uri->segment(3);
            $userData['logoutUrl'] = $this->facebook->logout_url();
            $userData['oauthURL'] = base_url() . $this->config->item('linkedin_redirect_url') . '?oauth_init=1';
            $userData['authUrl'] = $this->facebook->login_url();
            $userData['loginURL'] = $this->google->loginURL();
            $userData['email'] = "";
            $userData['login'] = 0;
            $userid = $this->session->userdata('userData');
            $userData['data1'] = $this->user_model->fetch_user($userid->id);
            $userData['data'] = $this->user_model->getUser($userid->id);
            $userData['userDetails'] = $this->user_model->getUserList();

            $userData['schedule'] = $this->user_model->getScheduleDetails($id);
            $schedule_ids = $this->user_model->getScheduleIds();
            $res = "";
            foreach ($schedule_ids as $id) {
                $res[] = $id->schedule_id;
            }
            $userData['ids'] = $res;
            $this->load->view('admin/schedule_calendar', $userData);
        } else {
            $this->output->set_status_header('404');
            $this->load->view('Error_page');
        }
    }

    public function checkSchedule() {
        $id = $this->input->post('id');
        $date = $this->input->post('date');
        $counsid = $this->input->post('couns_id');
        $res = $this->user_model->checkSchedule($id, date('Y-m-d', strtotime($date)), $counsid);
        echo count($res);
    }

    public function checkCounselorWorkingDay() {

        $day = $this->input->post('day');
        $user_id = $this->input->post('user_id');
        $res = $this->user_model->checkWorkingId($user_id);
        $date = $this->input->post('date');
        $new_date = date('h:i A');
        $todate = date('Y-m-d');
        if(strtotime($todate) == strtotime($date))
        {
            $timeSlot = $this->user_model->get_counstime($user_id, $day);
            if(!empty($timeSlot)){
                if(date('H:i', strtotime($new_date)) > date('H:i', strtotime($timeSlot[0]->time_ends))){
                    echo 1;exit;
                }
            
            }
        }
        $days = json_decode($res->days);
        if (in_array($day, $days)) {
            if (strtotime($date) > strtotime(date('Y-m-d'))) {
                $timeSlot = $this->user_model->get_counstime($user_id, $day);
                if (!empty($timeSlot)) {
                    $timeStart = $timeSlot[0]->time_starts;
                } else {
                    $timeStart = "12:00 AM";
                }
            }
            else {
                $timeSlot = $this->user_model->get_counstime($user_id, $day);
                $current_date = date('d-M-Y g:i:s A');

                $current_time = strtotime($current_date);

                $frac = 900;
                $r = $current_time % $frac;

                $new_time = $current_time + ($frac - $r);
                $new_date = date('h:i A', $new_time);

                if (date('H:i', strtotime($new_date)) > date('H:i', strtotime($timeSlot[0]->time_starts))) {
                    $timeStart = $new_date;
                } else {
                    $timeStart = $timeSlot[0]->time_starts;
                }
            }

            $time = explode("-", $timeSlot[0]->working_hours);
            $data_ren = $this->user_model->getCounsRenumeration($user_id);
            if (!empty($data_ren)) {
                $timeSlot1['duration'] = $data_ren[0]['minutes'];
            } else {
                $timeSlot1['duration'] = '45';  // split by 30 mins
            }
            $startTime = date('H:i:s', strtotime($time[1]));

            $date = "2018-03-12" . " " . $startTime;
            date_default_timezone_set('Asia/Kolkata');
            $time = date('h:i A', strtotime('-' . $timeSlot1['duration'] . ' minutes', strtotime($date)));

            echo $timeStart . "-" . $time;
        } else {
            echo 0;
        }
    }

    public function couponlist() {
        if ($this->session->userdata('loggedIn') == true) {

            $userid = $this->session->userdata('userData');
            if ($userid->role_id == 1 || $userid->role_id == 2) {

                $userData['logoutUrl'] = $this->facebook->logout_url();
                $userData['oauthURL'] = base_url() . $this->config->item('linkedin_redirect_url') . '?oauth_init=1';
                $userData['authUrl'] = $this->facebook->login_url();
                $userData['loginURL'] = $this->google->loginURL();
                $userid = $this->session->userdata('userData');
                $userData['email'] = "";
                $userData['login'] = 0;

                $userid = $this->session->userdata('userData');
                if (!isset($userid->id)) {
                    $userData['data1'] = $this->user_model->fetch_user_data($userid['oauth_uid']);
                } else {
                    $userData['data1'] = $this->user_model->fetch_user($userid->id);
                }

                $userData['userDetails'] = $this->user_model->getcouponlist();
                $this->load->view('admin/couponlist', $userData);
            } else {
                $this->output->set_status_header('404');
                $this->load->view('Error_page');
            }
        } else {
            $this->output->set_status_header('404');
            $this->load->view('Error_page');
        }
    }

    public function addcoupon() {

        if ($this->session->userdata('loggedIn') == true) {

            $userData['logoutUrl'] = $this->facebook->logout_url();
            $userData['oauthURL'] = base_url() . $this->config->item('linkedin_redirect_url') . '?oauth_init=1';
            $userData['authUrl'] = $this->facebook->login_url();
            $userData['loginURL'] = $this->google->loginURL();
            $userid = $this->session->userdata('userData');
            $userData['email'] = "";
            $userData['login'] = 0;

            $userid = $this->session->userdata('userData');
            if (!isset($userid->id)) {
                $userData['data1'] = $this->user_model->fetch_user_data($userid['oauth_uid']);
            } else {
                $userData['data1'] = $this->user_model->fetch_user($userid->id);
            }

            $this->load->view('admin/addcoupon', $userData);
        } else {
            $this->output->set_status_header('404');
            $this->load->view('Error_page');
        }
    }

    function generateRandomString($length = 6) {
        $characters = '0123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ';
        $charactersLength = strlen($characters);
        $randomString = '';
        for ($i = 0; $i < $length; $i++) {
            $randomString .= $characters[rand(0, $charactersLength - 1)];
        }
        echo $randomString;
    }

    public function save_coupon() {

        $data = array(
            'coupon_name' => $this->input->post('coupon_name'),
            'amount' => $this->input->post('amount'),
            'code' => $this->input->post('code'),
            'status' => $this->input->post('status'),
            'created_at' => date("Y-m-d H:i:s")
        );

        $this->user_model->insertcoupon($data);
        redirect('admin/couponlist');
    }

    public function updatecoupon() {
        $id = $this->uri->segment(3);
        $userData['logoutUrl'] = $this->facebook->logout_url();
        $userData['oauthURL'] = base_url() . $this->config->item('linkedin_redirect_url') . '?oauth_init=1';
        $userData['authUrl'] = $this->facebook->login_url();
        $userData['loginURL'] = $this->google->loginURL();
        $userid = $this->session->userdata('userData');
        $userData['email'] = "";
        $userData['login'] = 0;
        $userid = $this->session->userdata('userData');
        $userData['data1'] = $this->user_model->fetch_user($userid->id);
        $userData['data'] = $this->user_model->getUser($userid->id);
    }

    public function delcoupon() {
        $id = $this->input->post('id');
        //print_r($id);
        $this->db->where('id', $id);
        $this->db->delete('ey_coupon_code');
        echo 1;
    }

    public function admin_list() {

        $userid = $this->session->userdata('userData');
        if ($this->session->userdata('loggedIn') == true) {

            if ($userid->role_id == 1) {

                $userData['logoutUrl'] = $this->facebook->logout_url();
                $userData['oauthURL'] = base_url() . $this->config->item('linkedin_redirect_url') . '?oauth_init=1';
                $userData['authUrl'] = $this->facebook->login_url();
                $userData['loginURL'] = $this->google->loginURL();
                $userid = $this->session->userdata('userData');
                $userData['email'] = "";
                $userData['login'] = 0;

                $userid = $this->session->userdata('userData');
                if (!isset($userid->id)) {
                    $userData['data1'] = $this->user_model->fetch_user_data($userid['oauth_uid']);
                } else {
                    $userData['data1'] = $this->user_model->fetch_user($userid->id);
                }
                $userData['userDetails'] = $this->user_model->getAdminList();
                $this->load->view('admin/adminlist', $userData);
            } else {
                $this->output->set_status_header('404');
                $this->load->view('Error_page');
            }
        } else {
            $this->output->set_status_header('404');
            $this->load->view('Error_page');
        }
    }

    public function add_admin() {

        $userid = $this->session->userdata('userData');
        if ($this->session->userdata('loggedIn') == true) {

            $userData['logoutUrl'] = $this->facebook->logout_url();
            $userData['oauthURL'] = base_url() . $this->config->item('linkedin_redirect_url') . '?oauth_init=1';
            $userData['authUrl'] = $this->facebook->login_url();
            $userData['loginURL'] = $this->google->loginURL();
            $userid = $this->session->userdata('userData');
            $userData['email'] = "";
            $userData['login'] = 0;

            $userid = $this->session->userdata('userData');
            if (!isset($userid->id)) {
                $userData['data1'] = $this->user_model->fetch_user_data($userid['oauth_uid']);
            } else {
                $userData['data1'] = $this->user_model->fetch_user($userid->id);
            }

            $this->load->view('admin/addAdmin', $userData);
        } else {
            $this->output->set_status_header('404');
            $this->load->view('Error_page');
        }
    }

    public function save_admin() {

        $this->form_validation->set_rules('username', 'User Name', 'trim|required|min_length[3]|max_length[30]');
        $this->form_validation->set_rules('email', 'Email ID', 'trim|required');
        $this->form_validation->set_rules('password', 'Password', 'trim|required');


        //validate form input
        if ($this->form_validation->run() == FALSE) {
            // fails
            $userData['logoutUrl'] = $this->facebook->logout_url();
            $userData['oauthURL'] = base_url() . $this->config->item('linkedin_redirect_url') . '?oauth_init=1';
            $userData['authUrl'] = $this->facebook->login_url();
            $userData['loginURL'] = $this->google->loginURL();
            $userData['data'] = $this->user_model->fetchUser();
            $userData['email'] = "";
            $userData['login'] = 0;
            $this->session->set_userdata('page', 'joinus');
            $this->load->view('admin/addAdmin', $userData);
        } else {

            if (!empty($_FILES['image']['name'])) {

                $config['upload_path'] = 'uploads/images/';
                $config['allowed_types'] = 'jpg|jpeg|png|gif';
                $config['file_name'] = $_FILES['image']['name'];

                //Load upload library and initialize configuration
                $this->load->library('upload', $config);
                $this->upload->initialize($config);

                if ($this->upload->do_upload('image')) {
                    $uploadData = $this->upload->data();
                    $picture = base_url() . 'uploads/images/' . $uploadData['file_name'];
                } else {
                    $picture = '';
                }
            } else {
                $picture = '';
            }

            $data = array(
                'username' => $this->input->post('username'),
                'email' => $this->input->post('email'),
                'password' => md5($this->input->post('password')),
                'photo' => $picture,
                'phone' => $this->input->post('phone'),
                'gender' => $this->input->post('gender'),
                'role_id' => $this->input->post('role'),
                'status' => $this->input->post('status'),
                'created_at' => date("Y-m-d H:i:s")
            );
            $data1 = array(
                'domain' => $this->input->post('domain'),
            );
            // insert form data into database
            if ($this->user_model->insertAdmin($data, $data1)) {

                $email = $this->input->post('email');
                $htmlContent = '<h1>eYogi Login Credentials</h1>';
                $htmlContent .= '<p>Please use the following credentials to login to eYogi.</p>';
                $htmlContent .= '<p>Username : ' . $this->input->post('email') . '</p>';
                $htmlContent .= '<p>Password : ' . $this->input->post('password') . '</p>';

                $config['mailtype'] = 'html';
                $this->email->initialize($config);
                $this->email->to($email);
                $this->email->from('admin@eyogi.in', 'eYogi');
                $this->email->subject('Admin Login Details');
                $this->email->message($htmlContent);
                $this->email->send();

                redirect('admin/admin_list');
            }
        }
    }

    public function updateAdmin() {

        $userid = $this->session->userdata('userData');
        if ($this->session->userdata('loggedIn') == true) {

            $id = $this->uri->segment(3);
            $userData['logoutUrl'] = $this->facebook->logout_url();
            $userData['oauthURL'] = base_url() . $this->config->item('linkedin_redirect_url') . '?oauth_init=1';
            $userData['authUrl'] = $this->facebook->login_url();
            $userData['loginURL'] = $this->google->loginURL();
            $userid = $this->session->userdata('userData');
            $userData['email'] = "";
            $userData['login'] = 0;
            $userid = $this->session->userdata('userData');
            $userData['data1'] = $this->user_model->fetch_user($userid->id);
            $userData['data'] = $this->user_model->getUser($userid->id);
            $userData['userDetails'] = $this->user_model->getUserdata2($id);
            if (!empty($userData['userDetails'])) {
                $this->load->view('admin/updateadmin', $userData);
            } else {
                $this->output->set_status_header('404');
                $this->load->view('Error_page');
            }
        } else {
            $this->output->set_status_header('404');
            $this->load->view('Error_page');
        }
    }

    public function update_admin() {
        if (!empty($_FILES['image']['name'])) {

            $config['upload_path'] = 'uploads/images/';
            $config['allowed_types'] = 'jpg|jpeg|png|gif';
            $config['file_name'] = $_FILES['image']['name'];

            //Load upload library and initialize configuration
            $this->load->library('upload', $config);
            $this->upload->initialize($config);

            if ($this->upload->do_upload('image')) {
                $uploadData = $this->upload->data();
                $picture = base_url() . 'uploads/images/' . $uploadData['file_name'];
            } else {
                $picture = '';
            }
            $data = array(
                'username' => $this->input->post('username'),
                'email' => $this->input->post('email'),
                'phone' => $this->input->post('phone'),
                'status' => $this->input->post('status'),
                'photo' => $picture,
                'updated_at' => date("Y-m-d H:i:s")
            );
        } else {

            $data = array(
                'username' => $this->input->post('username'),
                'email' => $this->input->post('email'),
                'phone' => $this->input->post('phone'),
                'status' => $this->input->post('status'),
                'updated_at' => date("Y-m-d H:i:s")
            );
        }
        $id = $this->input->post('id_hidden');


        // insert form data into database
        if ($this->user_model->updateuser($id, $data)) {
            redirect('admin/admin_list');
        }
    }

    public function delAdmin() {
        $id = $this->input->post('id');
        $this->db->where('id', $id);
        $this->db->delete('ey_user');
        echo 1;
    }

    public function checkCounsData() {
        $id = $this->input->post('id');

        $res = $this->user_model->checkCounsData($id);

        echo count($res);
    }

    public function changeSlot() {
        $starttime = date("H:i", strtotime($this->input->post('start')));
        $endtime = date("H:i", strtotime($this->input->post('end')));
        $id = $this->input->post('id');
        $data_ren = $this->user_model->getCounsRenumeration($id);
        if (!empty($data_ren)) {
            $duration = $data_ren[0]['minutes'];
        } else {
            $duration = '60';  // split by 30 mins
        }

        $array_of_time = array();
        $start_time = strtotime($starttime); //change to strtotime
        $end_time = strtotime($endtime); //change to strtotime

        $add_mins = $duration * 60;

        while ($start_time < $end_time && ($start_time + $add_mins) <= $end_time) { // loop between time
            $array_of_time[] = date("h:i A", $start_time) . ' - ' . date("h:i A", $start_time + $add_mins);
            $start_time += $add_mins; // to check endtie=me
        }


        print json_encode($array_of_time);
    }

    public function purchaseDetails() {

        $userid = $this->session->userdata('userData');
        if ($this->session->userdata('loggedIn') == true) {

            if ($userid->role_id == 1 || $userid->role_id == 2) {

                $userData['logoutUrl'] = $this->facebook->logout_url();
                $userData['oauthURL'] = base_url() . $this->config->item('linkedin_redirect_url') . '?oauth_init=1';
                $userData['authUrl'] = $this->facebook->login_url();
                $userData['loginURL'] = $this->google->loginURL();
                $userData['data'] = $this->user_model->fetchUser();
                $userid = $this->session->userdata('userData');
                if (!isset($userid->id)) {
                    $userData['data1'] = $this->user_model->fetch_user_data($userid['oauth_uid']);
                } else {
                    $userData['data1'] = $this->user_model->fetch_user($userid->id);
                }

                $userData['payment'] = $this->user_model->fetchPaymentDetails();
                //print_r($userData);exit;
                $this->load->view('admin/purchase_Report', $userData);
            } else {

                $this->output->set_status_header('404');
                $this->load->view('Error_page');
            }
        } else {
            $this->output->set_status_header('404');
            $this->load->view('Error_page');
        }
    }

    public function callDetails() {
        $userid = $this->session->userdata('userData');
        if ($this->session->userdata('loggedIn') == true) {

            if ($userid->role_id == 1 || $userid->role_id == 2) {

                $userData['logoutUrl'] = $this->facebook->logout_url();
                $userData['oauthURL'] = base_url() . $this->config->item('linkedin_redirect_url') . '?oauth_init=1';
                $userData['authUrl'] = $this->facebook->login_url();
                $userData['loginURL'] = $this->google->loginURL();
                $userData['data'] = $this->user_model->fetchUser();
                $userid = $this->session->userdata('userData');
                if (!isset($userid->id)) {
                    $userData['data1'] = $this->user_model->fetch_user_data($userid['oauth_uid']);
                } else {
                    $userData['data1'] = $this->user_model->fetch_user($userid->id);
                }

                $userData['call'] = $this->user_model->fetchCalls();

//                print_r($userData['call']);exit;
                $this->load->view('admin/call_Report', $userData);
            } else {

                $this->output->set_status_header('404');
                $this->load->view('Error_page');
            }
        } else {
            $this->output->set_status_header('404');
            $this->load->view('Error_page');
        }
    }

    public function call_back_user_list() {

        $userid = $this->session->userdata('userData');
        if ($this->session->userdata('loggedIn') == true) {

            $userData['logoutUrl'] = $this->facebook->logout_url();
            $userData['oauthURL'] = base_url() . $this->config->item('linkedin_redirect_url') . '?oauth_init=1';
            $userData['authUrl'] = $this->facebook->login_url();
            $userData['loginURL'] = $this->google->loginURL();
            $userid = $this->session->userdata('userData');
            $userData['email'] = "";
            $userData['login'] = 0;
            $userid = $this->session->userdata('userData');
            $role = $userid->role_id;
            $userData['data1'] = $this->user_model->fetch_user($userid->id);
            $userData['data'] = $this->user_model->getUser($userid->id);
            if ($userid->role_id == 1 || $userid->role_id == 2) {
                $userData['userDetails'] = $this->user_model->getcallBackUserList();
                $this->load->view('admin/callback_users', $userData);
            }
        } else {
            $this->output->set_status_header('404');
            $this->load->view('Error_page');
        }
    }

    public function delCallUser() {
        $id = $this->input->post('id');
        $this->db->where('id', $id);
        $this->db->delete('ey_enquiry');
        echo 1;
    }

    public function scheduleInfo() {

        $schedule_id = $this->input->post('schedule_id');
        $visitor_info = $this->input->post('visitor_info');
        $sche_id = $this->input->post('sche_id');
        $start = $this->input->post('start');
        $res = $this->user_model->getScheduleData($schedule_id, $visitor_info, $sche_id, $start);
        print json_encode($res);
    }

    public function statuscheck() {

        $id = $this->input->post('id');
        $stats = $this->input->post('text');
        $data = $this->user_model->check_status($id, $stats);
        echo $data;
    }

    public function getUser() {
        $id = $this->input->post('id');
        $userDetails = $this->user_model->getUserdata2($id);
        $json = json_encode($userDetails);
        echo($json);
    }

    public function credits() {

        $userid = $this->session->userdata('userData');
        if ($this->session->userdata('loggedIn') == true) {

            if ($userid->role_id == 4) {
                //redirect to login page if user not logged in
                $this->session->unset_userdata('login');

                $userData['logoutUrl'] = $this->facebook->logout_url();
                $userData['oauthURL'] = base_url() . $this->config->item('linkedin_redirect_url') . '?oauth_init=1';
                $userData['authUrl'] = $this->facebook->login_url();
                $userData['loginURL'] = $this->google->loginURL();

                $userid = $this->session->userdata('userData');
                if (!isset($userid->id)) {
                    $userData['data1'] = $this->user_model->fetch_user_data($userid['oauth_uid']);
                } else {
                    $userData['data1'] = $this->user_model->fetch_user($userid->id);
                }

                $userData['data'] = $this->user_model->getUser($userid->id);
                $userData['coupon_details'] = $this->user_model->getStatus($userid->id);
                $userData['coupon_bal'] = $this->user_model->get_coupon_balance1($userid->id);
                // print_r($userData['coupon_bal']);exit;
                $this->load->view('admin/credits', $userData);
            }
        } else {

            $this->output->set_status_header('404');
            $this->load->view('Error_page');
        }
    }

    public function checkPhoneNumber() {
        $id = $this->input->post('id');
        $userDetails = $this->user_model->checkPhoneNumber($id);



        if ($userDetails->phone == "") {

            echo 0;
        } else {
            echo 1;
        }
    }

    public function updatePhoneNumber() {
        $phone = $this->input->post('user_reg_phone');
        $userid = $this->session->userdata('userData');
        $userDetails = $this->user_model->updatePhoneNumber($userid->id, $phone);
        echo 1;
    }

    public function counselor_paymnt() {
        $userid = $this->session->userdata('userData');
        if ($this->session->userdata('loggedIn') == true) {

            if ($userid->role_id == 1 || $userid->role_id == 2) {

                $userData['logoutUrl'] = $this->facebook->logout_url();
                $userData['oauthURL'] = base_url() . $this->config->item('linkedin_redirect_url') . '?oauth_init=1';
                $userData['authUrl'] = $this->facebook->login_url();
                $userData['loginURL'] = $this->google->loginURL();
                $userData['data'] = $this->user_model->fetchUser();
                $userid = $this->session->userdata('userData');
                if (!isset($userid->id)) {
                    $userData['data1'] = $this->user_model->fetch_user_data($userid['oauth_uid']);
                } else {
                    $userData['data1'] = $this->user_model->fetch_user($userid->id);
                }

                $userData['couns_payments'] = $this->user_model->counselor_payment();
                //print_r($userData['couns_payments']);exit;
                $this->load->view('admin/payment_counselor', $userData);
            } else {

                $this->output->set_status_header('404');
                $this->load->view('Error_page');
            }
        } else {
            $this->output->set_status_header('404');
            $this->load->view('Error_page');
        }
    }

    public function counspaymntsbydate() {
        $from = $this->input->post('date1');
        $to = $this->input->post('date2');
        $userData['couns_pay'] = $this->user_model->get_counspaymnt_date($from, $to);
        $this->load->view('admin/payment_counselor_bydate', $userData);
    }

    public function CounselorPaymentdetails() {


        $counsid = $this->input->post('id');
        $userData['paymnt_dts'] = $this->user_model->schedulepayment_details($counsid);
        //print_r($userData['paymnt_dts']);

        $this->load->view('admin/popup', $userData);

        // $json = json_encode($userData);
        // echo($json);
    }

    public function quickSchedule() {
        $userid = $this->session->userdata('userData');
        if ($this->session->userdata('loggedIn') == true) {

            if ($userid->role_id == 1 || $userid->role_id == 2) {

                $userData['logoutUrl'] = $this->facebook->logout_url();
                $userData['oauthURL'] = base_url() . $this->config->item('linkedin_redirect_url') . '?oauth_init=1';
                $userData['authUrl'] = $this->facebook->login_url();
                $userData['loginURL'] = $this->google->loginURL();
                $userData['data'] = $this->user_model->fetchUser();
                $userid = $this->session->userdata('userData');
                if (!isset($userid->id)) {
                    $userData['data1'] = $this->user_model->fetch_user_data($userid['oauth_uid']);
                } else {
                    $userData['data1'] = $this->user_model->fetch_user($userid->id);
                }

                $userData['counsDetails'] = $this->user_model->getCounselorList();
                $userData['userDetails'] = $this->user_model->getUserList();
                //print_r($userData['couns_payments']);exit;
                $this->load->view('admin/quickSchedule', $userData);
            } else {

                $this->output->set_status_header('404');
                $this->load->view('Error_page');
            }
        } else {
            $this->output->set_status_header('404');
            $this->load->view('Error_page');
        }
    }

    public function getCounsTimeSlot() {


        $user_id = $this->input->post('id');
        $t = date('Y-m-d');
        //$day = strtolower(date("l", strtotime($t)));
        //$timeSlot = $this->user_model->get_counstime_today($user_id, $day);
        
        $timeSlot = $this->user_model->get_counstime_today($user_id);

        $time = explode("-", $timeSlot[0]->working_hours);
        $data_ren = $this->user_model->getCounsRenumeration($user_id);
        if (!empty($data_ren)) {
            $duration = $data_ren[0]['minutes'];
        } else {
            $duration = '45';  // split by 30 mins
        }
        $startTime = date('H:i:s', strtotime($time[1]));
        ;
        $date = "2018-03-12" . " " . $startTime;
        date_default_timezone_set('Asia/Kolkata');
        $time = date('h:i A', strtotime('-' . $duration . ' minutes', strtotime($date)));
        $timeSlot['duration'] = $duration;  // split by 30 mins

        $timeSlot['timeslot'] = $timeSlot[0]->time_starts . "-" . $time;
        $user = $this->user_model->getUser1($user_id);
        $timeSlot['phone'] = $user->phone;


        print_r(json_encode($timeSlot));
    }

    public function getPhone() {


        $user_id = $this->input->post('id');

        $user = $this->user_model->getUser1($user_id);
        $timeSlot['phone'] = $user->phone;
        print_r(json_encode($timeSlot));
    }

    public function getCous() {

        $user_id = $this->input->post('id');
        if ($user_id != "counselorSchedule" && $user_id != "index") {
            $schedule = $this->user_model->getScheduleDetails($user_id);
            $data_ren = $this->user_model->getCounsRenumeration($user_id);
            $starts = [];
            $ends = [];
            foreach ($schedule as $sche) {
                $starts[] = strtotime($sche->time_starts);
                $ends[] = strtotime($sche->time_ends);
            }

            $minTime = min($starts);
            $maxTime = max($ends);
            $arr = explode('-', $schedule[0]->working_hours);
            $min = date("G:i", $minTime) . ":00";
            $max = date("G:i", $maxTime) . ":00";
        } elseif ($user_id == "index") {
            $userid = $this->session->userdata('userData');
            $user_id = $userid->id;
            $schedule = $this->user_model->getScheduleDetails($user_id);

            $data_ren = $this->user_model->getCounsRenumeration($user_id);
            $starts = [];
            $ends = [];
            foreach ($schedule as $sche) {
                $starts[] = strtotime($sche->time_starts);
                $ends[] = strtotime($sche->time_ends);
            }
            $minTime = min($starts);
            $maxTime = max($ends);
            $arr = explode('-', $schedule[0]->working_hours);
            $min = date("G:i", $minTime) . ":00";
            $max = date("G:i", $maxTime) . ":00";
        } else {
            $arr = "";
            $min = "00:00:00";
            $max = "24:00:00";
            $data_ren[0]['minutes'] = "";
        }


        echo $min . "-" . $max . "-" . $data_ren[0]['minutes'];
    }

    public function availability() {
        $userid = $this->session->userdata('userData');
        if ($this->session->userdata('loggedIn') == true) {

            if ($userid->role_id == 1 || $userid->role_id == 2 || $userid->role_id == 3) {

                $userData['logoutUrl'] = $this->facebook->logout_url();
                $userData['oauthURL'] = base_url() . $this->config->item('linkedin_redirect_url') . '?oauth_init=1';
                $userData['authUrl'] = $this->facebook->login_url();
                $userData['loginURL'] = $this->google->loginURL();
                $userData['data'] = $this->user_model->fetchUser();
                $userid = $this->session->userdata('userData');
                if (!isset($userid->id)) {
                    $userData['data1'] = $this->user_model->fetch_user_data($userid['oauth_uid']);
                } else {
                    $userData['data1'] = $this->user_model->fetch_user($userid->id);
                }
                $id = $this->uri->segment(3);
                $timing = $this->user_model->fetchSchedule($id);
                $userData['timing'] = $timing;
                $time_arr = [];
                foreach ($timing as $time) {
                    $time_arr[] = $time->day;
                }


                $userData['timing_arr'] = $time_arr;
                $userData['data2'] = $this->user_model->fetch_counselor($id);

                $this->load->view('admin/availability', $userData);
            } else {

                $this->output->set_status_header('404');
                $this->load->view('Error_page');
            }
        } else {
            $this->output->set_status_header('404');
            $this->load->view('Error_page');
        }
    }

    public function save_schedule() {
        $values = $this->input->post('dataStore');
        $id = $this->input->post('user_id');
        $res = $this->user_model->insertTimeSlots($values, $id);

        if ($res) {
            $userid = $this->session->userdata('userData');
            if ($userid->role_id == 1 || $userid->role_id == 2) {
                echo 1;
            } else if ($userid->role_id == 3) {
                echo 2;
            }
        } else {
            echo 0;
        }
    }
    
    public function videoCall()
    {
        $userid = $this->session->userdata('userData');
        if ($this->session->userdata('loggedIn') == true) {

            if ($userid->role_id == 1 || $userid->role_id == 2) {

                $userData['logoutUrl'] = $this->facebook->logout_url();
                $userData['oauthURL'] = base_url() . $this->config->item('linkedin_redirect_url') . '?oauth_init=1';
                $userData['authUrl'] = $this->facebook->login_url();
                $userData['loginURL'] = $this->google->loginURL();
                $userData['data'] = $this->user_model->fetchUser();
                $userid = $this->session->userdata('userData');
                if (!isset($userid->id)) {
                    $userData['data1'] = $this->user_model->fetch_user_data($userid['oauth_uid']);
                } else {
                    $userData['data1'] = $this->user_model->fetch_user($userid->id);
                }

                $userData['counselors'] = $this->user_model->getCounselors();
                //print_r($userData['couns_payments']);exit;
                $this->load->view('admin/counselor_Videocall',$userData);
            } else {

                $this->output->set_status_header('404');
                $this->load->view('Error_page');
            }
        } else {
            $this->output->set_status_header('404');
            $this->load->view('Error_page');
        }
    }

    public function videoCall_check()
    {
        $user_id = $this->input->post('id');
        $text = $this->input->post('text');
        $videoCall = $this->user_model->VideoCallCount($user_id, $text);
        echo 1;

    }

}
