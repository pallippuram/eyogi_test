<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class User extends CI_Controller {

    /**
     * Index Page for this controller.
     *
     * Maps to the following URL
     * 		http://example.com/welcome
     * 	- or -
     * 		http://example.com/welcome/index
     * 	- or -
     * Since this controller is set as the default controller in
     * config/routes.php, it's displayed at http://example.com/
     *
     * So any other public methods not prefixed with an underscore will
     * map to /welcome/<method_name>
     * @see https://codeigniter.com/user_guide/general/urls.html
     */
    public function __construct() {
        parent::__construct();
        $this->load->library('google');
        $this->load->model('user_model');
        // Load linkedin config
        $this->load->config('linkedin');
        $this->load->library('facebook');
    }

    public function index() {
        $userData = array();
        if ($this->session->userdata('loggedIn') == true) {
            redirect('admin/index/');
        }
        //Include the linkedin api php libraries
        include_once APPPATH . "libraries/linkedin-oauth-client/http.php";
        include_once APPPATH . "libraries/linkedin-oauth-client/oauth_client.php";


        //Get status and user info from session
        $oauthStatus = $this->session->userdata('oauth_status');
        $sessUserData = $this->session->userdata('userData');

        if (isset($oauthStatus) && $oauthStatus == 'verified') {
            //User info from session
            $userData = $sessUserData;
        } else if ((isset($_REQUEST["oauth_init"]) && $_REQUEST["oauth_init"] == 1) || (isset($_REQUEST['oauth_token']) && isset($_REQUEST['oauth_verifier']))) {
            $client = new oauth_client_class;
            $client->client_id = $this->config->item('linkedin_api_key');
            $client->client_secret = $this->config->item('linkedin_api_secret');
            $client->redirect_uri = base_url() . $this->config->item('linkedin_redirect_url');
            $client->scope = $this->config->item('linkedin_scope');
            $client->debug = false;
            $client->debug_http = true;
            $application_line = __LINE__;

            //If authentication returns success
            if ($success = $client->Initialize()) {
                if (($success = $client->Process())) {
                    if (strlen($client->authorization_error)) {
                        $client->error = $client->authorization_error;
                        $success = false;
                    } elseif (strlen($client->access_token)) {
                        $success = $client->CallAPI('http://api.linkedin.com/v1/people/~:(id,email-address,first-name,last-name,location,picture-url,public-profile-url,formatted-name)', 'GET', array('format' => 'json'), array('FailOnAccessError' => true), $userInfo);
                    }
                }
                $success = $client->Finalize($success);
            }

            if ($client->exit)
                exit;

            if ($success) {
                //Preparing data for database insertion
                $first_name = !empty($userInfo->firstName) ? $userInfo->firstName : '';
                $last_name = !empty($userInfo->lastName) ? $userInfo->lastName : '';
                $picture_url = !empty($userInfo->pictureUrl) ? $userInfo->pictureUrl : '';
                
                // Preparing data for database insertion
            $userData['oauth_provider'] = 'linkedin';
            $userData['oauth_uid'] = $userInfo->id;
            $userData['first_name'] = $first_name;
            $userData['last_name'] = $last_name;
            $userData['email'] = $userInfo->emailAddress;
            $userData['locale'] = $userInfo->location->name;
            $userData['profile_url'] = $userInfo->publicProfileUrl;
            $userData['picture_url'] = $picture_url;
                //Insert or update user data
                $userID = $this->user_model->checkUser($userData);

                //Store status and user profile info into session
//                $this->session->set_userdata('oauth_status', 'verified');
//                $this->session->set_userdata('userData', $userData);
                
                
                $userData['logoutUrl'] = $this->facebook->logout_url();
                $userData['oauthURL'] = base_url() . $this->config->item('linkedin_redirect_url') . '?oauth_init=1';
                $userData['authUrl'] = $this->facebook->login_url();
                $userData['loginURL'] = $this->google->loginURL();
                
                $this->session->set_userdata('userData', $userData);
                
                $page = $this->session->userdata('page');
                if (isset($page)) {
                    $this->load->view('joinus', $userData);
                } else {
                    $this->load->view('user/register', $userData);
                } 
            } else {
                $data['error_msg'] = 'Some problem occurred, please try again later!';
            }
        } elseif (isset($_REQUEST["oauth_problem"]) && $_REQUEST["oauth_problem"] <> "") {
            $data['error_msg'] = $_GET["oauth_problem"];
        } else if ($this->facebook->is_authenticated()) {
            // Get user facebook profile details
            $userProfile = $this->facebook->request('get', '/me?fields=id,first_name,last_name,email,gender,locale,picture');

            // Preparing data for database insertion
            $userData['oauth_provider'] = 'facebook';
            $userData['oauth_uid'] = $userProfile['id'];
            $userData['first_name'] = $userProfile['first_name'];
            $userData['last_name'] = $userProfile['last_name'];
            $userData['email'] = $userProfile['email'];
            $userData['gender'] = $userProfile['gender'];
            $userData['locale'] = $userProfile['locale'];
            $userData['profile_url'] = 'https://www.facebook.com/' . $userProfile['id'];
            $userData['picture_url'] = $userProfile['picture']['data']['url'];

            // Insert or update user data
            $userID = $this->user_model->checkUser($userData);

            $this->session->set_userdata('userData', $userData);
            // Check user data insert or update status
            if (!empty($userID)) {
                $data['userData'] = $userData;
                $this->session->set_userdata('userData', $userData);
            } else {
                $data['userData'] = array();
            }

            $userData['logoutUrl'] = $this->facebook->logout_url();
            $userData['oauthURL'] = base_url() . $this->config->item('linkedin_redirect_url') . '?oauth_init=1';
            $userData['authUrl'] = $this->facebook->login_url();
            $userData['loginURL'] = $this->google->loginURL();
            $page = $this->session->userdata('page');
            $reg = $this->session->userdata('reg');
            if (isset($page)) {
                $this->load->view('joinus', $userData);
            } else {
                $this->load->view('user/register', $userData);
            } 
        } else if (isset($_GET['code'])) {
            //authenticate user
            $this->google->getAuthenticate();

            //get user info from google
            $gpInfo = $this->google->getUserInfo();

            //preparing data for database insertion
            $userData['oauth_provider'] = 'google';
            $userData['oauth_uid'] = $gpInfo['id'];
            $userData['first_name'] = $gpInfo['given_name'];
            $userData['last_name'] = $gpInfo['family_name'];
            $userData['email'] = $gpInfo['email'];
            $userData['gender'] = !empty($gpInfo['gender']) ? $gpInfo['gender'] : '';
            $userData['locale'] = !empty($gpInfo['locale']) ? $gpInfo['locale'] : '';
            $userData['profile_url'] = !empty($gpInfo['link']) ? $gpInfo['link'] : '';
            $userData['picture_url'] = !empty($gpInfo['picture']) ? $gpInfo['picture'] : '';

            //insert or update user data to the database
            $userID = $this->user_model->checkUser($userData);

            $this->session->set_userdata('userData', $userData);
            //store status & user info in session
            // print_r($userData['first_name']);exit;
            $userData['logoutUrl'] = $this->facebook->logout_url();
            $userData['oauthURL'] = base_url() . $this->config->item('linkedin_redirect_url') . '?oauth_init=1';
            $userData['authUrl'] = $this->facebook->login_url();
            $userData['loginURL'] = $this->google->loginURL();
            $page = $this->session->userdata('page');
            $reg = $this->session->userdata('reg');
            if (isset($page)) {
                $this->load->view('joinus', $userData);
            } else {
                $this->load->view('user/register', $userData);
            } 
        } else {

            $this->session->unset_userdata('page');
            $this->session->set_userdata('reg', 'reg');

            $data['logoutUrl'] = $this->facebook->logout_url();
            $data['oauthURL'] = base_url() . $this->config->item('linkedin_redirect_url') . '?oauth_init=1';
            $data['authUrl'] = $this->facebook->login_url();
            $data['userData'] = $userData;
            $data['loginURL'] = $this->google->loginURL();
            $data['email'] = "";
            $data['login'] = 0;
            // Load login & profile view
            $this->load->view('user/register', $data);
        }
    }

    public function login() {
        $email = $this->input->post('email');
        $password = $this->input->post('password');
        $data = $this->user_model->check_login($email, $password);

        if ($data != false) {
            $this->session->set_userdata('loggedIn', true);
            $this->session->set_userdata('userData', $data);


            echo 1;
        } else {
            echo 0;
        }
    }

    public function register() {
        //set validation rules
        $this->form_validation->set_rules('username', 'User Name', 'trim|required|min_length[3]|max_length[30]');
        $this->form_validation->set_rules('email', 'Email ID', 'trim|required|valid_email');
        $this->form_validation->set_rules('password', 'Password', 'trim|required|matches[conpassword]');
        $this->form_validation->set_rules('conpassword', 'Confirm Password', 'trim|required');

        //validate form input
        if ($this->form_validation->run() == FALSE) {
            // fails
            $this->load->view('user/register');
        } else {



            if ($this->input->post('role_id') == 4) {

                //insert the user registration details into database
                $data = array(
                    'oauth_uid' => $this->input->post('oauth_uid'),
                    'username' => $this->input->post('username'),
                    'email' => $this->input->post('email'),
                    'password' => md5($this->input->post('password')),
                    'phone' => $this->input->post('phone'),
                    'age' => $this->input->post('age'),
                    'gender' => $this->input->post('gender'),
                    'qualification' => $this->input->post('qualification'),
                    'employed' => $this->input->post('employed'),
                    'marital_status' => $this->input->post('marital_status'),
                    'kids' => $this->input->post('kids'),
                    'role_id' => $this->input->post('role_id'),
                    'created_at' => date("Y-m-d H:i:s")
                );

                // insert form data into database
                if ($this->user_model->insertUser($data)) {
                    $data = $this->user_model->check_login($this->input->post('email'), $this->input->post('password'));
                    $this->session->set_userdata('userData', $data);
                    $this->session->set_userdata('loggedIn', true);
                    redirect('user/profile');
                } else {
                    // error
                    $this->session->set_flashdata('msg', '<div class="alert alert-danger text-center">Oops! Error.  Please try again later!!!</div>');
                    redirect('user/register');
                }
            } else {


                if (!empty($_FILES['image']['name'])) {

                    $config['upload_path'] = 'uploads/images/';
                    $config['allowed_types'] = 'jpg|jpeg|png|gif';
                    $config['file_name'] = $_FILES['image']['name'];

                    //Load upload library and initialize configuration
                    $this->load->library('upload', $config);
                    $this->upload->initialize($config);

                    if ($this->upload->do_upload('image')) {
                        $uploadData = $this->upload->data();
                        $picture = $uploadData['file_name'];
                    } else {
                        $picture = '';
                    }
                } else {
                    $picture = '';
                }

                $data = array(
                    'username' => $this->input->post('username'),
                    'email' => $this->input->post('email'),
                    'password' => md5($this->input->post('password')),
                    'photo' => $picture,
                    'created_at' => date("Y-m-d H:i:s"),
                    'role_id' => $this->input->post('role_id'),
                );
                $data1 = array(
                    'domain' => $this->input->post('domain'),
                    'experience' => $this->input->post('experience'),
                    'languages' => json_encode($this->input->post('languages')),
                    'certifications' => $this->input->post('certifications'),
                    'accrediations' => $this->input->post('accrediations'),
                );


                // insert form data into database
                if ($this->user_model->insertCounselor($data, $data1)) {
                    $data = $this->user_model->check_login($this->input->post('email'), md5($this->input->post('password')));
                