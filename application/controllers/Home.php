<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class Home extends CI_Controller {

    /**
     * Index Page for this controller.
     *
     * Maps to the following URL
     * 		http://example.com/welcome
     * 	- or -
     * 		http://example.com/welcome/index
     * 	- or -
     * Since this controller is set as the default controller in
     * config/routes.php, it's displayed at http://example.com/
     *
     * So any other public methods not prefixed with an underscore will
     * map to /welcome/<method_name>
     * @see https://codeigniter.com/user_guide/general/urls.html
     */
    public function __construct() {
        parent::__construct();
        $this->load->library('google');
        $this->load->model('user_model');
        $this->load->model('Calendar_model');
        $this->load->config('linkedin');
        $this->load->library('facebook');
    }

    public function index() {
        ?>

        <script>
            var currentLocation = window.location;
            if (currentLocation == 'http://beta.eyogi.in/') {
        //    alert("eyogi");
                window.location.href = 'http://dev.eyogi.in/';
            }
            if (currentLocation == 'https://eyogi.in/') {
        //    alert("eyogi");
                window.location.href = 'http://dev.eyogi.in/';
            }
        </script>

        <?php

//        if ($this->uri->segment(2)==='old_url') {
//            redirect(base_url() . $this->lang->lang() .'/new-url', 'location', 301);
//          }

        $data['oauthURL'] = base_url() . $this->config->item('linkedin_redirect_url') . '?oauth_init=1';
        $data['authUrl'] = $this->facebook->login_url();

        $data['loginURL'] = $this->google->loginURL();
        $userdata = $this->session->userdata('userData');
        $filterOpts1 = $this->session->unset_userdata('filterOpts1');
        if ($this->session->userdata('loggedIn') == true) {
            $userid = $this->session->userdata('userData');

            if (!isset($userid->id)) {
                $data['data1'] = $this->user_model->fetch_user_data($userid['oauth_uid']);
            } else {
                $data['data1'] = $this->user_model->fetch_user($userid->id);
            }
        }
        $data['counselling_types'] = $this->user_model->getCounsellingTypes();
        $this->load->view('index', $data);
    }

    public function howitworks() {
        $filterOpts1 = $this->session->unset_userdata('filterOpts1');
        $userData['logoutUrl'] = $this->facebook->logout_url();
        $userData['oauthURL'] = base_url() . $this->config->item('linkedin_redirect_url') . '?oauth_init=1';
        $userData['authUrl'] = $this->facebook->login_url();
        if ($this->session->userdata('loggedIn') == true) {
            $userid = $this->session->userdata('userData');
            $userData['data1'] = $this->user_model->fetch_user($userid->id);
        }
        $userData['loginURL'] = $this->google->loginURL();
        $this->load->view('howitworks', $userData);
    }
    
    public function aboutus(){
        
        $userData['logoutUrl'] = $this->facebook->logout_url();
        $userData['oauthURL'] = base_url() . $this->config->item('linkedin_redirect_url') . '?oauth_init=1';
        $userData['authUrl'] = $this->facebook->login_url();
        $userData['loginURL'] = $this->google->loginURL();
        if ($this->session->userdata('loggedIn') == true) {
            $userid = $this->session->userdata('userData');
            $userData['data1'] = $this->user_model->fetch_user($userid->id);
        }
        $this->load->view('aboutus', $userData);
    }

    public function schedule() {
        date_default_timezone_set('Asia/Kolkata');
        $this->session->set_userdata('page', 'reg');
        $userData['logoutUrl'] = $this->facebook->logout_url();
        $userData['oauthURL'] = base_url() . $this->config->item('linkedin_redirect_url') . '?oauth_init=1';
        $userData['authUrl'] = $this->facebook->login_url();
        $userData['loginURL'] = $this->google->loginURL();
        //  $userData['data'] = $this->user_model->fetchUser();
        //  $this->load->library('pagination');
        //$config['base_url'] = 'Rfetch1_controller/index/';
        $limit = 4;
       
        @$offset = $this->uri->segment(3);
        if (!is_null(@$offset)) {
            $offset = $this->uri->segment(3);
        }
        
        // if($limit>4){
        //    $d=$offset/$limit;
        //    $k=$d-1;
        //    $limit=$offset-($limit*$k);
        //    }

        $this->load->library('pagination');

        $filterOpts = $this->session->userdata('filterOpts');
        $filterOpts1 = $this->session->userdata('filterOpts1');

        $userData['types'] = $this->user_model->getCounselingTypes();

        $userData['count'] = $this->user_model->fetchcount();
        //  echo count($userData['count']);exit;
        $config['uri_segment'] = 3;
        $config['base_url'] = site_url('home/schedule');
        $config['total_rows'] = count($userData['count']);
        $config['per_page'] = $limit;
        $config['num_links'] = 5;
        $config['first_link'] = 'First';
        $config['last_link'] = 'Last';
        $config['full_tag_open'] = "<ul class='pagination pagination-job page-nav'>";
        $config['full_tag_close'] = '</ul>';
        $config['num_tag_open'] = '<li class="page-numbers">';
        $config['num_tag_close'] = '</li>';
        $config['cur_tag_open'] = '<li class="active page-numbers" data-paged="1"><a href="#">';
        $config['cur_tag_close'] = '</a></li>';
        $config['prev_tag_open'] = '<li>';
        $config['prev_tag_close'] = '</li>';
        $config['first_tag_open'] = '<li>';
        $config['first_tag_close'] = '</li>';
        $config['last_tag_open'] = '<li>';
        $config['last_tag_close'] = '</li>';
        $config['prev_link'] = '<span aria-label="Previous"><span aria-hidden="true"><i class="fa fa-angle-left"></i></span></span>';
        $config['prev_tag_open'] = '<li class="disabled page-numbers">';
        $config['prev_tag_close'] = '</li>';


        $config['next_link'] = '<span aria-hidden="true"><i class="fa fa-angle-right"></i></span>';
        $config['next_tag_open'] = '<li class="page-numbers" data-paged="2">';
        $config['next_tag_close'] = '</li>';
        $this->pagination->initialize($config);

        if (empty($filterOpts)) {
            $filterOpts = "";
        }
        if (empty($filterOpts1)) {
            $filterOpts1 = "";
        }

        $days = array(0=>'monday',1=>'tuesday',2=>'wednesday',3=>'thursday',4=>'friday',5=>'saturday',6=>'sunday');
        $startday = 0;
       
        for($k=0;$k<count($days); $k++){
           if(strtolower(date('l')) == $days[$k])
           $startday = $k;   
        }
        $new_days = array_merge(array_slice($days,$startday),array_slice($days,0,$startday));
        
        for($k=0;$k<count($new_days);$k++){
          $counsel[] = $this->user_model->get_counselor1($filterOpts, $filterOpts1, @$limit, @$offset, $new_days[$k]);
        }
        $result1 = [];
        foreach ($counsel as $value) {
            $result1 = array_merge($result1, $value);
        }
        //$today_counsel = array_map("unserialize", array_unique((array_map("serialize", $result1))));
        //$today_counsel = json_decode( json_encode($today_counsel), true);
        
        $time = date('H:i');
        $day = strtolower(date("l", strtotime(date('Y-m-d'))));
        
        $all = $this->user_model->AllCounselor($day);
        $rs = [];
        foreach ($all as $couns){
            if($time >= date("H:i", strtotime($couns->time_starts)) && $time <= date("H:i", strtotime($couns->time_ends)))
            {
                $rs[] = $this->user_model->todayCounsel($couns->user_id);
            }  
            
        }
        
        $result2 = [];
        foreach ($rs as $value) {
            $result2 = array_merge($result2, $value);
        }
        
        $merge = array_merge($result2, $result1);
        
        $userData['sort_data'] = array_map("unserialize", array_unique((array_map("serialize", $merge))));
        $userData['sort_data'] = json_decode( json_encode($userData['sort_data']), true);
        
        if ($offset != "") {
            if($offset>4 && $offset<=8){
                 $d=-4;
              //   $where .= ' limit ' . $offset . ',' . $d . '';
              $userData['sort_data']=array_slice($userData['sort_data'],$offset,$limit);    
            }
            else if ($offset>8) {
                $d=$offset-8;
                // $where .= ' limit ' . $offset . ',' . $d . '';
                $userData['sort_data']=array_slice($userData['sort_data'],$offset,$limit);  
            }
            else{ 
              //  $where .= ' limit ' . $offset . ',' . $offset . '';
                $userData['sort_data']=array_slice($userData['sort_data'],$limit,$limit);    
            }
        } else { 
            $userData['sort_data']=array_slice($userData['sort_data'],0,$limit);
        }
        // $userData['sort_data'] = (object) $userData['sort_data'];
        //echo '<pre>';
        //print_r($userData['sort_data']);exit;

        if ($this->session->userdata('loggedIn') == true) {
            $userid = $this->session->userdata('userData');

            $userData['data1'] = $this->user_model->fetch_user($userid->id);
            
        }
        $this->session->unset_userdata('res_value');
        $this->load->view('schedule', $userData);
    }

    public function purchasecode() {
        $filterOpts1 = $this->session->unset_userdata('filterOpts1');
        $userData['logoutUrl'] = $this->facebook->logout_url();
        $userData['oauthURL'] = base_url() . $this->config->item('linkedin_redirect_url') . '?oauth_init=1';
        $userData['authUrl'] = $this->facebook->login_url();
        $userData['loginURL'] = $this->google->loginURL();
        if ($this->session->userdata('loggedIn') == true) {
            $userid = $this->session->userdata('userData');
            $userData['data1'] = $this->user_model->fetch_user($userid->id);
        }

        $userData['codes'] = $this->user_model->getCouponCodes();
        $this->load->view('purchasecode', $userData);
    }

    public function keepintouch() {
        $filterOpts1 = $this->session->unset_userdata('filterOpts1');
        $userData['logoutUrl'] = $this->facebook->logout_url();
        $userData['oauthURL'] = base_url() . $this->config->item('linkedin_redirect_url') . '?oauth_init=1';
        $userData['authUrl'] = $this->facebook->login_url();
        $userData['loginURL'] = $this->google->loginURL();
        if ($this->session->userdata('loggedIn') == true) {
            $userid = $this->session->userdata('userData');
            $userData['data1'] = $this->user_model->fetch_user($userid->id);
        }
        $this->load->view('contactus', $userData);
    }

    public function register() {
        $filterOpts1 = $this->session->unset_userdata('filterOpts1');
        //set validation rules
        $this->form_validation->set_rules('username', 'User Name', 'trim|required|min_length[3]|max_length[30]');
        $this->form_validation->set_rules('email', 'Email ID', 'trim|required|valid_email');
        $this->form_validation->set_rules('password', 'Password', 'trim|required|matches[conpassword]');
        $this->form_validation->set_rules('conpassword', 'Confirm Password', 'trim|required');

        //validate form input
        if ($this->form_validation->run() == FALSE) {
            // fails
            $this->load->view('user/register');
        } else {
            //insert the user registration details into database
            $data = array(
                'username' => $this->input->post('username'),
                'email' => $this->input->post('email'),
                'password' => md5($this->input->post('password')),
                'phone' => $this->input->post('phone'),
                'age' => $this->input->post('age'),
                'gender' => $this->input->post('gender'),
                'qualification' => $this->input->post('qualification'),
                'employed' => $this->input->post('employed'),
                'marital_status' => $this->input->post('marital_status'),
                'kids' => $this->input->post('kids'),
            );

            // insert form data into database
            if ($this->user_model->insertUser($data)) {
                redirect('user/userhome');
            } else {
                // error
                $this->session->set_flashdata('msg', '<div class="alert alert-danger text-center">Oops! Error.  Please try again later!!!</div>');
                redirect('user/register');
            }
        }
    }

    public function lostpass() {
        $data['logoutUrl'] = $this->facebook->logout_url();
        $data['oauthURL'] = base_url() . $this->config->item('linkedin_redirect_url') . '?oauth_init=1';
        $data['authUrl'] = $this->facebook->login_url();

        $data['loginURL'] = $this->google->loginURL();
        $data['email'] = "";
        $data['login'] = 0;
        if ($this->session->userdata('loggedIn') == true) {
            $userid = $this->session->userdata('userData');
            $data['data1'] = $this->user_model->fetch_user($userid->id);
        }
        $this->load->view('user/lostpassword', $data);
    }

    public function reset_password() {
        $data['logoutUrl'] = $this->facebook->logout_url();
        $data['oauthURL'] = base_url() . $this->config->item('linkedin_redirect_url') . '?oauth_init=1';
        $data['authUrl'] = $this->facebook->login_url();

        $data['loginURL'] = $this->google->loginURL();
        $data['email'] = "";
        $data['login'] = 0;
        $token = $this->uri->segment(4);

        $token_new = $this->user_model->checkToken($token);
        $date = new DateTime($token_new->created_at);
        $now = new DateTime();
        $date_diff = $date->diff($now)->format("%d");
        if ($date_diff > 0) {
            $data['token_expired'] = "true";
        } else {
            $data['token_expired'] = "false";
        }
        $data['token'] = $token;
        $this->load->view('user/resetpassword', $data);
    }

    public function updatepassword() {
        $token = $this->input->post('token');
        $cleanToken = $this->security->xss_clean($token);

        $user_info = $this->user_model->checkToken($cleanToken); //either false or array();               

        if (!$user_info) {
            $this->session->set_flashdata('flash_message', 'Token is invalid or expired');
            redirect(site_url() . 'home/lostpass');
        }
        //$current_password = $this->input->post('current_password');

        $user_info = $this->user_model->checkPassword1($cleanToken);
        if (!empty($user_info)) {
            $post = $this->input->post('password');
            $conpassword = $this->input->post('conpassword');
            $cleanPost = $this->security->xss_clean($post);

            $password = md5($cleanPost);
            $id = $user_info->id;
            if ($post == $conpassword) {
                if (!$this->user_model->updatePassword($id, $password)) {
                    $this->session->set_flashdata('flash_message', 'There was a problem updating your password');
                } else {
                    $this->session->set_flashdata('flash_message', 'Your password has been updated. You may now login');
                }
                redirect(site_url() . '/');
            } else {
                $this->session->set_flashdata('flash_message', 'Password mismatch');
                redirect(site_url() . 'home/reset_password/token/' . $token);
            }
//        } else {
//            $this->session->set_flashdata('flash_message', 'Password not found in our database');
//            redirect(site_url() . 'home/reset_password/token/' . $token);
        }
    }

    public function logout() {
        //delete login status & user info from session
        $this->session->unset_userdata('loggedIn');
        $this->session->unset_userdata('userData');
        $this->session->sess_destroy();

        //redirect to login page
        redirect('/');
    }

    public function profile() {
        $filterOpts1 = $this->session->unset_userdata('filterOpts1');
        //redirect to login page if user not logged in
        if (!$this->session->userdata('loggedIn')) {
            redirect('/user/');
        }

        //get user info from session
        $data['userData'] = $this->session->userdata('userData');

        //load user profile view
        $this->load->view('user_authentication/profile', $data);
    }

    public function getCounselorDetails() {
        $id = $this->input->post('id');

        $res['timing'] = $this->user_model->getUserTiming($id);
        if ($this->session->userdata('loggedIn') == true) {
            $userid = $this->session->userdata('userData');
            $user = $this->user_model->fetch_user($userid->id);
            $res['user'] = $user[0]->id;
            $res['username'] = $user[0]->username;
        }

        print json_encode($res);
    }

    // get days info from counseller_workingdays
    public function daydate() {

        $dy = $this->input->POST('id');
        $data = $this->user_model->fetch_days();

        foreach ($data as $value) {

            $arr = json_decode($value->days, true);
            $array[] = "";


            if (in_array($dy, (array) $arr)) {
                $sel_counsel[] = $this->user_model->select_counselor($value->counselor_id);
            } else {
                $sel_counsel[] = "";
            }
        }
        $json = json_encode($sel_counsel);
        echo($json);
    }

    // get counselors on present day and time
    public function now() {
        
        $dy = $this->input->POST('text');
        $sel_con[] = "";
        if($dy == "Now"){
            $t = date('Y-m-d');
            $time = date('H:i');
            $day = strtolower(date("l", strtotime(date('Y-m-d'))));
        
            //$day = strtolower(date("l", strtotime($t)));
            //$today = lcfirst(date('l', strtotime($t)));
        
            $present_counsel = $this->user_model->get_time($dy, $day);
            foreach($present_counsel as $row)
            {
                if($time >= date("H:i", strtotime($row->time_starts)) && $time <= date("H:i", strtotime($row->time_ends)))
                {
                    $data[] = $row;
                }  
            }
            if (!empty($data)) {
                foreach ($data as $key => $value) {

                    $scheduleDetails = $this->user_model->checkScheduleDetails($value->id, $t);
                    $rescheduleDetails = $this->user_model->checkReScheduleDetails($value->id, $t);

                    if ($scheduleDetails != 1 && $rescheduleDetails != 1) {
                        $workingdays = $this->user_model->checkWorkingId($value->user_id);
                        $days = json_decode($workingdays->days);
                        if (in_array($day, $days)) {
                            $sel_con[] = $this->user_model->get_counsel($value->user_id, $dy);
                        }
                    }
                }
            } 
        }
        else if($dy == "All"){
            $sel_con[] = $this->user_model->get_counsel($value = NULL, $dy);
        }
        $json = json_encode($sel_con);
        echo($json);
    }

    public function ratelist() {

        $cid = $this->input->post('cid');
        $ccid = $this->input->post('ccid');
        $userData['ratings'] = $this->user_model->getratingsbyid($cid, $ccid);
        // print_r($userData);exit;
        //print_r($userData['call']);exit;
        $this->load->view('admin/callratings_model', $userData);
    }

    public function employee_counselor() {
        $emp_id = $this->session->userdata('counselor_id');
        $userData['logoutUrl'] = $this->facebook->logout_url();
        $userData['oauthURL'] = base_url() . $this->config->item('linkedin_redirect_url') . '?oauth_init=1';
        $userData['authUrl'] = $this->facebook->login_url();



        $userData['loginURL'] = $this->google->loginURL();
        $userData['data'] = $this->user_model->fetchUser();
        if ($this->session->userdata('loggedIn') == true) {
            $userid = $this->session->userdata('userData');
            //print_r($userid);
            $userData['data1'] = $this->user_model->fetch_user($userid->id);
        }

        $userData['data2'] = $this->user_model->get_employee($emp_id);
        $userData['data3'] = $this->user_model->get_counstime($emp_id, strtolower(date('l')));

        $this->load->view('employee', $userData);
    }

    public function checkCodeValid() {
        $code = $this->input->post('code');
        $result = $this->user_model->checkCodeValid($code);

        if (!empty($result)) {
            echo 1;
        } else {
            echo 0;
        }
    }

    public function balcheck() {
        $use_id = $this->input->post('uid');
        $data = $this->user_model->getbalanceamt($use_id);

        if (!empty($data)) {
            $json = json_encode($data);
            echo($json);
        } else {
            echo 1;
        }
    }

    public function Userbalcheck() {
        $balnce_id = $this->input->post('bal_id');
        $bal_data = $this->user_model->getUserBalance($balnce_id);

        if (!empty($bal_data)) {
            $json = json_encode($bal_data);
            echo($json);
        } else {
            echo 1;
        }
    }

    public function get_scheduled_counselor() {
        $code = $this->input->post('purchase_code');

        $result = $this->user_model->getScheduledCounselor($code);

        if (!empty($result)) {
            foreach ($result as $key => $res) {

                if ($res->is_modified == 1) {
                    $result1 = $this->user_model->getReScheduledCounselor($code);
                    unset($result[$key]);
                }
            }
        }

        if (!empty($result1)) {
            $arr = array_merge($result1, $result);
        } else {
            $arr = $result;
        }

        $json = json_encode($arr);
        echo($json);
    }



    public function userReschedule() {

        $id = $this->input->post('id');
        $user_id = $this->input->post('user_id');
        $description = $this->input->post('description');
        $start = $this->input->post('start');
        $end = $this->input->post('end');
        $schedule_id = $this->input->post('schedule_id');

        $res = $this->user_model->getCounselorSchedule($id);

        if (!empty($res)) {

            if($res->video_schedule == 1) {

                $data = array(
                    'counselor_id' => $user_id,
                    'visitor_info' => $res->visitor_info,
                    'current_schedule_id' => $id,
                    'date' => $start,
                    'time_starts' => $start,
                    'time_ends' => $end,
                    'schedule_id' => $schedule_id,
                    'reason_to_reschedule' => $description,
                    'video_schedule' => 1,
                    'status' => 'ACTIVE',
                    'created_at' => date("Y-m-d H:i:s")
                );
                $result = $this->user_model->updateCounselorSchedule($id, $data);

                if ($result) {
                    
                    if (is_numeric($res->visitor_info) && strlen((string) $res->visitor_info) < 6) {
                        
                        $user_phone = $this->user_model->get_phoneno($res->visitor_info);
                        $user_phone = json_decode(json_encode($user_phone), True);
                    }
                    else{
                        $user_phone = $this->user_model->getCouponPhone($res->visitor_info,$id);
                        $user_phone = json_decode(json_encode($user_phone), True);
                    }
                
                    $visitorinf = $res->visitor_info;
                    $counsilid = $user_id;
                    $schedule_id = $schedule_id;
                    $visitordet = $this->Calendar_model->getvisitordetail($visitorinf);
                    $counsildet = $this->Calendar_model->getcounsildetail($counsilid);
                    $visitordet = json_decode(json_encode($visitordet), True);
                    $counsildet = json_decode(json_encode($counsildet), True);

                    //$scedulertimestarts = date('h:i A', strtotime($start));
                    $scedulertimestarts = date("h:i A", strtotime($this->input->post('start')));
                    //$scedulertimeends = date('h:i A', strtotime($end));
                    $scedulertimeends = date("h:i A", strtotime($this->input->post('end')));
                    $start=date("Y-m-d", strtotime($this->input->post('start')));

                    $counsilorphn = $counsildet[0]['phone'];
                    $counsilormail = $counsildet[0]['email'];
                    $counselor_name = $counsildet[0]['username'];
                    
                    if (is_numeric(trim($user_phone[0]['phone']))) {
                        
                        /* SMS to user */
                        $phone = $user_phone[0]['phone'];
                        $ph = "$phone";
                    
                        $post_data = array(
                        // 'From' doesn't matter; For transactional, this will be replaced with your SenderId;
                        // For promotional, this will be ignored by the SMS gateway
                            'From' => '08039534067',
                            'To' => $ph,
                            'Body' => 'Dear user, your appointment has been Rescheduled with eYogi on ' . $start . ' starts from ' . $scedulertimestarts . ' to ' . $scedulertimeends . ' . Regards eYogi Team',
                            //'Body' => 'OTP for verifying your mobile with eYogi is ' . $pin . '', //Incase you are wondering who Dr. Rajasekhar is http://en.wikipedia.org/wiki/Dr._Rajasekhar_(actor)
                        );

                        $exotel_sid = "eyogi"; // Your Exotel SID - Get it from here: http://my.exotel.in/settings/site#api-settings
                        $exotel_token = "fb83c4d9b235b90d0e3b8b1194c942de489b003f"; // Your exotel token - Get it from here: http://my.exotel.in/settings/site#api-settings

                        $url = "https://" . $exotel_sid . ":" . $exotel_token . "@twilix.exotel.in/v1/Accounts/" . $exotel_sid . "/Sms/send";

                        $ch = curl_init();
                        curl_setopt($ch, CURLOPT_VERBOSE, 1);
                        curl_setopt($ch, CURLOPT_URL, $url);
                        curl_setopt($ch, CURLOPT_POST, 1);
                        curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
                        curl_setopt($ch, CURLOPT_FAILONERROR, 0);
                        curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, 0);
                        curl_setopt($ch, CURLOPT_POSTFIELDS, http_build_query($post_data));
                        /*   $data = array(
                                'phone' => $phone,
                                'code' => $pin,
                                'is_expired' => 0,
                                'created_at' => date("Y-m-d H:i:s"),
                            );

                            $this->user_model->insertOTPDetails($data); */
                        $http_result = curl_exec($ch);
                        $error = curl_error($ch);
                        $http_code = curl_getinfo($ch, CURLINFO_HTTP_CODE);

                        curl_close($ch);
                        
                        /* SMS to counselor */
                        
                        $phn = $counsildet[0]['phone'];
                        
                        $phone = $phn;
                        $ph = "$phone";
                        // $pin = $this->generatePIN(4);
                        $post_data = array(
                            // 'From' doesn't matter; For transactional, this will be replaced with your SenderId;
                            // For promotional, this will be ignored by the SMS gateway
                                'From' => '08039534067',
                                'To' => $ph,
                                'Body' => 'Dear user, your appointment has been Rescheduled with eYogi on ' . $start . ' starts from ' . $scedulertimestarts . ' to ' . $scedulertimeends . ' . Regards eYogi Team',
                            //'Body' => 'OTP for verifying your mobile with eYogi is ' . $pin . '', //Incase you are wondering who Dr. Rajasekhar is http://en.wikipedia.org/wiki/Dr._Rajasekhar_(actor)
                        );

                        $exotel_sid = "eyogi"; // Your Exotel SID - Get it from here: http://my.exotel.in/settings/site#api-settings
                        $exotel_token = "fb83c4d9b235b90d0e3b8b1194c942de489b003f"; // Your exotel token - Get it from here: http://my.exotel.in/settings/site#api-settings

                        $url = "https://" . $exotel_sid . ":" . $exotel_token . "@twilix.exotel.in/v1/Accounts/" . $exotel_sid . "/Sms/send";

                        $ch = curl_init();
                        curl_setopt($ch, CURLOPT_VERBOSE, 1);
                        curl_setopt($ch, CURLOPT_URL, $url);
                        curl_setopt($ch, CURLOPT_POST, 1);
                        curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
                        curl_setopt($ch, CURLOPT_FAILONERROR, 0);
                        curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, 0);
                        curl_setopt($ch, CURLOPT_POSTFIELDS, http_build_query($post_data));
                            /*   $data = array(
                                    'phone' => $phone,
                                    'code' => $pin,
                                    'is_expired' => 0,
                                    'created_at' => date("Y-m-d H:i:s"),
                                );

                                $this->user_model->insertOTPDetails($data); */
                        $http_result = curl_exec($ch);
                        $error = curl_error($ch);
                        $http_code = curl_getinfo($ch, CURLINFO_HTTP_CODE);

                        curl_close($ch);
                        
                        // Mail to eyogi_counselors
                    
                        $this->load->library('email');
                        //$counsilormail
                        $to = "keerthi@sectorqube.com";
                        $subject = "Rescheduled Details";

                        $message = '<html><body>';
                        
                        $message .= '<table rules="all" style="border-color: #666;" cellpadding="10">';
                        $message .= "<tr style='background: #eee;'><td><strong>Counsellor:</strong> </td><td>" . $counselor_name . "</td></tr>";
                        $message .= "<tr style='background: #eee;'><td><strong>Date:</strong> </td><td>" . $start . "</td></tr>";
                        $message .= "<tr style='background: #eee;'><td><strong>From:</strong> </td><td>" . $scedulertimestarts . "</td></tr>";
                        $message .= "<tr style='background: #eee;'><td><strong>Till:</strong> </td><td>" . $scedulertimeends . "</td></tr>";

                        $message .= "</table>";
                        $message .= "</body></html>";



                        $headers = "From: info@eyogi.in" . "\r\n" . "Reply-To: info@eyogi.in" . "\r\n" . "Return-Path: info@eyogi.in" . "\r\n" .
                            'X-Mailer: PHP/' . phpversion() . "\r\n" .
                            'Content-Type: text/html; charset=ISO-8859-1' . "\r\n" .
                            'MIME-Version: 1.0' . "\r\n\r\n";


                        // $headers = "From: ".$_POST['emailaddress']."" . "\r\n" .
                        //  "CC: bimalsasidharan@gmail.com";
                        mail($to, $subject, $message, $headers);
                        
                        /* mail to eyogi user video schedule details*/
                        
                        $to = 'keerthi@sectorqube.com';
                        $subject = "eYogi - Video Reschedule details";

                        $message = '<html><body>';
                        $message .= '<h2>Video Reschedule details of user is as follows :</h2></br>';
                        $message .= '<table rules="all" style="border-color: #666;" cellpadding="10">';
                        $message .= "<tr style='background: #eee;'><td><strong>Counsellor:</strong> </td><td>" . $counselor_name . "</td></tr>";
                        //$message .= "<tr style='background: #eee;'><td><strong>Visitor-info:</strong> </td><td>" . $visitordet[0]['visiter_info'] . "</td></tr>";
                        $message .= "<tr style='background: #eee;'><td><strong>User-Contact No:</strong> </td><td>" . $user_phone[0]['phone'] . "</td></tr>";
                        $message .= "<tr style='background: #eee;'><td><strong>Date:</strong> </td><td>" . $start . "</td></tr>";
                        $message .= "<tr style='background: #eee;'><td><strong>From:</strong> </td><td>" . $scedulertimestarts . "</td></tr>";
                        $message .= "<tr style='background: #eee;'><td><strong>Till:</strong> </td><td>" . $scedulertimeends . "</td></tr>";

                        $message .= "</table>";
                        $message .= "<p>If you face any issues, please contact us at info@eyogi.in</p>";
                        $message .= "</body></html>";

                        $headers = "From: info@eyogi.in" . "\r\n" . "Reply-To: info@eyogi.in" . "\r\n" . "Return-Path: info@eyogi.in" . "\r\n" .
                            'X-Mailer: PHP/' . phpversion() . "\r\n" .
                            'Bcc: info@eyogi.in' . "\r\n" .
                            'Content-Type: text/html; charset=ISO-8859-1' . "\r\n" .
                            'MIME-Version: 1.0' . "\r\n\r\n";


                        // $headers = "From: ".$_POST['emailaddress']."" . "\r\n" .
                        //  "CC: bimalsasidharan@gmail.com";
                        mail($to, $subject, $message, $headers);
                        
                        
                    }
                    
                    echo 1;
                } else {
                
                    echo 0;
                }

            }
            else {

                $data = array(
                    'counselor_id' => $user_id,
                    'visitor_info' => $res->visitor_info,
                    'current_schedule_id' => $id,
                    'date' => $start,
                    'time_starts' => $start,
                    'time_ends' => $end,
                    'schedule_id' => $schedule_id,
                    'reason_to_reschedule' => $description,
                    'status' => 'ACTIVE',
                    'created_at' => date("Y-m-d H:i:s")
                );
                $result = $this->user_model->updateCounselorSchedule($id, $data);

                if ($result) {
                    
                    if (is_numeric($res->visitor_info) && strlen((string) $res->visitor_info) < 6) {
                        
                        $user_phone = $this->user_model->get_phoneno($res->visitor_info);
                        $user_phone = json_decode(json_encode($user_phone), True);
                    }
                    else{
                        $user_phone = $this->user_model->getCouponPhone($res->visitor_info,$id);
                        $user_phone = json_decode(json_encode($user_phone), True);
                    }
                
                    $visitorinf = $res->visitor_info;
                    $counsilid = $user_id;
                    $schedule_id = $schedule_id;
                    $visitordet = $this->Calendar_model->getvisitordetail($visitorinf);
                    $counsildet = $this->Calendar_model->getcounsildetail($counsilid);
                    $visitordet = json_decode(json_encode($visitordet), True);
                    $counsildet = json_decode(json_encode($counsildet), True);

                    $scedulertimestarts = date('h:i A', strtotime($start));
                    $scedulertimeends = date('h:i A', strtotime($end));
                    $start=date("Y-m-d", strtotime($this->input->post('start')));

                    $counsilorphn = $counsildet[0]['phone'];
                    $counsilormail = $counsildet[0]['email'];
                    $counselor_name = $counsildet[0]['username'];

                    if (is_numeric(trim($user_phone[0]['phone']))) {
                        
                        /* SMS to user */
                        $phone = $user_phone[0]['phone'];
                        $ph = "$phone";
                    
                        $post_data = array(
                        // 'From' doesn't matter; For transactional, this will be replaced with your SenderId;
                        // For promotional, this will be ignored by the SMS gateway
                            'From' => '08039534067',
                            'To' => $ph,
                            'Body' => 'Dear user, your appointment has been Rescheduled with eYogi on ' . $start . ' starts from ' . $scedulertimestarts . ' to ' . $scedulertimeends . ' . Regards eYogi Team',
                            //'Body' => 'OTP for verifying your mobile with eYogi is ' . $pin . '', //Incase you are wondering who Dr. Rajasekhar is http://en.wikipedia.org/wiki/Dr._Rajasekhar_(actor)
                        );

                        $exotel_sid = "eyogi"; // Your Exotel SID - Get it from here: http://my.exotel.in/settings/site#api-settings
                        $exotel_token = "fb83c4d9b235b90d0e3b8b1194c942de489b003f"; // Your exotel token - Get it from here: http://my.exotel.in/settings/site#api-settings

                        $url = "https://" . $exotel_sid . ":" . $exotel_token . "@twilix.exotel.in/v1/Accounts/" . $exotel_sid . "/Sms/send";

                        $ch = curl_init();
                        curl_setopt($ch, CURLOPT_VERBOSE, 1);
                        curl_setopt($ch, CURLOPT_URL, $url);
                        curl_setopt($ch, CURLOPT_POST, 1);
                        curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
                        curl_setopt($ch, CURLOPT_FAILONERROR, 0);
                        curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, 0);
                        curl_setopt($ch, CURLOPT_POSTFIELDS, http_build_query($post_data));
                        /*   $data = array(
                                'phone' => $phone,
                                'code' => $pin,
                                'is_expired' => 0,
                                'created_at' => date("Y-m-d H:i:s"),
                            );

                            $this->user_model->insertOTPDetails($data); */
                        $http_result = curl_exec($ch);
                        $error = curl_error($ch);
                        $http_code = curl_getinfo($ch, CURLINFO_HTTP_CODE);

                        curl_close($ch);
                        
                        /* SMS to counselor */
                        
                        $phn = $counsildet[0]['phone'];
                        
                        $phone = $phn;
                        $ph = "$phone";
                        // $pin = $this->generatePIN(4);
                        $post_data = array(
                            // 'From' doesn't matter; For transactional, this will be replaced with your SenderId;
                            // For promotional, this will be ignored by the SMS gateway
                                'From' => '08039534067',
                                'To' => $ph,
                                'Body' => 'Dear user, your appointment has been Rescheduled with eYogi on ' . $start . ' starts from ' . $scedulertimestarts . ' to ' . $scedulertimeends . ' . Regards eYogi Team',
                            //'Body' => 'OTP for verifying your mobile with eYogi is ' . $pin . '', //Incase you are wondering who Dr. Rajasekhar is http://en.wikipedia.org/wiki/Dr._Rajasekhar_(actor)
                        );

                        $exotel_sid = "eyogi"; // Your Exotel SID - Get it from here: http://my.exotel.in/settings/site#api-settings
                        $exotel_token = "fb83c4d9b235b90d0e3b8b1194c942de489b003f"; // Your exotel token - Get it from here: http://my.exotel.in/settings/site#api-settings

                        $url = "https://" . $exotel_sid . ":" . $exotel_token . "@twilix.exotel.in/v1/Accounts/" . $exotel_sid . "/Sms/send";

                        $ch = curl_init();
                        curl_setopt($ch, CURLOPT_VERBOSE, 1);
                        curl_setopt($ch, CURLOPT_URL, $url);
                        curl_setopt($ch, CURLOPT_POST, 1);
                        curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
                        curl_setopt($ch, CURLOPT_FAILONERROR, 0);
                        curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, 0);
                        curl_setopt($ch, CURLOPT_POSTFIELDS, http_build_query($post_data));
                            /*   $data = array(
                                    'phone' => $phone,
                                    'code' => $pin,
                                    'is_expired' => 0,
                                    'created_at' => date("Y-m-d H:i:s"),
                                );

                                $this->user_model->insertOTPDetails($data); */
                        $http_result = curl_exec($ch);
                        $error = curl_error($ch);
                        $http_code = curl_getinfo($ch, CURLINFO_HTTP_CODE);

                        curl_close($ch);
                        
                        // Mail to eyogi_counselors
                    
                        $this->load->library('email');
                        //$counsilormail
                        $to = "keerthi@sectorqube.com";
                        $subject = "Rescheduled Details";

                        $message = '<html><body>';

                        $message .= '<table rules="all" style="border-color: #666;" cellpadding="10">';
                        $message .= "<tr style='background: #eee;'><td><strong>Counsellor:</strong> </td><td>" . $counselor_name . "</td></tr>";
                        $message .= "<tr style='background: #eee;'><td><strong>Date:</strong> </td><td>" . $start . "</td></tr>";
                        $message .= "<tr style='background: #eee;'><td><strong>From:</strong> </td><td>" . $scedulertimestarts . "</td></tr>";
                        $message .= "<tr style='background: #eee;'><td><strong>Till:</strong> </td><td>" . $scedulertimeends . "</td></tr>";

                        $message .= "</table>";
                        $message .= "</body></html>";



                        $headers = "From: info@eyogi.in" . "\r\n" . "Reply-To: info@eyogi.in" . "\r\n" . "Return-Path: info@eyogi.in" . "\r\n" .
                            'X-Mailer: PHP/' . phpversion() . "\r\n" .
                            'Content-Type: text/html; charset=ISO-8859-1' . "\r\n" .
                            'MIME-Version: 1.0' . "\r\n\r\n";


                        // $headers = "From: ".$_POST['emailaddress']."" . "\r\n" .
                        //  "CC: bimalsasidharan@gmail.com";
                        mail($to, $subject, $message, $headers);
                        
                    }
                    
                    echo 1;
                } else {
                    echo 0;
                }
            }
            
        }
    }


    public function makeCall() {

        $schedule_details = $this->user_model->getScheduleInfo();
        
            foreach ($schedule_details as $sche) {

                if (!empty($sche)) {
                

                        $counselorData = $this->user_model->getUserdata($sche->counselor_id, $sche->id);
                        $userData = $this->user_model->getUserdata($sche->visitor_info, $sche->id);

                        $counselor_number = "$counselorData->phone";
                        $user_number = "$userData->phone";
                        $data_ren = $this->user_model->getCounsRenumeration($sche->counselor_id);
                        $time = $data_ren[0]['minutes'] * 60;
                        $amnt = $data_ren[0]['amount'];
                        $timeOut = "$time";
                        $post_data = array(
                            'From' => $counselor_number,
                            'To' => $user_number,
                            'CallerId' => "08039534067",
                            'TimeLimit' => $timeOut,
                            'CallType' => "trans", //Can be "trans" for transactional and "promo" for promotional content
                            'MaxRetries' => "0",
                        );


                        $data = $this->eyogiCall($sche->id, $counselor_number, $user_number, $amnt, $post_data);
                        $counselor_number = "";
                        $user_number = "";
                        $data1 = array(
                            'schedule_id' => $sche->id,
                            'sid' => $data->Call->Sid,
                            'status' => $data->Call->Status,
                            'duration' => $data->Call->Duration,
                            'price' => $data->Call->Price,
                            'renumeration' => $amnt,
                            'startTime' => $data->Call->StartTime,
                            'refund_status' => 0,
                            'endTime' => $data->Call->EndTime,
                            'dateCreated' => $data->Call->DateCreated,
                            'dateUpdated' => $data->Call->DateUpdated,
                        );

                        $this->user_model->insertCallDetails($data1);
                }
            }    
                
                $call_details = $this->user_model->getCallDetails();

                if (!empty($call_details)) {
                    foreach ($call_details as $call) {
                        $data_ren = $this->user_model->getCounsRenumerationCall($call->schedule_id);

                        $this->getCall($call->sid, $call->startTime, $data_ren[0]['amount']);
                    }
                }

                $call_details = $this->user_model->getCallDetailsByDate();

                if (!empty($call_details)) {
                    foreach ($call_details as $call) {
                        date_default_timezone_set('Asia/Kolkata');
                        $date = date('Y-m-d', strtotime($call->dateCreated));
                        $time1 = date('h:i A', strtotime($call->dateCreated));
                        $date = new DateTime($time1);
                        $now = new DateTime();
                        $date_diff = $date->diff($now)->format("%i");


                        if ($date_diff == 5) {
                            $refund_status = $this->user_model->getRefundStatus($call->sid);
                            if ($refund_status->refund_status == 0) {
                                
                                $data_ren = $this->user_model->getCounsRenumerationCall($call->schedule_id);
                                $visitor_info = $this->user_model->getVisitorInfo($call->schedule_id);
                                if ($call->status != "completed" && $call->status != "in-progress") {

                                    $sql = "SELECT coupon_balance "
                                            . "FROM ey_coupon_balance "
                                            . "where visitor_info='" . $visitor_info->visitor_info . "' and payment_status='Captured'";
                                    $query = $this->db->query("SELECT coupon_balance "
                                            . "FROM ey_coupon_balance "
                                            . "where visitor_info='" . $visitor_info->visitor_info . "' and payment_status='Captured'");

                                    $res_user = $query->result_array();
                                    $coupon_bal = $res_user[0]['coupon_balance'];

                                    $new_bal = $coupon_bal + $data_ren[0]['amount'];

                                    if (is_numeric($visitor_info->visitor_info)) {
                                        $sql = "UPDATE `ey_coupon_balance` SET `coupon_balance`='" . $new_bal . "' WHERE `visitor_info` ='" . $visitor_info->visitor_info . "' and `payment_status` ='Captured'";

                                        $this->db->query($sql);
                                    } else {
                                        $sql = "UPDATE `ey_coupon_balance` SET `coupon_balance`='" . $new_bal . "' WHERE `visitor_info` ='" . $visitor_info->visitor_info . "' and `payment_status` ='Captured'";
                                        $this->db->query($sql);
                                    }
                                    $sql1 = "UPDATE `ey_call_details` SET `refund_status`=1 WHERE `sid` ='" . $call->sid . "'";

                                    $this->db->query($sql1);
                                    $myfile = file_put_contents('logs.txt', $coupon_bal . "--" . $new_bal . "--" . print_r($call, true) . "--" . $date_diff . PHP_EOL, FILE_APPEND | LOCK_EX);
                                }
                            }
                        }
                    }
                }


                $sms_details = $this->user_model->getReminderDetails();
                foreach ($sms_details as $sms) {
                    if (!empty($sms)) {
                        $counselorData = $this->user_model->getUserdata($sms->counselor_id, $sms->id);
                        $userData = $this->user_model->getUserdataSMS($sms->visitor_info, $sms->id);

                        $counselor_number = "$counselorData->phone";
                        $user_number = "$userData->phone";
                        $time = date('h:i A', strtotime($sms->time_starts));

                        $sms_data_couns = array(
                    // 'From' doesn't matter; For transactional, this will be replaced with your SenderId;
                    // For promotional, this will be ignored by the SMS gateway
                            'From' => '08039534067',
                            'To' => $counselor_number,
                            'Body' => 'Hi, You have an appointment scheduled with eYogi user on ' . date("Y-m-d") . ' at ' . $time . ' . Regards eYogi Team',
                        //Incase you are wondering who Dr. Rajasekhar is http://en.wikipedia.org/wiki/Dr._Rajasekhar_(actor)
                        );
                        $sms_data_user = array(
                            // 'From' doesn't matter; For transactional, this will be replaced with your SenderId;
                            // For promotional, this will be ignored by the SMS gateway
                            'From' => '08039534067',
                            'To' => $user_number,
                            'Body' => 'Hi, You have an appointment scheduled with eYogi user on ' . date("Y-m-d") . ' at ' . $time . ' . Regards eYogi Team',
                        //Incase you are wondering who Dr. Rajasekhar is http://en.wikipedia.org/wiki/Dr._Rajasekhar_(actor)
                        );


                        $data = $this->eyogiSMS($sms_data_couns);
                        $data = $this->eyogiSMS($sms_data_user);
                    }
                }
        
    }

    private function eyogiCall($id, $counselor_number, $user_number, $amnt, $post_data) {
        $http_result = "";
        $exotel_sid = "eyogi"; // Your Exotel SID - Get it from here: http://my.exotel.in/settings/site#api-settings
        $exotel_token = "fb83c4d9b235b90d0e3b8b1194c942de489b003f"; // Your exotel token - Get it from here: http://my.exotel.in/settings/site#api-settings

        $url = "https://" . $exotel_sid . ":" . $exotel_token . "@twilix.exotel.in/v1/Accounts/" . $exotel_sid . "/Calls/connect.json";

        $ch = curl_init();
        curl_setopt($ch, CURLOPT_VERBOSE, 1);
        curl_setopt($ch, CURLOPT_URL, $url);
        curl_setopt($ch, CURLOPT_POST, 1);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
        curl_setopt($ch, CURLOPT_FAILONERROR, 0);
        curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, 0);
        curl_setopt($ch, CURLOPT_POSTFIELDS, http_build_query($post_data));

        $http_result = curl_exec($ch);

        $txt = print_r($post_data, true);

        $myfile = file_put_contents('logs.txt', $txt . "-" . $sql . PHP_EOL, FILE_APPEND | LOCK_EX);

        $error = curl_error($ch);
        $http_code = curl_getinfo($ch, CURLINFO_HTTP_CODE);
        curl_close($ch);
        $data = json_decode($http_result);
        print_r($data);
        return $data;
    }

    public function getCall($sid, $time, $amnt) {
        $exotel_sid = "eyogi"; // Your Exotel SID - Get it from here: http://my.exotel.in/settings/site#api-settings
        $exotel_token = "fb83c4d9b235b90d0e3b8b1194c942de489b003f"; // Your exotel token - Get it from here: http://my.exotel.in/settings/site#api-settings

        $url = "https://" . $exotel_sid . ":" . $exotel_token . "@twilix.exotel.in/v1/Accounts/" . $exotel_sid . "/Calls/" . $sid . ".json";

        $ch = curl_init();
        curl_setopt($ch, CURLOPT_VERBOSE, 1);
        curl_setopt($ch, CURLOPT_URL, $url);

        curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
        curl_setopt($ch, CURLOPT_FAILONERROR, 0);
        curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, 0);
        curl_setopt($ch, CURLOPT_HTTPHEADER, array(
            'Content-Type: application/json',
            'Accept: application/json'
        ));

        $http_result = curl_exec($ch);
        $error = curl_error($ch);
        $http_code = curl_getinfo($ch, CURLINFO_HTTP_CODE);

        curl_close($ch);
        $data = json_decode($http_result);

        $sche = $this->user_model->getSchedData($sid);

        $visitor_info = $this->user_model->getVisitorData($sche->schedule_id);
        $data1 = array(
            'status' => $data->Call->Status,
            'duration' => $data->Call->Duration,
            'price' => $data->Call->Price,
            'startTime' => $data->Call->StartTime,
            'endTime' => $data->Call->EndTime,
            'dateCreated' => $data->Call->DateCreated,
            'dateUpdated' => $data->Call->DateUpdated,
        );
        if ($data->Call->Status == "completed") {
            if ($data->Call->Duration != null || $data->Call->Price != null) {
                $counselorData = $this->user_model->getUserdata($visitor_info->counselor_id, $sche->schedule_id);
                $userData = $this->user_model->getUserdataSMS($visitor_info->visitor_info, $sche->schedule_id);

                $counselor_number = "$counselorData->phone";
                $user_number = "$userData->phone";

                $link = base_url() . 'Counselor/counselor_addnotes/' . urlencode(base64_encode($sche->schedule_id)) . '/' . urlencode(base64_encode(date('Y-m-d')));
                //echo $link;
                $sms_data_couns = array(
                    // 'From' doesn't matter; For transactional, this will be replaced with your SenderId;
                    // For promotional, this will be ignored by the SMS gateway
                    'From' => '08039534067',
                    'To' => $counselor_number,
                    'Body' => 'Hi, Your appointment has been completed. Please update your comments and notes using the link: ' . $link . ' . Regards eYogi Team',
                        //Incase you are wondering who Dr. Rajasekhar is http://en.wikipedia.org/wiki/Dr._Rajasekhar_(actor)
                );
//                $sms_data_user = array(
//                    // 'From' doesn't matter; For transactional, this will be replaced with your SenderId;
//                    // For promotional, this will be ignored by the SMS gateway
//                    'From' => '08039534067',
//                    'To' => $user_number,
//                    'Body' => 'Hi, You have an appointment scheduled with eYogi user on ' . date("Y-m-d") . ' at ' . $sms->time_starts . ' . Regards eYogi Team',
//                        //Incase you are wondering who Dr. Rajasekhar is http://en.wikipedia.org/wiki/Dr._Rajasekhar_(actor)
//                );


                $c_id = $visitor_info->counselor_id;
                $u_id = $visitor_info->visitor_info;
                $s_id = $sche->schedule_id;



                $data5 = array(
                    'counselor_id' => $c_id,
                    'user_id' => $u_id,
                    'schedule_id' => $s_id,
                );

                $ratin = $this->user_model->insert_ratingurl($data5);


                $url1 = base_url() . $ratin;

                //  $phn = $visitordet[0]['visiter_info'];
                // print_r($visitordet[0]);exit;
                // $counsilorphn
                $phone = $user_number;
                $ph = "$phone";
                // $pin = $this->generatePIN(4);
                $sms_data_user = array(
                    // 'From' doesn't matter; For transactional, this will be replaced with your SenderId;
                    // For promotional, this will be ignored by the SMS gateway
                    'From' => '08039534067',
                    'To' => $ph,
                    'Body' => 'Hi, Your appointment has been completed. Please update your comments and ratings using the link: ' . $url1 . ' . Regards eYogi Team',
                        //'Body' => 'OTP for verifying your mobile with eYogi is ' . $pin . '', //Incase you are wondering who Dr. Rajasekhar is http://en.wikipedia.org/wiki/Dr._Rajasekhar_(actor)
                );


                $data = $this->eyogiSMS($sms_data_couns);
                $data = $this->eyogiSMS($sms_data_user);
                //   $data = $this->eyogiSMS($sms_data_user);
            }
        }
        $this->user_model->updateCallDetails($sid, $data1);

//        $callData = $this->user_model->getCallDetailsSID($sid);
//
//        if (!empty($callData)) {
//
//            if (($callData->status != "completed" && $callData->status != "in-progress") && ($callData->duration == 0 && $callData->duration == NULL)) {
//
//                $query = $this->db->query("SELECT coupon_balance "
//                        . "FROM ey_coupon_purchased "
//                        . "where coupon_code='" . $visitor_info->visitor_info . "' and payment_status='Captured'");
//
//                $res_user = $query->result_array();
//                $coupon_bal = $res_user[0]['coupon_balance'];
//
//                $new_bal = $coupon_bal + $amnt;
//
//                if (is_numeric($visitor_info->visitor_info)) {
//                    echo $sql = "UPDATE `ey_coupon_purchased` SET `coupon_balance`='" . $new_bal . "' WHERE `visiter_info` ='" . $visitor_info->visitor_info . "' and `payment_status` ='Captured'";
//
//                    $this->db->query($sql);
//                } else {
//                    echo $sql = "UPDATE `ey_coupon_purchased` SET `coupon_balance`='" . $new_bal . "' WHERE `coupon_code` ='" . $visitor_info->visitor_info . "' and `payment_status` ='Captured'";
//                    $this->db->query($sql);
//                }
//                $myfile = file_put_contents('logs.txt', $coupon_bal . "--" . $new_bal . "--" . $sql . PHP_EOL, FILE_APPEND | LOCK_EX);
//            }
//        }
        // print_r($http_result);
    }
    

    public function eyogiSMS($sms) {

        $exotel_sid = "eyogi"; // Your Exotel SID - Get it from here: http://my.exotel.in/settings/site#api-settings
        $exotel_token = "fb83c4d9b235b90d0e3b8b1194c942de489b003f"; // Your exotel token - Get it from here: http://my.exotel.in/settings/site#api-settings

        $url = "https://" . $exotel_sid . ":" . $exotel_token . "@twilix.exotel.in/v1/Accounts/" . $exotel_sid . "/Sms/send";

        $ch = curl_init();
        curl_setopt($ch, CURLOPT_VERBOSE, 1);
        curl_setopt($ch, CURLOPT_URL, $url);
        curl_setopt($ch, CURLOPT_POST, 1);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
        curl_setopt($ch, CURLOPT_FAILONERROR, 0);
        curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, 0);
        curl_setopt($ch, CURLOPT_POSTFIELDS, http_build_query($sms));

        $http_result = curl_exec($ch);
        $error = curl_error($ch);
        $http_code = curl_getinfo($ch, CURLINFO_HTTP_CODE);

        curl_close($ch);

        "Response = " . print_r($http_result);

        echo 1;
    }

    public function call() {

        
        $post_data = array(
            'From' => "9656237132",
            'To' => "8289947524",
            'CallerId' => "08039534067",
            'TimeLimit' => "120",
            'TimeOut' => "240",
            'CallType' => "trans" //Can be "trans" for transactional and "promo" for promotional content
        );



        $exotel_sid = "eyogi"; // Your Exotel SID - Get it from here: http://my.exotel.in/settings/site#api-settings
        $exotel_token = "fb83c4d9b235b90d0e3b8b1194c942de489b003f"; // Your exotel token - Get it from here: http://my.exotel.in/settings/site#api-settings

        $url = "https://" . $exotel_sid . ":" . $exotel_token . "@twilix.exotel.in/v1/Accounts/" . $exotel_sid . "/Calls/connect";

        $ch = curl_init();
        curl_setopt($ch, CURLOPT_VERBOSE, 1);
        curl_setopt($ch, CURLOPT_URL, $url);
        curl_setopt($ch, CURLOPT_POST, 1);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
        curl_setopt($ch, CURLOPT_FAILONERROR, 0);
        curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, 0);
        curl_setopt($ch, CURLOPT_POSTFIELDS, http_build_query($post_data));

        $http_result = curl_exec($ch);
        $error = curl_error($ch);
        $http_code = curl_getinfo($ch, CURLINFO_HTTP_CODE);

        curl_close($ch);
        print "Response = " . print_r($http_result);

//         $exotel_sid = "eyogi"; // Your Exotel SID - Get it from here: http://my.exotel.in/settings/site#api-settings
//        $exotel_token = "fb83c4d9b235b90d0e3b8b1194c942de489b003f"; // Your exotel token - Get it from here: http://my.exotel.in/settings/site#api-settings
//$sid="441ab953afbaa4393001dcd1dfc4dd78";
//        $url = "https://" . $exotel_sid . ":" . $exotel_token . "@twilix.exotel.in/v1/Accounts/" . $exotel_sid . "/Calls/" . $sid . ".json";
//
//        $ch = curl_init();
//        curl_setopt($ch, CURLOPT_VERBOSE, 1);
//        curl_setopt($ch, CURLOPT_URL, $url);
//
//        curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
//        curl_setopt($ch, CURLOPT_FAILONERROR, 0);
//        curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, 0);
//        curl_setopt($ch, CURLOPT_HTTPHEADER, array(
//            'Content-Type: application/json',
//            'Accept: application/json'
//        ));
//
//        $http_result = curl_exec($ch);
//        $error = curl_error($ch);
//        $http_code = curl_getinfo($ch, CURLINFO_HTTP_CODE);
//
//        curl_close($ch);
//        
//        print "Response = " . print_r($http_result);
    }

    public function appDetails() {
        $schedule_id = $this->input->post('id');
        $date = $this->input->post('date');

        $appointment_details = $this->user_model->getAppointment($schedule_id, $date);

        if (!empty($appointment_details)) {

            $call_details = $this->user_model->getCallData($schedule_id);
            if (!empty($call_details)) {
                $init = $call_details->duration;
                $hours = floor($init / 3600);
                $minutes = floor(($init / 60) % 60);
                $seconds = $init % 60;

                $duration = "$hours:$minutes:$seconds";

                $appointment_details->status = $call_details->status;
                $appointment_details->duration = $duration;
                $appointment_details->price = $call_details->price;
                $appointment_details->renumeration = $call_details->renumeration;
            } else {
                $appointment_details->status = "-";
                $appointment_details->duration = "-";
                $appointment_details->price = "-";
                $appointment_details->renumeration = "-";
            }
        }
        $json = json_encode($appointment_details);
        echo($json);
    }

    Public function contactform() {

        $email = $_POST['email12'];
        $subject = $_POST['subject12'] . ' #Contactform';
        $message1 = $_POST['message12'];

        $data = array(
            'email' => $email,
            'subject' => $subject,
            'message' => $message1,
            'date' => date('Y-m-d')
        );

        if ($this->user_model->insert_contact($data)) {


            $from_email = "info@eyogi.in";
            $this->load->library('email');

            $to = "info@eyogi.in";
            // $subject = "New mail from eyogi contact form";


            $message = '<html><body>';

            $message .= '<table rules="all" style="border-color: #666;" cellpadding="10">';

            $message .= "<tr><td><strong>Email:</strong> </td><td>" . $email . "</td></tr>";
            $message .= "<tr><td><strong>Subject:</strong> </td><td>" . $subject . "</td></tr>";
            $message .= "<tr><td><strong>Message:</strong> </td><td>" . $message1 . "</td></tr>";
            $message .= "<tr><td><strong>Date:</strong> </td><td>" . date('Y-m-d') . "</td></tr>";

            $message .= "</table>";
            $message .= "</body></html>";

            $headers = "From: " . $email . "" . "\r\n" . "Reply-To: info@eyogi.in" . "\r\n" . "Return-Path: info@eyogi.in" . "\r\n" .
                    'X-Mailer: PHP/' . phpversion() . "\r\n" .
                    'Content-Type: text/html; charset=ISO-8859-1' . "\r\n" .
                    'MIME-Version: 1.0' . "\r\n\r\n";


            // $headers = "From: ".$_POST['emailaddress']."" . "\r\n" .
            //  "CC: bimalsasidharan@gmail.com";
            if (mail($to, $subject, $message, $headers)) {
                // echo 'wiqr';
                if ($_POST['page_type'] == "index") {
                    redirect('/?msg=success');
                } else {
                    redirect('home/keepintouch?msg=success');
                }
            }
        }
    }

    Public function contactform1() {

        $email = $_POST['email14'];
        $subject = $_POST['subject14'] . ' #homepageContactform';
        $message1 = $_POST['message14'];

        $data = array(
            'email' => $email,
            'subject' => $subject,
            'message' => $message1,
            'date' => date('Y-m-d')
        );

        if ($this->user_model->insert_contact($data)) {


            $from_email = "info@eyogi.in";
            $this->load->library('email');

            $to = "info@eyogi.in";
            // $subject = "New mail from eyogi contact form";

            $message = '<html><body>';

            $message .= '<table rules="all" style="border-color: #666;" cellpadding="10">';

            $message .= "<tr><td><strong>Email:</strong> </td><td>" . $email . "</td></tr>";
            $message .= "<tr><td><strong>Subject:</strong> </td><td>" . $subject . "</td></tr>";
            $message .= "<tr><td><strong>Message:</strong> </td><td>" . $message1 . "</td></tr>";
            $message .= "<tr><td><strong>Date:</strong> </td><td>" . date('Y-m-d') . "</td></tr>";

            $message .= "</table>";
            $message .= "</body></html>";

            $headers = "From: " . $email . "" . "\r\n" . "Reply-To: info@eyogi.in" . "\r\n" . "Return-Path: info@eyogi.in" . "\r\n" .
                    'X-Mailer: PHP/' . phpversion() . "\r\n" .
                    'Content-Type: text/html; charset=ISO-8859-1' . "\r\n" .
                    'MIME-Version: 1.0' . "\r\n\r\n";


            // $headers = "From: ".$_POST['emailaddress']."" . "\r\n" .
            //  "CC: bimalsasidharan@gmail.com";
            if (mail($to, $subject, $message, $headers)) {
                // echo 'wiqr';
                if ($_POST['page_type'] == "index") {
                    redirect('/?msg=success');
                } else {
                    redirect('home/keepintouch?msg=success');
                }
            }
        }
    }

    Public function enquiry() {

        $phone = $_POST['phone13'];
        $email = $_POST['email13'];
        $name = $_POST['name13'];
        $message1 = $_POST['message13'];

        $data = array(
            'email' => $email,
            'name' => $name,
            'phone' => $phone,
            'message' => $message1,
            'date' => date('Y-m-d'),
        );
        //print_r($data);
        if ($this->user_model->insert_enquiry($data)) {


            $this->load->library('email');

            $to = "vipin.menon@gmail.com";
            $subject = "Enquiry Details";

            $message = '<html><body>';

            $message .= '<table rules="all" style="border-color: #666;" cellpadding="10">';
            $message .= "<tr><td><strong>Name:</strong> </td><td>" . $name . "</td></tr>";
            $message .= "<tr><td><strong>Email:</strong> </td><td>" . $email . "</td></tr>";
            $message .= "<tr><td><strong>Phone Number:</strong> </td><td>" . $phone . "</td></tr>";
            $message .= "<tr><td><strong>Message:</strong> </td><td>" . $message1 . "</td></tr>";
            $message .= "<tr><td><strong>Date:</strong> </td><td>" . date('Y-m-d') . "</td></tr>";

            $message .= "</table>";
            $message .= "</body></html>";



            $headers = 'From:  info@eyogi.in ' . "\r\n" . "Reply-To: info@eyogi.in" . "\r\n" . "Return-Path: info@eyogi.in" . "\r\n" .
                    'X-Mailer: PHP/' . phpversion() . "\r\n" .
                    'Content-Type: text/html; charset=ISO-8859-1' . "\r\n" .
                    'MIME-Version: 1.0' . "\r\n\r\n";


            // $headers = "From: ".$_POST['emailaddress']."" . "\r\n" .
            //  "CC: bimalsasidharan@gmail.com";
            if (mail($to, $subject, $message, $headers)) {

                echo "success";
            }
        }
    }

    public function keepPageUrl() {
        $url = $this->input->post('url');
        $this->session->set_userdata('page_url', $url);
        $this->session->set_userdata('login', 1);
    }

    public function join() {
        $to = "vipin.menon@gmail.com";
        $subject = "Enquiry Details";

        $message = $this->load->view('email', '', true);

        $headers = 'From:  info@eyogi.in ' . "\r\n" . "Reply-To: info@eyogi.in" . "\r\n" . "Return-Path: info@eyogi.in" . "\r\n" .
                'X-Mailer: PHP/' . phpversion() . "\r\n" .
                'Content-Type: text/html; charset=ISO-8859-1' . "\r\n" .
                'MIME-Version: 1.0' . "\r\n\r\n";


        // $headers = "From: ".$_POST['emailaddress']."" . "\r\n" .
        //  "CC: bimalsasidharan@gmail.com";
        if (mail($to, $subject, $message, $headers)) {

            echo "success";
        }
    }

    public function blog() {

        $userData['logoutUrl'] = $this->facebook->logout_url();
        $userData['oauthURL'] = base_url() . $this->config->item('linkedin_redirect_url') . '?oauth_init=1';
        $userData['authUrl'] = $this->facebook->login_url();
        $userData['loginURL'] = $this->google->loginURL();
        $userid = $this->session->userdata('userData');
        if (!isset($userid->id)) {
            $userData['data1'] = $this->user_model->fetch_user_data($userid['oauth_uid']);
        } else {
            $userData['data1'] = $this->user_model->fetch_user($userid->id);
        }
        $this->load->view('blog', $userData);
    }

    public function test() {
        echo "abc";
    }

    public function purchaseList() {

        $from = $this->input->post('date1');
        $to = $this->input->post('date2');
        $userData['payment'] = $this->user_model->get_purchase_details($from, $to);
        $this->load->view('admin/PurchaseReportByDate', $userData);
    }

    public function purchaseDetails() {
        $user_id = $this->input->post('id');
        $purchse_details = $this->user_model->getUserPurchase($user_id);
        $json = json_encode($purchse_details);
        echo($json);
    }

    public function delPurchasedetails() {
        $id = $this->input->post('id');
        $this->db->where('id', $id);
        $this->db->delete('ey_coupon_purchased');
        echo 1;
    }

    public function todaylist() {
        $from = $this->input->post('date1');
        $to = $this->input->post('date2');
        $userData['call'] = $this->user_model->get_today_logs($from, $to);
        //print_r($userData['call']);exit;
        $this->load->view('admin/callreportbydate', $userData);
    }

    public function callReports() {
        $user_id = $this->input->post('id');
        $call_logs = $this->user_model->getUserlogs($user_id);
        $json = json_encode($call_logs);
        echo($json);
    }

    public function delCalldetails() {
        $id = $this->input->post('id');
        $this->db->where('schedule_id', $id);
        $this->db->delete('ey_call_details');
        echo 1;
    }

    public function terms_conditions() {
        $userData['logoutUrl'] = $this->facebook->logout_url();
        $userData['oauthURL'] = base_url() . $this->config->item('linkedin_redirect_url') . '?oauth_init=1';
        $userData['authUrl'] = $this->facebook->login_url();
        $userData['loginURL'] = $this->google->loginURL();
        if ($this->session->userdata('loggedIn') == true) {
            $userid = $this->session->userdata('userData');
            $userData['data1'] = $this->user_model->fetch_user($userid->id);
        }
        $this->load->view('terms', $userData);
    }

    public function privacy() {
        $userData['logoutUrl'] = $this->facebook->logout_url();
        $userData['oauthURL'] = base_url() . $this->config->item('linkedin_redirect_url') . '?oauth_init=1';
        $userData['authUrl'] = $this->facebook->login_url();
        $userData['loginURL'] = $this->google->loginURL();
        if ($this->session->userdata('loggedIn') == true) {
            $userid = $this->session->userdata('userData');
            $userData['data1'] = $this->user_model->fetch_user($userid->id);
        }
        $this->load->view('privacy', $userData);
    }

    public function refund() {
        $userData['logoutUrl'] = $this->facebook->logout_url();
        $userData['oauthURL'] = base_url() . $this->config->item('linkedin_redirect_url') . '?oauth_init=1';
        $userData['authUrl'] = $this->facebook->login_url();
        $userData['loginURL'] = $this->google->loginURL();
        if ($this->session->userdata('loggedIn') == true) {
            $userid = $this->session->userdata('userData');
            $userData['data1'] = $this->user_model->fetch_user($userid->id);
        }
        $this->load->view('refund', $userData);
    }

    public function faq() {
        $userData['logoutUrl'] = $this->facebook->logout_url();
        $userData['oauthURL'] = base_url() . $this->config->item('linkedin_redirect_url') . '?oauth_init=1';
        $userData['authUrl'] = $this->facebook->login_url();
        $userData['loginURL'] = $this->google->loginURL();
        if ($this->session->userdata('loggedIn') == true) {
            $userid = $this->session->userdata('userData');
            $userData['data1'] = $this->user_model->fetch_user($userid->id);
        }
        $this->load->view('faq', $userData);
    }

    public function schedule_fb() {
        echo "abc";
        if ($this->facebook->is_authenticated()) {
            echo "123";
            $arr = $this->facebook->is_authenticated();

            if (is_array($arr)) {
                $this->session->unset_userdata('loggedIn');
                $this->session->unset_userdata('userData');
                $page = $this->session->userdata('page');

                if ($page == "reg") {
                    redirect('user/index');
                } else {
                    redirect('user/joinus');
                }
            }

            // Get user facebook profile details
            $userProfile = $this->facebook->request('get', '/me?fields=id,first_name,last_name,email,gender,locale,picture');

            // Preparing data for database insertion
            $userData['oauth_provider'] = 'facebook';
            $userData['oauth_uid'] = $userProfile['id'];
            $userData['first_name'] = $userProfile['first_name'];
            $userData['last_name'] = $userProfile['last_name'];
            $userData['email'] = $userProfile['email'];
            $userData['gender'] = $userProfile['gender'];
            $userData['locale'] = $userProfile['locale'];
            $userData['profile_url'] = 'https://www.facebook.com/' . $userProfile['id'];
            $userData['picture_url'] = $userProfile['picture']['data']['url'];

            // Insert or update user data
            $userID = $this->user_model->checkUser($userData);

            if ($userID == 0) {
                $this->session->set_userdata('page', 'reg');

                $data['logoutUrl'] = $this->facebook->logout_url();
                $data['oauthURL'] = base_url() . $this->config->item('linkedin_redirect_url') . '?oauth_init=1';
                $data['authUrl'] = $this->facebook->login_url();
                $data['userData'] = $userData;
                $data['loginURL'] = $this->google->loginURL();
                $data['email'] = "";
                $data['login'] = 0;
                $data['msg'] = "User already registerd with this email id!!! Please login";
                // Load login & profile view
                $this->load->view('user/register', $data);
            } else {
                $this->session->set_userdata('userData', $userData);
                // Check user data insert or update status
                if (!empty($userID)) {
                    $data['userData'] = $userData;
                    $this->session->set_userdata('userData', $userData);
                } else {
                    $data['userData'] = array();
                }

                $userData['logoutUrl'] = $this->facebook->logout_url();
                $userData['oauthURL'] = base_url() . $this->config->item('linkedin_redirect_url') . '?oauth_init=1';
                $userData['authUrl'] = $this->facebook->login_url();
                $userData['loginURL'] = $this->google->loginURL();
                $page = $this->session->userdata('page');

                $data = $this->user_model->getLoggedUser($userProfile['email'], $userProfile['id']);

                if (!empty($data)) {
                    $this->session->set_userdata('userData', $data);
                    $this->session->set_userdata('loggedIn', true);
                    if ($data->role_id == 4) {

                        redirect('admin/myprofile');
                    } elseif ($data->role_id == 3) {
                        $created = $data->created_at;
                        $then = strtotime($created);
                        $now = time();

                        $difference = $now - $then;

//Convert seconds into days.
                        $days = floor($difference / (60 * 60 * 24));


                        if ($days == 0) {
                            $to = $userProfile['email'];
                            $subject = "Update your profile - eYogi";
                            $message = '';
                            $message .= '<strong>Please login and update your profile details to list your profile in eYogi schedule section</strong><br>';


                            $headers = "From: info@eyogi.in" . "\r\n" . "Reply-To: info@eyogi.in" . "\r\n" . "Return-Path: info@eyogi.in" . "\r\n" .
                                    'X$headers-Mailer: PHP/' . phpversion() . "\r\n" .
                                    'Content-Type: text/html; charset=ISO-8859-1' . "\r\n" .
                                    'MIME-Version: 1.0' . "\r\n\r\n";


                            // $headers = "From: ".$_POST['emailaddress']."" . "\r\n" .
                            //  "CC: bimalsasidharan@gmail.com";
                            mail($to, $subject, $message, $headers);
                        }
                        redirect('counselor/index');
                    }
                } else {
                    redirect('/');
                }
            }
        }
    }

    public function reconnectCall() {
        $post_data = array(
            'From' => "09656237132",
            'To' => "08039534067",
            'CallerId' => "08039534067",
            'Url' => "http://my.exotel.in/exoml/start/164257",
            'TimeLimit' => "240", //This is optional
            'TimeOut' => "240", //This is also optional
            'CallType' => "trans"
        );


        $exotel_sid = "eyogi"; // Your Exotel SID - Get it from here: http://my.exotel.in/settings/site#api-settings
        $exotel_token = "fb83c4d9b235b90d0e3b8b1194c942de489b003f"; // Your exotel token - Get it from here: http://my.exotel.in/settings/site#api-settings

        $url = "https://" . $exotel_sid . ":" . $exotel_token . "@twilix.exotel.in/v1/Accounts/" . $exotel_sid . "/Calls/connect";

        $ch = curl_init();
        curl_setopt($ch, CURLOPT_VERBOSE, 1);
        curl_setopt($ch, CURLOPT_URL, $url);
        curl_setopt($ch, CURLOPT_POST, 1);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
        curl_setopt($ch, CURLOPT_FAILONERROR, 0);
        curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, 0);
        curl_setopt($ch, CURLOPT_POSTFIELDS, http_build_query($post_data));

        $http_result = curl_exec($ch);
        $error = curl_error($ch);
        $http_code = curl_getinfo($ch, CURLINFO_HTTP_CODE);

        curl_close($ch);

        print "Response = " . print_r($http_result);
    }

    public function reconnectCallAPI() {
        // print_r($_REQUEST);
        $txt = print_r($_REQUEST, true);


        $callFrom = ltrim($_REQUEST['CallFrom'], '0');
        $CurrentTime = $_REQUEST['CurrentTime'];

        $date = date('Y-m-d', strtotime($CurrentTime));
        $time1 = date('h:i A', strtotime($CurrentTime));


//        $query = "SELECT u.id as user_id,s.counselor_id ,s.visitor_info,s.schedule_id,s.is_modified,s.note  "
//                . "FROM ey_schedule s "
//                . "join ey_user u on s.visitor_info=u.id "
//                . "where u.phone='" . $callFrom . "' and s.date='" . $date . "'";
//       

        $query = $this->db->query("SELECT distinct s.schedule_id, u.id as user_id,s.id as sche_id,s.counselor_id ,s.date,s.visitor_info,s.is_modified,s.note  "
                . "FROM ey_schedule s "
                . "join ey_user u on s.visitor_info=u.id "
                . "where u.phone='" . $callFrom . "' and s.date='" . $date . "' and s.is_modified=0");

        $res_user = $query->result();

        $q = "SELECT distinct s.schedule_id, u.id as user_id,s.id as sche_id,s.counselor_id ,s.date,s.visitor_info,s.is_modified,s.note  "
                . "FROM ey_schedule s "
                . "join ey_coupon_details u on u.visitor_info=s.visitor_info "
                . "where u.phone='" . $callFrom . "' and s.date='" . $date . "'and s.is_modified=0";
        $query = $this->db->query("SELECT distinct s.schedule_id, u.id as user_id,s.id as sche_id,s.counselor_id ,s.date,s.visitor_info,s.is_modified,s.note  "
                . "FROM ey_schedule s "
                . "join ey_coupon_details u on u.visitor_info=s.visitor_info "
                . "where u.phone='" . $callFrom . "' and s.date='" . $date . "'and s.is_modified=0");

        $res1_user = $query->result();


//        $query = $this->db->query("SELECT distinct u.id as user_id,s.counselor_id ,s.date,s.visitor_info,s.schedule_id,s.is_modified,s.note  "
//                . "FROM ey_reschedule s "
//                . "join ey_user u on s.visitor_info=u.id "
//                . "where u.phone='" . $callFrom . "' and s.date='" . $date . "' and s.is_modified=0");
//        
//        
//        if(!empty($query->result())){
//            $res_code = $query->result();
//        }else{
//            $res_code[] = "";
//        }
//
//        $query = $this->db->query("SELECT distinct u.id as user_id,s.counselor_id ,s.date,s.visitor_info,s.schedule_id,s.is_modified,s.note  "
//                . "FROM ey_reschedule s "
//                . "join ey_coupon_details u on u.visitor_info=s.visitor_info "
//                . "where u.phone='" . $callFrom . "' and s.date='" . $date . "'and s.is_modified=0");
//        if(!empty($query->result())){
//            $res1_code = $query->result();
//        }else{
//            $res1_code[] = "";
//        }
//$result_array = array_merge($res_user, $res1_user, $res_code, $res1_code);
        $result_array = array_merge($res_user, $res1_user);
        foreach ($result_array as $res) {
            $query = $this->db->query("SELECT *
                                 FROM ey_counsellor_schedule where id=" . $res->schedule_id);
            //return $query->result();
            $couns_schedule = $query->row();
            if (strtotime($couns_schedule->time_starts) <= strtotime($time1) && strtotime($couns_schedule->time_ends) > strtotime($time1)) {
                $counselorData = $this->user_model->getUserdata($res->counselor_id, $res->sche_id);
                $userData = $this->user_model->getUserdata($res->visitor_info, $res->sche_id);

                $counselor_number = "$counselorData->phone";
                $user_number = "$userData->phone";
                $data_ren = $this->user_model->getCounsRenumeration($res->counselor_id);
                $time = $data_ren[0]['minutes'] * 60;
                $amnt = $data_ren[0]['amount'];

                $mins = (strtotime($time1) - strtotime($couns_schedule->time_starts)) / 60;
                // $data = $this->eyogiCall($sche->id, $counselor_number, $user_number, $amnt, $post_data);
                $callTime = ((strtotime($couns_schedule->time_ends) - strtotime($time1)) / 60) * 60;


                $timeOut = "$callTime";
                $post_data = array(
                    'From' => $counselor_number,
                    'To' => $user_number,
                    'CallerId' => "08039534067",
                    'TimeLimit' => $timeOut,
                    'CallType' => "trans", //Can be "trans" for transactional and "promo" for promotional content
                    'MaxRetries' => "0",
                );

                $data = $this->eyogiCall($res->sche_id, $counselor_number, $user_number, $amnt, $post_data);
            }

            $myfile = file_put_contents('logs.txt', print_r($data, true) . PHP_EOL, FILE_APPEND | LOCK_EX);
        }
    }

    public function checkWhitelist() {
        //$phone = $this->input->post('phone');
        //echo $number = "$phone";

        $post_data = array(
            'VirtualNumber' => '08039534067',
            'Number' => '9447527646',
            //'Number' => $number,
            'Language' => 'en'
        );
        $exotel_sid = "eyogi"; // Your Exotel SID - Get it from here: http://my.exotel.in/settings/site#api-settings
        $exotel_token = "fb83c4d9b235b90d0e3b8b1194c942de489b003f"; // Your exotel token - Get it from here: http://my.exotel.in/settings/site#api-settings

        $url = "https://" . $exotel_sid . ":" . $exotel_token . "@api.exotel.com/v1/Accounts/" . $exotel_sid . "/CustomerWhitelist.json";

        $ch = curl_init();
        curl_setopt($ch, CURLOPT_VERBOSE, 1);
        curl_setopt($ch, CURLOPT_URL, $url);
        curl_setopt($ch, CURLOPT_POST, 1);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
        curl_setopt($ch, CURLOPT_FAILONERROR, 0);
        curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, 0);
        curl_setopt($ch, CURLOPT_POSTFIELDS, http_build_query($post_data));

        $http_result = curl_exec($ch);
        $error = curl_error($ch);
        $http_code = curl_getinfo($ch, CURLINFO_HTTP_CODE);

        curl_close($ch);
        $data = json_decode($http_result);

        print_r($data);
        // echo $data->Result->Failed;
        echo 0;
    }

    public function coupon_login() {

        $userData['logoutUrl'] = $this->facebook->logout_url();
        $userData['oauthURL'] = base_url() . $this->config->item('linkedin_redirect_url') . '?oauth_init=1';
        $userData['authUrl'] = $this->facebook->login_url();
        $userData['loginURL'] = $this->google->loginURL();
        if ($this->session->userdata('loggedIn') == true) {
            $userid = $this->session->userdata('userData');
            $userData['data1'] = $this->user_model->fetch_user($userid->id);
        }

        $userData['codes'] = $this->user_model->getCouponCodes();
        $this->load->view('coupon_login', $userData);
    }

    public function coupon_create() {

        $userData['logoutUrl'] = $this->facebook->logout_url();
        $userData['oauthURL'] = base_url() . $this->config->item('linkedin_redirect_url') . '?oauth_init=1';
        $userData['authUrl'] = $this->facebook->login_url();
        $userData['loginURL'] = $this->google->loginURL();
        if ($this->session->userdata('loggedIn') == true) {
            $userid = $this->session->userdata('userData');
            $userData['data1'] = $this->user_model->fetch_user($userid->id);
        }

        $userData['codes'] = $this->user_model->getCouponCodes();
        $this->load->view('create_coupon', $userData);
    }

    Public function coupon_loginsubmit() {

        $email = $_POST['email6'];
        $pass = $_POST['pass6'];
        $pass = md5($pass);


        $data = array(
            'email' => $email,
            'pass' => $pass
        );

        if ($this->user_model->check_couponlogin($data)) {

            // $userid = $this->session->set_userdata('is_logged',true);
//redirect('home/coupon_create');
            echo "1";
        } else {


            echo 0;
        }
    }

    public function coupon_creates() {
        //    if($this->session->userdata('is_logged')==true){
        $amount = $_POST['amount12'];
        $coupon = $this->generateRandomString();
        // echo $coupon;exit;

        $data = array(
            'coupon_amount' => $amount,
            'coupon_code' => $coupon,
            'visiter_info' => 'info@eyogi.in',
            'type' => 3,
            'purchase_date' => date("Y-m-d H:i:s"),
            'coupon_balance' => $amount,
            'payment_status' => 'Captured',
            'status' => 'ACTIVE',
        );
        $data1 = array(
            'visitor_info' => $coupon,
            'coupon_balance' => $amount,
            'payment_status' => "Captured"
        );

        $this->user_model->insertCouponBalance($data1);
        if ($this->user_model->insert_coupon($data)) {

            //  $this->load->view('blog', $userData);

            echo "1";
        } else {

            echo "0";
        }
        //    }else{
        //  redirect('home/coupon_login');
        // }
    }

    public function getCouponlist() {

        $userData['logoutUrl'] = $this->facebook->logout_url();
        $userData['oauthURL'] = base_url() . $this->config->item('linkedin_redirect_url') . '?oauth_init=1';
        $userData['authUrl'] = $this->facebook->login_url();
        $userData['loginURL'] = $this->google->loginURL();
        if ($this->session->userdata('loggedIn') == true) {
            $userid = $this->session->userdata('userData');
            $userData['data1'] = $this->user_model->fetch_user($userid->id);
        }

        $userData['couponlist'] = $this->user_model->fetchcouponlist();
        $this->load->view('couponlist', $userData);
    }

    function generateRandomString($length = 6) {
        $characters = '0123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ';
        $charactersLength = strlen($characters);
        $randomString = '';
        for ($i = 0; $i < $length; $i++) {
            $randomString .= $characters[rand(0, $charactersLength - 1)];
        }
        return $randomString;
    }

    public function ratings() {

        $userData['logoutUrl'] = $this->facebook->logout_url();
        $userData['oauthURL'] = base_url() . $this->config->item('linkedin_redirect_url') . '?oauth_init=1';
        $userData['authUrl'] = $this->facebook->login_url();
        $userData['loginURL'] = $this->google->loginURL();
        if ($this->session->userdata('loggedIn') == true) {
            $userid = $this->session->userdata('userData');
            $userData['data1'] = $this->user_model->fetch_user($userid->id);
        }

        /* $c_id = 53;
          $u_id = 66; */

        $id = $this->uri->segment(1);

        $id_enc = urldecode(base64_decode($id));
        $id = substr($id_enc, 0, -1);
        //echo $id;exit;
        $userData['rat_url'] = $this->user_model->getrat_url($id);

        //    print_r($userData['rat_url'][0]);exit;
        //    echo $userData['rat_url'][0]['user_id'];exit;

        $c_id = $userData['rat_url'][0]['counselor_id'];
        $u_id = $userData['rat_url'][0]['user_id'];
        $s_id = $userData['rat_url'][0]['schedule_id'];
        $userData['s_id'] = $s_id;
        $userData['ratings'] = $this->user_model->getratings();

        $userData['cdetails'] = $this->user_model->getratecounsilordetails($c_id);
        $userData['udetails'] = $this->user_model->getratecounsilordetails($u_id);
        //print_r($userData['udetails']);exit;
        // $userData['codes'] = $this->user_model->getCouponCodes();
        $this->load->view('rating', $userData);
    }

    public function insert_ratings() {

        //echo 'tutr';exit;
        $data_ratng = $this->db->get_where('ey_ratings', array('c_id' => $_POST['c_id'], 'user_id' => $_POST['u_id'], 'schedule_id' => $_POST['s_id'])  )->result();
        if (empty($data_ratng)) {
            
            $c_name = $_POST['c_name'];
            $c_id = $_POST['c_id'];
            $u_name = $_POST['u_name'];
            $u_id = $_POST['u_id'];
            $comment = $_POST['comment'];
            $cstatus = $_POST['cstatus'];
            $cstart = $_POST['cstart'];
            $callstart = $_POST['callstart'];
            // echo $coupon;exit;
            $s_id = $_POST['s_id'];
            $data = array(
                'c_name' => $c_name,
                'c_id' => $c_id,
                'u_name' => $u_name,
                'user_id' => $u_id,
                'comment' => $comment,
                'cstatus' => $cstatus,
                'schedule_id' => $s_id,
                'time_entered' => date('y-m-d'),
                'comment_count' => $cstart,
                'call_count' => $callstart,
            );
            //print_r($data);exit;
            if ($this->user_model->insert_ratings($data)) {
                //echo 'sducah';exit;
                $userData['ratings'] = $this->user_model->getratings();
                $this->load->view('list_ratings', $userData);

            //   echo "a";
            }
        }
        else{
            echo 2;
        }



        /* $c_id=53;
          $u_id=66;

          $userData['cdetails'] = $this->user_model->getratecounsilordetails($c_id);
          $userData['udetails'] = $this->user_model->getratecounsilordetails($u_id);

          // $userData['codes'] = $this->user_model->getCouponCodes();
          $this->load->view('rating', $userData);

         */
    }

    public function add_ratings() {

        $userData['count'] = $_POST['count'];

        if ($userData['count'] < 6) {

            $this->load->view('add_ratings', $userData);
        } else if ($userData['count'] > 5) {

            $this->load->view('add_ratings1', $userData);
        }
    }

    public function searchissues() {

//print_r($_POST['domain1']);exit;

        $this->session->set_userdata('page', 'reg');
        $userData['logoutUrl'] = $this->facebook->logout_url();
        $userData['oauthURL'] = base_url() . $this->config->item('linkedin_redirect_url') . '?oauth_init=1';
        $userData['authUrl'] = $this->facebook->login_url();
        $userData['loginURL'] = $this->google->loginURL();
        //  $userData['data'] = $this->user_model->fetchUser();
        //  $this->load->library('pagination');
        //$config['base_url'] = 'Rfetch1_controller/index/';
        $limit = 4;
        @$offset = $this->uri->segment(3);
        if (!is_null(@$offset)) {
            $offset = $this->uri->segment(3);
        }
        $filterOpts = $this->session->userdata('filterOpts');
        $filterOpts1 = $this->session->userdata('filterOpts1');
        $this->load->library('pagination');
        $userData['count'] = $this->user_model->fetchcount();
        //  echo count($userData['count']);exit;
        $config['uri_segment'] = 3;
        $config['base_url'] = site_url('home/schedule');
        $config['total_rows'] = count($userData['count']);
        $config['per_page'] = $limit;
        $config['num_links'] = 5;
        $config['first_link'] = 'First';
        $config['last_link'] = 'Last';
        $config['full_tag_open'] = "<ul class='pagination pagination-job page-nav'>";
        $config['full_tag_close'] = '</ul>';
        $config['num_tag_open'] = '<li class="page-numbers">';
        $config['num_tag_close'] = '</li>';
        $config['cur_tag_open'] = '<li class="active page-numbers" data-paged="1"><a href="#">';
        $config['cur_tag_close'] = '</a></li>';
        $config['prev_tag_open'] = '<li>';
        $config['prev_tag_close'] = '</li>';
        $config['first_tag_open'] = '<li>';
        $config['first_tag_close'] = '</li>';
        $config['last_tag_open'] = '<li>';
        $config['last_tag_close'] = '</li>';
        $config['prev_link'] = '<span aria-label="Previous"><span aria-hidden="true"><i class="fa fa-angle-left"></i></span></span>';
        $config['prev_tag_open'] = '<li class="disabled page-numbers">';
        $config['prev_tag_close'] = '</li>';


        $config['next_link'] = '<span aria-hidden="true"><i class="fa fa-angle-right"></i></span>';
        $config['next_tag_open'] = '<li class="page-numbers" data-paged="2">';
        $config['next_tag_close'] = '</li>';
        $this->pagination->initialize($config);

        if (empty($filterOpts)) {
            $filterOpts = "";
        }
        if (empty($filterOpts1)) {
            $filterOpts1 = "";
        }


        if ($_POST['domain1'] == 'All Categories') {

            $userData['data'] = $this->user_model->get_counselor1($filterOpts, $filterOpts1, @$limit, @$offset);
            //print_r($userData);exit;
            if ($this->session->userdata('loggedIn') == true) {
                $userid = $this->session->userdata('userData');

                $userData['data1'] = $this->user_model->fetch_user($userid->id);
            }
        } else {
            $filterOpts1 = $_POST['domain1'];
            $filterOpts1 = $this->session->set_userdata('filterOpts1', $filterOpts1);
            //  $userData['data'] = $this->user_model->get_counselor1($filterOpts,$filterOpts1,@$limit, @$offset);
        }


        echo 1;
    }

    public function getCounsRenum() {
        $user_id = $_POST['url'];
        $day = $_POST['day'];
        //echo $day;
       // echo $user_id;exit;
        $data_ren = $this->user_model->getCounsRenumeration($user_id);
        $data = $this->user_model->get_counstime($user_id, $day);
        //echo strtolower($date('l'));
        //print_r($data);
        if (!empty($data_ren)) {
            $timeSlot['duration'] = $data_ren[0]['minutes'];
            
        } else {
            $timeSlot['duration'] = '45';  // split by 30 mins
        }
        echo $timeSlot['duration'] . "-" . $data[0]->working_hours;
    }
    
    public function getCounsRenumQuick(){
        
        $user_id = $_POST['url'];
        //$day = $_POST['day'];
        //echo $day;
        // echo $user_id;exit;
        $data_ren = $this->user_model->getCounsRenumeration($user_id);
        $data = $this->user_model->get_counstime_today($user_id);
        //echo strtolower($date('l'));
        //print_r($data);
        if (!empty($data_ren)) {
            $timeSlot['duration'] = $data_ren[0]['minutes'];
            
        } else {
            $timeSlot['duration'] = '45';  // split by 30 mins
        }
        echo $timeSlot['duration'] . "-" . $data[0]->working_hours;
    }

    public function setUserId() {
        $code = $this->input->post('id');
        $userid = $this->session->set_userdata('counselor_id', $code);


        echo 0;
    }
    
    public function webinar(){

        $this->load->view('webinar');
    }
    
    /*public function test_call(){
        
        $exotel_sid = "eyogi"; // Your Exotel SID - Get it from here: http://my.exotel.in/settings/site#api-settings
        $exotel_token = "fb83c4d9b235b90d0e3b8b1194c942de489b003f"; // Your exotel token - Get it from here: http://my.exotel.in/settings/site#api-settings
        $sid = "36e2b3364dd6f4dad1a84287886899fa";

        $url = "https://" . $exotel_sid . ":" . $exotel_token . "@twilix.exotel.in/v1/Accounts/" . $exotel_sid . "/Calls/" . $sid . ".json";

        $ch = curl_init();
        curl_setopt($ch, CURLOPT_VERBOSE, 1);
        curl_setopt($ch, CURLOPT_URL, $url);

        curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
        curl_setopt($ch, CURLOPT_FAILONERROR, 0);
        curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, 0);
        curl_setopt($ch, CURLOPT_HTTPHEADER, array(
            'Content-Type: application/json',
            'Accept: application/json'
        ));

        $http_result = curl_exec($ch);
        $error = curl_error($ch);
        $http_code = curl_getinfo($ch, CURLINFO_HTTP_CODE);

        curl_close($ch);
        $data = json_decode($http_result);
        print_r($data);
        
    }*/
    
}
