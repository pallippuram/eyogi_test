<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class Calendar extends CI_Controller {

    function __construct() {
        // Call the Model constructor
        parent::__construct();
        $this->load->model('user_model');
        $this->load->model('Calendar_model');
    }

    /* Home page Calendar view  */

    Public function index() {
        $this->load->view('home');
    }

    /* Get all Events */

    Public function getEvents() {

        $result = $this->Calendar_model->getEvents();

        echo json_encode($result);
    }

    /* Add new event */

    Public function addEvent() {

        // print_r($_POST);exit;
        //echo $_POST['user_id'];exit;

        $amt = $this->user_model->getamt();
        $code = $this->user_model->getcoupon();
        //print_r($code);
        //echo $code;
        if ($code >= $amt) {
            // print_r($_POST);exit;
            /*  $result = $this->Calendar_model->addEvent();
              echo $result; */
            
            $phone = $_POST['sche_phone'];
            if($phone==""){
                
                $whitelist_status = "Neutral";
            }else{
                $whitelist_status = $this->checkWhitelist($phone);
            }
              //print_r($whitelist_status);exit;
            if ($whitelist_status == "Neutral" || $whitelist_status == "Whitelist") {

                if ($result = $this->Calendar_model->addEvent()) {

                    $visitorinf  = $_POST['user_id'];
                    $counsilid   = $_POST['con_id'];
                    $schedule_id = $_POST['schedule_id'];
                    $user_phone  = $_POST['sche_phone'];
                    $video_mail  = $_POST['videocall'];
                    
                    if (is_numeric($visitorinf) && strlen((string) $visitorinf) < 6) {

                        $visitordet = $this->Calendar_model->getvisitordetailUser($visitorinf);

                        $visitordet = json_decode(json_encode($visitordet), True);

                        $visitordet[0]['visiter_info'] = $visitordet[0]['email'];
                    } else {
                        $visitordet = $this->Calendar_model->getvisitordetail($visitorinf);
                        $visitordet = json_decode(json_encode($visitordet), True);
                        $visitordet[0]['visiter_info'] = $visitordet[0]['visiter_info'];
                    }


                   // $scheduledet = $this->Calendar_model->getscheduledetail($schedule_id);
                    $counsildet = $this->Calendar_model->getcounsildetail($counsilid);

                    // $visitordet = json_decode(json_encode($visitordet), True);
                 //   $scheduledet = json_decode(json_encode($scheduledet), True);
                    $counsildet = json_decode(json_encode($counsildet), True);
                    $counselor_name = $counsildet[0]['username'];
                    
                    $start=date("Y-m-d", strtotime($this->input->post('start')));
                    
                    $scedulertimestarts = date("h:i A", strtotime($this->input->post('start')));
                    $scedulertimeends = date("h:i A", strtotime($this->input->post('end')));

//              echo  $counsilorphn=$counsildet[0]['phone'];
//             echo   $counsilormail=$counsildet[0]['email'];
//echo $visitordet[0]['email']; exit;
                    if($user_phone == "") {
                        $visitordet = $this->Calendar_model->getvisitordetailUser($visitorinf);
                        $visitordet = json_decode(json_encode($visitordet), True);
                        $visitordet[0]['phone'] = $visitordet[0]['phone'];
                    }
                    else {
                        $visitordet[0]['phone'] = $user_phone;
                    }
                    
                    if (is_numeric(trim($visitordet[0]['phone']))) {
                        
                        /* SMS to user */
                        $phone = $visitordet[0]['phone'];
                        $ph = "$phone";
                    
                        $post_data = array(
                        // 'From' doesn't matter; For transactional, this will be replaced with your SenderId;
                        // For promotional, this will be ignored by the SMS gateway
                            'From' => '08039534067',
                            'To' => $ph,
                            'Body' => 'Dear user, your appointment has been scheduled with eYogi on ' . $start . ' starts from ' . $scedulertimestarts . ' to ' . $scedulertimeends . ' . Regards eYogi Team',
                            //'Body' => 'OTP for verifying your mobile with eYogi is ' . $pin . '', //Incase you are wondering who Dr. Rajasekhar is http://en.wikipedia.org/wiki/Dr._Rajasekhar_(actor)
                        );

                        $exotel_sid = "eyogi"; // Your Exotel SID - Get it from here: http://my.exotel.in/settings/site#api-settings
                        $exotel_token = "fb83c4d9b235b90d0e3b8b1194c942de489b003f"; // Your exotel token - Get it from here: http://my.exotel.in/settings/site#api-settings

                        $url = "https://" . $exotel_sid . ":" . $exotel_token . "@twilix.exotel.in/v1/Accounts/" . $exotel_sid . "/Sms/send";

                        $ch = curl_init();
                        curl_setopt($ch, CURLOPT_VERBOSE, 1);
                        curl_setopt($ch, CURLOPT_URL, $url);
                        curl_setopt($ch, CURLOPT_POST, 1);
                        curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
                        curl_setopt($ch, CURLOPT_FAILONERROR, 0);
                        curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, 0);
                        curl_setopt($ch, CURLOPT_POSTFIELDS, http_build_query($post_data));
                        /*   $data = array(
                                'phone' => $phone,
                                'code' => $pin,
                                'is_expired' => 0,
                                'created_at' => date("Y-m-d H:i:s"),
                            );

                            $this->user_model->insertOTPDetails($data); */
                        $http_result = curl_exec($ch);
                        $error = curl_error($ch);
                        $http_code = curl_getinfo($ch, CURLINFO_HTTP_CODE);

                        curl_close($ch);
                        
                        /* SMS to counselor */
                        
                        $phn = $counsildet[0]['phone'];
                        // print_r($visitordet[0]);exit;
                        // $counsilorphn
                        $phone = $phn;
                        $ph = "$phone";
                        // $pin = $this->generatePIN(4);
                        $post_data = array(
                            // 'From' doesn't matter; For transactional, this will be replaced with your SenderId;
                            // For promotional, this will be ignored by the SMS gateway
                                'From' => '08039534067',
                                'To' => $ph,
                                'Body' => 'Dear user, your appointment has been scheduled with eYogi on ' . $start . ' starts from ' . $scedulertimestarts . ' to ' . $scedulertimeends . ' . Regards eYogi Team',
                            //'Body' => 'OTP for verifying your mobile with eYogi is ' . $pin . '', //Incase you are wondering who Dr. Rajasekhar is http://en.wikipedia.org/wiki/Dr._Rajasekhar_(actor)
                        );

                        $exotel_sid = "eyogi"; // Your Exotel SID - Get it from here: http://my.exotel.in/settings/site#api-settings
                        $exotel_token = "fb83c4d9b235b90d0e3b8b1194c942de489b003f"; // Your exotel token - Get it from here: http://my.exotel.in/settings/site#api-settings

                        $url = "https://" . $exotel_sid . ":" . $exotel_token . "@twilix.exotel.in/v1/Accounts/" . $exotel_sid . "/Sms/send";

                        $ch = curl_init();
                        curl_setopt($ch, CURLOPT_VERBOSE, 1);
                        curl_setopt($ch, CURLOPT_URL, $url);
                        curl_setopt($ch, CURLOPT_POST, 1);
                        curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
                        curl_setopt($ch, CURLOPT_FAILONERROR, 0);
                        curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, 0);
                        curl_setopt($ch, CURLOPT_POSTFIELDS, http_build_query($post_data));
                            /*   $data = array(
                                    'phone' => $phone,
                                    'code' => $pin,
                                    'is_expired' => 0,
                                    'created_at' => date("Y-m-d H:i:s"),
                                );

                                $this->user_model->insertOTPDetails($data); */
                        $http_result = curl_exec($ch);
                        $error = curl_error($ch);
                        $http_code = curl_getinfo($ch, CURLINFO_HTTP_CODE);

                        curl_close($ch);
                        
                        
                        // Mail to eyogi_counselors
                    
                        $this->load->library('email');
                        //$counsilormail
                        $to = "keerthi@sectorqube.com";
                        $subject = "Scheduled Details";

                        $message = '<html><body>';
                        $message .= '<h3>Dear user, </h3></br>';
                        $message .= '<h3>Your appointment details with eYogi is as follows :</h3></br>';
                        $message .= '<table rules="all" style="border-color: #666;" cellpadding="10">';
                        $message .= "<tr style='background: #eee;'><td><strong>Counsellor:</strong> </td><td>" . $counselor_name . "</td></tr>";
                        $message .= "<tr style='background: #eee;'><td><strong>Date:</strong> </td><td>" . $start . "</td></tr>";
                        $message .= "<tr style='background: #eee;'><td><strong>From:</strong> </td><td>" . $scedulertimestarts . "</td></tr>";
                        $message .= "<tr style='background: #eee;'><td><strong>Till:</strong> </td><td>" . $scedulertimeends . "</td></tr>";

                        $message .= "</table>";
                        $message .= "</body></html>";



                        $headers = "From: info@eyogi.in" . "\r\n" . "Reply-To: info@eyogi.in" . "\r\n" . "Return-Path: info@eyogi.in" . "\r\n" .
                            'X-Mailer: PHP/' . phpversion() . "\r\n" .
                            'Content-Type: text/html; charset=ISO-8859-1' . "\r\n" .
                            'MIME-Version: 1.0' . "\r\n\r\n";


                        // $headers = "From: ".$_POST['emailaddress']."" . "\r\n" .
                        //  "CC: bimalsasidharan@gmail.com";
                        mail($to, $subject, $message, $headers);
                        
                        if($video_mail == 1){
                            
                            $to = 'keerthi@sectorqube.com';
                            $subject = "eYogi - Video Schedule details";

                            $message = '<html><body>';
                            $message .= '<h3>Dear user, Your appointment details with eYogi is as follows :</h3></br>';
                            $message .= '<table rules="all" style="border-color: #666;" cellpadding="10">';
                            $message .= "<tr style='background: #eee;'><td><strong>Counselor:</strong> </td><td>" . $counselor_name . "</td></tr>";
                            $message .= "<tr style='background: #eee;'><td><strong>Visitor-info:</strong> </td><td>" . $visitordet[0]['phone'] . "</td></tr>";
                            $message .= "<tr style='background: #eee;'><td><strong>Date:</strong> </td><td>" . $start . "</td></tr>";
                            $message .= "<tr style='background: #eee;'><td><strong>From:</strong> </td><td>" . $scedulertimestarts . "</td></tr>";
                            $message .= "<tr style='background: #eee;'><td><strong>Till:</strong> </td><td>" . $scedulertimeends . "</td></tr>";

                            $message .= "</table>";
                            $message .= "<p>If you face any issues, please contact us at info@eyogi.in</p>";
                            $message .= "</body></html>";



                            $headers = "From: info@eyogi.in" . "\r\n" . "Reply-To: info@eyogi.in" . "\r\n" . "Return-Path: info@eyogi.in" . "\r\n" .
                                'X-Mailer: PHP/' . phpversion() . "\r\n" .
                                'Bcc: info@eyogi.in' . "\r\n" .
                                'Content-Type: text/html; charset=ISO-8859-1' . "\r\n" .
                                'MIME-Version: 1.0' . "\r\n\r\n";


                            // $headers = "From: ".$_POST['emailaddress']."" . "\r\n" .
                            //  "CC: bimalsasidharan@gmail.com";
                            mail($to, $subject, $message, $headers);
                            
                        }
                        
                    }
                
                    echo $result;
                }
            } else {
                echo 123;
            }
        } else {
            echo 2;
        }
    }
    

    /* Update Event */

    Public function updateEvent() {
        $result = $this->Calendar_model->updateEvent();
        echo $result;
    }

    /* Reschedule Event */

    Public function rescheduleEvent() {
        $result = $this->Calendar_model->rescheduleEvent();
        echo $result;
    }

    /* Delete Event */

    Public function deleteEvent() {
        $result = $this->Calendar_model->deleteEvent();
        echo $result;
    }

    Public function dragUpdateEvent() {

        $result = $this->Calendar_model->dragUpdateEvent();
        echo $result;
    }

    public function checkScheduleValid() {
        $result = $this->Calendar_model->checkSchedule();
        $schedule = $this->user_model->getSchedule($result->counselor_id);
        
        $now = strtotime('now');
        $time = strtotime($schedule->time_starts);

        $difference = round(abs($now - $time) / 3600, 2);
        echo $difference;
    }
    
    public function checkWhitelist($ph) {
        
        $phone = $ph;
        $number = "$phone";
        
        $exotel_sid = "eyogi"; // Your Exotel SID - Get it from here: http://my.exotel.in/settings/site#api-settings
        $exotel_token = "fb83c4d9b235b90d0e3b8b1194c942de489b003f"; // Your exotel token - Get it from here: http://my.exotel.in/settings/site#api-settings
       //$phone = "8547966500";
       
        //$url = "https://" . $exotel_sid . ":" . $exotel_token . "@api.exotel.com/v1/Accounts/" . $exotel_sid . "/CustomerWhitelist.json";
        $url = "https://" . $exotel_sid . ":" . $exotel_token . "@api.exotel.com/v1/Accounts/" . $exotel_sid . "/CustomerWhitelist/" . $number . ".json";
       
        //Initialize cURL.
        $ch = curl_init();

        //Set the URL that you want to GET by using the CURLOPT_URL option.
        curl_setopt($ch, CURLOPT_URL, $url);

        //Set CURLOPT_RETURNTRANSFER so that the content is returned as a variable.
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);

        //Set CURLOPT_FOLLOWLOCATION to true to follow redirects.
        curl_setopt($ch, CURLOPT_FOLLOWLOCATION, true);

        //Execute the request.
        $data = curl_exec($ch);

        //Close the cURL handle.
        curl_close($ch);

        //Print the data out onto the page.
        $data = json_decode($data);


        //print_r($data);exit;
        return $data->Result->Status;
        // return 0;
    }
    
    Public function checkPhone(){
        
        $phone = $this->input->post('phone');
        //echo $phone;
        $number = "$phone";
        
        $exotel_sid = "eyogi"; // Your Exotel SID - Get it from here: http://my.exotel.in/settings/site#api-settings
        $exotel_token = "fb83c4d9b235b90d0e3b8b1194c942de489b003f"; // Your exotel token - Get it from here: http://my.exotel.in/settings/site#api-settings
       //$phone = "8547966500";
       
       //$url = "https://" . $exotel_sid . ":" . $exotel_token . "@api.exotel.com/v1/Accounts/" . $exotel_sid . "/CustomerWhitelist.json";
       $url = "https://" . $exotel_sid . ":" . $exotel_token . "@api.exotel.com/v1/Accounts/" . $exotel_sid . "/CustomerWhitelist/" . $number . ".json";
       
        //Initialize cURL.
        $ch = curl_init();

        //Set the URL that you want to GET by using the CURLOPT_URL option.
        curl_setopt($ch, CURLOPT_URL, $url);

        //Set CURLOPT_RETURNTRANSFER so that the content is returned as a variable.
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);

        //Set CURLOPT_FOLLOWLOCATION to true to follow redirects.
        curl_setopt($ch, CURLOPT_FOLLOWLOCATION, true);

        //Execute the request.
        $data = curl_exec($ch);

        //Close the cURL handle.
        curl_close($ch);

        //Print the data out onto the page.
        $data = json_decode($data);


        //print_r($data);exit;
        if($data->Result->Status == "Neutral" || $data->Result->Status == "Whitelist")
        echo 1;
        
        else
        echo 0;
        
    }
    Public function checkTimeEnds() {
        
        $end=$this->input->post('end');
        $start=$this->input->post('start');
        $result = $this->Calendar_model->checkTimeEnds($end,$start);
       
        if(empty($result)){
            echo 1;
        }else{
            echo 0;
        }
    }
}
