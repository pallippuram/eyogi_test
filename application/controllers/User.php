<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class User extends CI_Controller {

    /**
     * Index Page for this controller.
     *
     * Maps to the following URL
     * 		http://example.com/welcome
     * 	- or -
     * 		http://example.com/welcome/index
     * 	- or -
     * Since this controller is set as the default controller in
     * config/routes.php, it's displayed at http://example.com/
     *
     * So any other public methods not prefixed with an underscore will
     * map to /welcome/<method_name>
     * @see https://codeigniter.com/user_guide/general/urls.html
     */
    public function __construct() {
        parent::__construct();
        $this->load->library('google');
        $this->load->model('user_model');
        // Load linkedin config
        $this->load->config('linkedin');
        $this->load->library('facebook');
        date_default_timezone_set('Asia/Kolkata');
    }

    public function index() {
        $userData = array();
        if ($this->session->userdata('loggedIn') == true) {
            redirect('admin/index/');
        }
        //Include the linkedin api php libraries
        include_once APPPATH . "libraries/linkedin-oauth-client/http.php";
        include_once APPPATH . "libraries/linkedin-oauth-client/oauth_client.php";


        //Get status and user info from session
        $oauthStatus = $this->session->userdata('oauth_status');
        $sessUserData = $this->session->userdata('userData');

        if (isset($oauthStatus) && $oauthStatus == 'verified') {
            //User info from session
            $userData = $sessUserData;
        } else if ((isset($_REQUEST["oauth_init"]) && $_REQUEST["oauth_init"] == 1) || (isset($_REQUEST['oauth_token']) && isset($_REQUEST['oauth_verifier']))) {
            $client = new oauth_client_class;
            $client->client_id = $this->config->item('linkedin_api_key');
            $client->client_secret = $this->config->item('linkedin_api_secret');
            $client->redirect_uri = base_url() . $this->config->item('linkedin_redirect_url');
            $client->scope = $this->config->item('linkedin_scope');
            $client->debug = false;
            $client->debug_http = true;
            $application_line = __LINE__;

            //If authentication returns success
            if ($success = $client->Initialize()) {
                if (($success = $client->Process())) {
                    if (strlen($client->authorization_error)) {
                        $client->error = $client->authorization_error;
                        $success = false;
                    } elseif (strlen($client->access_token)) {
                        $success = $client->CallAPI('http://api.linkedin.com/v1/people/~:(id,email-address,first-name,last-name,location,picture-url,public-profile-url,formatted-name)', 'GET', array('format' => 'json'), array('FailOnAccessError' => true), $userInfo);
                    }
                }
                $success = $client->Finalize($success);
            }

            if ($client->exit)
                exit;

            if ($success) {
                //Preparing data for database insertion
                $first_name = !empty($userInfo->firstName) ? $userInfo->firstName : '';
                $last_name = !empty($userInfo->lastName) ? $userInfo->lastName : '';
                $picture_url = !empty($userInfo->pictureUrl) ? $userInfo->pictureUrl : '';

                // Preparing data for database insertion
                $userData['oauth_provider'] = 'linkedin';
                $userData['oauth_uid'] = $userInfo->id;
                $userData['first_name'] = $first_name;
                $userData['last_name'] = $last_name;

                $userData['email'] = $userInfo->emailAddress;
                $userData['locale'] = $userInfo->location->name;
                $userData['profile_url'] = $userInfo->publicProfileUrl;
                $userData['picture_url'] = $picture_url;
                //Insert or update user data
                $userID = $this->user_model->checkUser($userData);
                if ($userID == 0) {
                    $this->session->set_userdata('page', 'reg');

                    $data['logoutUrl'] = $this->facebook->logout_url();
                    $data['oauthURL'] = base_url() . $this->config->item('linkedin_redirect_url') . '?oauth_init=1';
                    $data['authUrl'] = $this->facebook->login_url();
                    $data['userData'] = $userData;
                    $data['loginURL'] = $this->google->loginURL();
                    $data['email'] = "";
                    $data['login'] = 0;
                    $data['msg'] = "User already registerd with this email id!!! Please login";
                    // Load login & profile view
                    $this->load->view('user/register', $data);
                } else {
                    //Store status and user profile info into session
//                $this->session->set_userdata('oauth_status', 'verified');
//                $this->session->set_userdata('userData', $userData);


                    $userData['logoutUrl'] = $this->facebook->logout_url();
                    $userData['oauthURL'] = base_url() . $this->config->item('linkedin_redirect_url') . '?oauth_init=1';
                    $userData['authUrl'] = $this->facebook->login_url();
                    $userData['loginURL'] = $this->google->loginURL();

                    $this->session->set_userdata('userData', $userData);
                    if (!empty($userID)) {
                        $data['userData'] = $userData;
                        $this->session->set_userdata('userData', $userData);
                    } else {
                        $data['userData'] = array();
                    }
                    $page = $this->session->userdata('page');
                    $data = $this->user_model->getLoggedUser($userInfo->emailAddress, $userInfo->id);

                    $this->session->set_userdata('userData', $data);
                    $this->session->set_userdata('loggedIn', true);
                    if ($data->role_id == 4) {

                        redirect('home/schedule');
                    } elseif ($data->role_id == 3) {
                        $created = $data->created_at;
                        $then = strtotime($created);
                        $now = time();

                        $difference = $now - $then;

//Convert seconds into days.
                        $days = floor($difference / (60 * 60 * 24));

                        if ($days == 0) {

                            $to = $userInfo->emailAddress;
                            $subject = "Update your profile - eYogi";
                            $message = '';
                            $message .= '<strong>Please login and update your profile details to list your profile in eYogi schedule section</strong><br>';


                            $headers = "From: info@eyogi.in" . "\r\n" ."Reply-To: info@eyogi.in" . "\r\n" ."Return-Path: info@eyogi.in" . "\r\n" .
                                    'X$headers-Mailer: PHP/' . phpversion() . "\r\n" .
                                    'Content-Type: text/html; charset=ISO-8859-1' . "\r\n" .
                                    'MIME-Version: 1.0' . "\r\n\r\n";


                            // $headers = "From: ".$_POST['emailaddress']."" . "\r\n" .
                            //  "CC: bimalsasidharan@gmail.com";
                            mail($to, $subject, $message, $headers);
                        }
                        redirect('counselor/index');
                    }
                }
            } else {
                $data['error_msg'] = 'Some problem occurred, please try again later!';
            }
        } else if ($this->facebook->is_authenticated()) {
            $arr = $this->facebook->is_authenticated();

            if (is_array($arr)) {
                $this->session->unset_userdata('loggedIn');
                $this->session->unset_userdata('userData');
                $page = $this->session->userdata('page');

                if ($page == "reg") {
                    redirect('user/index');
                } else {
                    redirect('user/joinus');
                }
            }

            // Get user facebook profile details
            $userProfile = $this->facebook->request('get', '/me?fields=id,first_name,last_name,email,gender,locale,picture');

            // Preparing data for database insertion
            $userData['oauth_provider'] = 'facebook';
            $userData['oauth_uid'] = $userProfile['id'];
            $userData['first_name'] = $userProfile['first_name'];
            $userData['last_name'] = $userProfile['last_name'];
            $userData['email'] = $userProfile['email'];
            $userData['gender'] = $userProfile['gender'];
            $userData['locale'] = $userProfile['locale'];
            $userData['profile_url'] = 'https://www.facebook.com/' . $userProfile['id'];
            $userData['picture_url'] = $userProfile['picture']['data']['url'];

            // Insert or update user data
            $userID = $this->user_model->checkUser($userData);

            if ($userID == 0) {
                $this->session->set_userdata('page', 'reg');

                $data['logoutUrl'] = $this->facebook->logout_url();
                $data['oauthURL'] = base_url() . $this->config->item('linkedin_redirect_url') . '?oauth_init=1';
                $data['authUrl'] = $this->facebook->login_url();
                $data['userData'] = $userData;
                $data['loginURL'] = $this->google->loginURL();
                $data['email'] = "";
                $data['login'] = 0;
                $data['msg'] = "User already registerd with this email id!!! Please login";
                // Load login & profile view
                $this->load->view('user/register', $data);
            } else {
                $this->session->set_userdata('userData', $userData);
                // Check user data insert or update status
                if (!empty($userID)) {
                    $data['userData'] = $userData;
                    $this->session->set_userdata('userData', $userData);
                } else {
                    $data['userData'] = array();
                }

                $userData['logoutUrl'] = $this->facebook->logout_url();
                $userData['oauthURL'] = base_url() . $this->config->item('linkedin_redirect_url') . '?oauth_init=1';
                $userData['authUrl'] = $this->facebook->login_url();
                $userData['loginURL'] = $this->google->loginURL();
                $page = $this->session->userdata('page');

                $data = $this->user_model->getLoggedUser($userProfile['email'], $userProfile['id']);

                if (!empty($data)) {
                    $this->session->set_userdata('userData', $data);
                    $this->session->set_userdata('loggedIn', true);
                    if ($data->role_id == 4) {

                        redirect('home/schedule');
                    } elseif ($data->role_id == 3) {
                        $created = $data->created_at;
                        $then = strtotime($created);
                        $now = time();

                        $difference = $now - $then;

//Convert seconds into days.
                        $days = floor($difference / (60 * 60 * 24));

                        if ($days == 0) {
                            $to = $userProfile['email'];
                            $subject = "Update your profile - eYogi";
                            $message = '';
                            $message .= '<strong>Please login and update your profile details to list your profile in eYogi schedule section</strong><br>';


                            $headers = "From: info@eyogi.in" . "\r\n" ."Reply-To: info@eyogi.in" . "\r\n" ."Return-Path: info@eyogi.in" . "\r\n" .
                                    'X$headers-Mailer: PHP/' . phpversion() . "\r\n" .
                                    'Content-Type: text/html; charset=ISO-8859-1' . "\r\n" .
                                    'MIME-Version: 1.0' . "\r\n\r\n";


                            // $headers = "From: ".$_POST['emailaddress']."" . "\r\n" .
                            //  "CC: bimalsasidharan@gmail.com";
                            mail($to, $subject, $message, $headers);
                        }
                        redirect('counselor/index');
                    }
                } else {
                    redirect('/');
                }
            }
        } else if (isset($_GET['code'])) {
            //authenticate user


            $this->google->getAuthenticate();

            //get user info from google
            $gpInfo = $this->google->getUserInfo();
            
            //preparing data for database insertion
            $userData['oauth_provider'] = 'google';
            $userData['oauth_uid'] = $gpInfo['id'];
            $userData['first_name'] = $gpInfo['given_name'];
            $userData['last_name'] = $gpInfo['family_name'];
            $userData['email'] = $gpInfo['email'];
            $userData['gender'] = !empty($gpInfo['gender']) ? $gpInfo['gender'] : '';
            $userData['locale'] = !empty($gpInfo['locale']) ? $gpInfo['locale'] : '';
            $userData['profile_url'] = !empty($gpInfo['link']) ? $gpInfo['link'] : '';
            $userData['picture_url'] = !empty($gpInfo['picture']) ? $gpInfo['picture'] : '';

            //insert or update user data to the database
            $userID = $this->user_model->checkUser($userData);
            if ($userID == 0) {
                $this->session->set_userdata('page', 'reg');

                $data['logoutUrl'] = $this->facebook->logout_url();
                $data['oauthURL'] = base_url() . $this->config->item('linkedin_redirect_url') . '?oauth_init=1';
                $data['authUrl'] = $this->facebook->login_url();
                $data['userData'] = $userData;
                $data['loginURL'] = $this->google->loginURL();
                $data['email'] = "";
                $data['login'] = 0;
                $data['msg'] = "User already registerd with this email id!!! Please login";
                // Load login & profile view
                $this->load->view('user/register', $data);
            } else {
                $this->session->set_userdata('userData', $userData);

                if (!empty($userID)) {
                    $data['userData'] = $userData;
                    $this->session->set_userdata('userData', $userData);
                } else {
                    $data['userData'] = array();
                }
                //store status & user info in session
                // print_r($userData['first_name']);exit;
                $userData['logoutUrl'] = $this->facebook->logout_url();
                $userData['oauthURL'] = base_url() . $this->config->item('linkedin_redirect_url') . '?oauth_init=1';
                $userData['authUrl'] = $this->facebook->login_url();
                $userData['loginURL'] = $this->google->loginURL();
                $page = $this->session->userdata('page');

//            if (isset($page)) {
//                $this->load->view('joinus', $userData);
//            } elseif(isset($reg)) {
//                $this->load->view('user/register', $userData);
//            }else{
//                redirect('admin/myprofile');
//            } 
                $data = $this->user_model->getLoggedUser($gpInfo['email'], $gpInfo['id']);


                if ($data->role_id == 4) {
                    $this->session->set_userdata('userData', $data);
                    $this->session->set_userdata('loggedIn', true);
                    redirect('home/schedule');
                } elseif ($data->role_id == 3) {
                    $this->session->set_userdata('userData', $data);
                    $this->session->set_userdata('loggedIn', true);
                    $created = $data->created_at;
                    $then = strtotime($created);
                    $now = time();

                    $difference = $now - $then;

//Convert seconds into days.
                    $days = floor($difference / (60 * 60 * 24));

                    if ($days == 0) {
                        $to = $gpInfo['email'];
                        $subject = "Update your profile - eYogi";
                        $message = '';
                        $message .= '<strong>Please login and update your profile details to list your profile in eYogi schedule section</strong><br>';


                        $headers = "From: info@eyogi.in" . "\r\n" ."Reply-To: info@eyogi.in" . "\r\n" ."Return-Path: info@eyogi.in" . "\r\n" .
                                'X$headers-Mailer: PHP/' . phpversion() . "\r\n" .
                                'Content-Type: text/html; charset=ISO-8859-1' . "\r\n" .
                                'MIME-Version: 1.0' . "\r\n\r\n";


                        // $headers = "From: ".$_POST['emailaddress']."" . "\r\n" .
                        //  "CC: bimalsasidharan@gmail.com";
                        mail($to, $subject, $message, $headers);
                    }
                    redirect('counselor/index');
                }
            }
        } else if (isset($_GET['error'])) {

            $this->session->unset_userdata('loggedIn');
            $this->session->unset_userdata('userData');

            redirect('user/index');
        } else {
            $this->session->unset_userdata('login');
            $this->session->unset_userdata('loggedIn');
            $this->session->unset_userdata('userData');
            $this->session->set_userdata('page', 'reg');

            $data['logoutUrl'] = $this->facebook->logout_url();
            $data['oauthURL'] = base_url() . $this->config->item('linkedin_redirect_url') . '?oauth_init=1';
            $data['authUrl'] = $this->facebook->login_url();
            $data['userData'] = $userData;
            $data['loginURL'] = $this->google->loginURL();
            $data['email'] = "";
            $data['login'] = 0;
            $data['msg'] = "";
            // Load login & profile view
            $this->load->view('user/register', $data);
        }
        
        
        
    }

    public function login() {


        $email = $this->input->post('email');
        $password = $this->input->post('password');
        $data = $this->user_model->check_login($email, $password);

        if ($data != false) {
            $this->session->set_userdata('loggedIn', true);
            $this->session->set_userdata('userData', $data);


            if ($this->session->userdata('page_url')) {
                echo $this->session->userdata('page_url');
            } else {
                echo 1;
            }
        } else {
            echo 0;
        }
    }

    public function register() {
        //set validation rules
        $this->form_validation->set_rules('username', 'User Name', 'trim|required|max_length[30]');
        $this->form_validation->set_rules('email', 'Email ID', 'trim|required|valid_email');
        $this->form_validation->set_rules('password', 'Password', 'trim|required|matches[conpassword]');
        $this->form_validation->set_rules('conpassword', 'Confirm Password', 'trim|required');

        //validate form input
        if ($this->form_validation->run() == FALSE) {
            // fails
            redirect(site_url() . 'user/index');
            
        } else {



            if ($this->input->post('role_id') == 4) {

                //insert the user registration details into database
                $data = array(
                    'oauth_uid' => $this->input->post('oauth_uid'),
                    'username' => $this->input->post('username'),
                    'email' => $this->input->post('email'),
                    'password' => md5($this->input->post('password')),
                    'phone' => $this->input->post('phone'),
                    'age' => $this->input->post('age'),
                    'gender' => $this->input->post('gender'),
                    'qualification' => $this->input->post('qualification'),
                    'employed' => $this->input->post('employed'),
                    'marital_status' => $this->input->post('marital_status'),
                    'kids' => $this->input->post('kids'),
                    'role_id' => $this->input->post('role_id'),
                    'created_at' => date("Y-m-d H:i:s")
                );
                $data_user = $this->user_model->check_login1($this->input->post('email'), $this->input->post('password'));
           
         if ($data_user > 0) {
                $this->session->set_userdata('page', 'reg');

                $data['logoutUrl'] = $this->facebook->logout_url();
                $data['oauthURL'] = base_url() . $this->config->item('linkedin_redirect_url') . '?oauth_init=1';
                $data['authUrl'] = $this->facebook->login_url();
                $data['userData'] = $data;
                $data['loginURL'] = $this->google->loginURL();
                $data['email'] = "";
                $data['login'] = 0;
                $data['msg'] = "User already registerd with this email id!!! Please login";
                // Load login & profile view
                $this->load->view('user/register', $data);
            } else {
                
                 // insert form data into database
                if ($this->user_model->insertUser($data)) {
                    $data = $this->user_model->check_login($this->input->post('email'), $this->input->post('password'));
                    $this->session->set_userdata('userData', $data);
                    $this->session->set_userdata('loggedIn', true);
                    redirect('home/schedule');
                } else {
                    // error
                    $this->session->set_flashdata('msg', '<div class="alert alert-danger text-center">Oops! Error.  Please try again later!!!</div>');
                    redirect('user/register');
                }
            }
               
            } else {


                if (!empty($_FILES['image']['name'])) {

                    $config['upload_path'] = 'uploads/images/';
                    $config['allowed_types'] = 'jpg|jpeg|png|gif';
                    $config['file_name'] = $_FILES['image']['name'];

                    //Load upload library and initialize configuration
                    $this->load->library('upload', $config);
                    $this->upload->initialize($config);

                    if ($this->upload->do_upload('image')) {
                        $uploadData = $this->upload->data();
                        $picture = base_url().'uploads/images/'.$uploadData['file_name'];
                    } else {
                        $picture = '';
                    }
                } else {
                    $picture = '';
                }

                $data = array(
                    'username' => $this->input->post('username'),
                    'email' => $this->input->post('email'),
                    'password' => md5($this->input->post('password')),
                    'photo' => $picture,
                    'created_at' => date("Y-m-d H:i:s"),
                    'role_id' => $this->input->post('role_id'),
                );
                $data1 = array(
                    'domain' => $this->input->post('domain'),
                    'experience' => $this->input->post('experience'),
                    'languages' => json_encode($this->input->post('languages')),
                    'certifications' => $this->input->post('certifications'),
                    'accrediations' => $this->input->post('accrediations'),
                );
                $data_user = $this->user_model->check_login1($this->input->post('email'), $this->input->post('password'));
           
         if ($data_user > 0) {
                $this->session->set_userdata('page', 'reg');

                $data['logoutUrl'] = $this->facebook->logout_url();
                $data['oauthURL'] = base_url() . $this->config->item('linkedin_redirect_url') . '?oauth_init=1';
                $data['authUrl'] = $this->facebook->login_url();
                $data['userData'] = $userData;
                $data['loginURL'] = $this->google->loginURL();
                $data['email'] = "";
                $data['login'] = 0;
                $data['msg'] = "User already registerd with this email id!!! Please login";
                // Load login & profile view
                $this->load->view('joinus', $data);
            } else {
                 if ($this->user_model->insertCounselor($data, $data1)) {
                    $data = $this->user_model->check_login($this->input->post('email'), md5($this->input->post('password')));
                    $this->session->set_userdata('userData', $data);
                    redirect('admin/index');
                }
            }

                // insert form data into database
               
            }
        }
    }

    public function joinus() {
        $this->session->unset_userdata('loggedIn');
        $this->session->unset_userdata('userData');
        $userData['logoutUrl'] = $this->facebook->logout_url();
        $userData['oauthURL'] = base_url() . $this->config->item('linkedin_redirect_url') . '?oauth_init=1';
        $userData['authUrl'] = $this->facebook->login_url();
        $userData['loginURL'] = $this->google->loginURL();
        $userData['msg']="";
        $this->session->unset_userdata('login');
        $this->session->set_userdata('page', 'joinus');
        $userid = $this->session->userdata('userData');
        if (!isset($userid->id)) {
            $userData['data1'] = $this->user_model->fetch_user_data($userid['oauth_uid']);
        } else {
            $userData['data1'] = $this->user_model->fetch_user($userid->id);
        }
        $userData['counselling_types'] = $this->user_model->getCounsellingTypes();
        $this->load->view('joinus', $userData);
    }

    public function logout() {
        //delete login status & user info from session
        $this->session->unset_userdata('loggedIn');
        $this->session->unset_userdata('userData');
        $this->session->sess_destroy();

        //redirect to login page
        redirect('/');
    }

    public function profile() {
        //redirect to login page if user not logged in
        $this->session->unset_userdata('login');
        $data = $this->session->userdata('userData');
        $userData['logoutUrl'] = $this->facebook->logout_url();
        $userData['oauthURL'] = base_url() . $this->config->item('linkedin_redirect_url') . '?oauth_init=1';
        $userData['authUrl'] = $this->facebook->login_url();
        $userData['loginURL'] = $this->google->loginURL();
        $userData['data'] = $this->user_model->fetchUser();
        $this->session->set_userdata('page', 'joinus');
        $this->data['data'] = $data;
        $userid = $this->session->userdata('userData');
        if (!isset($userid->id)) {
            $userData['data1'] = $this->user_model->fetch_user_data($userid['oauth_uid']);
        } else {
            $userData['data1'] = $this->user_model->fetch_user($userid->id);
        }

        if ($data->role_id == 1) {
            redirect(site_url() . 'admin/index', $userData, $data);
            //$this->load->view('admin/userhome', $userData, $data);
        } else if ($data->role_id == 3) {
            redirect(site_url() . 'counselor/index', $userData, $data);
        } else {
            redirect('admin/myprofile');
        }
    }

     public function get_counselor() {
        $limit = 4;
        @$offset = $this->uri->segment(3);
        if (!is_null(@$offset)) {
            $offset = $this->uri->segment(3);
        }
        $select = 'SELECT ey.id, ey.username,ey.photo,ey.video_call,cn.domain,cn.experience,eyr.amount,eyr.minutes';
        $from = ' FROM ey_user ey JOIN ey_counsellor_expertise cn ON ey.id=cn.user_id join ey_counselor_renumeration eyr on ey.id=eyr.user_id ';
        $where = ' WHERE ey.role_id=3 and ey.status="approve" and ';



        if (empty($_POST['filterOpts'])) {
            // 0 checkboxes checked
            $this->session->set_userdata('filterOpts', "");
            $where .= 'true';
        } else {
            $this->session->set_userdata('filterOpts', $_POST['filterOpts']);
            $opts1 = $_POST['filterOpts'];
            $opts = explode(',', $opts1);

            $where .= '(';
            if (count($opts) == 1) {
                // 1 checkbox checked
                $where .= '  cn.languages like "%' . $opts[0] . '%"';
            } else {
                // 2+ checkboxes checked
                //$options=explode(',',$opts);
                //  $options= json_decode($opts);

                for ($i = 0; $i < count($opts); $i++) {
                    $where .= 'cn.languages like "%' . $opts[$i] . '%"';
                    if ($i < count($opts) - 1) {
                        $where .= ' OR ';
                    }
                }
            }
            $where .= ')';
        }

        $where .= ' and';
        if (empty($_POST['filterOpts1'])) {
            // 0 checkboxes checked
            $this->session->set_userdata('filterOpts1', "");
            $where .= ' true ';
        } else {
            $this->session->set_userdata('filterOpts1', $_POST['filterOpts1']);
            $opts1 = $_POST['filterOpts1'];
            $opts = explode(',', $opts1);
            $where .= '(';
            if (count($opts) == 1) {
                // 1 checkbox checked

                if ($opts[0] == 1) {
                     $where .= '  cn.domain like "%' . $opts[0] . '%"';
                } else {
                    for ($i = 0; $i < count($opts); $i++) {
                            $where .= 'cn.domain like "%' . $opts[$i] . '%"';
                            if ($i < count($opts) - 1) {
                                $where .= ' OR ';
                            }
                        }
                }
            } else {
                // 2+ checkboxes checked
                //$options=explode(',',$opts);
                //  $options= json_decode($opts);

                for ($i = 0; $i < count($opts); $i++) {
                    if ($opts[$i] == 1) {
                        $where .= '  cn.domain like "%' . $opts[0] . '%"';
                    } else {
                        for ($i = 0; $i < count($opts); $i++) {
                            $where .= 'cn.domain like "%' . $opts[$i] . '%"';
                            if ($i < count($opts) - 1) {
                                $where .= ' OR ';
                            }
                        }
                    }


                    if ($i < count($opts) - 1) {
                        $where .= ' OR ';
                    }
                }
            }
            $where .= ')';
        }
        if ($offset != "") {
            $where .= ' limit ' . $limit . ',' . $offset . '';
        } else {
            $where .= ' limit ' . $limit . '';
        }

        $sql = $select . $from . $where;
        $query = $this->db->query($sql);
        $results = $query->result_array();
        $array_res = $query->result();
        //$this->session->set_userdata('res_value', $array_res);
        $json = json_encode($results);
        echo($json);
    }

    public function updatepassword() {

        $email = $this->input->post('email');
        $clean = $this->security->xss_clean($email);
        $userInfo = $this->user_model->getUserInfoByEmail($clean);

        if (!$userInfo) {
            $this->session->set_flashdata('flash_message', 'We cant find your email address');
            redirect(site_url() . 'home/lostpass');
        }

        

        //build token 
        $this->load->helper('string');

        $qstring = random_string('alnum', 20);
        $now = date('Y-m-d H:i:s');
        $data_arr = array(
            'user_id' => $userInfo->id,
            'token' => $qstring,
            'created_at' => $now
        );
        $token = $this->user_model->insertToken($data_arr);

        $url = site_url() . 'home/reset_password/token/' . $qstring;
        $link = '<a href="' . $url . '">' . $url . '</a>';
        $to = $email;
        $subject = "Reset Password";
        $message = '';
        $message .= '<strong>A password reset has been requested for this email account</strong><br>';
        $message .= '<strong>Please click:</strong> ' . $link;

        $headers = "From: info@eyogi.in" . "\r\n" ."Reply-To: info@eyogi.in" . "\r\n" ."Return-Path: info@eyogi.in" . "\r\n" .
                'X$headers-Mailer: PHP/' . phpversion() . "\r\n" .
                'Content-Type: text/html; charset=ISO-8859-1' . "\r\n" .
                'MIME-Version: 1.0' . "\r\n\r\n";


        // $headers = "From: ".$_POST['emailaddress']."" . "\r\n" .
        //  "CC: bimalsasidharan@gmail.com";
        mail($to, $subject, $message, $headers);
        $this->session->set_flashdata('flash_message', 'Reset password link has been sent to your mail id!!');
        redirect(site_url() . 'home/lostpass');
    }

    public function appointment_list() {
        $userid = $this->session->userdata('userData');
        if ($this->session->userdata('loggedIn') == true) {

            if ($userid->role_id == 4) {

                $this->session->unset_userdata('login');
                $data = $this->session->userdata('userData');
                $userData['logoutUrl'] = $this->facebook->logout_url();
                $userData['oauthURL'] = base_url() . $this->config->item('linkedin_redirect_url') . '?oauth_init=1';
                $userData['authUrl'] = $this->facebook->login_url();
                $userData['loginURL'] = $this->google->loginURL();
                $userData['data'] = $this->user_model->fetchUser();
                $this->session->set_userdata('page', 'joinus');
                $this->data['data'] = $data;
                $userid = $this->session->userdata('userData');
                $userData['data1'] = $this->user_model->fetch_user($userid->id);

                if ($data->role_id == 1) {

                    redirect(site_url() . 'admin/index', $userData, $data);
                    //$this->load->view('admin/userhome', $userData, $data);
                } else if ($data->role_id == 3) {

                    redirect(site_url() . 'counselor/index', $userData, $data);
                } else {
                    $uid = $userid->id;
                    $userData['data2'] = $this->user_model->get_appointment($uid);
                    //print_r($userData);exit;
                    $this->load->view('user/appointmentlist', $userData, $data);
                }
            } else {

                $this->output->set_status_header('404');
                $this->load->view('Error_page');
            }
        } else {
            $this->output->set_status_header('404');
            $this->load->view('Error_page');
        }
    }

    public function myaccount() {
        $userid = $this->session->userdata('userData');
 if ($this->session->userdata('loggedIn') == true) {
        if ($userid->role_id == 4) {
            $userData['logoutUrl'] = $this->facebook->logout_url();
            $userData['oauthURL'] = base_url() . $this->config->item('linkedin_redirect_url') . '?oauth_init=1';
            $userData['authUrl'] = $this->facebook->login_url();
            $userData['loginURL'] = $this->google->loginURL();
            $userData['data'] = $this->user_model->fetchUser();


            $userid = $this->session->userdata('userData');
            if (!isset($userid->id)) {
                $userData['data1'] = $this->user_model->fetch_user_data($userid['oauth_uid']);
            } else {
                $userData['data1'] = $this->user_model->fetch_user($userid->id);
            }

            $userData['payment'] = $this->user_model->fetchPayment($userid->id);
            //print_r($userid->id);exit;
            //print_r($userData['payment']);exit;
            $userData['call'] = $this->user_model->fetchCallDetails($userid->id);

//print_r($userData['call']);exit;
            $this->load->view('user/myaccount', $userData);
        } else {

            $this->output->set_status_header('404');
            $this->load->view('Error_page');
 }} else {

            $this->output->set_status_header('404');
            $this->load->view('Error_page');
 }
    }

    public function generate_code() {
        $phone = $this->input->post('phone');
        $ph = "$phone";
        $pin = $this->generatePIN(4);
        $post_data = array(
            // 'From' doesn't matter; For transactional, this will be replaced with your SenderId;
            // For promotional, this will be ignored by the SMS gateway
            'From' => '08039534067',
            'To' => $ph,
            'Body' => 'OTP for verifying your mobile with eYogi is ' . $pin . '. Regards eYogi team', //Incase you are wondering who Dr. Rajasekhar is http://en.wikipedia.org/wiki/Dr._Rajasekhar_(actor)
        );

        $exotel_sid = "eyogi"; // Your Exotel SID - Get it from here: http://my.exotel.in/settings/site#api-settings
        $exotel_token = "fb83c4d9b235b90d0e3b8b1194c942de489b003f"; // Your exotel token - Get it from here: http://my.exotel.in/settings/site#api-settings

        $url = "https://" . $exotel_sid . ":" . $exotel_token . "@twilix.exotel.in/v1/Accounts/" . $exotel_sid . "/Sms/send";

        $ch = curl_init();
        curl_setopt($ch, CURLOPT_VERBOSE, 1);
        curl_setopt($ch, CURLOPT_URL, $url);
        curl_setopt($ch, CURLOPT_POST, 1);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
        curl_setopt($ch, CURLOPT_FAILONERROR, 0);
        curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, 0);
        curl_setopt($ch, CURLOPT_POSTFIELDS, http_build_query($post_data));
        $data = array(
            'phone' => $phone,
            'code' => $pin,
            'is_expired' => 0,
            'created_at' => date("Y-m-d H:i:s"),
        );

        $this->user_model->insertOTPDetails($data);
        $http_result = curl_exec($ch);
        $error = curl_error($ch);
        $http_code = curl_getinfo($ch, CURLINFO_HTTP_CODE);

        curl_close($ch);

        echo 1;
    }

    public function generatePIN($digits = 4) {
        $i = 0; //counter
        $pin = ""; //our default pin is blank.
        while ($i < $digits) {
            //generate a random number between 0 and 9.
            $pin .= mt_rand(0, 9);
            $i++;
        }
        return $pin;
    }

    public function validate_otp() {
        $phone = $this->input->post('phone');
        $otp = $this->input->post('otp');
        $res = $this->user_model->getOTP($phone, $otp);

        if (!empty($res)) {
            $from_time = strtotime($res->created_at);
            $to_time = strtotime(date("Y-m-d H:i:s"));
            $time = round(abs($to_time - $from_time) / 60, 2) . " minute";
            if ($res->is_expired == 0 && $time <= 5) {
                echo 1;
            } else {
                echo 0;
            }
        } else {
            echo 0;
        }
    }

}