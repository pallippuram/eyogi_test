<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class Counselor extends CI_Controller {

    public function __construct() {
        parent::__construct();
        $this->load->library('google');
        $this->load->model('user_model');
        // Load linkedin config
        $this->load->config('linkedin');
        $this->load->library('facebook');
        
        $this->load->helper('session_helper');
        
//        if(!is_logged_in())  // if you add in constructor no need write each function in above controller. 
//        {
//          redirect(site_url());
//        }
    }

    public function index() {

        $userid = $this->session->userdata('userData');

        if ($userid->role_id == 3) {

            $userData['logoutUrl'] = $this->facebook->logout_url();
            $userData['oauthURL'] = base_url() . $this->config->item('linkedin_redirect_url') . '?oauth_init=1';
            $userData['authUrl'] = $this->facebook->login_url();
            $userData['loginURL'] = $this->google->loginURL();

            $userid = $this->session->userdata('userData');

            $userData['data1'] = $this->user_model->fetch_user($userid->id);
            $userData['data'] = $this->user_model->getUser($userid->id);
            $userData['userDetails'] = $this->user_model->getUserList();

            $userData['schedule'] = $this->user_model->getScheduleDetails($userid->id);
            $schedule_ids = $this->user_model->getScheduleIds();
            
            if (!empty($schedule_ids)) {
                foreach ($schedule_ids as $id) {
                    $res[] = $id->schedule_id;
                }
            } else {
                $res[] = "";
            }
            $userData['ids'] = $res;
            $this->load->view('admin/schedule_calendar', $userData);
        } else {
            $this->output->set_status_header('404');
            $this->load->view('Error_page');
        }
    }

    public function myprofile() {
        //redirect to login page if user not logged in
        $this->session->unset_userdata('login');
        if ($this->session->userdata('loggedIn') == true){
            
            $userData['logoutUrl'] = $this->facebook->logout_url();
            $userData['oauthURL'] = base_url() . $this->config->item('linkedin_redirect_url') . '?oauth_init=1';
            $userData['authUrl'] = $this->facebook->login_url();
            $userData['loginURL'] = $this->google->loginURL();

            $userid = $this->session->userdata('userData');
            if (!isset($userid->id)) {
                $userData['data1'] = $this->user_model->fetch_user_data($userid['oauth_uid']);
            } else {
                $userData['data1'] = $this->user_model->fetch_user($userid->id);
            }

            $userData['data'] = $this->user_model->getUser($userid->id);
            $userData['coupon_details'] = $this->user_model->getStatus($userid->id);

            $this->load->view('admin/counselor_profile', $userData);
            
        } 
        else{
            $this->output->set_status_header('404');
            $this->load->view('Error_page');
        }
        
    }

    public function edit_profile() {
        
        if ($this->session->userdata('loggedIn') == true){
            
            $id = $this->uri->segment(3);
            $userData['logoutUrl'] = $this->facebook->logout_url();
            $userData['oauthURL'] = base_url() . $this->config->item('linkedin_redirect_url') . '?oauth_init=1';
            $userData['authUrl'] = $this->facebook->login_url();
            $userData['loginURL'] = $this->google->loginURL();
            $userid = $this->session->userdata('userData');
            $userData['email'] = "";
            $userData['login'] = 0;
            $userid = $this->session->userdata('userData');
            $userData['data1'] = $this->user_model->fetch_user($userid->id);

            $userData['data'] = $this->user_model->getUserCounselor($userid->id);
            //print_r($userData['data']);exit;
            if(empty($userData['data'])){
                $data1=$this->user_model->getUser1($userid->id);
                $data1->accrediations="";
                $data1->certifications="";
                $data1->experience="";
                $data1->user_id=$data1->id;
                $userData['data'] =$data1;
            }
            $userData['userDetails'] = $this->user_model->getUserdata($id,$sche_id="");
            $userData['counselling_types'] = $this->user_model->getCounsellingTypes();
            //print_r($userData['userDetails']);exit;
            $this->load->view('admin/counselor_editprofile', $userData);
        }
        else{
            $this->output->set_status_header('404');
            $this->load->view('Error_page');
        }
        
    }
    
    // bimal
 
    public function update_counselor() {
        // echo $_FILES['image']['name'];exit;
        // print_r($_POST);exit;
        if (!empty($_FILES['image']['name'])) {

            $config['upload_path'] = 'uploads/images/';
            $config['allowed_types'] = 'jpg|jpeg|png|gif';
            $config['file_name'] = $_FILES['image']['name'];

            //Load upload library and initialize configuration
            $this->load->library('upload', $config);
            $this->upload->initialize($config);

            if ($this->upload->do_upload('image')) {
                $uploadData = $this->upload->data();
                $picture = base_url().'uploads/images/'.$uploadData['file_name'];
            } else {
                $picture = '';
            }
            $data = array(
                'username' => $this->input->post('username'),
                'email' => $this->input->post('email'),
                'photo' => $picture,
                'phone' => $this->input->post('phone'),
                'gender' => $this->input->post('gender'),
                'created_at' => date("Y-m-d H:i:s")
            );
            $data1 = array(
                'domain' => json_encode($this->input->post('domain')),
                'experience' => $this->input->post('experience'),
                'languages' => json_encode($this->input->post('languages')),
                'certifications' => $this->input->post('certifications'),
                'accrediations' => $this->input->post('accrediations'),
            );
        } else {

            $data = array(
                'username' => $this->input->post('username'),
                'email' => $this->input->post('email'),
                //'photo' => $picture,
                'phone' => $this->input->post('phone'),
                'gender' => $this->input->post('gender'),
                'created_at' => date("Y-m-d H:i:s")
            );
            $data1 = array(
                'domain' => json_encode($this->input->post('domain')),
                'experience' => $this->input->post('experience'),
                'languages' => json_encode($this->input->post('languages')),
                'certifications' => $this->input->post('certifications'),
                'accrediations' => $this->input->post('accrediations'),
            );
        }
       
        $id = $this->input->post('id_hidden');


        // insert form data into database
        if ($this->user_model->updateCounsel($id, $data, $data1)) {

//                $email=$this->input->post('email');
//                $htmlContent = '<h1>eYogi Login Credentials</h1>';
//                $htmlContent .= '<p>Please use the following credentials to login to eYogi.</p>';
//                $htmlContent .= '<p>Username : '.$this->input->post('email').'</p>';
//                $htmlContent .= '<p>Password : '.$this->input->post('password').'</p>';
//
//                $config['mailtype'] = 'html';
//                $this->email->initialize($config);
//                $this->email->to($email);
//                $this->email->from('admin@eyogi.in', 'eYogi');
//                $this->email->subject('Counselor Login Details');
//                $this->email->message($htmlContent);
//                $this->email->send();

            redirect('counselor/myprofile');
        }
    }

    public function counselor_appointments() {
        
        $userid = $this->session->userdata('userData');
        if ($this->session->userdata('loggedIn') == true){
            
            if ($userid->role_id == 3) {
                $this->session->unset_userdata('login');
                $data = $this->session->userdata('userData');
                $userData['logoutUrl'] = $this->facebook->logout_url();
                $userData['oauthURL'] = base_url() . $this->config->item('linkedin_redirect_url') . '?oauth_init=1';
                $userData['authUrl'] = $this->facebook->login_url();
                $userData['loginURL'] = $this->google->loginURL();
                $userData['data'] = $this->user_model->fetchUser();
                $this->session->set_userdata('page', 'joinus');
                $this->data['data'] = $data;
                $userid = $this->session->userdata('userData');
                $userData['data1'] = $this->user_model->fetch_user($userid->id);

                if ($data->role_id == 1) {

                    redirect(site_url() . 'admin/index', $userData, $data);
                    //$this->load->view('admin/userhome', $userData, $data);
                } else if ($data->role_id == 4) {

                    redirect(site_url() . 'user/index', $userData, $data);
                } else {

                    $uid = $userid->id;
                    $userData['data2'] = $this->user_model->counselor_appointment($uid);

//                  echo '<pre>';
//                  print_r($userData);exit;
                    $this->load->view('user/appointmentlist', $userData);
                }
            } else {

                $this->output->set_status_header('404');
                $this->load->view('Error_page');
            }
            
        }
        else{
            $this->output->set_status_header('404');
            $this->load->view('Error_page');
        }
    }
    
    public function testmail()
    {
        
        $this->load->library('email');
        $to = 'keerthi.sectorqube@gmail.com';

        $subject = 'Eyogi';

        $headers = "From: info@eyogi.in " . "\r\n";
        $headers .= "Reply-To: keerthi.sectorqube@gmail.com " . "\r\n";
        //$headers .= "CC: praseetha.sectorqube@gmail.com\r\n";
        $headers .= "MIME-Version: 1.0\r\n";
        $headers .= "Content-Type: text/html; charset=UTF-8\r\n";

        $message = '<html>
            <head>
            <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
                <title></title>
            <style>
                #email-wrap {
                background: #303C42;
                color: #FFF;
                margin-left:200px;
                height: 400px;
                width: 50%;
                border-radius:15px;
                }
            </style>
            </head>
            <img class="main-logo" style="margin-left:350px;" src="http://dev.eyogi.in/assets/images/logo.png" alt="eYogi">
            <body>
            <div id="email-wrap">
                <h2 style="line-height:80px;margin-bottom:50px;margin:0 50px 50px">Hi keerthi,</h2> 
                <h4><center>Your Appointment has been scheduled on following day </center></h4>
                <table rules="all" style="border-color:#FFFFFF;margin:20px 210px 0px;" cellpadding="15">
                
                    <tr style=""><td><strong>Date:</strong> </td><td> 3/22/2018 </td></tr>
                    
                    <tr style=""><td><strong>From:</strong> </td><td> 9:00 AM  </td></tr>
                    
                    <tr style=""><td><strong>Till:</strong> </td><td> 10:00 AM </td></tr>
                    
                </table>
                
                <p style="padding-left:50px" >Regards,</p>
                <p style="padding-left:50px">Eyogi teams</p>
               
                

            </div>
            </body>
            </html>';
        mail($to, $subject, $message, $headers);
        
       
    }
    public function counselor_addnotes() { 
        
        $userid = $this->session->userdata('userData');
        if ($this->session->userdata('loggedIn') == true){
            $userData['data1'] = $this->user_model->fetch_user($userid->id);
        }
        $sche_id = urldecode(base64_decode($this->uri->segment(3))); 
        $date = urldecode(base64_decode($this->uri->segment(4)));
        
        $userData['logoutUrl'] = $this->facebook->logout_url();
        $userData['oauthURL'] = base_url() . $this->config->item('linkedin_redirect_url') . '?oauth_init=1';
        $userData['authUrl'] = $this->facebook->login_url();
        
        $userData['counselling_types'] = $this->user_model->getCounsellingTypes();
        
        $userData['details'] = $this->user_model->getAppointment($sche_id, $date);
        $userData['user'] = new StdClass;
        if(is_numeric($userData['details']->visitor_info)){
             $userData['user']=$this->user_model->getUser1($userData['details']->visitor_info);
        }else{
            $userData['user']->username="Anonymous User";
        }
        $userData['notes'] = $this->user_model->getNotes($sche_id);
        $this->load->view('user/addnotes', $userData);
   
//        }
//        else{
//            $this->output->set_status_header('404');
//            $this->load->view('Error_page');
//        }
    } 
     
      public function counselor_insertnotes() {

       //  print_r($_POST);exit;

//        $domain = array( );
//
//   for($k=0;$k<count($_POST['domain']);$k++){
//
//            $domain[]=implode(",",$_POST['domain'][$k]);
//
//        } 
//
//        $str="";
//         for($k=0;$k<count($_POST['domain']);$k++){
//        $str .= $_POST['domain'][$k] . ",";
//        }
//        $str = trim($str, ',');

      

   // print_r($domain);exit;        
        
        $name = $_POST['name'];
      // $domain = $_POST['domain'];
        $notes = $_POST['notes'];
        $user_id = $_POST['user_id1'];

        $data = array(
            'name' => $name,
            'sche_id' => $user_id,
            'issues' => json_encode($this->input->post('domain')),
            'notes' => $notes,
            'time_entered' => date('Y-m-d')
        );

         if ($this->user_model->insert_notes($data)) {

             $userData['notes'] = $this->user_model->getNotes($user_id);
             $this->load->view('user/listnotes', $userData);

         }
    }

}
