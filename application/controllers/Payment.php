<?php

defined('BASEPATH') OR exit('No direct script access allowed');
require_once ('config.php');
require_once (APPPATH . 'libraries/razorpay-php/Razorpay.php');

use Razorpay\Api\Api;
use Razorpay\Api\Errors\SignatureVerificationError;

class Payment extends CI_Controller {

    public function __construct() {
        parent::__construct();
        $this->load->library('google');
        $this->load->model('user_model');
        $this->load->model('Calendar_model');
        $this->load->library('session');
        $this->load->config('linkedin');
        $this->load->library('facebook');
    }

    public function index() {


        /*$keyId = 'rzp_live_hpowfXpoLEJ4hJ';
        $keySecret = 'Kg0tIXNNT3r9ou8vo1diKRBg';*/
        $keyId = 'rzp_test_H0juQ2pLQaoTQt';
        $keySecret = '7FrwOTuB3q8z675KAfk88qmu';
        $displayCurrency = 'INR';
        $userid = $this->session->userdata('userData');

        $type=$this->input->post('ind_type'); 
        if($type=="user"){
            $user_info=$userid->id;
            $email =$userid->email;
            $phone =$userid->phone;
            $amount = $this->input->post('total_input');
            $org="";
        }
        else if($type=="anonymous"){
            
            $description = $this->input->post('descrptn');
            $date = $this->input->post('date');
            $start = $this->input->post('start');
            $end = $this->input->post('end');
            $schedule_id = $this->input->post('schedule_id');
            $video_call = $this->input->post('video_count');
            
            $counselor_id = $this->input->post('couns');
            $amount = $this->user_model->getConAmt($counselor_id);
            $phone = $this->input->post('user_ph');
            
            $arraydata = array(
                'description' => $description,
                'date'        => $date,
                'start'       => $start,
                'end'         => $end,
                'schedule_id' => $schedule_id,
                'videocall_enable'=> $video_call,
                'counselor_id'=> $counselor_id,
                'phone' => $phone,
                'type' => $type,
            );
            $this->session->set_userdata($arraydata);
            //echo '<pre>';
            //print_r($this->session->userdata());exit;
            
            $user_info = $phone;
            $email = "";
            $org="";
            
            
        }
       else if($type==0){
            $amount = $this->input->post('ind_amt_hdn');
            $radio_email = $this->input->post('radio_email');

        $email = "";
        $phone = "";
        if ($radio_email == "email") {
            $user_info = $this->input->post('purachase_email');
            $email = $user_info;
        }

        if ($radio_email == "phone") {
            $user_info = $this->input->post('purachase_phone');
            $phone = $user_info;
        }
        
        $org="";
        
        }else{
            $amount = $this->input->post('org_amt_hdn');
            $org=$this->input->post('organization_input');
            $user_info = $this->input->post('organization_email');
            $email=$user_info;
            $phone = "";
        }

$qty_val = $this->input->post('qty_input');

        $api = new Api($keyId, $keySecret);

        $orderData = [
            'receipt' => 3456,
            'amount' => $qty_val*($amount * 100), // 2000 rupees in paise
            'currency' => 'INR',
            'payment_capture' => 1 // auto capture
        ];

        $razorpayOrder = $api->order->create($orderData);

        $razorpayOrderId = $razorpayOrder['id'];

        $_SESSION['razorpay_order_id'] = $razorpayOrderId;

        $displayAmount = $amount1 = $orderData['amount'];

        if ($displayCurrency !== 'INR') {
            $url = "https://api.fixer.io/latest?symbols=$displayCurrency&base=INR";
            $exchange = json_decode(file_get_contents($url), true);

            $displayAmount = $exchange['rates'][$displayCurrency] * $amount1 / 100;
        }

        $checkout = 'manual';


        $data = [
            "key" => $keyId,
            "amount" => $amount1,
            "name" => "eYogi",
            "description" => "Purchase Code",
            "image" => site_url() . "assets/images/logo.png",
            "prefill" => [
                "name" => "",
                "email" => $email,
                "contact" => $phone,
            ],
            "notes" => [
                "address" => "Hello World",
                "merchant_order_id" => "12312321",
            ],
            "theme" => [
                "color" => "#F37254"
            ],
            "order_id" => $razorpayOrderId,
        ];

        if ($displayCurrency !== 'INR') {
            $data['display_currency'] = $displayCurrency;
            $data['display_amount'] = $displayAmount;
        }
        $this->data['amount'] = $amount;
        $this->data['user_info'] = $user_info;
        $this->data['qty'] = $this->input->post('qty_input');
        $this->data['total'] = $this->input->post('total_input');
        
        if ($this->session->userdata('loggedIn') == true && $type=="user") {
             $userid = $this->session->userdata('userData');
             $coupon_bal=$this->user_model->getCouponBalance($userid->id); 
             
        }else{
            $coupon_bal=0;
        }
        
        for($i=0;$i<$this->input->post('qty_input');$i++){
            
             
        if(is_numeric($user_info)&&strlen((string)$user_info)<6){
             $token = $user_info; 
        }else{
            $token = $this->generateRandomString();
            $this->session->set_userdata('visitor_info', $token);
        }
        $coupondetails = array(
            'visiter_info' => $user_info,
            'coupon_amount' => $amount,
            'coupon_code' => $token,
            'type' => $this->input->post('ind_type'),
            'organization' => $org,
            'qty' => $this->input->post('qty_input'),
            'purchase_date' => date("Y-m-d H:i:s"),
            'order_id' => $razorpayOrderId,
            //'coupon_balance'=>$coupon_bal+$amount,
            'coupon_balance'=>$amount,
            'payment_status' => 'Pending'
        );
        

        
        $token_new = $this->user_model->insertCouponDetails($coupondetails);
        }
        // $json = json_encode($data);
        // print_r($data);exit;
        $this->data['data'] = $data;
        
        if ($this->session->userdata('loggedIn') == true) {
           

            if (!isset($userid->id)) {
                $this->data['data1'] = $this->user_model->fetch_user_data($userid['oauth_uid']);
            } else {
                $this->data['data1'] = $this->user_model->fetch_user($userid->id);
            }
        }
        
        
        
        $this->load->view('payment/manual', $this->data);
    }

    function generateRandomString($length = 6) {
        $characters = '0123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ';
        $charactersLength = strlen($characters);
        $randomString = '';
        for ($i = 0; $i < $length; $i++) {
            $randomString .= $characters[rand(0, $charactersLength - 1)];
        }
        return $randomString;
    }

    public function verify() {
        /*$keyId = 'rzp_live_hpowfXpoLEJ4hJ';
        $keySecret = 'Kg0tIXNNT3r9ou8vo1diKRBg';*/
        $keyId = 'rzp_test_H0juQ2pLQaoTQt';
        $keySecret = '7FrwOTuB3q8z675KAfk88qmu';
        $displayCurrency = 'INR';
        $success = true;

        $error = "Payment Failed";

        if (empty($_POST['razorpay_payment_id']) === false) {
            $api = new Api($keyId, $keySecret);

            try {
                // Please note that the razorpay order ID must
                // come from a trusted source (session here, but
                // could be database or something else)
                $attributes = array(
                    'razorpay_order_id' => $_SESSION['razorpay_order_id'],
                    'razorpay_payment_id' => $_POST['razorpay_payment_id'],
                    'razorpay_signature' => $_POST['razorpay_signature']
                );

                $api->utility->verifyPaymentSignature($attributes);
            } catch (SignatureVerificationError $e) {
                $success = false;
                $error = 'Razorpay Error : ' . $e->getMessage();
            }
        }

        $payment = $api->payment->fetch($_POST['razorpay_payment_id']);
        if ($this->session->userdata('loggedIn') == true) {
            $userid = $this->session->userdata('userData');

            if (!isset($userid->id)) {
                $this->data['data1'] = $this->user_model->fetch_user_data($userid['oauth_uid']);
            } else {
                $this->data['data1'] = $this->user_model->fetch_user($userid->id);
            }
        }
        $paymentData = array(
            'order_id' => $_SESSION['razorpay_order_id'],
            'payment_id' => $payment->id,
            'amount' => $payment->amount,
            'currency' => $payment->currency,
            'payment_status' => $payment->status,
            'invoice_id' => $payment->invoice_id,
            'method' => $payment->method,
            'bank' => $payment->bank,
            'description' => $payment->description,
            'email' => $payment->email,
            'contact' => $payment->contact,
            'error_code' => $payment->error_code,
            'error_description' => $payment->error_description,
            'created_at' => $payment->created_at,
            'amount_refunded' => $payment->amount_refunded,
            'refund_status' => $payment->refund_status,
        );
       
        $this->user_model->updatePaymentDetails($_SESSION['razorpay_order_id'], $payment->status, $paymentData);

           $visitor=$this->user_model->fetch_visitor_info($_SESSION['razorpay_order_id']);
           
           $coupon_details=$this->user_model->get_coupon_details($visitor->coupon_code);
           $data=array(
               'visitor_info'=>$visitor->coupon_code,
               'coupon_balance'=>$coupon_details,
               'payment_status'=>"Captured"
           );
           
           $this->user_model->insertCouponBalance($data);

        

        if ($success === true) {
            
            $sche_det = $this->session->userdata();
    
            if(!empty($sche_det)) {
                //echo $this->session->userdata('type');exit;   
                if(!empty($this->session->userdata('type'))){
                    //echo 'abc';exit;
                    if($this->session->userdata('type') == "anonymous") {
                    
                        if ($result = $this->Calendar_model->payEvent()) {
                    
                            $counsilid = $this->session->userdata('counselor_id');
                            $counsildet = $this->Calendar_model->getcounsildetail($counsilid);
                            $counsildet = json_decode(json_encode($counsildet), True);
                            $counselor_name = $counsildet[0]['username'];
                    
                            $start=date("Y-m-d", strtotime($this->session->userdata('start')));
                            $scedulertimestarts = date("h:i A", strtotime($this->session->userdata('start')));
                            $scedulertimeends = date("h:i A", strtotime($this->session->userdata('end')));
                    
                            if (is_numeric(trim($this->session->userdata('phone')))) {
                        
                                // SMS to user
                                $phone = $this->session->userdata('phone');
                                $ph = "$phone";
                    
                                $post_data = array(
                                // 'From' doesn't matter; For transactional, this will be replaced with your SenderId;
                                // For promotional, this will be ignored by the SMS gateway
                                    'From' => '08039534067',
                                    'To' => $ph,
                                    'Body' => 'Dear user, your appointment has been scheduled with eYogi on ' . $start . ' starts from ' . $scedulertimestarts . ' to ' . $scedulertimeends . ' . Regards eYogi Team',
                                //'Body' => 'OTP for verifying your mobile with eYogi is ' . $pin . '', //Incase you are wondering who Dr. Rajasekhar is http://en.wikipedia.org/wiki/Dr._Rajasekhar_(actor)
                                );

                                $exotel_sid = "eyogi"; // Your Exotel SID - Get it from here: http://my.exotel.in/settings/site#api-settings
                                $exotel_token = "fb83c4d9b235b90d0e3b8b1194c942de489b003f"; // Your exotel token - Get it from here: http://my.exotel.in/settings/site#api-settings

                                $url = "https://" . $exotel_sid . ":" . $exotel_token . "@twilix.exotel.in/v1/Accounts/" . $exotel_sid . "/Sms/send";

                                $ch = curl_init();
                                curl_setopt($ch, CURLOPT_VERBOSE, 1);
                                curl_setopt($ch, CURLOPT_URL, $url);
                                curl_setopt($ch, CURLOPT_POST, 1);
                                curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
                                curl_setopt($ch, CURLOPT_FAILONERROR, 0);
                                curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, 0);
                                curl_setopt($ch, CURLOPT_POSTFIELDS, http_build_query($post_data));
                                /*   $data = array(
                                    'phone' => $phone,
                                    'code' => $pin,
                                    'is_expired' => 0,
                                    'created_at' => date("Y-m-d H:i:s"),
                                );

                                $this->user_model->insertOTPDetails($data); */
                                $http_result = curl_exec($ch);
                                $error = curl_error($ch);
                                $http_code = curl_getinfo($ch, CURLINFO_HTTP_CODE);

                                curl_close($ch);
                        
                                // SMS to counselor
                                $phn = $counsildet[0]['phone'];
                                // print_r($visitordet[0]);exit;
                                // $counsilorphn
                                $phone = $phn;
                                $ph = "$phone";
                                // $pin = $this->generatePIN(4);
                                $post_data = array(
                                    // 'From' doesn't matter; For transactional, this will be replaced with your SenderId;
                                    // For promotional, this will be ignored by the SMS gateway
                                    'From' => '08039534067',
                                    'To' => $ph,
                                    'Body' => 'Dear user, your appointment has been scheduled with eYogi on ' . $start . ' starts from ' . $scedulertimestarts . ' to ' . $scedulertimeends . ' . Regards eYogi Team',
                                    //'Body' => 'OTP for verifying your mobile with eYogi is ' . $pin . '', //Incase you are wondering who Dr. Rajasekhar is http://en.wikipedia.org/wiki/Dr._Rajasekhar_(actor)
                                );

                                $exotel_sid = "eyogi"; // Your Exotel SID - Get it from here: http://my.exotel.in/settings/site#api-settings
                                $exotel_token = "fb83c4d9b235b90d0e3b8b1194c942de489b003f"; // Your exotel token - Get it from here: http://my.exotel.in/settings/site#api-settings

                                $url = "https://" . $exotel_sid . ":" . $exotel_token . "@twilix.exotel.in/v1/Accounts/" . $exotel_sid . "/Sms/send";

                                $ch = curl_init();
                                curl_setopt($ch, CURLOPT_VERBOSE, 1);
                                curl_setopt($ch, CURLOPT_URL, $url);
                                curl_setopt($ch, CURLOPT_POST, 1);
                                curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
                                curl_setopt($ch, CURLOPT_FAILONERROR, 0);
                                curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, 0);
                                curl_setopt($ch, CURLOPT_POSTFIELDS, http_build_query($post_data));
                                /*   $data = array(
                                    'phone' => $phone,
                                    'code' => $pin,
                                    'is_expired' => 0,
                                    'created_at' => date("Y-m-d H:i:s"),
                                );

                                $this->user_model->insertOTPDetails($data); */
                                $http_result = curl_exec($ch);
                                $error = curl_error($ch);
                                $http_code = curl_getinfo($ch, CURLINFO_HTTP_CODE);

                                curl_close($ch);
                        
                        
                                // Mail to eyogi_counselors
                    
                                $this->load->library('email');
                                //$counsilormail
                                $to = "bimal@sectorqube.com";
                                $subject = "Scheduled Details";

                                $message = '<html><body>';

                                $message .= '<table rules="all" style="border-color: #666;" cellpadding="10">';
                                $message .= "<tr style='background: #eee;'><td><strong>Counsellor:</strong> </td><td>" . $counselor_name . "</td></tr>";
                                $message .= "<tr style='background: #eee;'><td><strong>Date:</strong> </td><td>" . $start . "</td></tr>";
                                $message .= "<tr style='background: #eee;'><td><strong>From:</strong> </td><td>" . $scedulertimestarts . "</td></tr>";
                                $message .= "<tr style='background: #eee;'><td><strong>Till:</strong> </td><td>" . $scedulertimeends . "</td></tr>";

                                $message .= "</table>";
                                $message .= "</body></html>";



                                $headers = "From: info@eyogi.in" . "\r\n" . "Reply-To: info@eyogi.in" . "\r\n" . "Return-Path: info@eyogi.in" . "\r\n" .
                                    'X-Mailer: PHP/' . phpversion() . "\r\n" .
                                    'Content-Type: text/html; charset=ISO-8859-1' . "\r\n" .
                                    'MIME-Version: 1.0' . "\r\n\r\n";


                                // $headers = "From: ".$_POST['emailaddress']."" . "\r\n" .
                                //  "CC: bimalsasidharan@gmail.com";
                                mail($to, $subject, $message, $headers);
                        
                            }
                        
                            $this->session->unset_userdata('description');
                            $this->session->unset_userdata('date');
                            $this->session->unset_userdata('start');
                            $this->session->unset_userdata('end');
                            $this->session->unset_userdata('schedule_id');
                            $this->session->unset_userdata('videocall_enable');
                            $this->session->unset_userdata('counselor_id');
                            $this->session->unset_userdata('phone');
                            $this->session->unset_userdata('type');
                            $this->session->unset_userdata('visitor_info');
                        }
                    }
                    $this->data[] = '';
                    $this->load->view('payment/schedule_success', $this->data);
                    
                }
                
                else {
                    //echo 'fghx';exit;
                    $tokenDetails = $this->user_model->getTokenDetails($_SESSION['razorpay_order_id']);

                    foreach ($tokenDetails as $token) {
                        $code[] = $token->coupon_code;
                    }

                    if (!empty($tokenDetails) && $tokenDetails[0]->payment_status == "Captured") {

                        if (filter_var($tokenDetails[0]->visiter_info, FILTER_VALIDATE_EMAIL)) {

                            $this->load->library('email');

                            $to = $tokenDetails[0]->visiter_info;
                            $subject = "Coupon Details";

                            $message = '<html><body>';
                            $message .= '<p>Your Coupon codes are ' . implode(",", $code) . '</p>';
                            $message .= "</body></html>";

                            $headers = "From: info@eyogi.in" . "\r\n" . "Reply-To: info@eyogi.in" . "\r\n" . "Return-Path: info@eyogi.in" . "\r\n" .
                                // 'CC:  bimal.sectorqube@gmail.com' . "\r\n" .
                                'X-Mailer: PHP/' . phpversion() . "\r\n" .
                                'Content-Type: text/html; charset=ISO-8859-1' . "\r\n" .
                                'MIME-Version: 1.0' . "\r\n\r\n";

                                // $headers = "From: ".$_POST['emailaddress']."" . "\r\n" .
                                //  "CC: bimalsasidharan@gmail.com";
                            mail($to, $subject, $message, $headers);
                        } else {

                            $phn = $tokenDetails[0]->visiter_info;
                            // print_r($visitordet[0]);exit;
                            // $counsilorphn
                            $phone = $phn;
                            $ph = "$phone";
                            // $pin = $this->generatePIN(4);
                            $post_data = array(
                                // 'From' doesn't matter; For transactional, this will be replaced with your SenderId;
                                // For promotional, this will be ignored by the SMS gateway
                                'From' => '08039534067',
                                'To' => $ph,
                                'Body' => 'Dear user, Your Coupon code is ' . implode(",", $code) . '. Regards eYogi team',
                                //'Body' => 'OTP for verifying your mobile with eYogi is ' . $pin . '', //Incase you are wondering who Dr. Rajasekhar is http://en.wikipedia.org/wiki/Dr._Rajasekhar_(actor)
                            );

                            $exotel_sid = "eyogi"; // Your Exotel SID - Get it from here: http://my.exotel.in/settings/site#api-settings
                            $exotel_token = "fb83c4d9b235b90d0e3b8b1194c942de489b003f"; // Your exotel token - Get it from here: http://my.exotel.in/settings/site#api-settings

                            $url = "https://" . $exotel_sid . ":" . $exotel_token . "@twilix.exotel.in/v1/Accounts/" . $exotel_sid . "/Sms/send";

                            $ch = curl_init();
                            curl_setopt($ch, CURLOPT_VERBOSE, 1);
                            curl_setopt($ch, CURLOPT_URL, $url);
                            curl_setopt($ch, CURLOPT_POST, 1);
                            curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
                            curl_setopt($ch, CURLOPT_FAILONERROR, 0);
                            curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, 0);
                            curl_setopt($ch, CURLOPT_POSTFIELDS, http_build_query($post_data));
                                /*   $data = array(
                                        'phone' => $phone,
                                        'code' => $pin,
                                        'is_expired' => 0,
                                        'created_at' => date("Y-m-d H:i:s"),
                                    );

                            $this->user_model->insertOTPDetails($data); */
                            $http_result = curl_exec($ch);
                            $error = curl_error($ch);
                            $http_code = curl_getinfo($ch, CURLINFO_HTTP_CODE);

                            curl_close($ch);

                            // print "Response = " . print_r($http_result);
                        }
                    }
                    $this->data[] = '';
                    $this->load->view('payment/payment_success', $this->data);
                }
            }        
        } 
        else {
            $html = "<p>Your payment failed</p>
            <p>{$error}</p>";
        } 
        //$this->data[] = '';
        //$this->load->view('payment/payment_success', $this->data);
    }

    public function getPayment() {
        /*$keyId = 'rzp_live_hpowfXpoLEJ4hJ';
        $keySecret = 'Kg0tIXNNT3r9ou8vo1diKRBg';*/
        $keyId = 'rzp_test_H0juQ2pLQaoTQt';
        $keySecret = '7FrwOTuB3q8z675KAfk88qmu';
        $displayCurrency = 'INR';

        $api = new Api($keyId, $keySecret);
        $payment = $api->payment->fetch('pay_99AC7e77atoqVb');
        echo '<pre>';
        //   print_r($payment);
        exit;
    }
    
    public function webinar_payment(){

        /*$keyId = 'rzp_live_hpowfXpoLEJ4hJ';
        $keySecret = 'Kg0tIXNNT3r9ou8vo1diKRBg';*/
        $keyId = 'rzp_test_H0juQ2pLQaoTQt';
        $keySecret = '7FrwOTuB3q8z675KAfk88qmu';
        $displayCurrency = 'INR';

        $amt = $this->input->post('amnt_radio');

        //echo $amt;exit;

        $api = new Api($keyId, $keySecret);
        
        $orderData = [
            'receipt' => 3456,
            'amount' => $amt * 100, // 2000 rupees in paise
            'currency' => 'INR',
            'payment_capture' => 1 // auto capture
        ];

        $razorpayOrder = $api->order->create($orderData);

        $razorpayOrderId = $razorpayOrder['id'];

        $_SESSION['razorpay_order_id'] = $razorpayOrderId;
        $displayAmount = $amount1 = $orderData['amount'];

        if ($displayCurrency !== 'INR') {
            $url = "https://api.fixer.io/latest?symbols=$displayCurrency&base=INR";
            $exchange = json_decode(file_get_contents($url), true);

            $displayAmount = $exchange['rates'][$displayCurrency] * $amount1 / 100;
        }

        $checkout = 'proceed_to_pay';


        $data = [
            "key" => $keyId,
            "amount" => $amount1,
            "name" => "eYogi",
            "description" => "Webinar",
            "image" => site_url() . "assets/images/logo.png",
            "prefill" => [
                "name" => "",
                "email" => "",
                "contact" => "",
            ],
            "notes" => [
                "address" => "Hello World",
                "merchant_order_id" => "12312321",
            ],
            "theme" => [
                "color" => "#F37254"
            ],
            "order_id" => $razorpayOrderId,
        ];
        if ($displayCurrency !== 'INR') {
            $data['display_currency'] = $displayCurrency;
            $data['display_amount'] = $displayAmount;
        }
        $this->data['amount'] = $amt;
        $this->data['data'] = $data;
        $this->load->view('proceed_to_pay', $this->data);
        //$this->data['amount'] =  $amt;

        //echo json_encode($data);
       // echo $this->data;
        
    }

    public function webinar_verify() {
        
        /*$keyId = 'rzp_live_hpowfXpoLEJ4hJ';
        $keySecret = 'Kg0tIXNNT3r9ou8vo1diKRBg';*/
        $keyId = 'rzp_test_H0juQ2pLQaoTQt';
        $keySecret = '7FrwOTuB3q8z675KAfk88qmu';
        $displayCurrency = 'INR';
        $success = true;

        $error = "Payment Failed";

        if (empty($_POST['razorpay_payment_id']) === false) {
            $api = new Api($keyId, $keySecret);

            try {
                // Please note that the razorpay order ID must
                // come from a trusted source (session here, but
                // could be database or something else)
                $attributes = array(
                    'razorpay_order_id' => $_SESSION['razorpay_order_id'],
                    'razorpay_payment_id' => $_POST['razorpay_payment_id'],
                    'razorpay_signature' => $_POST['razorpay_signature']
                );

                $api->utility->verifyPaymentSignature($attributes);
            } catch (SignatureVerificationError $e) {
                $success = false;
                $error = 'Razorpay Error : ' . $e->getMessage();
            }
        }

        $payment = $api->payment->fetch($_POST['razorpay_payment_id']);
        //print_r($payment);exit;
        /*if ($this->session->userdata('loggedIn') == true) {
            $userid = $this->session->userdata('userData');

            if (!isset($userid->id)) {
                $this->data['data1'] = $this->user_model->fetch_user_data($userid['oauth_uid']);
            } else {
                $this->data['data1'] = $this->user_model->fetch_user($userid->id);
            }
        }*/
        $paymentData = array(
            'order_id' => $payment->order_id,
            'payment_id' => $payment->id,
            'amount' => $payment->amount,
            'currency' => $payment->currency,
            'payment_status' => $payment->status,
            'method' => $payment->method,
            'bank' => $payment->bank,
            'description' => $payment->description,
            'email' => $payment->email,
            'contact' => $payment->contact,
            'created_at' => $payment->created_at,
            'amount_refunded' => $payment->amount_refunded,
            
        );
       //print_r($paymentData);exit;

        $detail = $this->user_model->insertWebinar($paymentData);
        //print_r($detail);exit;
        //$this->db->insert('seminar_payment', $paymentData);
        $this->load->view('success');   

                      
    }

}
