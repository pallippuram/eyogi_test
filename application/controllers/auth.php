<?php

defined('BASEPATH') OR exit('No direct script access allowed');
error_reporting(0);

class Welcome extends CI_Controller {

    public function __construct() {
        parent::__construct();
        $this->load->library('googleplus');
        $this->load->helper('url');
        $this->load->model('welcome_model', 'welcome');
    }

    public function index() {

        $this->load->view('login');
    }

    public function google_login() {
        if (isset($_GET['code'])) {
            $this->googleplus->getAuthenticate();
            $user = $this->googleplus->getUserInfo();
            $checkemail = $this->db->query('select id,email 
              from users where email = "' . $user["email"] . '"');
            $emailresult = $checkemail->result_array();
            if ($emailresult[0]['email'] != $user["email"]) {
                $user_information = array(
                    'name' => $user["name"],
                    'first_name' => $user["given_name"],
                    'last_name' => $user["family_name"],
                    'email' => $user["email"],
                    'gender' => $user["gender"],
                    'source_id' => $user["id"],
                    'source' => 'Google',
                    'profilepicture' => $user["picture"],
                );
                $this->welcome->insert_user($user_information);
                $insert_id = $this->db->insert_id();
                $fetchuser = $this->db->query('select * 
             from users where id = "' . $insert_id . '"');
                $userdata = $fetchuser->result_array();
                $this->session->set_userdata('user_id', $userdata[0]['id']);
                $this->session->set_userdata('user_name', $userdata[0]['name']);
                $this->session->set_userdata('user_email', $userdata[0]['email']);
                $this->session->set_userdata('user_gender', $userdata[0]['gender']);
                $this->session->set_userdata('user_source', $userdata[0]['source']);
                $this->session->set_userdata('user_source_id', $userdata[0]['source_id']);
            } else if ($emailresult[0]['email'] == $user["email"]) {
                $update_id = array('source_id' => $user["id"],
                    'source' => 'Google',
                    'profilepicture' => $user["picture"]);
                $this->db->where('id', $emailresult[0]['id']);
                $this->db->update('users', $update_id);
                $fetchuser = $this->db->query('select * 
             from users where id = "' . $emailresult[0]['id'] . '"');
                $userdata = $fetchuser->result_array();
                $this->session->set_userdata('user_id', $userdata[0]['id']);
                $this->session->set_userdata('user_name', $userdata[0]['name']);
                $this->session->set_userdata('user_email', $userdata[0]['email']);
                $this->session->set_userdata('user_gender', $userdata[0]['gender']);
                $this->session->set_userdata('user_source', $userdata[0]['source']);
                $this->session->set_userdata('user_source_id', $userdata[0]['source_id']);
            }

            $data['userprofile'] = $this->session->userdata();
            redirect('welcome/profile', $data);
        } else {
            echo 'We are unable fetch your google information.';
            exit;
        }
    }

    public function profile() {
        if ($this->session->userdata('user_id') != '') {
            $this->data['userprofile'] = $this->welcome->fetch_user();
            $this->load->view('profile', $this->data, FALSE);
        } else {
            redirect('welcome', $data);
        }
    }

    public function logout() {
        $this->session->sess_destroy();
        redirect(base_url(), redirect);
    }

}