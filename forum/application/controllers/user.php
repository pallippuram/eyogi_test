<?php

class User extends CI_Controller {
    public $data = array();
    
    public function __construct() {
        parent::__construct();
        $this->load->model('user_model');
        $this->user_model->check_role();
    }

    public function index()
    {
        if ($this->session->userdata('cibb_logged_in') == true){
            $userid = $this->session->userdata('cibb_user_id');
            $this->data['data1'] = $this->user_model->fetch_user_details($userid);

        }
        //print_r($this->data['data1']);exit;
        $this->data['title']   = 'User Index :: '.CIBB_TITLE;
        $this->load->view('header', $this->data);
        $this->load->view('main');
        $this->load->view('footer');
    }

    public function login(){
        if ($this->session->userdata('cibb_logged_in') == true){
            redirect('/');
        }
        else {
            $this->data['title']   = 'User Index :: '.CIBB_TITLE;
            $this->load->view('header', $this->data);
            $this->load->view('user/join');
            $this->load->view('footer');
        }
    }
    
    public function join()
    {
        $email = $this->input->post('email');
        $password = $this->input->post('password');
        $data = $this->user_model->check_login($email, $password);

        if ($data != false) {
            $this->session->set_userdata('cibb_logged_in', 1);
            $this->session->set_userdata('cibbData', $data);
            $this->session->set_userdata('cibb_user_id'  , $data->id);
            $this->session->set_userdata('cibb_username' , $data->username);
            $this->session->set_userdata('cibb_user_roleid' , $data->role_id);

            if ($this->session->userdata('page_url')) {
                echo $this->session->userdata('page_url');
            } else {
                echo 1;
            }
        } else {
            echo 0;
        }
    }
    
    public function logout()
    {
        $this->session->sess_destroy();
        redirect('/');
    }
    
    
}