<div class="span10">  
    <div class="page-header">
        <h1>List of Category</h1>
    </div>
    
    <div class="modal hide" id="modalConfirm">
        <div class="modal-header">
            <button type="button" class="close" data-dismiss="modal">&times;</button>
            <h3>Delete Category</h3>
        </div>
        <div class="modal-body">
            <p style="text-align: center;">Are you sure want to delete this category ?</p>
        </div>
        <div class="modal-footer" style="text-align: center;">
            <a href="#" class="btn" data-dismiss="modal">Cancel</a>
            <a href="#" class="btn btn-primary" id="btn-delete">Delete</a>
        </div>
    </div>
    <?php if (isset($tmp_success_del)): ?>
        <div class="alert alert-info">
            <a class="close" data-dismiss="alert" href="#">&times;</a>
            <h4 class="alert-heading">Category deleted!</h4>
        </div>
    <?php endif; ?>
    
    <style>table td {padding:7px !important;}</style>
    
    <table class="table table-striped table-bordered table-condensed">
        <thead>
            <tr>
                <th width="38%">Category</th>
                <th width="38%">Slug</th>
                <th width="12%"></th>
                <th width="12%"></th>
            </tr>
        </thead>
        <tbody>
            <?php foreach ($categories as $cat): ?>
            <tr>
                <td><?php echo $cat['name']; ?></td>
                <td><?php echo $cat['slug']; ?></td>
                <td style="text-align: center;"><a title="edit" href="<?php echo site_url('admin/category_edit').'/'.$cat['id']; ?>"><img src="<?php echo base_url(); ?>resources/icons/pencil.png"/></a> </td>
                <td style="text-align: center;"><a title="delete" onclick="delete_cat(<?=$cat['id']?>)"><img src="<?php echo base_url(); ?>resources/icons/delete.png"/></a> </td>
            </tr>
            <?php endforeach; ?>
        </tbody>
    </table>
          
</div>

<script>
    $(function() {
        $('#modalConfirm').modal({
            keyboard: true,
            backdrop: true,
            show: false
        });
        
        var cat_id;
        
        $('.del').click(function() {
            cat_id = $(this).attr('id').replace("cat_id_", "");
            $('#modalConfirm').modal('show');
            return false;
        });
        
        $('#btn-delete').click(function() {
            window.location = '<?php echo site_url('admin/category_delete'); ?>/'+cat_id;
        });
    })

    function delete_cat(id) {

        event.preventDefault();
        var form = event.target.form;
        swal({
            title: "Are you sure?",
            text: "You want to be delete this thread!",
            type: "warning",
            showCancelButton: true,
            confirmButtonColor: '#DD6B55',
            confirmButtonText: 'Yes, I am sure!',
            cancelButtonText: "No, cancel it!",
            closeOnConfirm: false,
            closeOnCancel: false
        },
        function (isConfirm) {
            if (isConfirm) {
                
                swal({
                    title: 'Deleted!',
                    text: 'Record is successfully deleted!',
                    type: 'success'
                }, function () {
                    
                    $.ajax({
                        url: '<?php echo base_url(); ?>admin/category_delete',
                        data: {id: id},
                        type: 'post',
                        success: function (data) {
                            if (data == 1) {
                                window.location.href = '<?php echo base_url(); ?>admin/thread_view';
                            } else {
                                $('#error_msg_login').text('Username or Passwird you entered is incorrect!!!').show();
                            }
                        }
                    });
                });

            } else {
                swal("Cancelled", "Your Record is safe :)", "error");
            }
        });
    }
</script>