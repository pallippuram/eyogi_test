<div class="span10">  
    <style>
        th {vertical-align:bottom;text-align:center !important;}

    </style>
    <div class="page-header">
        <h1>List of Threads</h1>
    </div>
    
    <div class="modal hide" id="delConfirm">
        <div class="modal-header">
            <button type="button" class="close" data-dismiss="modal">&times;</button>
            <h3>Delete Category</h3>
        </div>
        <div class="modal-body">
            <p style="text-align: center;">
                Are you sure want to delete this thread ?
                <br/>
                <span style="font-weight: bold;color:#ff0000;font-size: 14px;">All posts in this thread will be deleted<span>
                        </p>
                        </div>
                        <div class="modal-footer" style="text-align: center;">
                            <a href="#" class="btn" data-dismiss="modal">Cancel</a>
                            <a href="#" class="btn btn-primary" id="btn-delete">Delete</a>
                        </div>
                        </div>
                        <?php if (isset($tmp_success)): ?>
                            <div class="alert alert-info">
                                <a class="close" data-dismiss="alert" href="#">&times;</a>
                                <h4 class="alert-heading">Thread updated!</h4>
                            </div>
                        <?php endif; ?>
                        <?php if (isset($tmp_success_del)): ?>
                            <div class="alert alert-info">
                                <a class="close" data-dismiss="alert" href="#">&times;</a>
                                <h4 class="alert-heading">Thread deleted!</h4>
                            </div>
                        <?php endif; ?>
                        <style>table td {padding:7px !important;vertical-align: middle !important;}</style>
                        <script>
                            $(function () {
                                $('.linkviewtip').tooltip();
                            });
                        </script>
                        <table class="table table-striped table-bordered table-condensed">
                            <thead>
                                <tr>
                                    <th width="5%" style="text-align:center;">No</th>
                                    <th width="65%">Threads</th>
                                    <th width="10%">Edit</th>
                                    <th width="10%">Delete</th>
                                    <th width="10%">Keep Sticky</th>
                                     <th width="10%">Closed</th>
                                </tr>
                            </thead>
                            <tbody>
                                <?php foreach ($threads as $key => $thread): ?>
                                    <tr>
                                        <td style="text-align:center;"><?php echo $key + 1 + $start; ?></td>
                                        <td>
                                            <a class="linkviewtip" title="Go to: <?php echo $thread->title; ?>" href="<?php echo site_url('thread/talk/' . $thread->slug); ?>"><?php echo $thread->title; ?></a>
                                            <span style="display:block;font-size: 10px;font-style: italic;"><?php echo $thread->cat_name; ?></span>
                                        </td>
                                        <td style="text-align: center;"><a title="edit" href="<?php echo site_url('admin/thread_edit') . '/' . $thread->id; ?>"><img src="<?php echo base_url(); ?>resources/icons/pencil.png"/></a> </td>
                                        <td style="text-align: center;"><a title="delete" onclick="delete_thread(<?=$thread->id?>)" href=""><img src="<?php echo base_url(); ?>resources/icons/delete.png"/></a></td>

                                        <td style="text-align: center;"><input <?php if($thread->status==1){ echo 'checked'; } ?>  type="checkbox" name="sticky" class="<?php echo $thread->id; ?>" title="Sticky"></td>

                                        <td style="text-align: center;"><input <?php if($thread->open_closed==1){ echo 'checked'; } ?>  type="checkbox" name="status" class="<?php echo $thread->id; ?>" title="Open/Closed"></td>
                                    </tr>
                                <?php endforeach; ?>
                            </tbody>
                        </table>

                        <div class="pagination" style="text-align:center;margin-left: 800px;">
                            <ul><?php echo $page; ?></ul>
                        </div>
                        </div>
<script>
$('input[name="sticky"]').click(function(){ 
    
   var id=$(this).attr('class');
    if ($(this).is(':checked')) {
            var status = 1;
        } else  {
            var status = 0;
        }
    $.ajax({
            url: '<?php echo base_url(); ?>admin/sticky_thread',
            data: {id: id,status:status},
            type: 'post',
            success: function (data) {
                console.log(data);
              
            }
        });
});
$('input[name="status"]').click(function(){ 
    
   var id=$(this).attr('class');
    if ($(this).is(':checked')) {
            var status = 1;
        } else  {
            var status = 0;
        }
    $.ajax({
            url: '<?php echo base_url(); ?>admin/open_closed',
            data: {id: id,status:status},
            type: 'post',
            success: function (data) {
                console.log(data);
              
            }
        });
});
</script>
<script>
    $(function () {
        $('#modalConfirm').modal({
            keyboard: true,
            backdrop: true,
            show: false
        });

        var cat_id;

        $('.del').click(function () {
            thread_id = $(this).attr('id').replace("thread_id", "");
            $('#delConfirm').modal('show');
            return false;
        });

        $('#btn-delete').click(function () {
            window.location = '<?php echo site_url('admin/thread_delete'); ?>/' + thread_id;
        });

    })

    function delete_thread(id) {

        event.preventDefault();
        var form = event.target.form;
        swal({
            title: "Are you sure?",
            text: "You want to be delete this thread!",
            type: "warning",
            showCancelButton: true,
            confirmButtonColor: '#DD6B55',
            confirmButtonText: 'Yes, I am sure!',
            cancelButtonText: "No, cancel it!",
            closeOnConfirm: false,
            closeOnCancel: false
        },
        function (isConfirm) {
            if (isConfirm) {
                
                swal({
                    title: 'Deleted!',
                    text: 'Record is successfully deleted!',
                    type: 'success'
                }, function () {
                    
                    $.ajax({
                        url: '<?php echo base_url(); ?>admin/thread_delete',
                        data: {id: id},
                        type: 'post',
                        success: function (data) {
                            if (data == 1) {
                                window.location.href = '<?php echo base_url(); ?>admin/thread_view';
                            } else {
                                $('#error_msg_login').text('Username or Passwird you entered is incorrect!!!').show();
                            }
                        }
                    });
                });

            } else {
                swal("Cancelled", "Your Record is safe :)", "error");
            }
        });
    }
</script>