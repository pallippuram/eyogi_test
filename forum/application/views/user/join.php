    
<!DOCTYPE html>
<html lang="en">
<head>
    <title>Forum-Login</title>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
<!--===============================================================================================-->  
    <link rel="icon" type="image/png" href="<?php echo base_url(); ?>images/icons/favicon.ico" media='all'>
<!--===============================================================================================-->
    <link rel="stylesheet" type="text/css" href="<?php echo base_url(); ?>vendor/bootstrap/css/bootstrap.min.css" media='all'>
<!--===============================================================================================-->
    <link rel="stylesheet" type="text/css" href="<?php echo base_url(); ?>fonts/font-awesome-4.7.0/css/font-awesome.min.css" media='all'>
<!--===============================================================================================-->
    <link rel="stylesheet" type="text/css" href="<?php echo base_url(); ?>fonts/Linearicons-Free-v1.0.0/icon-font.min.css" media='all'>
<!--===============================================================================================-->
    <link rel="stylesheet" type="text/css" href="<?php echo base_url(); ?>vendor/animate/animate.css" media='all'>
<!--===============================================================================================-->  
    <link rel="stylesheet" type="text/css" href="<?php echo base_url(); ?>vendor/css-hamburgers/hamburgers.min.css" media='all'>
<!--===============================================================================================-->
    <link rel="stylesheet" type="text/css" href="<?php echo base_url(); ?>vendor/animsition/css/animsition.min.css" media='all'>
<!--===============================================================================================-->
    <link rel="stylesheet" type="text/css" href="<?php echo base_url(); ?>vendor/select2/select2.min.css" media='all'>
<!--===============================================================================================-->  
    <link rel="stylesheet" type="text/css" href="<?php echo base_url(); ?>vendor/daterangepicker/daterangepicker.css" media='all'>
<!--===============================================================================================-->
    <link rel="stylesheet" type="text/css" href="<?php echo base_url(); ?>css/util.css" media='all'>
    <link rel="stylesheet" type="text/css" href="<?php echo base_url(); ?>css/main.css" media='all'>
<!--===============================================================================================-->
</head>
<body>
    
    <div class="limiter">
        <div class="container-login100">
            <div class="wrap-login100 p-t-50 p-b-90">
                <form id="login_popup" action="<?php echo base_url(); ?>user/join" method="post" class="login100-form validate-form flex-sb flex-w">
                    <span class="login100-form-title p-b-51">
                        Login
                    </span>

                    
                    <div class="wrap-input100 validate-input m-b-16" data-validate = "Username is required">
                        <input class="input100" type="text" id="login_email" name="email" placeholder="Username">
                        <span class="focus-input100"></span>
                    </div>
                    
                    
                    <div class="wrap-input100 validate-input m-b-16" data-validate = "Password is required">
                        <input class="input100" type="password" id="login_password" name="password" placeholder="Password">
                        <span class="focus-input100"></span>
                    </div>
                    
                    <div class="flex-sb-m w-full p-t-3 p-b-24">
                        <div class="contact100-form-checkbox">
                            <input class="input-checkbox100" id="ckb1" type="checkbox" name="remember-me">
                            <label class="label-checkbox100" for="ckb1">
                                Remember me
                            </label>
                        </div>

                        <div>
                            <a href="#" class="txt1">
                                Forgot?
                            </a>
                        </div>
                    </div>

                    <div class="container-login100-form-btn m-t-17">
                        <button class="login100-form-btn" name="btn-login" value="Login">
                            Login
                        </button>
                    </div>

                </form>
            </div>
        </div>
    </div>
    

    <div id="dropDownSelect1"></div>
    
<!--===============================================================================================-->
    <script type='text/javascript' src="<?php echo base_url(); ?>vendor/jquery/jquery-3.2.1.min.js"></script>
<!--===============================================================================================-->
    <script type='text/javascript' src="<?php echo base_url(); ?>vendor/animsition/js/animsition.min.js"></script>
<!--===============================================================================================-->
    <script type='text/javascript' src="<?php echo base_url(); ?>vendor/bootstrap/js/popper.js"></script>
    <script type='text/javascript' src="<?php echo base_url(); ?>vendor/bootstrap/js/bootstrap.min.js"></script>
<!--===============================================================================================-->
    <script type='text/javascript' src="<?php echo base_url(); ?>vendor/select2/select2.min.js"></script>
<!--===============================================================================================-->
    <script type='text/javascript' src="<?php echo base_url(); ?>vendor/daterangepicker/moment.min.js"></script>
    <script type='text/javascript' src="<?php echo base_url(); ?>vendor/daterangepicker/daterangepicker.js"></script>
<!--===============================================================================================-->
    <script type='text/javascript' src="<?php echo base_url(); ?>vendor/countdowntime/countdowntime.js"></script>
<!--===============================================================================================-->
    <script type='text/javascript' src="<?php echo base_url(); ?>js/main.js"></script>



<script type="text/javascript">
    
    $(document).ready(function () {

        $('#login_popup').submit(function (e) {
            var ret = true;

            if (!isEmail($('#login_email').val())) {

                $('#login_email').parent().parent().addClass("error");
                $('#login_email').attr("placeholder", "Please enter a valid email address");
                ret = false;
            }

            if ($('#login_password').val() == "") {

                $('#login_password').parent().parent().addClass("error");
                $('#login_password').attr("placeholder", "Please enter your password");
                ret = false;
            }
            if (ret != false) {
                $.ajax({
                    url: $(this).attr('action'),
                    data: $(this).serialize(),
                    type: 'post',
                    success: function (data) {
                        if (data == 1) {
                            window.location.href = '<?php echo base_url(); ?>thread/index';
                        } else if (data == 0) {
                            $('#error_msg_login').text('Username or Password you entered is incorrect!!!').show();
                        } else {
                            window.location.href = window.location.href = '<?php echo base_url(); ?>home/schedule';
                        }
                    }
                });
            }

            e.preventDefault();
        });
    });

    function isEmail(email) {
        var regex = /^([a-zA-Z0-9_.+-])+\@(([a-zA-Z0-9-])+\.)+([a-zA-Z0-9]{2,4})+$/;
        return regex.test(email);
    }
</script>
</body>
</html>
    