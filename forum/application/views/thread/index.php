<div style="height: 650px;" class="row-fluid">
    
    <?php
    function time_ago($date) {

        if(empty($date)) {
            return "No date provided";
        }

        $periods = array("second", "minute", "hour", "day", "week", "month", "year", "decade");
        $lengths = array("60","60","24","7","4.35","12","10");
        $now = time();
        $unix_date = strtotime($date);

        // check validity of date

        if(empty($unix_date)) {
            return "Bad date";
        }

        // is it future date or past date
        if($now > $unix_date) {
            $difference = $now - $unix_date;
            $tense = "ago";
        } else {
            $difference = $unix_date - $now;
            $tense = "from now";
        }
        for($j = 0; $difference >= $lengths[$j] && $j < count($lengths)-1; $j++) {
            $difference /= $lengths[$j];
        }
        $difference = round($difference);
        if($difference != 1) {
            $periods[$j].= "s";
        }

        return "$difference $periods[$j] {$tense}";
    }
    ?>
    <?php if ($this->session->userdata('cibb_logged_in') == true){ ?>
        
        <style>table td, table th {padding:10px 7px !important;} .cat {font-weight:bold;font-size: 10px;color: #333;font-style: italic;}</style>
        <table class="table table-striped table-condensed">
        <thead>
            <tr><th width="20%"></th>
                <th width="65%">All Threads</th>
                <th width="10%">Last Updates</th>
                <th width="5%"></th>
            </tr>
        </thead>
        <tbody>

            <?php foreach($threads as $thread): ?>
            <tr>
                <td><div class="votes" style="display: inline-block;text-align: center">
                        <div class="mini-counts"><span title="0 votes" style="font-size: 18px;">
                        <?php if($thread->vote == ""){ 
                            echo 0; 
                        }else{
                            echo $thread->vote;
                        }
                        ?>
                        </span></div>
                        <div style="text-align: center">votes</div>
                    </div>
                    <div class="answer" style="display: inline-block;text-align: center">
                        <div class="mini-counts"><span title="0 answer" style="font-size: 18px;"><?php echo $thread->ans ?></span>
                        </div>
                        <div style="text-align: center">answer</div>
                    </div>
          <!--<div class="views" style="display: inline-block;text-align: center">
            <div class="mini-counts"><span title="0 views" style="font-size: 18px;">5</span></div>
            <div style="text-align: center">views</div>
          </div> -->


                </td>   
                <td style="font-size:12px;">
                    <a style="font-family: verdana;" href="<?php echo site_url('thread/talk/'.$thread->slug); ?>"><?php echo $thread->title; ?></a>
                    <?php if($thread->open_closed==1){ ?>
                        <span style="margin-left: 5px;color:#fff;background:red;border-radius: 15px;padding: 0px 5px">Closed
                        </span> 
                    <?php } ?>
                    <span style="display: block">
                        <a href="<?php echo site_url('thread/category/'.$thread->category_slug); ?>" class="cat">Category: <?php echo $thread->category_name; ?></a>
                    </span>
                </td>

                <td style="font-size:12px;color:#999;vertical-align: middle;">
                    <!-- <?php echo date("m/d/y g:i A", strtotime($thread->date_add)); ?> -->
                    <?php echo time_ago($thread->date_add); ?>
            
                </td>
                <td>
                    <?php if($thread->status==1){ ?>
                        <i class="glyphicon glyphicon-pushpin" style="color: #ff8e16;font-size: 18px"></i>
                    <?php } ?>
             
                </td>
            </tr>
            <?php endforeach; ?>
        </tbody>
        </table>

    <?php } ?>
    
    <div class="pagination" style="text-align:center;height: 41px;margin: -11px;margin-left: 1190px;">
        <ul><?php echo $page; ?></ul>
    </div>
</div>