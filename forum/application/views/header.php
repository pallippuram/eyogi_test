<!DOCTYPE html>
<html lang="en">
    <head>
    <title>eYogi Forum</title>
    
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css">
    <link rel="stylesheet" href="<?php echo base_url(); ?>resources/bootstrap/css/bootstrap.min.css"/>
    <link rel="stylesheet" href="<?php echo base_url(); ?>resources/bootstrap/css/bootstrap-responsive.min.css"/>
    <script src="<?php echo base_url(); ?>resources/bootstrap/js/jquery-1.7.2.min.js"></script>
    <script src="<?php echo base_url(); ?>resources/bootstrap/js/bootstrap.min.js"></script>
    <link rel='stylesheet' href='<?php echo base_url(); ?>css/sweet-alert.css' type='text/css' media='all' />


    <style type="text/css">
        
        .navbar-inner{
           
            position: fixed;
            top: 0;
            left: 0;
            right: 0;
            background: #fff !important;
            min-width: 1260px;
            z-index: 2000;
            border-bottom: 1px solid #fff;
            color: #fff !important;
            padding-top:10px;
            padding-bottom: 10px;
        }
        .navbar-inner a {
            color: #000;
        }
        

    </style>

    </head>
    <body>
        <div class="navbar" id="nav">
            <div class="navbar-inner">
                <div class="container">
                    <a class="main-logo" alt="eYogi" href="" style="float: left;"><img src="http://localhost/eYogi_local/assets/images/logo.png" ></a>
                    <div class="nav-collapse">
                        <ul class="nav pull-right">                        
                            <?php if ($this->session->userdata('cibb_logged_in') != 1){ ?>
                            <li class="dropdown">
                            <a href="<?php echo site_url('user/login'); ?>" style="color: #827ffe;font-size: 14px;" class="button dropdown-toggle">Login <b class="caret"></b></a>
                            <!-- <ul class="dropdown-menu">
                                <li>
                                    <form class="well" action="<?php echo site_url('user/join'); ?>" method="post" style="margin: 5px 10px;">
                                    <label style="color: #ff9128;">Username</label>
                                    <input type="text" name="row[username]" class="span3" placeholder="">
                                    <label style="color: #ff9128;">Password</label>
                                    <input type="password" name="row[password]" class="span3" placeholder="">
                                    <input type="submit" name="btn-login" class="btn btn-primary" value="Login"/>
                                    </form>
                                </li>
                            </ul> -->
                            
                            </li>
                            <?php } else { ?>
                                <ul class="nav">
                                    <li><button id="btn-new-thread" class="btn btn-primary btn-mini">New Thread</button></li>
                                </ul>
                                <?php if ($this->session->userdata('cibb_logged_in') == true){
                                    $userid = $this->session->userdata('cibb_user_roleid');
                                    if($userid == 1){ ?>
                                        <li><a style="color:#827ffe;text-transform: capitalize;" href="<?php echo site_url('admin/index');?>"><?php echo $data1[0]->username; ?></a></li>
                                    <?php }else { ?>
                                        <li><a style="color:#827ffe;text-transform: capitalize;" href="#"><?php echo $data1[0]->username; ?></a></li>
                                    <?php }
                                } ?>
                                

                                <li><a style="color:#827ffe;" href="<?php echo site_url('user/logout'); ?>">Logout</a></li>
                        <?php } ?>
                    </ul>
                        
                    </div><!-- /.nav-collapse -->
                </div>
            </div><!-- /navbar-inner -->
        </div><!-- /navbar -->

<script type="text/javascript">

    $(function() {
        $('#btn-new-thread').click(function() {
            window.location = '<?php echo site_url('thread/create'); ?>';
        });
    });

</script>

    </body> 
    </html>           
        