$(function() {


  // Initialize form validation on the registration form.
  // It has the name attribute "registration"
  $("form[name='contact1']").validate({
    // Specify validation rules
    rules: {
      // The key name on the left side is the name attribute
      // of an input field. Validation rules are defined
      // on the right side
      name: "required",
   
      
      subject12: {
        required: true,
               
    
      },
      message12: {
        required: true,
               
    
      },
      email12: {
        required: true,
        // Specify that email should be validated
        // by the built-in "email" rule
        email: true
      },

    },
    // Specify validation error messages
    messages: {
        
      email12: "Please Enter a valid email address",
  
      subject12:"Please Enter subject",

      message12:"Please Enter Message"


    },
    // Make sure the form is submitted to the destination defined
    // in the "action" attribute of the form when valid
    submitHandler: function(form) {
      event.preventDefault();
   
      var form = $('#contactForm')[0]; 
     
      var formData = new FormData(form);

     var base_url=$("#base").val();
     
    $.ajax({
        url: base_url+"home/contactform",
        type: 'POST',
        data: formData,
         processData: false,
          contentType: false,
        async: false,
        success: function (data) {
         //  alert(JSON.stringify(data));
          
            $("#email12").val("");
            $("#subject12").val("");
            $("#message12").val("");
            $('#succes').fadeIn();
            $('#succes').html("");
  
            $('#succes').append("Details Submitted Successfully");

            $('#succes').fadeOut(3000);
        },
       
    });

    return false;
    }
  });
   $("form[name='contact2']").validate({
    // Specify validation rules
    rules: {
      // The key name on the left side is the name attribute
      // of an input field. Validation rules are defined
      // on the right side
      name13: "required",
   
      
       phone13: {
        required: true,
        number:true,
        maxlength:10,
        minlength:10
               
    
      },
      message13: {
        required: true,
               
    
      },
      email13: {
        required: true,
        // Specify that email should be validated
        // by the built-in "email" rule
        email: true
      },

    },
    // Specify validation error messages
    messages: {
    
        name13 : "Please Enter Your Name",
        email13: "Please Enter a valid email address",
  
        phone13:"Please Enter Your Number",

        message13:"Please Enter Message"


    },
    // Make sure the form is submitted to the destination defined
    // in the "action" attribute of the form when valid
    submitHandler: function(form) {
      event.preventDefault();
   
      var form = $('#contactForm2')[0]; 
     
      var formData = new FormData(form);

     var base_url=$("#base1").val();
 // alert(base_url);

    $.ajax({
        url: base_url+"home/enquiry",
        type: 'POST',
        data: formData,
         processData: false,
          contentType: false,
        async: false,
        success: function (data) {
        //   alert(JSON.stringify(data));
          
           $("#email13").val("");
           $("#name13").val("");
           $("#phone13").val("");
           $("#message13").val("");
          

$('#succes5').fadeIn();
$('#succes5').html("");
  
$('#succes5').append("Details Submitted Successfully");

$('#succes5').fadeOut(3000);
        },
       
    });

    return false;
    }
  });
  
  
  $("form[name='contact5']").validate({
    // Specify validation rules
    rules: {
      // The key name on the left side is the name attribute
      // of an input field. Validation rules are defined
      // on the right side
      name: "required",
   
      
      subject14: {
        required: true,
               
    
      },
      message14: {
        required: true,
               
    
      },
      email14: {
        required: true,
        // Specify that email should be validated
        // by the built-in "email" rule
        email: true
      },

    },
    // Specify validation error messages
    messages: {
        
      email14: "Please Enter a valid email address",
  
      subject14:"Please Enter subject",

      message14:"Please Enter Message"


    },
    // Make sure the form is submitted to the destination defined
    // in the "action" attribute of the form when valid
    submitHandler: function(form) {
      event.preventDefault();
   
      var form = $('#contactForm5')[0]; 
     
      var formData = new FormData(form);

     var base_url=$("#base5").val();
     
    $.ajax({
        url: base_url+"home/contactform1",
        type: 'POST',
        data: formData,
         processData: false,
          contentType: false,
        async: false,
        success: function (data) {
         //  alert(JSON.stringify(data));
          
            $("#email14").val("");
            $("#subject14").val("");
            $("#message14").val("");
            $('#succes').fadeIn();
            $('#succes').html("");
  
            $('#succes').append("Details Submitted Successfully");

            $('#succes').fadeOut(3000);
        },
       
    });

    return false;
    }
  });

});