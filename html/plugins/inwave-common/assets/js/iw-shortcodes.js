(function($){
    "use strict";

    /**
     * Tabs
     */
    $.fn.iwTabs = function (type) {
        $(this).each(function () {
            var iwTabObj = this, $iwTab = $(this);
            if (type === 'tab') {
                iwTabObj.content_list = $iwTab.find('.iw-tab-content .iw-tab-item-content');
                iwTabObj.list = $iwTab.find('.iw-tab-items .iw-tab-item');
                iwTabObj.item_click_index = 0;
                $('.iw-tab-items .iw-tab-item', this).click(function () {
                    if ($(this).hasClass('active')) {
                        return;
                    }
                    var itemclick = this, item_active = $iwTab.find('.iw-tab-items .iw-tab-item.active');
                    iwTabObj.item_click_index = iwTabObj.list.index(itemclick);
                    $(itemclick).addClass('active');
                    iwTabObj.list.each(function () {
                        if (iwTabObj.list.index(this) !== iwTabObj.list.index(itemclick) && $(this).hasClass('active')) {
                            $(this).removeClass('active');
                        }
                    });
                    iwTabObj.loadTabContent();
                });
                this.loadTabContent = function () {
                    var item_click = $(iwTabObj.content_list.get(iwTabObj.item_click_index));
                    iwTabObj.content_list.each(function () {
                        if (iwTabObj.content_list.index(this) < iwTabObj.content_list.index(item_click)) {
                            $(this).addClass('prev').removeClass('active next');
                        } else if (iwTabObj.content_list.index(this) === iwTabObj.content_list.index(item_click)) {
                            $(this).addClass('active').removeClass('prev next');
//                            $(".map-contain",this).iwMap();
                        } else {
                            $(this).addClass('next').removeClass('prev active');
                        }
                    });
                };
            } else {
                this.accordion_list = $iwTab.find('.iw-accordion-item');
                $('.iw-accordion-header', this).click(function () {
                    var itemClick = $(this);
                    var item_target = itemClick.parent();
                    if (itemClick.hasClass('active')) {
                        itemClick.removeClass('active');
                        item_target.find('.iw-accordion-content').slideUp({easing: 'easeOutQuad'});
                        item_target.find('.iw-accordion-header-icon .expand').hide();
                        item_target.find('.iw-accordion-header-icon .no-expand').show();
                        return;
                    }
                    itemClick.addClass('active');
                    item_target.find('.iw-accordion-content').slideDown({easing: 'easeOutQuad'});
                    item_target.find('.iw-accordion-header-icon .expand').show();
                    item_target.find('.iw-accordion-header-icon .no-expand').hide();
                    iwTabObj.accordion_list.each(function () {
                        if (iwTabObj.accordion_list.index(this) !== iwTabObj.accordion_list.index(item_target) && $(this).find('.iw-accordion-header').hasClass('active')) {
                            $(this).find('.iw-accordion-header').removeClass('active');
                            $(this).find('.iw-accordion-content').slideUp({easing: 'easeOutQuad'});
                            $(this).find('.iw-accordion-header-icon .expand').hide();
                            $(this).find('.iw-accordion-header-icon .no-expand').show();
                        }
                    });
                });

                $('.iw-accordion-header', this).hover(function () {
                    var item = $(this), item_target = item.parent();
                    if (item.hasClass('active')) {
                        return;
                    }
                    item_target.find('.iw-accordion-header-icon .expand').show();
                    item_target.find('.iw-accordion-header-icon .no-expand').hide();
                }, function () {
                    var item = $(this), item_target = item.parent();
                    if (item.hasClass('active')) {
                        return;
                    }
                    item_target.find('.iw-accordion-header-icon .expand').hide();
                    item_target.find('.iw-accordion-header-icon .no-expand').show();
                });
            }

        });
    };
})(jQuery);


jQuery(document).ready(function($){
    /**
     * Video
     */
    $('.iw-video .play-button').click(function () {
        if (!$(this).parents('.iw-video').hasClass('playing')) {
            $(this).parents('.iw-video').find('video').get(0).play();
            $(this).parents('.iw-video').addClass('playing');
            return false;
        }
    });
	
    $('.iw-video,.iw-event-facts').click(function () {
        $(this).find('video').get(0).pause();
    });
    $('.iw-video video').on('pause', function (e) {
        $(this).parents('.iw-video').removeClass('playing');
    });

     $(document).on('invalid.wpcf7', function () {
        $('.wpcf7-form .iw-form-step').hide();
        $('.wpcf7-form .wpcf7-not-valid:eq(0)').closest('.iw-form-step').fadeIn();
    });

    $('.wpcf7-form .next-step').click(function(e){
        e.preventDefault();
        var parent = $(this).closest('.iw-form-step');
        parent.hide();
        parent.next('.iw-form-step').fadeIn();
    });

    $('.wpcf7-form .prev-step').click(function(e){
        e.preventDefault();
        var parent = $(this).closest('.iw-form-step');
        parent.hide();
        parent.prev('.iw-form-step').fadeIn();
    });

    if($('.inwave-funfact').length){
        $('.inwave-funfact').waypoint(function() {
            var default_settings =  {
                from: 0,
                to: 0,
                speed: 2500,
                refreshInterval: 50,
                formatter: function(value, settings){
                    if(settings.add_comma){
                        return value.toFixed(settings.decimals).replace(/\B(?=(\d{3})+(?!\d))/g, ",");
                    }
                    else{
                        return value.toFixed(settings.decimals);
                    }
                }
            };

            var settings = $(this).data('settings');
            settings = $.extend({}, default_settings, settings);
            $(".funfact-number", this).countTo(settings);
        },{
            triggerOnce: true,
            offset: '90%'
        });
    }

    //countdown
    if ( $('.inwave-countdown').length > 0) {
        $('.inwave-countdown').each(function () {
            var countdown = $(this).data('countdown');
            var date_number = $(this).data('date-number');
            $(this).countdown(countdown, function(event){
                var inwave_day = event.strftime('%-D');
                var inwave_hour = event.strftime('%-H');
                var inwave_minute = event.strftime('%-M');
                var inwave_second = event.strftime('%-S');

                $(this).find('.day').html(inwave_day);
                $(this).find('.hour').html(inwave_hour);
                $(this).find('.minute').html(inwave_minute);
                $(this).find('.second').html(inwave_second);
                var w_line = (((date_number - inwave_day) / date_number ) * 100);
                $('.price-line .line').css("width", w_line+"%");
                if (w_line >= 48) {
                    var price_center = $('.price-countdown .price.price-center');
                    price_center.addClass('active');
                }
                if (w_line >= 98) {
                    var price_end = $('.price-countdown .price.price-end');
                    price_end.addClass('active');
                }
            })
        });
    }
    /** hash link scroll */
    $('a').click(function (e) {
        var anchor = $(this).attr('href');
        if (!$(this).hasClass('ui-tabs-anchor') && anchor.indexOf('#') >= 0 && $(anchor).length) {
            var top = $(anchor).offset().top;
            $('html, body').animate({
                scrollTop: top
            }, 'slow');
            e.preventDefault();
        } else {
            if (anchor == '#') {
                e.preventDefault();
            }
        }
        var ul = $(this).parents('.wc-menu-content ul');
        if(ul.length){
            ul.find('li').removeClass('active');
            $(this).parent().addClass('active');
        }
    });

    $(window).on("load resize", function () {
        /* WC Version */
        var container_with = $('body .container').outerWidth();
        var window_with = $(window).width();
        $('.wc-version-left').css({'padding-left' : (window_with - container_with) /2});
        $('.wc-version-right').css({'padding-right' : (window_with - container_with) /2, 'padding-left' : 0});
    });

    /* accordions */
    var id_accordions = $(".iw-accordions").data('id');
    $("#"+id_accordions).iwTabs("accordion");

    /* accordions */
    $('.faq-accordion-item').each(function() {
        $('.faq-accordion-header', this).click(function() {
            if($(this).hasClass('active')){
                $(this).removeClass('active');

            }else{
                var parent = $(this).closest('.iw-accordions-items');
                var itemClick = $(this);
                parent.find('.faq-accordion-header.active').removeClass('active');
                itemClick.addClass('active');
            }
        });
    });

    /* Load more FAQ */
    var ppp = $('.iw-faqs').data('posts_per_page'); // Post per page
    var pageNumber = 1;
    var totalpages  =  $('#hidden_data_total_post').data('totalpost'); // get total pages

    $("#load-more-faq").on("click",function(){
        var itemTarget = $(this);
        pageNumber++;
        var str = '&pageNumber=' + pageNumber + '&ppp=' + ppp + '&action=more_faq_ajax';
        $.ajax({
            type: "POST",
            dataType: "html",
            url: ajax_posts.ajaxurl,
            cache: false,
            data: str,
            beforeSend: function (xhr) {
                itemTarget.addClass('loading');
            },
            success: function(data){
                var $data = $(data);
                if($data.length){
                    $(".iw-faqs-main").append($data);
                } else{
                    $("#load-more-faq").addClass("loaded");
                }

                if(totalpages === pageNumber){ // check total pages
                    $("#load-more-faq").addClass("loaded");
                }

                itemTarget.removeClass('loading');
            }

        });
        return false;
    });
});

function iwaveSetCookie(cname, cvalue, exdays) {
    var d = new Date();
    d.setTime(d.getTime() + (exdays*24*60*60*1000));
    var expires = "expires="+d.toUTCString();
    document.cookie = cname + "=" + cvalue + "; " + expires;
}

function iwaveGetCookie(cname) {
    var name = cname + "=";
    var ca = document.cookie.split(';');
    for(var i=0; i<ca.length; i++) {
        var c = ca[i];
        while (c.charAt(0)==' ') c = c.substring(1);
        if (c.indexOf(name) == 0) return c.substring(name.length, c.length);
    }
    return "";
}

function iwaveCheckCookie() {
    var user = getCookie("username");
    if (user != "") {
        alert("Welcome again " + user);
    } else {
        user = prompt("Please enter your name:", "");
        if (user != "" && user != null) {
            setCookie("username", user, 365);
        }
    }
}