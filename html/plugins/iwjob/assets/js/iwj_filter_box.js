
jQuery(document).ready(function($) {
    $('body').delegate('li.iwj-filter-selected-item', 'click', function(e) {
        e.preventDefault();
        $(this).remove();

        var term_id = $(this).data('termid');
        var type = $(this).data('type');

        if(typeof window.iwj_before_remove_filter_callback == 'object'){
            for (var key in window.iwj_before_remove_filter_callback) {
                // skip loop if the property is from prototype
                if (!window.iwj_before_remove_filter_callback.hasOwnProperty(key)) continue;

                var callback_function = window.iwj_before_remove_filter_callback[key];
                if(typeof callback_function == 'function'){
                    callback_function(term_id, type);
                }
            }
        }

        var in_form_class = 'input#iwjob-tax-url-'+term_id;
        $('form[name="iwjob-filter-url"]').find(in_form_class).remove();
        if (type == 'candidate') {
            $('#iwjob-filter-candidates-cbx-'+term_id).trigger('click');
        } else if (type == 'employer') {
            $('#iwjob-filter-employers-cbx-'+term_id).trigger('click');
        } else {
            $('#iwjob-filter-jobs-cbx-'+term_id).trigger('click');
        }

        iwj_filter_common.display_filter_box();

        if(typeof window.iwj_after_remove_filter_callback == 'object'){
            for (var key in window.iwj_after_remove_filter_callback) {
                // skip loop if the property is from prototype
                if (!window.iwj_after_remove_filter_callback.hasOwnProperty(key)) continue;

                var callback_function = window.iwj_after_remove_filter_callback[key];
                if(typeof callback_function == 'function'){
                    callback_function(term_id, type);
                }
            }
        }
    });
});



